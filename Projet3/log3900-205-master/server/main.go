package main

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/controllers"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/middlewares"
)

func main() {
	instance := engine.NewEngine()

	instance.LoadConfig("./config.json")
	instance.Use(middlewares.CORS)

	registerControllers(instance)
	instance.Run()
}

func registerControllers(instance *engine.Engine) {
	controllers := []engine.IController{
		controllers.NewRoomController(),
		controllers.NewUserController(),
		controllers.NewAuthController(),
		controllers.NewAlbumController(),
		controllers.NewDrawingController(),
		controllers.NewContactController(),
		controllers.NewExpositionController(),
	}

	for _, controller := range controllers {
		controller.RegisterRoutes(instance)
	}
}
