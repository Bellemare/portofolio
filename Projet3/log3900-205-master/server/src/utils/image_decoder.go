package utils

import (
	"encoding/base64"
	"errors"
	"net/http"
	"strings"
)

const (
	MimePng  = "image/png"
	MimeJpeg = "image/jpeg"
)

func Base64ImageDecode(base64Img string) ([]byte, error) {
	imageData := strings.Split(base64Img, "base64,")
	err := errors.New("Impossible de lire l'image")
	if len(imageData) < 2 {
		return nil, err
	}

	data, decodeErr := base64.StdEncoding.DecodeString(imageData[1])
	if decodeErr != nil {
		return nil, err
	} else {
		mimeType := http.DetectContentType(data)
		if mimeType != MimePng && mimeType != MimeJpeg {
			return nil, errors.New("Image de mauvais format")
		}
	}

	return data, nil
}

func ByteImageToBase64(image []byte) string {
	mimeType := http.DetectContentType(image)

	img := base64.StdEncoding.EncodeToString(image)
	switch mimeType {
	case MimeJpeg:
		img = "data:image/jpeg;base64," + img
	case MimePng:
		img = "data:image/png;base64," + img
	}

	return img
}
