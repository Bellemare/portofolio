package utils

import "strconv"

func ParseModelId(id string) (uint, error) {
	parsedId, err := strconv.ParseUint(id, 10, 64)
	if err != nil {
		return 0, err
	}
	return uint(parsedId), nil
}
