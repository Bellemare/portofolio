package utils

import (
	"bytes"
	"encoding/json"
)

func EncodeMessage(content interface{}) ([]byte, error) {
	var buf bytes.Buffer
	encoder := json.NewEncoder(&buf)

	err := encoder.Encode(content)
	if err != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

func DecodeMessage(content []byte, dst interface{}) error {
	buf := bytes.NewBuffer(content)
	decoder := json.NewDecoder(buf)

	err := decoder.Decode(dst)
	if err != nil {
		return err
	}

	return nil
}
