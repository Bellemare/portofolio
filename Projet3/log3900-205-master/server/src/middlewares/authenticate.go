package middlewares

import (
	"net/http"
	"strings"

	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/entities"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/services"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/utils"
	"gorm.io/gorm"
)

func AuthenticateUser(route *engine.Route, ctx *engine.Context) {
	authorization := ctx.Request.Header.Get("Authorization")
	splitToken := strings.Split(authorization, "Bearer ")
	if len(splitToken) != 2 {
		authenticationError(route, ctx)
		return
	}
	token := splitToken[1]
	if payload, ok := verifySession(token); ok {
		ctx.Authentication = payload
		return
	}
	authenticationError(route, ctx)
}

func verifySession(token string) (*engine.AuthTokenPayload, bool) {
	authService := services.NewAuthService()
	payload, err := authService.ValidateToken(token)
	if err != nil {
		return nil, false
	}

	valid := validateSession(payload)
	if !valid {
		return nil, false
	}
	return payload, true
}

func authenticationError(route *engine.Route, ctx *engine.Context) {
	ctx.WriteJSONError(http.StatusUnauthorized, engine.NewError("Authentification invalide"))
	route.CancelExecution()
}

func validateSession(token *engine.AuthTokenPayload) bool {
	session := entities.NewSession()
	query := &models.SessionModel{}
	id, err := utils.ParseModelId(token.Id)
	if err != nil {
		return false
	}
	query.Model = gorm.Model{
		ID: id,
	}
	res, err := session.Get(query)

	return err == nil && res.Valid
}
