package middlewares

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
)

func AuthenticateSocketClient(route *engine.Route, ctx *engine.Context) {
	token := ctx.GetParam("token")
	if token == "" {
		authenticationError(route, ctx)
		return
	}
	if payload, ok := verifySession(token); ok {
		ctx.Authentication = payload
		return
	}
	authenticationError(route, ctx)
}
