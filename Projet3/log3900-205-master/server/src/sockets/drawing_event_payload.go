package sockets

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/sockets/drawing"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/utils"
)

const (
	DrawingEventTypeDraw uint = iota
	DrawingEventTypePreview
)

type SocketDrawingEvent struct {
	UserID uint                 `json:"user_id"`
	User   string               `json:"user"`
	Event  drawing.DrawingEvent `json:"event"`
}

type DrawingEventPayload struct {
	SocketPayload
	Payload SocketDrawingEvent `json:"payload"`
}

func NewDrawingEventPayload(payload []byte) *DrawingEventPayload {
	var drawingEventPayload DrawingEventPayload
	utils.DecodeMessage(payload, &drawingEventPayload)

	return &drawingEventPayload
}

func (payload *DrawingEventPayload) HandleNewEvent(client *SocketClient) bool {
	payload.Payload.UserID = client.auth.UserID
	if user := client.hub.getCachedUser(client.auth.UserID); user != nil {
		payload.Payload.User = user.Pseudonym
	}
	if drawingHub, ok := client.hub.socketHubHandler.(*drawingHubHandler); ok {
		drawingHub.deleteCachedPreviewEvents(client)
		if !drawingHub.saveDrawingEvent(&payload.Payload.Event, client) {
			return false
		}
	}

	return payload.broadcast(payload, client)
}
