package sockets

import (
	"sync"
	"time"

	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/sockets/drawing"
)

const (
	clientTimeoutBetweenLayerPreviewEventsMS = 50
)

type LayerEventSynchronizer interface {
	updateSync(LayerEventSynchronizer)
	getLayer() int
	getID() int
	setLayer(int)
	synchronize(payloadData interface{}, client *SocketClient) bool
	send(payloadData interface{}, client *SocketClient) bool
}

type layerHandler struct {
	clientMetricsHandler *clientMetricsHandler

	selectedDrawingEvents    map[uint]*LayerEvent
	cachedLayerPreviewEvents map[uint]LayerEventSynchronizer
	cachedWaitingSyncLayer   map[uint]LayerEventSynchronizer

	cachedLayerTextEvents      map[uint]LayerEventSynchronizer
	cachedWaitingSyncLayerText map[uint]LayerEventSynchronizer

	clientsLayerPreviewSyncTimeout map[uint]bool

	layerPreviewLock *sync.Mutex
	drawingLock      *sync.Mutex

	cachedDrawingEvents []drawing.DrawingEvent
	maxID               int
}

func newLayerHandler(clientMetricsHandler *clientMetricsHandler) *layerHandler {
	hub := &layerHandler{
		clientMetricsHandler,
		make(map[uint]*LayerEvent),
		make(map[uint]LayerEventSynchronizer),
		make(map[uint]LayerEventSynchronizer),
		make(map[uint]LayerEventSynchronizer),
		make(map[uint]LayerEventSynchronizer),
		make(map[uint]bool),
		&sync.Mutex{},
		&sync.Mutex{},
		[]drawing.DrawingEvent{},
		0,
	}

	return hub
}

func (handler *layerHandler) saveDrawingEvent(event *drawing.DrawingEvent, client *SocketClient) bool {
	defer handler.drawingLock.Unlock()
	handler.drawingLock.Lock()

	if event.NewDrawingElement().IsEmpty() {
		return false
	}

	handler.maxID++
	event.ID = handler.maxID
	handler.cachedDrawingEvents = append(handler.cachedDrawingEvents, *event)
	handler.clientMetricsHandler.onEditDrawing(client)

	return true
}

func (handler *layerHandler) handleLayerEvent(layerEvent *LayerEvent, client *SocketClient) bool {
	defer handler.drawingLock.Unlock()
	handler.drawingLock.Lock()

	layer := handler.findLayerByID(layerEvent.ID)
	if layer == -1 {
		return false
	}
	layerEvent.Layer = layer

	switch layerEvent.Type {
	case LayerDeleteEvent:
		handler.deleteLayer(layerEvent.Event, layer)
		handler.clientMetricsHandler.onEditDrawing(client)
	case LayerChangeEvent:
		handler.changeLayer(layerEvent.Event, layer)
		delete(handler.cachedLayerPreviewEvents, client.auth.UserID)
		delete(handler.selectedDrawingEvents, client.auth.UserID)
		handler.clientMetricsHandler.onEditDrawing(client)
	case LayerSelectEvent:
		if handler.isSelected(layerEvent.ID) {
			return false
		}
		handler.selectLayer(layerEvent, client)
	}

	return true
}

func (handler *layerHandler) deleteLayer(event drawing.DrawingEvent, layer int) {
	if layer < len(handler.cachedDrawingEvents) {
		handler.cachedDrawingEvents = append(handler.cachedDrawingEvents[:layer], handler.cachedDrawingEvents[layer+1:]...)
	}
}

func (handler *layerHandler) changeLayer(event drawing.DrawingEvent, layer int) {
	if layer < len(handler.cachedDrawingEvents) {
		handler.cachedDrawingEvents[layer] = event
	}
}

func (handler *layerHandler) addLayer(event drawing.DrawingEvent, layer int) {
	if layer == len(handler.cachedDrawingEvents) || len(handler.cachedDrawingEvents) == 0 {
		handler.cachedDrawingEvents = append(handler.cachedDrawingEvents, event)
	} else if layer >= 0 {
		handler.cachedDrawingEvents = append(handler.cachedDrawingEvents[:layer+1], handler.cachedDrawingEvents[layer:]...)
		handler.cachedDrawingEvents[layer] = event
	}
}

func (handler *layerHandler) isSelected(id int) bool {
	for _, evt := range handler.selectedDrawingEvents {
		if evt.ID == id {
			return true
		}
	}

	return false
}

func (handler *layerHandler) selectLayer(event *LayerEvent, client *SocketClient) {
	handler.selectedDrawingEvents[client.auth.UserID] = event
}

func (handler *layerHandler) findLayerByID(id int) int {
	for i := 0; i < len(handler.cachedDrawingEvents); i++ {
		if handler.cachedDrawingEvents[i].ID == id {
			return i
		}
	}

	return -1
}

func (handler *layerHandler) moveLayer(layerMoveEvent *LayerMoveEvent, client *SocketClient) {
	defer handler.drawingLock.Unlock()
	handler.drawingLock.Lock()

	if layerMoveEvent.Layer >= 0 && layerMoveEvent.Layer < len(handler.cachedDrawingEvents) {
		currentLayer := handler.findLayerByID(layerMoveEvent.ID)
		if currentLayer == -1 {
			return
		}
		event := handler.cachedDrawingEvents[currentLayer]
		handler.deleteLayer(event, currentLayer)
		afterLayer := handler.findLayerByID(layerMoveEvent.After)
		handler.addLayer(event, afterLayer+1)
		layerMoveEvent.NewLayer = afterLayer + 1
		layerMoveEvent.Layer = currentLayer
		handler.clientMetricsHandler.onEditDrawing(client)
	}
}

func (handler *layerHandler) isInTimeout(clientId uint, cache map[uint]bool) bool {
	isInTimeout, ok := cache[clientId]
	return ok && isInTimeout
}

func (handler *layerHandler) handleNewLayerTextEvent(payload *LayerTextEventPayload, client *SocketClient) {
	handler.handleNewSyncEvent(payload, handler.cachedLayerTextEvents, handler.cachedWaitingSyncLayerText, client)
}

func (handler *layerHandler) handleNewLayerPreviewEvent(payload *LayerPreviewEventPayload, client *SocketClient) {
	handler.handleNewSyncEvent(payload, handler.cachedLayerPreviewEvents, handler.cachedWaitingSyncLayer, client)
}

func (handler *layerHandler) handleNewSyncEvent(payload LayerEventSynchronizer, cache map[uint]LayerEventSynchronizer, cacheWaitingSync map[uint]LayerEventSynchronizer, client *SocketClient) {
	defer handler.layerPreviewLock.Unlock()
	handler.layerPreviewLock.Lock()
	if payload.getLayer() >= 0 && payload.getLayer() < len(handler.cachedDrawingEvents) {
		if _, ok := cache[client.auth.UserID]; !ok {
			cache[client.auth.UserID] = payload
		}
		cachedLayerEvent := cache[client.auth.UserID]
		if handler.isInTimeout(client.auth.UserID, handler.clientsLayerPreviewSyncTimeout) {
			cachedSyncEvent, ok := cacheWaitingSync[client.auth.UserID]
			if ok {
				cachedSyncEvent.updateSync(payload)
				cachedLayerEvent.updateSync(payload)
			}
		} else {
			cachedLayerEvent.updateSync(payload)
			handler.clientsLayerPreviewSyncTimeout[client.auth.UserID] = true
			cacheWaitingSync[client.auth.UserID] = payload
			go handler.startClientLayerPreviewSyncRefresh(client, cacheWaitingSync)
		}
	}
}

func (handler *layerHandler) startClientLayerPreviewSyncRefresh(client *SocketClient, cacheWaitingSync map[uint]LayerEventSynchronizer) {
	time.Sleep(clientTimeoutBetweenLayerPreviewEventsMS * time.Millisecond)

	defer handler.layerPreviewLock.Unlock()
	handler.layerPreviewLock.Lock()

	defer handler.drawingLock.Unlock()
	handler.drawingLock.Lock()

	cachedSyncEvent, ok := cacheWaitingSync[client.auth.UserID]
	handler.clientsLayerPreviewSyncTimeout[client.auth.UserID] = false
	currentLayer := handler.findLayerByID(cachedSyncEvent.getID())
	if currentLayer == -1 {
		return
	}
	cachedSyncEvent.setLayer(currentLayer)
	if ok {
		cachedSyncEvent.synchronize(cachedSyncEvent, client)
	}
}
