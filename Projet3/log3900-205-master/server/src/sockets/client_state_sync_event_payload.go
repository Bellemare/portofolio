package sockets

type ClientSync struct {
	User   string `json:"user"`
	UserID uint   `json:"user_id"`
	Avatar string `json:"avatar"`
}

type ClientStateSyncEvent struct {
	Clients []ClientSync `json:"clients"`
}

type ClientStateSyncEventPayload struct {
	SocketPayload
	Payload ClientStateSyncEvent `json:"payload"`
}

func NewClientStateSyncEventPayload() *ClientStateSyncEventPayload {
	payload := &ClientStateSyncEventPayload{
		SocketPayload{
			Type: ClientStateSyncEventType,
		},
		ClientStateSyncEvent{
			[]ClientSync{},
		},
	}

	return payload
}

func (payload *ClientStateSyncEventPayload) HandleNewEvent(client *SocketClient) bool {
	for _, cl := range client.hub.clients {
		if user := client.hub.getCachedUser(cl.auth.UserID); user != nil {
			clientSync := ClientSync{
				User:   user.Pseudonym,
				UserID: user.ID,
				Avatar: user.Avatar,
			}
			payload.Payload.Clients = append(payload.Payload.Clients, clientSync)
		}
	}

	return payload.send(payload, client)
}
