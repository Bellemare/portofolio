package sockets

type StateType int

const (
	UndefinedStateType StateType = iota
	ConnectStateType
	DisconnectStateType
	ChangeStateType
)

type ClientStateEvent struct {
	State  StateType `json:"state"`
	User   string    `json:"user"`
	UserID uint      `json:"user_id"`
	Avatar string    `json:"avatar"`
}

type ClientStateEventPayload struct {
	SocketPayload
	Payload ClientStateEvent `json:"payload"`
}

func NewClientStateEventPayload(state StateType) *ClientStateEventPayload {
	return &ClientStateEventPayload{
		SocketPayload{
			Type: ClientStateEventType,
		},
		ClientStateEvent{
			State: state,
		},
	}
}

func (payload *ClientStateEventPayload) setEventUser(userId uint, client *SocketClient) {
	payload.Payload.UserID = userId
	if user := client.hub.getCachedUser(userId); user != nil {
		payload.Payload.User = user.Pseudonym
		if payload.Payload.State == ConnectStateType {
			payload.Payload.Avatar = user.Avatar
		}
	}
}

func (payload *ClientStateEventPayload) HandleNewEvent(client *SocketClient) bool {
	payload.setEventUser(client.auth.UserID, client)

	if payload.Payload.State == ConnectStateType {
		NewClientStateSyncEventPayload().HandleNewEvent(client)

		return payload.synchronize(payload, client)
	} else {
		return payload.broadcast(payload, client)
	}
}
