package sockets

type RefreshAlbumJoinRequestsPayload struct {
	SocketPayload
}

func NewRefreshAlbumJoinRequestsPayload() *RefreshAlbumJoinRequestsPayload {
	return &RefreshAlbumJoinRequestsPayload{
		SocketPayload{
			Type: RefreshAlbumJoinRequestsType,
		},
	}
}

func (payload *RefreshAlbumJoinRequestsPayload) HandleNewEvent(client *SocketClient) bool {
	return payload.synchronize(payload, client)
}
