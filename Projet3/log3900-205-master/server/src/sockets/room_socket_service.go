package sockets

import (
	"net/http"
	"sync"

	"github.com/gorilla/websocket"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dto"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/entities"
)

type hubMap map[uint]*SocketHub

var (
	lock          = &sync.Mutex{}
	roomHubs      hubMap
	drawingHubs   hubMap
	socketService *RoomSocketService
)

type RoomSocketService struct {
	*websocket.Upgrader

	AppHub *SocketHub
}

func NewRoomSocketService() *RoomSocketService {
	initInstance()

	return socketService
}

func UserIsConnected(userId uint) bool {
	client := getUserInAppHub(userId)
	if client == nil {
		client = getUserInHubsMap(userId, roomHubs)
	}
	if client == nil {
		client = getUserInHubsMap(userId, drawingHubs)
	}

	return client != nil
}

func getUserInAppHub(userId uint) *SocketClient {
	return NewRoomSocketService().AppHub.GetClient(userId)
}

func getUserInHubsMap(userId uint, hubs hubMap) *SocketClient {
	for _, hub := range hubs {
		if client := hub.GetClient(userId); client != nil {
			return client
		}
	}
	return nil
}

func initInstance() {
	lock.Lock()
	defer lock.Unlock()

	if socketService == nil {
		roomHubs = make(hubMap)
		drawingHubs = make(hubMap)

		upgrader := websocket.Upgrader{
			ReadBufferSize:  1024,
			WriteBufferSize: 1024,
		}
		upgrader.CheckOrigin = func(r *http.Request) bool {
			return true
		}

		socketService = &RoomSocketService{
			Upgrader: &upgrader,
			AppHub:   NewSocketHub(0, AppHub),
		}
		go socketService.AppHub.Run(false)
	}
}

func getHubsMapFromType(hubType HubType) hubMap {
	if hubType == RoomHub {
		return roomHubs
	}

	return drawingHubs
}

func (service *RoomSocketService) GetClientCollaborativeSession(userId uint) *dto.ContactCollaborationSessionDTO {
	if client := getUserInHubsMap(userId, drawingHubs); client != nil {
		collaborationSession, err := entities.NewDrawing().Dao.GetCollaborativeSessionInfo(client.hub.ID)
		if err != nil {
			return nil
		}
		return collaborationSession
	}

	return nil
}

func (service *RoomSocketService) GetDrawingClientCount(drawingId uint) int {
	if hub, ok := drawingHubs[drawingId]; ok {
		return hub.getNumClients()
	}

	return 0
}

func (service *RoomSocketService) ForceStopDrawingHub(drawingId uint, reason string) {
	if hub, ok := drawingHubs[drawingId]; ok {
		for _, client := range hub.clients {
			NewKickEventPayload(reason).HandleNewEvent(client)
		}
	}
}

func (service *RoomSocketService) HandleNewDrawingClient(drawingId uint, ctx *engine.Context) engine.Error {
	hub, ok := drawingHubs[drawingId]
	if ok && hub.getNumClients() == 4 {
		return engine.NewError("La salle est pleine")
	}

	service.handleNewClient(drawingId, DrawingHub, ctx)
	return nil
}

func (service *RoomSocketService) HandleNewRoomClient(roomId uint, ctx *engine.Context) {
	service.handleNewClient(roomId, RoomHub, ctx)
}

func (service *RoomSocketService) HandleNewAppClient(ctx *engine.Context) {
	conn, err := service.Upgrade(ctx.Writer, ctx.Request, nil)
	if err != nil {
		engine.PrintError("Error while connecting with client", err)
		return
	}

	client := NewSocketClient(conn, service.AppHub, ctx.Authentication)
	go client.Read()
	go client.Write()
}

func (service *RoomSocketService) handleNewClient(id uint, hubType HubType, ctx *engine.Context) {
	conn, err := service.Upgrade(ctx.Writer, ctx.Request, nil)
	if err != nil {
		engine.PrintError("Error while connecting with client", err)
		return
	}
	hubs := getHubsMapFromType(hubType)

	hub, ok := hubs[id]
	if !ok {
		hub = NewSocketHub(id, hubType)
		hubs[id] = hub
		go service.startNewHub(hub)
	}

	client := NewSocketClient(conn, hub, ctx.Authentication)
	go client.Read()
	go client.Write()
}

func (service *RoomSocketService) startNewHub(hub *SocketHub) {
	defer service.RemoveHub(hub)
	hub.Run(true)
}

func (service *RoomSocketService) RemoveHub(hub *SocketHub) {
	engine.PrintInfo("Cleaning up hub")
	hub.onDestroy()
	hubs := getHubsMapFromType(hub.Type)

	for id, h := range hubs {
		if h == hub {
			delete(hubs, id)
		}
	}
}
