package sockets

type KickPayload struct {
	Reason string `json:"reason"`
}

type KickEventPayload struct {
	SocketPayload
	Payload KickPayload `json:"payload"`
}

func NewKickEventPayload(reason string) *KickEventPayload {
	return &KickEventPayload{
		SocketPayload{
			Type: KickEventType,
		},
		KickPayload{
			Reason: reason,
		},
	}
}

func (payload *KickEventPayload) HandleNewEvent(client *SocketClient) bool {
	client.kick(payload)
	return true
}
