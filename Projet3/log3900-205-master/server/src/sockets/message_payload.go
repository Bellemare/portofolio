package sockets

import (
	"time"

	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/services"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/utils"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/validators"
)

type MessagePayload struct {
	SocketPayload
	Payload models.Message `json:"payload"`
}

var (
	validator   *engine.Validator
	userService = services.NewUserService()
)

func NewMessagePayload(payload []byte) *MessagePayload {
	var messagePayload MessagePayload
	utils.DecodeMessage(payload, &messagePayload)
	messagePayload.Payload.SentAt = time.Now()
	if validator == nil {
		validator = engine.NewValidator()
		validator.RegisterValidation("validLength", validators.IsValidMessageLength)
	}

	return &messagePayload
}

func (payload *MessagePayload) HandleNewEvent(client *SocketClient) bool {
	if err := validator.ValidateModel(payload.Payload); err != nil {
		return false
	}

	payload.Payload.UserID = client.auth.UserID
	if user := client.hub.getCachedUser(client.auth.UserID); user != nil {
		payload.Payload.User = user.Pseudonym
	}
	if id := client.hub.saveMessageToHistory(payload, client); id > 0 {
		payload.Payload.ID = id
	}

	return payload.broadcast(payload, client)
}
