package sockets

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/entities"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
)

type roomHubHandler struct {
	*hubHandler
}

var (
	room = entities.NewRoom()
)

func newRoomHubHandler(handler *hubHandler) *roomHubHandler {
	return &roomHubHandler{
		handler,
	}
}

func (handler *roomHubHandler) saveMessageToHistory(message *MessagePayload, client *SocketClient) uint {
	historyEntry := &models.RoomHistoryModel{
		RoomId:      client.hub.ID,
		UserID:      message.Payload.UserID,
		Message:     message.Payload.Message,
		SentAt:      message.Payload.SentAt,
		ReplyTo:     message.Payload.ReplyTo,
		MessageType: message.Payload.MessageType,
		Recipients:  message.Payload.Recipients,
	}
	err := room.RoomHistoryDao.AddHistory(historyEntry)
	if err != nil {
		return 0
	}

	return historyEntry.ID
}
