package sockets

import (
	"time"

	"github.com/gorilla/websocket"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/utils"
)

const (
	WRITE_WAIT       = 10 * time.Second
	PONG_WAIT        = 60 * time.Second
	PING_PERIOD      = (PONG_WAIT * 9) / 10
	MAX_MESSAGE_SIZE = 1048576 * 10
)

type SocketClient struct {
	hub  *SocketHub
	conn *websocket.Conn
	auth *engine.AuthTokenPayload

	send chan []byte
}

func NewSocketClient(conn *websocket.Conn, hub *SocketHub, auth *engine.AuthTokenPayload) *SocketClient {
	client := &SocketClient{
		hub:  hub,
		conn: conn,
		send: make(chan []byte, 256),
		auth: auth,
	}
	hub.register <- client

	return client
}

func (client *SocketClient) setReadDeadline(string) error {
	client.conn.SetReadDeadline(time.Now().Add(PONG_WAIT))
	return nil
}

func (client *SocketClient) setWriteDeadline() {
	client.conn.SetWriteDeadline(time.Now().Add(WRITE_WAIT))
}

func (client *SocketClient) Read() {
	defer client.closeConnection()
	client.conn.SetReadLimit(MAX_MESSAGE_SIZE)
	client.conn.SetPongHandler(client.setReadDeadline)
	for {
		_, message, err := client.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				engine.PrintInfo("Client connection lost", err)
				return
			}
			engine.PrintError("Error while reading for client", err)
			return
		}
		payload := NewPayload(message)
		if payload != nil {
			payload.HandleNewEvent(client)
		}
	}
}

func (client *SocketClient) Write() {
	ticker := time.NewTicker(PING_PERIOD)
	defer client.closeWriteConnection(ticker)
	for {
		select {
		case message, ok := <-client.send:
			client.sendHandler(message, ok)
		case <-ticker.C:
			if !client.verifyWriteConnection() {
				return
			}
		}
	}
}

func (client *SocketClient) sendHandler(message []byte, ok bool) bool {
	client.setWriteDeadline()
	if !ok {
		client.conn.WriteMessage(websocket.CloseMessage, []byte{})
		return false
	}

	w, err := client.conn.NextWriter(websocket.TextMessage)
	if err != nil {
		return false
	}
	w.Write(message)

	return w.Close() == nil
}

func (client *SocketClient) verifyWriteConnection() bool {
	client.setWriteDeadline()
	return client.conn.WriteMessage(websocket.PingMessage, nil) == nil
}

func (client *SocketClient) kick(kickEvent *KickEventPayload) {
	if data, err := utils.EncodeMessage(kickEvent); data != nil && err == nil {
		client.sendHandler(data, true)
	}
	client.closeConnection()
}

func (client *SocketClient) closeConnection() {
	client.hub.unregister <- client
	client.conn.Close()
}

func (client *SocketClient) closeWriteConnection(ticker *time.Ticker) {
	ticker.Stop()
	client.closeConnection()
}
