package sockets

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/utils"
)

type LayerTextEvent struct {
	UserID uint        `json:"user_id"`
	User   string      `json:"user"`
	Layer  int         `json:"layer"`
	ID     int         `json:"id"`
	Text   interface{} `json:"text"`
}

type LayerTextEventPayload struct {
	SocketPayload
	Payload LayerTextEvent `json:"payload"`
}

func NewLayerTextEventPayload(payload []byte) *LayerTextEventPayload {
	var layerEventPayload LayerTextEventPayload
	utils.DecodeMessage(payload, &layerEventPayload)

	return &layerEventPayload
}

func (payload *LayerTextEventPayload) HandleNewEvent(client *SocketClient) bool {
	payload.Payload.UserID = client.auth.UserID
	if user := client.hub.getCachedUser(client.auth.UserID); user != nil {
		payload.Payload.User = user.Pseudonym
	}
	if drawingHub, ok := client.hub.socketHubHandler.(*drawingHubHandler); ok {
		drawingHub.handleNewLayerTextEvent(payload, client)
	}

	return true
}

func (payload *LayerTextEventPayload) updateSync(newEvent LayerEventSynchronizer) {
	if event, ok := newEvent.(*LayerTextEventPayload); ok {
		payload.Payload.Text = event.Payload.Text
	}
}

func (payload *LayerTextEventPayload) getLayer() int {
	return payload.Payload.Layer
}

func (payload *LayerTextEventPayload) setLayer(layer int) {
	payload.Payload.Layer = layer
}

func (payload *LayerTextEventPayload) getID() int {
	return payload.Payload.ID
}
