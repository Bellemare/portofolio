package drawing

import (
	"bytes"
	"encoding/json"
)

type Vec2 struct {
	X float64 `json:"x"`
	Y float64 `json:"y"`
}

type DrawingPos struct {
	Data            []Vec2      `json:"data"`
	BoundingBox     BoundingBox `json:"bounding_box"`
	BaseBoundingBox BoundingBox `json:"base_bounding_box"`
}

func NewDrawingPos(data interface{}) *DrawingPos {
	var drawingPos DrawingPos
	buffer := new(bytes.Buffer)
	json.NewEncoder(buffer).Encode(data)
	json.NewDecoder(buffer).Decode(&drawingPos)

	return &drawingPos
}

func (drawingPos *DrawingPos) HandleNewEvent(newData DrawingElement) bool {
	if newData == nil {
		return true
	}

	data, ok := newData.(*DrawingPos)
	if ok {
		drawingPos.Data = append(drawingPos.Data, data.Data...)
		drawingPos.BoundingBox = data.BoundingBox
		drawingPos.BaseBoundingBox = data.BaseBoundingBox
	}

	return ok
}

func (drawingPos *DrawingPos) Clear() {
	drawingPos.Data = []Vec2{}
}

func (drawingPos *DrawingPos) Copy() DrawingElement {
	var newData []Vec2
	copy(newData, drawingPos.Data)

	return &DrawingPos{
		newData,
		drawingPos.BoundingBox,
		drawingPos.BaseBoundingBox,
	}
}

func (drawingPos *DrawingPos) GetBaseBoundingBox() BoundingBox {
	return *drawingPos.BaseBoundingBox.Copy()
}

func (drawingPos *DrawingPos) GetBoundingBox() BoundingBox {
	return *drawingPos.BoundingBox.Copy()
}

func (drawingPos *DrawingPos) IsEmpty() bool {
	return len(drawingPos.Data) <= 1
}
