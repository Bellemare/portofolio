package drawing

import (
	"bytes"
	"encoding/json"
	"math"
)

type DrawingEllipse struct {
	X               float64     `json:"x"`
	Y               float64     `json:"y"`
	RadiusX         float64     `json:"radius_x"`
	RadiusY         float64     `json:"radius_y"`
	BoundingBox     BoundingBox `json:"bounding_box"`
	BaseBoundingBox BoundingBox `json:"base_bounding_box"`
}

func NewDrawingEllipse(data interface{}) *DrawingEllipse {
	var drawingEllipse DrawingEllipse
	buffer := new(bytes.Buffer)
	json.NewEncoder(buffer).Encode(data)
	json.NewDecoder(buffer).Decode(&drawingEllipse)

	return &drawingEllipse
}

func (drawingEllipse *DrawingEllipse) HandleNewEvent(newData DrawingElement) bool {
	if newData == nil {
		return true
	}

	data, ok := newData.(*DrawingEllipse)
	if ok {
		drawingEllipse.X = data.X
		drawingEllipse.Y = data.Y
		drawingEllipse.RadiusX = data.RadiusX
		drawingEllipse.RadiusY = data.RadiusY
		drawingEllipse.BoundingBox = data.BoundingBox
		drawingEllipse.BaseBoundingBox = data.BaseBoundingBox
	}

	return ok
}

func (drawingEllipse *DrawingEllipse) Clear() {
	drawingEllipse.X = 0
	drawingEllipse.Y = 0
	drawingEllipse.RadiusX = 0
	drawingEllipse.RadiusY = 0
}

func (drawingEllipse *DrawingEllipse) Copy() DrawingElement {
	return NewDrawingEllipse(drawingEllipse)
}

func (drawingEllipse *DrawingEllipse) GetBaseBoundingBox() BoundingBox {
	return *drawingEllipse.BaseBoundingBox.Copy()
}

func (drawingEllipse *DrawingEllipse) GetBoundingBox() BoundingBox {
	return *drawingEllipse.BoundingBox.Copy()
}

func (drawingEllipse *DrawingEllipse) IsEmpty() bool {
	return math.Abs(drawingEllipse.RadiusX) <= 0.5 || math.Abs(drawingEllipse.RadiusY) <= 0.5
}
