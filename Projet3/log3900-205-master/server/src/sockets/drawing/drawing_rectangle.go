package drawing

import (
	"bytes"
	"encoding/json"
	"math"
)

type DrawingRectangle struct {
	X               float64     `json:"x"`
	Y               float64     `json:"y"`
	Width           float64     `json:"width"`
	Height          float64     `json:"height"`
	BoundingBox     BoundingBox `json:"bounding_box"`
	BaseBoundingBox BoundingBox `json:"base_bounding_box"`
}

func NewDrawingRectangle(data interface{}) *DrawingRectangle {
	var drawingRectangle DrawingRectangle
	buffer := new(bytes.Buffer)
	json.NewEncoder(buffer).Encode(data)
	json.NewDecoder(buffer).Decode(&drawingRectangle)

	return &drawingRectangle
}

func (drawingRectangle *DrawingRectangle) HandleNewEvent(newData DrawingElement) bool {
	if newData == nil {
		return true
	}

	data, ok := newData.(*DrawingRectangle)
	if ok {
		drawingRectangle.X = data.X
		drawingRectangle.Y = data.Y
		drawingRectangle.Width = data.Width
		drawingRectangle.Height = data.Height
		drawingRectangle.BoundingBox = data.BoundingBox
		drawingRectangle.BaseBoundingBox = data.BaseBoundingBox
	}

	return ok
}

func (drawingRectangle *DrawingRectangle) Clear() {
	drawingRectangle.X = 0
	drawingRectangle.Y = 0
	drawingRectangle.Width = 0
	drawingRectangle.Height = 0
}

func (drawingRectangle *DrawingRectangle) Copy() DrawingElement {
	return NewDrawingRectangle(drawingRectangle)
}

func (drawingRectangle *DrawingRectangle) GetBaseBoundingBox() BoundingBox {
	return *drawingRectangle.BaseBoundingBox.Copy()
}

func (drawingRectangle *DrawingRectangle) GetBoundingBox() BoundingBox {
	return *drawingRectangle.BoundingBox.Copy()
}

func (drawingRectangle *DrawingRectangle) IsEmpty() bool {
	return math.Abs(drawingRectangle.Width) <= 0.5 || math.Abs(drawingRectangle.Height) <= 0.5
}
