package drawing

type ToolType uint

const (
	UndefinedToolType ToolType = iota
	PencilToolType
	EllipseToolType
	RectangleToolType
)

type BoundingBox struct {
	X float64 `json:"x"`
	Y float64 `json:"y"`
	W float64 `json:"w"`
	H float64 `json:"h"`
}

type DrawingElement interface {
	HandleNewEvent(DrawingElement) bool
	Clear()
	Copy() DrawingElement
	GetBaseBoundingBox() BoundingBox
	GetBoundingBox() BoundingBox
	IsEmpty() bool
}

type DrawingEvent struct {
	ID          int         `json:"id"`
	Type        uint        `json:"type"`
	Data        interface{} `json:"data"`
	ToolType    ToolType    `json:"tool_type"`
	Attributes  interface{} `json:"attributes"`
	StrokeColor interface{} `json:"stroke_color"`
	FillColor   interface{} `json:"fill_color"`
	TextColor   interface{} `json:"text_color"`
	DrawingType uint        `json:"drawing_type"`
}

func (event *DrawingEvent) NewDrawingElement() DrawingElement {
	switch event.ToolType {
	case PencilToolType:
		return NewDrawingPos(event.Data)
	case RectangleToolType:
		return NewDrawingRectangle(event.Data)
	case EllipseToolType:
		return NewDrawingEllipse(event.Data)
	default:
		return nil
	}
}

func (event *DrawingEvent) Copy() *DrawingEvent {
	eventCopy := &DrawingEvent{
		Type:        event.Type,
		ToolType:    event.ToolType,
		Attributes:  event.Attributes,
		StrokeColor: event.StrokeColor,
		FillColor:   event.FillColor,
		TextColor:   event.TextColor,
		DrawingType: event.DrawingType,
	}
	if drawingElement, ok := event.Data.(DrawingElement); ok {
		event.Data = drawingElement.Copy()
	} else {
		eventCopy.Data = event.Data
	}

	return eventCopy
}

func (box *BoundingBox) Copy() *BoundingBox {
	return &BoundingBox{
		X: box.X,
		Y: box.Y,
		W: box.W,
		H: box.H,
	}
}
