package sockets

import (
	"sync"
	"time"

	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/sockets/drawing"
)

const (
	clientTimeoutBetweenDrawingEventsMS = 20
)

type previewHandler struct {
	cachedPreviewEvents     map[uint]*DrawingPreviewEventPayload
	cachedWaitingSyncEvents map[uint]*DrawingPreviewEventPayload

	clientsPreviewSyncTimeout map[uint]bool
	previewLock               *sync.Mutex
}

func newPreviewHandler() *previewHandler {
	hub := &previewHandler{
		make(map[uint]*DrawingPreviewEventPayload),
		make(map[uint]*DrawingPreviewEventPayload),
		make(map[uint]bool),
		&sync.Mutex{},
	}

	return hub
}

func (handler *previewHandler) isInTimeout(clientId uint, cache map[uint]bool) bool {
	isInTimeout, ok := cache[clientId]
	return ok && isInTimeout
}

func (handler *previewHandler) handleNewPreviewDrawingEvent(payload *DrawingPreviewEventPayload, client *SocketClient) {
	defer handler.previewLock.Unlock()
	handler.previewLock.Lock()
	if drawingElement := payload.Payload.Event.NewDrawingElement(); drawingElement != nil {
		cachedUserEvent, ok := handler.cachedPreviewEvents[client.auth.UserID]
		if ok {
			cachedDrawingElement := cachedUserEvent.Payload.Event.Data.(drawing.DrawingElement)
			cachedDrawingElement.HandleNewEvent(drawingElement)
		} else {
			cachedPayload := payload.Copy()
			cachedPayload.Payload.Event.Data = drawingElement.Copy()
			handler.cachedPreviewEvents[client.auth.UserID] = cachedPayload
		}

		if handler.isInTimeout(client.auth.UserID, handler.clientsPreviewSyncTimeout) {
			handler.handleTimeoutSyncClientPreviewEvent(drawingElement, client)
		} else {
			handler.handleSyncClientPreviewEvent(payload, drawingElement, client)
		}
	}
}

func (handler *previewHandler) handleTimeoutSyncClientPreviewEvent(drawingElement drawing.DrawingElement, client *SocketClient) {
	cachedSyncEvent, ok := handler.cachedWaitingSyncEvents[client.auth.UserID]
	if ok {
		if cachedDrawingElement, ok := cachedSyncEvent.Payload.Event.Data.(drawing.DrawingElement); ok {
			cachedDrawingElement.HandleNewEvent(drawingElement)
		}
	}
}

func (handler *previewHandler) handleSyncClientPreviewEvent(payload *DrawingPreviewEventPayload, drawingElement drawing.DrawingElement, client *SocketClient) {
	handler.clientsPreviewSyncTimeout[client.auth.UserID] = true
	cachedSyncEvent, isSet := handler.cachedWaitingSyncEvents[client.auth.UserID]
	if isSet {
		if cachedDrawingElement, ok := cachedSyncEvent.Payload.Event.Data.(drawing.DrawingElement); ok {
			cachedDrawingElement.Clear()
			cachedDrawingElement.HandleNewEvent(drawingElement)
		}
	} else {
		payload.Payload.Event.Data = drawingElement
		handler.cachedWaitingSyncEvents[client.auth.UserID] = payload
	}
	go handler.startClientPreviewSyncRefresh(client)
}

func (handler *previewHandler) startClientPreviewSyncRefresh(client *SocketClient) {
	time.Sleep(clientTimeoutBetweenDrawingEventsMS * time.Millisecond)

	defer handler.previewLock.Unlock()
	handler.previewLock.Lock()

	cachedSyncEvent, ok := handler.cachedWaitingSyncEvents[client.auth.UserID]
	if ok {
		cachedSyncEvent.synchronize(cachedSyncEvent, client)
	}
	handler.clientsPreviewSyncTimeout[client.auth.UserID] = false
}
