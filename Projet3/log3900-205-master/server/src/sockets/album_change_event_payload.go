package sockets

type AlbumChangeEventPayload struct {
	SocketPayload
}

func NewAlbumChangeEventPayload() *AlbumChangeEventPayload {
	return &AlbumChangeEventPayload{
		SocketPayload{
			Type: AlbumChangeEventType,
		},
	}
}

func (payload *AlbumChangeEventPayload) HandleNewEvent(client *SocketClient) bool {
	return payload.synchronize(payload, client)
}
