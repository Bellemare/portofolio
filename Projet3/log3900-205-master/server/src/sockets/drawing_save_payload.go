package sockets

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/utils"
)

type DrawingSave struct {
	Image string `json:"image"`
}

type DrawingSavePayload struct {
	SocketPayload
	Payload DrawingSave `json:"payload"`
}

func NewDrawingSavePayload(payload []byte) *DrawingSavePayload {
	var drawingSavePayload DrawingSavePayload
	utils.DecodeMessage(payload, &drawingSavePayload)

	return &drawingSavePayload
}

func (payload *DrawingSavePayload) HandleNewEvent(client *SocketClient) bool {
	if payload.Payload.Image != "" {
		if handler, ok := client.hub.socketHubHandler.(*drawingHubHandler); ok {
			handler.saveImageToFile(payload.Payload.Image)
			return true
		}
	}
	return false
}
