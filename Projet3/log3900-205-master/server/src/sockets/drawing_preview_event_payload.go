package sockets

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/utils"
)

type DrawingPreviewEventPayload struct {
	SocketPayload
	Payload SocketDrawingEvent `json:"payload"`
}

func NewDrawingPreviewEventPayload(payload []byte) *DrawingPreviewEventPayload {
	var drawingEventPayload DrawingPreviewEventPayload
	utils.DecodeMessage(payload, &drawingEventPayload)

	return &drawingEventPayload
}

func (payload *DrawingPreviewEventPayload) HandleNewEvent(client *SocketClient) bool {
	payload.Payload.UserID = client.auth.UserID
	if user := client.hub.getCachedUser(client.auth.UserID); user != nil {
		payload.Payload.User = user.Pseudonym
	}
	if drawingHub, ok := client.hub.socketHubHandler.(*drawingHubHandler); ok {
		drawingHub.handleNewPreviewDrawingEvent(payload, client)
	}

	return true
}

func (payload *DrawingPreviewEventPayload) Copy() *DrawingPreviewEventPayload {
	return &DrawingPreviewEventPayload{
		payload.SocketPayload,
		SocketDrawingEvent{
			payload.Payload.UserID,
			payload.Payload.User,
			*payload.Payload.Event.Copy(),
		},
	}
}
