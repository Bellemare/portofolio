package sockets

type DrawingChangeEventPayload struct {
	SocketPayload
}

func NewDrawingChangeEventPayload() *DrawingChangeEventPayload {
	return &DrawingChangeEventPayload{
		SocketPayload{
			Type: DrawingChangeEventType,
		},
	}
}

func (payload *DrawingChangeEventPayload) HandleNewEvent(client *SocketClient) bool {
	return payload.synchronize(payload, client)
}
