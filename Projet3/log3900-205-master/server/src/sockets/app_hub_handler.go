package sockets

type appHubHandler struct {
	*hubHandler
}

func newAppHubHandler(handler *hubHandler) *appHubHandler {
	return &appHubHandler{
		handler,
	}
}

func (handler *appHubHandler) onNewClient(client *SocketClient) {
	NewClientStateEventPayload(ConnectStateType).HandleNewEvent(client)
}

func (handler *appHubHandler) onClientLeave(client *SocketClient) {
	NewClientStateEventPayload(DisconnectStateType).HandleNewEvent(client)
}
