package sockets

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/sockets/drawing"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/utils"
)

type LayerActionType int

const (
	UndefinedLayerEvent LayerActionType = iota
	LayerChangeEvent
	LayerDeleteEvent
	LayerAddEvent
	LayerSelectEvent
)

type LayerEvent struct {
	UserID uint                 `json:"user_id"`
	User   string               `json:"user"`
	ID     int                  `json:"id"`
	Layer  int                  `json:"layer"`
	Type   LayerActionType      `json:"type"`
	Event  drawing.DrawingEvent `json:"event"`
}

type LayerEventPayload struct {
	SocketPayload
	Payload LayerEvent `json:"payload"`
}

func NewLayerEventPayload(payload []byte) *LayerEventPayload {
	var layerEventPayload LayerEventPayload
	utils.DecodeMessage(payload, &layerEventPayload)

	return &layerEventPayload
}

func (payload *LayerEventPayload) HandleNewEvent(client *SocketClient) bool {
	payload.Payload.UserID = client.auth.UserID
	if user := client.hub.getCachedUser(client.auth.UserID); user != nil {
		payload.Payload.User = user.Pseudonym
	}
	if drawingHub, ok := client.hub.socketHubHandler.(*drawingHubHandler); ok {
		if !drawingHub.handleLayerEvent(&payload.Payload, client) {
			return false
		}
	}

	return payload.broadcast(payload, client)
}
