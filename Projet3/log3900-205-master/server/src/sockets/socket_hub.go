package sockets

type SynchronizationRequest struct {
	data   []byte
	origin *SocketClient
}

type HubType uint

const (
	RoomHub HubType = iota
	DrawingHub
	AppHub
)

type SocketHub struct {
	ID   uint
	Type HubType

	socketHubHandler

	clients []*SocketClient

	broadcast   chan []byte
	synchronize chan *SynchronizationRequest
	register    chan *SocketClient
	unregister  chan *SocketClient
}

func NewSocketHub(id uint, hubType HubType) *SocketHub {
	hub := &SocketHub{
		clients:     []*SocketClient{},
		ID:          id,
		Type:        hubType,
		broadcast:   make(chan []byte),
		synchronize: make(chan *SynchronizationRequest),
		register:    make(chan *SocketClient),
		unregister:  make(chan *SocketClient),
	}
	hub.socketHubHandler = newHubHandler(hub)
	return hub
}

func (hub *SocketHub) Run(autoDestroy bool) {
	for {
		select {
		case client := <-hub.register:
			hub.addClient(client)
			go hub.onNewClient(client)
		case client := <-hub.unregister:
			if hub.removeClient(client) {
				if len(hub.clients) > 0 || !autoDestroy {
					go hub.onClientLeave(client)
				} else {
					hub.onClientLeave(client)
				}
			}
			if autoDestroy && len(hub.clients) == 0 {
				return
			}
		case data := <-hub.broadcast:
			request := SynchronizationRequest{
				data:   data,
				origin: nil,
			}
			hub.handleBroadcast(&request)
		case request := <-hub.synchronize:
			hub.handleBroadcast(request)
		}
	}
}
