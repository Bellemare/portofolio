package sockets

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/services"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/sockets/drawing"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/utils"
)

const (
	drawingEventsCacheFolder = "/var/www/drawingsCache"
)

type drawingHubHandler struct {
	*hubHandler
	*clientMetricsHandler
	*layerHandler
	*previewHandler
	drawingService *services.DrawingService

	messagesHistory []*models.Message
}

func newDrawingHubHandler(handler *hubHandler) *drawingHubHandler {
	clientMetricsHandler := newClientMetricsHandler(handler.hub.ID)
	hub := &drawingHubHandler{
		handler,
		clientMetricsHandler,
		newLayerHandler(clientMetricsHandler),
		newPreviewHandler(),
		services.NewDrawingService(),
		[]*models.Message{},
	}
	hub.retrieveCachedEvents()
	hub.retrieveRoomHistry()

	return hub
}

func (handler *drawingHubHandler) retrieveCachedEvents() {
	if data, err := os.ReadFile(fmt.Sprintf("%s/%d.json", drawingEventsCacheFolder, handler.hub.ID)); err == nil {
		var events []drawing.DrawingEvent
		utils.DecodeMessage(data, &events)
		handler.cachedDrawingEvents = events
		for _, evt := range events {
			if handler.maxID < evt.ID {
				handler.maxID = evt.ID
			}
		}
	}
}

func (handler *drawingHubHandler) onDestroy() {
	if json, err := json.Marshal(&handler.cachedDrawingEvents); err == nil {
		ioutil.WriteFile(fmt.Sprintf("%s/%d.json", drawingEventsCacheFolder, handler.hub.ID), json, os.ModePerm)
	}
}

func (handler *drawingHubHandler) deleteCachedPreviewEvents(client *SocketClient) {
	delete(handler.cachedWaitingSyncEvents, client.auth.UserID)
	delete(handler.cachedPreviewEvents, client.auth.UserID)
	if layerEvent, ok := handler.selectedDrawingEvents[client.auth.UserID]; ok {
		layer := handler.findLayerByID(layerEvent.ID)
		if layer > 0 {
			event := handler.cachedDrawingEvents[layer]
			changeEvent := &LayerEventPayload{
				SocketPayload{
					Type: LayerEventType,
				},
				LayerEvent{
					ID:    event.ID,
					Layer: layerEvent.Layer,
					Type:  LayerChangeEvent,
					Event: event,
				},
			}
			changeEvent.HandleNewEvent(client)
		}
		delete(handler.cachedLayerPreviewEvents, client.auth.UserID)
		delete(handler.selectedDrawingEvents, client.auth.UserID)
	}
}

func (handler *drawingHubHandler) onNewClient(client *SocketClient) {
	handler.clientMetricsHandler.onNewClient(client)
	NewMessageHistoryPayload(handler.messagesHistory).HandleNewEvent(client)
	NewClientStateEventPayload(ConnectStateType).HandleNewEvent(client)

	if appClient := socketService.AppHub.GetClient(client.auth.UserID); appClient != nil {
		NewDrawingChangeEventPayload().HandleNewEvent(appClient)
		NewClientStateEventPayload(ChangeStateType).HandleNewEvent(appClient)
	}

	NewDrawingHistoryPayload(handler.cachedDrawingEvents).HandleNewEvent(client)
	for _, cachedPreviewEvent := range handler.cachedPreviewEvents {
		cachedPreviewEvent.send(cachedPreviewEvent, client)
	}
	for _, cachedLayerPreview := range handler.cachedLayerPreviewEvents {
		cachedLayerPreview.send(cachedLayerPreview, client)
	}
}

func (handler *drawingHubHandler) onClientLeave(client *SocketClient) {
	handler.clientMetricsHandler.onClientLeave(client)
	if len(client.hub.clients) > 0 {
		if c := handler.GetClient(client.auth.UserID); c == nil {
			NewClientStateEventPayload(DisconnectStateType).HandleNewEvent(client)
		}
		handler.deleteCachedPreviewEvents(client)
	}
	if appClient := socketService.AppHub.GetClient(client.auth.UserID); appClient != nil {
		NewDrawingChangeEventPayload().HandleNewEvent(appClient)
		NewClientStateEventPayload(ChangeStateType).HandleNewEvent(appClient)
	}
}

func (handler *drawingHubHandler) retrieveRoomHistry() {
	if messages, err := room.RoomHistoryDao.GetDrawingRoomHistory(handler.drawingId); err == nil {
		handler.messagesHistory = messages
	}
}

func (handler *drawingHubHandler) saveMessageToHistory(message *MessagePayload, client *SocketClient) uint {
	handler.messagesHistory = append(handler.messagesHistory, &message.Payload)

	historyEntry := &models.DrawingRoomHistoryModel{
		RoomHistoryModel: models.RoomHistoryModel{
			RoomId:      handler.drawingId,
			UserID:      message.Payload.UserID,
			Message:     message.Payload.Message,
			SentAt:      message.Payload.SentAt,
			ReplyTo:     message.Payload.ReplyTo,
			MessageType: message.Payload.MessageType,
			Recipients:  message.Payload.Recipients,
		},
	}
	err := room.RoomHistoryDao.AddDrawingHistory(historyEntry)
	if err != nil {
		return 0
	}

	return historyEntry.ID
}

func (handler *drawingHubHandler) saveImageToFile(base64Image string) {
	data, err := utils.Base64ImageDecode(base64Image)
	if err != nil {
		return
	}

	mimeType := http.DetectContentType(data)
	if ok := handler.drawingService.CreateDrawingsFolder(handler.hub.ID); !ok {
		engine.PrintError("Error creating drawing's images folder", err)
		return
	}
	count := handler.drawingService.GetDrawingImagesCount(handler.hub.ID)
	fileName, err := handler.drawingService.GetDrawingImageFileName(handler.hub.ID, count+1, mimeType)
	if err != nil {
		engine.PrintError(err.Error())
		return
	}
	file, _ := os.Create(fileName)
	if err != nil {
		engine.PrintError("Error creating drawing image file", err)
	} else {
		if _, err := file.Write(data); err != nil {
			engine.PrintError("Error saving drawing image", err)
		}
		file.Close()
	}
}
