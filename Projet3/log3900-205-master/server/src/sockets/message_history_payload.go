package sockets

import "gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"

type MessageHistory struct {
	Messages []*models.Message `json:"messages"`
}

type MessageHistoryPayload struct {
	SocketPayload
	Payload MessageHistory `json:"payload"`
}

func NewMessageHistoryPayload(payload []*models.Message) *MessageHistoryPayload {
	return &MessageHistoryPayload{
		SocketPayload{
			Type: MessageHistoryType,
		},
		MessageHistory{payload},
	}
}

func (payload *MessageHistoryPayload) HandleNewEvent(client *SocketClient) bool {
	return payload.send(payload, client)
}
