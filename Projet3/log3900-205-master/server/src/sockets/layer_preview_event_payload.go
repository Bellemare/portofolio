package sockets

import (
	"bytes"
	"encoding/json"

	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/utils"
)

type Transform struct {
	Dx float64 `json:"dx"`
	Dy float64 `json:"dy"`
	Dw float64 `json:"dw"`
	Dh float64 `json:"dh"`

	FlipX bool `json:"flip_x"`
	FlipY bool `json:"flip_y"`
}

type LayerPreviewEvent struct {
	UserID      uint        `json:"user_id"`
	User        string      `json:"user"`
	Layer       int         `json:"layer"`
	ID          int         `json:"id"`
	Transform   Transform   `json:"transform"`
	Attributes  interface{} `json:"attributes"`
	StrokeColor interface{} `json:"stroke_color"`
	FillColor   interface{} `json:"fill_color"`
	TextColor   interface{} `json:"text_color"`
}

type LayerPreviewEventPayload struct {
	SocketPayload
	Payload LayerPreviewEvent `json:"payload"`
}

func NewLayerPreviewEventPayload(payload []byte) *LayerPreviewEventPayload {
	var layerEventPayload LayerPreviewEventPayload
	utils.DecodeMessage(payload, &layerEventPayload)

	return &layerEventPayload
}

func (payload *LayerPreviewEventPayload) HandleNewEvent(client *SocketClient) bool {
	payload.Payload.UserID = client.auth.UserID
	if user := client.hub.getCachedUser(client.auth.UserID); user != nil {
		payload.Payload.User = user.Pseudonym
	}
	if drawingHub, ok := client.hub.socketHubHandler.(*drawingHubHandler); ok {
		drawingHub.handleNewLayerPreviewEvent(payload, client)
	}

	return true
}

func (transform *Transform) Copy() *Transform {
	return &Transform{
		transform.Dx,
		transform.Dy,
		transform.Dw,
		transform.Dh,
		transform.FlipX,
		transform.FlipY,
	}
}

func (payload *LayerPreviewEventPayload) Copy() *LayerPreviewEventPayload {
	var layerPreview LayerPreviewEvent
	buffer := new(bytes.Buffer)
	json.NewEncoder(buffer).Encode(payload.Payload)
	json.NewDecoder(buffer).Decode(&layerPreview)
	layerPreview.Transform = *payload.Payload.Transform.Copy()

	return &LayerPreviewEventPayload{
		payload.SocketPayload,
		layerPreview,
	}
}

func (payload *LayerPreviewEventPayload) updateSync(event LayerEventSynchronizer) {
	if newEvent, ok := event.(*LayerPreviewEventPayload); ok {
		newTransform := Transform{
			Dx:    newEvent.Payload.Transform.Dx,
			Dy:    newEvent.Payload.Transform.Dy,
			Dw:    newEvent.Payload.Transform.Dw,
			Dh:    newEvent.Payload.Transform.Dh,
			FlipX: payload.Payload.Transform.FlipX,
			FlipY: payload.Payload.Transform.FlipY,
		}
		if newEvent.Payload.Transform.FlipX {
			newTransform.FlipX = !newTransform.FlipX
		}
		if newEvent.Payload.Transform.FlipY {
			newTransform.FlipY = !newTransform.FlipY
		}
		payload.Payload.Transform = newTransform
		payload.Payload.Attributes = newEvent.Payload.Attributes
		payload.Payload.FillColor = newEvent.Payload.FillColor
		payload.Payload.StrokeColor = newEvent.Payload.StrokeColor
		payload.Payload.TextColor = newEvent.Payload.TextColor
	}
}

func (payload *LayerPreviewEventPayload) getLayer() int {
	return payload.Payload.Layer
}

func (payload *LayerPreviewEventPayload) setLayer(layer int) {
	payload.Payload.Layer = layer
}

func (payload *LayerPreviewEventPayload) getID() int {
	return payload.Payload.ID
}
