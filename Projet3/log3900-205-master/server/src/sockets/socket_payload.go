package sockets

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/utils"
)

type PayloadType int

const (
	Undefined PayloadType = iota
	MessageType
	DrawingEventType
	MessageHistoryType
	DrawingHistoryType
	RefreshAlbumJoinRequestsType
	NotificationType
	AlbumChangeEventType
	DrawingChangeEventType
	ClientStateEventType
	DrawingSaveType
	DrawingPreviewEventType
	LayerEventType
	LayerPreviewEventType
	LayerMoveEventType
	KickEventType
	LayerTextEventType
	ClientStateSyncEventType
)

type SocketPayload struct {
	Type    PayloadType `json:"type"`
	Payload interface{} `json:"payload"`
}

type ISocketEvent interface {
	HandleNewEvent(*SocketClient) bool
}

func NewPayload(payload []byte) ISocketEvent {
	var socketPayload SocketPayload
	err := utils.DecodeMessage(payload, &socketPayload)
	if err != nil {
		return nil
	}

	switch socketPayload.Type {
	case MessageType:
		return NewMessagePayload(payload)
	case DrawingEventType:
		return NewDrawingEventPayload(payload)
	case DrawingPreviewEventType:
		return NewDrawingPreviewEventPayload(payload)
	case DrawingSaveType:
		return NewDrawingSavePayload(payload)
	case LayerEventType:
		return NewLayerEventPayload(payload)
	case LayerPreviewEventType:
		return NewLayerPreviewEventPayload(payload)
	case LayerMoveEventType:
		return NewLayerMoveEventPayload(payload)
	case LayerTextEventType:
		return NewLayerTextEventPayload(payload)
	default:
		return nil
	}
}

func (payload *SocketPayload) send(payloadData interface{}, client *SocketClient) bool {
	if data, err := utils.EncodeMessage(payloadData); data != nil && err == nil {
		client.send <- data
		return true
	}

	return false
}

func (payload *SocketPayload) broadcast(payloadData interface{}, client *SocketClient) bool {
	if data, err := utils.EncodeMessage(payloadData); data != nil && err == nil {
		client.hub.broadcast <- data
		return true
	}

	return false
}

func (payload *SocketPayload) synchronize(payloadData interface{}, client *SocketClient) bool {
	if data, err := utils.EncodeMessage(payloadData); err == nil {
		client.hub.synchronize <- &SynchronizationRequest{
			data:   data,
			origin: client,
		}
		return true
	}

	return false
}
