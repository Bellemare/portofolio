package sockets

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/sockets/drawing"
)

type DrawingHistory struct {
	Events []drawing.DrawingEvent `json:"events"`
}

type DrawingHistoryPayload struct {
	SocketPayload
	Payload DrawingHistory `json:"payload"`
}

func NewDrawingHistoryPayload(events []drawing.DrawingEvent) *DrawingHistoryPayload {
	return &DrawingHistoryPayload{
		SocketPayload{
			Type: DrawingHistoryType,
		},
		DrawingHistory{
			events,
		},
	}
}

func (payload *DrawingHistoryPayload) HandleNewEvent(client *SocketClient) bool {
	return payload.send(payload, client)
}
