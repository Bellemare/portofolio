package sockets

import (
	"sync"
	"time"

	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/services"
)

type clientMetricsHandler struct {
	clientCollaborationSession map[uint]*models.CollaborationSessionModel

	drawingId uint

	lock *sync.Mutex
}

func newClientMetricsHandler(drawingId uint) *clientMetricsHandler {
	handler := &clientMetricsHandler{
		make(map[uint]*models.CollaborationSessionModel),
		drawingId,
		&sync.Mutex{},
	}

	return handler
}

func (handler *clientMetricsHandler) onNewClient(client *SocketClient) {
	defer handler.lock.Unlock()
	handler.lock.Lock()

	handler.clientCollaborationSession[client.auth.UserID] = &models.CollaborationSessionModel{
		UserID:    client.auth.UserID,
		StartTime: time.Now(),
		DrawingID: handler.drawingId,
	}
}

func (handler *clientMetricsHandler) onEditDrawing(client *SocketClient) {
	defer handler.lock.Unlock()
	handler.lock.Lock()

	if session, ok := handler.clientCollaborationSession[client.auth.UserID]; ok {
		session.EditedDrawing = true
	}
}

func (handler *clientMetricsHandler) onClientLeave(client *SocketClient) {
	defer handler.lock.Unlock()
	handler.lock.Lock()

	if session, ok := handler.clientCollaborationSession[client.auth.UserID]; ok {
		session.EndTime = time.Now()
		service := services.NewCollaborationSessionService()
		service.Create(session)
		delete(handler.clientCollaborationSession, client.auth.UserID)
	}
}
