package sockets

type Notification struct {
	Message string `json:"message"`
}

type NotificationPayload struct {
	SocketPayload
	Payload Notification `json:"payload"`
}

func NewNotificationPayload(notification string) *NotificationPayload {
	return &NotificationPayload{
		SocketPayload{
			Type: NotificationType,
		},
		Notification{
			Message: notification,
		},
	}
}

func (payload *NotificationPayload) HandleNewEvent(client *SocketClient) bool {
	return payload.send(payload, client)
}
