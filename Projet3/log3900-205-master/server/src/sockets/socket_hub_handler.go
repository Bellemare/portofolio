package sockets

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dto"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/services"
)

type socketHubHandler interface {
	GetClient(uint) *SocketClient
	getNumClients() int
	onDestroy()
	addClient(client *SocketClient)
	onNewClient(client *SocketClient)
	removeClient(client *SocketClient) bool
	onClientLeave(client *SocketClient)
	getCachedUser(userId uint) *dto.UserDTO
	handleBroadcast(request *SynchronizationRequest)
	saveMessageToHistory(*MessagePayload, *SocketClient) uint
}

type hubHandler struct {
	hub *SocketHub

	userCache   map[uint]dto.UserDTO
	userService *services.UserService
}

func newHubHandler(hub *SocketHub) socketHubHandler {
	handler := &hubHandler{
		hub:         hub,
		userCache:   make(map[uint]dto.UserDTO),
		userService: services.NewUserService(),
	}

	switch hub.Type {
	case RoomHub:
		return newRoomHubHandler(handler)
	case DrawingHub:
		return newDrawingHubHandler(handler)
	case AppHub:
		return newAppHubHandler(handler)
	}

	return nil
}

func (handler *hubHandler) GetClient(userId uint) *SocketClient {
	for _, client := range handler.hub.clients {
		if client.auth.UserID == userId {
			return client
		}
	}

	return nil
}

func (handler *hubHandler) getNumClients() int {
	num := 0
	parsedClients := []uint{}
	for _, client := range handler.hub.clients {
		isAlreadyParsed := false
		for _, parsedClient := range parsedClients {
			if parsedClient == client.auth.UserID {
				isAlreadyParsed = true
				break
			}
		}
		if !isAlreadyParsed {
			num++
			parsedClients = append(parsedClients, client.auth.UserID)
		}
	}

	return num
}

func (handler *hubHandler) onDestroy() {}

func (handler *hubHandler) getCachedUser(userId uint) *dto.UserDTO {
	if user, ok := handler.userCache[userId]; ok {
		return &user
	}

	return nil
}

func (handler *hubHandler) addClient(client *SocketClient) {
	isInHubAlready := handler.GetClient(client.auth.UserID) != nil
	handler.hub.clients = append(handler.hub.clients, client)

	if !isInHubAlready {
		if user, err := userService.GetUser(client.auth.UserID); err == nil {
			var userDTO dto.UserDTO
			engine.ParseModelToDTO(&user, &userDTO)
			userDTO.Avatar = userService.GetUserAvatar(user.ID)
			handler.userCache[user.ID] = userDTO
		}
	}
}

func (handler *hubHandler) removeClient(client *SocketClient) bool {
	for i, c := range handler.hub.clients {
		if c == client {
			handler.hub.clients[i] = handler.hub.clients[len(handler.hub.clients)-1]
			clients := handler.hub.clients[:len(handler.hub.clients)-1]
			if cachedClient := handler.GetClient(client.auth.UserID); cachedClient == nil {
				delete(handler.userCache, client.auth.UserID)
			}

			handler.hub.clients = clients
			return true
		}
	}
	return false
}

func (handler *hubHandler) handleBroadcast(request *SynchronizationRequest) {
	for _, client := range handler.hub.clients {
		if request.origin != nil && client == request.origin {
			continue
		}

		select {
		case client.send <- request.data:
		default:
			close(client.send)
			handler.hub.removeClient(client)
		}
	}
}

func (handler *hubHandler) saveMessageToHistory(message *MessagePayload, client *SocketClient) uint {
	return 0
}

func (handler *hubHandler) onNewClient(client *SocketClient)   {}
func (handler *hubHandler) onClientLeave(client *SocketClient) {}
