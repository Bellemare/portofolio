package sockets

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/utils"
)

type LayerMoveEvent struct {
	UserID   uint   `json:"user_id"`
	User     string `json:"user"`
	ID       int    `json:"id"`
	After    int    `json:"after"`
	Layer    int    `json:"layer"`
	NewLayer int    `json:"new_layer"`
}

type LayerMoveEventPayload struct {
	SocketPayload
	Payload LayerMoveEvent `json:"payload"`
}

func NewLayerMoveEventPayload(payload []byte) *LayerMoveEventPayload {
	var layerEventPayload LayerMoveEventPayload
	utils.DecodeMessage(payload, &layerEventPayload)

	return &layerEventPayload
}

func (payload *LayerMoveEventPayload) HandleNewEvent(client *SocketClient) bool {
	payload.Payload.UserID = client.auth.UserID
	if user := client.hub.getCachedUser(client.auth.UserID); user != nil {
		payload.Payload.User = user.Pseudonym
	}
	if drawingHub, ok := client.hub.socketHubHandler.(*drawingHubHandler); ok {
		drawingHub.moveLayer(&payload.Payload, client)
	}

	return payload.broadcast(payload, client)
}
