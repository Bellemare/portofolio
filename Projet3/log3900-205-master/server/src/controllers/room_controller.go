package controllers

import (
	"net/http"

	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/classes"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dto"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/entities"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/middlewares"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/services"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/sockets"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/utils"
)

type RoomController struct {
	service *services.RoomService
}

func NewRoomController() *RoomController {
	return &RoomController{
		service: services.NewRoomService(),
	}
}

func (controller *RoomController) RegisterRoutes(router engine.IRouter) {
	router.Group("/Room", func(router engine.IRouter) {
		router.POST("", controller.createRoom).Use(middlewares.AuthenticateUser)
		router.DELETE("/{id}", controller.deleteRoom).Use(middlewares.AuthenticateUser)
		router.GET("/By/{name}", controller.getRooms).Use(middlewares.AuthenticateUser)
		router.GET("", controller.getRooms).Use(middlewares.AuthenticateUser)
		router.GET("/JoinedRooms", controller.getJoinedRooms).Use(middlewares.AuthenticateUser)
		router.GET("/{id}/History/Limit/{limit}/Offset/{offset}", controller.getHistory).Use(middlewares.AuthenticateUser)
		router.GET("/{id}/Members", controller.getRoomMembers).Use(middlewares.AuthenticateUser)
		router.POST("/{id}/Join", controller.joinRoom).Use(middlewares.AuthenticateUser)
		router.POST("/{id}/Leave", controller.leaveRoom).Use(middlewares.AuthenticateUser)
		router.GET("/{id}/Connect/{token}", controller.connectRoom).Use(middlewares.AuthenticateSocketClient)
		router.GET("/ConnectToPublic/{token}", controller.connectToPublicRoom).Use(middlewares.AuthenticateSocketClient)
		router.GET("/ConnectToApp/{token}", controller.connectToAppRoom).Use(middlewares.AuthenticateSocketClient)
	})
}

func (controller *RoomController) checkIsRoomMember(roomId uint, ctx *engine.Context) bool {
	isMember, err := controller.service.IsRoomMember(&models.RoomMemberModel{
		RoomId: roomId,
		UserId: ctx.Authentication.UserID,
	})
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
		return false
	}
	if !isMember {
		ctx.WriteJSONError(http.StatusForbidden, engine.NewError("L'utilisateur n'est pas membre de la salle"))
		return false
	}

	return true
}

func (controller *RoomController) createRoom(ctx *engine.Context) {
	var roomCreateRequest classes.RoomCreateRequest
	ctx.ParseBody(&roomCreateRequest)
	roomCreateRequest.OwnerId = ctx.Authentication.UserID

	room, err := controller.service.CreateRoom(&roomCreateRequest)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
		return
	}
	roomMemberModel := &models.RoomMemberModel{
		RoomId: room.ID,
		UserId: ctx.Authentication.UserID,
	}
	controller.service.JoinRoom(roomMemberModel)

	var roomDTO dto.RoomDTO
	engine.ParseModelToDTO(&room, &roomDTO)
	ctx.WriteJSON(&roomDTO)
}

func (controller *RoomController) writeRoomsResponse(rooms *[]models.RoomModel, ctx *engine.Context) {
	roomsDTO := []dto.RoomDTO{}
	for _, room := range *rooms {
		var roomDTO dto.RoomDTO
		engine.ParseModelToDTO(&room, &roomDTO)
		roomsDTO = append(roomsDTO, roomDTO)
	}
	ctx.WriteJSON(&roomsDTO)
}

func (controller *RoomController) getRoomMembers(ctx *engine.Context) {
	id, ok := engine.ParseRouteIdentifier("id", ctx)
	if !ok {
		return
	}

	members, err := controller.service.GetRoomMembers(id)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
	} else {
		userService := services.NewUserService()
		for i, member := range *members {
			(*members)[i].Avatar = userService.GetUserAvatar(member.ID)
		}
		ctx.WriteJSON(members)
	}
}

func (controller *RoomController) getRooms(ctx *engine.Context) {
	var roomRequestFilter classes.RoomRequestFilter
	roomRequestFilter.Name = ctx.GetParam("name")

	rooms, err := controller.service.GetRooms(ctx.Authentication.UserID, &roomRequestFilter)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, engine.NewError("Impossible de récupérer les salles"))
	} else {
		controller.writeRoomsResponse(rooms, ctx)
	}
}

func (controller *RoomController) getJoinedRooms(ctx *engine.Context) {
	rooms, err := controller.service.GetJoinedRooms(ctx.Authentication.UserID)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, engine.NewError("Impossible de récupérer les salles"))
	} else {
		controller.writeRoomsResponse(rooms, ctx)
	}
}

func (controller *RoomController) getHistoryRequest(ctx *engine.Context) *classes.RoomFetchHistoryRequest {
	id, ok := engine.ParseRouteIdentifier("id", ctx)
	if !ok {
		return nil
	}
	if id != entities.PublicRoomID {
		if ok := controller.checkIsRoomMember(id, ctx); !ok {
			return nil
		}
	}
	var req *classes.RoomFetchHistoryRequest
	limit, err := utils.ParseModelId(ctx.GetParam("limit"))
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, engine.NewError("Limite invalide"))
	} else {
		offset, err := utils.ParseModelId(ctx.GetParam("offset"))
		if err != nil {
			ctx.WriteJSONError(http.StatusBadRequest, engine.NewError("Page invalide"))
		} else {
			req = &classes.RoomFetchHistoryRequest{
				RoomId: id,
				Limit:  limit,
				Offset: offset,
			}
		}
	}
	return req
}

func (controller *RoomController) getHistory(ctx *engine.Context) {
	req := controller.getHistoryRequest(ctx)
	if req == nil {
		return
	}
	history, err := controller.service.GetRoomHistory(req)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
	} else {
		ctx.WriteJSON(history)
	}
}

func (controller *RoomController) deleteRoom(ctx *engine.Context) {
	id, ok := engine.ParseRouteIdentifier("id", ctx)
	if !ok {
		return
	}

	isOwner, err := controller.service.IsRoomOwner(id, ctx.Authentication.UserID)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
		return
	}
	if !isOwner {
		ctx.WriteJSONError(http.StatusForbidden, engine.NewError("Une salle peut être supprimée seulement par son propriétaire"))
		return
	}

	err = controller.service.DeleteRoom(id)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, engine.NewError("Impossible de supprimer la salle"))
	} else {
		ctx.WriteJSON(&engine.RequestResponse{
			Title: "Succès",
			Body:  "La salle a été supprimée",
		})
	}
}

func (controller *RoomController) joinRoom(ctx *engine.Context) {
	id, ok := engine.ParseRouteIdentifier("id", ctx)
	if !ok {
		return
	}

	err := controller.service.JoinRoom(&models.RoomMemberModel{
		RoomId: id,
		UserId: ctx.Authentication.UserID,
	})
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
	} else {
		ctx.WriteJSON(&engine.RequestResponse{
			Title: "Succès",
			Body:  "L'utilisateur a été ajouté à la salle",
		})
	}
}

func (controller *RoomController) leaveRoom(ctx *engine.Context) {
	id, ok := engine.ParseRouteIdentifier("id", ctx)
	if !ok {
		return
	}

	err := controller.service.LeaveRoom(&models.RoomMemberModel{
		RoomId: id,
		UserId: ctx.Authentication.UserID,
	})
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
	} else {
		ctx.WriteJSON(&engine.RequestResponse{
			Title: "Succès",
			Body:  "L'utilisateur a été retiré de la salle",
		})
	}
}

func (controller *RoomController) connectRoom(ctx *engine.Context) {
	if ctx.GetParam("id") == "" {
		ctx.WriteJSONError(http.StatusBadRequest, engine.NewError("L'identifiant de la salle est obligatoire"))
		return
	}
	id, ok := engine.ParseRouteIdentifier("id", ctx)
	if !ok {
		return
	}
	if ok := controller.checkIsRoomMember(id, ctx); !ok {
		return
	}

	sockets.NewRoomSocketService().HandleNewRoomClient(id, ctx)
}

func (controller *RoomController) connectToPublicRoom(ctx *engine.Context) {
	sockets.NewRoomSocketService().HandleNewRoomClient(entities.PublicRoomID, ctx)
}

func (controller *RoomController) connectToAppRoom(ctx *engine.Context) {
	sockets.NewRoomSocketService().HandleNewAppClient(ctx)
}
