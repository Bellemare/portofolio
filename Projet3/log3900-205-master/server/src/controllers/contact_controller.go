package controllers

import (
	"net/http"

	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/classes"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dto"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/middlewares"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/services"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/sockets"
)

type ContactController struct {
	service *services.ContactService
}

func NewContactController() *ContactController {
	return &ContactController{
		service: services.NewContactService(),
	}
}

func (controller *ContactController) RegisterRoutes(router engine.IRouter) {
	router.Group("/Contact", func(router engine.IRouter) {
		router.POST("", controller.addContact).Use(middlewares.AuthenticateUser)
		router.GET("", controller.getContacts).Use(middlewares.AuthenticateUser)
		router.GET("/UserList", controller.getUserList).Use(middlewares.AuthenticateUser)
		router.GET("/{id}/CollaborativeSessions", controller.getContactCollaborativeSessions).Use(middlewares.AuthenticateUser)
		router.GET("/{id}/Expositions", controller.getContactExpositions).Use(middlewares.AuthenticateUser)
		router.DELETE("/{id}", controller.deleteContact).Use(middlewares.AuthenticateUser)
	})
}

func (controller *ContactController) addContact(ctx *engine.Context) {
	var contactAddRequest classes.ContactAddRequest
	ctx.ParseBody(&contactAddRequest)
	contactAddRequest.UserId = ctx.Authentication.UserID

	contact, err := controller.service.AddContact(&contactAddRequest)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
	} else {
		var contactDTO dto.ContactDTO
		engine.ParseModelToDTO(&contact, &contactDTO)
		ctx.WriteJSON(&contactDTO)
	}
}

func (controller *ContactController) getContacts(ctx *engine.Context) {
	contacts, err := controller.service.GetUserContacts(ctx.Authentication.UserID)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
	} else {
		roomService := sockets.NewRoomSocketService()
		userService := services.NewUserService()
		for i, contact := range *contacts {
			collaborationSession := roomService.GetClientCollaborativeSession(contact.ContactId)
			(*contacts)[i].IsConnected = sockets.UserIsConnected(contact.ContactId)
			if collaborationSession != nil {
				(*contacts)[i].CollaborativeSession = collaborationSession
				(*contacts)[i].IsInCollaborativeSession = true
			}
			(*contacts)[i].ContactAvatar = userService.GetUserAvatar(contact.ContactId)
		}
		ctx.WriteJSON(&contacts)
	}
}

func (controller *ContactController) getContactCollaborativeSessions(ctx *engine.Context) {
	id, ok := engine.ParseRouteIdentifier("id", ctx)
	if !ok {
		return
	}

	if sessions, err := services.NewCollaborationSessionService().GetUserSessions(id); err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
	} else {
		ctx.WriteJSON(sessions)
	}
}

func (controller *ContactController) getContactExpositions(ctx *engine.Context) {
	id, ok := engine.ParseRouteIdentifier("id", ctx)
	if !ok {
		return
	}

	if sessions, err := services.NewExpositionService().GetContactExpositions(id); err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
	} else {
		ctx.WriteJSON(sessions)
	}
}

func (controller *ContactController) getUserList(ctx *engine.Context) {
	users, err := controller.service.GetUserList(ctx.Authentication.UserID)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
	} else {
		userService := services.NewUserService()
		for i, user := range *users {
			(*users)[i].Avatar = userService.GetUserAvatar(user.ID)
		}
		ctx.WriteJSON(&users)
	}
}

func (controller *ContactController) deleteContact(ctx *engine.Context) {
	id, ok := engine.ParseRouteIdentifier("id", ctx)
	if !ok {
		return
	}

	err := controller.service.RemoveContact(&models.ContactModel{
		UserId:    ctx.Authentication.UserID,
		ContactId: id,
	})
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
	} else {
		ctx.WriteJSON(&engine.RequestResponse{
			Title: "Succès",
			Body:  "Le contact a été retiré avec succès",
		})
	}
}
