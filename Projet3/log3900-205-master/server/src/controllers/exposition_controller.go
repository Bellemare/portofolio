package controllers

import (
	"net/http"

	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/classes"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/middlewares"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/services"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/utils"
)

type ExpositionController struct {
	service *services.ExpositionService
}

func NewExpositionController() *ExpositionController {
	return &ExpositionController{
		service: services.NewExpositionService(),
	}
}

func (controller *ExpositionController) RegisterRoutes(router engine.IRouter) {
	router.Group("/Exposition", func(router engine.IRouter) {
		router.GET("", controller.getAllExpositions).Use(middlewares.AuthenticateUser)
		router.GET("/{id}", controller.getExposition).Use(middlewares.AuthenticateUser)
		router.POST("/{id}", controller.addDrawingToExposition).Use(middlewares.AuthenticateUser)
		router.DELETE("/{id}/Drawing/{drawingId}", controller.deleteDrawingFromExposition).Use(middlewares.AuthenticateUser)
	})
}

func (controller *ExpositionController) getAllExpositions(ctx *engine.Context) {
	if expositions, err := controller.service.GetExpositions(); err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
	} else {
		ctx.WriteJSON(expositions)
	}
}

func (controller *ExpositionController) getExposition(ctx *engine.Context) {
	id, ok := engine.ParseRouteIdentifier("id", ctx)
	if !ok {
		return
	}
	if drawings, err := controller.service.GetExposition(id); err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
	} else {
		drawingService := services.NewDrawingService()
		for i, drawing := range *drawings {
			if img, err := drawingService.GetLatestDrawingImage(drawing.ID); err == nil {
				(*drawings)[i].Image = utils.ByteImageToBase64(img)
			}
		}
		ctx.WriteJSON(drawings)
	}
}

func (controller *ExpositionController) addDrawingToExposition(ctx *engine.Context) {
	id, ok := engine.ParseRouteIdentifier("id", ctx)
	if !ok {
		return
	}
	if !checkIsAlbumMember(id, ctx) {
		return
	}

	var addRequest classes.ExpositionAddRequest
	ctx.ParseBody(&addRequest)
	addRequest.AlbumID = id
	if err := controller.service.AddDrawingToExposition(&addRequest); err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
	} else {
		sendDrawingChangeEvent(ctx)
		sendAlbumChangeEvent(ctx)
		ctx.WriteJSON(&engine.RequestResponse{
			Title: "Succès",
			Body:  "Le dessin a été ajouté à l'exposition",
		})
	}
}

func (controller *ExpositionController) deleteDrawingFromExposition(ctx *engine.Context) {
	id, ok := engine.ParseRouteIdentifier("id", ctx)
	if !ok {
		return
	}
	if !checkIsAlbumMember(id, ctx) {
		return
	}
	drawingId, err := utils.ParseModelId(ctx.GetParam("drawingId"))
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, engine.NewError("L'identifiant du dessin est invalide"))
		return
	}

	model := &models.DrawingExpositionModel{
		DrawingID: drawingId,
		AlbumID:   id,
	}
	if err := controller.service.RemoveDrawingFromExposition(model); err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
	} else {
		sendDrawingChangeEvent(ctx)
		sendAlbumChangeEvent(ctx)
		ctx.WriteJSON(&engine.RequestResponse{
			Title: "Succès",
			Body:  "Le dessin a retiré de l'exposition",
		})
	}
}
