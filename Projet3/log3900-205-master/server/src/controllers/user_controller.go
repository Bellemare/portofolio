package controllers

import (
	"net/http"

	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/classes"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dto"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/middlewares"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/services"
)

type UserController struct {
	service *services.UserService
}

func NewUserController() *UserController {
	return &UserController{
		service: services.NewUserService(),
	}
}

func (controller *UserController) RegisterRoutes(router engine.IRouter) {
	router.Group("/User", func(router engine.IRouter) {
		router.POST("", controller.addUser)
		router.GET("/DefaultAvatars", controller.getDefaultAvatars)
		router.GET("/Current", controller.getCurrentUser).Use(middlewares.AuthenticateUser)
		router.GET("/{id}/Avatar", controller.getUserAvatar).Use(middlewares.AuthenticateUser)
		router.GET("/ConnectionHistory", controller.getConnectionHistory).Use(middlewares.AuthenticateUser)
		router.GET("/Metrics", controller.getUserMetrics).Use(middlewares.AuthenticateUser)
		router.GET("/CollaborativeSessions", controller.getUserCollaborationSessions).Use(middlewares.AuthenticateUser)
		router.GET("/{id}", controller.getUser).Use(middlewares.AuthenticateUser)
		router.PUT("/{id}", controller.updateUser).Use(middlewares.AuthenticateUser)
	})
}

func (controller *UserController) addUser(ctx *engine.Context) {
	var userCreateRequest classes.UserCreateRequest
	ctx.ParseBody(&userCreateRequest)

	user, err := controller.service.CreateUser(&userCreateRequest)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
	} else {
		var userDTO dto.UserDTO
		engine.ParseModelToDTO(&user, &userDTO)
		ctx.WriteJSON(&userDTO)
	}
}

func (controller *UserController) getConnectionHistory(ctx *engine.Context) {
	history, err := controller.service.GetConnectionHistory(ctx.Authentication.UserID)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
		return
	}

	ctx.WriteJSON(history)
}

func (controller *UserController) getDefaultAvatars(ctx *engine.Context) {
	ctx.WriteJSON(controller.service.GetDefaultAvatars())
}

func (controller *UserController) getUserMetrics(ctx *engine.Context) {
	if metrics, err := controller.service.GetUserMetrics(ctx.Authentication.UserID); err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
	} else {
		ctx.WriteJSON(metrics)
	}
}

func (controller *UserController) getUserCollaborationSessions(ctx *engine.Context) {
	if sessions, err := services.NewCollaborationSessionService().GetUserSessions(ctx.Authentication.UserID); err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
	} else {
		ctx.WriteJSON(sessions)
	}
}

func (controller *UserController) getCurrentUser(ctx *engine.Context) {
	if user := controller.fetchUser(ctx.Authentication.UserID, ctx); user != nil {
		ctx.WriteJSON(user)
	}
}

func (controller *UserController) fetchUser(id uint, ctx *engine.Context) *dto.UserDTO {
	user, err := controller.service.GetUser(id)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, engine.NewError(err.Error()))
	} else {
		var userDTO dto.UserDTO
		engine.ParseModelToDTO(&user, &userDTO)
		userDTO.IsContact = services.NewContactService().HasContact(&models.ContactModel{
			ContactId: id,
			UserId:    ctx.Authentication.UserID,
		})
		userDTO.Avatar = controller.service.GetUserAvatar(user.ID)
		return &userDTO
	}

	return nil
}

func (controller *UserController) getUserAvatar(ctx *engine.Context) {
	id, ok := engine.ParseRouteIdentifier("id", ctx)
	if !ok {
		return
	}

	var avatarDTO dto.UserAvatarDTO
	avatarDTO.Avatar = controller.service.GetUserAvatar(id)
	avatarDTO.UserID = id

	ctx.WriteJSON(&avatarDTO)
}

func (controller *UserController) getUser(ctx *engine.Context) {
	id, ok := engine.ParseRouteIdentifier("id", ctx)
	if !ok {
		return
	}

	if user := controller.fetchUser(id, ctx); user != nil {
		if !user.IsPublicEmail {
			user.Email = ""
		}
		ctx.WriteJSON(user)
	}
}

func (controller *UserController) updateUser(ctx *engine.Context) {
	id, ok := engine.ParseRouteIdentifier("id", ctx)
	if !ok {
		return
	}
	var userUpdateRequest classes.UserUpdateRequest
	ctx.ParseBody(&userUpdateRequest)
	userUpdateRequest.ID = id

	_, err := controller.service.UpdateUser(&userUpdateRequest)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
	} else {
		ctx.WriteJSON(&engine.RequestResponse{
			Title: "Succès",
			Body:  "Les informations ont été modifiées succès",
		})
	}
}
