package controllers

import (
	"fmt"
	"net/http"

	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/classes"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dto"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/entities"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/middlewares"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/services"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/sockets"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/utils"
)

type AlbumController struct {
	service *services.AlbumService
}

func NewAlbumController() *AlbumController {
	return &AlbumController{
		service: services.NewAlbumService(),
	}
}

func (controller *AlbumController) RegisterRoutes(router engine.IRouter) {
	router.Group("/Album", func(router engine.IRouter) {
		router.POST("", controller.createAlbum).Use(middlewares.AuthenticateUser)
		router.PUT("/{id}", controller.updateAlbum).Use(middlewares.AuthenticateUser)
		router.GET("/JoinedAlbums", controller.getUserAlbums).Use(middlewares.AuthenticateUser)
		router.GET("/PendingJoinRequests", controller.getAllPendingJoinRequests).Use(middlewares.AuthenticateUser)
		router.GET("", controller.getAlbums).Use(middlewares.AuthenticateUser)
		router.GET("/{id}", controller.getAlbum).Use(middlewares.AuthenticateUser)
		router.GET("/{id}/Drawings", controller.getDrawings).Use(middlewares.AuthenticateUser)
		router.POST("/{id}/Drawings/Filter", controller.filterDrawings).Use(middlewares.AuthenticateUser)
		router.GET("/{id}/PendingJoinRequests", controller.getPendingJoinRequests).Use(middlewares.AuthenticateUser)
		router.DELETE("/{id}", controller.deleteAlbum).Use(middlewares.AuthenticateUser)
		router.POST("/{id}/RequestToJoin", controller.requestToJoin).Use(middlewares.AuthenticateUser)
		router.POST("/{id}/JoinRequest/{requestId}/Accept", controller.acceptJoinRequest).Use(middlewares.AuthenticateUser)
		router.POST("/{id}/JoinRequest/{requestId}/Decline", controller.declineJoinRequest).Use(middlewares.AuthenticateUser)
		router.POST("/{id}/Leave", controller.leaveAlbum).Use(middlewares.AuthenticateUser)
	})
}

func checkIsAlbumMember(albumId uint, ctx *engine.Context) bool {
	isMember, err := services.NewAlbumService().IsAlbumMember(&models.AlbumMemberModel{
		AlbumId: albumId,
		UserId:  ctx.Authentication.UserID,
	})
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
		return false
	}
	if !isMember {
		ctx.WriteJSONError(http.StatusForbidden, engine.NewError("L'utilisateur n'est pas membre de l'album"))
		return false
	}

	return true
}

func checkIsAlbumOwner(albumId uint, ctx *engine.Context) bool {
	isOwner, err := services.NewAlbumService().IsAlbumOwner(albumId, ctx.Authentication.UserID)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
		return false
	}
	if !isOwner {
		ctx.WriteJSONError(http.StatusForbidden, engine.NewError("L'utilisateur n'est pas propriétaire de l'album"))
		return false
	}

	return true
}

func sendAlbumChangeEvent(ctx *engine.Context) {
	if client := sockets.NewRoomSocketService().AppHub.GetClient(ctx.Authentication.UserID); client != nil {
		sockets.NewAlbumChangeEventPayload().HandleNewEvent(client)
	}
}

func (controller *AlbumController) requestToJoin(ctx *engine.Context) {
	id, ok := engine.ParseRouteIdentifier("id", ctx)
	if !ok {
		return
	}

	req := &models.AlbumJoinRequestModel{
		AlbumId: id,
		UserId:  ctx.Authentication.UserID,
	}
	if err := controller.service.RequestJoinAlbum(req); err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
	} else {
		if client := sockets.NewRoomSocketService().AppHub.GetClient(ctx.Authentication.UserID); client != nil {
			sockets.NewRefreshAlbumJoinRequestsPayload().HandleNewEvent(client)
		}
		ctx.WriteJSON(&engine.RequestResponse{
			Title: "Succès",
			Body:  "La demande a été envoyée avec succès",
		})
	}
}

func (controller *AlbumController) valideJoinRequest(ctx *engine.Context) uint {
	id, ok := engine.ParseRouteIdentifier("id", ctx)
	if !ok {
		return 0
	}
	if ok := checkIsAlbumMember(id, ctx); !ok {
		return 0
	}
	reqId, err := utils.ParseModelId(ctx.GetParam("requestId"))
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, engine.NewError("Identifiant de requête invalide"))
		return 0
	}
	return reqId
}

func (controller *AlbumController) acceptJoinRequest(ctx *engine.Context) {
	reqId := controller.valideJoinRequest(ctx)
	if reqId <= 0 {
		return
	}

	req, err := controller.service.AcceptJoinRequest(reqId)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
	} else {
		if client := sockets.NewRoomSocketService().AppHub.GetClient(req.UserId); client != nil {
			if album, err := controller.service.GetAlbum(req.AlbumId); err == nil {
				msg := fmt.Sprintf("Votre demande pour rejoindre l'album %s a été acceptée.", album.Name)
				sockets.NewNotificationPayload(msg).HandleNewEvent(client)
			}
		}
		sendAlbumChangeEvent(ctx)
		ctx.WriteJSON(&engine.RequestResponse{
			Title: "Succès",
			Body:  "L'utilisateur a été ajouté à l'album",
		})
	}
}

func (controller *AlbumController) declineJoinRequest(ctx *engine.Context) {
	reqId := controller.valideJoinRequest(ctx)
	if reqId <= 0 {
		return
	}

	req, err := controller.service.DeclineJoinRequest(reqId)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
	} else {
		if client := sockets.NewRoomSocketService().AppHub.GetClient(req.UserId); client != nil {
			if album, err := controller.service.GetAlbum(req.AlbumId); err == nil {
				msg := fmt.Sprintf("Votre demande pour rejoindre l'album %s a été refusée.", album.Name)
				sockets.NewNotificationPayload(msg).HandleNewEvent(client)
			}
		}
		sendAlbumChangeEvent(ctx)
		ctx.WriteJSON(&engine.RequestResponse{
			Title: "Succès",
			Body:  "La demande a été refusée",
		})
	}
}

func (controller *AlbumController) getPendingJoinRequests(ctx *engine.Context) {
	id, ok := engine.ParseRouteIdentifier("id", ctx)
	if !ok {
		return
	}
	if ok := checkIsAlbumMember(id, ctx); !ok {
		return
	}
	requests, err := controller.service.GetPendingJoinRequests(id)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
	} else {
		ctx.WriteJSON(requests)
	}
}

func (controller *AlbumController) getAllPendingJoinRequests(ctx *engine.Context) {
	requests, err := controller.service.GetAllPendingJoinRequests(ctx.Authentication.UserID)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
	} else {
		ctx.WriteJSON(requests)
	}
}

func (controller *AlbumController) leaveAlbum(ctx *engine.Context) {
	id, ok := engine.ParseRouteIdentifier("id", ctx)
	if !ok {
		return
	}
	if isOwner, err := controller.service.IsAlbumOwner(id, ctx.Authentication.UserID); err != nil || isOwner {
		if err != nil {
			ctx.WriteJSONError(http.StatusBadRequest, err)
		} else {
			ctx.WriteJSONError(http.StatusForbidden, engine.NewError("Le propriétaire d'un album ne peut le quitter"))
		}
		return
	}

	member := &models.AlbumMemberModel{
		UserId:  ctx.Authentication.UserID,
		AlbumId: id,
	}
	if err := controller.service.LeaveAlbum(member); err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
	} else {
		ctx.WriteJSON(&engine.RequestResponse{
			Title: "Succès",
			Body:  "L'utilisateur a été retiré de l'album",
		})
	}
}

func (controller *AlbumController) createAlbum(ctx *engine.Context) {
	var albumCreateRequest classes.AlbumCreateRequest
	ctx.ParseBody(&albumCreateRequest)
	albumCreateRequest.OwnerId = ctx.Authentication.UserID

	album, err := controller.service.CreateAlbum(&albumCreateRequest)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
	} else {
		controller.service.AddAlbumMember(&models.AlbumMemberModel{
			AlbumId: album.ID,
			UserId:  ctx.Authentication.UserID,
		})

		sendAlbumChangeEvent(ctx)
		var albumDTO dto.AlbumDTO
		engine.ParseModelToDTO(&album, &albumDTO)
		ctx.WriteJSON(&albumDTO)
	}
}

func (controller *AlbumController) updateAlbum(ctx *engine.Context) {
	id, ok := engine.ParseRouteIdentifier("id", ctx)
	if !ok {
		return
	}
	if ok := checkIsAlbumOwner(id, ctx); !ok {
		return
	}
	var albumUpdateRequest classes.AlbumUpdateRequest
	ctx.ParseBody(&albumUpdateRequest)
	albumUpdateRequest.ID = id

	_, err := controller.service.UpdateAlbum(&albumUpdateRequest)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
	} else {
		sendAlbumChangeEvent(ctx)
		ctx.WriteJSON(&engine.RequestResponse{
			Title: "Succès",
			Body:  "L'album a été modifié avec succès",
		})
	}
}

func (controller *AlbumController) getUserAlbums(ctx *engine.Context) {
	albums, err := controller.service.GetUserAlbumsList(ctx.Authentication.UserID)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
	} else {
		controller.writeAlbumsDTOResponse(ctx, albums)
	}
}

func (controller *AlbumController) writeAlbumsDTOResponse(ctx *engine.Context, albums *[]models.AlbumModel) {
	albumsDTO := []dto.AlbumDTO{}
	for _, val := range *albums {
		var albumDTO dto.AlbumDTO
		engine.ParseModelToDTO(&val, &albumDTO)
		albumsDTO = append(albumsDTO, albumDTO)
	}
	ctx.WriteJSON(albumsDTO)
}

func (controller *AlbumController) getAlbums(ctx *engine.Context) {
	albums, err := controller.service.GetAlbums(ctx.Authentication.UserID)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, engine.NewError("Impossible de récupérer les albums"))
	} else {
		controller.writeAlbumsDTOResponse(ctx, albums)
	}
}

func (controller *AlbumController) getAlbum(ctx *engine.Context) {
	id, ok := engine.ParseRouteIdentifier("id", ctx)
	if !ok {
		return
	}

	album, err := controller.service.GetAlbum(id)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, engine.NewError("Impossible de récupérer les albums"))
	} else {
		albumDTO := &dto.AlbumDTO{}
		engine.ParseModelToDTO(&album, albumDTO)
		ctx.WriteJSON(albumDTO)
	}
}

func (controller *AlbumController) deleteAlbum(ctx *engine.Context) {
	id, ok := engine.ParseRouteIdentifier("id", ctx)
	if !ok {
		return
	}
	if ok := checkIsAlbumOwner(id, ctx); !ok {
		return
	}

	drawingService := services.NewDrawingService()
	drawings, _ := drawingService.GetAll(id, &classes.DrawingsFilterRequest{})
	err := controller.service.DeleteAlbum(id)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, engine.NewError("Impossible de supprimer l'album"))
	} else {
		if drawings != nil {
			roomService := sockets.NewRoomSocketService()
			for _, drawing := range *drawings {
				roomService.ForceStopDrawingHub(drawing.ID, "L'album a été supprimé par le propriétaire")
			}
		}
		sendAlbumChangeEvent(ctx)
		ctx.WriteJSON(&engine.RequestResponse{
			Title: "Succès",
			Body:  "L'album a été supprimé",
		})
	}
}

func (controller *AlbumController) getDrawings(ctx *engine.Context) {
	controller.getDrawingsWithFilter(&classes.DrawingsFilterRequest{}, ctx)
}

func (controller *AlbumController) filterDrawings(ctx *engine.Context) {
	var drawingsFilter *classes.DrawingsFilterRequest
	ctx.ParseBody(&drawingsFilter)

	controller.getDrawingsWithFilter(drawingsFilter, ctx)
}

func (controller *AlbumController) getDrawingsWithFilter(filter *classes.DrawingsFilterRequest, ctx *engine.Context) {
	albumId, ok := engine.ParseRouteIdentifier("id", ctx)
	if !ok {
		return
	}
	if albumId != entities.PublicAlbumID {
		if ok := checkIsAlbumMember(albumId, ctx); !ok {
			return
		}
	}

	drawingService := services.NewDrawingService()
	drawings, err := drawingService.GetAll(albumId, filter)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, engine.NewError("Impossible de récupérer les dessins"))
	} else {
		roomService := sockets.NewRoomSocketService()
		for i, drawing := range *drawings {
			if img, err := drawingService.GetLatestDrawingImage(drawing.ID); err == nil {
				(*drawings)[i].Image = utils.ByteImageToBase64(img)
			}
			(*drawings)[i].ActiveCollaborators = roomService.GetDrawingClientCount(drawing.ID)
		}
		ctx.WriteJSON(drawings)
	}
}
