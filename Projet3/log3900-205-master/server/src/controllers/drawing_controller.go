package controllers

import (
	"net/http"

	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/classes"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dto"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/entities"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/middlewares"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/services"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/sockets"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/utils"
)

type DrawingController struct {
	service      *services.DrawingService
	albumService *services.AlbumService
}

func NewDrawingController() *DrawingController {
	return &DrawingController{
		service:      services.NewDrawingService(),
		albumService: services.NewAlbumService(),
	}
}

func (controller *DrawingController) RegisterRoutes(router engine.IRouter) {
	router.Group("/Drawing", func(router engine.IRouter) {
		router.POST("", controller.createDrawing).Use(middlewares.AuthenticateUser)
		router.POST("/SmartFilter", controller.smartFilter).Use(middlewares.AuthenticateUser)
		router.POST("/{id}/VerifyPassword", controller.verifyPassword).Use(middlewares.AuthenticateUser)
		router.PUT("/{id}", controller.updateDrawing).Use(middlewares.AuthenticateUser)
		router.GET("/{id}", controller.getDrawing).Use(middlewares.AuthenticateUser)
		router.DELETE("/{id}", controller.deleteDrawing).Use(middlewares.AuthenticateUser)
		router.GET("/{id}/JoinSession/{token}", controller.joinCollaborationSession).Use(middlewares.AuthenticateSocketClient)
	})
}

func sendDrawingChangeEvent(ctx *engine.Context) {
	if client := sockets.NewRoomSocketService().AppHub.GetClient(ctx.Authentication.UserID); client != nil {
		sockets.NewDrawingChangeEventPayload().HandleNewEvent(client)
	}
}

func checkIsDrawingOwner(albumId uint, ctx *engine.Context) bool {
	isOwner, err := services.NewDrawingService().IsDrawingOwner(albumId, ctx.Authentication.UserID)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
		return false
	}
	if !isOwner {
		ctx.WriteJSONError(http.StatusForbidden, engine.NewError("L'utilisateur n'est pas propriétaire du dessin"))
		return false
	}

	return true
}

func (controller *DrawingController) verifyPassword(ctx *engine.Context) {
	id, ok := engine.ParseRouteIdentifier("id", ctx)
	if !ok {
		return
	}
	var request classes.PasswordVerifyRequest
	ctx.ParseBody(&request)

	if isValid, err := controller.service.VerifyPassword(id, request.Password); err != nil || !isValid {
		if err == nil {
			err = engine.NewError("Mot de passe invalide")
		}
		ctx.WriteJSONError(http.StatusBadRequest, err)
	} else {
		ctx.WriteJSON(&engine.RequestResponse{
			Title: "Succès",
			Body:  "Le mot de passe est valide",
		})
	}
}

func (controller *DrawingController) createDrawing(ctx *engine.Context) {
	var drawingCreateRequest classes.DrawingCreateRequest
	ctx.ParseBody(&drawingCreateRequest)
	drawingCreateRequest.OwnerId = ctx.Authentication.UserID
	if drawingCreateRequest.AlbumId != entities.PublicAlbumID {
		if ok := checkIsAlbumMember(drawingCreateRequest.AlbumId, ctx); !ok {
			return
		}
	}

	album, err := controller.service.Create(&drawingCreateRequest)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
	} else {
		sendDrawingChangeEvent(ctx)
		var drawingDTO dto.DrawingDTO
		engine.ParseModelToDTO(&album, &drawingDTO)
		ctx.WriteJSON(&drawingDTO)
	}
}

func (controller *DrawingController) updateDrawing(ctx *engine.Context) {
	id, ok := engine.ParseRouteIdentifier("id", ctx)
	if !ok {
		return
	}
	if ok := checkIsDrawingOwner(id, ctx); !ok {
		return
	}
	var drawingUpdateRequest classes.DrawingUpdateRequest
	ctx.ParseBody(&drawingUpdateRequest)
	drawingUpdateRequest.ID = id

	if _, err := controller.service.Update(&drawingUpdateRequest); err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
	} else {
		sendDrawingChangeEvent(ctx)
		ctx.WriteJSON(&engine.RequestResponse{
			Title: "Succès",
			Body:  "Le dessin a été modifié avec succès",
		})
	}
}

func (controller *DrawingController) smartFilter(ctx *engine.Context) {
	var filter *classes.DrawingsSmartFilter
	ctx.ParseBody(&filter)
	filter.UserID = ctx.Authentication.UserID

	if drawings, err := controller.service.SmartFilter(filter); err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
	} else {
		roomService := sockets.NewRoomSocketService()
		for i, drawing := range *drawings {
			if img, err := controller.service.GetLatestDrawingImage(drawing.ID); err == nil {
				(*drawings)[i].Image = utils.ByteImageToBase64(img)
			}
			(*drawings)[i].ActiveCollaborators = roomService.GetDrawingClientCount(drawing.ID)
		}
		ctx.WriteJSON(drawings)
	}
}

func (controller *DrawingController) getDrawing(ctx *engine.Context) {
	id, ok := engine.ParseRouteIdentifier("id", ctx)
	if !ok {
		return
	}

	drawing, err := controller.service.Get(id)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
		return
	}
	if *drawing.AlbumId != entities.PublicAlbumID {
		if ok := checkIsAlbumMember(*drawing.AlbumId, ctx); !ok {
			return
		}
	}

	drawingDTO := &dto.DrawingDTO{}
	engine.ParseModelToDTO(&drawing, drawingDTO)
	ctx.WriteJSON(drawingDTO)
}

func (controller *DrawingController) deleteDrawing(ctx *engine.Context) {
	id, ok := engine.ParseRouteIdentifier("id", ctx)
	if !ok {
		return
	}
	if ok := checkIsDrawingOwner(id, ctx); !ok {
		return
	}

	err := controller.service.Delete(id)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, engine.NewError("Impossible de supprimer le dessin"))
	} else {
		sendDrawingChangeEvent(ctx)
		sockets.NewRoomSocketService().ForceStopDrawingHub(id, "Le dessin a été supprimé par l'auteur")
		ctx.WriteJSON(&engine.RequestResponse{
			Title: "Succès",
			Body:  "Le dessin a été supprimé",
		})
	}
}

func (controller *DrawingController) joinCollaborationSession(ctx *engine.Context) {
	id, ok := engine.ParseRouteIdentifier("id", ctx)
	if !ok {
		return
	}
	drawing, err := controller.service.Get(id)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
		return
	}
	if *drawing.AlbumId != entities.PublicAlbumID {
		if ok := checkIsAlbumMember(*drawing.AlbumId, ctx); !ok {
			return
		}
	}

	if err := sockets.NewRoomSocketService().HandleNewDrawingClient(id, ctx); err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
	}
}
