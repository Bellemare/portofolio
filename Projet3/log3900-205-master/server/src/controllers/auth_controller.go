package controllers

import (
	"net/http"

	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/classes"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/middlewares"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/services"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/sockets"
)

type AuthController struct {
	service *services.AuthService
}

func NewAuthController() *AuthController {
	return &AuthController{
		service: services.NewAuthService(),
	}
}

func (controller *AuthController) RegisterRoutes(router engine.IRouter) {
	router.Group("/Auth", func(router engine.IRouter) {
		router.POST("", controller.login)
		router.POST("/Disconnect", controller.logout).Use(middlewares.AuthenticateUser)
	})
}

func (controller *AuthController) login(ctx *engine.Context) {
	var authRequest classes.AuthRequest
	ctx.ParseBody(&authRequest)
	if authRequest.Email == "" || authRequest.Password == "" {
		ctx.WriteJSONError(http.StatusBadRequest, engine.NewError("le courriel et le mot de passe sont obligatoires"))
		return
	}
	user, err := controller.service.VerifyUserInfo(&authRequest)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, err)
		return
	}
	if sockets.UserIsConnected(user.ID) {
		ctx.WriteJSONError(http.StatusForbidden, engine.NewError("Un utilisateur est déjà connecté sur le compte"))
		return
	}

	token, err := controller.service.CreateUserSession(user)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, engine.NewError(err.Error()))
	} else {
		ctx.WriteJSON(&token)
	}
}

func (controller *AuthController) logout(ctx *engine.Context) {
	err := controller.service.DisconnectUser(ctx.Authentication)
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, engine.NewError(err.Error()))
		return
	}

	ctx.WriteJSON(&engine.RequestResponse{
		Title: "Succès",
		Body:  "L'utilisateur a été déconnecté avec succès",
	})
}
