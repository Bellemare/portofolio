package dto

type AlbumJoinRequestDTO struct {
	ID      uint   `json:"id"`
	UserId  uint   `json:"user_id"`
	User    string `json:"user"`
	AlbumId uint   `json:"album_id"`
	Album   string `json:"album"`
}
