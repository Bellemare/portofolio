package dto

import "time"

type DrawingDTO struct {
	ID                  uint      `json:"id"`
	CreatedAt           time.Time `json:"created_at"`
	ActiveCollaborators int       `json:"active_collaborators"`
	Name                string    `json:"name"`
	OwnerId             uint      `json:"owner_id"`
	Owner               string    `json:"owner"`
	AlbumId             uint      `json:"album_id"`
	Image               string    `json:"image"`
	IsExposed           bool      `json:"is_exposed"`
	IsPasswordProtected bool      `json:"is_password_protected"`
}
