package dto

import "time"

type RoomHistoryDTO struct {
	ID           uint      `json:"id"`
	RoomId       uint      `json:"room_id"`
	Message      string    `json:"message"`
	UserID       uint      `json:"user_id"`
	SentAt       time.Time `json:"sent_at"`
	MessageType  uint      `json:"message_type"`
	ReplyTo      uint      `json:"reply_to"`
	ReplyMessage string    `json:"reply_message"`
	Recipients   []uint    `json:"recipients" gorm:"-"`

	User   string `json:"user"`
	Avatar string `json:"avatar"`
}
