package dto

type ContactCollaborationSessionDTO struct {
	DrawingID           uint   `json:"drawing_id"`
	Drawing             string `json:"drawing"`
	IsPasswordProtected bool   `json:"is_password_protected"`
	AlbumID             uint   `json:"album_id"`
	Album               string `json:"album"`
}
