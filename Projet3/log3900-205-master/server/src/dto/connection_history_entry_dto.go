package dto

import "time"

type ConnectionHistoryEntryDTO struct {
	CreatedAt     time.Time  `json:"created_at"`
	InvalidatedAt *time.Time `json:"invalidated_at"`
}
