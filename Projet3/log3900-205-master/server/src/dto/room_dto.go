package dto

type RoomDTO struct {
	ID      uint   `json:"id"`
	Name    string `json:"name"`
	OwnerId uint   `json:"owner_id"`
}
