package dto

type UserDTO struct {
	ID            uint   `json:"id"`
	Email         string `json:"email"`
	Pseudonym     string `json:"pseudonym"`
	IsContact     bool   `json:"is_contact"`
	Avatar        string `json:"avatar"`
	IsPublicEmail bool   `json:"is_public_email"`
}
