package dto

type UserAvatarDTO struct {
	UserID uint   `json:"user_id"`
	Avatar string `json:"avatar"`
}
