package dto

type ContactDTO struct {
	ID                       uint                            `json:"id"`
	UserId                   uint                            `json:"user_id"`
	ContactId                uint                            `json:"contact_id"`
	Contact                  string                          `json:"contact"`
	ContactAvatar            string                          `json:"contact_avatar"`
	IsConnected              bool                            `json:"is_connected"`
	IsInCollaborativeSession bool                            `json:"is_in_collaborative_session"`
	CollaborativeSession     *ContactCollaborationSessionDTO `json:"collaborative_session" gorm:"-"`
}
