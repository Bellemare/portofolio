package dto

type UserMetricsDTO struct {
	UserID                   uint `json:"user_id"`
	OwnedDrawings            int  `json:"owned_drawings"`
	CollaboratedDrawings     int  `json:"collaborated_drawings"`
	MemberAlbums             int  `json:"member_albums"`
	AverageCollaborationTime int  `json:"average_collaboration_time"`
	TotalCollaborationTime   int  `json:"total_collaboration_time"`
}
