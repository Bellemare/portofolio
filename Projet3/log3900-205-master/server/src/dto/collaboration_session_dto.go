package dto

import "time"

type CollaborationSessionDTO struct {
	StartTime           time.Time `json:"start_time"`
	EndTime             time.Time `json:"end_time"`
	UserID              uint      `json:"user_id"`
	DrawingID           uint      `json:"drawing_id"`
	Drawing             string    `json:"drawing"`
	IsPasswordProtected bool      `json:"is_password_protected"`
	AlbumID             uint      `json:"album_id"`
	Album               string    `json:"album"`
	EditedDrawing       bool      `json:"edited_drawing"`
}
