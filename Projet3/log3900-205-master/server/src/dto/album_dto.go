package dto

type AlbumDTO struct {
	ID          uint   `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	OwnerId     uint   `json:"owner_id"`
}
