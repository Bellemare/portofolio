package dao

import (
	"time"

	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dto"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
)

type SessionDao struct {
	engine.Dao
}

func NewSessionDao() *SessionDao {
	dao := &SessionDao{}
	engine.ConnectDb(&dao.Dao)

	return dao
}

func (dao *SessionDao) GetUserHistory(userId uint) (*[]dto.ConnectionHistoryEntryDTO, error) {
	var history []dto.ConnectionHistoryEntryDTO
	res := dao.Db.Model(&models.SessionModel{}).Where("user_id = ?", userId).Order("created_at DESC").Find(&history)

	return &history, res.Error
}

func (dao *SessionDao) InvalidateUserSession(userID uint) error {
	session, err := dao.GetByUserID(userID)
	if err != nil {
		return err
	}

	session.Valid = false
	session.InvalidatedAt = time.Now()
	res := dao.Db.Save(&session)

	return res.Error
}

func (dao *SessionDao) IsConnected(userID uint) bool {
	session, err := dao.GetByUserID(userID)
	return err == nil && session != nil
}

func (dao *SessionDao) Create(session *models.SessionModel) error {
	session.Valid = true
	result := dao.Db.Create(session)

	return result.Error
}

func (dao *SessionDao) Get(query *models.SessionModel) (*models.SessionModel, error) {
	var session models.SessionModel
	result := dao.Db.Where(&query).First(&session)

	return &session, result.Error
}

func (dao *SessionDao) GetByUserID(userID uint) (*models.SessionModel, error) {
	query := &models.SessionModel{
		UserID: userID,
		Valid:  true,
	}
	var session models.SessionModel
	result := dao.Db.Where(&query).First(&session)

	return &session, result.Error
}
