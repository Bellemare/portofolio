package dao

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
)

type AlbumDao struct {
	engine.Dao
}

func NewAlbumDao() *AlbumDao {
	dao := &AlbumDao{}
	engine.ConnectDb(&dao.Dao)

	return dao
}

func (dao *AlbumDao) GetAll(userId uint) (*[]models.AlbumModel, error) {
	var albums []models.AlbumModel

	albumsIdsQuery := dao.Db.Model(&models.AlbumMemberModel{}).Select("album_id").Where("user_id = ?", userId).Table("album_members")
	joinRequestsIdsQuery := dao.Db.Model(&models.AlbumJoinRequestModel{}).Select("album_id").Where("user_id = ?", userId).Table("album_join_requests")
	res := dao.Db.Model(&models.AlbumModel{}).Where("id NOT IN (?)", albumsIdsQuery).Where("id NOT IN (?)", joinRequestsIdsQuery).Order("id asc").Find(&albums)

	return &albums, res.Error
}

func (dao *AlbumDao) ExistsByName(name string) bool {
	var exists bool
	err := dao.Db.Model(&models.AlbumModel{}).Select("count(*)").Where("name = ?", name).Find(&exists).Error

	return exists && err == nil
}

func (dao *AlbumDao) MemberAlbumsCount(userId uint) int {
	var count int
	albumsIdsQuery := dao.Db.Model(&models.AlbumMemberModel{}).Select("album_id").Where("user_id = ?", userId).Table("album_members")
	err := dao.Db.Model(&models.AlbumModel{}).Select("count(*)").Where("id IN (?)", albumsIdsQuery).Find(&count).Error

	if err != nil {
		return 0
	}

	return count
}
