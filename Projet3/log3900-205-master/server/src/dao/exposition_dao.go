package dao

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dto"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
)

type ExpositionDao struct {
	engine.Dao
}

func NewExpositionDao() *ExpositionDao {
	dao := &ExpositionDao{}
	engine.ConnectDb(&dao.Dao)

	return dao
}

func (dao *ExpositionDao) GetUserExpositions(userId uint) (*[]dto.AlbumDTO, error) {
	var albums []dto.AlbumDTO

	userAlbums := dao.Db.Model(&models.AlbumMemberModel{}).Select("album_id").Where("user_id = ?", userId).Table("album_members")
	expositions := dao.Db.Model(&models.DrawingExpositionModel{}).Select("album_id")
	res := dao.Db.Model(&models.AlbumModel{}).Where("id IN (?)", expositions).Where("id IN (?)", userAlbums).Find(&albums)

	return &albums, res.Error
}

func (dao *ExpositionDao) GetAll() (*[]dto.AlbumDTO, error) {
	var albums []dto.AlbumDTO

	expositions := dao.Db.Model(&models.DrawingExpositionModel{}).Select("drawing_expositions.album_id").
		Joins("inner join albums on drawing_expositions.album_id = albums.id").
		Joins("inner join drawings on drawing_expositions.drawing_id = drawings.id and drawings.deleted_at IS NULL")
	res := dao.Db.Model(&models.AlbumModel{}).Where("id IN (?)", expositions).Find(&albums)

	return &albums, res.Error
}

func (dao *ExpositionDao) Get(albumId uint) (*[]dto.DrawingDTO, error) {
	var drawings []dto.DrawingDTO
	req := dao.Db.Model(&models.DrawingModel{}).
		Select("users.pseudonym as owner", "drawings.*", "IF(drawing_expositions.id IS NULL, FALSE, TRUE) as is_exposed").
		Joins("left join users on users.id = drawings.owner_id").
		Joins("inner join drawing_expositions on drawing_expositions.drawing_id = drawings.id and drawing_expositions.deleted_at IS NULL")
	result := req.Where("drawings.album_id = ?", albumId).Order("created_at DESC").Find(&drawings)

	return &drawings, result.Error
}

func (dao *ExpositionDao) RemoveDrawingFromExposition(exposition *models.DrawingExpositionModel) error {
	res := dao.Db.Where("drawing_id = ?", exposition.DrawingID).
		Where("album_id = ?", exposition.AlbumID).
		Delete(&models.DrawingExpositionModel{})

	return res.Error
}

func (dao *ExpositionDao) IsAlreadyInExposition(exposition *models.DrawingExpositionModel) bool {
	var exists bool
	err := dao.Db.Model(&models.DrawingExpositionModel{}).
		Select("count(*) > 0").
		Where("drawing_id = ?", exposition.DrawingID).
		Where("album_id = ?", exposition.AlbumID).Find(&exists).Error

	return exists && err == nil
}
