package dao

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dto"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
)

type ContactDao struct {
	engine.Dao
}

func NewContactDao() *ContactDao {
	dao := &ContactDao{}
	engine.ConnectDb(&dao.Dao)

	return dao
}

func (dao *ContactDao) HasContact(req *models.ContactModel) bool {
	var exists bool
	err := dao.Db.Model(&models.ContactModel{}).Select("count(*)").Where("user_id = ?", req.UserId).Where("contact_id = ?", req.ContactId).Find(&exists).Error

	return exists && err == nil
}

func (dao *ContactDao) GetNonContacts(userId uint) (*[]dto.UserDTO, error) {
	var users []dto.UserDTO
	contactsReq := dao.Db.Model(&models.ContactModel{}).Select("contact_id").Where("user_id = ?", userId)
	res := dao.Db.Model(&models.UserModel{}).Select("users.*").Where("id NOT IN (?)", contactsReq).Where("id != ?", userId).Find(&users)

	return &users, res.Error
}

func (dao *ContactDao) GetAll(userId uint) (*[]dto.ContactDTO, error) {
	var contacts []dto.ContactDTO
	selectReq := dao.Db.Model(&models.ContactModel{}).Select("users.pseudonym as contact", "contacts.*").Joins("left join users on users.id = contacts.contact_id")
	res := selectReq.Where("user_id = ?", userId).Find(&contacts)

	return &contacts, res.Error
}

func (dao *ContactDao) RemoveContact(req *models.ContactModel) error {
	res := dao.Db.Model(&models.ContactModel{}).Where("user_id = ?", req.UserId).Where("contact_id = ?", req.ContactId).Delete(req)

	return res.Error
}
