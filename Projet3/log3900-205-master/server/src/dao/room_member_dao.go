package dao

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
)

type RoomMemberDao struct {
	engine.Dao
}

func NewRoomMemberDao() *RoomMemberDao {
	dao := &RoomMemberDao{}
	engine.ConnectDb(&dao.Dao)

	return dao
}

func (dao *RoomMemberDao) AddUser(roomMember *models.RoomMemberModel) error {
	result := dao.Db.Create(roomMember)

	return result.Error
}

func (dao *RoomMemberDao) RemoveUser(roomMember *models.RoomMemberModel) error {
	result := dao.Db.Where("room_id = ?", roomMember.RoomId).Where("user_id = ?", roomMember.UserId).Delete(roomMember)

	return result.Error
}

func (dao *RoomMemberDao) IsRoomMember(roomMember *models.RoomMemberModel) bool {
	var exists bool
	err := dao.Db.Model(&models.RoomMemberModel{}).Select("count(*) > 0").Where("room_id = ?", roomMember.RoomId).Where("user_id = ?", roomMember.UserId).Find(&exists).Error

	return exists && err == nil
}

func (dao *RoomMemberDao) DeleteRoom(roomId uint) error {
	result := dao.Db.Where("room_id = ?", roomId).Delete(&models.RoomMemberModel{})

	return result.Error
}

func (dao *RoomMemberDao) GetMembersCount(roomId uint) (int64, error) {
	var count int64
	err := dao.Db.Model(&models.RoomMemberModel{}).Select("count(*)").Where("room_id = ?", roomId).Count(&count).Error

	return count, err
}
