package dao

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dto"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
)

type UserDao struct {
	engine.Dao
}

func NewUserDao() *UserDao {
	dao := &UserDao{}
	engine.ConnectDb(&dao.Dao)

	return dao
}

func (dao *UserDao) UserExistsByPseudonym(pseudonym string) bool {
	var exists bool
	err := dao.Db.Model(&models.UserModel{}).Select("count(*)").Where("pseudonym = ?", pseudonym).Find(&exists).Error

	return exists && err == nil
}

func (dao *UserDao) UserExistsByEmail(email string) bool {
	var exists bool
	err := dao.Db.Model(&models.UserModel{}).Select("count(*) > 0").Where("email = ?", email).Find(&exists).Error

	return exists && err == nil
}

func (dao *UserDao) Create(user *models.UserModel) error {
	result := dao.Db.Create(user)

	return result.Error
}

func (dao *UserDao) Get(model *models.UserModel) (*models.UserModel, error) {
	var user models.UserModel
	result := dao.Db.Where(model).First(&user)

	return &user, result.Error
}

func (dao *UserDao) Update(model *models.UserModel, update *models.UserModel) error {
	isPublicEmailIntVal := 0
	if update.IsPublicEmail {
		isPublicEmailIntVal = 1
	}
	result := dao.Db.Model(model).Update("pseudonym", update.Pseudonym).Update("is_public_email", isPublicEmailIntVal)

	return result.Error
}

func (dao *UserDao) GetAll() (*[]dto.UserDTO, error) {
	var users []dto.UserDTO
	result := dao.Db.Model(&models.UserModel{}).Select("users.id", "users.pseudonym").Find(&users)

	return &users, result.Error
}
