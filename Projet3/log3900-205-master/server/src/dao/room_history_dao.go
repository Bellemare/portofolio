package dao

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/classes"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dto"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
)

type RoomHistoryDao struct {
	engine.Dao
}

func NewRoomHistoryDao() *RoomHistoryDao {
	dao := &RoomHistoryDao{}
	engine.ConnectDb(&dao.Dao)

	return dao
}

func (dao *RoomHistoryDao) AddHistory(roomHistory *models.RoomHistoryModel) error {
	recipients := roomHistory.Recipients
	result := dao.Db.Create(roomHistory)
	if result.Error == nil && len(recipients) > 0 {
		var tags []models.RoomHistoryTagModel
		for _, recipient := range recipients {
			tags = append(tags, models.RoomHistoryTagModel{
				MessageId: roomHistory.ID,
				UserID:    recipient,
			})
		}
		dao.Db.Create(&tags)
	}

	return result.Error
}

func (dao *RoomHistoryDao) AddDrawingHistory(drawingRoomHistory *models.DrawingRoomHistoryModel) error {
	recipients := drawingRoomHistory.Recipients
	result := dao.Db.Create(drawingRoomHistory)
	if result.Error == nil && len(recipients) > 0 {
		var tags []models.DrawingRoomHistoryTagModel
		for _, recipient := range recipients {
			tags = append(tags, models.DrawingRoomHistoryTagModel{
				RoomHistoryTagModel: models.RoomHistoryTagModel{
					MessageId: drawingRoomHistory.ID,
					UserID:    recipient,
				},
			})
		}
		dao.Db.Create(&tags)
	}

	return result.Error
}

func (dao *RoomHistoryDao) GetHistory(req *classes.RoomFetchHistoryRequest) (*[]dto.RoomHistoryDTO, error) {
	var messages []dto.RoomHistoryDTO
	selectStmt := dao.Db.Model(&models.RoomHistoryModel{}).Select("users.pseudonym as user,room_history.*,reply.message reply_message")
	request := selectStmt.Joins("left join users on users.id = room_history.user_id").
		Joins("left join room_history reply on reply.id = room_history.reply_to")
	result := request.Where("room_history.room_id = ?", req.RoomId).Limit(int(req.Limit)).Offset(int(req.Offset)).Order("room_history.sent_at desc").Find(&messages)

	if result.Error == nil {
		for i, message := range messages {
			var tags []uint
			dao.Db.Model(&models.RoomHistoryTagModel{}).Select("room_history_tags.user_id").Where("message_id = ?", message.ID).Find(&tags)
			messages[i].Recipients = tags
		}
	}

	return &messages, result.Error
}

func (dao *RoomHistoryDao) GetDrawingRoomHistory(id uint) ([]*models.Message, error) {
	var messages []*models.Message
	selectStmt := dao.Db.Model(&models.DrawingRoomHistoryModel{}).Select("users.pseudonym as user,drawing_room_history.*,reply.message reply_message")
	request := selectStmt.Joins("left join users on users.id = drawing_room_history.user_id").
		Joins("left join drawing_room_history reply on reply.id = drawing_room_history.reply_to")
	result := request.Where("drawing_room_history.room_id = ?", id).Order("drawing_room_history.sent_at desc").Find(&messages)

	if result.Error == nil {
		for i, message := range messages {
			var tags []uint
			dao.Db.Model(&models.DrawingRoomHistoryTagModel{}).Select("drawing_room_history_tags.user_id").Where("message_id = ?", message.ID).Find(&tags)
			messages[i].Recipients = tags
		}
	}

	return messages, result.Error
}
