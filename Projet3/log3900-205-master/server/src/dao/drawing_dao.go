package dao

import (
	"strings"
	"time"
	"unicode"

	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/classes"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dto"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
	"golang.org/x/text/runes"
	"golang.org/x/text/transform"
	"golang.org/x/text/unicode/norm"
	"gorm.io/gorm"
)

type DrawingDao struct {
	engine.Dao
}

var months = [...]string{"janvier", "fevrier", "mars", "avril", "mai", "juin", "juillet", "aout", "septembre", "octobre", "novembre", "decembre"}

func NewDrawingDao() *DrawingDao {
	dao := &DrawingDao{}
	engine.ConnectDb(&dao.Dao)

	return dao
}

func (dao *DrawingDao) getMonthInterval(searchValue string) (*time.Time, *time.Time) {
	t := transform.Chain(norm.NFD, runes.Remove(runes.In(unicode.Mn)), norm.NFC)
	value, _, _ := transform.String(t, searchValue)
	value = strings.ToLower(value)
	for i := 0; i < len(months); i++ {
		if months[i] == value {
			y, _, _ := time.Now().Date()
			firstDay := time.Date(y, time.Month(i+1), 1, 0, 0, 0, 0, time.UTC)
			lastDay := time.Date(y, time.Month(i+2), 1, 0, 0, 0, -1, time.UTC)
			return &firstDay, &lastDay
		}
	}

	return nil, nil
}

func (dao *DrawingDao) createSmartFilterQuery(filter *classes.DrawingsSmartFilter) *gorm.DB {
	firstDay, lastDay := dao.getMonthInterval(filter.SearchValue)
	if firstDay != nil && lastDay != nil {
		return dao.Db.Model(&models.DrawingModel{}).Where("drawings.created_at >= ?", firstDay).Where("drawings.created_at <= ?", lastDay)
	}

	value := "%" + strings.ToLower(filter.SearchValue) + "%"
	emailsReq := dao.Db.Model(&models.UserModel{}).Select("id").Where("email LIKE ?", value).Where("is_public_email = 1")
	return dao.Db.Model(&models.DrawingModel{}).Where("drawings.owner_id IN (?)", emailsReq).
		Or("users.pseudonym LIKE ?", value).
		Or("drawings.name LIKE ?", value)
}

func (dao *DrawingDao) SmartFilter(filter *classes.DrawingsSmartFilter) (*[]dto.DrawingDTO, error) {
	var drawings []dto.DrawingDTO
	albumsIdsQuery := dao.Db.Model(&models.AlbumMemberModel{}).Select("album_id").Where("user_id = ?", filter.UserID).Table("album_members")
	req := dao.Db.Model(&models.DrawingModel{}).
		Select("users.pseudonym as owner", "drawings.*", "IF(drawing_expositions.id IS NULL, FALSE, TRUE) as is_exposed", "IF(drawings.password IS NULL, FALSE, TRUE) as is_password_protected").
		Joins("inner join users on users.id = drawings.owner_id").
		Joins("left join albums on albums.id = drawings.album_id").
		Joins("left join drawing_expositions on drawing_expositions.drawing_id = drawings.id and drawing_expositions.deleted_at IS NULL").
		Where(dao.createSmartFilterQuery(filter)).
		Where(dao.Db.Where("albums.deleted_at IS NULL").Or("drawings.album_id = 0")).
		Where("drawings.album_id IN (?) OR drawings.album_id=0", albumsIdsQuery).
		Order("drawings.created_at desc")

	if filter.AlbumID >= 0 {
		req = req.Where("drawings.album_id = ?", filter.AlbumID)
	}
	result := req.Find(&drawings)

	return &drawings, result.Error
}

func (dao *DrawingDao) createConditionFromArray(field string, values []string) *gorm.DB {
	req := dao.Db
	for _, name := range values {
		req = req.Or(field+" LIKE ?", "%"+name+"%")
	}
	return req
}

func (dao *DrawingDao) createFilter(filter *classes.DrawingsFilterRequest) *gorm.DB {
	req := dao.Db.Model(&models.DrawingModel{})
	if len(filter.NamesFilter) > 0 {
		req.Where(dao.createConditionFromArray("drawings.name", filter.NamesFilter))
	}
	if len(filter.PseudonymsFilter) > 0 {
		req.Where(dao.createConditionFromArray("users.pseudonym", filter.PseudonymsFilter))
	}
	if filter.StartDate != nil {
		date := time.Date(filter.StartDate.Year(), filter.StartDate.Month(), filter.StartDate.Day(), 0, 0, 0, 0, time.UTC)
		req.Where("drawings.created_at >= ?", date)
	}
	if filter.EndDate != nil {
		date := time.Date(filter.EndDate.Year(), filter.EndDate.Month(), (filter.EndDate.Day() + 1), 0, 0, 0, -1, time.UTC)
		req.Where("drawings.created_at <= ?", date)
	}

	return req
}

func (dao *DrawingDao) GetAll(albumId uint, filter *classes.DrawingsFilterRequest) (*[]dto.DrawingDTO, error) {
	var drawings []dto.DrawingDTO
	req := dao.Db.Model(&models.DrawingModel{}).
		Select("users.pseudonym as owner", "drawings.*", "IF(drawing_expositions.id IS NULL, FALSE, TRUE) as is_exposed", "IF(drawings.password IS NULL, FALSE, TRUE) as is_password_protected").
		Joins("left join users on users.id = drawings.owner_id").
		Joins("left join drawing_expositions on drawing_expositions.drawing_id = drawings.id and drawing_expositions.deleted_at IS NULL")
	result := req.Where(dao.createFilter(filter)).Where("drawings.album_id = ?", albumId).Order("created_at DESC").Find(&drawings)

	return &drawings, result.Error
}

func (dao *DrawingDao) GetCollaborativeSessionInfo(drawingId uint) (*dto.ContactCollaborationSessionDTO, error) {
	var info dto.ContactCollaborationSessionDTO
	result := dao.Db.Model(&models.DrawingModel{}).
		Select("drawings.*", "albums.name as album", "drawings.name as drawing", "drawings.id as drawing_id", "IF(drawings.password IS NULL, FALSE, TRUE) as is_password_protected").
		Joins("left join albums on albums.id = drawings.album_id").
		Where("drawings.id = ?", drawingId).Find(&info)

	return &info, result.Error
}

func (dao *DrawingDao) Update(model *models.DrawingModel, update *models.DrawingModel) error {
	result := dao.Db.Model(model).Updates(map[string]interface{}{"password": update.Password, "album_id": update.AlbumId, "name": update.Name})

	return result.Error
}

func (dao *DrawingDao) TransferOwnership(userDrawings *models.DrawingModel, newOwnerId uint) error {
	newDrawingOwner := &models.DrawingModel{
		OwnerId: newOwnerId,
	}
	result := dao.Db.Model(&models.DrawingModel{}).Where("owner_id = ?", userDrawings.OwnerId).Where("album_id = ?", userDrawings.AlbumId).Updates(&newDrawingOwner)

	return result.Error
}

func (dao *DrawingDao) GetUserDrawingsOwnedCount(userId uint) int {
	var count int
	result := dao.Db.Model(&models.DrawingModel{}).Select("count(*)").
		Joins("left join albums on albums.id = drawings.album_id").
		Where("albums.deleted_at IS NULL").
		Where("drawings.owner_id = ?", userId).Find(&count)

	if result.Error != nil {
		return 0
	}

	return count
}
