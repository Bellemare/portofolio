package dao

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dto"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
)

type CollaborationSessionDao struct {
	engine.Dao
}

func NewCollaborationSessionDao() *CollaborationSessionDao {
	dao := &CollaborationSessionDao{}
	engine.ConnectDb(&dao.Dao)

	return dao
}

func (dao *CollaborationSessionDao) GetSessions(userId uint) (*[]dto.CollaborationSessionDTO, error) {
	var sessions []dto.CollaborationSessionDTO
	res := dao.Db.Model(&models.CollaborationSessionModel{}).
		Select(
			"user_collaboration_sessions.*",
			"albums.id as album_id",
			"IF(drawings.album_id = 0 , 'Colorimage', albums.name) as album",
			"drawings.name as drawing",
			"IF(drawings.password IS NULL, FALSE, TRUE) as is_password_protected").
		Joins("inner join drawings on drawings.id = user_collaboration_sessions.drawing_id").
		Joins("left join albums on albums.id = drawings.album_id").
		Joins("inner join users on users.id = user_collaboration_sessions.user_id").
		Where("user_collaboration_sessions.user_id = ?", userId).
		Where("user_collaboration_sessions.edited_drawing = 1").
		Order("user_collaboration_sessions.start_time DESC").
		Find(&sessions)

	return &sessions, res.Error
}

func (dao *CollaborationSessionDao) GetAllSessions(userId uint) (*[]dto.CollaborationSessionDTO, error) {
	var sessions []dto.CollaborationSessionDTO
	res := dao.Db.Model(&models.CollaborationSessionModel{}).
		Select("user_collaboration_sessions.*").
		Where("user_collaboration_sessions.user_id = ?", userId).
		Find(&sessions)

	return &sessions, res.Error
}
