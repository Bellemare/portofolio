package dao

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/classes"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dto"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
	"gorm.io/gorm"
)

type RoomDao struct {
	engine.Dao
}

func NewRoomDao() *RoomDao {
	dao := &RoomDao{}
	engine.ConnectDb(&dao.Dao)

	return dao
}

func (dao *RoomDao) GetAll(userId uint, query *classes.RoomRequestFilter) (*[]models.RoomModel, error) {
	var rooms []models.RoomModel
	var result *gorm.DB

	roomMemberIds := dao.Db.Model(&models.RoomMemberModel{}).Select("room_id").Where("user_id = ?", userId).Table("room_members")
	if query.Name == "" {
		result = dao.Db.Where("id NOT IN (?)", roomMemberIds).Find(&rooms)
	} else {
		result = dao.Db.Where("id NOT IN (?)", roomMemberIds).Where("name LIKE ?", "%"+query.Name+"%").Find(&rooms)
	}

	return &rooms, result.Error
}

func (dao *RoomDao) GetMemberRooms(userId uint) (*[]models.RoomModel, error) {
	var rooms []models.RoomModel

	roomMemberIds := dao.Db.Model(&models.RoomMemberModel{}).Select("room_id").Where("user_id = ?", userId).Table("room_members")
	result := dao.Db.Where("id IN (?)", roomMemberIds).Find(&rooms)

	return &rooms, result.Error
}

func (dao *RoomDao) GetRoomMembers(roomId uint) (*[]dto.UserDTO, error) {
	var members []dto.UserDTO

	roomMembers := dao.Db.Model(&models.RoomMemberModel{}).Select("users.id", "users.pseudonym").Joins("left join users on users.id = room_members.user_id")
	result := roomMembers.Where("room_members.room_id = ?", roomId).Find(&members)

	return &members, result.Error
}

func (dao *RoomDao) ExistsByName(name string) bool {
	var exists bool
	err := dao.Db.Model(&models.RoomModel{}).Select("count(*) > 0").Where("name = ?", name).Find(&exists).Error

	return exists && err == nil
}
