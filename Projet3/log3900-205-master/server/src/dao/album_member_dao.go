package dao

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
)

type AlbumMemberDao struct {
	engine.Dao
}

func NewAlbumMemberDao() *AlbumMemberDao {
	dao := &AlbumMemberDao{}
	engine.ConnectDb(&dao.Dao)

	return dao
}

func (dao *AlbumMemberDao) AddUser(albumMember *models.AlbumMemberModel) error {
	result := dao.Db.Create(albumMember)

	return result.Error
}

func (dao *AlbumMemberDao) RemoveUser(albumMember *models.AlbumMemberModel) error {
	result := dao.Db.Where("album_id = ?", albumMember.AlbumId).Where("user_id = ?", albumMember.UserId).Delete(albumMember)

	return result.Error
}

func (dao *AlbumMemberDao) IsAlbumMember(albumMember *models.AlbumMemberModel) bool {
	var exists bool
	err := dao.Db.Model(&models.AlbumMemberModel{}).Select("count(*)").Where("album_id = ?", albumMember.AlbumId).Where("user_id = ?", albumMember.UserId).Find(&exists).Error

	return exists && err == nil
}

func (dao *AlbumMemberDao) GetUserAlbumList(userId uint) (*[]models.AlbumModel, error) {
	var albums []models.AlbumModel

	albumsIdsQuery := dao.Db.Model(&models.AlbumMemberModel{}).Select("album_id").Where("user_id = ?", userId).Table("album_members")
	res := dao.Db.Model(&models.AlbumModel{}).Where("id IN (?)", albumsIdsQuery).Order("id asc").Find(&albums)

	return &albums, res.Error
}

func (dao *AlbumMemberDao) DeleteAlbum(albumId uint) error {
	result := dao.Db.Where("album_id = ?", albumId).Delete(&models.AlbumMemberModel{})

	return result.Error
}

func (dao *AlbumMemberDao) GetMembersCount(albumId uint) (int64, error) {
	var count int64
	err := dao.Db.Model(&models.AlbumMemberModel{}).Select("count(*)").Where("album_id = ?", albumId).Count(&count).Error

	return count, err
}
