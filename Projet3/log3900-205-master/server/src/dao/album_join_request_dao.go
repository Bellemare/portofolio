package dao

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dto"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
)

type AlbumJoinRequestDao struct {
	engine.Dao
}

func NewAlbumJoinRequestDao() *AlbumJoinRequestDao {
	dao := &AlbumJoinRequestDao{}
	engine.ConnectDb(&dao.Dao)

	return dao
}

func (dao *AlbumJoinRequestDao) HasPendingRequest(req *models.AlbumJoinRequestModel) bool {
	var exists bool
	err := dao.Db.Model(&models.AlbumJoinRequestModel{}).Select("count(*) > 0").Where("album_id = ?", req.AlbumId).Where("user_id = ?", req.UserId).Find(&exists).Error

	return exists && err == nil
}

func (dao *AlbumJoinRequestDao) GetAlbumPendingRequests(albumId uint) (*[]dto.AlbumJoinRequestDTO, error) {
	pendingRequests := &[]dto.AlbumJoinRequestDTO{}
	req := dao.Db.Model(&models.AlbumJoinRequestModel{}).Select("users.pseudonym as user", "albums.name as album", "album_join_requests.*")
	req = req.Joins("inner join users on users.id = album_join_requests.user_id").Joins("inner join albums on albums.id = album_join_requests.album_id")
	result := req.Where("album_id = ?", albumId).Find(pendingRequests)

	return pendingRequests, result.Error
}

func (dao *AlbumJoinRequestDao) GetAllAlbumPendingRequests(userId uint) (*[]dto.AlbumJoinRequestDTO, error) {
	pendingRequests := &[]dto.AlbumJoinRequestDTO{}

	req := dao.Db.Model(&models.AlbumJoinRequestModel{}).Select("users.pseudonym as user", "albums.name as album", "album_join_requests.*")
	req = req.Joins("inner join users on users.id = album_join_requests.user_id").Joins("inner join albums on albums.id = album_join_requests.album_id")
	albumsIdsQuery := dao.Db.Model(&models.AlbumMemberModel{}).Select("album_id").Where("user_id = ?", userId).Table("album_members")

	result := req.Where("album_id IN (?)", albumsIdsQuery).Find(pendingRequests)

	return pendingRequests, result.Error
}

func (dao *AlbumJoinRequestDao) DeleteUserRequest(req *models.AlbumJoinRequestModel) error {
	result := dao.Db.Where("album_id = ?", req.AlbumId).Where("user_id = ?", req.UserId).Delete(&models.AlbumJoinRequestModel{})

	return result.Error
}

func (dao *AlbumJoinRequestDao) DeleteAlbum(albumId uint) error {
	result := dao.Db.Where("album_id = ?", albumId).Delete(&models.AlbumJoinRequestModel{})

	return result.Error
}
