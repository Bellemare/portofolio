package models

import (
	"gorm.io/gorm"
)

type DrawingModel struct {
	gorm.Model
	Name     string  `json:"name"`
	OwnerId  uint    `json:"owner_id"`
	AlbumId  *uint   `json:"album_id"`
	Password *string `json:"password" gorm:"default:null"`
}

func (model *DrawingModel) TableName() string {
	return "drawings"
}
