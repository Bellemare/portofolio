package models

import (
	"gorm.io/gorm"
)

type RoomHistoryTagModel struct {
	gorm.Model
	MessageId uint `json:"message_id"`
	UserID    uint `json:"user_id"`
}

func (model *RoomHistoryTagModel) TableName() string {
	return "room_history_tags"
}
