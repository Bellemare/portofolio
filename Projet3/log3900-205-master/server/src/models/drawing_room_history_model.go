package models

type DrawingRoomHistoryModel struct {
	RoomHistoryModel
}

func (model *DrawingRoomHistoryModel) TableName() string {
	return "drawing_room_history"
}
