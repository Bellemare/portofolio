package models

import (
	"time"

	"gorm.io/gorm"
)

type CollaborationSessionModel struct {
	gorm.Model
	UserID        uint      `json:"user_id"`
	DrawingID     uint      `json:"drawing_id"`
	EditedDrawing bool      `json:"edited_drawing"`
	StartTime     time.Time `json:"start_time"`
	EndTime       time.Time `json:"end_time"`
}

func (model *CollaborationSessionModel) TableName() string {
	return "user_collaboration_sessions"
}
