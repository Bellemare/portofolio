package models

type DrawingRoomHistoryTagModel struct {
	RoomHistoryTagModel
}

func (model *DrawingRoomHistoryTagModel) TableName() string {
	return "drawing_room_history_tags"
}
