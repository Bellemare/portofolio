package models

import (
	"gorm.io/gorm"
)

type DrawingExpositionModel struct {
	gorm.Model
	AlbumID   uint `json:"album_id"`
	DrawingID uint `json:"drawing_id"`
}

func (model *DrawingExpositionModel) TableName() string {
	return "drawing_expositions"
}
