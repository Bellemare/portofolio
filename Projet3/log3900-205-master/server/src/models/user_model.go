package models

import (
	"gorm.io/gorm"
)

const (
	UserEmail     = "email"
	UserPassword  = "password"
	UserPseudonym = "pseudonym"
)

type UserModel struct {
	gorm.Model
	Email         string `json:"email"`
	Password      string `json:"password"`
	Pseudonym     string `json:"pseudonym"`
	IsPublicEmail bool   `json:"is_public_email"`
}

func (model *UserModel) TableName() string {
	return "users"
}
