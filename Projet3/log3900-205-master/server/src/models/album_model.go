package models

import (
	"gorm.io/gorm"
)

const (
	AlbumName = "name"
)

type AlbumModel struct {
	gorm.Model
	Name        string `json:"name"`
	OwnerId     uint   `json:"owner_id"`
	Description string `json:"description"`
}

func (model *AlbumModel) TableName() string {
	return "albums"
}

func GetPublicAlbum() *AlbumModel {
	return &AlbumModel{
		gorm.Model{ID: 0},
		"Colorimage",
		0,
		"Album public",
	}
}
