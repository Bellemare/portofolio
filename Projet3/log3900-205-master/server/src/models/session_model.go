package models

import (
	"time"

	"gorm.io/gorm"
)

type SessionModel struct {
	gorm.Model
	UserID        uint      `json:"user_id"`
	Valid         bool      `json:"valid"`
	InvalidatedAt time.Time `json:"invalidated_at" gorm:"default:null"`
}

func (model *SessionModel) TableName() string {
	return "sessions"
}
