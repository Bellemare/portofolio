package models

import (
	"gorm.io/gorm"
)

type ContactModel struct {
	gorm.Model
	UserId    uint `json:"user_id"`
	ContactId uint `json:"contact_id"`
}

func (model *ContactModel) TableName() string {
	return "contacts"
}
