package models

import (
	"gorm.io/gorm"
)

const (
	RoomName = "name"
)

type RoomModel struct {
	gorm.Model
	Name    string `json:"name"`
	OwnerId uint   `json:"owner_id"`
}

func (model *RoomModel) TableName() string {
	return "rooms"
}

func GetPublicRoom() *RoomModel {
	return &RoomModel{
		gorm.Model{ID: 0},
		"Général",
		0,
	}
}
