package models

import (
	"time"

	"gorm.io/gorm"
)

type RoomHistoryModel struct {
	gorm.Model
	RoomId      uint      `json:"room_id"`
	Message     string    `json:"message"`
	UserID      uint      `json:"user_id"`
	SentAt      time.Time `json:"sent_at"`
	ReplyTo     uint      `json:"reply_to"`
	MessageType uint      `json:"message_type"`
	Recipients  []uint    `json:"recipients" gorm:"-"`
}

func (model *RoomHistoryModel) TableName() string {
	return "room_history"
}
