package models

import (
	"gorm.io/gorm"
)

type AlbumMemberModel struct {
	gorm.Model
	AlbumId uint `json:"album_id"`
	UserId  uint `json:"user_id"`
}

func (model *AlbumMemberModel) TableName() string {
	return "album_members"
}
