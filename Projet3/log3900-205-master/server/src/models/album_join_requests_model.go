package models

import (
	"gorm.io/gorm"
)

type AlbumJoinRequestModel struct {
	gorm.Model
	AlbumId uint `json:"album_id"`
	UserId  uint `json:"user_id"`
}

func (model *AlbumJoinRequestModel) TableName() string {
	return "album_join_requests"
}
