package models

import "time"

type Message struct {
	ID          uint      `json:"id"`
	Message     string    `json:"message" validate:"validLength"`
	UserID      uint      `json:"user_id"`
	User        string    `json:"user"`
	SentAt      time.Time `json:"sent_at"`
	ReplyTo     uint      `json:"reply_to"`
	MessageType uint      `json:"message_type"`
	Recipients  []uint    `json:"recipients" gorm:"-"`
}
