package models

import (
	"gorm.io/gorm"
)

type RoomMemberModel struct {
	gorm.Model
	RoomId uint `json:"room_id"`
	UserId uint `json:"user_id"`
}

func (model *RoomMemberModel) TableName() string {
	return "room_members"
}
