package entities

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/classes"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dao"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dto"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
)

type Contact struct {
	Dao *dao.ContactDao
}

func NewContact() *Contact {
	return &Contact{
		Dao: dao.NewContactDao(),
	}
}

func (contact *Contact) AddContact(request *classes.ContactAddRequest) (*models.ContactModel, error) {
	model := models.ContactModel{
		UserId:    request.UserId,
		ContactId: request.ContactId,
	}
	err := contact.Dao.Create(&model)

	return &model, err
}

func (contact *Contact) GetAll(userId uint) (*[]dto.ContactDTO, error) {
	return contact.Dao.GetAll(userId)
}

func (contact *Contact) RemoveContact(request *models.ContactModel) error {
	return contact.Dao.RemoveContact(request)
}
