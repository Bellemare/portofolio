package entities

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/classes"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dao"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
)

const (
	PublicRoomID = 0
)

type Room struct {
	Dao            *dao.RoomDao
	RoomMemberDao  *dao.RoomMemberDao
	RoomHistoryDao *dao.RoomHistoryDao
}

func NewRoom() *Room {
	return &Room{
		Dao:            dao.NewRoomDao(),
		RoomMemberDao:  dao.NewRoomMemberDao(),
		RoomHistoryDao: dao.NewRoomHistoryDao(),
	}
}

func (room *Room) Create(request *classes.RoomCreateRequest) (*models.RoomModel, error) {
	model := models.RoomModel{
		Name:    request.Name,
		OwnerId: request.OwnerId,
	}
	err := room.Dao.Create(&model)

	return &model, err
}

func (room *Room) GetAll(userId uint, request *classes.RoomRequestFilter) (*[]models.RoomModel, error) {
	return room.Dao.GetAll(userId, request)
}

func (room *Room) Get(id uint) (*models.RoomModel, error) {
	model := &models.RoomModel{}
	err := room.Dao.Get(id, model)
	return model, err
}

func (room *Room) Delete(id uint) error {
	err := room.Dao.Delete(id, &models.RoomModel{})
	if err == nil {
		room.RoomMemberDao.DeleteRoom(id)
	}

	return err
}
