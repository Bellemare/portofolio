package entities

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/classes"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dao"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dto"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/utils"
	"gorm.io/gorm"
)

type Drawing struct {
	Dao *dao.DrawingDao
}

func NewDrawing() *Drawing {
	return &Drawing{
		Dao: dao.NewDrawingDao(),
	}
}

func (drawing *Drawing) setPassword(model *models.DrawingModel, password string) error {
	if *model.AlbumId == PublicAlbumID && password != "" {
		if password, err := utils.HashPassword(password); err != nil {
			return err
		} else {
			model.Password = &password
		}
	}
	return nil
}

func (drawing *Drawing) Create(request *classes.DrawingCreateRequest) (*models.DrawingModel, error) {
	model := &models.DrawingModel{
		Name:    request.Name,
		OwnerId: request.OwnerId,
		AlbumId: &request.AlbumId,
	}
	if err := drawing.setPassword(model, request.Password); err != nil {
		return nil, err
	}
	err := drawing.Dao.Create(&model)

	return model, err
}

func (drawing *Drawing) Update(request *classes.DrawingUpdateRequest) (*models.DrawingModel, error) {
	model := &models.DrawingModel{
		Name:     request.Name,
		AlbumId:  &request.AlbumId,
		Password: nil,
	}
	if err := drawing.setPassword(model, request.Password); err != nil {
		return nil, err
	}
	record := &models.DrawingModel{}
	record.Model = gorm.Model{ID: request.ID}
	err := drawing.Dao.Update(record, model)

	return model, err
}

func (drawing *Drawing) Get(id uint) (*models.DrawingModel, error) {
	model := &models.DrawingModel{}
	err := drawing.Dao.Get(id, model)
	return model, err
}

func (drawing *Drawing) GetAll(albumId uint, filter *classes.DrawingsFilterRequest) (*[]dto.DrawingDTO, error) {
	return drawing.Dao.GetAll(albumId, filter)
}

func (drawing *Drawing) Delete(id uint) error {
	err := drawing.Dao.Delete(id, &models.DrawingModel{})

	return err
}
