package entities

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/classes"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dao"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
)

type Session struct {
	Dao *dao.SessionDao
}

func NewSession() *Session {
	return &Session{
		Dao: dao.NewSessionDao(),
	}
}

func (session *Session) Create(request *classes.SessionCreateRequest) (*models.SessionModel, error) {
	model := models.SessionModel{
		UserID: request.UserID,
	}
	session.Dao.InvalidateUserSession(request.UserID)
	err := session.Dao.Create(&model)

	return &model, err
}

func (session *Session) Get(sessionQuery *models.SessionModel) (*models.SessionModel, error) {
	result, err := session.Dao.Get(sessionQuery)

	return result, err
}
