package entities

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dao"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dto"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
)

type CollaborationSession struct {
	Dao *dao.CollaborationSessionDao
}

func NewCollaborationSession() *CollaborationSession {
	return &CollaborationSession{
		Dao: dao.NewCollaborationSessionDao(),
	}
}

func (session *CollaborationSession) Create(model *models.CollaborationSessionModel) error {
	return session.Dao.Create(&model)
}

func (session *CollaborationSession) GetUserSessions(userId uint) (*[]dto.CollaborationSessionDTO, error) {
	return session.Dao.GetSessions(userId)
}
