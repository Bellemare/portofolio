package entities

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/classes"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dao"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/utils"
	"gorm.io/gorm"
)

type User struct {
	Dao *dao.UserDao
}

func NewUser() *User {
	return &User{
		Dao: dao.NewUserDao(),
	}
}

func (user *User) Create(request *classes.UserCreateRequest) (*models.UserModel, error) {
	hashedPassword, err := utils.HashPassword(request.Password)
	if err != nil {
		return nil, err
	}

	model := models.UserModel{
		Email:     request.Email,
		Pseudonym: request.Pseudonym,
		Password:  hashedPassword,
	}
	err = user.Dao.Create(&model)

	return &model, err
}

func (user *User) Update(request *classes.UserUpdateRequest) (*models.UserModel, error) {
	model := &models.UserModel{
		Pseudonym:     request.Pseudonym,
		IsPublicEmail: request.IsPublicEmail,
	}
	record := &models.UserModel{}
	record.Model = gorm.Model{ID: request.ID}
	err := user.Dao.Update(record, model)

	return model, err
}

func (user *User) Get(userQuery *models.UserModel) (*models.UserModel, error) {
	result, err := user.Dao.Get(userQuery)

	return result, err
}
