package entities

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/classes"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dao"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dto"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
)

type Exposition struct {
	Dao *dao.ExpositionDao
}

func NewExposition() *Exposition {
	return &Exposition{
		Dao: dao.NewExpositionDao(),
	}
}

func (album *Exposition) Create(request *classes.ExpositionAddRequest) (*models.DrawingExpositionModel, error) {
	model := models.DrawingExpositionModel{
		AlbumID:   request.AlbumID,
		DrawingID: request.DrawingID,
	}
	err := album.Dao.Create(&model)

	return &model, err
}

func (album *Exposition) GetAll() (*[]dto.AlbumDTO, error) {
	return album.Dao.GetAll()
}

func (album *Exposition) Get(albumId uint) (*[]dto.DrawingDTO, error) {
	return album.Dao.Get(albumId)
}

func (album *Exposition) Delete(exposition *models.DrawingExpositionModel) error {
	err := album.Dao.RemoveDrawingFromExposition(exposition)

	return err
}
