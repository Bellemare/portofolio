package entities

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/classes"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dao"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
	"gorm.io/gorm"
)

const (
	PublicAlbumID = 0
)

type Album struct {
	Dao             *dao.AlbumDao
	MembersDao      *dao.AlbumMemberDao
	JoinRequestsDao *dao.AlbumJoinRequestDao
}

func NewAlbum() *Album {
	return &Album{
		Dao:             dao.NewAlbumDao(),
		MembersDao:      dao.NewAlbumMemberDao(),
		JoinRequestsDao: dao.NewAlbumJoinRequestDao(),
	}
}

func (album *Album) Create(request *classes.AlbumCreateRequest) (*models.AlbumModel, error) {
	model := models.AlbumModel{
		Name:        request.Name,
		OwnerId:     request.OwnerId,
		Description: request.Description,
	}
	err := album.Dao.Create(&model)

	return &model, err
}

func (album *Album) GetAll(userId uint) (*[]models.AlbumModel, error) {
	return album.Dao.GetAll(userId)
}

func (album *Album) Get(id uint) (*models.AlbumModel, error) {
	model := &models.AlbumModel{}
	err := album.Dao.Get(id, model)
	return model, err
}

func (album *Album) Update(request *classes.AlbumUpdateRequest) (*models.AlbumModel, error) {
	model := &models.AlbumModel{
		Name:        request.Name,
		Description: request.Description,
	}
	record := &models.AlbumModel{}
	record.Model = gorm.Model{ID: request.ID}
	err := album.Dao.Update(record, model)

	return model, err
}

func (album *Album) Delete(id uint) error {
	err := album.Dao.Delete(id, &models.AlbumModel{})
	if err == nil {
		album.MembersDao.DeleteAlbum(id)
		album.JoinRequestsDao.DeleteAlbum(id)
	}

	return err
}
