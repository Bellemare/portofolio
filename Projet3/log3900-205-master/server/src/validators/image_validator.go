package validators

import (
	"reflect"

	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/utils"
)

func IsBase64ImageType(value reflect.Value, field reflect.StructField) (bool, error) {
	val := value.String()
	if val == "" {
		return true, nil
	}

	_, err := utils.Base64ImageDecode(val)
	isValid := err == nil

	if !isValid {
		err = engine.NewError("L'image " + engine.GetFieldLabel(field) + " doit être de type png ou jpeg")
	}
	return isValid, err
}
