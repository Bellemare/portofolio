package validators

import (
	"errors"
	"fmt"
	"reflect"
	"strings"

	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
)

func IsValidMessageLength(value reflect.Value, field reflect.StructField) (bool, error) {
	val := value.String()

	return validateLength(val, 1, 240, field)
}

func IsValidDescriptionLength(value reflect.Value, field reflect.StructField) (bool, error) {
	val := value.String()

	return validateLength(val, 0, 250, field)
}

func IsValidNameLength(value reflect.Value, field reflect.StructField) (bool, error) {
	val := value.String()

	return validateLength(val, 1, 50, field)
}

func IsValidShortNameLength(value reflect.Value, field reflect.StructField) (bool, error) {
	val := value.String()

	return validateLength(val, 1, 20, field)
}

func validateLength(val string, minLength int, maxLength int, field reflect.StructField) (bool, error) {
	length := len(strings.TrimSpace(val))
	if length < minLength || length > maxLength {
		err := "Le champ " + engine.GetFieldLabel(field) + " doit avoir une longueur entre " + fmt.Sprintf("%d", minLength) + " et " + fmt.Sprintf("%d", maxLength) + " caractères"
		return false, errors.New(err)
	}

	return true, nil
}
