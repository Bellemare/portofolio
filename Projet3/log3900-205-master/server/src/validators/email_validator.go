package validators

import (
	"errors"
	"reflect"
	"regexp"

	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
)

func IsValidEmail(value reflect.Value, field reflect.StructField) (bool, error) {
	val := value.String()
	ok, err := regexp.Match("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$", []byte(val))

	if !ok || err != nil {
		return false, errors.New("Le format du courriel du champ " + engine.GetFieldLabel(field) + " est invalide")
	}

	return true, nil
}
