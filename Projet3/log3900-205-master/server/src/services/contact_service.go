package services

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/classes"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dto"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/entities"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
)

type ContactService struct{}

func NewContactService() *ContactService {
	return &ContactService{}
}

func (service *ContactService) HasContact(req *models.ContactModel) bool {
	return entities.NewContact().Dao.HasContact(req)
}

func (service *ContactService) ContactExists(contactId uint) bool {
	return entities.NewUser().Dao.ExistsById(contactId, &models.UserModel{})
}

func (service *ContactService) validateContactAdd(request *classes.ContactAddRequest) engine.Error {
	if ok := service.ContactExists(request.ContactId); !ok {
		return engine.NewError("Aucun utilisateur existe avec cet identifiant")
	}
	if request.ContactId == request.UserId {
		return engine.NewError("Impossible de s'ajouter soi-même dans les contacts")
	}
	model := &models.ContactModel{
		UserId:    request.UserId,
		ContactId: request.ContactId,
	}
	if ok := service.HasContact(model); ok {
		return engine.NewError("L'utilisateur est déjà dans les contacts")
	}

	return nil
}

func (service *ContactService) AddContact(request *classes.ContactAddRequest) (*models.ContactModel, engine.Error) {
	if err := service.validateContactAdd(request); err != nil {
		return nil, err
	}
	contact := entities.NewContact()
	model, err := contact.AddContact(request)
	if err != nil {
		return nil, engine.NewError("Le contact n'a pas pu être créé")
	}

	return model, nil
}

func (service *ContactService) GetUserList(userId uint) (*[]dto.UserDTO, engine.Error) {
	contact := entities.NewContact()
	users, err := contact.Dao.GetNonContacts(userId)
	if err != nil {
		return nil, engine.NewError("Impossible de récupérer la liste d'utilisateurs")
	}

	return users, nil
}

func (service *ContactService) GetUserContacts(userId uint) (*[]dto.ContactDTO, engine.Error) {
	contact := entities.NewContact()
	contacts, err := contact.GetAll(userId)
	if err != nil {
		return nil, engine.NewError("Impossible de récupérer la liste de contacts")
	}

	return contacts, nil
}

func (service *ContactService) RemoveContact(request *models.ContactModel) engine.Error {
	if ok := service.HasContact(request); !ok {
		return engine.NewError("L'utilisateur n'est pas dans les contacts")
	}
	contact := entities.NewContact()
	if err := contact.RemoveContact(request); err != nil {
		return engine.NewError("Le contact n'a pas pu être créé")
	}

	return nil
}
