package services

import (
	"errors"
	"os"
	"strconv"
	"time"

	"github.com/golang-jwt/jwt/v4"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/classes"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dao"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dto"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/entities"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/utils"
)

type AuthService struct {
	*UserService
}

func NewAuthService() *AuthService {
	return &AuthService{
		UserService: NewUserService(),
	}
}

func tokenValidator(token *jwt.Token) (interface{}, error) {
	secret := os.Getenv("TOKEN_SECRET")
	if secret == "" {
		return "", errors.New("impossible de vérifier la validité de la clé")
	}
	if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
		return nil, errors.New("clé invalide")
	}

	return []byte(secret), nil
}

func (service *AuthService) DisconnectUser(token *engine.AuthTokenPayload) error {
	if token == nil {
		return errors.New("la requête n'a pas pu être traitée correctement")
	}
	dao := dao.NewSessionDao()
	err := dao.InvalidateUserSession(token.UserID)
	if err != nil {
		return errors.New("erreur en invalidant la session")
	}
	return nil
}

func (service *AuthService) ValidateToken(tokenStr string) (*engine.AuthTokenPayload, error) {
	token, err := jwt.Parse(tokenStr, tokenValidator)
	if err != nil {
		return nil, err
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		payload := engine.AuthTokenPayload{}
		engine.ParseModelToDTO(&claims, &payload)
		return &payload, nil
	}

	return nil, errors.New("clé invalide")
}

func (service *AuthService) CreateUserSession(user *models.UserModel) (*dto.AuthResponseDTO, error) {
	req := classes.SessionCreateRequest{UserID: user.ID}
	session := entities.NewSession()
	res, err := session.Create(&req)
	if err != nil {
		return nil, engine.NewError("Impossible de connecter l'utilisateur")
	}
	expiration := time.Now().Add(time.Hour * 12)
	token, err := service.createToken(user, expiration, res.ID)

	return &dto.AuthResponseDTO{
		UserID:    user.ID,
		Token:     token,
		ExpiresAt: expiration,
	}, err
}

func (service *AuthService) VerifyUserInfo(request *classes.AuthRequest) (*models.UserModel, engine.Error) {
	user, err := service.GetUserByEmail(request.Email)
	if err != nil {
		utils.HashPassword("random password")
		return nil, engine.NewError("Le mot de passe ou le courriel n'est pas valide")
	}
	ok := utils.VerifyPassword(request.Password, user.Password)
	if !ok {
		return nil, engine.NewError("Le mot de passe ou le courriel n'est pas valide")
	}

	return user, nil
}

func (service *AuthService) createToken(user *models.UserModel, expiration time.Time, id uint) (string, error) {
	secret := os.Getenv("TOKEN_SECRET")
	if secret == "" {
		return "", errors.New("impossible de créer la clé d'authentification")
	}

	payload := engine.AuthTokenPayload{UserID: user.ID}
	payload.StandardClaims = jwt.StandardClaims{
		ExpiresAt: expiration.Unix(),
		Id:        strconv.FormatUint(uint64(id), 10),
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, payload)

	tokenString, err := token.SignedString([]byte(secret))
	if err != nil {
		return "", errors.New("erreur en créant la clé d'authentification")
	}

	return tokenString, nil
}
