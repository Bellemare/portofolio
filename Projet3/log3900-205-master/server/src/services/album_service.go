package services

import (
	"strings"

	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/classes"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dto"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/entities"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/validators"
)

type AlbumService struct{}

func NewAlbumService() *AlbumService {
	return &AlbumService{}
}

func (service *AlbumService) IsAlbumOwner(id uint, userId uint) (bool, engine.Error) {
	album := entities.NewAlbum()

	albumModel, err := album.Get(id)
	if err != nil {
		return false, engine.NewError("Album inexistant")
	}

	return albumModel.OwnerId == userId, nil
}

func (service *AlbumService) AlbumExists(id uint) bool {
	album := entities.NewAlbum()
	return album.Dao.ExistsById(id, &models.AlbumModel{})
}

func (service *AlbumService) IsAlbumMember(req *models.AlbumMemberModel) (bool, engine.Error) {
	album := entities.NewAlbum()
	if exists := service.AlbumExists(req.AlbumId); !exists {
		return false, engine.NewError("L'album est inexistant")
	}

	isMember := album.MembersDao.IsAlbumMember(req)
	return isMember, nil
}

func (service *AlbumService) validateRequest(req interface{}) engine.Error {
	validator := engine.NewValidator()
	validator.RegisterValidation("validLength", validators.IsValidNameLength)
	validator.RegisterValidation("validDescription", validators.IsValidDescriptionLength)
	if err := validator.ValidateModel(req); err != nil {
		return err
	}

	return nil
}

func (service *AlbumService) CreateAlbum(req *classes.AlbumCreateRequest) (*models.AlbumModel, engine.Error) {
	req.Name = strings.TrimSpace(req.Name)
	if err := service.validateRequest(req); err != nil {
		return nil, err
	}

	album := entities.NewAlbum()
	exists := album.Dao.ExistsByName(req.Name)
	if exists {
		return nil, engine.NewFieldsError([]string{"Un album existe avec le même nom"}, []string{models.AlbumName})
	}
	return album.Create(req)
}

func (service *AlbumService) UpdateAlbum(req *classes.AlbumUpdateRequest) (*models.AlbumModel, engine.Error) {
	req.Name = strings.TrimSpace(req.Name)
	if err := service.validateRequest(req); err != nil {
		return nil, err
	}

	album := entities.NewAlbum()
	albumModel, err := album.Get(req.ID)
	if err != nil {
		return nil, engine.NewError("Album inexistant")
	}
	exists := album.Dao.ExistsByName(req.Name)
	if exists && albumModel.Name != req.Name {
		return nil, engine.NewFieldsError([]string{"Un album existe avec le même nom"}, []string{models.AlbumName})
	}
	return album.Update(req)
}

func (service *AlbumService) validateJoinRequest(req *models.AlbumJoinRequestModel) engine.Error {
	member := &models.AlbumMemberModel{
		AlbumId: req.AlbumId,
		UserId:  req.UserId,
	}
	if isMember, err := service.IsAlbumMember(member); err != nil || isMember {
		if err == nil {
			err = engine.NewError("L'utilisateur est déjà membre de l'album")
		}
		return err
	}
	if entities.NewAlbum().JoinRequestsDao.HasPendingRequest(req) {
		return engine.NewError("L'Utilisateur a déjà une requête active pour joindre cet album")
	}

	return nil
}

func (service *AlbumService) RequestJoinAlbum(req *models.AlbumJoinRequestModel) engine.Error {
	if err := service.validateJoinRequest(req); err != nil {
		return err
	}

	if err := entities.NewAlbum().JoinRequestsDao.Create(req); err != nil {
		return engine.NewError("Impossible de créer la demande")
	}
	return nil
}

func (service *AlbumService) getAcceptRequest(requestId uint) (*models.AlbumJoinRequestModel, engine.Error) {
	album := entities.NewAlbum()
	var req *models.AlbumJoinRequestModel
	if err := album.JoinRequestsDao.Get(requestId, &req); err != nil {
		return nil, engine.NewError("Aucune demande n'existe avec cet identifiant")
	}

	member := &models.AlbumMemberModel{
		AlbumId: req.AlbumId,
		UserId:  req.UserId,
	}
	if isMember, err := service.IsAlbumMember(member); err != nil || isMember {
		if err == nil {
			err = engine.NewError("L'utilisateur est déjà membre de l'album")
		}
		return nil, err
	}

	return req, nil
}

func (service *AlbumService) AcceptJoinRequest(requestId uint) (*models.AlbumJoinRequestModel, engine.Error) {
	req, err := service.getAcceptRequest(requestId)
	if err != nil {
		return nil, err
	}
	album := entities.NewAlbum()
	if err := album.JoinRequestsDao.DeleteUserRequest(req); err != nil {
		return nil, engine.NewError("Impossible de traiter la demande")
	}

	member := &models.AlbumMemberModel{
		AlbumId: req.AlbumId,
		UserId:  req.UserId,
	}
	if err := album.MembersDao.AddUser(member); err != nil {
		return nil, engine.NewError("La requête a échouée")
	}

	return req, nil
}

func (service *AlbumService) DeclineJoinRequest(requestId uint) (*models.AlbumJoinRequestModel, engine.Error) {
	album := entities.NewAlbum()
	var req *models.AlbumJoinRequestModel
	if err := album.JoinRequestsDao.Get(requestId, &req); err != nil {
		return nil, engine.NewError("Aucune demande n'existe avec cet identifiant")
	}

	if err := album.JoinRequestsDao.DeleteUserRequest(req); err != nil {
		return nil, engine.NewError("Erreur en supprimant la demande de l'utilisateur")
	}

	return req, nil
}

func (service *AlbumService) GetPendingJoinRequests(albumId uint) (*[]dto.AlbumJoinRequestDTO, engine.Error) {
	album := entities.NewAlbum()
	requests, err := album.JoinRequestsDao.GetAlbumPendingRequests(albumId)
	if err != nil {
		return nil, engine.NewError("Impossible de récupérer les demandes pour cet album")
	}

	return requests, nil
}

func (service *AlbumService) GetAllPendingJoinRequests(userId uint) (*[]dto.AlbumJoinRequestDTO, engine.Error) {
	album := entities.NewAlbum()
	requests, err := album.JoinRequestsDao.GetAllAlbumPendingRequests(userId)
	if err != nil {
		return nil, engine.NewError("Impossible de récupérer les demandes")
	}

	return requests, nil
}

func (service *AlbumService) AddAlbumMember(req *models.AlbumMemberModel) engine.Error {
	if isMember, err := service.IsAlbumMember(req); err != nil || isMember {
		if err == nil {
			err = engine.NewError("L'utilisateur est déjà membre de l'album")
		}
		return err
	}
	if err := entities.NewAlbum().MembersDao.Create(req); err != nil {
		return engine.NewError("Impossible d'ajouter l'utilisateur à l'album")
	}

	return nil
}

func (service *AlbumService) LeaveAlbum(req *models.AlbumMemberModel) engine.Error {
	if isMember, err := service.IsAlbumMember(req); err != nil || !isMember {
		if err == nil {
			err = engine.NewError("L'utilisateur n'est pas membre de l'album")
		}
		return err
	}
	if err := entities.NewAlbum().MembersDao.RemoveUser(req); err != nil {
		return engine.NewError("Impossible retirer l'utilisateur de l'album")
	}
	if album, err := entities.NewAlbum().Get(req.AlbumId); err == nil {
		drawing := entities.NewDrawing()
		drawing.Dao.TransferOwnership(&models.DrawingModel{
			OwnerId: req.UserId,
			AlbumId: &req.AlbumId,
		}, album.OwnerId)
	}

	return nil
}

func (service *AlbumService) GetUserAlbumsList(userId uint) (*[]models.AlbumModel, engine.Error) {
	album := entities.NewAlbum()
	albums, err := album.MembersDao.GetUserAlbumList(userId)
	if err != nil {
		return nil, engine.NewError("Impossible de récupérer la liste d'albums")
	}
	userAlbums := append([]models.AlbumModel{*models.GetPublicAlbum()}, *albums...)

	return &userAlbums, nil
}

func (service *AlbumService) GetAlbums(userId uint) (*[]models.AlbumModel, error) {
	album := entities.NewAlbum()
	return album.GetAll(userId)
}

func (service *AlbumService) GetAlbum(id uint) (*models.AlbumModel, error) {
	album := entities.NewAlbum()
	return album.Get(id)
}

func (service *AlbumService) DeleteAlbum(id uint) error {
	album := entities.NewAlbum()
	return album.Delete(id)
}
