package services

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dto"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/entities"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
)

type CollaborationSessionService struct{}

func NewCollaborationSessionService() *CollaborationSessionService {
	return &CollaborationSessionService{}
}

func (service *CollaborationSessionService) Create(model *models.CollaborationSessionModel) engine.Error {
	session := entities.NewCollaborationSession()

	if err := session.Create(model); err != nil {
		err = engine.NewError("Impossible de sauvegarder l'historique de la session collaborative")
	}

	return nil
}

func (service *CollaborationSessionService) GetUserSessions(userId uint) (*[]dto.CollaborationSessionDTO, engine.Error) {
	session := entities.NewCollaborationSession()

	sessions, err := session.GetUserSessions(userId)
	if err != nil {
		return nil, engine.NewError("Impossible de récupérer l'historique de sessions collaborative")
	}

	return sessions, nil
}
