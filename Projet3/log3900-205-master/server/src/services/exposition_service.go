package services

import (
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/classes"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dto"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/entities"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
)

type ExpositionService struct{}

func NewExpositionService() *ExpositionService {
	return &ExpositionService{}
}

func (service *ExpositionService) validateAddRequest(request *classes.ExpositionAddRequest) engine.Error {
	if request.AlbumID == entities.PublicAlbumID {
		return engine.NewError("Impossible d'ajouter des dessins de l'album public à une exposition")
	}
	if _, err := NewDrawingService().Get(request.DrawingID); err != nil {
		return err
	}
	model := &models.DrawingExpositionModel{
		AlbumID:   request.AlbumID,
		DrawingID: request.DrawingID,
	}
	if entities.NewExposition().Dao.IsAlreadyInExposition(model) {
		return engine.NewError("Le dessin est déjà dans l'exposition")
	}

	return nil
}

func (service *ExpositionService) AddDrawingToExposition(request *classes.ExpositionAddRequest) engine.Error {
	if err := service.validateAddRequest(request); err != nil {
		return err
	}

	exposition := entities.NewExposition()
	_, err := exposition.Create(request)

	if err != nil {
		err = engine.NewError("Impossible d'ajouter le dessin à l'exposition")
	}

	return err
}

func (service *ExpositionService) GetContactExpositions(userId uint) (*[]dto.AlbumDTO, engine.Error) {
	exposition := entities.NewExposition()
	expositions, err := exposition.Dao.GetUserExpositions(userId)

	if err != nil {
		err = engine.NewError("Impossible les expositions du contact")
	}

	return expositions, err
}

func (service *ExpositionService) GetExpositions() (*[]dto.AlbumDTO, engine.Error) {
	exposition := entities.NewExposition()
	expositions, err := exposition.GetAll()

	if err != nil {
		err = engine.NewError("Impossible de récupérer la liste d'expositions")
	}

	return expositions, err
}

func (service *ExpositionService) GetExposition(albumId uint) (*[]dto.DrawingDTO, engine.Error) {
	if !NewAlbumService().AlbumExists(albumId) {
		return nil, engine.NewError("L'album est inexistant")
	}
	exposition := entities.NewExposition()
	drawings, err := exposition.Get(albumId)

	if err != nil {
		err = engine.NewError("Impossible de récupérer l'exposition de dessins")
	}

	return drawings, err
}

func (service *ExpositionService) RemoveDrawingFromExposition(drawingExposition *models.DrawingExpositionModel) engine.Error {
	if _, err := NewDrawingService().Get(drawingExposition.DrawingID); err != nil {
		return err
	}

	exposition := entities.NewExposition()
	err := exposition.Delete(drawingExposition)

	if err != nil {
		err = engine.NewError("Impossible de retirer le dessin de l'exposition")
	}

	return err
}
