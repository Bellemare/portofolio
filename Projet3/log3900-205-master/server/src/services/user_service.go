package services

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/classes"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dao"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dto"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/entities"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/utils"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/validators"
	"gorm.io/gorm"
)

const (
	AvatarsFolder = "/var/www/avatars"
	DefaultAvatar = "blue.png"
)

type UserService struct{}

func NewUserService() *UserService {
	return &UserService{}
}

func (service *UserService) validateUserCreation(request *classes.UserCreateRequest) engine.Error {
	validator := engine.NewValidator()
	validator.RegisterValidation("email", validators.IsValidEmail)
	validator.RegisterValidation("validShortName", validators.IsValidShortNameLength)
	validator.RegisterValidation("image", validators.IsBase64ImageType)
	if err := validator.ValidateModel(request); err != nil {
		return err
	}
	if ok, err := service.isValidNewPassword(request); !ok {
		return err
	}
	if err := service.validateUniqueUser(request); err != nil {
		return err
	}
	return nil
}

func (service *UserService) validateUserUpdate(request *classes.UserUpdateRequest) engine.Error {
	validator := engine.NewValidator()
	validator.RegisterValidation("validShortName", validators.IsValidShortNameLength)
	validator.RegisterValidation("image", validators.IsBase64ImageType)
	if err := validator.ValidateModel(request); err != nil {
		return err
	}
	user := entities.NewUser()
	userModel := &models.UserModel{}
	userModel.Model = gorm.Model{ID: request.ID}
	currentUser, err := user.Get(userModel)
	if err != nil {
		return engine.NewError("Utilisateur inexistant")
	}
	if currentUser.Pseudonym != request.Pseudonym && user.Dao.UserExistsByPseudonym(request.Pseudonym) {
		return engine.NewFieldsError([]string{"Un utilisateur avec le même pseudonyme existe déjà"}, []string{models.UserPseudonym})
	}
	return nil
}

func (service *UserService) createAvatarFolder() bool {
	return os.MkdirAll(AvatarsFolder, os.ModePerm) == nil
}

func (service *UserService) getUserAvatarFileName(userId uint, mimeType string) string {
	return AvatarsFolder + fmt.Sprintf("/user-%d.%s", userId, strings.ReplaceAll(mimeType, "image/", ""))
}

func (service *UserService) saveUserAvatar(userId uint, base64Avatar string) engine.Error {
	if base64Avatar == "" {
		return nil
	}
	data, err := utils.Base64ImageDecode(base64Avatar)
	if err != nil {
		return engine.NewError("Impossible de sauvegarder l'avatar")
	}

	mimeType := http.DetectContentType(data)
	service.createAvatarFolder()
	file, _ := os.Create(service.getUserAvatarFileName(userId, mimeType))

	if _, err := file.Write(data); err != nil {
		return err
	}
	file.Close()

	return nil
}

func (service *UserService) CreateUser(request *classes.UserCreateRequest) (*models.UserModel, engine.Error) {
	request.Pseudonym = strings.TrimSpace(request.Pseudonym)
	if err := service.validateUserCreation(request); err != nil {
		return nil, err
	}

	user := entities.NewUser()
	model, err := user.Create(request)
	if err == nil {
		service.saveUserAvatar(model.ID, request.Avatar)
	}

	if err != nil {
		return nil, engine.NewError("L'utilisateur n'a pas pu être créé")
	}

	return model, nil
}

func (service *UserService) UpdateUser(request *classes.UserUpdateRequest) (*models.UserModel, engine.Error) {
	request.Pseudonym = strings.TrimSpace(request.Pseudonym)
	if err := service.validateUserUpdate(request); err != nil {
		return nil, err
	}
	if err := service.saveUserAvatar(request.ID, request.Avatar); err != nil {
		return nil, err
	}

	user := entities.NewUser()
	model, err := user.Update(request)
	if err != nil {
		return nil, engine.NewError("Impossible de modifier l'utilisateur")
	}
	return model, nil
}

func (service *UserService) GetUserAvatar(userId uint) string {
	jpegFileName := service.getUserAvatarFileName(userId, utils.MimeJpeg)
	pngFileName := service.getUserAvatarFileName(userId, utils.MimePng)

	if file, err := ioutil.ReadFile(jpegFileName); err == nil {
		return utils.ByteImageToBase64(file)
	} else {
		if file, err := ioutil.ReadFile(pngFileName); err == nil {
			return utils.ByteImageToBase64(file)
		}
	}

	return service.GetDefaultAvatar()
}

func (service *UserService) GetDefaultAvatar() string {
	defaultFileName := AvatarsFolder + "/" + DefaultAvatar

	if file, err := ioutil.ReadFile(defaultFileName); err == nil {
		return utils.ByteImageToBase64(file)
	}

	return ""
}

func (service *UserService) GetDefaultAvatars() []string {
	defaults := []string{"blue.png", "orange.png", "red.png", "purple.png", "green.png", "yellow.png"}

	avatars := []string{}
	for _, name := range defaults {
		if file, err := ioutil.ReadFile(AvatarsFolder + "/" + name); err == nil {
			avatars = append(avatars, utils.ByteImageToBase64(file))
		}
	}

	return avatars
}

func (service *UserService) GetConnectionHistory(userId uint) (*[]dto.ConnectionHistoryEntryDTO, engine.Error) {
	session := entities.NewSession()
	history, err := session.Dao.GetUserHistory(userId)
	if err != nil {
		return nil, engine.NewError("Impossible de récupérer l'historique de connexions")
	}

	return history, nil
}

func (service *UserService) GetUserMetrics(userId uint) (*dto.UserMetricsDTO, engine.Error) {
	collaboration := entities.NewCollaborationSession()
	history, err := collaboration.Dao.GetAllSessions(userId)
	if err != nil {
		return nil, engine.NewError("Impossible de récupérer les métriques")
	}

	totalTime := 0
	nCollaborated := 0
	cache := make(map[uint]uint)
	for _, session := range *history {
		diff := session.EndTime.Sub(session.StartTime)
		totalTime += int(diff.Seconds())
		if _, inCache := cache[session.DrawingID]; session.EditedDrawing && !inCache {
			nCollaborated++
			cache[session.DrawingID] = session.DrawingID
		}
	}

	userAlbumsCount := entities.NewAlbum().Dao.MemberAlbumsCount(userId)
	ownedDrawingsCount := entities.NewDrawing().Dao.GetUserDrawingsOwnedCount(userId)
	average := 0
	if len(*history) > 0 {
		average = totalTime / len(*history)
	}

	return &dto.UserMetricsDTO{
		TotalCollaborationTime:   totalTime,
		AverageCollaborationTime: average,
		CollaboratedDrawings:     nCollaborated,
		MemberAlbums:             userAlbumsCount,
		OwnedDrawings:            ownedDrawingsCount,
	}, nil
}

func (service *UserService) GetUser(id uint) (*models.UserModel, error) {
	userQuery := &models.UserModel{
		Model: gorm.Model{ID: id},
	}

	user := entities.NewUser()
	result, err := user.Get(userQuery)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func (service *UserService) GetUserByEmail(email string) (*models.UserModel, error) {
	userQuery := &models.UserModel{Email: email}

	user := entities.NewUser()
	result, err := user.Get(userQuery)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func (service *UserService) GetAll() (*[]dto.UserDTO, engine.Error) {
	user := entities.NewUser()

	users, err := user.Dao.GetAll()
	if err != nil {
		err = engine.NewError("Impossible de récupérer la liste d'utilisateurs")
	}

	return users, err
}

func (service *UserService) isValidNewPassword(request *classes.UserCreateRequest) (bool, *engine.FieldErrorResponse) {
	if request.Password != request.PasswordRepeat {
		return false, engine.NewFieldsError([]string{"Les mots de passes ne sont pas identiques"}, []string{models.UserPassword, "password_repeat"})
	}

	return true, nil
}

func (service *UserService) validateUniqueUser(request *classes.UserCreateRequest) *engine.FieldErrorResponse {
	userDao := dao.NewUserDao()
	if userDao.UserExistsByEmail(request.Email) {
		return engine.NewFieldsError([]string{"Un utilisateur avec le même courriel existe déjà"}, []string{models.UserEmail})
	}
	if userDao.UserExistsByPseudonym(request.Pseudonym) {
		return engine.NewFieldsError([]string{"Un utilisateur avec le même pseudonyme existe déjà"}, []string{models.UserPseudonym})
	}

	return nil
}
