package services

import (
	"fmt"
	"image"
	"image/png"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/classes"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dto"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/entities"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/utils"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/validators"
)

const (
	DrawingsFolder = "/var/www/images"
	DrawingWidth   = 1200
	DrawingHeight  = 720
)

type DrawingService struct{}

func NewDrawingService() *DrawingService {
	return &DrawingService{}
}

func (service *DrawingService) IsDrawingOwner(id uint, userId uint) (bool, engine.Error) {
	drawing := entities.NewDrawing()

	drawingModel, err := drawing.Get(id)
	if err != nil {
		return false, engine.NewError("Dessin inexistant")
	}

	return drawingModel.OwnerId == userId, nil
}

func (service *DrawingService) ValidateAlbum(albumId uint) engine.Error {
	if albumId == entities.PublicAlbumID {
		return nil
	}

	if exists := NewAlbumService().AlbumExists(albumId); !exists {
		return engine.NewError("Album inexistant")
	}

	return nil
}

func (service *DrawingService) validateRequest(albumId uint, req interface{}) engine.Error {
	validator := engine.NewValidator()
	validator.RegisterValidation("validLength", validators.IsValidNameLength)
	if err := validator.ValidateModel(req); err != nil {
		return err
	}
	if err := service.ValidateAlbum(albumId); err != nil {
		return err
	}

	return nil
}

func (service *DrawingService) Create(req *classes.DrawingCreateRequest) (*models.DrawingModel, engine.Error) {
	req.Name = strings.TrimSpace(req.Name)
	if err := service.validateRequest(req.AlbumId, req); err != nil {
		return nil, err
	}

	drawing := entities.NewDrawing()
	drawingModel, err := drawing.Create(req)
	if err == nil {
		service.CreateDefaultImage(drawingModel.ID, req)
	}

	return drawingModel, err
}

func (service *DrawingService) VerifyPassword(id uint, password string) (bool, engine.Error) {
	drawing, err := service.Get(id)
	if err != nil {
		return false, err
	}
	if drawing.Password == nil {
		return true, nil
	}

	ok := utils.VerifyPassword(password, *drawing.Password)
	if !ok {
		err = engine.NewError("Le mot de passe est invalide")
	}
	return ok, err
}

func (service *DrawingService) Update(req *classes.DrawingUpdateRequest) (*models.DrawingModel, engine.Error) {
	req.Name = strings.TrimSpace(req.Name)
	if err := service.validateRequest(req.AlbumId, req); err != nil {
		return nil, err
	}

	drawing := entities.NewDrawing()
	return drawing.Update(req)
}

func (service *DrawingService) SmartFilter(filter *classes.DrawingsSmartFilter) (*[]dto.DrawingDTO, engine.Error) {
	drawing := entities.NewDrawing()
	drawings, err := drawing.Dao.SmartFilter(filter)
	if err != nil {
		return nil, engine.NewError("Impossible de récupérer les dessins")
	}

	return drawings, nil
}

func (service *DrawingService) GetAll(albumId uint, filter *classes.DrawingsFilterRequest) (*[]dto.DrawingDTO, engine.Error) {
	drawing := entities.NewDrawing()
	drawings, err := drawing.GetAll(albumId, filter)
	if err != nil {
		return nil, engine.NewError("Impossible de récupérer les dessins")
	}

	return drawings, nil
}

func (service *DrawingService) Get(id uint) (*models.DrawingModel, engine.Error) {
	drawing := entities.NewDrawing()
	drawingModel, err := drawing.Get(id)
	if err != nil {
		return nil, engine.NewError("Le dessin n'existe pas")
	}

	return drawingModel, nil
}

func (service *DrawingService) Delete(id uint) engine.Error {
	drawing := entities.NewDrawing()
	if err := drawing.Delete(id); err != nil {
		return engine.NewError("Impossible de supprimer le dessin")
	}

	return nil
}

func (service *DrawingService) GetDrawingsFolderName(drawingId uint) string {
	return filepath.Join(DrawingsFolder, fmt.Sprintf("drawing-%d", drawingId))
}

func (service *DrawingService) GetDrawingImageFileName(drawingId uint, imageNb int, mime string) (string, engine.Error) {
	if mime != utils.MimePng && mime != utils.MimeJpeg {
		return "", engine.NewError("Type d'image invalide")
	}
	fileType := strings.ReplaceAll(mime, "image/", "")

	return service.GetDrawingsFolderName(drawingId) + fmt.Sprintf("/%d.%s", imageNb, fileType), nil
}

func (service *DrawingService) GetLatestDrawingImage(drawingId uint) ([]byte, engine.Error) {
	count := service.GetDrawingImagesCount(drawingId)
	if count == 0 {
		return nil, engine.NewError("Le dessin n'a pas d'image sauvegardée")
	}
	drawingFileName, _ := service.GetDrawingImageFileName(drawingId, count, utils.MimePng)
	file, err := ioutil.ReadFile(drawingFileName)
	if err != nil {
		return nil, err
	}

	return file, nil
}

func (service *DrawingService) GetDrawingImagesCount(drawingId uint) int {
	if files, err := ioutil.ReadDir(service.GetDrawingsFolderName(drawingId)); err == nil {
		return len(files)
	}

	return 0
}

func (service *DrawingService) CreateDrawingsFolder(drawingId uint) bool {
	if err := os.MkdirAll(DrawingsFolder, os.ModePerm); err != nil {
		engine.PrintError("Error creating images folder", err)
		return false
	}

	return os.MkdirAll(service.GetDrawingsFolderName(drawingId), os.ModePerm) == nil
}

func (service *DrawingService) CreateDefaultImage(drawingId uint, req *classes.DrawingCreateRequest) {
	upLeft := image.Point{0, 0}
	lowRight := image.Point{DrawingWidth, DrawingHeight}

	img := image.NewRGBA(image.Rectangle{upLeft, lowRight})
	service.CreateDrawingsFolder(drawingId)
	count := service.GetDrawingImagesCount(drawingId)
	fileName, _ := service.GetDrawingImageFileName(drawingId, count+1, utils.MimePng)
	if f, err := os.Create(fileName); err == nil {
		png.Encode(f, img)
	}
}
