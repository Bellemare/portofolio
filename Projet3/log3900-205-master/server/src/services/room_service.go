package services

import (
	"strings"

	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/engine"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/classes"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/dto"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/entities"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/models"
	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/validators"
)

type RoomService struct{}

func NewRoomService() *RoomService {
	return &RoomService{}
}

func (service *RoomService) IsRoomMember(req *models.RoomMemberModel) (bool, engine.Error) {
	room := entities.NewRoom()
	roomExists := room.Dao.ExistsById(req.RoomId, &models.RoomModel{})
	if !roomExists {
		return false, engine.NewError("La salle est inexistante")
	}

	isMember := room.RoomMemberDao.IsRoomMember(req)
	return isMember, nil
}

func (service *RoomService) CreateRoom(req *classes.RoomCreateRequest) (*models.RoomModel, engine.Error) {
	req.Name = strings.TrimSpace(req.Name)
	validator := engine.NewValidator()
	validator.RegisterValidation("validLength", validators.IsValidShortNameLength)
	if err := validator.ValidateModel(req); err != nil {
		return nil, err
	}

	room := entities.NewRoom()
	exists := room.Dao.ExistsByName(req.Name)
	if exists {
		return nil, engine.NewFieldsError([]string{"Une salle existe avec le même nom"}, []string{models.RoomName})
	}
	return room.Create(req)
}

func (service *RoomService) JoinRoom(req *models.RoomMemberModel) engine.Error {
	if isMember, err := service.IsRoomMember(req); err != nil || isMember {
		if err == nil {
			err = engine.NewError("L'utilisateur est déjà membre de la salle")
		}
		return err
	}

	room := entities.NewRoom()
	err := room.RoomMemberDao.AddUser(req)
	if err != nil {
		return engine.NewError("Impossible d'ajouter l'utilisateur à la salle")
	}

	return nil
}

func (service *RoomService) LeaveRoom(req *models.RoomMemberModel) engine.Error {
	if isMember, err := service.IsRoomMember(req); err != nil || !isMember {
		if err == nil {
			err = engine.NewError("L'utilisateur n'est pas membre de la salle")
		}
		return err
	}

	room := entities.NewRoom()
	err := room.RoomMemberDao.RemoveUser(req)
	if err != nil {
		return engine.NewError("Impossible de retirer l'utilisateur de la salle")
	}

	return nil
}

func (service *RoomService) GetRoomMembers(roomId uint) (*[]dto.UserDTO, engine.Error) {
	room := entities.NewRoom()
	var members *[]dto.UserDTO
	var err engine.Error
	if roomId != entities.PublicRoomID {
		roomExists := room.Dao.ExistsById(roomId, &models.RoomModel{})
		if !roomExists {
			return nil, engine.NewError("La salle est inexistante")
		}

		members, err = room.Dao.GetRoomMembers(roomId)
		if err != nil {
			err = engine.NewError("Impossible de récupérer la liste de membres")
		}
	} else {
		userService := NewUserService()
		members, err = userService.GetAll()
	}

	return members, err
}

func (service *RoomService) GetRoomHistory(req *classes.RoomFetchHistoryRequest) (*[]dto.RoomHistoryDTO, engine.Error) {
	room := entities.NewRoom()
	if req.RoomId != entities.PublicRoomID {
		roomExists := room.Dao.ExistsById(req.RoomId, &models.RoomModel{})
		if !roomExists {
			return nil, engine.NewError("La salle est inexistante")
		}
	}
	history, err := room.RoomHistoryDao.GetHistory(req)
	if err != nil {
		return nil, engine.NewError("Impossible de récupérer l'historique de communication")
	}

	return history, nil
}

func (service *RoomService) GetRooms(userId uint, req *classes.RoomRequestFilter) (*[]models.RoomModel, error) {
	room := entities.NewRoom()
	return room.GetAll(userId, req)
}

func (service *RoomService) GetJoinedRooms(userId uint) (*[]models.RoomModel, error) {
	room := entities.NewRoom()
	joinedRooms := []models.RoomModel{*models.GetPublicRoom()}
	rooms, err := room.Dao.GetMemberRooms(userId)
	if err != nil {
		return nil, engine.NewError("Impossible de récupérer la lsite de salles")
	}
	joinedRooms = append(joinedRooms, *rooms...)

	return &joinedRooms, nil
}

func (service *RoomService) DeleteRoom(id uint) error {
	room := entities.NewRoom()
	return room.Delete(id)
}

func (service *RoomService) IsRoomOwner(id uint, userId uint) (bool, engine.Error) {
	room := entities.NewRoom()

	roomModel, err := room.Get(id)
	if err != nil {
		return false, engine.NewError("Salle inexistante")
	}

	return roomModel.OwnerId == userId, nil
}
