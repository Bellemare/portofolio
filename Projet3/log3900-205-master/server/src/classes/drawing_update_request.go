package classes

type DrawingUpdateRequest struct {
	ID       uint   `json:"id"`
	Name     string `json:"name" validate:"required,validLength" label:"Nom"`
	AlbumId  uint   `json:"album_id" label:"Album"`
	Password string `json:"password"`
}
