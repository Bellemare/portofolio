package classes

type RoomFetchHistoryRequest struct {
	RoomId uint `json:"owner_id"`
	Limit  uint `json:"limit"`
	Offset uint `json:"offset"`
}
