package classes

type UserUpdateRequest struct {
	ID            uint
	Pseudonym     string `json:"pseudonym" validate:"required,validShortName" label:"Pseudonyme"`
	Avatar        string `json:"avatar" validate:"image" label:"Avatar"`
	IsPublicEmail bool   `json:"is_public_email"`
}
