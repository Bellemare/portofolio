package classes

type DrawingCreateRequest struct {
	Name     string `json:"name" validate:"required,validLength" label:"Nom"`
	AlbumId  uint   `json:"album_id"`
	OwnerId  uint   `json:"owner_id"`
	Password string `json:"password"`
}
