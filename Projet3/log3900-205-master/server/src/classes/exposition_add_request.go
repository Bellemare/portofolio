package classes

type ExpositionAddRequest struct {
	DrawingID uint `json:"drawing_id" validate:"required" label:"Dessin"`
	AlbumID   uint
}
