package classes

import "time"

type DrawingsFilterRequest struct {
	NamesFilter      []string   `json:"names_filter"`
	PseudonymsFilter []string   `json:"pseudonyms_filter"`
	StartDate        *time.Time `json:"start_date"`
	EndDate          *time.Time `json:"end_date"`
}
