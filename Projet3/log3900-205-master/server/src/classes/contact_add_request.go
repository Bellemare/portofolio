package classes

type ContactAddRequest struct {
	ContactId uint `json:"contact_id" validate:"required" label:"Contact"`
	UserId    uint
}
