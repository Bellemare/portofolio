package classes

type AlbumUpdateRequest struct {
	ID          uint   `json:"id"`
	Name        string `json:"name" validate:"required,validLength" label:"Nom"`
	Description string `json:"description" validate:"validDescription" label:"Description"`
}
