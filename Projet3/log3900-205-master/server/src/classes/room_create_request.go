package classes

type RoomCreateRequest struct {
	Name    string `json:"name" validate:"required,validLength" label:"Nom"`
	OwnerId uint   `json:"owner_id"`
}
