package classes

type UserCreateRequest struct {
	Email          string `json:"email" validate:"required,email" label:"Courriel"`
	Password       string `json:"password" validate:"required" label:"Mot de passe"`
	PasswordRepeat string `json:"password_repeat" validate:"required" label:"Mot de passe répété"`
	Pseudonym      string `json:"pseudonym" validate:"required,validShortName" label:"Pseudonyme"`
	Avatar         string `json:"avatar" validate:"image" label:"Avatar"`
}
