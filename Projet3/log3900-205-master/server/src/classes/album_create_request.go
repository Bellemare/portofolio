package classes

type AlbumCreateRequest struct {
	Name        string `json:"name" validate:"required,validLength" label:"Nom"`
	Description string `json:"description" validate:"validDescription" label:"Description"`
	OwnerId     uint   `json:"owner_id"`
}
