package classes

type DrawingsSmartFilter struct {
	SearchValue string `json:"search_value"`
	AlbumID     int    `json:"album_id"`
	UserID      uint
}
