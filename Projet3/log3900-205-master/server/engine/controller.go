package engine

import (
	"net/http"

	"gitlab.com/polytechnique-montr-al/log3900/22-1/equipe-205/log3900-205/server/src/utils"
)

type IController interface {
	RegisterRoutes(router IRouter)
}

func ParseRouteIdentifier(key string, ctx *Context) (uint, bool) {
	if ctx.GetParam(key) == "" {
		ctx.WriteJSONError(http.StatusBadRequest, NewError("L'identifiant est obligatoire"))
		return 0, false
	}
	id, err := utils.ParseModelId(ctx.GetParam(key))
	if err != nil {
		ctx.WriteJSONError(http.StatusBadRequest, NewError("L'identifiant est invalide"))
		return 0, false
	}

	return id, true
}
