package engine

type RequestResponse struct {
	Title string `json:"title"`
	Body  string `json:"body"`
}
