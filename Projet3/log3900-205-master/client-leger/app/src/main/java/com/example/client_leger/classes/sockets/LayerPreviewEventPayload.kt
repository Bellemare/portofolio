package com.example.client_leger.classes.sockets

import com.example.client_leger.classes.Color
import com.example.client_leger.classes.drawing.Transformation
import com.example.client_leger.classes.toolProperties.ToolProperties

class LayerPreviewEventPayload(
    val id: Int,
    val layer: Int,
    val transform: Transformation,
    val attributes: ToolProperties,
    val strokeColor: Color,
    val fillColor: Color,
    val textColor: Color,
    val userId: Int? = null,
    val user: String? = null,
): Payload() {}
class SocketLayerPreviewEventPayload(type: Int, payload: LayerPreviewEventPayload): SocketPayload(type, payload)
