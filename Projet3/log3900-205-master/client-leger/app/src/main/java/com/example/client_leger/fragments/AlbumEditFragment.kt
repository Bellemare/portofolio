package com.example.client_leger.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.fragment.app.activityViewModels
import com.example.client_leger.R
import com.example.client_leger.classes.Album
import com.example.client_leger.classes.AlbumModel
import com.example.client_leger.classes.RequestFieldErrorResponse
import com.example.client_leger.classes.RequestResponse
import com.example.client_leger.viewModels.AlbumMenuViewModel
import com.shopify.promises.Promise
import java.lang.Exception
import java.nio.charset.Charset

class AlbumEditFragment : Fragment() {
    private lateinit var editBtn: Button
    private lateinit var cancelBtn: Button

    private lateinit var albumName: EditText
    private lateinit var albumDescription: EditText

    private lateinit var album: Album

    private val viewModel: AlbumMenuViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_album_edit, container, false)
        album = requireArguments().get("album") as Album

        albumName = view.findViewById(R.id.newAlbumName)
        albumDescription = view.findViewById(R.id.newAlbumDescription)

        albumName.setText(album.name)
        albumDescription.setText(album.description)

        setUpClosingButtons(view)

        return view
    }

    private fun setUpClosingButtons(view: View) {
        editBtn = view.findViewById(R.id.editBtn)
        cancelBtn = view.findViewById(R.id.cancelBtn)

        editBtn.setOnClickListener {
            album.name = albumName.text.toString()
            album.description = albumDescription.text.toString()

            viewModel.updateAlbum(album).whenComplete { result ->
                when(result) {
                    is Promise.Result.Success -> {
                        val requestResponse: RequestResponse = result.value
                        requestResponse.displayResponse(view)

                        (parentFragment as AlbumInfoFragment).removeActionFragment()
                    }
                    is Promise.Result.Error -> {
                        //handle error
                        try {
                            val error = RequestResponse.Deserializer().deserialize(result.error.errorData.toString(
                                Charset.defaultCharset()))
                            error.displayResponse(view)
                        }
                        catch (e: Exception) {
                            val error = RequestFieldErrorResponse.Deserializer().deserialize(result.error.errorData.toString(
                                Charset.defaultCharset()))
                            error.displayError(view)
                        }
                    }
                }
            }

        }
        cancelBtn.setOnClickListener {
            (parentFragment as AlbumInfoFragment).removeActionFragment()
        }
    }
}