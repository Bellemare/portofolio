package com.example.client_leger.classes;

import java.util.HashSet;

public class CallbackManager {
    private HashSet<Runnable> runnables;

    public CallbackManager() {
        runnables = new HashSet<Runnable>();
    }

    public Subscription addSubscriber(Runnable r) {
        runnables.add(r);
        return new Subscription(this, r);
    }

    public void removeSubscriber(Runnable r) {
        runnables.remove(r);
    }

    public void callback() {
        for (Runnable r : runnables)
            r.run();
    }
}
