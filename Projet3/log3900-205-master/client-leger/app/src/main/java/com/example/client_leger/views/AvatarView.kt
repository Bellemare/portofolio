package com.example.client_leger.views

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.Base64
import android.view.View
import com.example.client_leger.classes.Vec2
import com.example.client_leger.services.CanvasSizeService
import com.example.client_leger.services.DrawingService

class AvatarView: View {
    private val IMAGE_WIDTH = 80f
    private val IMAGE_HEIGHT = 80f
    private var imageSize: Vec2 = Vec2(IMAGE_WIDTH, IMAGE_HEIGHT)

    private var canvasData: Bitmap? = null

    constructor(context: Context) : super(context, null){
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs){
    }

    override fun onDraw(canvas: Canvas) {
        var data = canvasData
        if (data != null) {
            val ratioWidth: Float = imageSize.x / data.width.toFloat()
            val ratioHeight: Float = imageSize.y / data.height.toFloat()
            val ratio: Float = Math.min(ratioWidth, ratioHeight)
            val adjustedSize: Vec2 = Vec2(data.width.toFloat() * ratio, data.height.toFloat() * ratio)
            val corner : Vec2 = Vec2((imageSize.x - adjustedSize.x)/2f, (imageSize.y - adjustedSize.y) / 2f)

            val tmpBitmap = Bitmap.createBitmap(imageSize.x.toInt(), imageSize.y.toInt(), Bitmap.Config.ARGB_8888)
            val tmpCanvas = Canvas(tmpBitmap)
            val transformedRect = Rect(corner.x.toInt(),corner.y.toInt(),(corner.x + adjustedSize.x).toInt(), (corner.y + adjustedSize.y).toInt())
            tmpCanvas.drawBitmap(data, Rect(0,0,data.width, data.height), transformedRect, null)

            val circleBitmap = Bitmap.createBitmap(imageSize.x.toInt(), imageSize.y.toInt(), Bitmap.Config.ARGB_8888)
            val circleCanvas = Canvas(circleBitmap)
            val paint = Paint()
            paint.isAntiAlias = true
            paint.color = Color.RED
            circleCanvas.drawOval(RectF(0f,0f,imageSize.x, imageSize.y), paint)
            paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.DST_IN)
            tmpCanvas.drawBitmap(circleBitmap, Matrix(), paint)

            canvas.drawBitmap(tmpBitmap, Matrix(), null)
        }
        super.onDraw(canvas)
    }

    fun initImage(avatar: String, pixelSize: Vec2?) {
        if(pixelSize != null) {
            imageSize = pixelSize
        }
        val imageBytes:ByteArray = Base64.decode(avatar.split(",").elementAt(1), Base64.DEFAULT)
        canvasData = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
    }

}