package com.example.client_leger.viewModels

import android.graphics.Color
import androidx.lifecycle.ViewModel
import com.example.client_leger.classes.apiEvents.*
import com.example.client_leger.classes.drawing.ToolType
import com.example.client_leger.classes.toolProperties.ToolProperties
import com.example.client_leger.services.DrawingService
import kotlinx.coroutines.runBlocking

class ToolOptionViewModel : ViewModel() {
    var currentTool: String = "Crayon"

    var colors: Array<Int> = arrayOf(Color.BLACK, Color.WHITE, Color.BLACK)

    var properties: ToolProperties = ToolProperties()

    private var toolPropertiesEventListener: AppEvents.IntWrapper = AppEvents.IntWrapper(0)

    fun init() {
        currentTool = "Crayon"
        runBlocking {
            AppEvents.listenTo(AppEventType.TOOL_PROPERTIES_CHANGE_EVENT, toolPropertiesEventListener) {
                val evt = it as ToolPropertiesChangeEvent
                properties = evt.properties
                val tools = ToolType.values().associateBy(ToolType::value)
                val toolType = tools[DrawingService.getCurrentTool().toolType.value]
                if (toolType != null) {
                    currentTool = toolType.tool
                }
            }
        }
    }

    fun setTool(tool: String) {
        currentTool = tool
        val tools = ToolType.values().associateBy(ToolType::tool)
        val toolType = tools[currentTool]
        if (toolType != null) {
            DrawingService.setCurrentTool(toolType)
        }
    }

    fun setColor(index: Int, color: Int) {
        colors[index] = color
        val transparency: Int = if (color == Color.TRANSPARENT) 0 else 1

        val rgbaColor = com.example.client_leger.classes.Color(Color.red(color), Color.green(color), Color.blue(color), transparency)
        if (index == 0) {
            DrawingService.primaryColor = rgbaColor
        } else if (index == 1) {
            DrawingService.secondaryColor = rgbaColor
        } else {
            DrawingService.textColor = rgbaColor
        }
        AppEvents.emitEvent(ColorChangeEvent())
    }

    fun setProperty(key: String, value: Any) {
        AppEvents.emitEvent(ToolPropertySetEvent(key, value))
    }

    fun destroy() {
        runBlocking {
            AppEvents.unregister(toolPropertiesEventListener.value, AppEventType.TOOL_PROPERTIES_CHANGE_EVENT)
        }
    }
}