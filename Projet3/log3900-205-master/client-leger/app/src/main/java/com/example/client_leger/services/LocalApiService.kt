package com.example.client_leger.services

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.GsonBuilder

object LocalApiService {
    lateinit var sharedPreferences: SharedPreferences

    val STORAGE = "Storage"

    fun initialize(activity: Activity) {
        sharedPreferences = activity.getSharedPreferences(STORAGE, Context.MODE_PRIVATE)
    }

    inline fun <reified T> getFromSharedPreferences(key: String) : T? {
        val value = sharedPreferences.getString(key, null)
        return GsonBuilder().create().fromJson(value, T::class.java)
    }

    fun <T> saveToSharedPreferences(key: String, attr: T) {
        val editor : SharedPreferences.Editor = sharedPreferences.edit()
        editor.putString(key, Gson().toJson(attr))
        editor.apply()
    }

    fun deleteFromSharedPreferences(key: String) {
        sharedPreferences.edit().remove(key).apply()
    }
}