package com.example.client_leger.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import androidx.navigation.navOptions
import com.example.client_leger.R
import com.example.client_leger.classes.apiEvents.AppEvents
import com.example.client_leger.classes.apiEvents.DisconnectEvent
import com.example.client_leger.services.RoomService
import com.example.client_leger.viewModels.AlbumMenuViewModel
import com.shopify.promises.Promise

class LogoutButtonFragment : Fragment() {
    private lateinit var logoutButton: TextView

    private val viewModel: AlbumMenuViewModel by activityViewModels()
    private var appDisconnectEventListener: AppEvents.IntWrapper = AppEvents.IntWrapper(0)

       override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_logout_button, container, false)

        logoutButton = view.findViewById(R.id.logoutButton)
        logoutButton.setOnClickListener { logout(view) }

        return view
    }


    private fun logout (inflaterView: View) {
        viewModel.logout().whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    AppEvents.emitEvent(DisconnectEvent())
                    RoomService.onDestroy()
                    Navigation.findNavController(inflaterView).navigate(R.id.loginFragment)
                }
                is Promise.Result.Error -> {
                    println (it.error)
                }
            }
        }
    }



}