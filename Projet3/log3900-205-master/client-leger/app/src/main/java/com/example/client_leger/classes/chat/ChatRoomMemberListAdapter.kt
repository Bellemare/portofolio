package com.example.client_leger.classes.chat;

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.example.client_leger.R
import com.example.client_leger.R.*
import com.example.client_leger.classes.Color
import com.example.client_leger.classes.User
import com.example.client_leger.classes.Vec2
import com.example.client_leger.classes.sockets.MessagePayload
import com.example.client_leger.viewModels.ChatViewModel
import com.example.client_leger.views.AvatarView
import com.shopify.promises.Promise
import java.util.*
import kotlin.collections.ArrayList

class ChatRoomMemberListAdapter(var memberList: ArrayList<User>, var viewModel: ChatViewModel, var memberSelectedListener: memberSelectedListener): RecyclerView.Adapter<ChatRoomMemberListAdapter.ViewHolder>() {


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val avatar: AvatarView
        val name: TextView
        val cancelButton: TextView


        init {
            avatar = view.findViewById(R.id.avatarView)
            name = view.findViewById(R.id.contactName)
            cancelButton = view.findViewById(R.id.removeContactBtn)
        }
    }

    lateinit var originalMemberList: ArrayList<User>

    fun setList( newList: ArrayList<User>) {
        var size = memberList.size
        memberList.clear()
        notifyItemRangeRemoved(0, size)
        this.memberList.addAll(newList)
        notifyItemRangeInserted(0, newList.size)
        originalMemberList = memberList.clone() as ArrayList<User>
    }

    override fun getItemCount() = memberList.size

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(layout.fragment_contact_list_item, viewGroup, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        val user = memberList[position]

        viewHolder.name.text = user.pseudonym

        viewHolder.avatar.initImage(user.avatar, Vec2(80f, 80f))

        viewHolder.cancelButton.visibility = View.GONE

        viewHolder.itemView.setOnClickListener { memberSelectedListener.onMemberClicked(user.pseudonym) }
    }

    fun filterList(searchTerm: String) {
        val size = memberList.size
        memberList.clear()
        notifyItemRangeRemoved(0, size)

        var tempList = ArrayList<User>()

        for(member in originalMemberList){
            if(member.pseudonym.contains(searchTerm)){
                tempList.add(member)
            }
        }
        memberList = tempList
        notifyItemRangeInserted(0, memberList.size )
    }
}
