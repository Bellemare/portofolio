package com.example.client_leger.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.add
import androidx.fragment.app.commit
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.client_leger.R
import com.example.client_leger.classes.Album
import com.example.client_leger.classes.AlbumListAdapter
import com.example.client_leger.classes.ViewDisabler
import com.example.client_leger.viewModels.AlbumMenuViewModel
import com.shopify.promises.Promise

class AlbumExpositionMenuFragment : Fragment(), ViewDisabler {

    private val viewModel: AlbumMenuViewModel by activityViewModels()

    private lateinit var mainView: LinearLayout
    private lateinit var exitBtn: TextView

    private lateinit var albumListView: RecyclerView
    private lateinit var albumAdapter: AlbumListAdapter
    private var albums: ArrayList<Album> = ArrayList()

    private lateinit var noExpoMessage: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_album_exposition_menu, container, false)
        mainView = view.findViewById(R.id.mainView)
        noExpoMessage = view.findViewById(R.id.noExpositionsMessage)

        setUpExit(view)
        setUpAlbumListView(view)
        fetchAlbums()

        return view
    }

    private fun fetchAlbums() {
        viewModel.getExpositionAlbums().whenComplete { result ->
            when(result) {
                is Promise.Result.Success -> {
                    albums.clear()
                    albums.addAll(result.value)
                    albumAdapter.notifyDataSetChanged()
                    noExpoMessage.visibility = View.GONE
                }
                is Promise.Result.Error -> {
                    println(result.error)
                }
            }
        }
    }

    private fun setUpAlbumListView(view: View) {
        albumListView = view.findViewById(R.id.AlbumList)
        albumAdapter = AlbumListAdapter(albums) { position: Int -> onAlbumClick(position) }
        albumListView.adapter = albumAdapter
        albumListView.layoutManager = GridLayoutManager(context, 4, RecyclerView.HORIZONTAL, false)
    }

    private fun onAlbumClick(position: Int) {
        val bundle = bundleOf(Pair("album", albums[position]))

        if(childFragmentManager.findFragmentByTag("albumExpoAction") == null) {
            childFragmentManager.commit {
                setReorderingAllowed(true)
                add<AlbumExpositionInfoFragment>(R.id.AlbumActionFragmentContainer, "albumExpoAction", args = bundle)
            }
            enableDisableViewGroup(mainView, false)
            (parentFragment as AlbumMenuFragment).resetAlbumInfoSettings()
        }
    }

    private fun setUpExit(view: View) {
        exitBtn = view.findViewById(R.id.exitBtn)
        exitBtn.setOnClickListener {
            (parentFragment as AlbumMenuFragment).removeAlbumActionFragment()
        }
    }

    fun removeAlbumExpositionActionFragment() {
        val albumActionFragment = childFragmentManager.findFragmentByTag("albumExpoAction")
        if(albumActionFragment != null) {
            childFragmentManager.commit {
                setReorderingAllowed(true)
                remove(albumActionFragment)
            }
        }
        enableDisableViewGroup(mainView, true)
        enableDisableAlbumMenuView(true)

        fetchAlbums()
    }

    fun enableDisableAlbumMenuView(isEnabled: Boolean) {
        (parentFragment as AlbumMenuFragment).enableDisableAlbumMenuView(isEnabled)
    }

}