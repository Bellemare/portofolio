package com.example.client_leger.classes.apiEvents


class SelectionDeleteEvent(): AppEvent() {
    override val type = AppEventType.SELECTION_DELETE_EVENT
}