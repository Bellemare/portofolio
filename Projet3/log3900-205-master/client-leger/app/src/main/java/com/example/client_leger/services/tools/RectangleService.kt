package com.example.client_leger.services.tools

import com.example.client_leger.classes.Vec2
import com.example.client_leger.classes.drawing.DrawingRect
import com.example.client_leger.classes.drawing.ToolType
import com.example.client_leger.services.drawingServices.RectangleDrawingService

class RectangleService : ShapeService() {
    override var previewDrawingElement: DrawingRect = DrawingRect(0f,0f,0f,0f)
    override var drawingElement: DrawingRect = DrawingRect(0f,0f,0f,0f)
    override val toolDrawingService: RectangleDrawingService = RectangleDrawingService()
    override val toolType: ToolType
        get() = ToolType.RECTANGLE

    override fun updatePreviewData(actionCoord: Vec2) {
        val actionCoordDiff: Vec2 = Vec2(x = actionCoord.x - actionDownCoord.x, y= actionCoord.y - actionDownCoord.y)
        previewDrawingElement.x = Math.round(actionDownCoord.x).toFloat()
        previewDrawingElement.y = Math.round(actionDownCoord.y).toFloat()
        previewDrawingElement.width = Math.round(actionCoordDiff.x).toFloat()
        previewDrawingElement.height = Math.round(actionCoordDiff.y).toFloat()
    }

    override fun copyPreviewInData() {
        drawingElement = previewDrawingElement.deepCopy() as DrawingRect
    }

    override fun clearData() {
        previewDrawingElement = DrawingRect(0f,0f,0f,0f)
        drawingElement = DrawingRect(0f,0f,0f,0f)
    }

}