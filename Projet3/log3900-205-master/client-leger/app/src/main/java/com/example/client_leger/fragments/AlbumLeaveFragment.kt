package com.example.client_leger.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.activityViewModels
import com.example.client_leger.R
import com.example.client_leger.classes.Album
import com.example.client_leger.classes.RequestResponse
import com.example.client_leger.viewModels.AlbumMenuViewModel
import com.shopify.promises.Promise
import java.nio.charset.Charset

class AlbumLeaveFragment : Fragment() {

    private val CONFIRM_MESSAGE: String = "Voulez-vous vraiment quitter cet album?\n Tous vos dessins seront transférés au propriétaire de l'album.\n"

    private lateinit var confirmMessage: TextView
    private lateinit var album: Album

    private lateinit var leaveButton: Button
    private lateinit var cancelButton: Button

    private val viewModel: AlbumMenuViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_album_leave, container, false)
        album = requireArguments().get("album") as Album

        confirmMessage = view.findViewById(R.id.confirmMsg)
        confirmMessage.text = CONFIRM_MESSAGE

        leaveButton = view.findViewById(R.id.leaveBtn)
        leaveButton.setOnClickListener {
            viewModel.quitAlbum(album.id).whenComplete { result ->
                when(result) {
                    is Promise.Result.Success -> {
                        val requestResponse: RequestResponse = result.value
                        requestResponse.displayResponse(view)

                        (parentFragment as AlbumInfoFragment).returnToMainMenu()
                    }
                    is Promise.Result.Error -> {
                        //handle error
                        val error = RequestResponse.Deserializer().deserialize(result.error.errorData.toString(
                            Charset.defaultCharset()))
                        error.displayResponse(view)
                    }
                }
            }
        }

        cancelButton = view.findViewById(R.id.cancelBtn)
        cancelButton.setOnClickListener {
            (parentFragment as AlbumInfoFragment).removeActionFragment()
        }

        return view
    }
}