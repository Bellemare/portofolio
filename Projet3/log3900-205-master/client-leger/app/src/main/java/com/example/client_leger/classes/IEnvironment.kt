package com.example.client_leger.classes

interface IEnvironment {
    val production: Boolean
    val defaultHost: String
    val websocketHost: String
    val defaultPort: Int
    val defaultEndpoint: String
}