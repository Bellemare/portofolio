package com.example.client_leger.classes

import com.example.client_leger.getApiMapper
import com.example.client_leger.services.LocalApiService
import com.github.kittinunf.fuel.core.ResponseDeserializable
import java.util.*

object Authentication {
    private var authToken: AuthToken? = null
    private var user: User? = null

    fun setAuthToken(auth: AuthToken) {
        authToken = auth
    }

    fun getAuthenticationToken(): AuthToken? {
        if (authToken == null) {
            loadAuthToken()
        }

        return authToken
    }

    fun getUser(): User? {
        return user
    }

    fun setUser(user: User) {
        this.user = user
    }

    fun loadAuthToken() {
        authToken = LocalApiService.getFromSharedPreferences<AuthToken>("session")
    }

    fun unloadAuthToken() {
        LocalApiService.deleteFromSharedPreferences("session")
        authToken = null
        user = null
    }

    fun isAuthenticated(): Boolean {
        val token = getAuthenticationToken()
        return token != null && token.expires_at.after(Date())
    }
}

data class AuthToken(val token: String, val user_id: String, val expires_at: Date) {
    class Deserializer : ResponseDeserializable<AuthToken> {
        override fun deserialize(content: String) =
            getApiMapper().readValue(content, AuthToken::class.java)
    }
}