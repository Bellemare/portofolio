package com.example.client_leger.fragments

import android.annotation.SuppressLint
import android.graphics.*
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.Switch
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.commit
import com.example.client_leger.R
import com.example.client_leger.classes.Vec2
import com.example.client_leger.databinding.FragmentColorPaletteBinding
import com.example.client_leger.viewModels.ToolOptionViewModel
import kotlin.properties.Delegates

class ColorPaletteFragment : Fragment() {

    private val model: ToolOptionViewModel by activityViewModels()
    private lateinit var paletteCanvas: Canvas
    private var paletteBitmap: Bitmap = Bitmap.createBitmap(450,450, Bitmap.Config.ARGB_8888)
    private lateinit var colorSliderCanvas: Canvas
    private var colorSliderBitmap: Bitmap = Bitmap.createBitmap(450,100, Bitmap.Config.ARGB_8888)

    private lateinit var paletteImageView : ImageView
    private lateinit var colorSliderImageView : ImageView
    private lateinit var newColor : TextView
    private lateinit var okBtn: Button
    private lateinit var cancelBtn: Button

    @SuppressLint("UseSwitchCompatOrMaterialCode")
    private lateinit var transparencySwitch: Switch
    private var isTransparent: Boolean = false

    private var currentPalettePosition: Vec2 = Vec2(449f,0f)

    private var currentColor: Int = Color.WHITE
    private var currentSliderColor: Int = Color.WHITE
    private var colorIndex: Int = 0
    private val GRADIENT_COLORS: IntArray = intArrayOf(
        Color.RED,
        Color.YELLOW,
        Color.GREEN,
        Color.CYAN,
        Color.BLUE,
        Color.MAGENTA,
        Color.RED)


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding: FragmentColorPaletteBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_color_palette, container, false)
        val view = binding.root


        colorIndex = requireArguments().getInt("colorIndex")
        binding.allowedTransparency = false

        if (colorIndex == 1) {
            binding.allowedTransparency = true
            transparencySwitch = view.findViewById(R.id.transparentSwitch)
            transparencySwitch.setOnCheckedChangeListener { _, isChecked ->
                isTransparent = isChecked
            }
        }

        newColor = view.findViewById(R.id.newColor)

        if (model.colors[colorIndex] == Color.TRANSPARENT) {
            currentSliderColor = Color.WHITE
            updateNewColor(Color.WHITE)
        }
        else {
            currentSliderColor = model.colors[colorIndex]
            updateNewColor(model.colors[colorIndex])
        }

        setUpImageViews(view)
        drawPaletteColorGradient(currentColor)
        drawSliderGradient(null)

        setUpButtons(view)

        return view
    }

    private fun setUpButtons(view: View) {
        okBtn = view.findViewById(R.id.okBtn)
        okBtn.setOnClickListener {
            if(isTransparent){
                model.setColor(colorIndex, Color.TRANSPARENT)
            }
            else {
                model.setColor(colorIndex, currentColor)
            }
            (parentFragment as ToolOptionFragment).updateColors()

            val fragment: Fragment = this
            parentFragmentManager.commit {
                remove(fragment)
            }
        }

        cancelBtn = view.findViewById(R.id.cancelBtn)
        cancelBtn.setOnClickListener {
            val fragment: Fragment = this
            parentFragmentManager.commit {
                remove(fragment)
            }
        }
    }

    private fun drawPaletteColorGradient(color: Int) {
        val paint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.color = color
        paletteCanvas.drawRect(0f,0f,450f,450f,paint)

        paint.reset()
        paint.shader = LinearGradient(0f,0f, paletteBitmap.width.toFloat(), 0f,Color.WHITE, Color.TRANSPARENT, Shader.TileMode.MIRROR)
        paletteCanvas.drawPaint(paint)

        paint.reset()
        paint.shader = LinearGradient(0f,paletteBitmap.width.toFloat(),0f, 0f,Color.BLACK, Color.TRANSPARENT, Shader.TileMode.MIRROR)
        paletteCanvas.drawPaint(paint)

        paint.reset()
        paint.style=Paint.Style.STROKE
        paint.color=Color.WHITE
        paint.strokeWidth = 3f
        paletteCanvas.drawCircle(currentPalettePosition.x, currentPalettePosition.y, 10f, paint)

        paletteImageView.setImageBitmap(paletteBitmap)
    }

    private fun drawSliderGradient(x: Int?) {
        val paint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.shader = LinearGradient(0f,0f, colorSliderBitmap.width.toFloat(), 0f, GRADIENT_COLORS, null, Shader.TileMode.MIRROR)
        colorSliderCanvas.drawPaint(paint)

        if(x != null) {
            val rectPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
            rectPaint.color = Color.BLACK
            rectPaint.style = Paint.Style.STROKE
            rectPaint.strokeWidth = 5f
            colorSliderCanvas.drawRect(x - 15f, 0f, x + 15f, colorSliderBitmap.height.toFloat(), rectPaint)
        }

        colorSliderImageView.setImageBitmap(colorSliderBitmap)
    }

    private fun updateNewColor(color: Int) {
        newColor.setBackgroundColor(color)
        currentColor = color
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setUpImageViews(view: View) {
        paletteImageView = view.findViewById(R.id.palette)
        paletteImageView.setOnTouchListener { _, event ->
            if(event.x.toInt() in 0 until paletteBitmap.width && event.y.toInt() in 0 until paletteBitmap.height) {
                when(event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        currentPalettePosition = Vec2(event.x, event.y)
                        drawPaletteColorGradient(currentSliderColor)
                        updateNewColor(paletteBitmap.getPixel(event.x.toInt(), event.y.toInt()))
                    }
                    MotionEvent.ACTION_MOVE -> {
                        currentPalettePosition = Vec2(event.x, event.y)
                        drawPaletteColorGradient(currentSliderColor)
                        updateNewColor(paletteBitmap.getPixel(event.x.toInt(), event.y.toInt()))
                    }
                }
            }
            true
        }

        colorSliderImageView = view.findViewById(R.id.slider)
        colorSliderImageView.setOnTouchListener { _, event ->
            if(event.x.toInt() in 0 until colorSliderBitmap.width && event.y.toInt() in 0 until colorSliderBitmap.height) {
                when(event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        colorSliderBitmap.eraseColor(Color.TRANSPARENT)
                        drawSliderGradient(event.x.toInt())
                        currentSliderColor = colorSliderBitmap.getPixel(event.x.toInt(), event.y.toInt())
                        drawPaletteColorGradient(currentSliderColor)
                        updateNewColor(paletteBitmap.getPixel(currentPalettePosition.x.toInt(), currentPalettePosition.y.toInt()))
                    }
                    MotionEvent.ACTION_MOVE -> {
                        colorSliderBitmap.eraseColor(Color.TRANSPARENT)
                        drawSliderGradient(event.x.toInt())
                        currentSliderColor = colorSliderBitmap.getPixel(event.x.toInt(), event.y.toInt())
                        drawPaletteColorGradient(currentSliderColor)
                        updateNewColor(paletteBitmap.getPixel(currentPalettePosition.x.toInt(), currentPalettePosition.y.toInt()))
                    }
                }
            }
            true
        }

        paletteCanvas = Canvas(paletteBitmap)
        colorSliderCanvas = Canvas(colorSliderBitmap)
    }

}