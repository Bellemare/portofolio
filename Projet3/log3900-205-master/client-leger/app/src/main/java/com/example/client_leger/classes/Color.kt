package com.example.client_leger.classes

import kotlin.math.*

class Color(val r: Int, val g: Int, val b: Int, val a: Int) {
    private val HUE_VALUE_TO_CIRCLE = 60.0
    private val MAX_VALUE = 255
    private val CIRCLE_VALUE = 360
    private var hue: Double = 0.0
    private var saturation: Double = 0.0
    private var brightness: Double = 0.0

    fun init() {
        hue = calculateColorHue()
        brightness = calculateColorBrightness()
        saturation = calculateSaturation()
    }

    fun hueShift(shift: Double) {
        hue = shift
    }

    fun hsvToRGB(): Color {
        val c = (brightness * saturation);
        val x = (c * (1 - abs(((hue / HUE_VALUE_TO_CIRCLE) % 2) - 1)));
        val m = this.brightness - c;
        var colorR = 0.0
        var colorG = 0.0
        var colorB = 0.0

        val section = floor(hue / HUE_VALUE_TO_CIRCLE)
        if (section == 0.0) {
            colorR = c
            colorG =  x
        } else if (section == 1.0) {
            colorR = x
            colorG = c
        } else if (section == 2.0) {
            colorG = c
            colorB = x
        } else if (section == 3.0) {
            colorG = x
            colorB = c
        } else if (section == 4.0) {
            colorR = x
            colorB = c
        } else if (section == 5.0 || section == 6.0) {
            colorR = c
            colorB = x
        }

        return Color(
            round((colorR + m) * MAX_VALUE).toInt(),
            round((colorG + m) * MAX_VALUE).toInt(),
            round((colorB + m) * MAX_VALUE).toInt(),
            1,
        );
    }

    private fun calculateColorHue(): Double {
        val colorPercent = Color(r / MAX_VALUE, g / MAX_VALUE, b / MAX_VALUE, 1)
        val min = min(min(colorPercent.r, colorPercent.g), colorPercent.b);
        val max = max(max(colorPercent.r, colorPercent.g), colorPercent.b);
        val delta: Double = (max - min).toDouble();

        var hue: Double = 0.0;
        if (max != min) {
            if (max == colorPercent.r) {
                hue = (colorPercent.g - colorPercent.b) / delta;
            } else if (max == colorPercent.g) {
                hue = (colorPercent.b - colorPercent.r) / delta + 2;
            } else {
                hue = (colorPercent.r - colorPercent.g) / delta + 4;
            }
        }
        hue *= HUE_VALUE_TO_CIRCLE;
        if (hue < 0) {
            return round(hue + CIRCLE_VALUE)
        } else {
            return round(hue)
        }
    }

    private fun calculateColorBrightness(): Double {
        val colorPercent = Color(r / MAX_VALUE, g / MAX_VALUE, b / MAX_VALUE, 1)
        val max = max(max(colorPercent.r, colorPercent.g), colorPercent.b);

        return (round(max * 100.0) / 100.0);
    }

    private fun calculateSaturation(): Double {
        val colorPercent = Color(r / MAX_VALUE, g / MAX_VALUE, b / MAX_VALUE, 1)
        val min = min(min(colorPercent.r, colorPercent.g), colorPercent.b);
        val max = max(max(colorPercent.r, colorPercent.g), colorPercent.b);

        var value: Double = 0.0
        if (max != 0) {
            value = (max - min) / max.toDouble()
        }

        return (round(value * 100.0) / 100.0);
    }
}
