package com.example.client_leger.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.client_leger.R
import com.example.client_leger.classes.AlbumJoinRequest
import com.example.client_leger.classes.AlbumRequestAdapter
import com.example.client_leger.classes.ContactListAdapter
import com.example.client_leger.classes.SoundType
import com.example.client_leger.services.SoundPlayer
import com.example.client_leger.viewModels.AlbumMenuViewModel
import com.shopify.promises.Promise

class NotificationFragment : Fragment() {

    private val albumViewModel: AlbumMenuViewModel by activityViewModels()

    private var albumJoinRequests: ArrayList<AlbumJoinRequest> = ArrayList()

    private lateinit var requestListView: RecyclerView
    private lateinit var requestListAdapter: AlbumRequestAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_notification, container, false)
        setUpListView(view)

        getNotificationEntries()
        return view
    }

    private fun setUpListView(view: View) {
        requestListView = view.findViewById(R.id.notificationList)
        requestListAdapter = AlbumRequestAdapter(albumJoinRequests, {position: Int ->  onAcceptRequest(position)}, {position: Int ->  onDenyRequest(position)})
        requestListView.adapter = requestListAdapter
        requestListView.layoutManager = LinearLayoutManager(context)
    }

    private fun getNotificationEntries() {
        albumViewModel.getAlbumJoinRequests().whenComplete { result ->
            when(result) {
                is Promise.Result.Success -> {
                    albumJoinRequests.clear()
                    albumJoinRequests.addAll(result.value)
                    requestListAdapter.notifyDataSetChanged()
                }
                is Promise.Result.Error -> {
                    println(result.error)
                }
            }
        }
    }

    private fun onAcceptRequest(position: Int) {
        val request = albumJoinRequests[position]
        albumViewModel.acceptJoinRequest(request.album_id, request.id).whenComplete { result ->
            when(result) {
                is Promise.Result.Success -> {
                    getNotificationEntries()
                    requestListAdapter.notifyDataSetChanged()
                }
                is Promise.Result.Error -> {
                    println(result.error)
                }
            }
        }
    }

    private fun onDenyRequest(position: Int) {
        val request = albumJoinRequests[position]
        albumViewModel.declineJoinRequest(request.album_id, request.id).whenComplete { result ->
            when(result) {
                is Promise.Result.Success -> {
                    albumJoinRequests.removeAt(position)
                    requestListAdapter.notifyDataSetChanged()
                }
                is Promise.Result.Error -> {
                    println(result.error)
                }
            }
        }
    }
}