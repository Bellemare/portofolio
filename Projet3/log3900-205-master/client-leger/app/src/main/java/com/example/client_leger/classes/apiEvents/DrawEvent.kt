package com.example.client_leger.classes.apiEvents

import android.graphics.Bitmap

class DrawEvent(val data: Bitmap): AppEvent() {
    override val type = AppEventType.DRAW_EVENT
}