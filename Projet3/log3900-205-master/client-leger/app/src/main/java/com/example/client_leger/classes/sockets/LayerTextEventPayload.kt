package com.example.client_leger.classes.sockets

import com.example.client_leger.classes.drawing.DrawingText

class LayerTextEventPayload(
    val id: Int,
    val layer: Int,
    val text: DrawingText,
    val userId: Int? = null,
    val user: String? = null,
): Payload() {}
class SocketLayerTextEventPayload(type: Int, payload: LayerTextEventPayload): SocketPayload(type, payload)
