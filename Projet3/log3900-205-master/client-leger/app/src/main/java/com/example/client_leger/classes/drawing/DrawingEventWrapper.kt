package com.example.client_leger.classes.drawing

import android.graphics.Bitmap
import com.example.client_leger.classes.CallbackManager
import com.example.client_leger.classes.Subscription
import com.example.client_leger.services.CanvasSizeService

class DrawingEventWrapper (var drawingEvent: DrawingEvent? = null) {
    val event: CallbackManager = CallbackManager()
    var isSelected: Boolean = false
    var selectedBy: Int = -1;
    var boundingBox: BoundingBox? = null
    var image: Bitmap? = null
    var renderFct: ((DrawingEventWrapper) -> Boolean)? = null
    var redrawFct: ((DrawingEvent) -> Unit)? = null

    init {
        val evt = drawingEvent
        if (evt != null && evt.data.boundingBox != null) {
            boundingBox = evt.data.boundingBox
        } else {
            boundingBox = BoundingBox(0f, 0f, 0f, 0f)
        }

        image = Bitmap.createBitmap(CanvasSizeService.canvasSize.x.toInt(), CanvasSizeService.canvasSize.y.toInt(), Bitmap.Config.ARGB_8888)
    }

    fun setBaseImage(image: Bitmap) {
        this.image = image
    }

    fun setImageData(image: Bitmap) {
        this.image = image;
        if (!isSelected) {
            event.callback()
        }
    }

    fun renderLayer(): Boolean {
        val fct = this.renderFct
        if (fct != null)
            return fct(this)

        return false
    }

    fun redraw(): Boolean {
        val fct = redrawFct
        val evt = drawingEvent
        if (fct != null && evt != null) {
            fct(evt)
            return true
        }

        return false
    }

    fun subscribe(runnable: () -> Unit): Subscription {
        return this.event.addSubscriber(runnable)
    }

    fun unsubscribe(subscription: Subscription): Boolean {
        if (subscription.callbackManager == this.event) {
            subscription.unsubscribe()
            return true
        }
        return false
    }
}