package com.example.client_leger.classes.apiEvents

import com.example.client_leger.classes.Vec2

class SelectionStartEvent(val position: Vec2, val size: Vec2, val isShape: Boolean, val text: String? = null): AppEvent() {
    override val type = AppEventType.SELECTION_START_EVENT
}