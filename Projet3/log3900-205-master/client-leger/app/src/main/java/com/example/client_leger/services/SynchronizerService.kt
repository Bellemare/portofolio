package com.example.client_leger.services

import android.graphics.Bitmap
import android.os.Handler
import android.util.Base64
import com.example.client_leger.classes.Authentication
import com.example.client_leger.classes.apiEvents.AppEvents
import com.example.client_leger.classes.apiEvents.ClientStateEvent
import com.example.client_leger.classes.apiEvents.ClientStateSyncEvent
import com.example.client_leger.classes.apiEvents.DrawingErrorEvent
import com.example.client_leger.classes.drawing.*
import com.example.client_leger.classes.sockets.*
import com.example.client_leger.services.couches.CouchesService
import com.shopify.promises.Promise
import kotlinx.coroutines.runBlocking
import java.io.ByteArrayOutputStream


object SynchronizerService {
    private var conn: SocketClient? = null
    private var drawingId: Int = 0

    private var layerEvents: HashMap<Int, DrawingEventWrapper> = hashMapOf()

    private var previewEvents: HashMap<Int, DrawingEventPayload> = hashMapOf()
    private var previousEvent: DrawingElement? = null

    var redrawHandler: ((save: Boolean) -> Unit)? = null
    var mainHandler: Handler? = null

    var layerSelectEvent: ((event: DrawingEventWrapper) -> Unit)? = null
    var layerDeleteEvent: (() -> Unit)? = null

    fun init(drawingId: Int) {
        DrawingApiService.leaveSession(drawingId)
        this.drawingId = drawingId
        DrawingApiService.joinCollaborativeSession(drawingId).whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    conn = it.value
                    RoomService.addDrawingRoom(conn as SocketClient)
                    it.value.onMessage(PayloadTypes.DRAWING_EVENT) { payload -> onNewDrawingEvent(payload as DrawingEventPayload) }
                    it.value.onMessage(PayloadTypes.DRAWING_PREVIEW_EVENT) { payload -> onNewDrawingPreviewEvent(payload as DrawingEventPayload) }
                    it.value.onMessage(PayloadTypes.DRAWING_HISTORY) { payload -> onDrawingHistory(payload as DrawingHistoryPayload) }
                    it.value.onMessage(PayloadTypes.LAYER_EVENT) { payload -> onNewLayerEvent(payload as LayerEventPayload) }
                    it.value.onMessage(PayloadTypes.LAYER_TEXT_EVENT) { payload -> onNewLayerTextEvent(payload as LayerTextEventPayload) }
                    it.value.onMessage(PayloadTypes.LAYER_PREVIEW_EVENT) { payload -> onNewLayerPreviewEvent(payload as LayerPreviewEventPayload) }
                    it.value.onMessage(PayloadTypes.LAYER_MOVE_EVENT) { payload -> onLayerMoveEvent(payload as LayerMoveEventPayload) }
                    it.value.onMessage(PayloadTypes.KICK_EVENT) { payload ->
                        payload as KickEventPayload
                        AppEvents.emitEvent(DrawingErrorEvent(payload.reason))
                    }
                    it.value.onMessage(PayloadTypes.MESSAGE_HISTORY){payload ->  RoomService.setDrawingRoomMessageHistory(payload as MessageHistoryPayload)}
                    it.value.onMessage(PayloadTypes.CLIENT_STATE_EVENT){payload ->
                        RoomService.updateClientSyncs(payload as ClientStateEventPayload)
                        AppEvents.emitEvent(ClientStateEvent(payload as ClientStateEventPayload))
                    }
                    it.value.onMessage(PayloadTypes.CLIENT_STATE_SYNC_EVENT){ payload ->
                        RoomService.setClientSyncs(payload as ClientStateSyncEventPayload)
                        AppEvents.emitEvent(ClientStateSyncEvent(payload as ClientStateSyncEventPayload))
                    }
                }
                is Promise.Result.Error -> {
                    AppEvents.emitEvent(DrawingErrorEvent("Impossible de se connecter au dessin"))
                }
            }
        }
    }

    fun onDestroy() {
        DrawingApiService.leaveSession(drawingId)
        layerEvents = hashMapOf()
        previewEvents = hashMapOf()
        previousEvent = null
        RoomService.removeDrawingRoom()
    }

    fun getLayerEvents(): HashMap<Int, DrawingEventWrapper> {
        return layerEvents
    }

    fun getPreviewEvents(): HashMap<Int, DrawingEventPayload> {
        return previewEvents
    }

    private fun onDrawingHistory(history: DrawingHistoryPayload) {
        DrawingService.initLayerTree(history)
    }

    private fun onNewLayerEvent(payload: LayerEventPayload) {
        when (payload.type) {
            LayerEventType.SELECT.value -> onSelectLayer(payload)
            LayerEventType.CHANGE.value -> onLayerChange(payload)
            LayerEventType.DELETE.value -> onLayerDelete(payload)
        }
    }

    private fun redraw(userId: Int?) {
        val handler = redrawHandler
        if (handler != null) {
            handler(userId == Authentication.getUser()?.id)
        }
    }

    private fun onLayerChange(payload: LayerEventPayload) {
        val events = layerEvents
        var layer = events.get(payload.id)
        if (!events.containsKey(payload.id)) {
            val layer = CouchesService.getElementFromId(payload.id)
            updateLayerChange(layer, payload)
        } else {
            updateLayerChange(layer!!, payload)
        }
    }

    private fun updateLayerChange(layer: DrawingEventWrapper, payload: LayerEventPayload) {
        layer.isSelected = false
        layer.drawingEvent = payload.event
        layer.renderLayer()
        redraw(payload.userId)
        layerEvents.remove(payload.id)
        runBlocking {
            DrawingService.launchPreviewRefresh()
        }
    }

    private fun onLayerDelete(payload: LayerEventPayload) {
        layerEvents.remove(payload.id)
        CouchesService.removeLayer(payload.layer)
        if (payload.userId == Authentication.getUser()?.id) {
            layerDeleteEvent!!()
        }
        redraw(payload.userId)
        runBlocking {
            DrawingService.launchPreviewRefresh()
        }
    }

    private fun onSelectLayer(payload: LayerEventPayload) {
        val layer = CouchesService.getElementFromId(payload.id)
        if (layer != null) {
            if (payload.userId == Authentication.getUser()?.id) {
                layerSelectEvent!!(layer)
            } else {
                setLayerSelect(layer, payload.id, payload.userId as Int)
            }
        }
    }

    private fun onNewLayerTextEvent(payload: LayerTextEventPayload) {
        val events = layerEvents
        val layer = events.get(payload.id)
        if (layer == null) {
            val layer = CouchesService.getElementFromId(payload.id)
            setLayerSelect(layer, payload.id, payload.userId as Int)
            updateTextEvent(layer, payload)
        } else {
            updateTextEvent(layer, payload)
        }
    }

    private fun updateTextEvent(layer: DrawingEventWrapper, payload: LayerTextEventPayload) {
        val data = layer.drawingEvent?.data
        if (data is DrawingShape) {
            data.text = payload.text
            layer.renderLayer()
            runBlocking {
                DrawingService.launchPreviewRefresh()
            }
        }
    }

    private fun onLayerMoveEvent(payload: LayerMoveEventPayload) {
        CouchesService.moveLayer(payload.layer, payload.newLayer)
        redraw(payload.userId)
        runBlocking {
            DrawingService.launchPreviewRefresh()
        }
    }

    private fun setLayerSelect(event: DrawingEventWrapper, id: Int, userId: Int) {
        layerEvents.put(id, event)
        event.isSelected = true
        event.selectedBy = userId
        event.event.callback()
        redraw(null)
        runBlocking {
            DrawingService.launchPreviewRefresh()
        }
    }

    private fun onNewLayerPreviewEvent(payload: LayerPreviewEventPayload) {
        val events = layerEvents
        var layer = events.get(payload.id)
        if (layer == null) {
            val layer = CouchesService.getElementFromId(payload.id)
            updatePreviewEvent(layer, payload)
        } else {
            updatePreviewEvent(layer, payload)
        }
    }

    private fun updatePreviewEvent(layer: DrawingEventWrapper, payload: LayerPreviewEventPayload) {
        val transform = payload.transform
        val drawingEvent = layer.drawingEvent
        if (drawingEvent != null) {
            drawingEvent.attributes.lineWidth = payload.attributes.lineWidth
            drawingEvent.strokeColor = payload.strokeColor
            drawingEvent.fillColor = payload.fillColor
            drawingEvent.textColor = payload.textColor
            val data = drawingEvent.data
            val currentBox = data.editBoundingBox()
            if (currentBox == null) {
                return
            }
            val dw = transform.dw / currentBox.w
            val dh = transform.dh / currentBox.h
            val dx = transform.dx - currentBox.x
            val dy = transform.dy - currentBox.y
            data.applyScaling(dw, dh, transform.flipX, transform.flipY, payload.attributes)
            data.applyTranslation(dx, dy)
            data.boundingBox = null
            layer.redraw()
            runBlocking {
                DrawingService.launchPreviewRefresh()
            }
        }
    }

    private fun onNewDrawingPreviewEvent(payload: DrawingEventPayload) {
        if (!previewEvents.containsKey(payload.userId)) {
            previewEvents[payload.userId] = payload
        } else {
            val evt = previewEvents[payload.userId]!!
            if (!evt.event.data.handleNewEvent(payload)) {
                previewEvents[payload.userId] = payload
            }
        }
        runBlocking {
            DrawingService.launchPreviewRefresh()
        }
    }

    private fun onNewDrawingEvent(payload: DrawingEventPayload) {
        previewEvents.remove(payload.userId)
        DrawingService.drawCollaboratorEvent(payload)
    }

    fun sendEvent(event: DrawingEvent) {
        val payload = DrawingEventPayload(event, 0, "")
        if (event.type == EventType.PREVIEW.value) {
            val previous = previousEvent
            previousEvent = event.data.deepCopy()
            event.data.createNewEvent(previous)
            conn?.send(payload, PayloadTypes.DRAWING_PREVIEW_EVENT)
        } else {
            previousEvent = null
            conn?.send(payload, PayloadTypes.DRAWING_EVENT)
        }
    }

    fun layerTextEvent(payload: LayerTextEventPayload) {
        conn?.send(payload, PayloadTypes.LAYER_TEXT_EVENT)
    }

    fun saveDrawing(bitmap: Bitmap) {
        val payload = DrawingSavePayload("")
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
        val byteArray: ByteArray = byteArrayOutputStream.toByteArray()
        payload.image = "data:image/png;base64,${Base64.encodeToString(byteArray, Base64.DEFAULT)}"

        conn?.send(payload, PayloadTypes.DRAWING_SAVE)
    }

    fun layerEvent(type: LayerEventType, event: DrawingEventWrapper, layer: Int) {
        val id = event.drawingEvent!!.id
        if (id != null) {
            val payload = LayerEventPayload(event.drawingEvent!!, id, layer, type.value)
            conn?.send(payload, PayloadTypes.LAYER_EVENT)
        }
    }

    fun layerPreview(payload: LayerPreviewEventPayload) {
        conn?.send(payload, PayloadTypes.LAYER_PREVIEW_EVENT)
    }

    fun moveLayer(newIndex: Int, oldIndex: Int) {
        val id = CouchesService.getElement(oldIndex).drawingEvent?.id
        if (id != null) {
            var afterId = -1
            if (newIndex > 0) {
                if (newIndex > oldIndex) {
                    afterId = CouchesService.getElement(newIndex).drawingEvent?.id as Int
                } else {
                    afterId = CouchesService.getElement(newIndex - 1).drawingEvent?.id as Int
                }
            }
            val payload = LayerMoveEventPayload(id, oldIndex, newIndex, afterId)
            conn?.send(payload, PayloadTypes.LAYER_MOVE_EVENT)
        }
    }
}