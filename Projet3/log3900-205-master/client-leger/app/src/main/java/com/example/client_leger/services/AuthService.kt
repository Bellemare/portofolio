package com.example.client_leger.services

import com.example.client_leger.classes.AuthRequest
import com.example.client_leger.classes.AuthToken
import com.example.client_leger.classes.RequestConfig
import com.example.client_leger.classes.RequestResponse
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.coroutines.awaitObjectResult
import com.github.kittinunf.result.Result
import com.github.kittinunf.result.map

class AuthService: ApiService() {
    val BASE_URL = "/Auth"

    suspend fun login(req: AuthRequest): Result<AuthToken, FuelError> {
        return handleResponse(post(RequestConfig(url=BASE_URL), req).awaitObjectResult(AuthToken.Deserializer()))
    }

    suspend fun logout(): Result<RequestResponse, FuelError> {
        return handleResponse(post(RequestConfig(url="${BASE_URL}/Disconnect"), null).awaitObjectResult(RequestResponse.Deserializer()))
    }

}