package com.example.client_leger.classes.sockets

class KickEventPayload(val reason: String): Payload()
class SocketKickEventPayload(type: Int, payload: KickEventPayload): SocketPayload(type, payload)
