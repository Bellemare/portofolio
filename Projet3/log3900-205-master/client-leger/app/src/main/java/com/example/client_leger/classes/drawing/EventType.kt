package com.example.client_leger.classes.drawing

enum class EventType(val value: Int) {
    PREVIEW(1),
    DRAW(2);
}