package com.example.client_leger.classes

import com.example.client_leger.getApiMapper
import com.github.kittinunf.fuel.core.ResponseDeserializable
import java.util.*

data class CollaborationSession(
    val userId: Int,
    val drawingId: Int,
    val drawing: String,
    val albumId: Int,
    val album: String,
    val isPasswordProtected: Boolean,
    val editedDrawing: Boolean,
    val startTime: Date,
    val endTime: Date
) {
    class ListDeserializer : ResponseDeserializable<ArrayList<CollaborationSession>> {
        override fun deserialize(content: String): ArrayList<CollaborationSession> {
            val mapper = getApiMapper()
            return mapper.readValue(content, mapper.typeFactory.constructCollectionType(ArrayList::class.java, CollaborationSession::class.java))
        }
    }
}
