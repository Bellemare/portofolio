package com.example.client_leger.classes.sockets

enum class ClientStateType(val value: Int) {
    CONNECT(1),
    DISCONNECT(2),
    CHANGE(3),
}