package com.example.client_leger.services

import com.example.client_leger.classes.*
import com.example.client_leger.getApiMapper
import com.example.client_leger.views.AvatarView
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.coroutines.awaitObjectResult
import com.github.kittinunf.result.Result

object UserService : ApiService() {
    val BASE_URL = "/User"

    suspend fun getCurrentUser(): Result<User, FuelError> {
        return handleResponse(get(RequestConfig(url="${BASE_URL}/Current")).awaitObjectResult(User.Deserializer()))
    }

    suspend fun getDefaultAvatars(): Result<ArrayList<String>, FuelError> {
        return handleResponse(get(RequestConfig(url="${BASE_URL}/DefaultAvatars")).awaitObjectResult(DefaultAvatarList.AvatarListDeserializer()))
    }

    suspend fun getUser(id: Int): Result<User, FuelError> {
        return handleResponse(get(RequestConfig(url="${BASE_URL}/${id}")).awaitObjectResult(User.Deserializer()))
    }

    suspend fun getConnectionHistory(): Result<ArrayList<ConnectionHistory>, FuelError> {
        return handleResponse(get(RequestConfig(url="${BASE_URL}/ConnectionHistory")).awaitObjectResult(ConnectionHistory.ListDeserializer()))
    }

    suspend fun getCollaborativeSessions(): Result<ArrayList<CollaborationSession>, FuelError> {
        return handleResponse(get(RequestConfig(url="${BASE_URL}/CollaborativeSessions")).awaitObjectResult(CollaborationSession.ListDeserializer()))
    }

    suspend fun getUserMetrics(): Result<UserMetrics, FuelError> {
        return handleResponse(get(RequestConfig(url="${BASE_URL}/Metrics")).awaitObjectResult(UserMetrics.Deserializer()))
    }

    suspend fun register(req: RegisterRequest): Result<User, FuelError> {
        return handleResponse(post(RequestConfig(url=BASE_URL), req).awaitObjectResult(User.Deserializer()))
    }

    suspend fun updateUser(user: User): Result<RequestResponse, FuelError> {
        return handleResponse(put(RequestConfig(url="${BASE_URL}/${user.id}"), user).awaitObjectResult(RequestResponse.Deserializer()))
    }


}