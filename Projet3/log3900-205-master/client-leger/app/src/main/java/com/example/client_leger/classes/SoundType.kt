package com.example.client_leger.classes;

import com.example.client_leger.R;

enum class SoundType(val sound: Int) {
    NewMessage(R.raw.mixkitmessage),
    NewCollaborator(R.raw.mixkitpositive),
    NewContact(R.raw.mixkitstopwatch),
    NewJoinRequest(R.raw.mixkitcorrect)
}
