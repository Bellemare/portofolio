package com.example.client_leger.fragments

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.*
import androidx.navigation.Navigation
import com.example.client_leger.R
import com.example.client_leger.classes.ViewDisabler
import com.example.client_leger.classes.apiEvents.AppEventType
import com.example.client_leger.classes.apiEvents.AppEvents
import com.example.client_leger.classes.AlbumJoinRequest
import com.example.client_leger.classes.sockets.PayloadTypes
import com.example.client_leger.fragments.chat.ChatWindowFragment
import com.example.client_leger.services.RoomService
import com.example.client_leger.viewModels.SideBarViewModel
import com.example.client_leger.views.AvatarView
import com.shopify.promises.Promise
import kotlinx.coroutines.runBlocking

class SideBarFragment : Fragment(), ViewDisabler {
    private lateinit var avatarImageView: AvatarView
    private val viewModel: SideBarViewModel = SideBarViewModel()

    private lateinit var notificationButton: NotificationButtonFragment
    private lateinit var contactButton: ContactButtonFragment

    private var avatarListenerId: AppEvents.IntWrapper = AppEvents.IntWrapper(0)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_side_bar, container, false)

        avatarImageView = view.findViewById(R.id.avatarImage)
        avatarImageView.setOnClickListener {navigateToUserFragment(view)}

        loadAvatar()
        setUpContactButton()
        setUpNotifications()

        if(parentFragment is DrawingRoomFragment) {
            view.findViewById<FragmentContainerView>(R.id.logoutBtnContainer).visibility = View.GONE
        }

        return view
    }

    override fun onStart() {
        super.onStart()
        listenToAvatarChange()
        notificationButton = childFragmentManager.findFragmentByTag("notificationButton") as NotificationButtonFragment
        contactButton = childFragmentManager.findFragmentByTag("contactButton") as ContactButtonFragment
    }

    private fun setUpContactButton() {
        childFragmentManager.commit {
            setReorderingAllowed(true)
            add<ContactButtonFragment>(R.id.contactButtonContainer, "contactButton")
        }
    }

    private fun setUpNotifications() {
        childFragmentManager.commit {
            setReorderingAllowed(true)
            add<NotificationButtonFragment>(R.id.notificationButtonContainer, "notificationButton")
        }
        listenToRefreshNotification()
    }

    private fun loadAvatar() {
        viewModel.getCurrentUser().whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    initAvatarImage(it.value.avatar)
                }
                is Promise.Result.Error -> {
                    println(it.error.message)
                }
            }
        }
    }

    private fun listenToAvatarChange() {
        runBlocking {
            AppEvents.listenTo(AppEventType.AVATAR_CHANGE_EVENT, avatarListenerId) {
                loadAvatar()
            }
        }
    }

    override fun onStop() {
        super.onStop()
        runBlocking {
            AppEvents.unregister(avatarListenerId.value, AppEventType.AVATAR_CHANGE_EVENT)
        }
    }

    private fun initAvatarImage(avatar: String) {
        avatarImageView.initImage(avatar, null)
        avatarImageView.postInvalidate()
    }

    fun inflateChat() {
        parentFragmentManager.commit {
            setReorderingAllowed(true)
            add<ChatWindowFragment>(R.id.chatWindowContainerView, "ChatWindowFragment")
        }
    }

    private fun navigateToUserFragment(inflaterView: View) {
        Navigation.findNavController(inflaterView).navigate(R.id.userFragment)
    }

    fun removeActionFragment() {
        val actionFragment = childFragmentManager.findFragmentByTag("sideBarAction")
        when(actionFragment) {
            is ContactListFragment -> notificationButton?.isEnabled = false
            is ContactAddFragment -> notificationButton?.isEnabled = false
            is NotificationFragment -> contactButton?.isEnabled = false
            null -> {
                notificationButton?.isEnabled = false
                contactButton?.isEnabled = false
            }
        }

        if(actionFragment != null) {
            childFragmentManager.commit {
                remove(actionFragment)
            }
        }
    }

    fun inflateContactList() {
        removeActionFragment()
        childFragmentManager.commit {
            add<ContactListFragment>(R.id.sideBarActionContainer, "sideBarAction")
        }
    }

    fun inflateContactAdd() {
        removeActionFragment()
        childFragmentManager.commit {
            add<ContactAddFragment>(R.id.sideBarActionContainer, "sideBarAction")
        }
    }

    fun removeConfirmPromptFragment() {
        val confirmPromptView = childFragmentManager.findFragmentByTag("confirmPrompt")
        if (confirmPromptView != null) {
            childFragmentManager.commit {
                remove(confirmPromptView)
            }
            enableDisableMainView(true)
        }
    }

    private fun listenToRefreshNotification() {
        RoomService.getAppClient().whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    it.value.onMessage(PayloadTypes.REFRESH_ALBUM_JOIN_REQUESTS) { payload ->
                        Handler(Looper.getMainLooper()).post {
                            if(notificationButton == null) {
                                notificationButton = childFragmentManager.findFragmentByTag("notificationButton") as NotificationButtonFragment
                            }
                            notificationButton!!.newNotificationReceived()
                        }
                    }

                }
                is Promise.Result.Error -> {
                    println(it.error)
                }
            }
        }
    }

    fun inflateConfirmPrompt(contactName: String) {
        val bundle: Bundle = bundleOf(Pair("confirmMessage", contactName))
        childFragmentManager.commit {
            add<ContactDeleteFragment>(R.id.confirmPromptFragmentContainer, "confirmPrompt", bundle)
        }
        enableDisableMainView(false)
    }

    fun deleteContact() {
        removeConfirmPromptFragment()
        val contactListView = childFragmentManager.findFragmentByTag("sideBarAction") as ContactListFragment
        contactListView.deleteContact()
    }

    fun enableDisableMainView(isEnabled: Boolean) {
        enableDisableViewGroup(requireView().findViewById(R.id.mainView), isEnabled)

        when(parentFragment) {
            is AlbumMenuFragment -> (parentFragment as AlbumMenuFragment).enableDisableAlbumMenuView(isEnabled)
            is ContactFragment -> (parentFragment as ContactFragment).enableDisableContactView(isEnabled)
            is UserFragment -> (parentFragment as UserFragment).enableDisableUserView(isEnabled)
        }
    }

}
