package com.example.client_leger.classes

import android.os.Parcel
import android.os.Parcelable
import com.example.client_leger.getApiMapper
import com.github.kittinunf.fuel.core.ResponseDeserializable

data class Drawing(
    val id: Int,
    val created_at: String,
    var active_collaborators: Int,
    val name: String,
    val owner_id: Int,
    val owner: String,
    val album_id: Int,
    val image: String,
    val was_transfered: Boolean?,
    val isExposed: Boolean,
    val isPasswordProtected: Boolean) {
    class Deserializer : ResponseDeserializable<Drawing> {
        override fun deserialize(content: String): Drawing =
            getApiMapper().readValue(content, Drawing::class.java)
    }

    class DrawingListDeserializer : ResponseDeserializable<ArrayList<Drawing>> {
        override fun deserialize(content: String): ArrayList<Drawing> {
            val mapper = getApiMapper()
            return mapper.readValue(content, mapper.typeFactory.constructCollectionType(ArrayList::class.java, Drawing::class.java))
        }
    }
}

data class DrawingModel(val name: String, val album_id: Int, val id: Int?, val password: String?): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString().toString(),
        parcel.readInt(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString().toString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeInt(album_id)
        parcel.writeValue(id)
        parcel.writeString(password)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DrawingModel> {
        override fun createFromParcel(parcel: Parcel): DrawingModel {
            return DrawingModel(parcel)
        }

        override fun newArray(size: Int): Array<DrawingModel?> {
            return arrayOfNulls(size)
        }
    }

}