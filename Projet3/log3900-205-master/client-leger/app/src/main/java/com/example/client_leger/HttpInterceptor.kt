package com.example.client_leger

import com.example.client_leger.classes.Authentication
import com.github.kittinunf.fuel.core.Request

object HttpInterceptor {
    fun httpRequestInterceptor() = {
        next: (Request) -> Request ->
        { req: Request ->
            val authToken = Authentication.getAuthenticationToken()
            if(authToken != null) {
                req.header(mapOf("Authorization" to "Bearer ${authToken.token}"))
            }
            next(req)
        }
    }
}
