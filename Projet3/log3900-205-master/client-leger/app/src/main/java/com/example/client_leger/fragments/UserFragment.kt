package com.example.client_leger.fragments

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.core.os.bundleOf
import androidx.fragment.app.*
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.client_leger.R
import com.example.client_leger.classes.*
import com.example.client_leger.classes.apiEvents.AppEventType
import com.example.client_leger.classes.apiEvents.AppEvents
import com.example.client_leger.classes.apiEvents.AvatarChangeEvent
import com.example.client_leger.services.UserService
import com.example.client_leger.viewModels.UserViewModel
import com.example.client_leger.views.AvatarView
import com.shopify.promises.Promise
import java.lang.Exception
import java.nio.charset.Charset

class UserFragment : Fragment(), ViewDisabler, OnClickAvatar {

    private lateinit var sidebarContainer: FragmentContainerView
    private lateinit var acceptPseudonymChangeBtn: Button
    private lateinit var cancelBtn: Button
    private lateinit var editPseudonymIconBtn: Button
    private lateinit var homeBtn: Button
    private lateinit var selectAvatarBtn: Button
    private lateinit var removePseudonymChangeBtn: Button
    private lateinit var privateCheckcircle: CheckBox
    private lateinit var publicCheckcircle: CheckBox
    private lateinit var emailText: TextView
    private lateinit var pseudonymEditText: EditText

    private lateinit var avatarAdapter: DefaultAvatarListAdapter
    private lateinit var avatarView: AvatarView
    private lateinit var avatarRecyclerView: RecyclerView
    private lateinit var currentUser: User
    private lateinit var defaultAvatarsLinearLayout: LinearLayout

    private lateinit var authorDrawingCount: TextView
    private lateinit var avgCollabTime: TextView
    private lateinit var collabDrawingCount: TextView
    private lateinit var metrics: UserMetrics
    private lateinit var privateMemberAlbumCount: TextView
    private lateinit var totalCollabTime: TextView

    private val viewModel: UserViewModel by activityViewModels()
    private var selectedPositon: Int = -1
    private var isSelectionOpen: Boolean = false
    private var avatarArray: ArrayList<String> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_user, container, false)

        initReferences(view)
        setUpAvatarRecyclerView(view)
        initReferencesListener(view)
        loadUser()
        getDefaultAvatars()
        getStats()

        return view
    }

    private fun initReferences(view: View) {
        sidebarContainer = view.findViewById(R.id.sideBarContainer)
        acceptPseudonymChangeBtn = view.findViewById(R.id.acceptPseudonymChangeBtn)
        cancelBtn = view.findViewById(R.id.cancelBtn)
        editPseudonymIconBtn = view.findViewById(R.id.editPseudonymIconBtn)
        homeBtn = view.findViewById(R.id.homeBtn)
        selectAvatarBtn = view.findViewById(R.id.selectAvatarBtn)
        removePseudonymChangeBtn = view.findViewById(R.id.removePseudonymChangeBtn)

        privateCheckcircle = view.findViewById(R.id.privateCheckcircle)
        publicCheckcircle = view.findViewById(R.id.publicCheckcircle)

        emailText = view.findViewById(R.id.emailText)
        pseudonymEditText = view.findViewById(R.id.pseudonymEditText)

        authorDrawingCount = view.findViewById(R.id.authorDrawingCount)
        avgCollabTime = view.findViewById(R.id.avgCollabTime)
        collabDrawingCount = view.findViewById(R.id.collabDrawingCount)
        privateMemberAlbumCount = view.findViewById(R.id.privateMemberAlbumCount)
        totalCollabTime = view.findViewById(R.id.totalCollabTime)

        avatarView = view.findViewById(R.id.avatar)
        defaultAvatarsLinearLayout = view.findViewById(R.id.defaultAvatarsLinearLayout)
    }

    private fun setUpAvatarRecyclerView(view: View) {
        avatarRecyclerView = view.findViewById(R.id.imageChoiceList)
        avatarAdapter = DefaultAvatarListAdapter(avatarArray, this)
        avatarRecyclerView.adapter = avatarAdapter
        avatarRecyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
    }

    private fun initReferencesListener(view: View) {
        acceptPseudonymChangeBtn.setOnClickListener { acceptPseudonymChange(view) }
        avatarView.setOnClickListener { openCloseAvatarSelection() }
        cancelBtn.setOnClickListener { openCloseAvatarSelection() }
        editPseudonymIconBtn.setOnClickListener { activatePseudonymEditText() }
        homeBtn.setOnClickListener { navigateToHomeFragment(view) }
        removePseudonymChangeBtn.setOnClickListener { removePseudonymChange() }
        selectAvatarBtn.setOnClickListener { selectAvatar(view) }

        privateCheckcircle.setOnClickListener { toggleEmailCheckCircle(view,false) }
        publicCheckcircle.setOnClickListener { toggleEmailCheckCircle(view,true) }

        pseudonymEditText.setOnFocusChangeListener { view, hasFocus -> changePseudonymEdition(hasFocus) }
    }

    private fun loadUser() {
        viewModel.getCurrentUser().whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    currentUser = it.value
                    emailText.text = it.value.email
                    pseudonymEditText.setText(it.value.pseudonym)
                    privateCheckcircle.isChecked = !it.value.isPublicEmail
                    publicCheckcircle.isChecked = it.value.isPublicEmail
                    initAvatarImage(it.value?.avatar)
                }
                is Promise.Result.Error -> {
                    println(it.error.message)
                }
            }
        }
    }

    private fun getDefaultAvatars() {
        avatarArray.clear()
        viewModel.getDefaultAvatars().whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    avatarArray.addAll(it.value)
                    avatarAdapter.notifyDataSetChanged()
                }
                is Promise.Result.Error -> {
                    println(it.error.message)
                }
            }
        }
    }

    private fun getStats() {
        getUserMetrics()
        inflateConnectionHistory()
        inflateEditionHistory()
    }

    private fun getUserMetrics() {
        viewModel.getUserMetrics().whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    metrics = it.value
                    authorDrawingCount.text = metrics.ownedDrawings.toString()
                    avgCollabTime.text = formatSeconds(metrics.averageCollaborationTime)
                    collabDrawingCount.text = metrics.collaboratedDrawings.toString()
                    privateMemberAlbumCount.text = metrics.memberAlbums.toString()
                    totalCollabTime.text = formatSeconds(metrics.totalCollaborationTime)
                }
                is Promise.Result.Error -> {
                    println(it.error.message)
                }
            }
        }
    }

    private fun inflateConnectionHistory() {
        childFragmentManager.commit {
            add<HistoryConnectionListFragment>(R.id.connectionContainer, "connectionHistory")
        }
    }

    private fun inflateEditionHistory() {
        childFragmentManager.commit {
            add<HistoryEditionListFragment>(R.id.editionContainer, "editionHistory")
        }
    }

    private fun formatSeconds(seconds: Int): String {
        val minutes: Int = Math.floor(seconds / 60.0).toInt();
        val remaining: Int = seconds % 60;

        if (minutes > 0)
            return String.format("%s min %s s", minutes, remaining)
        else
            return String.format("%s s", remaining)
    }

    private fun acceptPseudonymChange(view: View) {
        resetPseudonymEditTextFocus()
        val editedUser: User = currentUser.deepCopy()
        editedUser.pseudonym = pseudonymEditText.text.toString()
        viewModel.updateUser(editedUser).whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    currentUser.pseudonym = editedUser.pseudonym
                    pseudonymEditText.setText(editedUser.pseudonym)
                    hideKeyboard()

                    it.value.displayResponse(view)
                }
                is Promise.Result.Error -> {
                    //handle error
                    try {
                        val error = RequestResponse.Deserializer().deserialize(it.error.errorData.toString(
                            Charset.defaultCharset()))
                        error.displayResponse(view)
                    }
                    catch (e: Exception) {
                        val error = RequestFieldErrorResponse.Deserializer().deserialize(it.error.errorData.toString(
                            Charset.defaultCharset()))
                        error.displayError(view)
                    }
                }
            }
        }
    }

    private fun openCloseAvatarSelection() {
        if(isSelectionOpen) {
            defaultAvatarsLinearLayout.visibility = View.INVISIBLE
        }
        else {
            defaultAvatarsLinearLayout.visibility = View.VISIBLE
        }
        isSelectionOpen = !isSelectionOpen
    }

    private fun navigateToHomeFragment(inflaterView: View) {
        Navigation.findNavController(inflaterView).navigate(R.id.albumMenuFragment)
    }

    private fun selectAvatar(view: View) {
        if(selectedPositon >= 0 && selectedPositon < avatarArray.size) {
            acceptAvatarChange(view)
            openCloseAvatarSelection()
        }
        else {
            println("error no avatar selected")
        }
    }

    private fun acceptAvatarChange(view: View) {
        val editedUser: User = currentUser.deepCopy()
        editedUser.avatar = avatarArray[selectedPositon]
        viewModel.updateUser(editedUser).whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    currentUser.avatar = editedUser.avatar
                    AppEvents.emitEvent(AvatarChangeEvent())
                    initAvatarImage(currentUser.avatar)

                    it.value.displayResponse(view)
                }
                is Promise.Result.Error -> {
                    //handle error
                    try {
                        val error = RequestResponse.Deserializer().deserialize(it.error.errorData.toString(
                            Charset.defaultCharset()))
                        error.displayResponse(view)
                    }
                    catch (e: Exception) {
                        val error = RequestFieldErrorResponse.Deserializer().deserialize(it.error.errorData.toString(
                            Charset.defaultCharset()))
                        error.displayError(view)
                    }
                }
            }
        }
    }

    private fun toggleEmailCheckCircle(view: View, isPublic: Boolean) {
        val editedUser: User = currentUser.deepCopy()
        editedUser.isPublicEmail = isPublic
        viewModel.updateUser(editedUser).whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    currentUser.isPublicEmail = isPublic
                    privateCheckcircle.isChecked = !isPublic
                    publicCheckcircle.isChecked = isPublic

                    it.value.displayResponse(view)
                }
                is Promise.Result.Error -> {
                    //handle error
                    try {
                        val error = RequestResponse.Deserializer().deserialize(it.error.errorData.toString(
                            Charset.defaultCharset()))
                        error.displayResponse(view)
                    }
                    catch (e: Exception) {
                        val error = RequestFieldErrorResponse.Deserializer().deserialize(it.error.errorData.toString(
                            Charset.defaultCharset()))
                        error.displayError(view)
                    }
                }
            }
        }
    }

    override fun OnClickAvatar(position: Int) {
        selectedPositon = position
    }

    private fun initAvatarImage(avatar: String) {
        avatarView.initImage(avatar, Vec2(300f,300f))
        avatarView.postInvalidate()
    }

    private fun activatePseudonymEditText() {
        pseudonymEditText.requestFocus()
        showKeyboard()
    }

    private fun removePseudonymChange() {
        resetPseudonymEditTextFocus()
        pseudonymEditText.setText(currentUser.pseudonym)
        hideKeyboard()
    }

    private fun resetPseudonymEditTextFocus() {
        pseudonymEditText.isFocusable = false
        pseudonymEditText.isFocusableInTouchMode = true
    }

    private fun changePseudonymEdition(hasFocus: Boolean) {
        if(hasFocus) {
            acceptPseudonymChangeBtn.visibility = View.VISIBLE
            removePseudonymChangeBtn.visibility = View.VISIBLE
            editPseudonymIconBtn.visibility = View.INVISIBLE
        }
        else
        {
            acceptPseudonymChangeBtn.visibility = View.INVISIBLE
            removePseudonymChangeBtn.visibility = View.INVISIBLE
            editPseudonymIconBtn.visibility = View.VISIBLE
        }
    }

    private fun callSidebarEnableDisableView(isEnabled: Boolean) {
        // Calls sidebar to enable / disable everything (sidebarFragment + contactfragment)
        sidebarContainer.getFragment<SideBarFragment>().enableDisableMainView(isEnabled)
    }

    // On devrait probablement mettre ces fonctions répétitives ailleurs
    private fun Fragment.hideKeyboard() {
        view?.let { activity?.hideKeyboard(it) }
    }

    private fun Context.hideKeyboard(view: View) {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private fun Fragment.showKeyboard() {
        view?.let { activity?.showKeyboard(it) }
    }

    private fun Context.showKeyboard(view: View) {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }

    fun enableDisableUserView(isEnabled: Boolean) {
        enableDisableViewGroup(requireView().findViewById(R.id.mainUserView), isEnabled)
    }

    fun inflatePasswordView(session: CollaborationSession) {
        //theoretically should never be called since it is the same user
        if(childFragmentManager.findFragmentByTag("passwordFragmentContainer") == null) {
            val bundle = bundleOf(Pair("drawing", DrawingModel(session.drawing, session.albumId, session.drawingId, null)))
            childFragmentManager.commit {
                setReorderingAllowed(true)
                add<DrawingPasswordVerifyFragment>(R.id.passwordFragmentContainer, "passwordFragmentContainer", args = bundle)
            }
            callSidebarEnableDisableView(false)
        }
    }

    fun removePasswordFragment() {
        val albumPasswordFragment = childFragmentManager.findFragmentByTag("passwordFragmentContainer")
        if(albumPasswordFragment != null) {
            childFragmentManager.commit {
                setReorderingAllowed(true)
                remove(albumPasswordFragment)
            }
        }
        callSidebarEnableDisableView(true)
    }

}