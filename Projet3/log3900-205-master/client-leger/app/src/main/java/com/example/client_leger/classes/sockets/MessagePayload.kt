package com.example.client_leger.classes.sockets

import android.graphics.Bitmap
import com.example.client_leger.getApiMapper
import com.github.kittinunf.fuel.core.ResponseDeserializable
import java.util.*
import kotlin.collections.ArrayList

data class MessagePayload(
    val id: Int? = null,
    val roomId: Int? = null,
    val userId: Int? = null,
    val message: String,
    val sentAt: Date? = null,
    val message_type: Int? = null,
    val reply_to: Int? = null,
    val reply_message: String? = null,
    val recipients: Array<Int>? = null,
    val user: String? = null,
    val avatar: String? = null) : Payload(), Comparable<MessagePayload>
{
    class HistoryDeserializer : ResponseDeserializable<ArrayList<MessagePayload>> {
        override fun deserialize(content: String) : ArrayList<MessagePayload> {
            val mapper = getApiMapper()

            return mapper.readValue(content, mapper.typeFactory.constructCollectionType(ArrayList::class.java, MessagePayload::class.java))
        }
    }

    override fun compareTo(other: MessagePayload): Int {
        if (sentAt == null || other.sentAt == null)
            return 0

        return sentAt.compareTo(other.sentAt)
    }
}

class SocketMessagePayload(type: Int, payload: MessagePayload): SocketPayload(type, payload)
