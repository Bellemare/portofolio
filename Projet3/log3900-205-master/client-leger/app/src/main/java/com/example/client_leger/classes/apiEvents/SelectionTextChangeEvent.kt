package com.example.client_leger.classes.apiEvents

import com.example.client_leger.classes.Vec2

class SelectionTextChangeEvent(val text: String): AppEvent() {
    override val type = AppEventType.SELECTION_TEXT_CHANGE_EVENT
}