package com.example.client_leger.classes.apiEvents

import com.example.client_leger.classes.sockets.ClientStateEventPayload

class ClientStateEvent(val payload: ClientStateEventPayload): AppEvent() {
    override val type = AppEventType.CLIENT_STATE_EVENT
}