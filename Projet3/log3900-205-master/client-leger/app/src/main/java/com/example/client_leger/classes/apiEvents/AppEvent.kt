package com.example.client_leger.classes.apiEvents

abstract class AppEvent() {
    abstract val type: AppEventType
}