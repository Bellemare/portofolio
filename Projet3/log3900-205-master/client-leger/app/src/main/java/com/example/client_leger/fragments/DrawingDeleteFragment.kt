package com.example.client_leger.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.activityViewModels
import com.example.client_leger.R
import com.example.client_leger.classes.DrawingModel
import com.example.client_leger.classes.RequestResponse
import com.example.client_leger.viewModels.AlbumMenuViewModel
import com.shopify.promises.Promise
import java.nio.charset.Charset

class DrawingDeleteFragment : Fragment() {
    private val CONFIRM_MESSAGE: String = "Voulez-vous vraiment supprimer le dessin "

    private lateinit var confirmMessage: TextView
    private lateinit var drawing: DrawingModel

    private lateinit var deleteButton: Button
    private lateinit var cancelButton: Button

    private val viewModel: AlbumMenuViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_drawing_delete, container, false)
        drawing = requireArguments().get("drawing") as DrawingModel

        confirmMessage = view.findViewById(R.id.confirmMsg)
        confirmMessage.text = "$CONFIRM_MESSAGE${drawing.name}?\n"

        deleteButton = view.findViewById(R.id.deleteBtn)
        deleteButton.setOnClickListener {
            viewModel.deleteDrawing(drawing.id!!).whenComplete { result ->
                when(result) {
                    is Promise.Result.Success -> {
                        val requestResponse: RequestResponse = result.value
                        requestResponse.displayResponse(view)

                        removeFragment()
                    }
                    is Promise.Result.Error -> {
                        //handle error
                        val error = RequestResponse.Deserializer().deserialize(result.error.errorData.toString(
                            Charset.defaultCharset()))
                        error.displayResponse(view)
                    }
                }
            }
        }

        cancelButton = view.findViewById(R.id.cancelBtn)
        cancelButton.setOnClickListener {
            (parentFragment as AlbumInfoFragment).removeActionFragment()
        }

        return view
    }

    private fun removeFragment() {
        when(parentFragment){
            is AlbumSearchDrawingFragment -> (parentFragment as AlbumSearchDrawingFragment).removeActionFragment()
            is AlbumInfoFragment -> (parentFragment as AlbumInfoFragment).removeActionFragment()
        }
    }

}