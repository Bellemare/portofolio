package com.example.client_leger.classes

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.client_leger.R
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class HistoryEditionListAdapter (var editionList: ArrayList<CollaborationSession>, var albumList: ArrayList<Album>, var availableAlbumList: ArrayList<Album>, listener: OnClickEditionAction): RecyclerView.Adapter<HistoryEditionListAdapter.ViewHolder>() {
    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

        var editionAlbum: TextView = view.findViewById(R.id.editionAlbum)
        var editionDate: TextView = view.findViewById(R.id.editionDate)
        var editionDrawing: TextView = view.findViewById(R.id.editionDrawing)
        var actionBtn: TextView = view.findViewById(R.id.action)
    }

    var actionListener: OnClickEditionAction = listener

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.fragment_history_edition_list_item, viewGroup, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val edition: CollaborationSession = editionList[position]

        holder.editionAlbum.text = edition.album
        holder.editionDate.text = setDate(edition.startTime)
        holder.editionDrawing.text = edition.drawing

        if (isAlbumMember(edition.albumId)) {
            holder.actionBtn.text = "Éditer\n le dessin"
            holder.actionBtn.setBackgroundColor(ContextCompat.getColor(holder.itemView.context, R.color.backgroundLightColor))
            holder.actionBtn.setOnClickListener { actionListener.callJoinAlbum(edition) }
        }
        else {
            if(isPendingJoin(edition.albumId)) {
                holder.actionBtn.text = ""
                holder.actionBtn.setBackgroundColor(ContextCompat.getColor(holder.itemView.context, R.color.backgroundDarkColor))
                holder.actionBtn.setOnClickListener { null }
            }
            else {
                holder.actionBtn.text = "Rejoindre\n l'album"
                holder.actionBtn.setBackgroundColor(ContextCompat.getColor(holder.itemView.context, R.color.backgroundLighterColor))
                holder.actionBtn.setOnClickListener { actionListener.callRequestToJoin(edition.albumId) }
            }
        }
    }

    private fun setDate(date: Date?): String {
        if(date == null)
            return ""

        val dayOfWeekFormatter = SimpleDateFormat("EEEE", Locale.CANADA_FRENCH)
        dayOfWeekFormatter.timeZone = TimeZone.getTimeZone("America/New_York")

        val dayOfYearFormatter = SimpleDateFormat("dd MMMM YYYY", Locale.CANADA_FRENCH)
        dayOfYearFormatter.timeZone = TimeZone.getTimeZone("America/New_York")

        val timeFormatter = SimpleDateFormat("hh:mm:ss a", Locale.CANADA_FRENCH)
        timeFormatter.timeZone = TimeZone.getTimeZone("America/New_York")

        val dayOfWeek: String = dayOfWeekFormatter.format(date)
        val dayOfYear: String = dayOfYearFormatter.format(date)
        val time: String = timeFormatter.format(date)

        return String.format("%s %s %s", dayOfWeek, dayOfYear, time)
    }

    private fun isAlbumMember(albumId: Int): Boolean {
        return this.albumList.indexOfFirst { u -> u.id == albumId } > -1
    }

    private fun isPendingJoin(albumId: Int): Boolean {
        return !this.isAlbumMember(albumId) && this.availableAlbumList.indexOfFirst { a -> a.id == albumId } == -1
    }

    override fun getItemCount(): Int {
        return editionList.size
    }
}