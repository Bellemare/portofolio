package com.example.client_leger.services.drawingServices

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import com.example.client_leger.classes.Vec2
import com.example.client_leger.classes.drawing.*
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min
import kotlin.math.sign

class RectangleDrawingService : ToolDrawingService() {

    override fun drawEvent(canvas: Canvas, paint: Paint, event: DrawingEvent): DrawingEvent {

        if(isShouldFill(event)) {
            paint.style = Paint.Style.FILL
            paint.color = Color.argb(event.strokeColor.a * 255, event.strokeColor.r, event.strokeColor.g, event.strokeColor.b)
            canvas.drawRect(createFillRect(event), paint)
        } else {
            paint.style = Paint.Style.FILL
            paint.color = Color.argb(event.fillColor.a * 255, event.fillColor.r, event.fillColor.g, event.fillColor.b)
            canvas.drawRect(createFillRect(event), paint)

            paint.style = Paint.Style.STROKE
            paint.color = Color.argb(event.strokeColor.a * 255, event.strokeColor.r, event.strokeColor.g, event.strokeColor.b)
            canvas.drawRect(createStrokeRect(event), paint)
        }
        if (event.data.baseBoundingBox == null) {
            event.data.baseBoundingBox = setBoundingBox(event, 0.0f)
        }
        val textHeight = drawText(canvas, event)
        if (event.data.boundingBox == null) {
            event.data.boundingBox = setBoundingBox(event, textHeight)
        }

        return event
    }

    private fun drawText(canvas: Canvas, event: DrawingEvent): Float {
        val data = event.data as DrawingRect
        val lw = event.attributes.lineWidth
        val pos = Vec2(data.x + data.width / 2, data.y + lw + TextDrawingService.LINE_HEIGHT / 2 + TextDrawingService.PADDING.y + 5)
        return TextDrawingService.drawText(canvas, pos, event) + lw
    }

    private fun setBoundingBox(event: DrawingEvent, textHeight: Float): BoundingBox {
        val data = event.data as DrawingRect
        return BoundingBox(
            min(data.x, data.x + data.width),
            min(data.y, data.y + data.height),
            abs(data.width),
            max(abs(data.height), textHeight + (TextDrawingService.PADDING.y * 2)),
        )
    }

    private fun isShouldFill(event: DrawingEvent): Boolean {
        val data: DrawingRect = event.data as DrawingRect
        val adjustedWidth = Math.max(0f, Math.abs(data.width) - event.attributes.lineWidth)
        val adjustedHeight = Math.max(0f, Math.abs(data.height) - event.attributes.lineWidth)
        return event.attributes.lineWidth > adjustedWidth || event.attributes.lineWidth > adjustedHeight
    }

    private fun createStrokeRect(event: DrawingEvent): Rect {
        val data: DrawingRect = event.data as DrawingRect
        val sizeAdjustment = (event.attributes.lineWidth / 2f).toInt()

        val xDirection: Int = sign(data.width).toInt()
        val yDirection: Int = sign(data.height).toInt()

        val left: Int = data.x.toInt() + sizeAdjustment * xDirection
        val top: Int = data.y.toInt() + sizeAdjustment * yDirection
        val right: Int = data.x.toInt() + data.width.toInt() - sizeAdjustment * xDirection
        val bottom: Int = data.y.toInt() + data.height.toInt() - sizeAdjustment * yDirection

        return Rect(left, top, right, bottom)
    }

    private fun createFillRect(event: DrawingEvent): Rect {
        val data: DrawingRect = event.data as DrawingRect

        val left: Int = data.x.toInt()
        val top: Int = data.y.toInt()
        val right: Int = data.x.toInt() + data.width.toInt()
        val bottom: Int = data.y.toInt() + data.height.toInt()

        return Rect(left, top, right, bottom)
    }

    override fun isEmpty(element: DrawingElement): Boolean {
        val drawingElement = element as DrawingRect
        return abs(drawingElement.width) <= 0.5 || abs(drawingElement.height) <= 0.5
    }
}