package com.example.client_leger.classes.sockets

import android.os.Handler
import android.os.Looper
import com.example.client_leger.classes.Environment
import com.example.client_leger.classes.drawing.*
import com.example.client_leger.getApiMapper
import com.google.gson.Gson
import okhttp3.*
import org.json.JSONArray
import org.json.JSONObject
import kotlin.concurrent.thread


class SocketClient(private val client: OkHttpClient) {
    private val environment: Environment = Environment()
    private val WEBSOCKET_HOST : String = environment.websocketHost
    private val WEBSOCKET_PORT : Int = environment.defaultPort

    private lateinit var conn: WebSocket
    private val onMessageMap: HashMap<PayloadTypes, (Payload) -> Unit> = hashMapOf()
    lateinit var onClose: () -> Unit

    fun connect(uri: String, onConnect: () -> Unit) {
        var url: String = ""
        if (WEBSOCKET_PORT != 80) {
            url = "${WEBSOCKET_HOST}:${WEBSOCKET_PORT}${uri}"
        } else {
            url = "${WEBSOCKET_HOST}${uri}"
        }
        val request = Request.Builder().url(url).build()

        thread {
            client.newWebSocket(request, createListener(onConnect))
        }
    }

    private fun createListener(onConnect: () -> Unit): WebSocketListener {
        return object : WebSocketListener() {
            override fun onOpen(webSocket: WebSocket, response: Response) {
                super.onOpen(webSocket, response)
                conn = webSocket
                if (response.code() == 101) {
                    onConnect()
                }
            }

            private fun parseDrawingEvent(bytes: String): SocketPayload? {
                val parser = Gson()
                val drawingEventType = parser.fromJson(bytes, SocketDrawingEventType::class.java)
                var payload: SocketPayload? = null
                when (drawingEventType.payload.event.tool_type) {
                    ToolType.PENCIL.value -> payload = getApiMapper().readValue(bytes, SocketDrawingPosEventPayload::class.java)
                    ToolType.RECTANGLE.value -> payload = getApiMapper().readValue(bytes, SocketDrawingRectEventPayload::class.java)
                    ToolType.ELLIPSE.value -> payload = getApiMapper().readValue(bytes, SocketDrawingEllipseEventPayload::class.java)
                }

                return payload
            }

            private fun parseDrawingHistory(bytes: String): SocketPayload? {
                val drawingHistory = DrawingHistoryPayload(arrayListOf())
                val payload = SocketDrawingHistoryPayload(PayloadTypes.DRAWING_HISTORY.type, drawingHistory)
                val data = JSONObject(bytes)
                val payloadData: JSONObject = data.get("payload") as JSONObject
                val events = payloadData.get("events") as JSONArray
                for (i in 0 until events.length()) {
                    val event = events[i] as JSONObject
                    when (event.get("tool_type")) {
                        ToolType.PENCIL.value -> drawingHistory.events.add(getApiMapper().readValue(event.toString(), DrawingPosEvent::class.java))
                        ToolType.RECTANGLE.value -> drawingHistory.events.add(getApiMapper().readValue(event.toString(), DrawingRectEvent::class.java))
                        ToolType.ELLIPSE.value -> drawingHistory.events.add(getApiMapper().readValue(event.toString(), DrawingEllipseEvent::class.java))
                    }
                }

                return payload
            }

            private fun parseLayerEvent(bytes: String): SocketPayload? {
                val parser = Gson()
                val drawingEventType = parser.fromJson(bytes, SocketDrawingEventType::class.java)
                var payload: SocketPayload? = null
                when (drawingEventType.payload.event.tool_type) {
                    ToolType.PENCIL.value -> payload = getApiMapper().readValue(bytes, SocketLayerDrawingPosEventPayload::class.java)
                    ToolType.RECTANGLE.value -> payload = getApiMapper().readValue(bytes, SocketLayerDrawingRectEventPayload::class.java)
                    ToolType.ELLIPSE.value -> payload = getApiMapper().readValue(bytes, SocketLayerDrawingEllipseEventPayload::class.java)
                }

                return payload
            }

            override fun onMessage(webSocket: WebSocket, bytes: String) {
                super.onMessage(webSocket, bytes)
                val parser = Gson()
                val data = parser.fromJson(bytes, SocketMessage::class.java)
                val types = PayloadTypes.values().associateBy(PayloadTypes::type)
                val type = types[data.type]
                if (type == null || !onMessageMap.containsKey(type)) {
                    return
                }
                var payload: SocketPayload? = null
                when (data.type) {
                    PayloadTypes.MESSAGE.type -> payload = getApiMapper().readValue(bytes, SocketMessagePayload::class.java)
                    PayloadTypes.DRAWING_EVENT.type -> payload = parseDrawingEvent(bytes)
                    PayloadTypes.DRAWING_PREVIEW_EVENT.type -> payload = parseDrawingEvent(bytes)
                    PayloadTypes.MESSAGE_HISTORY.type -> payload = getApiMapper().readValue(bytes, SocketMessageHistoryPayload::class.java)
                    PayloadTypes.DRAWING_HISTORY.type -> payload = parseDrawingHistory(bytes)
                    PayloadTypes.LAYER_EVENT.type -> payload = parseLayerEvent(bytes)
                    PayloadTypes.LAYER_MOVE_EVENT.type -> payload = getApiMapper().readValue(bytes, SocketLayerMoveEventPayload::class.java)
                    PayloadTypes.LAYER_PREVIEW_EVENT.type -> payload = getApiMapper().readValue(bytes, SocketLayerPreviewEventPayload::class.java)
                    PayloadTypes.LAYER_TEXT_EVENT.type -> payload = getApiMapper().readValue(bytes, SocketLayerTextEventPayload::class.java)
                    PayloadTypes.REFRESH_ALBUM_JOIN_REQUESTS.type -> payload = SocketPayload(PayloadTypes.REFRESH_ALBUM_JOIN_REQUESTS.type, Payload())
                    PayloadTypes.ALBUM_CHANGE.type -> payload = SocketPayload(PayloadTypes.ALBUM_CHANGE.type, Payload())
                    PayloadTypes.DRAWING_CHANGE.type -> payload = SocketPayload(PayloadTypes.DRAWING_CHANGE.type, Payload())
                    PayloadTypes.KICK_EVENT.type -> payload = getApiMapper().readValue(bytes, SocketKickEventPayload::class.java)
                    PayloadTypes.CLIENT_STATE_EVENT.type -> payload = getApiMapper().readValue(bytes, SocketClientStateEventPayload::class.java)
                    PayloadTypes.CLIENT_STATE_SYNC_EVENT.type -> payload = getApiMapper().readValue(bytes, SocketClientStateSyncEventPayload::class.java)
                }
                val handler = onMessageMap[type]
                Handler(Looper.getMainLooper()).post {
                    if (handler != null && payload != null)
                        handler(payload!!.payload)
                }
            }

            override fun onClosed(webSocket: WebSocket, code: Int, reason: String) {
                super.onClosed(webSocket, code, reason)
                if (onClose != null)
                    onClose()
            }

            override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
                super.onFailure(webSocket, t, response)
                if (onClose != null)
                    onClose()
            }
        }
    }

    fun closeConnection() {
        conn.close(1000, "User disconnected")
    }

    fun send(payload: Payload, type: PayloadTypes) {
        val socketPayload = SocketPayload(type.type, payload)
        conn.send(getApiMapper().writeValueAsString(socketPayload))
    }

    fun onMessage(type: PayloadTypes, onmessage: (Payload) -> Unit) {
        onMessageMap[type] = onmessage
    }
}