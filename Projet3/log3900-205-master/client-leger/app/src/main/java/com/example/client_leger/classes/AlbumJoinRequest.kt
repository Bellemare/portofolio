package com.example.client_leger.classes

import com.example.client_leger.getApiMapper
import com.github.kittinunf.fuel.core.ResponseDeserializable

data class AlbumJoinRequest(val id: Int, val user_id: Int, val user: String, val album_id: Int, val album: String) {

    class AlbumJoinRequestListDeserializer : ResponseDeserializable<ArrayList<AlbumJoinRequest>> {
        override fun deserialize(content: String): ArrayList<AlbumJoinRequest> {
            val mapper = getApiMapper()
            return mapper.readValue(content, mapper.typeFactory.constructCollectionType(ArrayList::class.java, AlbumJoinRequest::class.java))
        }
    }
}