package com.example.client_leger.classes.apiEvents

import com.example.client_leger.classes.sockets.ClientStateSyncEventPayload

class ClientStateSyncEvent(val payload: ClientStateSyncEventPayload): AppEvent() {
    override val type = AppEventType.CLIENT_STATE_SYNC_EVENT
}