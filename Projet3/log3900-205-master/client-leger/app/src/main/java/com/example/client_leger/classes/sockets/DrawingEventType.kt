package com.example.client_leger.classes.sockets

data class SocketDrawingEventType(val payload: DrawingEventType) {
    data class DrawingEventType(val event: SocketDrawingEvent) {
        data class SocketDrawingEvent(var tool_type: Int)
    }
}
