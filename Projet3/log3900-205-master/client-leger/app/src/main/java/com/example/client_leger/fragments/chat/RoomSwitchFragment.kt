package com.example.client_leger.fragments.chat

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.add
import androidx.fragment.app.commit
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.client_leger.R
import com.example.client_leger.classes.Authentication
import com.example.client_leger.classes.chat.RoomListSwitchAdapter
import com.example.client_leger.classes.apiEvents.Room
import com.example.client_leger.classes.chat.SeeRoomListener
import com.example.client_leger.services.RoomService
import com.example.client_leger.viewModels.ChatViewModel
import com.shopify.promises.Promise

class RoomSwitchFragment : Fragment(), SeeRoomListener {

    private lateinit var viewModel: ChatViewModel
    private var roomArray = ArrayList<Room>()
    private lateinit var adapter: RoomListSwitchAdapter
    lateinit var listview: RecyclerView
    private lateinit var addRoomBtn : Button
    private lateinit var returnToChatBtn : Button

    private var userId: Int? = Authentication.getUser()?.id

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.room_selection_fragment, container, false)
        listview = view.findViewById(R.id.roomListSwitch)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(requireActivity()).get(ChatViewModel::class.java)
        addRoomBtn = (view as View).findViewById(R.id.addRoomBtn)
        returnToChatBtn = (view as View).findViewById(R.id.returnToChatBtn)

        adapter = RoomListSwitchAdapter(roomArray, userId?: -1, this, { roomId -> leaveRoom(roomId) }, { roomId -> deleteRoom(roomId) })
        listview.adapter = adapter
        listview.layoutManager = LinearLayoutManager(context)


        fetchJoinedRooms()

        addRoomBtn.setOnClickListener{ addRoom() }
        returnToChatBtn.setOnClickListener { returnToChat() }

    }

    private fun deleteRoom(roomId: Int) {
        viewModel.deleteRoom(roomId).whenComplete {
            when(it) {
                is Promise.Result.Success -> {
                    it.value.displayResponse(requireView())
                }
                is Promise.Result.Error -> {
                    println(it.error)
                }
            }
        }
    }

    private fun leaveRoom(roomId: Int) {
       viewModel.leaveRoom(roomId).whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    adapter.roomList.removeIf { room: Room ->
                        room.id == roomId
                    }
                    adapter.notifyDataSetChanged()
                }
                is Promise.Result.Error -> {
                    println(it.error)
                }
            }
        }
    }

    private fun fetchJoinedRooms(){
        viewModel.fetchJoinedRooms().whenComplete {
            when (it) {
                is Promise.Result.Success -> {

                    val rooms: ArrayList<Room> = ArrayList()
                    if (RoomService.getDrawingRoomConnected()){
                        rooms.add(viewModel.DRAWING_ROOM)
                    }

                    rooms.addAll(it.value)

                    adapter.setRooms(rooms)
                }
                is Promise.Result.Error -> {
                    println(it.error)
                }
            }
        }
    }

    override fun onSeeClick(room: Room){
        val roomSwitchFragment = parentFragmentManager.findFragmentByTag("RoomSwitchFragment")
        val chatWindowFragment = roomSwitchFragment?.parentFragment as ChatWindowFragment

        chatWindowFragment.updateChat(room)
        returnToChat()
    }
    override fun onLeaveClick(room: Room){
        val roomSwitchFragment = parentFragmentManager.findFragmentByTag("RoomSwitchFragment")
        val chatWindowFragment = roomSwitchFragment?.parentFragment as ChatWindowFragment

        chatWindowFragment.updateChat(viewModel.GENERAL_ROOM)
    }
    private fun addRoom() {
        val fragment = parentFragmentManager.findFragmentByTag("RoomSwitchFragment")
        parentFragmentManager.commit {
            setReorderingAllowed(true)
            remove(fragment as Fragment)
        }
        parentFragmentManager.commit {
            setReorderingAllowed(true)
            add<RoomAddFragment>(R.id.roomMenuContainerView, "RoomAddFragment")
        }
    }

    private fun returnToChat(){
        val roomSwitchFragment = parentFragmentManager.findFragmentByTag("RoomSwitchFragment")

        parentFragmentManager.commit {
            setReorderingAllowed(true)
            remove(roomSwitchFragment as Fragment)
        }
    }
}