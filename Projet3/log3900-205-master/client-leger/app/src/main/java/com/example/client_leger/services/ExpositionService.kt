package com.example.client_leger.services

import com.example.client_leger.classes.*
import com.example.client_leger.classes.apiEvents.DrawingExpositionAddRequest
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.coroutines.awaitObjectResult
import com.github.kittinunf.result.Result


class ExpositionService: ApiService() {
    val BASE_URL = "/Exposition"

    suspend fun addDrawingToExposition(id: Int, drawingId: Int): Result<RequestResponse, FuelError> {
        val url = "$BASE_URL/$id"
        return handleResponse(post(RequestConfig(url=url), DrawingExpositionAddRequest(drawingId)).awaitObjectResult(RequestResponse.Deserializer()))
    }

    suspend fun removeDrawingFromExposition(id: Int, drawingId: Int): Result<RequestResponse, FuelError> {
        val url = "$BASE_URL/$id/Drawing/$drawingId"
        return handleResponse(delete(RequestConfig(url=url)).awaitObjectResult(RequestResponse.Deserializer()))
    }

    suspend fun getAll(): Result<ArrayList<Album>, FuelError> {
        val url = "$BASE_URL"
        return handleResponse(get(RequestConfig(url=url)).awaitObjectResult(Album.AlbumListDeserializer()))
    }

    suspend fun getExposition(albumId: Int): Result<ArrayList<Drawing>, FuelError> {
        val url = "$BASE_URL/$albumId"
        return handleResponse(get(RequestConfig(url=url)).awaitObjectResult(Drawing.DrawingListDeserializer()))
    }
}