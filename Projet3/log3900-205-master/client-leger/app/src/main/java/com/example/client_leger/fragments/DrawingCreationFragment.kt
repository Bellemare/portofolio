package com.example.client_leger.fragments

import android.content.Context
import android.os.Bundle
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import com.example.client_leger.R
import com.example.client_leger.classes.*
import com.example.client_leger.viewModels.AlbumMenuViewModel
import com.shopify.promises.Promise
import java.nio.charset.Charset

class DrawingCreationFragment : Fragment(), ViewDisabler {

    private lateinit var publicBtn: ToggleButton
    private lateinit var privateBtn: ToggleButton

    private lateinit var createBtn: Button
    private lateinit var editBtn: Button
    private lateinit var cancelBtn: Button

    private lateinit var drawingName: EditText
    private lateinit var albumDropDownContainer: FrameLayout
    private var albumDropdown: AutoCompleteTextView? = null
    private var currentAlbumPosition: Int = -1
    private lateinit var passwordText: EditText

    private lateinit var albums: ArrayList<Album>
    private var isInsideAlbum: Boolean = false
    private val viewModel: AlbumMenuViewModel by activityViewModels()
    private var isPublic: Boolean = true
    private lateinit var mode: DrawingMode
    private lateinit var drawing: DrawingModel

    private var isBlocked: Boolean = false

    enum class DrawingMode {
        CREATE, EDIT
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_drawing_creation, container, false)
        mode = requireArguments().get("mode") as DrawingMode
        albums = requireArguments().get("albums") as ArrayList<Album>

        when(mode) {
            DrawingMode.CREATE -> {
                isInsideAlbum = requireArguments().getBoolean("isInsideAlbum")

                setUpView(view)
                if(isInsideAlbum) {
                    setStaticView()
                }
            }
            DrawingMode.EDIT -> {
                drawing = requireArguments().get("drawing") as DrawingModel
                setUpView(view)
                setUpEditView(view)
            }
        }

        return view
    }

    private fun setUpEditView(view: View) {
        editBtn.visibility = View.VISIBLE
        createBtn.visibility = View.GONE

        drawingName.setText(drawing.name)
        if(drawing.album_id != 0){
            privateBtn.performClick()

            var index: Int = 0
            for(i in albums.indices) {
                if(albums[i].id == drawing.album_id) {
                    index = i
                    break
                }
            }
            albumDropdown?.setText(albumDropdown!!.adapter.getItem(index).toString(), false)
            currentAlbumPosition = index
        }
        else {
            publicBtn.performClick()
        }
    }

    private fun setUpView(view: View) {
        drawingName = view.findViewById(R.id.newDrawingName)
        passwordText = view.findViewById(R.id.passwordText)
        setUpSpinner(view)
        setUpVisibilityButtons(view)
        setUpClosingButtons(view)

        drawingName.setOnKeyListener { view, i, keyEvent ->
            if(keyEvent.action == KeyEvent.ACTION_DOWN) {
                when(i) {
                    KeyEvent.KEYCODE_ENTER ->  (view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(view.windowToken, 0)
                }
            }
            true
        }

    }

    private fun setStaticView() {
        if(albums[0].id == 0) publicBtn.performClick() else privateBtn.performClick()
        privateBtn.isEnabled = false
        publicBtn.isEnabled = false
        enableDisableViewGroup(albumDropDownContainer, false)
    }

    private fun setUpClosingButtons(view: View) {
        createBtn = view.findViewById(R.id.createBtn)
        cancelBtn = view.findViewById(R.id.cancelBtn)
        editBtn = view.findViewById(R.id.editBtn)

        createBtn.setOnClickListener {
            val name: String = drawingName.text.toString()
            var password: String? = passwordText.text.toString()
            var albumId: Int = 0
            if(!isPublic) {
                if(currentAlbumPosition != -1){
                    albumId = (albumDropdown?.adapter?.getItem(currentAlbumPosition) as Album).id
                }
                else {
                    albumId = -1
                }
                password = null
            }

            if (!isBlocked) {
                isBlocked = true
                viewModel.createDrawing(DrawingModel(name, albumId, null, password)).whenComplete { result ->
                    when(result) {
                        is Promise.Result.Success -> {
                            val bundle = bundleOf(Pair("drawingId", result.value.id), Pair("drawingName", name))
                            Navigation.findNavController(requireView()).navigate(R.id.action_albumMenuFragment_to_drawingRoomFragment, args = bundle)
                        }
                        is Promise.Result.Error -> {
                            //handle error
                            val error = RequestFieldErrorResponse.Deserializer().deserialize(result.error.errorData.toString(Charset.defaultCharset()))
                            error.displayError(view)

                            for(field in error.fields){
                                when(field) {
                                    "name" -> DrawableCompat.setTint(drawingName.background, ContextCompat.getColor(view.context, R.color.errorColor))
                                }
                            }
                            (view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(view.windowToken, 0)
                            isBlocked = false
                        }
                    }
                }
            }
        }
        cancelBtn.setOnClickListener {
            removeFragment()
            (view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(view.windowToken, 0)
        }

        editBtn.setOnClickListener {
            var password: String? = if(isPublic) passwordText.text.toString() else null
            val editedDrawing: DrawingModel = DrawingModel(drawingName.text.toString(),   if (isPublic) 0 else (albumDropdown?.adapter?.getItem(currentAlbumPosition) as Album).id, drawing.id, password)
            viewModel.updateDrawing(editedDrawing).whenComplete { result ->
                when (result) {
                    is Promise.Result.Success -> {
                        val requestResponse: RequestResponse = result.value
                        requestResponse.displayResponse(view)

                        (view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(view.windowToken, 0)
                        removeFragment()
                    }
                    is Promise.Result.Error -> {
                        //handle error
                        val error = RequestFieldErrorResponse.Deserializer()
                            .deserialize(result.error.errorData.toString(Charset.defaultCharset()))
                        error.displayError(view)

                        for (field in error.fields) {
                            when (field) {
                                "name" -> DrawableCompat.setTint(
                                    drawingName.background,
                                    ContextCompat.getColor(view.context, R.color.errorColor)
                                )
                            }
                        }
                        (view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(view.windowToken, 0)
                    }
                }
            }
        }
    }

    private fun removeFragment() {
        when(parentFragment){
            is AlbumMenuFragment -> (parentFragment as AlbumMenuFragment).removeAlbumActionFragment()
            is AlbumInfoFragment -> (parentFragment as AlbumInfoFragment).removeActionFragment()
            is AlbumSearchDrawingFragment -> (parentFragment as AlbumSearchDrawingFragment).removeActionFragment()
        }
    }

    private fun setUpSpinner(view: View) {
        albumDropDownContainer = view.findViewById(R.id.albumDropdownContainer)
        albumDropdown = view.findViewById<com.google.android.material.textfield.TextInputLayout>(R.id.albumDropdown).editText as? AutoCompleteTextView

        val adapter = ArrayAdapter(view.context, R.layout.tool_spinner_item, albums)
        albumDropdown?.setAdapter(adapter)
        albumDropdown?.setDropDownBackgroundResource(R.color.backgroundColor)

        if(albums.size != 0) {
            albumDropdown?.setText(adapter.getItem(0).toString(), false)
            currentAlbumPosition = 0
        } else {
            currentAlbumPosition = -1
        }


        albumDropdown?.setOnItemClickListener { _, _, position, _ ->
            currentAlbumPosition = position
        }
    }

    private fun setUpVisibilityButtons(view: View) {
        publicBtn = view.findViewById(R.id.publicToggle)
        privateBtn = view.findViewById(R.id.privateToggle)

        //default state
        publicBtn.isEnabled = false
        privateBtn.isEnabled = true
        isPublic = true
        albumDropDownContainer.visibility = View.INVISIBLE
        passwordText.visibility = View.VISIBLE

        //stateChange clickListeners
        publicBtn.setOnClickListener {
            publicBtn.setBackgroundColor(ContextCompat.getColor(view.context, R.color.primaryColor))
            privateBtn.setBackgroundColor(ContextCompat.getColor(view.context, R.color.backgroundDarkColor))

            publicBtn.isEnabled = false
            privateBtn.isEnabled = true
            isPublic = true

            albumDropDownContainer.visibility = View.INVISIBLE
            passwordText.visibility = View.VISIBLE
        }
        privateBtn.setOnClickListener {
            publicBtn.setBackgroundColor(ContextCompat.getColor(view.context, R.color.backgroundDarkColor))
            privateBtn.setBackgroundColor(ContextCompat.getColor(view.context, R.color.primaryColor))

            publicBtn.isEnabled = true
            privateBtn.isEnabled = false
            isPublic = false

            albumDropDownContainer.visibility = View.VISIBLE
            passwordText.visibility = View.INVISIBLE
        }
    }

}