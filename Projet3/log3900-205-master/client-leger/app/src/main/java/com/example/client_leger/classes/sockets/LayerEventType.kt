package com.example.client_leger.classes.sockets

enum class LayerEventType(val value: Int) {
    CHANGE(1),
    DELETE(2),
    ADD(3),
    SELECT(4),
}