package com.example.client_leger.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.example.client_leger.R

class ContactDeleteFragment : Fragment() {

    private lateinit var confirmMsg: TextView
    private lateinit var cancelBtn: Button
    private lateinit var deleteBtn: Button

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_contact_delete, container, false)

        confirmMsg = view.findViewById(R.id.confirmMsg)
        confirmMsg.text = String.format("Voulez-vous vraiment supprimer %s de vos contacts?", requireArguments().getString("confirmMessage"))

        cancelBtn = view.findViewById(R.id.cancelBtn)
        deleteBtn = view.findViewById(R.id.deleteBtn)

        when (parentFragment) {
            is SideBarFragment -> {
                val sideBarFragment: SideBarFragment = parentFragment as SideBarFragment
                cancelBtn.setOnClickListener { sideBarFragment.removeConfirmPromptFragment() }
                deleteBtn.setOnClickListener { sideBarFragment.deleteContact() }
            }

            is ContactFragment -> {
                val contactFragment: ContactFragment = parentFragment as ContactFragment
                cancelBtn.setOnClickListener { contactFragment.removeConfirmPromptFragment() }
                deleteBtn.setOnClickListener { contactFragment.deleteContact() }
            }
        }

        return view
    }
}