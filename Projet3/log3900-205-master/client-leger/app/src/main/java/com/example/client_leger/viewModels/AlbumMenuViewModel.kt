package com.example.client_leger.viewModels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.client_leger.classes.*
import com.example.client_leger.services.*
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.result.Result
import com.shopify.promises.Promise
import kotlinx.coroutines.launch

class AlbumMenuViewModel: ViewModel() {
    private val albumService: AlbumService = AlbumService()
    private val expositionService: ExpositionService = ExpositionService()

    fun fetchUserAlbums(): Promise<ArrayList<Album>, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = albumService.getUserAlbums()
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun fetchAlbumList(): Promise<ArrayList<Album>, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = albumService.getAlbumList()
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun createAlbum(album: AlbumModel): Promise<Album, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = albumService.create(album)
                when (res) {
                    is Result.Success ->  {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun createDrawing(drawing: DrawingModel): Promise<Drawing, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = DrawingApiService.create(drawing)
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun requestToJoinAlbum(albumId: Int): Promise<RequestResponse, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = albumService.requestToJoinAlbum(albumId)
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun getAlbumDrawings(albumId: Int): Promise<ArrayList<Drawing>, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = albumService.getDrawings(albumId)
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun updateAlbum(album: Album): Promise<RequestResponse, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = albumService.update(album)
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun deleteAlbum(albumId: Int): Promise<RequestResponse, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = albumService.delete(albumId)
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun quitAlbum(albumId: Int): Promise<RequestResponse, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = albumService.quitAlbum(albumId)
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun updateDrawing(drawing: DrawingModel): Promise<RequestResponse, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = DrawingApiService.updateDrawing(drawing)
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun deleteDrawing(drawingId: Int): Promise<RequestResponse, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = DrawingApiService.deleteDrawing(drawingId)
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun logout(): Promise<RequestResponse, FuelError> {
        return Promise {
            viewModelScope.launch {
                val service = AuthService()
                val res = service.logout()
                when (res) {
                    is Result.Success -> {
                        Authentication.unloadAuthToken()
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun getCurrentUser(): Promise<User, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = UserService.getCurrentUser()
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun drawingSmartFilter(searchTerm: String, albumId: Int): Promise<ArrayList<Drawing>, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = DrawingApiService.smartFilter(searchTerm, albumId)
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun getAlbumJoinRequests(): Promise<ArrayList<AlbumJoinRequest>, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = albumService.getAllJoinRequests()
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun acceptJoinRequest(albumId: Int, requestId: Int): Promise<RequestResponse, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = albumService.acceptJoinRequest(albumId, requestId)
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun declineJoinRequest(albumId: Int, requestId: Int): Promise<RequestResponse, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = albumService.declineJoinRequest(albumId, requestId)
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun getExpositionAlbums(): Promise<ArrayList<Album>, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = expositionService.getAll()
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun getAlbumExposition(albumId: Int): Promise<ArrayList<Drawing>, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = expositionService.getExposition(albumId)
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun addDrawingToExposition(albumId: Int, drawingId: Int): Promise<RequestResponse, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = expositionService.addDrawingToExposition(albumId, drawingId)
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun removeDrawingFromExposition(albumId: Int, drawingId: Int): Promise<RequestResponse, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = expositionService.removeDrawingFromExposition(albumId, drawingId)
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun verifyPassword(request: PasswordVerifyRequest): Promise<RequestResponse, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = DrawingApiService.verifyPassword(request)
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                 }
            }
        }
    }

}