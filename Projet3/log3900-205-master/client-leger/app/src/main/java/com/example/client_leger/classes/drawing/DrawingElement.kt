package com.example.client_leger.classes.drawing

import com.example.client_leger.classes.Vec2
import com.example.client_leger.classes.sockets.DrawingEventPayload
import com.example.client_leger.classes.toolProperties.ToolProperties
import com.google.gson.Gson
import kotlin.math.abs
import kotlin.math.sign

open class DrawingElement(
    var boundingBox: BoundingBox? = null,
    var baseBoundingBox: BoundingBox? = null,
    var transformation: Transformation? = null
) {

    protected fun copyBoundingBox(copy: DrawingElement) {
        val baseBoundingBox = baseBoundingBox
        if (baseBoundingBox != null) {
            copy.baseBoundingBox = BoundingBox(baseBoundingBox.x, baseBoundingBox.y, baseBoundingBox.w, baseBoundingBox.h)
        }
        val boundingBox = boundingBox
        if (boundingBox != null) {
            copy.boundingBox = BoundingBox(boundingBox.x, boundingBox.y, boundingBox.w, boundingBox.h)
        }
    }

    open fun editBoundingBox(): BoundingBox? {
        return boundingBox
    }

    open fun handleNewEvent(evt: DrawingEventPayload): Boolean {
        return false
    }

    open fun createNewEvent(previous: DrawingElement?) {
        return
    }

    open fun getTopLeftPosition(attributes: ToolProperties): Vec2 {
        return Vec2(0.0f, 0.0f)
    }

    open fun applyScaling(dw: Float, dh: Float, flipX: Boolean, flipY: Boolean, attributes: ToolProperties): Boolean {
        if (transformation == null) {
            transformation = Transformation(0.0f,0.0f,1.0f,1.0f)
        }

        val transformation = this.transformation as Transformation
        val boundingBox = this.boundingBox
        val baseBoundingBox = this.baseBoundingBox
        if (baseBoundingBox != null && boundingBox != null) {
            transformation.dw = (boundingBox.w * abs(dw) / baseBoundingBox.w) * sign(dw)
            transformation.dh = (boundingBox.h * abs(dh) / baseBoundingBox.h) * sign(dh)
        }
        return true
    }

    open fun applyTranslation(dx: Float, dy: Float) {
        if (transformation == null) {
            transformation = Transformation(0.0f,0.0f,1.0f,1.0f)
        }

        val transformation = this.transformation as Transformation
        val boundingBox = this.boundingBox
        val baseBoundingBox = this.baseBoundingBox
        if (baseBoundingBox != null && boundingBox != null) {
            transformation.dx = (boundingBox.x + dx) - baseBoundingBox.x
            transformation.dy = (boundingBox.y + dy) - baseBoundingBox.y
        }
    }

    open fun deepCopy(): DrawingElement {
        val parser = Gson()
        return parser.fromJson(parser.toJson(this), DrawingElement::class.java)
    }
}
