package com.example.client_leger.classes

import com.example.client_leger.getApiMapper
import com.github.kittinunf.fuel.core.ResponseDeserializable

data class AddContactRequest(val contactId: Int) {
    class Deserializer : ResponseDeserializable<AddContactRequest> {
        override fun deserialize(content: String) =
            getApiMapper().readValue(content, AddContactRequest::class.java)
    }
}
