package com.example.client_leger.services.tools

import android.graphics.Paint
import android.view.MotionEvent
import com.example.client_leger.classes.Color
import com.example.client_leger.classes.Vec2
import com.example.client_leger.classes.apiEvents.*
import com.example.client_leger.classes.drawing.*
import com.example.client_leger.classes.sockets.LayerEventType
import com.example.client_leger.classes.sockets.LayerPreviewEventPayload
import com.example.client_leger.classes.sockets.LayerTextEventPayload
import com.example.client_leger.classes.toolProperties.ToolProperties
import com.example.client_leger.services.DrawingService
import com.example.client_leger.services.SynchronizerService
import com.example.client_leger.services.couches.CouchesService
import kotlinx.coroutines.runBlocking

class SelectionService: ToolService() {
    override val toolType: ToolType
        get() = ToolType.SELECTION

    private var isQuickSelect: Boolean = false;
    var endSelectionEvent: ((changeTool: Boolean) -> Unit)? = null
    var onRedrawSelectionEvent: (() -> Unit)? = null
    var onPreviewSelectionEvent: ((selection: DrawingEventWrapper?) -> Unit)? = null

    var selectedDrawingEvent: DrawingEventWrapper? = null

    private var size: Vec2? = null
    private var position: Vec2? = null

    override var currentAttributes: ToolProperties = ToolProperties()

    private var selectionChangeListener: AppEvents.IntWrapper = AppEvents.IntWrapper(0)
    private var selectionResetListener: AppEvents.IntWrapper = AppEvents.IntWrapper(0)
    private var colorChangeListener: AppEvents.IntWrapper = AppEvents.IntWrapper(0)
    private var deleteListener: AppEvents.IntWrapper = AppEvents.IntWrapper(0)
    private var textListener: AppEvents.IntWrapper = AppEvents.IntWrapper(0)

    init {
        SynchronizerService.layerSelectEvent = { selectDrawingEvent(it) }
        SynchronizerService.layerDeleteEvent = {
            selectedDrawingEvent = null
            finishSelection(true)
        }
    }

    fun onInitQuickSelect() {
        isQuickSelect = true;
        onInit()
    }

    override fun onInit() {
        super.onInit()
        runBlocking {
            AppEvents.listenTo(AppEventType.SELECTION_CHANGE_EVENT, selectionChangeListener) {
                selectionChange(it as SelectionChangeEvent)
            }
            AppEvents.listenTo(AppEventType.SELECTION_DELETE_EVENT, deleteListener) {
                deleteSelection()
            }
            AppEvents.listenTo(AppEventType.SELECTION_RESET_EVENT, selectionResetListener) {
                it as SelectionResetEvent
                position = it.position
                size = it.size
            }
            AppEvents.listenTo(AppEventType.COLOR_CHANGE_EVENT, colorChangeListener) {
                colorChange()
            }
            AppEvents.listenTo(AppEventType.SELECTION_TEXT_CHANGE_EVENT, textListener) {
                setText(it as SelectionTextChangeEvent)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        runBlocking {
            AppEvents.unregister(selectionChangeListener.value, AppEventType.SELECTION_CHANGE_EVENT)
            AppEvents.unregister(selectionResetListener.value, AppEventType.SELECTION_RESET_EVENT)
            AppEvents.unregister(colorChangeListener.value, AppEventType.COLOR_CHANGE_EVENT)
            AppEvents.unregister(deleteListener.value, AppEventType.SELECTION_DELETE_EVENT)
        }
        if (selectedDrawingEvent != null) {
            finishSelection()
        }
    }

    private fun selectedLayer(): Int {
        val selection = selectedDrawingEvent
        if (selection == null)
            return -1

        return CouchesService.getDrawingEventIndex(selection)
    }

    override fun setProperty(key: String, value: Any) {
        super.setProperty(key, value)
        if (selectedDrawingEvent != null) {
            var boundingBox = selectedDrawingEvent!!.drawingEvent!!.data.editBoundingBox()
            resize(Vec2(boundingBox!!.w, boundingBox.h))
            selectedDrawingEvent?.drawingEvent?.data?.boundingBox = null
            renderTransformation()
            boundingBox = selectedDrawingEvent!!.drawingEvent!!.data.editBoundingBox()
            AppEvents.emitEvent(SelectionStartEvent(Vec2(boundingBox!!.x, boundingBox.y), Vec2(boundingBox.w, boundingBox.h), false, null))
            syncLayerPreview(false, false)
        }
    }

    private fun colorChange() {
        if (selectedDrawingEvent != null) {
            selectedDrawingEvent!!.drawingEvent!!.fillColor = DrawingService.secondaryColor
            selectedDrawingEvent!!.drawingEvent!!.textColor = DrawingService.textColor
            selectedDrawingEvent!!.drawingEvent!!.strokeColor = DrawingService.primaryColor
            renderTransformation()
            syncLayerPreview(false, false)
        }
    }

    private fun selectionHasText(): Boolean {
        val selection = selectedDrawingEvent
        return selection != null && (selection.drawingEvent?.toolType == ToolType.ELLIPSE.value || selection.drawingEvent?.toolType == ToolType.RECTANGLE.value)
    }

    private fun getPreviewTransform(flipX: Boolean, flipY: Boolean): Transformation {
        val boundingBox = selectedDrawingEvent?.drawingEvent?.data?.editBoundingBox() as BoundingBox
        return Transformation(
            boundingBox.x,
            boundingBox.y,
            boundingBox.w,
            boundingBox.h,
            flipX,
            flipY
        )
    }

    private fun syncLayerPreview(flipX: Boolean, flipY: Boolean) {
        if (selectedDrawingEvent?.drawingEvent?.id == null) {
            return
        }

        val payload = LayerPreviewEventPayload(
            selectedDrawingEvent?.drawingEvent?.id as Int,
            selectedLayer(),
            getPreviewTransform(flipX, flipY),
            selectedDrawingEvent?.drawingEvent?.attributes as ToolProperties,
            selectedDrawingEvent?.drawingEvent?.strokeColor as Color,
            selectedDrawingEvent?.drawingEvent?.fillColor as Color,
            selectedDrawingEvent?.drawingEvent?.textColor as Color,
        )
        SynchronizerService.layerPreview(payload)
    }

    private fun syncLayerText() {
        if (selectedDrawingEvent == null)
            return

        val text = (selectedDrawingEvent!!.drawingEvent!!.data as DrawingShape).text as DrawingText;

        val payload = LayerTextEventPayload(
            selectedDrawingEvent!!.drawingEvent!!.id as Int,
            selectedLayer(),
            text,
        )
        SynchronizerService.layerTextEvent(payload);
    }

    private fun setText(event: SelectionTextChangeEvent) {
        if (selectionHasText()) {
            val data = selectedDrawingEvent!!.drawingEvent!!.data as DrawingShape
            if (data.text == null) {
                data.text = DrawingText(event.text)
            } else {
                data.text!!.text = event.text
            }
            data.boundingBox = null
            renderTransformation()
            syncLayerText()
        }
    }

    private fun deleteSelection() {
        if (selectedDrawingEvent == null)
            return;

        val selectedLayer = selectedLayer()
        SynchronizerService.layerEvent(LayerEventType.DELETE, selectedDrawingEvent!!, selectedLayer);
    }

    private fun selectionChange(event: SelectionChangeEvent) {
        position = event.position
        val (flipX, flipY) = resize(event.size)
        size = Vec2(event.size.x, event.size.y)

        translate()
        if (selectedDrawingEvent?.drawingEvent?.data != null) {
            val data = selectedDrawingEvent?.drawingEvent?.data as DrawingElement
            data.boundingBox = null
        }
        renderTransformation()
        syncLayerPreview(flipX, flipY)
    }

    private fun resize(newSize: Vec2): Pair<Boolean, Boolean> {
        val boundingBox = selectedDrawingEvent?.drawingEvent?.data?.editBoundingBox()
        if (boundingBox != null) {
            val last = size
            val flipX = last != null && ((newSize.x <= 0 && last.x > 0) || (newSize.x > 0 && last.x <= 0))
            val flipY = last != null && ((newSize.y <= 0 && last.y > 0) || (newSize.y > 0 && last.y <= 0))
            val dw = newSize.x / boundingBox.w
            val dh = newSize.y / boundingBox.h
            val attributes = selectedDrawingEvent?.drawingEvent?.attributes as ToolProperties
            selectedDrawingEvent?.drawingEvent?.data?.applyScaling(dw, dh, flipX, flipY, attributes) as Boolean
            return Pair(flipX, flipY)
        }
        return Pair(false, false)
    }

    private fun translate() {
        val pos = position
        val selection = selectedDrawingEvent
        if (pos != null && selection != null) {
            val attributes = selection.drawingEvent?.attributes as ToolProperties
            val dataTopLeft = selection.drawingEvent?.data?.getTopLeftPosition(attributes) as Vec2
            val dx = pos.x - dataTopLeft.x
            val dy = pos.y - dataTopLeft.y
            selection.drawingEvent?.data?.applyTranslation(dx, dy)
        }
    }

    private fun renderTransformation() {
        selectedDrawingEvent?.redraw()
        val previewHandler = onPreviewSelectionEvent
        if (previewHandler != null) {
            previewHandler(selectedDrawingEvent)
        }
    }

    fun selectDrawingEvent(evt: DrawingEventWrapper) {
        if (selectedDrawingEvent != null) {
            finishSelection()
        }
        selectedDrawingEvent = evt
        selectedDrawingEvent!!.isSelected = true
        val event = selectedDrawingEvent?.drawingEvent as DrawingEvent
        currentAttributes = event.attributes
        AppEvents.emitEvent(ToolPropertiesChangeEvent(currentAttributes))
        AppEvents.emitEvent(ToolColorsChangeEvent(event.strokeColor, event.fillColor, event.textColor))
        if (!isQuickSelect) {
            selectedDrawingEvent!!.event.callback()
            val onRedrawHandler = onRedrawSelectionEvent
            if (onRedrawHandler != null) {
                onRedrawHandler()
            }
        }
        renderTransformation()
        initPreview()
    }

    private fun initPreview() {
        val boundingBox = selectedDrawingEvent?.drawingEvent?.data?.editBoundingBox()
        if (boundingBox == null) {
            return
        }

        position = Vec2(boundingBox.x, boundingBox.y)
        size = Vec2(boundingBox.w, boundingBox.h)
        var text: String? = null
        if (selectionHasText()) {
            text = (selectedDrawingEvent!!.drawingEvent!!.data as DrawingShape).text?.text
        }

        AppEvents.emitEvent(SelectionStartEvent(position as Vec2, size as Vec2, selectionHasText(), text))
        AppEvents.emitEvent(OnSelectEvent(selectedDrawingEvent, selectedLayer()))
    }

    private fun finishSelection(changeTool: Boolean = false) {
        val selection = selectedDrawingEvent
        if (selection != null) {
            SynchronizerService.layerEvent(LayerEventType.CHANGE, selection, selectedLayer())
        }
        selectedDrawingEvent = null

        AppEvents.emitEvent(OnSelectEvent(null, -1))

        AppEvents.emitEvent(SelectionStopEvent())
        val endSelectionHandler = endSelectionEvent
        if (endSelectionHandler != null) {
            endSelectionHandler(changeTool && isQuickSelect)
            isQuickSelect = false
        }
    }

    override fun onActionDown(event: MotionEvent) {
        if (selectedDrawingEvent != null) {
            finishSelection(true)
        } else {
            if (isQuickSelect)
                return

            runBlocking {
                val evt = CouchesService.findDrawingEvent(Vec2(event.x, event.y))
                if (evt != null) {
                    SynchronizerService.layerEvent(LayerEventType.SELECT, evt, CouchesService.getDrawingEventIndex(evt))
                }
            }
        }
    }

    override fun onActionMove(event: MotionEvent) {}
    override fun onActionUp(event: MotionEvent) {}
    override fun setPaintProperties(paint: Paint, properties: ToolProperties?) {}
    override fun clearData() {}
}