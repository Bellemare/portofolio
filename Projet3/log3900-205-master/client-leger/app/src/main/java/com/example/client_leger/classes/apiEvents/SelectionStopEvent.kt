package com.example.client_leger.classes.apiEvents

class SelectionStopEvent(): AppEvent() {
    override val type = AppEventType.SELECTION_STOP_EVENT
}