package com.example.client_leger.services.drawingServices

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Typeface
import android.text.TextPaint
import com.example.client_leger.classes.Vec2
import com.example.client_leger.classes.drawing.DrawingEvent
import com.example.client_leger.classes.drawing.DrawingShape


object TextDrawingService: ToolDrawingService() {
    const val LINE_HEIGHT = 18f
    val PADDING = Vec2(10f, 10f)

    fun getPaint(): TextPaint {
        val textPaint = TextPaint()
        textPaint.textAlign = Paint.Align.CENTER
        textPaint.textSize = 15f
        textPaint.isAntiAlias = true
        textPaint.typeface = Typeface.create("Arial", Typeface.NORMAL)

        return textPaint
    }

    fun prepareText(paint: TextPaint, event: DrawingEvent) {
        paint.color = Color.argb(event.textColor.a * 255, event.textColor.r, event.textColor.g, event.textColor.b)
    }

    fun drawText(canvas: Canvas, pos: Vec2, event: DrawingEvent): Float {
        if (event.data !is DrawingShape || (event.data as DrawingShape).text == null) {
            return 0.0f
        }

        val textPaint = getPaint()
        prepareText(textPaint, event)
        val shape = event.data as DrawingShape
        val lines = shape.generateTextLines(event.attributes)
        var totalHeight = 0.0f
        var currentIndex = 0
        for (i in lines.indices) {
            val newLines = lines[i].split("\n")
            for (j in newLines.indices) {
                val yOffset = currentIndex++ * LINE_HEIGHT
                canvas.drawText(newLines[j], pos.x, pos.y + yOffset, textPaint)
            }
            totalHeight += LINE_HEIGHT * newLines.size
        }

        return totalHeight
    }
}