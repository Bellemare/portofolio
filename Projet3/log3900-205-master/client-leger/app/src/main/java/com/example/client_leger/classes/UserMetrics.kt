package com.example.client_leger.classes

import com.example.client_leger.getApiMapper
import com.github.kittinunf.fuel.core.ResponseDeserializable

data class UserMetrics(
    val userId: Int,
    val ownedDrawings: Int,
    val collaboratedDrawings: Int,
    val memberAlbums: Int,
    val averageCollaborationTime: Int,
    val totalCollaborationTime: Int
) {
    class Deserializer : ResponseDeserializable<UserMetrics> {
        override fun deserialize(content: String) =
            getApiMapper().readValue(content, UserMetrics::class.java)
    }
}
