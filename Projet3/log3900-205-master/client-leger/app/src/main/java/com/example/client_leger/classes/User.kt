package com.example.client_leger.classes

import com.example.client_leger.getApiMapper
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class User(
    val id: Int,
    val email: String,
    var pseudonym: String,
    var isContact: Boolean,
    var avatar: String,
    var isPublicEmail: Boolean
) {
    fun deepCopy(): User {
        val parser = Gson()
        return parser.fromJson(parser.toJson(this), User::class.java)
    }

    class UserListDeserializer: ResponseDeserializable<ArrayList<User>> {
        override fun deserialize(content: String): ArrayList<User> {
            val mapper = getApiMapper()
            return mapper.readValue(content, mapper.typeFactory.constructCollectionType(ArrayList::class.java, User::class.java))
        }
    }

    class Deserializer : ResponseDeserializable<User> {
        override fun deserialize(content: String) =
            getApiMapper().readValue(content, User::class.java)
    }
}