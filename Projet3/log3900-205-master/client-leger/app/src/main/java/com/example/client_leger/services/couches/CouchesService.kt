package com.example.client_leger.services.couches

import com.example.client_leger.classes.Vec2
import com.example.client_leger.classes.couches.CoucheBranche
import com.example.client_leger.classes.drawing.DrawingEventWrapper
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import java.util.*
import kotlin.collections.ArrayList

object CouchesService {
    var topLayer: CoucheBranche = CoucheBranche(null)
    var layers: ArrayList<DrawingEventWrapper> = ArrayList()
    var height = 1
    var nElement = 0
    val IDEAL_CHILD_PER_LAYER = 4

    val lock: Mutex = Mutex()

    fun reset() {
        this.layers = ArrayList()
        this.topLayer = CoucheBranche(null)
        this.height = 1
        this.nElement = 0
    }

    fun findDrawingEvent(position: Vec2): DrawingEventWrapper? {
        return this.topLayer.selectChild(position)
    }

    fun getDrawingEventIndex(drawingEvent: DrawingEventWrapper): Int {
        return this.layers.indexOf(drawingEvent)
    }

    fun getElement(index: Int): DrawingEventWrapper {
        return seekElement(index).getDrawingEvent()!!
    }

    fun getElementFromId(id: Int): DrawingEventWrapper {
        layers.forEach {
            if (it.drawingEvent?.id == id) {
                return it
            }
        }

        return topLayer.getDrawingEvent() as DrawingEventWrapper
    }

    fun moveLayer(layer: Int, newLayer: Int) {
        val evt = this.removeLayer(layer)
        if (evt != null)
            this.addLayer(evt, true, newLayer)
    }

    fun removeLayer(pos: Int): DrawingEventWrapper? {
        var activeLayer = this.topLayer
        var total = 0

        for (layer in 0 until this.height) {
            for (i in 0 until activeLayer.childCount()) {
                if (total + activeLayer.getChild(i).drawingEventCount() > pos) {
                    activeLayer = activeLayer.getChild(i)
                    break
                } else {
                    total += (activeLayer.getChild(i).drawingEventCount())
                }
            }
        }

        activeLayer.removeChild(pos - total)
        this.nElement--
        if (layers.size <= pos) {
            return null
        }
        return this.layers.removeAt(pos)
    }

    fun renderAll() {
        val couches = Stack<CoucheBranche>();
        couches.push(topLayer)
        this.addChildrenToStack(this.topLayer, couches)

        while (couches.size > 0)
            couches.pop().makeRender(false)
    }

    private fun addChildrenToStack(couche: CoucheBranche, couches: Stack<CoucheBranche>) {
        for (i in 0 until couche.childCount()) {
            val child = couche.getChild(i)
            if (!child.isLeaf)
                couches.push(child)
        }

        for (i in 0 until couche.childCount()) {
            val child = couche.getChild(i)
            if (!child.isLeaf)
                addChildrenToStack(child, couches)
        }
    }

    fun addLayer(drawingEvent: DrawingEventWrapper, render: Boolean = true, position: Int = -1) {
        nElement++
        if (position != -1 && position < this.nElement) {
            if (position <= 0) {
                this.insertLeft(drawingEvent, render)
                this.layers.add(0, drawingEvent)
            } else {
                this.insert(drawingEvent, render, position)
                this.layers.add(position, drawingEvent)
            }
        } else {
            this.insertRight(drawingEvent, render)
            this.layers.add(drawingEvent)
        }
    }

    private fun scaleUpTree() {
        val newTopLayer = CoucheBranche(null)
        newTopLayer.addChild(this.topLayer, false)
        this.topLayer = newTopLayer
        height++
    }

    private fun seekElement(pos: Int): CoucheBranche {
        var activeLayer = this.topLayer
        var total = 0

        for (layer in 0 until this.height) {
            for (i in 0 until activeLayer.childCount()) {
                if (total + activeLayer.getChild(i).drawingEventCount() > pos) {
                    activeLayer = activeLayer.getChild(i)
                    break
                } else {
                    total += (activeLayer.getChild(i).drawingEventCount())
                }
            }
        }

        return activeLayer.getChild(pos - total)
    }

    private fun isFilledBranch(c: CoucheBranche): Boolean {
        return (c.childCount() >= IDEAL_CHILD_PER_LAYER)
    }

    private fun findBranchingPoint(stack: Stack<CoucheBranche>) {
        for (i in stack.size - 1 downTo 0) {
            if (isFilledBranch(stack[i]))
                stack.pop()
            else
                break
        }

        if (stack.size == 0) {
            this.scaleUpTree()
            stack.push(this.topLayer)
        }
    }

    private fun insert(drawingEvent: DrawingEventWrapper, render: Boolean, pos: Int) {
        var activeLayer = this.topLayer
        var total = 0

        for (layer in 0 until this.height) {
            for (i in 0 until activeLayer.childCount()) {
                if (total + activeLayer.getChild(i).drawingEventCount() >= pos) {
                    activeLayer = activeLayer.getChild(i)
                    break
                } else {
                    total += (activeLayer.getChild(i).drawingEventCount())
                }
            }
        }

        activeLayer.addChild(CoucheBranche(drawingEvent), render, pos - total)
    }

    private fun insertLeft(drawingEvent: DrawingEventWrapper, render: Boolean) {
        val couches = Stack<CoucheBranche>()
        var activeLayer = this.topLayer

        while (couches.size < height) {
            couches.push(activeLayer)
            if (activeLayer.childCount() > 0) {
                activeLayer = activeLayer.getChild(0)
            }
        }

        this.findBranchingPoint(couches)

        while (couches.size < height) {
            couches.push (CoucheBranche(null))
            couches[couches.size - 2].addChild(couches[couches.size - 1], false, 0)
        }

        couches[couches.size - 1].addChild(CoucheBranche(drawingEvent), render, 0)
    }

    private fun insertRight(drawingEvent: DrawingEventWrapper, render: Boolean) {
        val couches = Stack<CoucheBranche>()
        var activeLayer = this.topLayer

        while (couches.size < height) {
            couches.push(activeLayer)
            if (activeLayer.childCount() - 1 >= 0) {
                activeLayer = activeLayer.getChild(activeLayer.childCount() - 1)
            }
        }

        this.findBranchingPoint(couches)

        while (couches.size < height) {
            couches.push(CoucheBranche(null))
            couches[couches.size - 2].addChild(couches[couches.size - 1], false)
        }

        couches[couches.size - 1].addChild(CoucheBranche(drawingEvent), render)
    }

}
