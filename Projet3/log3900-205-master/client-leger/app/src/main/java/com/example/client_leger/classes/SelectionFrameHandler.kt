package com.example.client_leger.classes

import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View

class SelectionFrameHandler(val gestureDetector: GestureDetector, val onUpdate: (translation: Vec2) -> Unit, val onChange: (() -> Unit)? = null) : View.OnTouchListener {
    private var startPos: Vec2 = Vec2(0.0f,0.0f)
    private var previousPos: Vec2 = Vec2(0.0f, 0.0f)

    override fun onTouch(view: View, event: MotionEvent): Boolean {
        gestureDetector.onTouchEvent(event)
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                startPos = Vec2(event.rawX, event.rawY)
                previousPos = startPos
            }
            MotionEvent.ACTION_MOVE -> {
                val translation = Vec2(event.rawX - previousPos.x, event.rawY - previousPos.y)
                onUpdate(translation)
                previousPos = Vec2(event.rawX, event.rawY)
            }
            MotionEvent.ACTION_UP -> {
                val changeHandler = onChange
                if (changeHandler != null) {
                    changeHandler()
                }
            }
        }

        return true
    }
}
