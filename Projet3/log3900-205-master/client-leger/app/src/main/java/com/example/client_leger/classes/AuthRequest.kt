package com.example.client_leger.classes

data class AuthRequest(val email: String, val password: String) {}