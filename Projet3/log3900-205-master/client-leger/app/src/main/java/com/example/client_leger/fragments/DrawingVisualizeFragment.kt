package com.example.client_leger.fragments

import android.graphics.*
import android.os.Bundle
import android.util.Base64
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import com.example.client_leger.R
import com.example.client_leger.classes.Drawing

class DrawingVisualizeFragment : Fragment() {

    lateinit var imgBitmap: Bitmap
    var canvas: Canvas = Canvas()
    private lateinit var imageView: ImageView

    private lateinit var closeBtn: Button
    private lateinit var imageString: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_drawing_visualize, container, false)
        imageString = requireArguments().getString("drawing", "")

        setUpImageView(view)

        closeBtn = view.findViewById(R.id.closeBtn)
        closeBtn.setOnClickListener {
            when(parentFragment){
                is AlbumMenuFragment -> (parentFragment as AlbumMenuFragment).removeAlbumActionFragment()
                is AlbumInfoFragment -> (parentFragment as AlbumInfoFragment).removeActionFragment()
                is AlbumSearchDrawingFragment -> (parentFragment as AlbumSearchDrawingFragment).removeActionFragment()
                is AlbumExpositionInfoFragment -> (parentFragment as AlbumExpositionInfoFragment).removeVisualizeFragment()
            }
        }

        return view
    }

    private fun setUpImageView(view: View) {
        imageView = view.findViewById(R.id.drawingImageView)
        val img = imageString.replace("data:image/png;base64,", "")
        val decodedString: ByteArray = Base64.decode(img, Base64.DEFAULT)
        //drawEvents
        var decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)

        //background
        imgBitmap = Bitmap.createBitmap(decodedByte.width, decodedByte.height, Bitmap.Config.ARGB_8888)
        canvas.setBitmap(imgBitmap)
        val basePaint = Paint()
        basePaint.color = Color.WHITE
        canvas.drawPaint(basePaint)
        canvas.drawBitmap(decodedByte, Matrix(), null)
        imageView.setImageBitmap(imgBitmap)
    }

}