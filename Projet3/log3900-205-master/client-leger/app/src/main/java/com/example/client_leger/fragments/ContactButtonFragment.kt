package com.example.client_leger.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.add
import androidx.fragment.app.commit
import com.example.client_leger.R

class ContactButtonFragment : Fragment() {
    private lateinit var contactButton: TextView

    private lateinit var sideBarFragment: SideBarFragment

    var isEnabled: Boolean = false

     override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
         val view = inflater.inflate(R.layout.fragment_contact_button, container, false)

         sideBarFragment = parentFragment as SideBarFragment

         contactButton = view.findViewById(R.id.contactButton)
         contactButton.setOnClickListener { toggleContactLists() }

         return view
    }

    private fun toggleContactLists() {
        sideBarFragment.removeActionFragment()

        if(!isEnabled) {
            parentFragmentManager.commit {
                add<ContactListFragment>(R.id.sideBarActionContainer, "sideBarAction")
            }
            isEnabled = true
        }
        else {
            isEnabled = false
        }
    }

}