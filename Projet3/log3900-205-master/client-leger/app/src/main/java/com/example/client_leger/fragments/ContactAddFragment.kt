package com.example.client_leger.fragments

import android.media.MediaPlayer
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.client_leger.R
import com.example.client_leger.classes.*
import com.example.client_leger.services.SoundPlayer
import com.example.client_leger.viewModels.ContactViewModel
import com.shopify.promises.Promise
import java.nio.charset.Charset

class ContactAddFragment : Fragment(), OnClickAddContact {
    private val viewModel: ContactViewModel by activityViewModels()

    private lateinit var seeContactsBtn: Button
    private lateinit var sideBarFragment: SideBarFragment
    private lateinit var usersListView: RecyclerView
    private lateinit var usersAdapter: ContactAddAdapter
    private var usersArray: ArrayList<User> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_contact_add, container, false)

        sideBarFragment = parentFragment as SideBarFragment

        seeContactsBtn = view.findViewById(R.id.comeBackBtn)
        seeContactsBtn.setOnClickListener { showContactList() }

        setUpContactListView(view)
        getUserList()

        return view
    }

    private fun showContactList() {
        sideBarFragment.inflateContactList()
    }

    private fun setUpContactListView(view: View) {
        usersListView = view.findViewById(R.id.contactList)
        usersAdapter = ContactAddAdapter(usersArray, this)
        usersListView.adapter = usersAdapter
        usersListView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
    }

    private fun getUserList() {
        usersArray.clear()
        viewModel.getUserList().whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    usersArray.addAll(it.value)
                    usersAdapter.notifyDataSetChanged()
                }
                is Promise.Result.Error -> {
                    println(it.error.message)
                }
            }
        }
    }

    private fun addContact(user: User) {
        viewModel.addContact(user.id).whenComplete {
            SoundPlayer.playSound(SoundType.NewContact, requireContext())
            removeUserFromList(user)
            usersAdapter.notifyDataSetChanged()
        }
    }

    private fun removeUserFromList(user: User) {
        val index = usersArray.indexOfFirst{u -> u.id == user.id}
        if(index != -1) {
            usersArray.removeAt(index)
        }
    }

    override fun OnClickAddContact(user: User) {
        addContact(user)
    }
}