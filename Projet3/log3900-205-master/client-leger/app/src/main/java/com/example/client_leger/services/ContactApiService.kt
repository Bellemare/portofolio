package com.example.client_leger.services

import com.example.client_leger.classes.*
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.coroutines.awaitObjectResult
import com.github.kittinunf.result.Result

object ContactApiService: ApiService() {
    private val BASE_URL = "/Contact";

    suspend fun getContacts(): Result<ArrayList<Contact>, FuelError> {
        return handleResponse(get(RequestConfig(url= BASE_URL)).awaitObjectResult(Contact.ContactListDeserializer()))
    }

    suspend fun getUserList(): Result<ArrayList<User>, FuelError> {
        return handleResponse(get(RequestConfig(url="${BASE_URL}/UserList")).awaitObjectResult(User.UserListDeserializer()))
    }

    suspend fun getCollaborativeSessions(id: Int): Result<ArrayList<CollaborationSession>, FuelError> {
        return handleResponse(get(RequestConfig(url="${BASE_URL}/${id}/CollaborativeSessions")).awaitObjectResult(CollaborationSession.ListDeserializer()))
    }

    suspend fun getContactExpositions(id: Int): Result<ArrayList<Album>, FuelError> {
        return handleResponse(get(RequestConfig(url="${BASE_URL}/${id}/Expositions")).awaitObjectResult(Album.AlbumListDeserializer()))
    }

    suspend fun addContact(id: Int): Result<Contact, FuelError> {
        return handleResponse(post(RequestConfig(url= BASE_URL), AddContactRequest(id)).awaitObjectResult(Contact.Deserializer()))
    }

    suspend fun deleteContact(id: Int): Result<RequestResponse, FuelError> {
        return handleResponse(delete(RequestConfig(url="${BASE_URL}/${id}")).awaitObjectResult(RequestResponse.Deserializer()))
    }
}