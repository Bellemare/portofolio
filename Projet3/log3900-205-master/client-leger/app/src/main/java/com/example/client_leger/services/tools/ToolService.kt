package com.example.client_leger.services.tools

import android.graphics.Paint
import android.view.MotionEvent
import com.example.client_leger.classes.Color
import com.example.client_leger.classes.toolProperties.ToolProperties
import com.example.client_leger.classes.Vec2
import com.example.client_leger.classes.drawing.DrawingElement
import com.example.client_leger.classes.drawing.ToolType
import com.example.client_leger.services.DrawingService
import com.example.client_leger.services.drawingServices.ToolDrawingService
import kotlin.reflect.KMutableProperty
import kotlin.reflect.full.memberProperties

abstract class ToolService {
    protected open val toolDrawingService: ToolDrawingService = ToolDrawingService()

    protected open val defaultConfiguration: ToolProperties = ToolProperties()
    open val toolType: ToolType = ToolType.UNDEFINED

    protected open val previewDrawingElement: DrawingElement = DrawingElement()
    protected open val drawingElement: DrawingElement = DrawingElement()
    protected var dataChangeListener: ((DrawingElement) -> Unit)? = null
    protected var previewDataChangeListener: ((DrawingElement) -> Unit)? = null

    protected open val currentAttributes: ToolProperties = ToolProperties()

    protected var actionDown : Boolean = false
    protected lateinit var actionDownCoord : Vec2

    open fun onInit() {
        clearData()
        actionDown = false
        actionDownCoord = Vec2(0.0f,0.0f)
    }

    open fun onDestroy() {}

    open fun setProperty(key: String, value: Any) {
        val property = currentAttributes::class.memberProperties.find { it.name == key }
        if (property is KMutableProperty<*>) {
            property.setter.call(currentAttributes, value)
        }
    }

    open fun getStrokeColor(): Color {
        return DrawingService.primaryColor
    }

    open fun getFillColor(): Color {
        return DrawingService.secondaryColor
    }

    open fun getTextColor(): Color {
        return DrawingService.textColor
    }

    open fun getToolAttributes(): ToolProperties {
        return currentAttributes
    }

    fun getDrawingService(): ToolDrawingService {
        return toolDrawingService
    }

    fun listenPreviewChanged(listener: (DrawingElement) -> Unit) {
        previewDataChangeListener = listener
    }

    fun listenDataChanged(listener: (DrawingElement) -> Unit) {
        dataChangeListener = listener
    }

    abstract fun onActionDown(event: MotionEvent)

    abstract fun onActionMove(event: MotionEvent)

    abstract fun onActionUp(event: MotionEvent)

    abstract fun setPaintProperties(paint: Paint, properties: ToolProperties?)

    abstract fun clearData()

    protected fun dataChanged() {
        val listener = dataChangeListener
        if (listener != null ) {
            listener(drawingElement)
        }
    }

    protected fun previewChanged() {
        val listener = previewDataChangeListener
        if (listener != null ) {
            listener(previewDrawingElement)
        }
    }
}