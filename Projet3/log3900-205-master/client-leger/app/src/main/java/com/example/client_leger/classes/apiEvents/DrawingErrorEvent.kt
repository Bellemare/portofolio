package com.example.client_leger.classes.apiEvents


class DrawingErrorEvent(val error: String): AppEvent() {
    override val type = AppEventType.DRAWING_ERROR_EVENT
}