package com.example.client_leger.classes.toolProperties

import com.example.client_leger.classes.drawing.DrawingType
import com.google.gson.Gson

open class ToolProperties {
    open var drawingType: DrawingType = DrawingType.Stroke
    open var lineWidth: Int = 1

    open fun deepCopy(): ToolProperties {
        val parser = Gson()
        return parser.fromJson(parser.toJson(this), ToolProperties::class.java)
    }
}