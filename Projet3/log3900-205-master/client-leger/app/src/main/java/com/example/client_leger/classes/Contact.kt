package com.example.client_leger.classes

import com.example.client_leger.getApiMapper
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class Contact(
    val id: Int,
    val userId: Int,
    val contactId: Int,
    val contact: String,
    val contactAvatar: String,
    val isConnected: Boolean,
    val isInCollaborativeSession: Boolean,
    val collaborativeSession: ContactCollaborativeSession?
) {
    fun deepCopy(): Contact {
        val parser = Gson()
        return parser.fromJson(parser.toJson(this), Contact::class.java)
    }

    class ContactListDeserializer : ResponseDeserializable<ArrayList<Contact>> {
        override fun deserialize(content: String): ArrayList<Contact> {
            val mapper = getApiMapper()
            return mapper.readValue(content, mapper.typeFactory.constructCollectionType(ArrayList::class.java, Contact::class.java))
        }
    }

    class Deserializer : ResponseDeserializable<Contact> {
        override fun deserialize(content: String) =
            getApiMapper().readValue(content, Contact::class.java)
    }
}
