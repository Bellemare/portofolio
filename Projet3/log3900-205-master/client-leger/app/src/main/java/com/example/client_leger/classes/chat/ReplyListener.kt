package com.example.client_leger.classes.chat

import android.view.View
import com.example.client_leger.classes.sockets.MessagePayload

interface ReplyListener {
    fun onReplyLongClick(message: MessagePayload, view: View)
}