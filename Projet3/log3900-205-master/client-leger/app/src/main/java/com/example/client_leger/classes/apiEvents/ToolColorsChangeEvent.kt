package com.example.client_leger.classes.apiEvents

import com.example.client_leger.classes.Color

class ToolColorsChangeEvent(val primary: Color, val secondary: Color, val text: Color): AppEvent() {
    override val type = AppEventType.TOOL_COLORS_CHANGE_EVENT
}