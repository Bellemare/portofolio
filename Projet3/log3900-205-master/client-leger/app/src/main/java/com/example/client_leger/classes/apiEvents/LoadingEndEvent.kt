package com.example.client_leger.classes.apiEvents

class LoadingEndEvent(): AppEvent() {
    override val type = AppEventType.LOADING_END_EVENT
}