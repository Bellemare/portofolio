package com.example.client_leger.classes.chat

object MessageTypeConst{
    val URGENT_COLOR = "#1Fce3f3f"
    val INFO_COLOR = "#1Fceb93f"
    val ANNOUNCEMENT_COLOR = "#1Fce6c3f"
    val DEFAULT_COLOR = "#222222"

    val URGENT_COLOR_WRITER = "#ce3f3f"
    val INFO_COLOR_WRITER = "#ceb93f"
    val ANNOUNCEMENT_COLOR_WRITER = "#ce6c3f"
    val DEFAULT_COLOR_WRITER = "#FFFFFF"


    val DEFAULT_INT = 0
    val URGENT_INT = 1
    val INFO_INT = 2
    val ANNOUNCEMENT_INT = 3

    val URGENT_ICON = "\uf12a"
    val INFO_ICON = "\uf129"
    val ANNOUNCEMENT_ICON = "\uf0a1"
}
