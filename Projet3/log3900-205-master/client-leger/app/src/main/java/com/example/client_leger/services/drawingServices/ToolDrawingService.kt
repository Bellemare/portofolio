package com.example.client_leger.services.drawingServices

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import com.example.client_leger.classes.drawing.DrawingElement
import com.example.client_leger.classes.drawing.DrawingEvent
import com.example.client_leger.services.tools.ToolService

open class ToolDrawingService {
    protected fun prepareDraw(paint: Paint, event: DrawingEvent) {
        paint.color = Color.argb(event.strokeColor.a * 255, event.strokeColor.r, event.strokeColor.g, event.strokeColor.b)
    }

    fun createDrawingEvent(data: DrawingElement, tool: ToolService): DrawingEvent {
        return DrawingEvent(
            data,
            tool.getToolAttributes(),
            null,
            tool.toolType.value,
            tool.getToolAttributes().drawingType.value,
            tool.getStrokeColor(),
            tool.getFillColor(),
            tool.getTextColor(),
        )
    }

    open fun drawEvent(canvas: Canvas, paint: Paint, event: DrawingEvent): DrawingEvent {
        return event
    }

    open fun isEmpty(element: DrawingElement): Boolean {
        return false
    }
}