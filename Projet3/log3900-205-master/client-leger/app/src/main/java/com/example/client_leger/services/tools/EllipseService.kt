package com.example.client_leger.services.tools

import com.example.client_leger.classes.Vec2
import com.example.client_leger.classes.drawing.DrawingEllipse
import com.example.client_leger.classes.drawing.ToolType
import com.example.client_leger.services.drawingServices.EllipseDrawingService

class EllipseService: ShapeService() {
    override var previewDrawingElement: DrawingEllipse = DrawingEllipse(0f,0f,0f,0f)
    override var drawingElement: DrawingEllipse = DrawingEllipse(0f,0f,0f,0f)
    override val toolDrawingService: EllipseDrawingService = EllipseDrawingService()
    override val toolType: ToolType
        get() = ToolType.ELLIPSE

    override fun updatePreviewData(actionCoord: Vec2) {
        previewDrawingElement.x = Math.round((actionCoord.x + actionDownCoord.x) / 2f).toFloat()
        previewDrawingElement.y = Math.round((actionCoord.y + actionDownCoord.y) / 2f).toFloat()
        previewDrawingElement.radiusX = Math.round(Math.abs(actionCoord.x - actionDownCoord.x) / 2f).toFloat()
        previewDrawingElement.radiusY = Math.round(Math.abs(actionCoord.y - actionDownCoord.y) / 2f).toFloat()
    }

    override fun copyPreviewInData() {
        drawingElement = previewDrawingElement.deepCopy() as DrawingEllipse
    }

    override fun clearData() {
        previewDrawingElement = DrawingEllipse(0f,0f,0f,0f)
        drawingElement = DrawingEllipse(0f,0f,0f,0f)
    }
}