package com.example.client_leger.services.drawingServices

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import com.example.client_leger.classes.Vec2
import com.example.client_leger.classes.drawing.*
import kotlin.math.*

class EllipseDrawingService: ToolDrawingService() {
    override fun drawEvent(canvas: Canvas, paint: Paint, event: DrawingEvent): DrawingEvent {

        if(isShouldFill(event)) {
            paint.style = Paint.Style.FILL
            paint.color = Color.argb(event.strokeColor.a * 255, event.strokeColor.r, event.strokeColor.g, event.strokeColor.b)
            canvas.drawOval(createFillRectF(event), paint)
        } else {
            paint.style = Paint.Style.FILL
            paint.color = Color.argb(event.fillColor.a * 255, event.fillColor.r, event.fillColor.g, event.fillColor.b)
            canvas.drawOval(createFillRectF(event), paint)

            paint.style = Paint.Style.STROKE
            paint.color = Color.argb(event.strokeColor.a * 255, event.strokeColor.r, event.strokeColor.g, event.strokeColor.b)
            canvas.drawOval(createStrokeRectF(event), paint)
        }
        if (event.data.baseBoundingBox == null) {
            event.data.baseBoundingBox = setBoundingBox(event, 0.0f)
        }
        val textHeight = drawText(canvas, event)
        if (event.data.boundingBox == null) {
            event.data.boundingBox = setBoundingBox(event, textHeight)
        }

        return event
    }

    private fun drawText(canvas: Canvas, event: DrawingEvent): Float {
        val data = event.data as DrawingEllipse
        val lw = event.attributes.lineWidth
        val pos = Vec2(data.x, data.y - data.radiusY + lw + TextDrawingService.LINE_HEIGHT / 2 + TextDrawingService.PADDING.y + 5)
        return TextDrawingService.drawText(canvas, pos, event)
    }

    private fun setBoundingBox(event: DrawingEvent, textHeight: Float): BoundingBox {
        val data = event.data as DrawingEllipse
        return BoundingBox(
            floor(data.x - abs(data.radiusX)),
            floor(data.y - abs(data.radiusY)),
            ceil(abs(data.radiusX) * 2),
            max(ceil(data.radiusY * 2), textHeight + (TextDrawingService.PADDING.y * 2)),
        )
    }

    private fun convertCenterDataToRectF(event: DrawingEvent): RectF {
        val data: DrawingEllipse = event.data as DrawingEllipse

        val left: Float = data.x - abs(data.radiusX)
        val top: Float = data.y - abs(data.radiusY)
        val right: Float = data.x + abs(data.radiusX)
        val bottom: Float = data.y + abs(data.radiusY)

        return RectF(left.toInt().toFloat(), top.toInt().toFloat(), right.toInt().toFloat(), bottom.toInt().toFloat())
    }

    private fun isShouldFill(event: DrawingEvent): Boolean {
        val oval: RectF = convertCenterDataToRectF(event)

        val adjustedWidth = Math.max(0f, abs(oval.width()) - event.attributes.lineWidth)
        val adjustedHeight = Math.max(0f, abs(oval.height()) - event.attributes.lineWidth)

        return event.attributes.lineWidth > adjustedWidth || event.attributes.lineWidth > adjustedHeight
    }

    private fun createStrokeRectF(event: DrawingEvent): RectF {
        val oval: RectF = convertCenterDataToRectF(event)
        val sizeAdjustment = (event.attributes.lineWidth / 2f).toInt()

        val xDirection: Float = sign(oval.width())
        val yDirection: Float = sign(oval.height())

        val left: Float = oval.left + sizeAdjustment * xDirection
        val top: Float = oval.top + sizeAdjustment * yDirection
        val right: Float = oval.left + oval.width() - sizeAdjustment * xDirection
        val bottom: Float = oval.top + oval.height() - sizeAdjustment * yDirection

        return RectF(left, top, right, bottom)
    }

    private fun createFillRectF(event: DrawingEvent): RectF {
        val oval: RectF = convertCenterDataToRectF(event)

        val right: Float = oval.left + oval.width()
        val bottom: Float = oval.top + oval.height()

        return RectF(oval.left, oval.top, right, bottom)
    }

    override fun isEmpty(element: DrawingElement): Boolean {
        val drawingElement = element as DrawingEllipse

        return drawingElement.radiusY <= 0.5 || drawingElement.radiusX <= 0.5
    }
}