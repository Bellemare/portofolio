package com.example.client_leger.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.client_leger.R
import com.example.client_leger.classes.*
import com.example.client_leger.classes.apiEvents.AppEvents
import com.example.client_leger.classes.apiEvents.AvatarChangeEvent
import com.example.client_leger.viewModels.AccountCreationViewModel
import com.example.client_leger.viewModels.UserViewModel
import com.example.client_leger.views.AvatarView
import com.github.kittinunf.fuel.core.FuelError
import com.shopify.promises.Promise

class AccountCreationFragment : Fragment(), OnClickAvatar {
    private lateinit var editEmail: EditText
    private lateinit var editPassword: EditText
    private lateinit var editPasswordRepeat: EditText
    private lateinit var editPseudonym: EditText
    private lateinit var loginBtn: Button
    private lateinit var newUserBtn: Button
    private lateinit var errorsText: TextView

    private lateinit var cancelBtn: Button
    private lateinit var selectAvatarBtn: Button
    private lateinit var avatarAdapter: DefaultAvatarListAdapter
    private lateinit var avatarView: AvatarView
    private lateinit var avatarRecyclerView: RecyclerView
    private lateinit var currentAvatar: String
    private lateinit var defaultAvatarsLinearLayout: LinearLayout

    private lateinit var inputs: HashMap<String, TextView>
    private lateinit var accountCreationViewModel: AccountCreationViewModel

    private var avatarArray: ArrayList<String> = ArrayList()
    private var isSelectionOpen: Boolean = false
    private var selectedPositon: Int = -1
    private val userViewModel: UserViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_account_creation, container, false)

        accountCreationViewModel = ViewModelProvider(this).get(AccountCreationViewModel::class.java)
        editEmail = view.findViewById(R.id.editTextEmail)
        editPassword = view.findViewById(R.id.editTextPassword)
        editPasswordRepeat = view.findViewById(R.id.editTextPasswordRepeat)
        editPseudonym = view.findViewById(R.id.editTextPseudonym)
        loginBtn = view.findViewById(R.id.loginBtn)
        newUserBtn = view.findViewById(R.id.newUserBtn)
        errorsText = view.findViewById(R.id.errors)
        inputs = hashMapOf(
            "email" to editEmail,
            "password" to editPassword,
            "password_repeat" to editPasswordRepeat,
            "pseudonym" to editPseudonym,
            "loginBtn" to loginBtn,
            "newUserBtn" to newUserBtn)

        loginBtn.setOnClickListener { login(view) }
        newUserBtn.setOnClickListener { createNewAccount(view) }

        initAvatarReferences(view)
        listenToError()

        return view
    }

    private fun initAvatarReferences(view: View) {
        avatarView = view.findViewById(R.id.avatarView)
        cancelBtn = view.findViewById(R.id.cancelBtn)
        defaultAvatarsLinearLayout = view.findViewById(R.id.defaultAvatarsLinearLayout)
        selectAvatarBtn = view.findViewById(R.id.selectAvatarBtn)

        avatarView.setOnClickListener { openCloseAvatarSelection() }
        cancelBtn.setOnClickListener { openCloseAvatarSelection() }
        selectAvatarBtn.setOnClickListener { selectAvatar() }

        setUpAvatarRecyclerView(view)
        getDefaultAvatars()
    }

    private fun setUpAvatarRecyclerView(view: View) {
        avatarRecyclerView = view.findViewById(R.id.imageChoiceList)
        avatarAdapter = DefaultAvatarListAdapter(avatarArray, this)
        avatarRecyclerView.adapter = avatarAdapter
        avatarRecyclerView.layoutManager = GridLayoutManager(context, 3, RecyclerView.VERTICAL, false)
    }

    private fun getDefaultAvatars() {
        avatarArray.clear()
        userViewModel.getDefaultAvatars().whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    avatarArray.addAll(it.value)
                    currentAvatar = avatarArray[0]
                    initAvatarImage(currentAvatar)
                    avatarAdapter.notifyDataSetChanged()
                }
                is Promise.Result.Error -> {
                    println(it.error.message)
                }
            }
        }
    }

    private fun openCloseAvatarSelection() {
        if(isSelectionOpen) {
            defaultAvatarsLinearLayout.visibility = View.INVISIBLE
        }
        else {
            defaultAvatarsLinearLayout.visibility = View.VISIBLE
        }
        isSelectionOpen = !isSelectionOpen
    }

    private fun selectAvatar() {
        if(selectedPositon >= 0 && selectedPositon < avatarArray.size) {
            currentAvatar = avatarArray[selectedPositon]
            initAvatarImage(currentAvatar)
            openCloseAvatarSelection()
        }
        else {
            println("error no avatar selected")
        }
    }

    private fun initAvatarImage(avatar: String) {
        avatarView.initImage(avatar, Vec2(300f,300f))
        avatarView.postInvalidate()
    }

    override fun OnClickAvatar(position: Int) {
        selectedPositon = position
    }

    private fun listenToError() {
        accountCreationViewModel.getErrorData().observe(viewLifecycleOwner) {
            setErrorText(it)
        }
    }

    private fun disableEnableInputs(enabled: Boolean) {
        inputs.forEach {
            it.value.isEnabled = enabled
        }
        disableEnableAvatarInputs(enabled)
    }

    private fun disableEnableAvatarInputs(enabled: Boolean) {
        avatarView.isEnabled = enabled
        selectAvatarBtn.isEnabled = enabled
    }

    private fun createNewAccount(view: View) {
        disableEnableInputs(false)
        val registerRequest = RegisterRequest( email = editEmail.text.toString(), password = editPassword.text.toString(),
            passwordRepeat = editPasswordRepeat.text.toString(), pseudonym = editPseudonym.text.toString(), avatar = currentAvatar)
        accountCreationViewModel.createNewAccount(registerRequest).whenComplete { result: Promise.Result<User, FuelError> ->
            when (result) {
                is Promise.Result.Success -> {
                    disableEnableInputs(true)
                    Navigation.findNavController(view).navigate(R.id.action_accountCreationFragment_to_loginFragment)
                }
                is Promise.Result.Error -> {
                    disableEnableInputs(true)
                }
            }
        }
    }

    private fun setErrorText(error: RequestFieldErrorResponse) {
        val text: String = "\u2022 " + error.body.joinToString("\n\u2022 ")
        errorsText.text = text
        inputs.forEach {
            if (error.fields.contains(it.key)) {
                it.value.setHintTextColor(ContextCompat.getColor(requireActivity(), R.color.errorColor))
                it.value.setTextColor(ContextCompat.getColor(requireActivity(), R.color.errorColor))
            } else {
                it.value.setHintTextColor(ContextCompat.getColor(requireActivity(), R.color.white))
                it.value.setTextColor(ContextCompat.getColor(requireActivity(), R.color.white))
            }
        }
    }

    private fun login(view: View) {
        Navigation.findNavController(view).navigate(R.id.action_accountCreationFragment_to_loginFragment)
    }
}