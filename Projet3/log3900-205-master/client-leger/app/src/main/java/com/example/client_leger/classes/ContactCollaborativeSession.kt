package com.example.client_leger.classes

data class ContactCollaborativeSession(val drawingId: Int, val drawing: String, val isPasswordProtected: Boolean, val albumId: Int, val album: String)
