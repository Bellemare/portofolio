package com.example.client_leger.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.fragment.app.activityViewModels
import com.example.client_leger.R
import com.example.client_leger.classes.AlbumModel
import com.example.client_leger.classes.RequestFieldErrorResponse
import com.example.client_leger.viewModels.AlbumMenuViewModel
import com.shopify.promises.Promise
import java.nio.charset.Charset

class AlbumCreationFragment : Fragment() {
    private lateinit var createBtn: Button
    private lateinit var cancelBtn: Button

    private lateinit var albumName: EditText
    private lateinit var albumDescription: EditText

    private val viewModel: AlbumMenuViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_album_creation, container, false)

        albumName = view.findViewById(R.id.newAlbumName)
        albumDescription = view.findViewById(R.id.newAlbumDescription)

        setUpClosingButtons(view)

        return view
    }

    private fun setUpClosingButtons(view: View) {
        createBtn = view.findViewById(R.id.createBtn)
        cancelBtn = view.findViewById(R.id.cancelBtn)

        createBtn.setOnClickListener {
            val name = albumName.text.toString()
            val description = albumDescription.text.toString()

            viewModel.createAlbum(AlbumModel(name,description)).whenComplete { result ->
                when(result) {
                    is Promise.Result.Success -> {
                        val parent = parentFragment as AlbumMenuFragment
                        parent.removeAlbumActionFragment()
                    }
                    is Promise.Result.Error -> {
                        //handle error
                        val error = RequestFieldErrorResponse.Deserializer().deserialize(result.error.errorData.toString(Charset.defaultCharset()))
                        error.displayError(view)

                        for(field in error.fields){
                            when(field) {
                                "name" -> DrawableCompat.setTint(albumName.background, ContextCompat.getColor(view.context, R.color.errorColor))
                            }
                        }
                    }
                }
            }

        }
        cancelBtn.setOnClickListener {
            (parentFragment as AlbumMenuFragment).removeAlbumActionFragment()
        }
    }
}