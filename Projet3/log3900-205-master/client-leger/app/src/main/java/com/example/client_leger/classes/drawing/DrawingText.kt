package com.example.client_leger.classes.drawing

class DrawingText(var text: String): DrawingElement() {
    fun insertText(value: String, position: Int? = -1) {
        if (position != -1) {
            text = text.substring(0, position!!) + value + text.substring(position, text.length);
        } else {
            text += value
        }
    }

    fun breakLine() {
        text += "\n"
    }

    fun back() {
        text = text.substring(0, text.length - 1)
    }

    fun remove(start: Int, end: Int) {
        var startIndex = start
        if (startIndex == end) {
            startIndex -= 1
        }

        text = text.substring(0, start) + text.substring(end, text.length)
    }
}