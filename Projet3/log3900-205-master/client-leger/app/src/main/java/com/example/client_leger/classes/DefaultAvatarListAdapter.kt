package com.example.client_leger.classes

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.client_leger.R
import com.example.client_leger.views.AvatarView

class DefaultAvatarListAdapter(var avatarList: ArrayList<String>, listener: OnClickAvatar): RecyclerView.Adapter<DefaultAvatarListAdapter.ViewHolder>() {
    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        var avatarItem: AvatarView = view.findViewById(R.id.avatarItem)
        var cardView: CardView = view.findViewById(R.id.cardView)
    }

    private var selectedPosition: Int = -1
    private var itemClickListeer: OnClickAvatar = listener

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.fragment_avatar_list_item, viewGroup, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val resources = holder.itemView.context.resources
        val currentAvatar = avatarList[position]

        holder.avatarItem.initImage(currentAvatar, Vec2(100f, 100f))

        holder.cardView.setCardBackgroundColor(resources.getColor(R.color.backgroundColor))

        if(selectedPosition == position)
            holder.cardView.setCardBackgroundColor(resources.getColor(R.color.highlightedColor))

        holder.itemView.isSelected = selectedPosition == position

        holder.itemView.setOnClickListener { setSelectPosition(position) }
    }


    private fun setSelectPosition (position: Int) {
        notifyItemChanged(selectedPosition)
        selectedPosition = position
        itemClickListeer.OnClickAvatar(selectedPosition)
        notifyItemChanged(selectedPosition)
    }

    override fun getItemCount(): Int {
        return avatarList.size
    }
}