package com.example.client_leger.classes.apiEvents

class ColorChangeEvent(): AppEvent() {
    override val type = AppEventType.COLOR_CHANGE_EVENT
}