package com.example.client_leger.classes.drawing

import com.example.client_leger.classes.TextMeasurer
import com.example.client_leger.classes.toolProperties.ToolProperties
import kotlin.math.max


open class DrawingShape(var text: DrawingText? = null): DrawingElement() {
    open fun generateTextLines(attributes: ToolProperties): Array<String> {
        if (text == null) {
            return arrayOf()
        }

        return text!!.text.split("\n").toTypedArray()
    }

    protected fun getNewTextLine(line: String, maxWidthHandler: () -> Float): String {
        val lineWidth = TextMeasurer.measureText(line)
        val maxWidth = max(maxWidthHandler(), 10.0f)

        if (lineWidth > maxWidth && line.length > 1) {
            var charIndex = 1
            var width = TextMeasurer.measureText(line.substring(0, charIndex++))
            while (width < maxWidth && charIndex < line.length) {
                charIndex++
                width = TextMeasurer.measureText(line.substring(0, charIndex))
                if (line.substring(0, charIndex - 1).length <= 1 && width >= maxWidth)
                    break
            }

            var newLine = line.substring(0, charIndex - 1)
            val nextLine = line.substring(charIndex - 1, line.length)
            if (nextLine.isNotEmpty()) {
                newLine += "\n" + getNewTextLine(nextLine, maxWidthHandler)
            }

            return newLine
        }

        return line
    }
}