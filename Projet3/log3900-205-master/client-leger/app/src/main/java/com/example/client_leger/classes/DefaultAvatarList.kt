package com.example.client_leger.classes

import com.example.client_leger.getApiMapper
import com.github.kittinunf.fuel.core.ResponseDeserializable

data class DefaultAvatarList(var avatars: ArrayList<String>) {
    class AvatarListDeserializer: ResponseDeserializable<ArrayList<String>> {
        override fun deserialize(content: String): ArrayList<String> {
            val mapper = getApiMapper()
            return mapper.readValue(content, mapper.typeFactory.constructCollectionType(ArrayList::class.java, String::class.java))
        }
    }
}
