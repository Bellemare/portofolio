package com.example.client_leger.classes.chat
import com.example.client_leger.classes.apiEvents.Room

interface JoinRoomListener {
    fun onJoinClick(room: Room)
}