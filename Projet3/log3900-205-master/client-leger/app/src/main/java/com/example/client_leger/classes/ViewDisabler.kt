package com.example.client_leger.classes

import android.view.ViewGroup
import androidx.core.view.children

interface ViewDisabler {
    fun enableDisableViewGroup(viewGroup: ViewGroup, enabled: Boolean){
        for(childView in viewGroup.children){
            childView.isEnabled = enabled

            if(childView is ViewGroup){
                enableDisableViewGroup(childView, enabled)
            }
        }
    }
}