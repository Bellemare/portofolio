package com.example.client_leger.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.fragment.app.activityViewModels
import com.example.client_leger.R
import com.example.client_leger.classes.Album
import com.example.client_leger.classes.AlbumModel
import com.example.client_leger.classes.RequestFieldErrorResponse
import com.example.client_leger.classes.RequestResponse
import com.example.client_leger.viewModels.AlbumMenuViewModel
import com.shopify.promises.Promise
import java.nio.charset.Charset

class AlbumJoinRequestFragment : Fragment() {

    private lateinit var requestButton: Button
    private lateinit var cancelBtn: Button

    private lateinit var albumName: TextView
    private lateinit var albumDescription: TextView
    private lateinit var album: Album

    private val viewModel: AlbumMenuViewModel by activityViewModels()
    private var isWaitingResponse: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_album_join_request, container, false)
        album = requireArguments().get("album") as Album

        setUpClosingButtons(view)
        setUpAlbumInfo(view)

        return view
    }

    private fun setUpAlbumInfo(view: View) {
        albumName = view.findViewById(R.id.albumName)
        albumName.text = album.name

        albumDescription = view.findViewById(R.id.albumDescription)
        albumDescription.text = album.description
    }

    private fun setUpClosingButtons(view: View) {
        requestButton = view.findViewById(R.id.joinRequestButton)
        cancelBtn = view.findViewById(R.id.cancelBtn)

        requestButton.setOnClickListener(null)
        requestButton.setOnClickListener {
            if(!isWaitingResponse){
                isWaitingResponse = true
                val id: Int = album.id
                viewModel.requestToJoinAlbum(id).whenComplete { result ->
                    isWaitingResponse = false
                    when(result) {
                        is Promise.Result.Success -> {
                            val requestResponse: RequestResponse = result.value
                            requestResponse.displayResponse(view)

                            (parentFragment as AlbumJoinFragment).removeAlbumActionFragment()
                        }
                        is Promise.Result.Error -> {
                            //handle error
                            val error = RequestResponse.Deserializer().deserialize(result.error.errorData.toString(Charset.defaultCharset()))
                            error.displayResponse(view)
                        }
                    }
                }
            }
        }
        cancelBtn.setOnClickListener {
            (parentFragment as AlbumJoinFragment).removeAlbumActionFragment()
        }
    }

}