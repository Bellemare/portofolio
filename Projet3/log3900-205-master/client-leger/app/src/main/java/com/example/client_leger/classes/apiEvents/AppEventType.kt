package com.example.client_leger.classes.apiEvents

enum class AppEventType(val code: Int) {
    UNAUTHORIZED(401),
    DISCONNECTION(0),
    PREVIEW_DRAW_EVENT(1),
    DRAW_EVENT(2),
    TOOL_PROPERTIES_CHANGE_EVENT(3),
    TOOL_PROPERTY_SET_EVENT(4),
    COLOR_CHANGE_EVENT(5),
    SELECTION_CHANGE_EVENT(6),
    SELECTION_RESET_EVENT(7),
    SELECTION_START_EVENT(8),
    SELECTION_STOP_EVENT(9),
    AVATAR_CHANGE_EVENT(10),
    ON_SELECT_EVENT(11),
    SELECTION_DELETE_EVENT(12),
    SELECTION_TEXT_CHANGE_EVENT(13),
    LOADING_START_EVENT(14),
    LOADING_END_EVENT(15),
    DRAWING_ERROR_EVENT(16),
    CLIENT_STATE_SYNC_EVENT(17),
    CLIENT_STATE_EVENT(18),
    TOOL_COLORS_CHANGE_EVENT(19),
}