package com.example.client_leger.classes

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.client_leger.R
import com.example.client_leger.views.AvatarView

class ContactAddAdapter(var usersList: ArrayList<User>, listener: OnClickAddContact): RecyclerView.Adapter<ContactAddAdapter.ViewHolder>() {
    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

        var contactAvatar: AvatarView = view.findViewById(R.id.avatarView)
        var userName: TextView = view.findViewById(R.id.userName)
        var addContactBtn: TextView = view.findViewById(R.id.addContactBtn)

    }

    var addListener: OnClickAddContact = listener

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.fragment_contact_add_item, viewGroup, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user = usersList[position]
        val userName = if(user.pseudonym.length < 25) user.pseudonym else user.pseudonym.substring(IntRange(0,22)) + "..."
        holder.userName.text = userName

        holder.contactAvatar.initImage(user.avatar, Vec2(80f, 80f))
        val bundle = bundleOf(Pair("userId", user.id))
        holder.contactAvatar.setOnClickListener { Navigation.findNavController(holder.itemView).navigate(R.id.contactFragment, bundle) }

        holder.addContactBtn.setOnClickListener { addListener.OnClickAddContact(user) }
    }

    override fun getItemCount(): Int {
        return usersList.size
    }
}