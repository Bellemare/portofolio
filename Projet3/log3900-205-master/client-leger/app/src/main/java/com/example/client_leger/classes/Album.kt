package com.example.client_leger.classes
import android.os.Parcel
import android.os.Parcelable
import com.example.client_leger.getApiMapper
import com.github.kittinunf.fuel.core.ResponseDeserializable

data class Album(val id: Int, var name: String, var description: String, val ownerId: Int?): Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readValue(Int::class.java.classLoader) as? Int
    ) {
    }

    class AlbumListDeserializer : ResponseDeserializable<ArrayList<Album>> {
        override fun deserialize(content: String): ArrayList<Album> {
            val mapper = getApiMapper()
            return mapper.readValue(content, mapper.typeFactory.constructCollectionType(ArrayList::class.java, Album::class.java))
        }
    }

    class Deserializer : ResponseDeserializable<Album> {
        override fun deserialize(content: String): Album =
            getApiMapper().readValue(content, Album::class.java)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(name)
        parcel.writeString(description)
        parcel.writeValue(ownerId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Album> {
        override fun createFromParcel(parcel: Parcel): Album {
            return Album(parcel)
        }

        override fun newArray(size: Int): Array<Album?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return name
    }
}

data class AlbumModel(val name: String, val description: String) {}
