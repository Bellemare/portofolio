package com.example.client_leger.services

import com.example.client_leger.classes.*
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.coroutines.awaitObjectResult
import com.github.kittinunf.result.Result


class AlbumService: ApiService() {
    val BASE_URL = "/Album"

    suspend fun getUserAlbums(): Result<ArrayList<Album>, FuelError> {
        val url = "$BASE_URL/JoinedAlbums"
        return handleResponse(get(RequestConfig(url=url)).awaitObjectResult(Album.AlbumListDeserializer()))
    }

    suspend fun getAlbumList(): Result<ArrayList<Album>, FuelError> {
        val url = "$BASE_URL"
        return handleResponse(get(RequestConfig(url=url)).awaitObjectResult(Album.AlbumListDeserializer()))
    }

    suspend fun requestToJoinAlbum(albumId: Int): Result<RequestResponse, FuelError> {
        val url = "$BASE_URL/$albumId/RequestToJoin"
        return handleResponse(post(RequestConfig(url=url), null).awaitObjectResult(RequestResponse.Deserializer()))
    }

    suspend fun create(album: AlbumModel): Result<Album, FuelError>  {
        val url = BASE_URL
        return handleResponse(post(RequestConfig(url=url), album).awaitObjectResult(Album.Deserializer()))
    }

    suspend fun getDrawings(albumId: Int): Result<ArrayList<Drawing>, FuelError> {
        val url = "$BASE_URL/$albumId/Drawings"
        return handleResponse(get(RequestConfig(url=url)).awaitObjectResult(Drawing.DrawingListDeserializer()))
    }

    suspend fun update(album: Album): Result<RequestResponse, FuelError> {
        val url = "$BASE_URL/${album.id}"
        return handleResponse(put(RequestConfig(url=url), album).awaitObjectResult(RequestResponse.Deserializer()))
    }

    suspend fun delete(albumId: Int): Result<RequestResponse, FuelError> {
        val url = "$BASE_URL/${albumId}"
        return handleResponse(delete(RequestConfig(url=url)).awaitObjectResult(RequestResponse.Deserializer()))
    }

    suspend fun quitAlbum(albumId: Int): Result<RequestResponse, FuelError> {
        val url = "$BASE_URL/${albumId}/Leave"
        return handleResponse(post(RequestConfig(url=url), null).awaitObjectResult(RequestResponse.Deserializer()))
    }

    suspend fun getAllJoinRequests(): Result<ArrayList<AlbumJoinRequest>, FuelError> {
        val url = "$BASE_URL/PendingJoinRequests"
        return handleResponse(get(RequestConfig(url=url)).awaitObjectResult(AlbumJoinRequest.AlbumJoinRequestListDeserializer()))
    }

    suspend fun acceptJoinRequest(albumId: Int, requestId: Int): Result<RequestResponse, FuelError> {
        val url = "$BASE_URL/$albumId/JoinRequest/$requestId/Accept"
        return handleResponse(post(RequestConfig(url = url), null).awaitObjectResult(RequestResponse.Deserializer()))
    }

    suspend fun declineJoinRequest(albumId: Int, requestId: Int): Result<RequestResponse, FuelError> {
        val url = "$BASE_URL/$albumId/JoinRequest/$requestId/Decline"
        return handleResponse(post(RequestConfig(url = url), null).awaitObjectResult(RequestResponse.Deserializer()))
    }




}