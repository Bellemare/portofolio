package com.example.client_leger.services

import com.example.client_leger.classes.Authentication
import com.example.client_leger.classes.RequestConfig
import com.example.client_leger.classes.RequestResponse
import com.example.client_leger.classes.User
import com.example.client_leger.classes.chat.RoomFetchHistoryRequest
import com.example.client_leger.classes.apiEvents.Room
import com.example.client_leger.classes.sockets.*
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.extensions.jsonBody
import com.github.kittinunf.fuel.coroutines.awaitObjectResult
import com.github.kittinunf.result.Result
import com.shopify.promises.Promise
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

object RoomService: ApiService() {
    val BASE_URL = "/Room"
    val PUBLIC_ROOM_ID = 0

    private var client: OkHttpClient
    private var clientSync = ArrayList<ClientSync>()
    private var activeRooms: HashMap<Int, SocketClient> = hashMapOf()
    private lateinit var drawingRoomClient: SocketClient
    private lateinit var drawingRoomMessageHistory: MessageHistoryPayload
    private var drawingRoomConnected: Boolean = false

    private var appClient: OkHttpClient
    private var appClientPromise: Promise<SocketClient, Unit>? = null
    private var appClientConnection: SocketClient? = null

    init {
        client = OkHttpClient.Builder()
            .writeTimeout(5, TimeUnit.SECONDS)
            .readTimeout(5, TimeUnit.SECONDS)
            .connectTimeout(10, TimeUnit.SECONDS)
            .build()

        appClient = OkHttpClient.Builder()
            .writeTimeout(5, TimeUnit.SECONDS)
            .readTimeout(5, TimeUnit.SECONDS)
            .connectTimeout(10, TimeUnit.SECONDS)
            .build()
    }

    fun getActiveRooms(): HashMap<Int, SocketClient> {
        return activeRooms
    }

    suspend fun getFetchHistory(req: RoomFetchHistoryRequest): Result<ArrayList<MessagePayload>, FuelError> {
        val url = "${BASE_URL}/${req.roomId}/History/Limit/${req.limit}/Offset/${req.offset}"
        return handleResponse(get(RequestConfig(url=url)).awaitObjectResult(MessagePayload.HistoryDeserializer()))
    }

    fun connectRoom(roomId: Int): Promise<SocketClient, Unit> {
        return Promise {
            val conn = SocketClient(client)
            val authToken = Authentication.getAuthenticationToken()
            conn.onClose = {
                reject(Unit)
            }
            conn.connect("${getRoomConnectionUrl(roomId)}/${authToken?.token}", onConnect = {
                conn.onClose = {
                    closeConnection(roomId)
                }
                activeRooms[roomId] = conn
                resolve(conn)
            })
        }
    }

    fun addDrawingRoom(conn: SocketClient) {
        drawingRoomClient = conn
        drawingRoomConnected = true
    }

    fun removeDrawingRoom() {
        drawingRoomConnected = false
    }

    fun setDrawingRoomMessageHistory(messages: MessageHistoryPayload) {
        drawingRoomMessageHistory = messages
    }

    fun getDrawingRoomMessageHistory(): ArrayList<MessagePayload> {
        return drawingRoomMessageHistory.messages
    }

    fun addDrawingRoomMessageHistory(message: MessagePayload){
        drawingRoomMessageHistory.messages.add(message)
    }


    fun getDrawingClient(): SocketClient {
        return drawingRoomClient
    }

    fun getDrawingRoomConnected(): Boolean {
        return drawingRoomConnected
    }

    private fun getRoomConnectionUrl(roomId: Int): String {
        if (roomId == this.PUBLIC_ROOM_ID) {
            return "${BASE_URL}/ConnectToPublic"
        } else {
            return "${BASE_URL}/${roomId}/Connect"
        }
    }


    fun closeConnection(roomId: Int) {
        val room = activeRooms[roomId]
        if (room != null) {
            room.closeConnection()
            activeRooms.remove(roomId)
        }
    }

    fun onDestroy() {
        activeRooms.forEach { i, socketClient ->
            socketClient.closeConnection()
        }
        activeRooms = hashMapOf()
        if (appClientConnection != null) {
            appClientConnection!!.closeConnection()
        }
    }

    suspend fun joinRoom(roomId: Int): Result<RequestResponse, FuelError> {
        val url = "${BASE_URL}/${roomId}/Join"
        return handleResponse(post(RequestConfig(url=url), null).awaitObjectResult(RequestResponse.Deserializer()))
    }

    suspend fun createRoom(roomName: String): Result<Room, FuelError> {

        val url = "${BASE_URL}"
        val reqBody =  """{"name": "${roomName}"}"""
        return handleResponse(post(RequestConfig(url=url), null).jsonBody(reqBody).awaitObjectResult(Room.Deserializer()))
    }

    suspend fun leaveRoom(roomId: Int): Result<RequestResponse, FuelError> {
        val url = "${BASE_URL}/${roomId}/Leave"
        return handleResponse(post(RequestConfig(url=url), null).awaitObjectResult(RequestResponse.Deserializer()))
    }


    suspend fun fetchJoinedRooms (): Result<ArrayList<Room>, FuelError> {
        val url = "${BASE_URL}/JoinedRooms"
        return handleResponse(get(RequestConfig(url=url)).awaitObjectResult(Room.ListDeserializer()))
    }

    suspend fun fetchAllRooms(): Result<ArrayList<Room>, FuelError> {
        val url = "${BASE_URL}"
        return handleResponse(get(RequestConfig(url=url)).awaitObjectResult(Room.ListDeserializer()))
    }

    suspend fun fetchRoomsByName(roomName: String): Result<ArrayList<Room>, FuelError> {
        val url = "${BASE_URL}/By/${roomName}"
        return handleResponse(get(RequestConfig(url=url)).awaitObjectResult(Room.ListDeserializer()))
    }

    suspend fun fetchRoomMembers(id: Int): Result<ArrayList<User>, FuelError> {
        val url = "${BASE_URL}/${id}/Members"
        return handleResponse(get(RequestConfig(url=url)).awaitObjectResult(User.UserListDeserializer()))
    }

    suspend fun deleteRoom(roomId: Int): Result<RequestResponse, FuelError> {
        val url = "${BASE_URL}/$roomId"
        return handleResponse(delete(RequestConfig(url=url)).awaitObjectResult(RequestResponse.Deserializer()))
    }


    fun getAppClient(): Promise<SocketClient, Unit> {
        if (appClientPromise != null)
            return appClientPromise as Promise<SocketClient, Unit>;

        val promise = Promise<SocketClient, Unit> {
            val conn = SocketClient(appClient)
            val authToken = Authentication.getAuthenticationToken()
            conn.onClose = {
                reject(Unit)
            }
            conn.connect("$BASE_URL/ConnectToApp/${authToken?.token}", onConnect = {
                appClientConnection = conn
                conn.onClose = { }
                resolve(conn)
            })
        }

        appClientPromise = promise
        return appClientPromise as Promise<SocketClient, Unit>
    }

    fun setClientSyncs(payload: ClientStateSyncEventPayload) {
        clientSync = payload.clients
    }

    fun getClientSyncs(): ArrayList<ClientSync> {
        return clientSync
    }

    fun updateClientSyncs(payload: ClientStateEventPayload){
        if(payload.state == 2) {
            for ( client in clientSync){
                if (client.userId == payload.userId){
                    clientSync.remove(client)
                    break
                }
            }
        } else if(payload.state == 1)
            clientSync.add(ClientSync(payload.user, payload.userId, payload.avatar))
    }


}