package com.example.client_leger.classes.couches

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Rect
import android.os.SystemClock
import android.util.Log
import androidx.core.graphics.get
import com.example.client_leger.classes.CallbackManager
import com.example.client_leger.classes.Subscription
import com.example.client_leger.classes.Vec2
import com.example.client_leger.classes.drawing.BoundingBox
import com.example.client_leger.classes.drawing.DrawingEventWrapper
import com.example.client_leger.services.CanvasSizeService
import kotlin.math.max
import kotlin.math.min


class CoucheBranche(private var drawingEventWrapper: DrawingEventWrapper? = null) {
    val render: CallbackManager = CallbackManager()
    fun subscribe(f: () -> Unit): Subscription {
        return render.addSubscriber(f)
    }
    fun unsubscribe(s: Subscription): Boolean {
        if (render == s.callbackManager) {
            s.unsubscribe()
            return true
        }
        return false
    }
    private val childs: ArrayList<CoucheBranche> = ArrayList()
    private val branchSubscriptions: ArrayList<Subscription> = ArrayList()
    private val leafSubscriptions: ArrayList<Subscription> = ArrayList()
    var isLeaf: Boolean = false
        get() {
            return field
        }

    init {
        isLeaf = drawingEventWrapper != null
        if (drawingEventWrapper == null) {
            drawingEventWrapper = DrawingEventWrapper()
        }
    }

    fun addChild(child: CoucheBranche, render: Boolean = true, pos: Int = -1) {
        if (isLeaf)
            return

        if (pos == -1) {
            childs.add(child);
            branchSubscriptions.add(child.subscribe { makeRender() })
            leafSubscriptions.add(child.getDrawingEvent()!!.subscribe { makeRender() })
        } else {
            childs.add(pos, child)
            branchSubscriptions.add(pos, child.subscribe { makeRender() })
            leafSubscriptions.add(pos, child.getDrawingEvent()!!.subscribe { makeRender() })
        }

        if (render) {
            makeRender()
        }
    }

    fun removeChild(pos: Int) {
        if (isLeaf) {
            return
        }

        childs.removeAt(pos)
        branchSubscriptions.removeAt(pos).unsubscribe()
        leafSubscriptions.removeAt(pos).unsubscribe()
        makeRender()
    }

    fun selectChild(position: Vec2): DrawingEventWrapper? {
        if (isLeaf) {
            return drawingEventWrapper
        }

        for (i in childs.size - 1 downTo 0) {
            val child = childs[i]
            val localPosition = Vec2(position.x - child.getBoundingBox().x, position.y - child.getBoundingBox().y)
            if (localPosition.x >= child.getBoundingBox().w || localPosition.y >= child.getBoundingBox().h || localPosition.x < 0 || localPosition.y < 0) {
                continue
            }

            val opacity = child.getImage().get(localPosition.x.toInt(), localPosition.y.toInt())
            if (opacity != 0) {
                return child.selectChild(position)
            }
        }
        return null
    }

    fun makeRender(renderParents: Boolean = true) {
        if (isLeaf)
            return

        val maxBoundingBox: BoundingBox = getMaxBoundingBox()
        if (maxBoundingBox.w <= 0 || maxBoundingBox.h <= 0)
            return

        val image = Bitmap.createBitmap(maxBoundingBox.w.toInt(), maxBoundingBox.h.toInt(), Bitmap.Config.ARGB_8888)
        drawingEventWrapper!!.boundingBox = maxBoundingBox
        val canvas = Canvas(image)
        for (i in 0 until childs.size) {
            val child = childs[i]
            if(child.drawingEventWrapper!!.isSelected)
                continue

            val boundingBox = child.drawingEventWrapper!!.boundingBox
            if (boundingBox == null)
                continue

            val x = boundingBox.x - maxBoundingBox.x
            val y = boundingBox.y - maxBoundingBox.y
            // Draw image
            val bottomRight = Vec2(0 + boundingBox.w, 0 + boundingBox.h)
            val srcRect = Rect(0, 0, bottomRight.x.toInt(), bottomRight.y.toInt())
            val dstRect = Rect(x.toInt(), y.toInt(), (x + boundingBox.w).toInt(), (y + boundingBox.h).toInt())
            canvas.drawBitmap(child.getImage(), srcRect, dstRect, null)
        }

        drawingEventWrapper!!.setBaseImage(image)
        if (renderParents)
            render.callback()
    }

    fun getMaxBoundingBox(): BoundingBox {
        var xMin = CanvasSizeService.canvasSize.x
        var yMin = CanvasSizeService.canvasSize.y
        var xMax = 0f
        var yMax = 0f
        for (i in 0 until childs.size) {
            val child = this.childs[i]
            if (child.drawingEventWrapper!!.isSelected)
                continue

            val boundingBox = child.drawingEventWrapper!!.boundingBox
            if (boundingBox == null)
                continue

            xMin = min(boundingBox.x, xMin)
            yMin = min(boundingBox.y, yMin)
            xMax = max(boundingBox.x + boundingBox.w, xMax)
            yMax = max(boundingBox.y + boundingBox.h, yMax)
        }

        if (xMax - xMin <= 0 || yMax - yMin <= 0) {
            return BoundingBox(0.0f, 0.0f, xMin, yMin)
        }
        return BoundingBox(xMin, yMin, xMax - xMin, yMax - yMin)
    }

    fun getImage(): Bitmap {
        return this.drawingEventWrapper!!.image!!
    }

    fun getChild(pos: Int): CoucheBranche {
        return childs[pos]
    }

    fun childCount(): Int {
        if (this.isLeaf)
            return 0

        return this.childs.size
    }

    fun drawingEventCount(): Int {
        var count = 0
        for (child in childs) {
            if (child.isLeaf)
                count++
            else
                count += child.drawingEventCount()
        }

        return count
    }

    fun getBoundingBox(): BoundingBox {

        return this.drawingEventWrapper!!.boundingBox!!
    }

    fun getDrawingEvent(): DrawingEventWrapper? {
        return this.drawingEventWrapper
    }
}