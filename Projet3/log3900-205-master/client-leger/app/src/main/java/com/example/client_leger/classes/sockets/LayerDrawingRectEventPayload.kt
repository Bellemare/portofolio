package com.example.client_leger.classes.sockets

import com.example.client_leger.classes.drawing.DrawingRectEvent

class LayerDrawingRectEventPayload(
    override val event: DrawingRectEvent,
    id: Int,
    layer: Int,
    type: Int,
    userId: Int? = null,
    user: String? = null,
): LayerEventPayload(event, id, layer, type, userId, user) {}
class SocketLayerDrawingRectEventPayload(type: Int, payload: LayerDrawingRectEventPayload): SocketPayload(type, payload)
