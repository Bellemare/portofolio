package com.example.client_leger.classes.chat

data class RoomFetchHistoryRequest(val roomId: Int, val limit: Int, val offset: Int)
