package com.example.client_leger.classes.chat

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.client_leger.R
import com.example.client_leger.R.*
import com.example.client_leger.classes.Color
import com.example.client_leger.classes.Vec2
import com.example.client_leger.classes.sockets.MessagePayload
import com.example.client_leger.fragments.chat.ChatWindowFragment
import com.example.client_leger.services.UserService
import com.example.client_leger.viewModels.ChatViewModel
import com.example.client_leger.views.AvatarView
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.*

class ChatMessageListAdapter(var messageList: ArrayList<MessagePayload>, var replyListener: ReplyListener, var viewModel: ChatViewModel, var parentFragment: ChatWindowFragment): RecyclerView.Adapter<ChatMessageListAdapter.ViewHolder>()  {

    val replyColor = "#1F8f36f4"
    val highlightColor = "#FF292929"
    val normalColor = "#FF222222"
    val tagColor = "#1F1e65e9"
    val user = viewModel.currentUser()


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val avatarView: AvatarView
        val userText: TextView
        val messageText: TextView
        val dateText: TextView
        val messageTypeIcon: TextView
        val tagIcon: TextView
        var replyLayout: LinearLayout
        val replyTo: TextView
        val replyMessage: TextView
        var color = "#FF222222"



        init {
            avatarView = view.findViewById(id.avatarImage)
            dateText = view.findViewById(id.date)
            userText = view.findViewById(id.user)
            messageText = view.findViewById(id.message)
            messageTypeIcon = view.findViewById(id.messageTypeIcon)
            tagIcon = view.findViewById(id.tagIcon)
            replyLayout = view.findViewById(id.replyChat)
            replyTo = view.findViewById(id.replyTo)
            replyMessage = view.findViewById(id.replyMessage)

        }
    }

    fun setMessage(message: MessagePayload) {
        messageList.add(message)
        notifyDataSetChanged()
    }

    fun setMessages(messages: ArrayList<MessagePayload>) {
        this.messageList.addAll(messages)
        this.messageList.sort()
        notifyItemRangeInserted(0, messages.size)
    }

    fun clearMessages() {
        var size = messageList.size
        messageList.clear()
        notifyItemRangeRemoved(0, size)
    }

    override fun getItemCount() = messageList.size

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
                .inflate(layout.fragment_chat_message, viewGroup, false)

        return ViewHolder(view)
    }


    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val message = messageList[position]

        setAvatar(viewHolder.avatarView, message)
        setDate(viewHolder.dateText, message)
        setUser(viewHolder.userText, message)
        setMessage(viewHolder.messageText, message)
        setMessageType(viewHolder, message)
        setMessageReply(viewHolder, message)
        setTag(viewHolder, message)

        viewHolder.itemView.setOnLongClickListener{
            onReplyLongClick(message, viewHolder.itemView)
            it.setBackgroundColor(android.graphics.Color.parseColor(viewHolder.color))
            return@setOnLongClickListener true
        }

        viewHolder.itemView
        viewHolder.itemView.setOnTouchListener { view, motionEvent ->
            if (motionEvent.action == 0){
                view.setBackgroundColor(android.graphics.Color.parseColor(highlightColor))
            }
            if (motionEvent.action == 1 || motionEvent.action == 2 || motionEvent.action == 3){
                view.setBackgroundColor(android.graphics.Color.parseColor(viewHolder.color))
            }
            return@setOnTouchListener false
        }





    }

    private fun setTag(viewHolder: ChatMessageListAdapter.ViewHolder, message: MessagePayload) {
        val user = viewModel.currentUser()
        if (message.recipients?.contains(user.id) as Boolean){
            viewHolder.itemView.setBackgroundColor(android.graphics.Color.parseColor(tagColor))
            viewHolder.color = tagColor
            viewHolder.tagIcon.visibility = View.VISIBLE
        } else
            viewHolder.tagIcon.visibility = View.GONE
    }

    private fun setAvatar(avatarView: AvatarView, message: MessagePayload) {
        var avatar = viewModel.getUserAvatar(message.userId as Int)
        if (avatar.length > 5)
            avatarView.initImage(avatar, Vec2(120f, 120f))
        avatarView.setOnClickListener {
            if(message.userId != user.id) {
                val bundle = bundleOf(Pair("userId", message.userId))
                Navigation.findNavController(parentFragment.requireView()).navigate(R.id.contactFragment, bundle)
            }
            else {
                Navigation.findNavController(parentFragment.requireView()).navigate(R.id.userFragment)
            }
        }
    }

    private fun onReplyLongClick(message: MessagePayload, view: View){
        replyListener.onReplyLongClick(message, view)
    }



    private fun setDate(view: TextView, message: MessagePayload) {
        val date: Date = message.sentAt as Date
        val timeFormatter = SimpleDateFormat("hh:mm:ss a", Locale.CANADA_FRENCH)
        timeFormatter.timeZone = TimeZone.getTimeZone("America/New_York")
        val localDate: LocalDateTime = date.toInstant().atZone((timeFormatter.timeZone).toZoneId()).toLocalDateTime()
        val year: String = localDate.year.toString().padStart(2, '0')
        val month: String = localDate.monthValue.toString().padStart(2, '0')
        val day: String = localDate.dayOfMonth.toString().padStart(2, '0')
        val hours: String = localDate.hour.toString().padStart(2, '0')
        val minutes: String = localDate.minute.toString().padStart(2, '0')
        val seconds: String = localDate.second.toString().padStart(2, '0')

        view.text = "${year}-${month}-${day} ${hours}:${minutes}:${seconds}"
    }

    private fun setUser(view: TextView, message: MessagePayload) {
        view.text = message.user


        if (message.userId != null) {
            val color: Color = Color(255, 0, 0, 1)
            color.init()
            color.hueShift((message.userId * 37) % 360.0)
            val userColor = color.hsvToRGB()
            view.setTextColor(android.graphics.Color.rgb(userColor.r,userColor.g,userColor.b))
        }
    }

    private fun setMessage(view: TextView, message: MessagePayload) {
        view.text = message.message
    }

    private fun setMessageType(view: ViewHolder, message: MessagePayload) {
        when(message.message_type){
            MessageTypeConst.URGENT_INT -> {
                view.itemView.setBackgroundColor(android.graphics.Color.parseColor(MessageTypeConst.URGENT_COLOR))
                view.color = MessageTypeConst.URGENT_COLOR
                view.messageTypeIcon.text = MessageTypeConst.URGENT_ICON
            }
            MessageTypeConst.INFO_INT -> {
                view.itemView.setBackgroundColor(android.graphics.Color.parseColor(MessageTypeConst.INFO_COLOR))
                view.color = MessageTypeConst.INFO_COLOR
                view.messageTypeIcon.text = MessageTypeConst.INFO_ICON
            }
            MessageTypeConst.ANNOUNCEMENT_INT -> {
                view.itemView.setBackgroundColor(android.graphics.Color.parseColor(MessageTypeConst.ANNOUNCEMENT_COLOR))
                view.color = MessageTypeConst.ANNOUNCEMENT_COLOR
                view.messageTypeIcon.text = MessageTypeConst.ANNOUNCEMENT_ICON
            } else ->  {
            view.itemView.setBackgroundColor(android.graphics.Color.parseColor(MessageTypeConst.DEFAULT_COLOR))
            view.messageTypeIcon.text = ""
            }
        }
    }

    private fun setMessageReply(viewHolder: ViewHolder, message: MessagePayload) {
        if (message.reply_to == 0) {
            viewHolder.replyLayout.visibility = View.GONE
            return
        }


        val message_to_reply_to = viewModel.getMessageById(message.reply_to as Int)
        viewHolder.replyLayout.setBackgroundColor(android.graphics.Color.parseColor(replyColor))
        viewHolder.replyLayout.visibility = View.VISIBLE
        viewHolder.replyTo.text ="Réponse à " + message_to_reply_to?.user + " :"
        viewHolder.replyMessage.text = message_to_reply_to?.message
    }

}