package com.example.client_leger.classes

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.client_leger.R
import com.example.client_leger.views.AvatarView

class ContactListAdapter(var contactList: ArrayList<Contact>, listener: OnClickDeleteContact): RecyclerView.Adapter<ContactListAdapter.ViewHolder>() {
    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

        var contactAvatar: AvatarView = view.findViewById(R.id.avatarView)
        var contactName: TextView = view.findViewById(R.id.contactName)
        var removeContactBtn: TextView = view.findViewById(R.id.removeContactBtn)

    }

    var deleteListener: OnClickDeleteContact = listener

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.fragment_contact_list_item, viewGroup, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val contact = contactList[position]
        val contactName = if(contact.contact.length < 25) contact.contact else contact.contact.substring(IntRange(0,22)) + "..."
        holder.contactName.text = contactName


        holder.contactAvatar.initImage(contact.contactAvatar, Vec2(80f, 80f))
        val bundle = bundleOf(Pair("userId", contact.contactId))
        holder.contactAvatar.setOnClickListener { Navigation.findNavController(holder.itemView).navigate(R.id.contactFragment, bundle) }

        holder.removeContactBtn.setOnClickListener { deleteListener.onClickDeleteContact(contact) }
    }

    override fun getItemCount(): Int {
        return contactList.size
    }
}