package com.example.client_leger.fragments

import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.core.view.marginStart
import androidx.core.view.marginTop
import androidx.fragment.app.*
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.client_leger.R
import com.example.client_leger.classes.*
import com.example.client_leger.classes.apiEvents.*
import com.example.client_leger.classes.sockets.ClientStateType
import com.example.client_leger.services.CanvasSizeService
import com.example.client_leger.services.DrawingService
import com.example.client_leger.services.SoundPlayer
import com.example.client_leger.services.SynchronizerService
import com.example.client_leger.viewModels.ToolOptionViewModel
import com.example.client_leger.viewModels.UserViewModel
import com.example.client_leger.views.CanvasView
import com.shopify.promises.Promise
import kotlinx.coroutines.runBlocking

class DrawingRoomFragment : Fragment(), ViewDisabler {
    private val userViewModel: UserViewModel by activityViewModels()
    private val toolOptionViewModel: ToolOptionViewModel by activityViewModels()
    private lateinit var drawingCanvasView: CanvasView
    private lateinit var previewCanvasView: CanvasView
    private lateinit var leaveBtn: Button
    private lateinit var drawingNameText: TextView

    private lateinit var loadingView: TextView

    private lateinit var collaboratorListView: RecyclerView
    private lateinit var collaboratorListAdapter: CollaboratorListAdapter
    private lateinit var currentUser: User
    private var collaborators: ArrayList<User> = ArrayList()

    private var drawEventListenerId: AppEvents.IntWrapper = AppEvents.IntWrapper(0)
    private var previewEventListenerId: AppEvents.IntWrapper = AppEvents.IntWrapper(0)
    private var selectionEventListenerId: AppEvents.IntWrapper = AppEvents.IntWrapper(0)
    private var loadingStartListener: AppEvents.IntWrapper = AppEvents.IntWrapper(0)
    private var loadingEndListener: AppEvents.IntWrapper = AppEvents.IntWrapper(0)
    private var drawingRoomErrorListener: AppEvents.IntWrapper = AppEvents.IntWrapper(0)
    private var clientSyncListener: AppEvents.IntWrapper = AppEvents.IntWrapper(0)
    private var clientStateListener: AppEvents.IntWrapper = AppEvents.IntWrapper(0)

    private lateinit var selectionLayerEditFragment: SelectionLayerEditFragment
    private lateinit var selectionLayerFragmentContainer: FragmentContainerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_drawing_room, container, false)
        toolOptionViewModel.init()

        loadingView = view.findViewById(R.id.loading)
        loadingView.isClickable = true
        loadingView.isFocusable = true
        leaveBtn = view.findViewById(R.id.leaveBtn)
        leaveBtn.setOnClickListener {joinHome(view)}

        drawingNameText = view.findViewById(R.id.drawingName)
        drawingNameText.text = requireArguments().getString("drawingName")

        getCurrentUser()
        setUpCollaboratorsList(view)

        showSelectionBox()
        initCanvas(view)
        initSelectionLayerEditor(view)

        listenToDrawingEvents()
        return view
    }

    private fun getCurrentUser() {
        userViewModel.getCurrentUser().whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    currentUser = it.value
                }
                is Promise.Result.Error -> {
                    println(it.error.message)
                }
            }
        }
    }

    private fun setUpCollaboratorsList(view: View) {
        collaboratorListView = view.findViewById(R.id.collaboratorsList)
        collaboratorListAdapter = CollaboratorListAdapter(collaborators) { position -> onCollaboratorAvatarClick(position) }
        collaboratorListView.adapter = collaboratorListAdapter
        collaboratorListView.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
    }

    private fun onCollaboratorAvatarClick(position: Int) {
        val user: User = collaborators[position]
        if(user.id != currentUser.id) {
            val bundle = bundleOf(Pair("userId", user.id))
            Navigation.findNavController(requireView()).navigate(R.id.contactFragment, bundle)
        }
        else {
            Navigation.findNavController(requireView()).navigate(R.id.userFragment)
        }
    }

    private fun initSelectionLayerEditor(view: View) {
        selectionLayerFragmentContainer = view.findViewById(R.id.layerFragmentContainer)

        if(childFragmentManager.findFragmentByTag("selectionLayerAction") == null) {
            selectionLayerEditFragment = SelectionLayerEditFragment()
            childFragmentManager.commit {
                setReorderingAllowed(true)
                add(R.id.layerFragmentContainer,selectionLayerEditFragment, "selectionLayerAction")
            }
        }
        selectionLayerFragmentContainer.visibility = View.INVISIBLE
    }

    override fun onDestroyView() {
        super.onDestroyView()
        toolOptionViewModel.destroy()
        runBlocking {
            AppEvents.unregister(drawEventListenerId.value, AppEventType.DRAW_EVENT)
            AppEvents.unregister(previewEventListenerId.value, AppEventType.PREVIEW_DRAW_EVENT)
            AppEvents.unregister(selectionEventListenerId.value, AppEventType.ON_SELECT_EVENT)
            AppEvents.unregister(loadingStartListener.value, AppEventType.LOADING_START_EVENT)
            AppEvents.unregister(loadingEndListener.value, AppEventType.LOADING_END_EVENT)
            AppEvents.unregister(drawingRoomErrorListener.value, AppEventType.DRAWING_ERROR_EVENT)
            AppEvents.unregister(clientStateListener.value, AppEventType.CLIENT_STATE_EVENT)
            AppEvents.unregister(clientSyncListener.value, AppEventType.CLIENT_STATE_SYNC_EVENT)
        }

        DrawingService.onDestroy()
    }

    private fun initCanvas(view: View) {
        drawingCanvasView = view.findViewById(R.id.drawingCanvas)
        drawingCanvasView.initCanvas(Color.WHITE)
        previewCanvasView = view.findViewById(R.id.previewCanvas)
        previewCanvasView.initCanvas(Color.TRANSPARENT)
        CanvasSizeService.offset = Vec2(drawingCanvasView.marginStart.toFloat(), drawingCanvasView.marginTop.toFloat())
        DrawingService.init()
        DrawingService.setHandler(Looper.getMainLooper())
        SynchronizerService.init(requireArguments().getInt("drawingId"))
    }

    private fun listenToDrawingEvents() {
        runBlocking {
            AppEvents.listenTo(AppEventType.DRAW_EVENT, drawEventListenerId) {
                drawingCanvasView.setCanvasData(DrawingService.getDrawingData())
                drawingCanvasView.postInvalidate()
            }
            AppEvents.listenTo(AppEventType.PREVIEW_DRAW_EVENT, previewEventListenerId) {
                previewCanvasView.setCanvasData(DrawingService.getPreviewData())
                previewCanvasView.postInvalidate()
            }
            AppEvents.listenTo(AppEventType.ON_SELECT_EVENT, previewEventListenerId) {
                val onSelectEvent = it as OnSelectEvent
                if (onSelectEvent.layer != -1) {
                    selectionLayerEditFragment.newDrawEventSelected(onSelectEvent.selectedDrawingEvent, onSelectEvent.layer)
                    selectionLayerFragmentContainer.visibility = View.VISIBLE
                } else {
                    selectionLayerFragmentContainer.visibility = View.INVISIBLE
                }
            }
            AppEvents.listenTo(AppEventType.LOADING_START_EVENT, loadingStartListener) {
                loadingView.isVisible = true
            }
            AppEvents.listenTo(AppEventType.LOADING_END_EVENT, loadingEndListener) {
                loadingView.isVisible = false
            }
            AppEvents.listenTo(AppEventType.DRAWING_ERROR_EVENT, drawingRoomErrorListener) {
                Handler(Looper.getMainLooper()).post {
                    it as DrawingErrorEvent
                    val error = RequestResponse("Erreur", it.error)
                    val bundle = bundleOf(Pair("error", error))
                    Navigation.findNavController(requireView()).navigate(R.id.albumMenuFragment, args = bundle)
                }
            }
            AppEvents.listenTo(AppEventType.CLIENT_STATE_SYNC_EVENT, clientSyncListener) {
                it as ClientStateSyncEvent
                clientStateSync(it)
            }
            AppEvents.listenTo(AppEventType.CLIENT_STATE_EVENT, clientStateListener) {
                it as ClientStateEvent
                onNewClientState(it)
            }
        }
    }

    private fun clientStateSync(event: ClientStateSyncEvent) {
        event.payload.clients.forEach { user ->
            collaborators.add(User(
                user.userId,
                "",
                user.user,
                false,
                user.avatar,
                false
            ))
        }
        collaboratorListAdapter.notifyDataSetChanged()
    }

    private fun onNewClientState(event: ClientStateEvent) {
        when (event.payload.state) {
            ClientStateType.DISCONNECT.value -> {
                collaborators.removeIf { user: User -> user.id == event.payload.userId }
            }
            ClientStateType.CONNECT.value -> {
                collaborators.add(User(
                    event.payload.userId,
                    "",
                    event.payload.user,
                    false,
                    event.payload.avatar,
                    false
                ))
                SoundPlayer.playSound(SoundType.NewCollaborator, requireContext())
            }
        }
        collaboratorListAdapter.notifyDataSetChanged()
    }

    private fun joinHome(inflaterView: View) {
        Navigation.findNavController(inflaterView).navigate(R.id.albumMenuFragment)
    }

    private fun showSelectionBox() {
        parentFragmentManager.commit {
            setReorderingAllowed(true)
            add<SelectionFragment>(R.id.selectionContainer, "selection")
        }
    }
}