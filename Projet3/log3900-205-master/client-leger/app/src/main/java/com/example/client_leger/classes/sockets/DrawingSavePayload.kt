package com.example.client_leger.classes.sockets

open class DrawingSavePayload(
    var image: String,
) : Payload() {}
