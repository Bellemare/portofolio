package com.example.client_leger.classes

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.client_leger.R

class AlbumListAdapter(var albumList: ArrayList<Album>,private val onAlbumClick: (position: Int) -> Unit): RecyclerView.Adapter<AlbumListAdapter.ViewHolder>() {
    class ViewHolder(view: View, private val onAlbumClick: (position: Int) -> Unit): RecyclerView.ViewHolder(view), View.OnClickListener {

        var albumName: TextView = view.findViewById(R.id.albumName)
        var albumIcon: TextView = view.findViewById(R.id.albumIcon)

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View){
            onAlbumClick(adapterPosition)
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.fragment_album_list_item, viewGroup, false)
        return ViewHolder(view, onAlbumClick)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val album = albumList[position]
        val albumName = if(album.name.length < 25) album.name else album.name.substring(IntRange(0,22)) + "..."
        holder.albumName.text = albumName

        val blue: Int = ContextCompat.getColor(holder.itemView.context, R.color.albumFolderBlue)
        val green: Int = ContextCompat.getColor(holder.itemView.context, R.color.albumFolderPublic)
        holder.albumIcon.setTextColor(if(album.ownerId != 0) blue else green)
    }

    override fun getItemCount(): Int {
        return albumList.size
    }
}