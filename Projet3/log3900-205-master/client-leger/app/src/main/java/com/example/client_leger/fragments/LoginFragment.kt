package com.example.client_leger.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.example.client_leger.R
import com.example.client_leger.classes.AuthRequest
import com.example.client_leger.classes.Authentication
import com.example.client_leger.classes.RequestResponse
import com.example.client_leger.viewModels.LoginViewModel
import com.shopify.promises.Promise
import java.nio.charset.Charset


class LoginFragment : Fragment() {
    private lateinit var editEmail: EditText
    private lateinit var editPassword: EditText
    private lateinit var loginBtn: Button
    private lateinit var newUserBtn: Button
    private lateinit var errorsText: TextView
    private lateinit var inputs: List<TextView>
    private lateinit var viewModel: LoginViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_login, container, false)

        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)
        editEmail = view.findViewById(R.id.editTextEmail)
        editPassword = view.findViewById(R.id.editTextPassword)
        loginBtn = view.findViewById(R.id.loginBtn)
        newUserBtn = view.findViewById(R.id.newUserBtn)
        errorsText = view.findViewById(R.id.errors)
        inputs = listOf(editEmail, editPassword, loginBtn, newUserBtn)

        loginBtn.setOnClickListener { login(view) }
        newUserBtn.setOnClickListener { createNewAccount(view) }
        listenToError()

        return view
    }

    private fun listenToError() {
        viewModel.getErrorData().observe(viewLifecycleOwner) {
           setErrorText(it)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (Authentication.isAuthenticated())
            Navigation.findNavController(view).navigate(R.id.action_loginFragment_to_homeFragment)
    }

    private fun disableEnableInputs(enabled: Boolean) {
        inputs.forEach {
            it.isEnabled = enabled
        }
    }

    private fun login(inflaterView: View) {
        disableEnableInputs(false)
        val authRequest = AuthRequest(email = editEmail.text.toString(), password = editPassword.text.toString())
        viewModel.login(authRequest).whenComplete {
              when (it) {
                  is Promise.Result.Success -> {
                      disableEnableInputs(true)
                      Navigation.findNavController(inflaterView).navigate(R.id.action_loginFragment_to_homeFragment)
                  }
                  is Promise.Result.Error -> {
                      if (it.error.errorData != null) {
                          val error = RequestResponse.Deserializer().deserialize(it.error.errorData.toString(
                              Charset.defaultCharset()))
                          error.displayResponse(requireView())
                      }
                      disableEnableInputs(true)
                  }
              }
        }
    }

    private fun setErrorText(error: RequestResponse) {
        errorsText.text = error.body
        val inputs = listOf(editEmail, editPassword)
        inputs.forEach {
            it.setHintTextColor(ContextCompat.getColor(requireActivity(), R.color.errorColor))
            it.setTextColor(ContextCompat.getColor(requireActivity(), R.color.errorColor))
        }
    }

    private fun createNewAccount(view: View) {
        Navigation.findNavController(view).navigate(R.id.action_loginFragment_to_accountCreationFragment)
    }
}