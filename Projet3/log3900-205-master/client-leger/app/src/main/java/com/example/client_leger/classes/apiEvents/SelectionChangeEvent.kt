package com.example.client_leger.classes.apiEvents

import com.example.client_leger.classes.Vec2

class SelectionChangeEvent(val position: Vec2, val size: Vec2): AppEvent() {
    override val type = AppEventType.SELECTION_CHANGE_EVENT
}