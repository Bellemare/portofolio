package com.example.client_leger.classes.drawing

enum class ToolType(val value: Int, val tool: String) {
    UNDEFINED(0, "Undefined"),
    PENCIL(1, "Crayon"),
    ELLIPSE(2, "Ellipse"),
    RECTANGLE(3, "Rectangle"),
    SELECTION(4, "Sélection"),
}