package com.example.client_leger.classes.apiEvents

import com.github.kittinunf.fuel.core.FuelError

abstract class ApiEvent(): AppEvent() {
    abstract val err: FuelError
}