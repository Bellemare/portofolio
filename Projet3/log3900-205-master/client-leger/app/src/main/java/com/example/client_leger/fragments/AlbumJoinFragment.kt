package com.example.client_leger.fragments

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.*
import androidx.fragment.app.Fragment
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.add
import androidx.fragment.app.commit
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.client_leger.R
import com.example.client_leger.classes.Album
import com.example.client_leger.classes.AlbumListAdapter
import com.example.client_leger.classes.ViewDisabler
import com.example.client_leger.classes.sockets.PayloadTypes
import com.example.client_leger.services.RoomService
import com.example.client_leger.viewModels.AlbumMenuViewModel
import com.shopify.promises.Promise

class AlbumJoinFragment : Fragment(), ViewDisabler {

    private val viewModel: AlbumMenuViewModel by activityViewModels()

    private lateinit var mainView: LinearLayout
    private lateinit var exitBtn: TextView

    private lateinit var searchTerm: EditText
    private lateinit var searchButton: Button

    private lateinit var albumListView: RecyclerView
    private lateinit var albumAdapter: AlbumListAdapter
    private var displayedAlbums: ArrayList<Album> = ArrayList()
    private var albums: ArrayList<Album> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_album_join, container, false)
        mainView = view.findViewById(R.id.mainView)

        setUpExit(view)
        setUpSearch(view)
        setUpAlbumListView(view)
        listenToAlbumChange()
        fetchAlbums()

        return view
    }

    private fun listenToAlbumChange() {
        RoomService.getAppClient().whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    it.value.onMessage(PayloadTypes.ALBUM_CHANGE) { payload ->
                        Handler(Looper.getMainLooper()).post {
                            fetchAlbums()
                        }
                    }

                }
                is Promise.Result.Error -> {
                    println(it.error)
                }
            }
        }
    }

    private fun fetchAlbums() {
        albums.clear()
        displayedAlbums.clear()
        viewModel.fetchAlbumList().whenComplete { result ->
            when(result) {
                is Promise.Result.Success -> {
                    albums.addAll(result.value)
                    displayedAlbums.addAll(albums)
                    albumAdapter.notifyDataSetChanged()
                }
                is Promise.Result.Error -> {
                    println(result.error)
                }
            }
        }
    }

    private fun setUpAlbumListView(view: View) {
        albumListView = view.findViewById(R.id.AlbumList)
        albumAdapter = AlbumListAdapter(displayedAlbums) { position: Int -> onAlbumClick(position) }
        albumListView.adapter = albumAdapter
        albumListView.layoutManager = GridLayoutManager(context, 4, RecyclerView.HORIZONTAL, false)
    }

    private fun onAlbumClick(position: Int) {
        val bundle = bundleOf(Pair("album", displayedAlbums[position]))

        if(childFragmentManager.findFragmentByTag("albumJoinRequest") == null) {
            childFragmentManager.commit {
                setReorderingAllowed(true)
                add<AlbumJoinRequestFragment>(R.id.AlbumActionFragmentContainer, "albumJoinRequest", args = bundle)
            }
            enableDisableViewGroup(mainView, false)
            (parentFragment as AlbumMenuFragment).enableDisableAlbumMenuView(false)
        }
    }

    private fun setUpSearch(view: View) {
        searchTerm = view.findViewById(R.id.searchAlbum)
        searchTerm.setOnKeyListener { view, i, keyEvent ->
            if(keyEvent.action == KeyEvent.ACTION_DOWN) {
                when(i) {
                    KeyEvent.KEYCODE_ENTER -> filterJoinableAlbums(view)
                }
            }
                true
        }

        searchButton = view.findViewById(R.id.searchBtn)

        searchButton.setOnClickListener {
            filterJoinableAlbums(view)
        }
    }

    private fun filterJoinableAlbums(view: View) {
        val searchTerm = Regex(searchTerm.text.toString())
        displayedAlbums = albums.filter { it.name.contains(searchTerm) } as ArrayList<Album>
        albumAdapter.albumList = displayedAlbums
        albumAdapter.notifyDataSetChanged()

        (view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(view.windowToken, 0)
    }

    private fun setUpExit(view: View) {
        exitBtn = view.findViewById(R.id.exitBtn)
        exitBtn.setOnClickListener {
            (parentFragment as AlbumMenuFragment).removeAlbumActionFragment()
        }
    }

    fun removeAlbumActionFragment() {
        val albumActionFragment = childFragmentManager.findFragmentByTag("albumJoinRequest")
        if(albumActionFragment != null) {
            childFragmentManager.commit {
                setReorderingAllowed(true)
                remove(albumActionFragment)
            }
        }
        enableDisableViewGroup(mainView, true)
        (parentFragment as AlbumMenuFragment).enableDisableAlbumMenuView(true)

        fetchAlbums()
    }
}