package com.example.client_leger.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.client_leger.R
import com.example.client_leger.classes.*
import com.example.client_leger.viewModels.ContactViewModel
import com.shopify.promises.Promise
import java.lang.Exception
import java.nio.charset.Charset

class ContactListFragment : Fragment(), OnClickDeleteContact {
    private val viewModel: ContactViewModel by activityViewModels()

    private lateinit var addContactBtn: Button
    private lateinit var sideBarFragment: SideBarFragment
    private lateinit var contactListView: RecyclerView
    private lateinit var contactAdapter: ContactListAdapter
    private var contactArray: ArrayList<Contact> = ArrayList()
    private lateinit var currentContact: Contact

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_contact_list, container, false)

        sideBarFragment = parentFragment as SideBarFragment

        addContactBtn = view.findViewById(R.id.addContactBtn)
        addContactBtn.setOnClickListener { showUserList() }

        setUpContactListView(view)
        getContacts()

        return view
    }

    private fun showUserList() {
        sideBarFragment.inflateContactAdd()
    }

    private fun setUpContactListView(view: View) {
        contactListView = view.findViewById(R.id.contactList)
        contactAdapter = ContactListAdapter(contactArray, this)
        contactListView.adapter = contactAdapter
        contactListView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
    }

    private fun getContacts() {
        contactArray.clear()
        viewModel.getContacts().whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    contactArray.addAll(it.value)
                    contactAdapter.notifyDataSetChanged()
                }
                is Promise.Result.Error -> {
                    println(it.error.message)
                }
            }
        }
    }

    fun deleteContact() {
        viewModel.deleteContact(currentContact.contactId).whenComplete { result ->
            when (result) {
                is Promise.Result.Success -> {
                    removeContactFromList(currentContact)
                    contactAdapter.notifyDataSetChanged()
                    this.view?.let { result.value.displayResponse(it) }
                }
                is Promise.Result.Error -> {
                    //handle error
                    try {
                        val error = RequestResponse.Deserializer().deserialize(result.error.errorData.toString(
                            Charset.defaultCharset()))
                        this.view?.let { error.displayResponse(it) }
                    }
                    catch (e: Exception) {
                        val error = RequestFieldErrorResponse.Deserializer().deserialize(result.error.errorData.toString(
                            Charset.defaultCharset()))
                        this.view?.let { error.displayError(it) }
                    }
                }
            }
        }
    }

    private fun removeContactFromList(contact: Contact) {
        val index = contactArray.indexOfFirst{c -> c.id == contact.id}
        if(index != -1) {
            contactArray.removeAt(index)
        }
    }

    override fun onClickDeleteContact(contact: Contact) {
        currentContact = contact
        sideBarFragment.inflateConfirmPrompt(contact.contact)
    }
}