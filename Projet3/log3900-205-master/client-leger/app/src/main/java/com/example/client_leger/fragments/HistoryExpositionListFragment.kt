package com.example.client_leger.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.add
import androidx.fragment.app.commit
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.client_leger.R
import com.example.client_leger.classes.Album
import com.example.client_leger.classes.AlbumListAdapter
import com.example.client_leger.classes.CollaborationSession
import com.example.client_leger.classes.HistoryEditionListAdapter
import com.example.client_leger.viewModels.ContactViewModel
import com.example.client_leger.viewModels.UserViewModel
import com.shopify.promises.Promise

class HistoryExpositionListFragment : Fragment() {

    private lateinit var albumListView: RecyclerView
    private lateinit var albumAdapter: AlbumListAdapter
    private lateinit var currentParrentFragment: ContactFragment

    private var albums: ArrayList<Album> = ArrayList()

    private val contactViewModel: ContactViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_history_exposition_list, container, false)

        currentParrentFragment = parentFragment as ContactFragment

        albumListView = view.findViewById(R.id.expositionList)
        setUpExpositionListView(view)

        getContactExpositions(currentParrentFragment.userId)

        return view
    }

    private fun setUpExpositionListView(view: View) {
        albumListView = view.findViewById(R.id.expositionList)
        albumAdapter = AlbumListAdapter(albums) { position: Int -> onAlbumClick(position) }
        albumListView.adapter = albumAdapter
        albumListView.layoutManager = GridLayoutManager(context, 4, RecyclerView.VERTICAL, false)
    }

    private fun onAlbumClick(position: Int) {
        val bundle = bundleOf(Pair("album", albums[position]))

        if(childFragmentManager.findFragmentByTag("albumExpoAction") == null) {
            childFragmentManager.commit {
                setReorderingAllowed(true)
                add<AlbumExpositionInfoFragment>(R.id.drawingsContainer, "albumExpoAction", args = bundle)
            }
        }
    }

    private fun getContactExpositions(id: Int) {
        contactViewModel.getContactExpositions(id).whenComplete { result ->
            when(result) {
                is Promise.Result.Success -> {
                    albums.clear()
                    albums.addAll(result.value)
                    albumAdapter.notifyDataSetChanged()
                }
                is Promise.Result.Error -> {
                    println(result.error)
                }
            }
        }
    }

    fun removeAlbumExpositionActionFragment() {
        val albumActionFragment = childFragmentManager.findFragmentByTag("albumExpoAction")
        if(albumActionFragment != null) {
            childFragmentManager.commit {
                setReorderingAllowed(true)
                remove(albumActionFragment)
            }
        }

        getContactExpositions(currentParrentFragment.userId)
    }

}