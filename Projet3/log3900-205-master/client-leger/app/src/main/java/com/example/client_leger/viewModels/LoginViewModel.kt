package com.example.client_leger.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.client_leger.classes.AuthRequest
import com.example.client_leger.classes.AuthToken
import com.example.client_leger.classes.Authentication
import com.example.client_leger.classes.RequestResponse
import com.example.client_leger.services.AuthService
import com.example.client_leger.services.LocalApiService
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.result.Result
import com.shopify.promises.Promise
import kotlinx.coroutines.launch

class LoginViewModel: ViewModel() {
    private var errorData: MutableLiveData<RequestResponse> = MutableLiveData()

    fun getErrorData(): LiveData<RequestResponse> {
        return errorData
    }

    fun login(authRequest: AuthRequest): Promise<AuthToken, FuelError> {
        return Promise {
            viewModelScope.launch {
                val service = AuthService()
                val res = service.login(authRequest)
                when (res) {
                    is Result.Success -> {
                        val authToken = res.get()
                        LocalApiService.saveToSharedPreferences("session", authToken)
                        Authentication.setAuthToken(authToken)

                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        if (res.error.response.statusCode == 400) {
                            val err = RequestResponse.Deserializer().deserialize(String(res.error.errorData))
                            if (err != null)
                                errorData.postValue(err)
                        }
                        reject(res.error)
                    }
                }
            }
        }
    }
}