package com.example.client_leger.fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.activityViewModels
import com.example.client_leger.R
import com.example.client_leger.classes.DrawingModel
import com.example.client_leger.classes.RequestFieldErrorResponse
import com.example.client_leger.classes.RequestResponse
import com.example.client_leger.viewModels.AlbumMenuViewModel
import com.github.kittinunf.fuel.core.FuelError
import com.shopify.promises.Promise
import java.nio.charset.Charset

class DrawingPasswordEditFragment : Fragment() {

    private val viewModel: AlbumMenuViewModel by activityViewModels()

    private lateinit var confirmBtn: Button
    private var confirmBtnFirstClick: Boolean = true
    private lateinit var cancelBtn: Button

    private lateinit var passwordText: EditText

    private lateinit var drawing: DrawingModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_drawing_password_edit, container, false)
        drawing = requireArguments().get("drawing") as DrawingModel

        passwordText = view.findViewById(R.id.passwordText)
        confirmBtn = view.findViewById(R.id.createBtn)
        confirmBtn.setOnClickListener {

            if(confirmBtnFirstClick) {
                confirmBtnFirstClick = false
                val editedDrawingModel = DrawingModel(drawing.name, drawing.album_id, drawing.id, passwordText.text.toString())
                viewModel.updateDrawing(editedDrawingModel).whenComplete { result: Promise.Result<RequestResponse, FuelError> ->
                    when (result) {
                        is Promise.Result.Success -> {
                            val requestResponse: RequestResponse = result.value
                            requestResponse.displayResponse(view)

                            (view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(view.windowToken, 0)
                            removeFragment()
                        }
                        is Promise.Result.Error -> {
                            //handle error
                            val error = RequestFieldErrorResponse.Deserializer()
                                .deserialize(result.error.errorData.toString(Charset.defaultCharset()))
                            error.displayError(view)

                            (view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(view.windowToken, 0)
                        }
                    }
                }
            }
        }

        cancelBtn = view.findViewById(R.id.cancelBtn)
        cancelBtn.setOnClickListener {
            removeFragment()
            (view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(view.windowToken, 0)
        }

        return view
    }

    private fun removeFragment() {
        when(parentFragment){
            is AlbumInfoFragment -> (parentFragment as AlbumInfoFragment).removeActionFragment()
            is AlbumSearchDrawingFragment -> (parentFragment as AlbumSearchDrawingFragment).removeActionFragment()
        }
    }
}