package com.example.client_leger.classes.apiEvents

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

object AppEvents {
    private var listenerCount = 0
    private val listeners: HashMap<AppEventType, ArrayList<EventListener>> = hashMapOf()
    private val lock = Mutex()

    data class IntWrapper(var value: Int)

    suspend fun listenTo(type: AppEventType, listenerId: IntWrapper, handler: (AppEvent) -> Unit) {
        lock.withLock {
            if (!listeners.containsKey(type)) {
                listeners[type] = arrayListOf()
            }
            listeners[type]?.add(EventListener(listenerCount, handler))
            listenerId.value = listenerCount++
        }
    }

    suspend fun unregister(id: Int, type: AppEventType) {
        lock.withLock {
            if (listeners.containsKey(type)) {
                val listener = listeners[type]
                listener?.forEach {
                    if (it.id == id) {
                        listener.remove(it)
                    }
                }
            }
        }
    }

    fun emitEvent(event: AppEvent) {
        if (listeners.containsKey(event.type)) {
            val eventListener = listeners[event.type]
            if (eventListener != null) {
                eventListener.forEach {
                    it.handler(event)
                }
            }
        }
    }
}