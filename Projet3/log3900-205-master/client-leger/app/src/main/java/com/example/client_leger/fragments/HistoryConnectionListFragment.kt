package com.example.client_leger.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.client_leger.R
import com.example.client_leger.classes.ConnectionHistory
import com.example.client_leger.classes.HistoryConnectionListAdapter
import com.example.client_leger.viewModels.UserViewModel
import com.shopify.promises.Promise

class HistoryConnectionListFragment : Fragment() {

    private lateinit var connectionListView: RecyclerView
    private lateinit var connectionAdapter: HistoryConnectionListAdapter

    private var connectionArray: ArrayList<ConnectionHistory> = ArrayList()
    private val viewModel: UserViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_history_connection_list, container, false)

        connectionListView = view.findViewById(R.id.connectionList)

        setUpConnectionListView(view)
        getConnectionHistory()

        return view
    }

    private fun setUpConnectionListView(view: View) {
        connectionListView = view.findViewById(R.id.connectionList)
        connectionAdapter = HistoryConnectionListAdapter(connectionArray)
        connectionListView.adapter = connectionAdapter
        connectionListView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
    }

    private fun getConnectionHistory() {
        viewModel.getConnectionHistory().whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    connectionArray.addAll(it.value)
                    connectionAdapter.notifyDataSetChanged()
                }
                is Promise.Result.Error -> {
                    println(it.error.message)
                }
            }
        }
    }

}