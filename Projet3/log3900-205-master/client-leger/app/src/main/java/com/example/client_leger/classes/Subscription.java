package com.example.client_leger.classes;

public class Subscription {
    CallbackManager c;
    Runnable r;

    public Subscription(CallbackManager c, Runnable r) {
        this.c = c;
        this.r = r;
    }

    public void unsubscribe() {
        c.removeSubscriber(r);
    }

    public CallbackManager getCallbackManager() {
        return c;
    }
}
