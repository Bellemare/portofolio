package com.example.client_leger.services

import com.example.client_leger.classes.Vec2

object CanvasSizeService {
    val DEFAULT_WIDTH = 1200
    val DEFAULT_HEIGHT = 720

    var canvasSize: Vec2 = Vec2(DEFAULT_WIDTH.toFloat(), DEFAULT_HEIGHT.toFloat())
    var offset: Vec2 = Vec2(0.0f, 0.0f)

    fun isInCanvas(position: Vec2) : Boolean {
        return position.x >= 0 && position.y >= 0 && position.x < canvasSize.x && position.y < canvasSize.y
    }

    fun addOffset(pos: Vec2): Vec2 {
        val newPos = Vec2(pos.x + offset.x, pos.y + offset.y)
        return newPos
    }
}