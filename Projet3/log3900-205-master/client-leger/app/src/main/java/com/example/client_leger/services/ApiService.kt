package com.example.client_leger.services

import com.example.client_leger.classes.Environment
import com.example.client_leger.classes.RequestConfig
import com.example.client_leger.classes.apiEvents.*
import com.example.client_leger.getApiMapper
import com.github.kittinunf.fuel.core.*
import com.github.kittinunf.fuel.httpDelete
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.fuel.httpPut
import com.github.kittinunf.result.Result
import com.shopify.promises.Promise

abstract class ApiService {
    protected val ERROR_CODE_CONNECTION : Int = 0
    protected val ERROR_CODE_THRESHOLD : Int = 400

    protected val environment: Environment = Environment()
    protected val DEFAULT_HOST : String = environment.defaultHost
    protected val DEFAULT_PORT : Int = environment.defaultPort
    protected val DEFAULT_ENDPOINT : String = environment.defaultEndpoint

    protected fun get (config: RequestConfig) : Request {
        return constructUrl(config).httpGet()
    }

    protected fun <T> put (config: RequestConfig, data: T) : Request {
        return constructUrl(config).httpPut().body(getApiMapper().writeValueAsBytes(data)).addJsonHeader()
    }

    protected fun <T> post(config: RequestConfig, data: T): Request {
        return constructUrl(config).httpPost().body(getApiMapper().writeValueAsBytes(data)).addJsonHeader()
    }

    protected fun delete(config: RequestConfig): Request {
        return constructUrl(config).httpDelete().addJsonHeader()
    }

    private fun Request.addJsonHeader(): Request {
        return header("Content-Type" to "application/json", "Accept" to "application/json")
    }

    protected fun <T> handleResponse(res: Result<T, FuelError>): Result<T, FuelError> {
        res.runCatching {
            when (this) {
                is Result.Failure -> {
                    val err = this.error
                    println(err.response.statusCode)
                    if (err.response.statusCode == AppEventType.UNAUTHORIZED.code) {
                        AppEvents.emitEvent(UnauthorizedEvent(err))
                    }
                }
            }
        }
        return res
    }

    protected fun constructUrl(config: RequestConfig): String {

        val host = if (config.host != null) config.host else DEFAULT_HOST
        val port = if (config.port != null) config.port else DEFAULT_PORT
        val endpoint = if (config.endpoint != null) config.endpoint else this.DEFAULT_ENDPOINT

        return if (port != 80) "${host}:${port}${endpoint}${config.url}" else "${host}${endpoint}${config.url}"
    }
}