package com.example.client_leger.fragments.chat

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.add
import androidx.fragment.app.commit
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import com.example.client_leger.R
import com.example.client_leger.classes.SoundType
import com.example.client_leger.classes.chat.windowName
import com.example.client_leger.fragments.DrawingCreationFragment
import com.example.client_leger.fragments.SideBarFragment
import com.example.client_leger.fragments.ToolOptionFragment
import com.example.client_leger.services.SoundPlayer
import com.example.client_leger.viewModels.ChatViewModel

class ChatPopoutButtonFragment: Fragment() {
    private lateinit var chatButton: TextView
    private lateinit var unreadMessageTicker: TextView
    private var currentWindowName: CharSequence = ""
    private lateinit var viewModel: ChatViewModel

    private var hasPlayedSound: Boolean = false

    lateinit var mainHandler: Handler
    private val updateTickerTask = object : Runnable {
        override fun run() {
            showUnreadTicker()
            mainHandler.postDelayed(this, 10)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_chatpopoutbutton, container, false)

        viewModel = ViewModelProvider(requireActivity()).get(ChatViewModel::class.java)
        viewModel.fetchAllUsers()
        viewModel.setCurrentUser()


        chatButton = view.findViewById(R.id.chatButton)
        chatButton.setOnClickListener { inflateChatView(view) }

        unreadMessageTicker = view.findViewById(R.id.unreadMessages)

        val navHostFragment = getActivity()?.getSupportFragmentManager()?.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val navController = navHostFragment.navController
        currentWindowName = navController.currentBackStackEntry?.destination?.label as CharSequence


        mainHandler = Handler(Looper.getMainLooper())


        return view
    }

    override fun onResume() {
        super.onResume()
        mainHandler.post(updateTickerTask)
    }

    override fun onPause() {
        super.onPause()
        mainHandler.removeCallbacks(updateTickerTask)
    }

    private fun showUnreadTicker() {

        if(viewModel.getUnreadMessage()) {
            if (!hasPlayedSound) {
                hasPlayedSound = true
                SoundPlayer.playSound(SoundType.NewMessage, requireContext())
            }
            unreadMessageTicker.visibility = View.VISIBLE
        } else {
            hasPlayedSound = false
            unreadMessageTicker.visibility = View.GONE
        }
    }

    private fun inflateChatView(inflaterView: View) {
        (parentFragment as SideBarFragment).inflateChat()
    }
}