package com.example.client_leger.classes

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.client_leger.R
import com.example.client_leger.views.AvatarView

class CollaboratorListAdapter(var collaboratorList: ArrayList<User>,
                              private val onAvatarClick: ((position: Int) -> Unit)): RecyclerView.Adapter<CollaboratorListAdapter.ViewHolder>() {

    class ViewHolder(view: View, private val onAvatarClick: (position: Int) -> Unit) :
        RecyclerView.ViewHolder(view) {

        var userName: TextView = itemView.findViewById(R.id.userName)
        var imgView: AvatarView = itemView.findViewById(R.id.avatarImage)

        init {
            imgView.setOnClickListener { onAvatarClick(adapterPosition) }
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.fragment_user_list_item, viewGroup, false)
        return ViewHolder(view, onAvatarClick)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentUser: User = collaboratorList[position]

        holder.imgView.initImage(currentUser.avatar, Vec2(80f, 80f))
        holder.userName.text = currentUser.pseudonym

        val color: Color = Color(255, 0, 0, 1)
        color.init()
        color.hueShift((currentUser.id * 37) % 360.0)
        val userColor = color.hsvToRGB()
        holder.userName.setTextColor(android.graphics.Color.rgb(userColor.r,userColor.g,userColor.b))
    }

    override fun getItemCount(): Int {
        return collaboratorList.size
    }
}

