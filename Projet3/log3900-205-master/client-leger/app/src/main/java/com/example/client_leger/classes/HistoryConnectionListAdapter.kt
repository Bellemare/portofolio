package com.example.client_leger.classes

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.client_leger.R
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class HistoryConnectionListAdapter (var connectionList: ArrayList<ConnectionHistory>): RecyclerView.Adapter<HistoryConnectionListAdapter.ViewHolder>() {
    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

        var connectionDate: TextView = view.findViewById(R.id.connectionDate)
        var deconnectionDate: TextView = view.findViewById(R.id.deconnectionDate)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.fragment_history_connection_list_item, viewGroup, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val connection: ConnectionHistory = connectionList[position]

        holder.connectionDate.text = setDate(connection.createdAt)
        holder.deconnectionDate.text = setDate(connection.invalidatedAt)
    }

    private fun setDate(date: Date?): String {
        if(date == null)
            return ""

        val dayOfWeekFormatter = SimpleDateFormat("EEEE", Locale.CANADA_FRENCH)
        dayOfWeekFormatter.timeZone = TimeZone.getTimeZone("America/New_York")

        val dayOfYearFormatter = SimpleDateFormat("dd MMMM YYYY", Locale.CANADA_FRENCH)
        dayOfYearFormatter.timeZone = TimeZone.getTimeZone("America/New_York")

        val timeFormatter = SimpleDateFormat("hh:mm:ss a", Locale.CANADA_FRENCH)
        timeFormatter.timeZone = TimeZone.getTimeZone("America/New_York")

        val dayOfWeek: String = dayOfWeekFormatter.format(date)
        val dayOfYear: String = dayOfYearFormatter.format(date)
        val time: String = timeFormatter.format(date)

        return String.format("%s %s %s", dayOfWeek, dayOfYear, time)
    }

    override fun getItemCount(): Int {
        return connectionList.size
    }
}