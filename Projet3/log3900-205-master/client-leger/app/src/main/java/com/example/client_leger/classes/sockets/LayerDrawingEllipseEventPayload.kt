package com.example.client_leger.classes.sockets

import com.example.client_leger.classes.drawing.DrawingEllipseEvent

class LayerDrawingEllipseEventPayload(
    override val event: DrawingEllipseEvent,
    id: Int,
    layer: Int,
    type: Int,
    userId: Int? = null,
    user: String? = null,
): LayerEventPayload(event, id, layer, type, userId, user) {}
class SocketLayerDrawingEllipseEventPayload(type: Int, payload: LayerDrawingEllipseEventPayload): SocketPayload(type, payload)
