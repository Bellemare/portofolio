package com.example.client_leger.classes.chat

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.client_leger.R
import com.example.client_leger.classes.apiEvents.Room
import java.util.*


class RoomListAddAdapter(var roomList: ArrayList<Room>, var joinListener: JoinRoomListener): RecyclerView.Adapter<RoomListAddAdapter.RoomViewHolder>()  {

    class RoomViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        lateinit var room: Room
        var roomName: TextView = view.findViewById(R.id.roomNameOption)
        var joinBtn: Button = view.findViewById(R.id.joinRoomBtn)

    }

    fun setRoom(room: Room) {
        roomList.add(room)
        notifyItemInserted(itemCount)
    }

    fun setRooms (rooms: ArrayList<Room>) {

        roomList.addAll(rooms)
        notifyItemRangeInserted(0, rooms.size)
    }

    override fun getItemCount() = roomList.size

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): RoomViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.fragment_chat_room_item_for_add, viewGroup, false)
        return RoomViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: RoomViewHolder, position: Int) {
        val room = roomList[position]


        setName(viewHolder.roomName, room)
        setRoom(viewHolder, room)

        viewHolder.joinBtn.setOnClickListener { onJoinClick(viewHolder) }

    }


    private fun setName(view: TextView, room: Room) {
        view.text = room.name
    }
    private fun setRoom(view: RoomViewHolder, room: Room) {
        view.room= room;
    }

    private fun onJoinClick(viewHolder: RoomViewHolder) {
        joinListener.onJoinClick( viewHolder.room)
    }

    fun clear() {
        roomList.clear()
        notifyDataSetChanged()
    }


}