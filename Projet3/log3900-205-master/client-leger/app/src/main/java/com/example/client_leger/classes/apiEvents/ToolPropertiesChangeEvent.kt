package com.example.client_leger.classes.apiEvents

import com.example.client_leger.classes.toolProperties.ToolProperties

class ToolPropertiesChangeEvent(val properties: ToolProperties): AppEvent() {
    override val type = AppEventType.TOOL_PROPERTIES_CHANGE_EVENT
}