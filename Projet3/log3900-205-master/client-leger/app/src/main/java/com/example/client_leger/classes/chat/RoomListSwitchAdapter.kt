package com.example.client_leger.classes.chat

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.client_leger.R
import com.example.client_leger.classes.apiEvents.Room
import com.example.client_leger.services.RoomService
import java.util.*


class RoomListSwitchAdapter(var roomList: ArrayList<Room>, val userId: Int, var listener: SeeRoomListener, var onLeaveClick: (roomId: Int) -> Unit, var onDeleteClick: (roomId: Int) -> Unit): RecyclerView.Adapter<RoomListSwitchAdapter.RoomViewHolder>()  {


    class RoomViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        lateinit var room: Room
        var roomName: TextView = view.findViewById(R.id.roomNameOption)
        var seeBtn: Button = view.findViewById(R.id.seeRoomBtn)
        var leaveBtn: Button = view.findViewById(R.id.joinRoomBtn)
        var deleteButton: TextView = view.findViewById(R.id.deleteBtn)
    }

    fun setRoom(room: Room) {
        roomList.add(room)
        notifyItemInserted(itemCount)
    }

    fun setRooms (rooms: ArrayList<Room>) {
        roomList.clear()
        roomList.addAll(rooms)
        notifyItemRangeChanged(0, rooms.size)
    }

    override fun getItemCount() = roomList.size

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): RoomViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.fragment_chat_room_item_for_switch, viewGroup, false)
        return RoomViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: RoomViewHolder, position: Int) {
        val room = roomList[position]

        viewHolder.leaveBtn.visibility = View.VISIBLE;
        //Hide for general
        if(room.name == "Général" || room.name == "Session collaborative"){
            viewHolder.leaveBtn.visibility = View.GONE;
        }

        if(room.ownerId == userId) {
            viewHolder.deleteButton.visibility = View.VISIBLE
            viewHolder.deleteButton.setOnClickListener { onDeleteClick(viewHolder) }
        }
        else {
            viewHolder.deleteButton.visibility = View.INVISIBLE
        }

        setName(viewHolder.roomName, room)
        setRoom(viewHolder, room)

        viewHolder.leaveBtn.setOnClickListener { onLeaveClick(viewHolder) }
        viewHolder.seeBtn.setOnClickListener { onSeeClick(viewHolder) }
    }


    private fun setName(view: TextView, room: Room) {
        view.text = room.name
    }
    private fun setRoom(view: RoomViewHolder, room: Room) {
        view.room= room;
    }
    private fun onSeeClick(viewHolder: RoomViewHolder) {
        listener.onSeeClick(viewHolder.room)
    }

    private fun onLeaveClick(viewHolder: RoomViewHolder) {
        listener.onLeaveClick(viewHolder.room)
        onLeaveClick(viewHolder.room.id as Int)
    }

    private fun onDeleteClick(viewHolder: RoomViewHolder) {
        onDeleteClick(viewHolder.room.id as Int)
        roomList.removeAt(viewHolder.adapterPosition)
        notifyDataSetChanged()
    }



}