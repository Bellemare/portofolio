package com.example.client_leger.classes.apiEvents

import android.view.View
import androidx.navigation.Navigation
import com.example.client_leger.classes.Authentication
import com.github.kittinunf.fuel.core.FuelError

class UnauthorizedEvent(override val err: FuelError): ApiEvent() {
    override val type = AppEventType.UNAUTHORIZED

    companion object handler {
        fun handle(view: View, to: Int) {
            Authentication.unloadAuthToken()
            Navigation.findNavController(view).navigate(to)
        }
    }
}