package com.example.client_leger.fragments.chat

import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import com.example.client_leger.classes.ViewDisabler
import android.widget.*
import androidx.fragment.app.*
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.client_leger.R
import com.example.client_leger.classes.Authentication
import com.example.client_leger.classes.SoundType
import com.example.client_leger.classes.chat.ChatMessageListAdapter
import com.example.client_leger.classes.chat.ChatMessageScrollListener
import com.example.client_leger.classes.apiEvents.AppEventType
import com.example.client_leger.classes.apiEvents.AppEvents
import com.example.client_leger.classes.apiEvents.Room
import com.example.client_leger.classes.chat.*
import com.example.client_leger.classes.sockets.MessagePayload
import com.example.client_leger.services.SoundPlayer
import com.example.client_leger.viewModels.ChatViewModel
import com.shopify.promises.Promise
import kotlinx.coroutines.runBlocking
import kotlin.collections.ArrayList

class ChatWindowFragment : Fragment(R.layout.fragment_chatwindow), ReplyListener, ViewDisabler, memberSelectedListener {
    private var messageArray = ArrayList<MessagePayload>()
    lateinit var listview: RecyclerView
    private lateinit var sendBtn: Button
    private lateinit var urgentBtn: Button
    private lateinit var infoBtn: Button
    private lateinit var annoncementBtn: Button
    private lateinit var messageView: EditText
    private lateinit var roomName: TextView

    private lateinit var replyMessageLayout: LinearLayout
    private lateinit var replyMessageView: TextView
    private lateinit var cancelReplyBtn : TextView
    private var selectedMessage: View? = null

    private lateinit var switchRoomBtn : Button
    private lateinit var leaveChatBtn: Button
    private lateinit var adapter: ChatMessageListAdapter

    private lateinit var chatHeaderView: LinearLayout
    private lateinit var messagesWrapperView: LinearLayout
    private lateinit var messageWriterView: LinearLayout

    private lateinit var messageToContainer: FragmentContainerView
    private lateinit var messageToContainerFragment: ChatMessageToFragment

    private lateinit var viewModel: ChatViewModel

    private var autoScroll = true
    private var cursorPosition: Int = 0
    private var cursorWord= ""
    private lateinit var words: List<String>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listview = view.findViewById(R.id.messageContainer)
        sendBtn = view.findViewById(R.id.sendBtn)
        urgentBtn = view.findViewById(R.id.urgentBtn)
        infoBtn = view.findViewById(R.id.infoBtn)
        annoncementBtn = view.findViewById(R.id.annoncementBtn)

        messageView = view.findViewById(R.id.sendMessage)
        roomName = view.findViewById(R.id.roomName)
        replyMessageLayout = view.findViewById(R.id.replyMessageBox)
        replyMessageView = view.findViewById(R.id.replyMessageDest)
        cancelReplyBtn = view.findViewById(R.id.cancelReplyBtn)
        switchRoomBtn = view.findViewById(R.id.switchRoomBtn)
        chatHeaderView = view.findViewById(R.id.chatHeader)
        messagesWrapperView = view.findViewById(R.id.messagesWrapper)
        messageWriterView = view.findViewById(R.id.messageWriter)
        messageToContainer = view.findViewById(R.id.messageToContainer)
        messageToContainerFragment = (messageToContainer.getFragment() as ChatMessageToFragment)
        viewModel = ViewModelProvider(requireActivity()).get(ChatViewModel::class.java)

        adapter = ChatMessageListAdapter(messageArray, this, viewModel, this)
        listview.adapter = adapter
        listview.layoutManager = LinearLayoutManager(context)


        viewModel.setHandler {
            this.handleNewMessage(it)
        }
        leaveChatBtn = view.findViewById(R.id.leaveChatBtn)
        switchRoomBtn.setOnClickListener{ openSwitchRoomMenu(view) }
        leaveChatBtn.setOnClickListener { leaveChat(view) }

        viewModel.closeConnection()
        updateLastWindowName(view)
        viewModel.loadBaseRoom()
        connectToChat()
    }

    private fun handleNewMessage(message: MessagePayload) {
        activity?.runOnUiThread {
            adapter.setMessage(message)
            if (message.userId!! != (Authentication.getUser()!!.id))
                SoundPlayer.playSound(SoundType.NewMessage, requireContext());
            if (autoScroll)
                listview.scrollToPosition(adapter.itemCount - 1)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.setUnreadMessage(false)
    }

    // UI/set up functions

    fun updateChat(room: Room) {
        viewModel.closeConnection()
        viewModel.changeRoom(room)
        adapter.clearMessages()
        connectToChat()
    }

    fun connectToChat() {
        viewModel.connectToChat().whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    listenToInput()
                    fetchHistory()
                    setRoomName()
                    messageToContainerFragment.setMemberList()
                }
                is Promise.Result.Error -> {
                    println(it.error)
                }
            }
        }
    }

    private fun setRoomName(){
        roomName.setText(viewModel.getCurrentRoom().name)
    }

    private fun fetchHistory() {
        viewModel.fetchHistory().whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    val currSize = adapter.itemCount
                    if (it.value.size > 0) {
                        adapter.setMessages(it.value)
                        if (currSize == 0) {
                            listview.scrollToPosition(adapter.itemCount - 1)
                        }
                    }
                }
                is Promise.Result.Error -> {
                    println(it.error)
                }
            }
        }
    }

    //Chat function
    private fun listenToInput() {

        setMessageViewListener()
        messageView.setOnEditorActionListener { _, actionId, event ->
            if(actionId == EditorInfo.IME_ACTION_DONE) {
                sendMessage()
            }

            true
        }
        sendBtn.setOnClickListener {
            sendMessage()
        }
        urgentBtn.setOnClickListener {
            changeMessageType(MessageTypeConst.URGENT_INT)
        }
        infoBtn.setOnClickListener {
            changeMessageType(MessageTypeConst.INFO_INT)
        }
        annoncementBtn.setOnClickListener {
            changeMessageType(MessageTypeConst.ANNOUNCEMENT_INT)
        }

        cancelReplyBtn.setOnClickListener { resetReply() }
        listenToScroll()

    }

    private fun listenToScroll() {
        listview.addOnScrollListener(ChatMessageScrollListener {
            autoScroll = it
            if (listview.getChildAt(0) != null) {
                if (listview.getChildAt(0).top == 0) {
                    fetchHistory()
                }
            }
        })
    }

    private fun sendMessage() {
        val message = messageView.text.toString()
        viewModel.sendMessage(message)
        messageView.text.clear()
        listview.scrollToPosition(adapter.itemCount - 1)
        autoScroll = true

        messageView.setHintTextColor(Color.parseColor(MessageTypeConst.DEFAULT_COLOR_WRITER))
        messageView.setTextColor(Color.parseColor(MessageTypeConst.DEFAULT_COLOR_WRITER))
        viewModel.setMessageType(MessageTypeConst.DEFAULT_INT)

        resetReply()
    }

    private fun changeMessageType(messageType: Int) {
        if(viewModel.getMessageType() == messageType) {
            viewModel.setMessageType(MessageTypeConst.DEFAULT_INT)
            messageView.setHintTextColor(Color.parseColor(MessageTypeConst.DEFAULT_COLOR_WRITER))
            messageView.setTextColor(Color.parseColor(MessageTypeConst.DEFAULT_COLOR_WRITER))
        } else{
            when(messageType){
                MessageTypeConst.URGENT_INT -> {
                    viewModel.setMessageType(messageType)
                    messageView.setHintTextColor(Color.parseColor(MessageTypeConst.URGENT_COLOR_WRITER))
                    messageView.setTextColor(Color.parseColor(MessageTypeConst.URGENT_COLOR_WRITER))
                }
                MessageTypeConst.INFO_INT -> {
                    viewModel.setMessageType(messageType)
                    messageView.setHintTextColor(Color.parseColor(MessageTypeConst.INFO_COLOR_WRITER))
                    messageView.setTextColor(Color.parseColor(MessageTypeConst.INFO_COLOR_WRITER))
                }
                MessageTypeConst.ANNOUNCEMENT_INT -> {
                    viewModel.setMessageType(messageType)
                    messageView.setHintTextColor(Color.parseColor(MessageTypeConst.ANNOUNCEMENT_COLOR_WRITER))
                    messageView.setTextColor(Color.parseColor(MessageTypeConst.ANNOUNCEMENT_COLOR_WRITER))
                }
            }
        }
    }

    //navigation functions

    private fun leaveChat (inflaterView: View) {
        val fragment = parentFragmentManager.findFragmentByTag("ChatWindowFragment")
        parentFragmentManager.commit {
            setReorderingAllowed(true)
            remove(fragment as Fragment)
        }
    }

    private fun openSwitchRoomMenu (inflaterView: View) {
        childFragmentManager.commit {
            setReorderingAllowed(true)
            add<RoomSwitchFragment>(R.id.roomMenuContainerView, "RoomSwitchFragment")
        }
    }




    // Reply functions

    override fun onReplyLongClick(message: MessagePayload, view: View) {
        viewModel.setMessageToReplyTo(message)
        selectedMessage = view
        childFragmentManager.commit {
            setReorderingAllowed(true)
            add<ReplyToModal>(R.id.roomMenuContainerView, "ReplyToModalFragment")
        }
        enableDisableViewGroup(chatHeaderView,false)
        enableDisableViewGroup(messagesWrapperView,false)
        enableDisableViewGroup(messageWriterView,false)
    }



    fun replyToMessage(){
        replyMessageLayout.visibility = View.VISIBLE
        replyMessageView.text = "Réponse à " + viewModel.getMessageToReplyTo().user
        selectedMessage?.setBackgroundColor(Color.parseColor("#3FE91E63"))
    }

    fun enableView(){
        enableDisableViewGroup(chatHeaderView,true)
        enableDisableViewGroup(messagesWrapperView,true)
        enableDisableViewGroup(messageWriterView,true)
    }
    fun resetReply(){
        replyMessageLayout.visibility = View.GONE
        viewModel.setMessageToReplyTo(MessagePayload(message = ""))
        selectedMessage?.setBackgroundColor(Color.parseColor("#00000000"))
    }


    // send message to functions
    fun setMessageViewListener(){
        messageView.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {


                words = s.split(" ", " \n")
                var tags= ArrayList<String>()

                for (word in words){
                    if(word.isNotEmpty()){
                        if(word[0] == '@')
                            tags.add(word.substring(1))
                    }
                }
                viewModel.parseTags(tags)

                cursorWord= ""
                var lengthBefore = 0
                cursorPosition = messageView.selectionEnd

                for (word in words){
                    if(lengthBefore + word.length >= cursorPosition){
                        cursorWord = word
                        break
                    }
                    lengthBefore += word.length + 1
                }


                if(cursorWord.isNotEmpty() && cursorWord[0] == '@' && messageToContainer.visibility == View.GONE){
                    messageToContainer.visibility = View.VISIBLE
                    messageToContainerFragment.filterList(cursorWord.substring(1))
                }
                else if(cursorWord.isNotEmpty() && cursorWord[0] == '@' && messageToContainer.visibility == View.VISIBLE) {
                    messageToContainerFragment.filterList(cursorWord.substring(1))
                }
                else if((cursorWord.isEmpty() || cursorWord[0] != '@') && messageToContainer.visibility == View.VISIBLE){
                    messageToContainerFragment.setMemberList()
                    messageToContainer.visibility = View.GONE
                }

            }
        })
    }

    override fun onMemberClicked(name: String) {
        var newMessage = ""
        var newPosition = 0
        for (word in words){
            if ( word == cursorWord){
                newMessage += "@" + name + " "
                newPosition = newMessage.length
            }
            else
                newMessage += word + " "
        }
        messageView.setText(newMessage)
        messageView.setSelection(newPosition)
        messageToContainer.visibility = View.GONE
    }
    private fun updateLastWindowName(inflaterView: View) {
        val name = Navigation.findNavController(inflaterView).previousBackStackEntry?.destination?.label as CharSequence
        if(name == windowName.AlBUM.value || name == windowName.DRAWING_ROOM.value)
            viewModel.lastWindowName = name
    }
}