package com.example.client_leger.classes.sockets

import com.example.client_leger.classes.drawing.DrawingEvent

open class LayerEventPayload(
    open val event: DrawingEvent,
    val id: Int,
    val layer: Int,
    val type: Int,
    val userId: Int? = null,
    val user: String? = null,
): Payload() {}
class SocketLayerEventPayload(type: Int, payload: LayerEventPayload): SocketPayload(type, payload)
