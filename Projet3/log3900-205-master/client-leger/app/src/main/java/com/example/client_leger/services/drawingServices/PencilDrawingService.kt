package com.example.client_leger.services.drawingServices

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import com.example.client_leger.classes.Box
import com.example.client_leger.classes.Vec2
import com.example.client_leger.classes.drawing.BoundingBox
import com.example.client_leger.classes.drawing.DrawingElement
import com.example.client_leger.classes.drawing.DrawingEvent
import com.example.client_leger.classes.drawing.DrawingPos
import kotlin.math.ceil
import kotlin.math.floor
import kotlin.math.max
import kotlin.math.min

class PencilDrawingService : ToolDrawingService() {
    override fun drawEvent(canvas: Canvas, paint: Paint, event: DrawingEvent): DrawingEvent {
        prepareDraw(paint, event)
        val drawingElement: DrawingPos = event.data as DrawingPos
        paint.strokeWidth = drawingElement.getTransformedLineWidth(event.attributes)

        val radius = paint.strokeWidth / 2
        val baseBox = Box(1200.0f, 720.0f, 0.0f, 0.0f)

        val mid = Vec2(0.0f,0.0f)
        val path = Path()
        val points = drawingElement.data
        if (points.size > 0)
            path.moveTo(points[0].x, points[0].y)
        for (i in 0 .. (points.size - 2)) {
            val p1 = points[i]
            val p2 = points[i + 1]
            selectBoxPoint(baseBox, p1, p2, radius)
            mid.x = (p1.x + p2.x)/2
            mid.y = (p1.y + p2.y)/2
            path.quadTo(p1.x, p1.y, mid.x, mid.y)
        }
        canvas.drawPath(path, paint)

        if (drawingElement.baseBoundingBox == null) {
            drawingElement.baseBoundingBox = setBoundingBox(baseBox)
        }
        if (drawingElement.boundingBox == null) {
            drawingElement.boundingBox = setBoundingBox(baseBox)
        }
        return event
    }

    private fun selectBoxPoint(box: Box, p1: Vec2, p2: Vec2, radius: Float) {
        box.xMin = min(box.xMin, min(p1.x - radius - 5, p2.x - radius - 5))
        box.yMin = min(box.yMin, min(p1.y - radius - 5, p2.y - radius - 5))
        box.xMax = max(box.xMax, max(p1.x + radius + 5, p2.x + radius + 5))
        box.yMax = max(box.yMax, max(p1.y + radius + 5, p2.y + radius + 5))
    }

    private fun setBoundingBox(box: Box): BoundingBox {
        return BoundingBox(
            floor(box.xMin),
            floor(box.yMin),
            ceil(box.xMax - box.xMin),
            ceil(box.yMax - box.yMin),
        )
    }

    override fun isEmpty(element: DrawingElement): Boolean {
        val drawingElement = element as DrawingPos
        return drawingElement.data.size <= 1
    }
}