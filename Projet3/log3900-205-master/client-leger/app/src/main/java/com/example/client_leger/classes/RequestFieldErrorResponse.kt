package com.example.client_leger.classes

import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.example.client_leger.R
import com.example.client_leger.getApiMapper
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.android.material.snackbar.Snackbar

data class RequestFieldErrorResponse(val title: String, val body: List<String>, val fields: List<String>) {
    class Deserializer : ResponseDeserializable<RequestFieldErrorResponse> {
        override fun deserialize(content: String) =
            getApiMapper().readValue(content, RequestFieldErrorResponse::class.java)
    }

    fun displayError(view: View) {
        val snackbar = Snackbar.make(view, formatErrorString(), Snackbar.LENGTH_LONG)
            .setAction("Ok"){}.setAnchorView(view.findViewById(R.id.snackbarContainer))

        val textView: TextView = (snackbar.view.findViewById(com.google.android.material.R.id.snackbar_text) as TextView)
        textView.maxLines = 99
        textView.textSize = 15f

        val button: Button = (snackbar.view.findViewById(com.google.android.material.R.id.snackbar_action)) as Button
        button.setTextColor(ContextCompat.getColor(view.context, R.color.white))

        snackbar.setBackgroundTint(ContextCompat.getColor(view.context, R.color.errorColor))
        snackbar.view.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT

        snackbar.show()
    }

    fun formatErrorString(): String {
        var formattedErrorString: String = "Erreur \n";
        for(error in body) {
            formattedErrorString += "\n\t- $error"
        }

        return formattedErrorString
    }
}
