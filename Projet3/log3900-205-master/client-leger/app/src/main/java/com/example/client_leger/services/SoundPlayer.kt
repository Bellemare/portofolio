package com.example.client_leger.services

import android.content.Context
import android.media.MediaPlayer
import com.example.client_leger.R
import com.example.client_leger.classes.SoundType

object SoundPlayer {
    fun playSound(soundtype: SoundType, ctx: Context) {
        val player = MediaPlayer.create(ctx, soundtype.sound)
        player.start()
        player.setOnCompletionListener {
            player.release()
        }
    }
}