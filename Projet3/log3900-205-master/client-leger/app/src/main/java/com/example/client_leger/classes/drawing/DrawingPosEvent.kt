package com.example.client_leger.classes.drawing

import com.example.client_leger.classes.Color
import com.example.client_leger.classes.toolProperties.PencilProperties

class DrawingPosEvent(
    override var data: DrawingPos,
    override var attributes: PencilProperties,
    type: Int? = null,
    toolType: Int,
    drawingType: Int,
    strokeColor: Color,
    fillColor: Color,
    textColor: Color,
    id: Int?,
): DrawingEvent(data, attributes, type, toolType, drawingType, strokeColor, fillColor, textColor, id)