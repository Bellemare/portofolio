package com.example.client_leger.classes.chat

import com.example.client_leger.classes.apiEvents.Room

interface SeeRoomListener {
    fun onSeeClick(room: Room)
    fun onLeaveClick(room: Room)
}