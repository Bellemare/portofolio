package com.example.client_leger.viewModels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.client_leger.classes.*
import com.example.client_leger.services.ContactApiService
import com.example.client_leger.services.UserService
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.result.Result
import com.shopify.promises.Promise
import kotlinx.coroutines.launch

class ContactViewModel: ViewModel() {

    fun getContacts(): Promise<ArrayList<Contact>, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = ContactApiService.getContacts()
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun getUserList(): Promise<ArrayList<User>, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = ContactApiService.getUserList()
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun getCollaborativeSessions(id: Int): Promise<ArrayList<CollaborationSession>, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = ContactApiService.getCollaborativeSessions(id)
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun getContactExpositions(id: Int): Promise<ArrayList<Album>, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = ContactApiService.getContactExpositions(id)
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun addContact(id: Int): Promise<Contact, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = ContactApiService.addContact(id)
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun deleteContact(id: Int): Promise<RequestResponse, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = ContactApiService.deleteContact(id)
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }
}