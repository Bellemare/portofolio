package com.example.client_leger.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.fragment.app.*
import androidx.navigation.Navigation
import com.example.client_leger.R
import com.example.client_leger.classes.*
import com.example.client_leger.viewModels.ContactViewModel
import com.example.client_leger.viewModels.UserViewModel
import com.example.client_leger.views.AvatarView
import com.shopify.promises.Promise
import java.lang.Exception
import java.nio.charset.Charset

class ContactFragment : Fragment(), ViewDisabler {

    private lateinit var sidebarContainer: FragmentContainerView
    private lateinit var addRemoveContactBtn: Button
    private lateinit var currentContact: User
    private lateinit var homeBtn: Button
    private lateinit var avatarView: AvatarView
    private lateinit var emailText: TextView
    private lateinit var pseudonymText: TextView

    private val contactViewModel: ContactViewModel by activityViewModels()
    private val userViewModel: UserViewModel by activityViewModels()
    var userId: Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            userId = it.getInt("userId")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_contact, container, false)

        addRemoveContactBtn = view.findViewById(R.id.addRemoveContactBtn)
        avatarView = view.findViewById(R.id.avatar)
        emailText = view.findViewById(R.id.emailText)
        homeBtn = view.findViewById(R.id.homeBtn)
        pseudonymText = view.findViewById(R.id.pseudonymText)
        sidebarContainer = view.findViewById(R.id.sideBarContainer)

        homeBtn.setOnClickListener { navigateToAlbum(view) }
        addRemoveContactBtn.setOnClickListener { addRemoveContact(view) }

        loadContact()
        getStats()

        return view
    }



    private fun navigateToAlbum(view: View) {
        Navigation.findNavController(view).navigate(R.id.albumMenuFragment)
    }

    private fun loadContact() {
        if(userId != -1) {
            getContact(userId)
        }
    }

    private fun getContact(id: Int) {
        userViewModel.getUser(id).whenComplete {
            when(it) {
                is Promise.Result.Success -> {
                    currentContact = it.value
                    initAvatarImage(it.value.avatar)
                    pseudonymText.text = it.value.pseudonym
                    if(it.value.isPublicEmail) {
                        emailText.text = it.value.email
                    }
                    enableAddRemoveContactBtn()
                    setAddRemoveContactBtn(currentContact.isContact)
                }
                is Promise.Result.Error -> {
                    println(it.error.message)
                }
            }
        }
    }

    private fun getStats() {
        inflateExpositonHistory()
        inflateEditionHistory()
    }

    private fun inflateExpositonHistory() {
        childFragmentManager.commit {
            add<HistoryExpositionListFragment>(R.id.expositionContainer, "expositionHistory")
        }
    }

    private fun inflateEditionHistory() {
        childFragmentManager.commit {
            add<HistoryEditionListFragment>(R.id.editionContainer, "editionHistory")
        }
    }

    private fun initAvatarImage(avatar: String) {
        avatarView.initImage(avatar, Vec2(300f,300f))
        avatarView.postInvalidate()
    }

    private fun addRemoveContact(view: View) {
        if(currentContact.isContact) {
            inflateConfirmPrompt(currentContact.pseudonym)
        }
        else {
            addContact(view)
        }
    }

    private fun addContact(view: View) {
        contactViewModel.addContact(currentContact.id).whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    println(it.value)
                    setAddRemoveContactBtn(true)
                }
                is Promise.Result.Error -> {
                    println(it.error.message)
                }
            }
        }
    }

    private fun enableAddRemoveContactBtn() {
        addRemoveContactBtn.isEnabled = true
    }

    private fun setAddRemoveContactBtn(isContact: Boolean) {
        if(isContact) {
            addRemoveContactBtn.text = "Supprimer des contacts"
            addRemoveContactBtn.setBackgroundColor(resources.getColor(R.color.redIcon))
        }
        else {
            addRemoveContactBtn.text = "Ajouter aux contacts"
            addRemoveContactBtn.setBackgroundColor(resources.getColor(R.color.primaryColor))
        }

        currentContact.isContact = isContact
    }

    private fun callSidebarEnableDisableView(isEnabled: Boolean) {
        // Calls sidebar to enable / disable everything (sidebarFragment + contactfragment)
        sidebarContainer.getFragment<SideBarFragment>().enableDisableMainView(isEnabled)
    }

    fun inflateConfirmPrompt(contactName: String) {
        val bundle: Bundle = bundleOf(Pair("confirmMessage", contactName))
        childFragmentManager.commit {
            add<ContactDeleteFragment>(R.id.confirmPromptFragmentContainer, "confirmPrompt", bundle)
        }
        callSidebarEnableDisableView(false)
    }

    fun deleteContact() {
        removeConfirmPromptFragment()
        contactViewModel.deleteContact(currentContact.id).whenComplete { result ->
            when (result) {
                is Promise.Result.Success -> {
                    setAddRemoveContactBtn(false)
                    this.view?.let { it -> result.value.displayResponse(it) }
                }
                is Promise.Result.Error -> {
                    //handle error
                    try {
                        val error = RequestResponse.Deserializer().deserialize(result.error.errorData.toString(
                            Charset.defaultCharset()))
                        view?.let { it -> error.displayResponse(it) }
                    }
                    catch (e: Exception) {
                        val error = RequestFieldErrorResponse.Deserializer().deserialize(result.error.errorData.toString(
                            Charset.defaultCharset()))
                        view?.let { it -> error.displayError(it) }
                    }
                }
            }
        }
    }

    fun removeConfirmPromptFragment() {
        val confirmPromptView = childFragmentManager.findFragmentByTag("confirmPrompt")
        if(confirmPromptView != null) {
            childFragmentManager.commit {
                remove(confirmPromptView)
            }
            callSidebarEnableDisableView(true)
        }
    }

    fun enableDisableContactView(isEnabled: Boolean) {
        enableDisableViewGroup(requireView().findViewById(R.id.mainContactView), isEnabled)
    }

    fun inflatePasswordView(session: CollaborationSession) {
        if(childFragmentManager.findFragmentByTag("passwordFragmentContainer") == null) {
            val bundle = bundleOf(Pair("drawing", DrawingModel(session.drawing, session.albumId, session.drawingId, null)))
            childFragmentManager.commit {
                setReorderingAllowed(true)
                add<DrawingPasswordVerifyFragment>(R.id.passwordFragmentContainer, "passwordFragmentContainer", args = bundle)
            }
            callSidebarEnableDisableView(false)
        }
    }

    fun removePasswordFragment() {
        val albumPasswordFragment = childFragmentManager.findFragmentByTag("passwordFragmentContainer")
        if(albumPasswordFragment != null) {
            childFragmentManager.commit {
                setReorderingAllowed(true)
                remove(albumPasswordFragment)
            }
        }
        callSidebarEnableDisableView(true)
    }
}