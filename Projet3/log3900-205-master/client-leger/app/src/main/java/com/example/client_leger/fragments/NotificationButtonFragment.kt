package com.example.client_leger.fragments
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.add
import androidx.fragment.app.commit
import com.example.client_leger.R
import com.example.client_leger.classes.SoundType
import com.example.client_leger.services.SoundPlayer
import com.example.client_leger.viewModels.AlbumMenuViewModel
import com.shopify.promises.Promise

class NotificationButtonFragment : Fragment() {

    private lateinit var notificationButton: TextView
    private lateinit var unreadNotificationTicker: TextView
    private lateinit var sideBarFragment: SideBarFragment

    private val albumViewModel: AlbumMenuViewModel by activityViewModels()

    var isEnabled: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_notification_button, container, false)

        notificationButton = view.findViewById(R.id.notificationButton)
        notificationButton.setOnClickListener { showHideNotificationView() }

        unreadNotificationTicker = view.findViewById(R.id.unreadNotifications)
        unreadNotificationTicker.visibility = View.INVISIBLE

        sideBarFragment = parentFragment as SideBarFragment

        return view
    }

    private fun showHideNotificationView() {
        sideBarFragment.removeActionFragment()

        if(!isEnabled) {
            parentFragmentManager.commit {
                add<NotificationFragment>(R.id.sideBarActionContainer, "sideBarAction")
            }
            isEnabled = true
            unreadNotificationTicker.visibility = View.INVISIBLE
        }
        else {
            isEnabled = false
        }
    }

    fun newNotificationReceived() {
        albumViewModel.getAlbumJoinRequests().whenComplete { result ->
            when(result) {
                is Promise.Result.Success -> {
                    if(result.value.size != 0) {
                        unreadNotificationTicker.visibility = View.VISIBLE
                        SoundPlayer.playSound(SoundType.NewJoinRequest, requireContext())
                    }
                }
                is Promise.Result.Error -> {
                    println(result.error)
                }
            }
        }
    }





}