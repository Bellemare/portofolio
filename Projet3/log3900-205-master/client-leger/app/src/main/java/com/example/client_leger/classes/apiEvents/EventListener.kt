package com.example.client_leger.classes.apiEvents

data class EventListener(val id: Int, val handler: (AppEvent) -> Unit)
