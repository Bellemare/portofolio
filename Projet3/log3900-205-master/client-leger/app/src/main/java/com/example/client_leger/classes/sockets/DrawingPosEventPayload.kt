package com.example.client_leger.classes.sockets

import com.example.client_leger.classes.drawing.DrawingPosEvent

class DrawingPosEventPayload(
    override val event: DrawingPosEvent,
    userId: Int,
    user: String,
) : DrawingEventPayload(event, userId, user) {}
class SocketDrawingPosEventPayload(type: Int, payload: DrawingPosEventPayload): SocketPayload(type, payload)
