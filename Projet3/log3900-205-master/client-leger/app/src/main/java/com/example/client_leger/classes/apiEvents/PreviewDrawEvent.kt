package com.example.client_leger.classes.apiEvents

import android.graphics.Bitmap

class PreviewDrawEvent(val data: Bitmap): AppEvent() {
    override val type = AppEventType.PREVIEW_DRAW_EVENT
}