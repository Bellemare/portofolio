package com.example.client_leger.classes.sockets

import com.example.client_leger.classes.drawing.DrawingEvent

open class DrawingEventPayload(
    open val event: DrawingEvent,
    var userId: Int,
    var user: String,
) : Payload() {}
