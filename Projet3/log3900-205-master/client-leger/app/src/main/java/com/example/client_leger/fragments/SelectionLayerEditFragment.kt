package com.example.client_leger.fragments

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.*
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.client_leger.R
import com.example.client_leger.classes.apiEvents.AppEventType
import com.example.client_leger.classes.apiEvents.AppEvents
import com.example.client_leger.classes.apiEvents.DrawEvent
import com.example.client_leger.classes.apiEvents.SelectionDeleteEvent
import com.example.client_leger.classes.drawing.DrawingEventWrapper
import com.example.client_leger.services.SynchronizerService
import com.example.client_leger.services.couches.CouchesService
import kotlin.math.min

class SelectionLayerEditFragment : Fragment() {

    public val IMAGE_WIDTH: Int = 400
    public val IMAGE_HEIGHT: Int = 230

    private lateinit var minLayerButton: TextView
    private lateinit var minusLayerButton: TextView
    private lateinit var plusLayerButton: TextView
    private lateinit var maxLayerButton: TextView

    private lateinit var layerIndexText: TextView

    private lateinit var drawEventImageView: ImageView
    private var canvas: Canvas = Canvas()
    private lateinit var imgBitmap: Bitmap

    private lateinit var deleteBtn: TextView

    private var currentLayer: Int = -1
    private var selectedDrawingEvent: DrawingEventWrapper? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =inflater.inflate(R.layout.fragment_selection_layer_edit, container, false)

        setUpLayerButtons(view)
        layerIndexText = view.findViewById(R.id.layerIndexText)
        drawEventImageView = view.findViewById(R.id.drawingEventContainer)

        return view
    }

    private fun currentLayerIsValid(): Boolean {
        return currentLayer != -1
    }

    private fun getLayer(): Int {
        if (selectedDrawingEvent == null) {
            return -1
        }

        return CouchesService.getDrawingEventIndex(selectedDrawingEvent!!)
    }

    private fun setUpLayerButtons(view: View) {
        minLayerButton = view.findViewById(R.id.minLayer)
        minusLayerButton = view.findViewById(R.id.minusLayer)
        plusLayerButton = view.findViewById(R.id.plusLayer)
        maxLayerButton = view.findViewById(R.id.maxLayer)
        deleteBtn = view.findViewById(R.id.deleteBtn)

        //change tree on button clicks
        minLayerButton.setOnClickListener {
            val currentLayer = getLayer()
            if(currentLayerIsValid()) {
                SynchronizerService.moveLayer(0, currentLayer)
                layerIndexText.text = "0"
            }
        }

        minusLayerButton.setOnClickListener {
            val currentLayer = getLayer()
            if(currentLayerIsValid() && currentLayer > 0) {
                SynchronizerService.moveLayer(currentLayer - 1, currentLayer)
                layerIndexText.text = (currentLayer - 1).toString()
            }
        }

        plusLayerButton.setOnClickListener {
            val currentLayer = getLayer()
            if(currentLayerIsValid() && currentLayer < CouchesService.nElement - 1) {
                SynchronizerService.moveLayer(currentLayer + 1, currentLayer)
                layerIndexText.text = (currentLayer + 1).toString()
            }
        }

        maxLayerButton.setOnClickListener {
            val currentLayer = getLayer()
            if(currentLayerIsValid()) {
                SynchronizerService.moveLayer(CouchesService.nElement - 1, currentLayer)
                layerIndexText.text = (CouchesService.nElement - 1).toString()
            }
        }

        deleteBtn.setOnClickListener {
            AppEvents.emitEvent(SelectionDeleteEvent())
            currentLayer = -1
        }
    }

    fun newDrawEventSelected(drawEvent: DrawingEventWrapper?, layer: Int) {
        selectedDrawingEvent = drawEvent
        val drawEventBitmap = drawEvent?.image
        if(drawEvent == null || drawEventBitmap == null) {
            return
        }

        layerIndexText.text = layer.toString()
        currentLayer = layer

        val scalingMatrix: Matrix = Matrix()
        val ratioWidth = IMAGE_WIDTH.toFloat() / drawEventBitmap.width
        val ratioHeigth = IMAGE_HEIGHT.toFloat() / drawEventBitmap.height
        val ratio = min(ratioWidth, ratioHeigth)
        scalingMatrix.postScale(ratio, ratio)

        if(ratio == ratioWidth){
            val scaledHeight = drawEventBitmap.height * ratio
            scalingMatrix.postTranslate(0f, (IMAGE_HEIGHT - scaledHeight) / 2f)
        }
        else {
            val scaledWidth = drawEventBitmap.width * ratio
            scalingMatrix.postTranslate((IMAGE_WIDTH - scaledWidth) / 2f, 0f)
        }

        imgBitmap = Bitmap.createBitmap(IMAGE_WIDTH,IMAGE_HEIGHT, Bitmap.Config.ARGB_8888)
        canvas.setBitmap(imgBitmap)

        val basePaint: Paint = Paint()
        basePaint.color = Color.WHITE
        canvas.drawPaint(basePaint)
        canvas.drawBitmap(drawEventBitmap, scalingMatrix, null)

        drawEventImageView.setImageBitmap(imgBitmap)
    }

}