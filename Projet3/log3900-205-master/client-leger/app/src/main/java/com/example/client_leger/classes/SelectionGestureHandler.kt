package com.example.client_leger.classes

import android.view.GestureDetector
import android.view.MotionEvent

class SelectionGestureHandler(val onDoubleTapEvent: (() -> Unit)? = null) : GestureDetector.SimpleOnGestureListener() {
    override fun onDoubleTap(e: MotionEvent): Boolean {
        val handler = onDoubleTapEvent
        if (handler != null) {
            handler()
        }

        return true
    }

    override fun onDown(event: MotionEvent): Boolean {
        return true
    }
}
