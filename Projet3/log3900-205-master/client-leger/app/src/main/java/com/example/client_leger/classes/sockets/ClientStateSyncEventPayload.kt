package com.example.client_leger.classes.sockets

class ClientStateSyncEventPayload(val clients: ArrayList<ClientSync>): Payload()
class SocketClientStateSyncEventPayload(type: Int, payload: ClientStateSyncEventPayload): SocketPayload(type, payload)
