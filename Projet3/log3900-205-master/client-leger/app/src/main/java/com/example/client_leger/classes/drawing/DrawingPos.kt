package com.example.client_leger.classes.drawing

import com.example.client_leger.classes.Vec2
import com.example.client_leger.classes.sockets.DrawingEventPayload
import com.example.client_leger.classes.toolProperties.ToolProperties
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import kotlin.math.abs
import kotlin.math.floor
import kotlin.math.max
import kotlin.math.min

@JsonIgnoreProperties(ignoreUnknown = true)
data class DrawingPos(var data: ArrayList<Vec2>): DrawingElement() {
    override fun handleNewEvent(evt: DrawingEventPayload): Boolean {
        val drawingPos = evt.event.data as DrawingPos
        drawingPos.data.forEach {
            data.add(it)
        }

        return true
    }

    override fun createNewEvent(previous: DrawingElement?) {
        if (previous == null)
            return
        val drawingPos = previous as DrawingPos
        if (drawingPos.data.size > data.size) {
            data = ArrayList(data)
        } else {
            data = ArrayList(data.subList(drawingPos.data.size - 1, data.size - 1))
        }
    }

    override fun deepCopy(): DrawingElement {
        val copy = DrawingPos(ArrayList(data))
        copyBoundingBox(copy)
        return copy
    }

    fun getTransformedLineWidth(attributes: ToolProperties): Float {
        val transform = transformation

        if (transform != null) {
            val adw = abs(transform.dw)
            val adh = abs(transform.dh)
            return max(attributes.lineWidth * min(adw, adh), 1.0f)
        }
        return attributes.lineWidth.toFloat()
    }

    override fun getTopLeftPosition(attributes: ToolProperties): Vec2 {
        var xMin = Float.MAX_VALUE
        var yMin = Float.MAX_VALUE
        val lw = getTransformedLineWidth(attributes)
        val lineRadius = lw / 2
        data.forEach {
            xMin = min(it.x - lineRadius - 5, xMin)
            yMin = min(it.y - lineRadius - 5, yMin)
        }
        return Vec2(floor(xMin), floor(yMin))
    }

    override fun applyScaling(dw: Float, dh: Float, flipX: Boolean, flipY: Boolean, attributes: ToolProperties): Boolean {
        if (dw == 0.0f || dh == 0.0f) {
            return false
        }

        super.applyScaling(dw, dh, flipX, flipY, attributes)
        val adw = abs(dw)
        val adh = abs(dh)
        val boundingBox = boundingBox
        if (boundingBox != null) {
            val lw = getTransformedLineWidth(attributes)
            val transformX = boundingBox.w * adw >= lw && boundingBox.w * adw > 5
            val transformY = boundingBox.h * adh >= lw && boundingBox.h * adh > 5
            val centerX = boundingBox.x + boundingBox.w / 2
            val centerY = boundingBox.y + boundingBox.h / 2
            data.forEach {
                var x = if (transformX) (it.x - centerX) * adw else it.x
                var y = if (transformY) (it.y - centerY) * adh else it.y
                if (flipX) {
                    x = -x
                }
                if (flipY) {
                    y = -y
                }
                val offsetX = if (transformX) centerX else 0.0f
                val offsetY = if (transformY) centerY else 0.0f
                it.x = x + offsetX
                it.y = y + offsetY
            }
        }
        return true
    }

    override fun applyTranslation(dx: Float, dy: Float) {
        super.applyTranslation(dx, dy)
        data.forEach {
            it.x += dx
            it.y += dy
        }
    }
}