package com.example.client_leger.services.tools

import android.graphics.Paint
import android.view.MotionEvent
import com.example.client_leger.classes.Vec2
import com.example.client_leger.classes.drawing.DrawingPos
import com.example.client_leger.classes.drawing.DrawingType
import com.example.client_leger.classes.drawing.ToolType
import com.example.client_leger.classes.toolProperties.PencilProperties
import com.example.client_leger.classes.toolProperties.ToolProperties
import com.example.client_leger.services.drawingServices.PencilDrawingService

class PencilService: ToolService() {
    override val toolDrawingService: PencilDrawingService = PencilDrawingService()
    override var defaultConfiguration: PencilProperties = PencilProperties(lineWidth = 4, drawingType = DrawingType.Stroke)
    override val toolType: ToolType
        get() = ToolType.PENCIL

    override var previewDrawingElement: DrawingPos = DrawingPos(arrayListOf())
    override var drawingElement: DrawingPos = DrawingPos(arrayListOf())
    override val currentAttributes: PencilProperties = PencilProperties(defaultConfiguration.drawingType, defaultConfiguration.lineWidth)

    override fun onActionDown(event: MotionEvent) {
        actionDown = true
        previewDrawingElement.data.add(Vec2(event.x, event.y))
        previewChanged()
    }

    override fun onActionMove(event: MotionEvent) {
        if (actionDown) {
            previewDrawingElement.data.add(Vec2(event.x, event.y))
            previewChanged()
        }
    }

    override fun onActionUp(event: MotionEvent) {
        actionDown = false
        drawingElement = previewDrawingElement.copy()
        drawingElement.data.add(Vec2(event.x, event.y))
        dataChanged()
        clearData()
    }

    override fun setPaintProperties(paint: Paint, properties: ToolProperties?) {
        var toolProperties: PencilProperties
        if (properties != null) {
            toolProperties = properties as PencilProperties
        } else {
            toolProperties = currentAttributes
        }
        paint.style = Paint.Style.STROKE
        paint.isAntiAlias = false
        paint.strokeWidth = toolProperties.lineWidth.toFloat()
        paint.strokeCap = Paint.Cap.ROUND
        paint.strokeJoin = Paint.Join.ROUND
        paint.strokeMiter = 3f
    }

    override fun clearData() {
        previewDrawingElement = DrawingPos(arrayListOf())
        drawingElement = DrawingPos(arrayListOf())
        previewChanged()
        dataChanged()
    }
}