package com.example.client_leger.fragments

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.core.os.bundleOf
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.add
import androidx.fragment.app.commit
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.client_leger.R
import com.example.client_leger.classes.*
import com.example.client_leger.classes.sockets.PayloadTypes
import com.example.client_leger.services.RoomService
import com.example.client_leger.viewModels.AlbumMenuViewModel
import com.github.kittinunf.fuel.core.FuelError
import com.shopify.promises.Promise
import java.nio.charset.Charset
import kotlin.collections.ArrayList

class AlbumSearchDrawingFragment : Fragment(), ViewDisabler {

    private val viewModel: AlbumMenuViewModel by activityViewModels()
    private lateinit var mainView: LinearLayout

    private lateinit var checkBox: CheckBox
    private lateinit var searchField: EditText
    private lateinit var albumDropDownContainer: FrameLayout
    private var albumDropdown: AutoCompleteTextView? = null
    private var currentAlbumPosition: Int = 0

    private lateinit var closeBtn: Button
    private lateinit var searchButton: Button

    private lateinit var albums: ArrayList<Album>

    private lateinit var drawingListView: RecyclerView
    private lateinit var drawingListAdapter: DrawingListAdapter
    private var drawings = ArrayList<Drawing>()
    private var userId: Int = Authentication.getUser()!!.id

    private var firstSearchDone: Boolean = false


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_album_search_drawing, container, false)
        albums = requireArguments().get("albums") as ArrayList<Album>

        mainView = view.findViewById(R.id.mainView)
        setUpSearchField(view)
        setUpDropdown(view)
        setUpButtons(view)
        setUpDrawingList(view)
        listenToDrawingChange()

        return view
    }

    override fun onStart() {
        super.onStart()
        firstSearchDone = false
    }

    override fun onStop() {
        super.onStop()
        unregisterListenerToDrawingChange()
    }

    private fun unregisterListenerToDrawingChange() {
        RoomService.getAppClient().whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    it.value.onMessage(PayloadTypes.DRAWING_CHANGE) { }
                }
                is Promise.Result.Error -> {
                    println(it.error)
                }
            }
        }
    }

    private fun listenToDrawingChange() {
        RoomService.getAppClient().whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    it.value.onMessage(PayloadTypes.DRAWING_CHANGE) { payload ->
                        Handler(Looper.getMainLooper()).post {
                            if(firstSearchDone) {
                                fetchDrawings()
                            }
                        }
                    }

                }
                is Promise.Result.Error -> {
                    println(it.error)
                }
            }
        }
    }

    private fun setUpDrawingList(view: View) {
        drawingListView = view.findViewById(R.id.drawingsListView)
        drawingListAdapter = DrawingListAdapter(drawings, userId, DrawingListAdapter.ListType.SearchList,
            { position -> onDrawingJoin(position) },
            { position -> onDrawingVisualize(position) },
            { position -> onDrawingEdit(position) },
            { position -> onDrawingDelete(position) },
            { position, isExposed -> onDrawingExpose(position, isExposed) },
            { position, isPasswordProtected -> onDrawingPasswordClick(position, isPasswordProtected) })
        drawingListView.adapter = drawingListAdapter
        drawingListView.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        drawingListView.visibility = View.GONE
    }

    private fun setUpDropdown(view: View) {
        albumDropDownContainer = view.findViewById(R.id.albumDropdownContainer)
        albumDropdown = view.findViewById<com.google.android.material.textfield.TextInputLayout>(R.id.albumDropdown).editText as? AutoCompleteTextView

        val adapter = ArrayAdapter(view.context, R.layout.tool_spinner_item, albums)
        albumDropdown?.setAdapter(adapter)
        albumDropdown?.setDropDownBackgroundResource(R.color.backgroundColor)

        albumDropdown?.setText(adapter.getItem(0).toString(), false)
        currentAlbumPosition = 0
        albumDropdown?.setOnItemClickListener { _, _, position, _ ->
            currentAlbumPosition = position
        }
    }

    private fun setUpSearchField(view: View) {
        searchField = view.findViewById(R.id.searchDrawing)
        searchField.setOnKeyListener { view, i, keyEvent ->
            if(keyEvent.action == KeyEvent.ACTION_DOWN) {
                when(i) {
                    KeyEvent.KEYCODE_ENTER -> {
                        (view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(view.windowToken, 0)
                        fetchDrawings()
                    }
                }
            }
            true
        }
    }

    private fun setUpButtons(view: View) {
        checkBox = view.findViewById(R.id.checkBox)
        checkBox.setOnCheckedChangeListener { compoundButton, bool ->
            showHideAlbumDropdown(!bool)
        }
        checkBox.performClick()

        searchButton = view.findViewById(R.id.searchBtn)
        searchButton.setOnClickListener {
            firstSearchDone = true
            fetchDrawings()
        }

        closeBtn = view.findViewById(R.id.leaveBtn)
        closeBtn.setOnClickListener {
            (parentFragment as AlbumMenuFragment).removeAlbumActionFragment()
        }
    }

    private fun showHideAlbumDropdown(isVisible: Boolean) {
        albumDropDownContainer.visibility = if(isVisible) View.VISIBLE else View.GONE
    }

    private fun onDrawingDelete(position: Int) {
        val drawing = drawings[position]
        val bundle: Bundle = bundleOf(Pair("drawing", DrawingModel(drawing.name, drawing.album_id, drawing.id, null)))

        if(childFragmentManager.findFragmentByTag("albumInfoAction") == null) {
            childFragmentManager.commit {
                setReorderingAllowed(true)
                add<DrawingDeleteFragment>(R.id.DrawingActionFragmentContainer, "albumInfoAction", args = bundle)
            }
            enableDisableViewGroup(mainView, false)
            (parentFragment as AlbumMenuFragment).enableDisableAlbumMenuView(false)
        }
    }

    private fun onDrawingEdit(position: Int) {
        var albums: ArrayList<Album>

        viewModel.fetchUserAlbums().whenComplete { result ->
            when(result) {
                is Promise.Result.Success -> {
                    albums = result.value
                    albums.remove(albums.find { it.id == 0 })
                    val drawing = drawings[position]
                    val bundle: Bundle = bundleOf(
                        Pair("albums", albums),
                        Pair("mode", DrawingCreationFragment.DrawingMode.EDIT),
                        Pair("drawing", DrawingModel(drawing.name, drawing.album_id, drawing.id, null)))

                    if(childFragmentManager.findFragmentByTag("albumInfoAction") == null){
                        childFragmentManager.commit {
                            setReorderingAllowed(true)
                            add<DrawingCreationFragment>(R.id.DrawingActionFragmentContainer, "albumInfoAction", args = bundle)
                        }
                        enableDisableViewGroup(mainView, false)
                        (parentFragment as AlbumMenuFragment).enableDisableAlbumMenuView(false)
                    }
                }
                is Promise.Result.Error -> {
                    //handle error
                    val error = RequestFieldErrorResponse.Deserializer().deserialize(result.error.errorData.toString(
                        Charset.defaultCharset()))
                    error.displayError(mainView)
                }
            }
        }
    }


    private fun onDrawingJoin(position: Int) {
        val drawing = drawings.getOrNull(position)
        if(drawing != null) {
            if(drawing.isPasswordProtected && drawing.owner_id != userId) {
                if(childFragmentManager.findFragmentByTag("albumInfoAction") == null) {
                    val bundle = bundleOf(Pair("drawing", DrawingModel(drawing.name, drawing.album_id, drawing.id, null)))
                    childFragmentManager.commit {
                        setReorderingAllowed(true)
                        add<DrawingPasswordVerifyFragment>(R.id.DrawingActionFragmentContainer, "albumInfoAction", args = bundle)
                    }
                    enableDisableViewGroup(mainView, false)
                    (parentFragment as AlbumMenuFragment).enableDisableAlbumMenuView(false)
                }
            }
            else {
                val bundle = bundleOf(Pair("drawingId",drawing.id), Pair("drawingName", drawing.name))
                Navigation.findNavController(mainView).navigate(R.id.action_albumMenuFragment_to_drawingRoomFragment, args = bundle)
            }
        }
    }

    private fun onDrawingPasswordClick(position: Int, isPasswordProtected: Boolean) {
        val drawing = drawings[position]
        if(isPasswordProtected) {
            //setPasswordFrag
            val bundle = bundleOf(Pair("drawing", DrawingModel(drawing.name, drawing.album_id, drawing.id, null)))
            if(childFragmentManager.findFragmentByTag("albumInfoAction") == null){
                childFragmentManager.commit {
                    setReorderingAllowed(true)
                    add<DrawingPasswordEditFragment>(R.id.DrawingActionFragmentContainer, "albumInfoAction", args = bundle)
                }
                enableDisableViewGroup(mainView, false)
                (parentFragment as AlbumMenuFragment).enableDisableAlbumMenuView(false)
            }
        }
        else {
            //removePassword
            val editedDrawingModel = DrawingModel(drawing.name, drawing.album_id, drawing.id, "")
            viewModel.updateDrawing(editedDrawingModel).whenComplete { result: Promise.Result<RequestResponse, FuelError> ->
                when (result) {
                    is Promise.Result.Success -> {
                        val requestResponse: RequestResponse = result.value
                        requestResponse.displayResponse(requireView())
                    }
                    is Promise.Result.Error -> {
                        //handle error
                        val error = RequestFieldErrorResponse.Deserializer()
                            .deserialize(result.error.errorData.toString(Charset.defaultCharset()))
                        error.displayError(requireView())
                    }
                }
            }
        }
    }

    private fun onDrawingExpose(position: Int, isExposed: Boolean) {
        val drawing = drawings[position]
        if(isExposed) {
            viewModel.addDrawingToExposition(drawing.album_id, drawing.id).whenComplete { result ->
                when(result) {
                    is Promise.Result.Success -> {
                        val requestResponse: RequestResponse = result.value
                        requestResponse.displayResponse(requireView())
                    }
                    is Promise.Result.Error -> {
                        val error = RequestResponse.Deserializer().deserialize(result.error.errorData.toString(
                            Charset.defaultCharset()))
                        error.displayResponse(requireView())
                    }
                }
            }
        }
        else {
            viewModel.removeDrawingFromExposition(drawing.album_id, drawing.id).whenComplete { result ->
                when(result) {
                    is Promise.Result.Success -> {
                        val requestResponse: RequestResponse = result.value
                        requestResponse.displayResponse(requireView())
                    }
                    is Promise.Result.Error -> {
                        val error = RequestResponse.Deserializer().deserialize(result.error.errorData.toString(
                            Charset.defaultCharset()))
                        error.displayResponse(requireView())
                    }
                }
            }
        }
    }

    private fun onDrawingVisualize(position: Int) {
        //GIF ??
        val drawing: Drawing = drawings[position]
        val bundle: Bundle = bundleOf(Pair("drawing", drawing.image))

        if(childFragmentManager.findFragmentByTag("albumInfoAction") == null){
            childFragmentManager.commit {
                setReorderingAllowed(true)
                add<DrawingVisualizeFragment>(R.id.DrawingActionFragmentContainer, "albumInfoAction", args = bundle)
            }
            enableDisableViewGroup(mainView, false)
            (parentFragment as AlbumMenuFragment).enableDisableAlbumMenuView(false)
        }
    }

    fun removeActionFragment() {
        val albumActionFragment = childFragmentManager.findFragmentByTag("albumInfoAction")
        if(albumActionFragment != null) {
            childFragmentManager.commit {
                setReorderingAllowed(true)
                remove(albumActionFragment)
            }
        }
        enableDisableViewGroup(mainView, true)
        (parentFragment as AlbumMenuFragment).resetAlbumInfoSettings()

        fetchDrawings()
    }

    private fun fetchDrawings() {
        val albumId = if(checkBox.isChecked) -1 else albums[currentAlbumPosition].id

        viewModel.drawingSmartFilter(searchField.text.toString(), albumId).whenComplete { result ->
            when(result) {
                is Promise.Result.Success -> {
                    drawings.clear()
                    drawings.addAll(result.value)
                    drawingListAdapter.notifyDataSetChanged()

                    if(drawings.size == 0) {
                        drawingListView.visibility = View.GONE
                        enableDisableViewGroup(drawingListView, false)
                    }
                    else {
                        drawingListView.visibility = View.VISIBLE
                        enableDisableViewGroup(drawingListView, true)
                    }
                }
                is Promise.Result.Error -> {

                }
            }
        }
    }
}