package com.example.client_leger.classes

import com.example.client_leger.getApiMapper
import com.github.kittinunf.fuel.core.ResponseDeserializable
import java.util.*

data class ConnectionHistory(
    val createdAt: Date,
    val invalidatedAt: Date?
) {
    class ListDeserializer : ResponseDeserializable<ArrayList<ConnectionHistory>> {
        override fun deserialize(content: String): ArrayList<ConnectionHistory> {
            val mapper = getApiMapper()
            return mapper.readValue(content, mapper.typeFactory.constructCollectionType(ArrayList::class.java, ConnectionHistory::class.java))
        }
    }
}
