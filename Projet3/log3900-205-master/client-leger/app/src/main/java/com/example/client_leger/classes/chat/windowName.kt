package com.example.client_leger.classes.chat


enum class windowName(val value: String) {
    AlBUM("fragment_album_menu"),
    DRAWING_ROOM("drawing_room_fragment");
}