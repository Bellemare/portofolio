package com.example.client_leger.classes

interface OnClickEditionAction {
    fun callRequestToJoin(id: Int)
    fun callJoinAlbum(session: CollaborationSession)
}