package com.example.client_leger.classes.sockets

import kotlin.collections.ArrayList

data class MessageHistoryPayload(val messages: ArrayList<MessagePayload>) : Payload() {}
class SocketMessageHistoryPayload(type: Int, payload: MessageHistoryPayload): SocketPayload(type, payload)
