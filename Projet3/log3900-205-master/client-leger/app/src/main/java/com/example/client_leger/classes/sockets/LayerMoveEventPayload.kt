package com.example.client_leger.classes.sockets

import com.example.client_leger.classes.drawing.DrawingEvent

class LayerMoveEventPayload(
    val id: Int,
    val layer: Int,
    val newLayer: Int,
    val after: Int,
    val userId: Int? = null,
    val user: String? = null,
): Payload() {}
class SocketLayerMoveEventPayload(type: Int, payload: LayerMoveEventPayload): SocketPayload(type, payload)
