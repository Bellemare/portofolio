package com.example.client_leger.classes

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.client_leger.R

class AlbumRequestAdapter(var albumRequestList: ArrayList<AlbumJoinRequest>,
                          private val onAcceptClick: (position: Int) -> Unit,
                          private val onDenyClick: (position: Int) -> Unit): RecyclerView.Adapter<AlbumRequestAdapter.ViewHolder>() {
    class ViewHolder(view: View,
                     private val onAcceptClick: (position: Int) -> Unit,
                     private val onDenyClick: (position: Int) -> Unit): RecyclerView.ViewHolder(view) {

        val requestText: TextView = view.findViewById(R.id.requestText)
        val requestAccept: TextView = view.findViewById(R.id.acceptRequestBtn)
        val requestDeny: TextView = view.findViewById(R.id.denyRequestBtn)

        init {
            requestAccept.setOnClickListener { onAcceptClick(adapterPosition) }
            requestDeny.setOnClickListener { onDenyClick(adapterPosition) }
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.fragment_album_request_list_item, viewGroup, false)
        return ViewHolder(view, onAcceptClick, onDenyClick)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val request = albumRequestList[position]

        holder.requestText.setText(getRequestString(request.user, request.album))
    }

    override fun getItemCount(): Int {
        return albumRequestList.size
    }

    private fun getRequestString(username: String, album: String): String {
        val albumName = if(album.length >= 15) album.substring(0,12) + "..." else album

        return "L'utilisateur $username souhaite rejoindre l'album $albumName"
    }
 }