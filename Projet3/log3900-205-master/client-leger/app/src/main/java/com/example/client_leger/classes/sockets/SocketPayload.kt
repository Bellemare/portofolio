package com.example.client_leger.classes.sockets

open class SocketPayload(val type: Int, open val payload: Payload)
data class SocketMessage(val type: Int)
