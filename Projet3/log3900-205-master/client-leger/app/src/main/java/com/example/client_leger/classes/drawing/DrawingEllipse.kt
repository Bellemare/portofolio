package com.example.client_leger.classes.drawing

import com.example.client_leger.classes.Vec2
import com.example.client_leger.classes.toolProperties.ToolProperties
import com.example.client_leger.services.drawingServices.TextDrawingService
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.Gson
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.sqrt

@JsonIgnoreProperties(ignoreUnknown = true)
class DrawingEllipse(
    var x: Float,
    var y: Float,
    var radiusX: Float,
    var radiusY: Float,
    text: DrawingText? = null,
): DrawingShape(text) {
    override fun deepCopy(): DrawingElement {
        val parser = Gson()
        val copy = parser.fromJson(parser.toJson(this), DrawingEllipse::class.java)
        copyBoundingBox(copy)
        return copy
    }

    override fun editBoundingBox(): BoundingBox? {
        val boundingBox = boundingBox
        if (boundingBox == null) {
            return null
        }

        return BoundingBox(
            boundingBox.x,
            boundingBox.y,
            boundingBox.w,
            abs(radiusY * 2)
        )
    }

    override fun generateTextLines(attributes: ToolProperties): Array<String> {
        if (text == null || text!!.text.isEmpty()) {
            return arrayOf()
        }

        val dataLines = text!!.text.split("\n")
        val lines = arrayListOf<String>()
        val apparentRadiusX = radiusX - TextDrawingService.PADDING.x - attributes.lineWidth
        val apparentRadiusY = radiusY - TextDrawingService.PADDING.y - attributes.lineWidth
        var currentIndex = 0
        var max = 0.0f
        for (line in dataLines) {
            val newLine = getNewTextLine(line) {
                var maxWidth = getMaxWidth(apparentRadiusX, apparentRadiusY, currentIndex++)
                if (maxWidth == 0.0f) {
                    maxWidth = max
                }
                max = max(maxWidth, max)
                return@getNewTextLine maxWidth
            }
            lines.add(newLine)
        }

        return lines.toTypedArray()
    }

    private fun getMaxWidth(apparentRadiusX: Float, apparentRadiusY: Float, i: Int): Float {
        val height = 16.5 * (i + 1)
        val y = abs(apparentRadiusY - height)
        val x = sqrt(apparentRadiusX * apparentRadiusX * (1 - (y * y)/(apparentRadiusY * apparentRadiusY)))
        if (x.isNaN())
            return 0.0f
        return (x * 2).toFloat()
    }

    override fun getTopLeftPosition(attributes: ToolProperties): Vec2 {
        return Vec2(x - radiusX, y - radiusY)
    }

    override fun applyScaling(dw: Float, dh: Float, flipX: Boolean, flipY: Boolean, attributes: ToolProperties): Boolean {
        if (dw == 0.0f || dh == 0.0f) {
            return false
        }

        super.applyScaling(dw, dh, flipX, flipY, attributes)
        radiusX = abs(radiusX * dw)
        radiusY = abs(radiusY * dh)
        return true
    }

    override fun applyTranslation(dx: Float, dy: Float) {
        super.applyTranslation(dx, dy)
        x += dx
        y += dy
    }
}
