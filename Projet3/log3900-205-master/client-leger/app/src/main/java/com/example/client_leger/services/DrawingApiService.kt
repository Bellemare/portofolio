package com.example.client_leger.services

import com.example.client_leger.classes.*
import com.example.client_leger.classes.sockets.SocketClient
import com.example.client_leger.classes.Drawing
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.coroutines.awaitObjectResult
import com.github.kittinunf.result.Result
import com.shopify.promises.Promise
import okhttp3.OkHttpClient
import java.lang.Exception
import java.util.concurrent.TimeUnit

object DrawingApiService: ApiService() {
    val BASE_URL = "/Drawing"

    private var client: OkHttpClient
    private var activeRooms: HashMap<Int, SocketClient> = hashMapOf()

    init {
        client = OkHttpClient.Builder()
            .writeTimeout(5, TimeUnit.SECONDS)
            .readTimeout(5, TimeUnit.SECONDS)
            .connectTimeout(10, TimeUnit.SECONDS)
            .build()
    }

    fun joinCollaborativeSession(drawingId: Int): Promise<SocketClient, Unit> {
        return Promise {
            val conn = SocketClient(client)
            val authToken = Authentication.getAuthenticationToken()
            conn.onClose = {
                reject(Unit)
            }
            conn.connect("${BASE_URL}/${drawingId}/JoinSession/${authToken?.token}", onConnect = {
                conn.onClose = {
                    leaveSession(drawingId)
                }
                activeRooms[drawingId] = conn
                resolve(conn)
            })
        }
    }

    fun leaveSession(drawingId: Int) {
        val room = activeRooms[drawingId]
        if (room != null) {
            room.closeConnection()
            activeRooms.remove(drawingId)
        }
    }

    suspend fun create(drawing: DrawingModel): Result<Drawing, FuelError> {
        val url = "$BASE_URL"
        return handleResponse(post(RequestConfig(url=url), drawing).awaitObjectResult(Drawing.Deserializer()))
    }

    suspend fun updateDrawing(drawing: DrawingModel): Result<RequestResponse, FuelError> {
        val url = "$BASE_URL/${drawing.id}"
        return handleResponse(put(RequestConfig(url=url), drawing).awaitObjectResult(RequestResponse.Deserializer()))
    }

    suspend fun deleteDrawing(drawingId: Int): Result<RequestResponse, FuelError> {
        val url = "$BASE_URL/${drawingId}"
        return handleResponse(delete(RequestConfig(url=url)).awaitObjectResult(RequestResponse.Deserializer()))
    }

    suspend fun smartFilter(searchTerm: String, albumId: Int): Result<ArrayList<Drawing>, FuelError> {
        val url = "$BASE_URL/SmartFilter"
        return handleResponse(post(RequestConfig(url=url),SmartFilterRequestData(searchTerm,albumId)).awaitObjectResult(Drawing.DrawingListDeserializer()))
    }

    suspend fun verifyPassword(request: PasswordVerifyRequest): Result<RequestResponse, FuelError> {
        val url = "$BASE_URL/${request.drawingId}/VerifyPassword"
        return handleResponse(post(RequestConfig(url = url), request).awaitObjectResult(RequestResponse.Deserializer()))
    }

}

data class SmartFilterRequestData(val search_value: String, val album_id: Int)