package com.example.client_leger.classes.apiEvents

class AvatarChangeEvent(): AppEvent() {
    override val type: AppEventType = AppEventType.AVATAR_CHANGE_EVENT
}