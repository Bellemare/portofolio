package com.example.client_leger.viewModels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.client_leger.classes.User
import com.example.client_leger.services.UserService
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.result.Result
import com.shopify.promises.Promise
import kotlinx.coroutines.launch

class SideBarViewModel: ViewModel() {

    fun getCurrentUser(): Promise<User, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = UserService.getCurrentUser()
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }
}