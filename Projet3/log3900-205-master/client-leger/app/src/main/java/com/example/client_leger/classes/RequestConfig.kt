package com.example.client_leger.classes

data class RequestConfig(val endpoint: String? = null, val host: String? = null, val port: Int? = null, val url: String) {}