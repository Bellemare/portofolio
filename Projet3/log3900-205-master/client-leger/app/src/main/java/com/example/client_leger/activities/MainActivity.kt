package com.example.client_leger.activities

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import com.example.client_leger.HttpInterceptor
import com.example.client_leger.R
import com.example.client_leger.services.LocalApiService
import com.github.kittinunf.fuel.core.FuelManager

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        supportActionBar?.hide()

        setContentView(R.layout.activity_main)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE

        LocalApiService.initialize(this)

        val interceptor = HttpInterceptor.httpRequestInterceptor()
        FuelManager.instance.addRequestInterceptor(interceptor)
    }
}

