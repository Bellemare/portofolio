package com.example.client_leger.classes.sockets

import com.example.client_leger.classes.drawing.DrawingRectEvent

class DrawingRectEventPayload(
    override val event: DrawingRectEvent,
    userId: Int,
    user: String,
) : DrawingEventPayload(event, userId, user) {}
class SocketDrawingRectEventPayload(type: Int, payload: DrawingRectEventPayload): SocketPayload(type, payload)
