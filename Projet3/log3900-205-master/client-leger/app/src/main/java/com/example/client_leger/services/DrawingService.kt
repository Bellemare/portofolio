package com.example.client_leger.services

import android.graphics.*
import android.os.Handler
import android.os.Looper
import com.example.client_leger.classes.Authentication
import com.example.client_leger.classes.Vec2
import com.example.client_leger.classes.apiEvents.*
import com.example.client_leger.classes.drawing.*
import com.example.client_leger.classes.sockets.DrawingEventPayload
import com.example.client_leger.classes.sockets.DrawingHistoryPayload
import com.example.client_leger.classes.sockets.LayerEventType
import com.example.client_leger.services.couches.CouchesService
import com.example.client_leger.services.tools.*
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

object DrawingService {
    private val waitTimeoutMsPreviewRefresh = 50L

    private lateinit var previewBitmap: Bitmap
    private lateinit var previewCanvas: Canvas
    private lateinit var drawingBitmap: Bitmap
    private lateinit var drawingCanvas: Canvas

    private lateinit var drawingEventBitmap: Bitmap
    private lateinit var drawingEventCanvas: Canvas

    private val tools: HashMap<ToolType, ToolService> = hashMapOf()

    private lateinit var currentTool: ToolService
    private lateinit var paint: Paint

    private var propertySetListener: AppEvents.IntWrapper = AppEvents.IntWrapper(0)

    private var handlerLock: Mutex = Mutex()
    private var handler: Handler? = null
    private var isActiveHandler: Boolean = false
    private lateinit var previewRunnable: Runnable

    private var nextPreviewEvent: DrawingEvent? = null

    private var previousTool: ToolService? = null

    var disableEvents: Boolean = false

    var primaryColor: com.example.client_leger.classes.Color = com.example.client_leger.classes.Color(0,0,0,1)
    var secondaryColor: com.example.client_leger.classes.Color = com.example.client_leger.classes.Color(255,255,255,1)
    var textColor: com.example.client_leger.classes.Color = com.example.client_leger.classes.Color(0,0,0,1)

    fun setHandler(looper: Looper) {
        handler = Handler(looper)
        previewRunnable = Runnable {
            previewRefresh()
        }
    }

    fun init() {
        SynchronizerService.mainHandler = Handler(Looper.getMainLooper())
        initBitmap()
        initTools()
        SynchronizerService.redrawHandler = {
            handler?.post {
                drawTopLayer()
                if (it) {
                    SynchronizerService.saveDrawing(drawingBitmap)
                }
            }
        }
    }

    private fun initBitmap() {
        drawingEventBitmap = Bitmap.createBitmap(CanvasSizeService.DEFAULT_WIDTH, CanvasSizeService.DEFAULT_HEIGHT, Bitmap.Config.ARGB_8888)
        drawingEventCanvas = Canvas(drawingEventBitmap)
        drawingBitmap = Bitmap.createBitmap(CanvasSizeService.DEFAULT_WIDTH, CanvasSizeService.DEFAULT_HEIGHT, Bitmap.Config.ARGB_8888)
        drawingCanvas = Canvas(drawingBitmap)
        previewBitmap = Bitmap.createBitmap(CanvasSizeService.DEFAULT_WIDTH, CanvasSizeService.DEFAULT_HEIGHT, Bitmap.Config.ARGB_8888)
        previewCanvas = Canvas(previewBitmap)
    }

    private fun initTools() {
        tools[ToolType.PENCIL] = PencilService()
        tools[ToolType.RECTANGLE] = RectangleService()
        tools[ToolType.ELLIPSE] = EllipseService()
        tools[ToolType.SELECTION] = SelectionService()

        currentTool = tools.get(ToolType.PENCIL) as PencilService
        setCurrentTool(ToolType.PENCIL)
        listenSelectionEvents()
    }

    fun getDrawingData(): Bitmap {
        return drawingBitmap
    }

    fun getPreviewData(): Bitmap {
        return previewBitmap
    }

    fun getCurrentTool(): ToolService {
        return currentTool
    }

    fun initLayerTree(drawing: DrawingHistoryPayload) {
        try {
            handler?.post {
                AppEvents.emitEvent(LoadingStartEvent())
            }
            CouchesService.reset()
            val events = drawing.events
            events.forEach {
                val layer = DrawingEventWrapper(it)
                layer.renderFct = { evt -> drawFromDrawingEventWrapper(evt) }
                layer.redrawFct = { evt -> drawFromEvent(evt, drawingEventCanvas) }
                drawFromDrawingEventWrapper(layer)
                CouchesService.addLayer(layer, false)
            }
            CouchesService.renderAll()
            drawTopLayer()
            handler?.post {
                AppEvents.emitEvent(LoadingEndEvent())
            }
        } catch (e: OutOfMemoryError) {
            AppEvents.emitEvent(DrawingErrorEvent("Le dessin est trop volumineux pour être chargé"))
        }
    }

    private fun listenSelectionEvents() {
        val selectionTool = tools.get(ToolType.SELECTION) as SelectionService
        selectionTool.onRedrawSelectionEvent = {
            nextPreviewEvent = null
            drawTopLayer()
        }
        selectionTool.onPreviewSelectionEvent = {
            previewBitmap.eraseColor(Color.TRANSPARENT)
            val event = it?.drawingEvent
            if (event != null) {
                nextPreviewEvent = event
            }
            previewRefresh()
        }
        selectionTool.endSelectionEvent = {
            val previous = previousTool
            nextPreviewEvent = null
            if (previous != null && it) {
                setCurrentTool(previous.toolType)
            }
        }
    }

    fun setCurrentTool(tool: ToolType, init: Boolean = true) {
        if (disableEvents)
            return

        if (tools.containsKey(tool)) {
            previousTool = currentTool
            currentTool.onDestroy()
            currentTool = tools[tool]!!
            currentTool.setProperty("lineWidth", previousTool!!.getToolAttributes().lineWidth)
            if (init)
                currentTool.onInit()
            runBlocking {
                AppEvents.unregister(propertySetListener.value, AppEventType.TOOL_PROPERTY_SET_EVENT)
                AppEvents.listenTo(AppEventType.TOOL_PROPERTY_SET_EVENT, propertySetListener) {
                    val evt = it as ToolPropertySetEvent
                    currentTool.setProperty(evt.key, evt.value)
                    AppEvents.emitEvent(ToolPropertiesChangeEvent(currentTool.getToolAttributes()))
                }
            }
            AppEvents.emitEvent(ToolPropertiesChangeEvent(currentTool.getToolAttributes()))
            listenToToolEvents()
        }
    }

    fun drawCollaboratorEvent(event: DrawingEventPayload) {
        val layer = DrawingEventWrapper(event.event)
        layer.isSelected = true
        layer.renderFct = { drawFromDrawingEventWrapper(it) }
        layer.redrawFct = { evt -> drawFromEvent(evt, drawingEventCanvas) }
        layer.renderLayer()
        CouchesService.addLayer(layer, false)
        if (event.userId == Authentication.getUser()?.id) {
            disableEvents = false
            setCurrentTool(ToolType.SELECTION, false)
            val tool = currentTool as SelectionService
            tool.onInitQuickSelect()
            SynchronizerService.layerEvent(LayerEventType.SELECT, layer, CouchesService.getDrawingEventIndex(layer))
        }
    }

    private fun drawFromEvent(event: DrawingEvent, canvas: Canvas) {
        val types = ToolType.values().associateBy(ToolType::value)
        val type = types[event.toolType]
        val tool = tools[type]
        if (tool != null) {
            val paint = Paint()
            tool.setPaintProperties(paint, event.attributes)
            tool.getDrawingService().drawEvent(canvas, paint, event)
        }
    }

    private fun drawFromDrawingEventWrapper(evt: DrawingEventWrapper): Boolean {
        val drawingEvent = evt.drawingEvent
        if (drawingEvent == null) {
            return false
        }

        val bitmap = Bitmap.createBitmap(CanvasSizeService.canvasSize.x.toInt(), CanvasSizeService.canvasSize.y.toInt(), Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawFromEvent(drawingEvent, canvas)
        val boundingBox = drawingEvent.data.boundingBox
        val success = boundingBox != null && boundingBox.w > 0 && boundingBox.h > 0
        if (success && boundingBox != null) {
            val image = Bitmap.createBitmap(boundingBox.w.toInt(), boundingBox.h.toInt(), Bitmap.Config.ARGB_8888)
            val imageCanvas = Canvas(image)
            val bottomRight = Vec2(boundingBox.x + boundingBox.w, boundingBox.y + boundingBox.h)
            val srcRect = Rect(boundingBox.x.toInt(), boundingBox.y.toInt(), bottomRight.x.toInt(), bottomRight.y.toInt())
            val dstRect = Rect(0, 0, boundingBox.w.toInt(), boundingBox.h.toInt())
            imageCanvas.drawBitmap(bitmap, srcRect, dstRect, null)
            bitmap.recycle()
            evt.boundingBox = boundingBox
            evt.setImageData(image)
        }

        return success
    }

    fun drawTopLayer() {
        drawingCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR)
        val boundingBox = CouchesService.topLayer.getBoundingBox()
        if (boundingBox == null || boundingBox.w == 0.0f || boundingBox.h == 0.0f)
            return
        val srcRect = Rect(0, 0, boundingBox.w.toInt(), boundingBox.h.toInt())
        val bottomRight = Vec2(boundingBox.x + boundingBox.w, boundingBox.y + boundingBox.h)
        val dstRect = Rect(boundingBox.x.toInt(), boundingBox.y.toInt(), bottomRight.x.toInt(), bottomRight.y.toInt())
        drawingCanvas.drawBitmap(CouchesService.topLayer.getImage(), srcRect, dstRect, null)
        AppEvents.emitEvent(DrawEvent(drawingBitmap))
    }

    private fun listenToToolEvents() {
        currentTool.listenDataChanged() {
            val event = resolveDrawingService(it)
            if (event != null) {
                event.type = EventType.DRAW.value
                SynchronizerService.sendEvent(event)
                disableEvents = true
            }
        }
        currentTool.listenPreviewChanged() {
            it.boundingBox = null
            it.baseBoundingBox = null
            nextPreviewEvent = resolveDrawingService(it)
            val event = nextPreviewEvent?.deepCopy()
            if (event != null) {
                runBlocking {
                    launchPreviewRefresh()
                }
                event.type = EventType.PREVIEW.value
                SynchronizerService.sendEvent(event)
            } else {
                handler?.removeCallbacks(previewRunnable)
                isActiveHandler = false
            }
        }
    }

    suspend fun launchPreviewRefresh() {
        handlerLock.withLock {
            val mainHandler = handler
            if (!isActiveHandler) {
                isActiveHandler = true
                mainHandler!!.postDelayed(previewRunnable, waitTimeoutMsPreviewRefresh)
            }
        }
    }

    private fun previewRefresh() {
        val event = nextPreviewEvent
        previewBitmap.eraseColor(Color.TRANSPARENT)
        val layerEvents = HashMap(SynchronizerService.getLayerEvents())
        layerEvents.forEach {
            if (it.value.drawingEvent == null) {
                return@forEach
            }
            drawFromEvent(it.value.drawingEvent!!, previewCanvas)
        }
        val collaboratorEvents = HashMap(SynchronizerService.getPreviewEvents())
        collaboratorEvents.forEach {
            drawFromEvent(it.value.event, previewCanvas)
        }
        if (event != null) {
            drawFromEvent(event, previewCanvas)
        }
        AppEvents.emitEvent(PreviewDrawEvent(previewBitmap))
        isActiveHandler = false
    }

    private fun resolveDrawingService(data: DrawingElement): DrawingEvent? {
        if (!currentTool.getDrawingService().isEmpty(data)) {
            return currentTool.getDrawingService().createDrawingEvent(data, currentTool)
        }
        return null
    }

    fun onDestroy() {
        disableEvents = false
        if (currentTool is SelectionService) {
            val tool = currentTool as SelectionService
            val selection = tool.selectedDrawingEvent
            if (selection != null) {
                selection.isSelected = false;
                selection.renderLayer()
                drawTopLayer()
            }
            SynchronizerService.saveDrawing(drawingBitmap)
        }
        runBlocking {
            AppEvents.unregister(propertySetListener.value, AppEventType.TOOL_PROPERTY_SET_EVENT)
        }
        currentTool.onDestroy()
        SynchronizerService.onDestroy()
    }
}