package com.example.client_leger.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.client_leger.classes.RegisterRequest
import com.example.client_leger.classes.RequestFieldErrorResponse
import com.example.client_leger.classes.User
import com.example.client_leger.services.UserService
import com.github.kittinunf.fuel.core.FuelError
import kotlinx.coroutines.launch
import com.github.kittinunf.result.Result
import com.shopify.promises.Promise

class AccountCreationViewModel: ViewModel() {
    private val errorData: MutableLiveData<RequestFieldErrorResponse> = MutableLiveData()

    fun getErrorData(): LiveData<RequestFieldErrorResponse> {
        return errorData
    }

    fun createNewAccount(registerRequest: RegisterRequest): Promise<User, FuelError> {
        return Promise<User, FuelError> {
            viewModelScope.launch {
                val res = UserService.register(registerRequest)
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                        if (res.error.response.statusCode == 400) {
                            val err = RequestFieldErrorResponse.Deserializer().deserialize(String(res.error.errorData))
                            if (err != null)
                                errorData.postValue(err)
                        }
                    }
                }
            }
        }
    }
}