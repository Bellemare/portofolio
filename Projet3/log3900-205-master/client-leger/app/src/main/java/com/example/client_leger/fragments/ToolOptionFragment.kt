package com.example.client_leger.fragments

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.commit
import androidx.fragment.app.replace
import com.example.client_leger.R
import com.example.client_leger.classes.apiEvents.AppEventType
import com.example.client_leger.classes.apiEvents.AppEvents
import com.example.client_leger.classes.apiEvents.ToolColorsChangeEvent
import com.example.client_leger.classes.apiEvents.ToolPropertiesChangeEvent
import com.example.client_leger.classes.toolProperties.PencilProperties
import com.example.client_leger.viewModels.ToolOptionViewModel
import com.google.android.material.slider.Slider
import kotlinx.coroutines.runBlocking

class ToolOptionFragment : Fragment() {

    private var toolMenu: AutoCompleteTextView? = null
    private lateinit var widthSlider: Slider
    private lateinit var lineWidthValueText: TextView

    private val viewModel: ToolOptionViewModel by activityViewModels()

    private lateinit var primaryColor : TextView
    private lateinit var secondaryColor : TextView
    private lateinit var textColor : TextView

    private var toolChangeListener: AppEvents.IntWrapper = AppEvents.IntWrapper(0)
    private var colorsChangeListener: AppEvents.IntWrapper = AppEvents.IntWrapper(0)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.tool_option_fragment, container, false)
        setUpSpinner(view)
        setUpWidthSlider(view)
        setUpColorPalette(view)

        setUpExit(view)


        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolMenu?.setText(viewModel.currentTool, false)
        widthSlider.value = viewModel.properties.lineWidth.toFloat()
        onWidthSliderUpdate(viewModel.properties.lineWidth)

        updateColors()
    }

    override fun onStart() {
        super.onStart()
        runBlocking {
            AppEvents.listenTo(AppEventType.TOOL_PROPERTIES_CHANGE_EVENT, toolChangeListener) {
                it as ToolPropertiesChangeEvent
                reloadToolProperties(it)
            }
            AppEvents.listenTo(AppEventType.TOOL_COLORS_CHANGE_EVENT, colorsChangeListener) {
                it as ToolColorsChangeEvent
                reloadColors(it)
            }
        }
    }

    override fun onStop() {
        super.onStop()
        runBlocking {
            AppEvents.unregister(toolChangeListener.value, AppEventType.TOOL_PROPERTIES_CHANGE_EVENT)
            AppEvents.unregister(colorsChangeListener.value, AppEventType.TOOL_COLORS_CHANGE_EVENT)
        }
    }

    private fun getColor(color: com.example.client_leger.classes.Color): Int {
        return Color.argb(color.a * 255, color.r, color.g, color.b)
    }

    private fun reloadColors(event: ToolColorsChangeEvent) {
        viewModel.setColor(0, getColor(event.primary))
        viewModel.setColor(1, getColor(event.secondary))
        viewModel.setColor(2, getColor(event.text))

        updateColors()
    }

    private fun reloadToolProperties(event: ToolPropertiesChangeEvent) {
        val properties = event.properties
        widthSlider.value = properties.lineWidth.toFloat()
        toolMenu?.setText(viewModel.currentTool, false)
    }

    private fun setUpSpinner(view: View) {
        toolMenu = view.findViewById<com.google.android.material.textfield.TextInputLayout>(R.id.toolMenu).editText as? AutoCompleteTextView
        val adapter = ArrayAdapter(view.context, R.layout.tool_spinner_item, resources.getStringArray(R.array.drawing_tools))
        toolMenu?.setAdapter(adapter)
        toolMenu?.setDropDownBackgroundResource(R.color.backgroundColor)

        toolMenu?.setOnItemClickListener{ adapterView, _, position, _ ->
            viewModel.setTool(adapterView.getItemAtPosition(position).toString())
        }
    }

    private fun setUpExit(view: View) {
        val exitBtn: TextView = view.findViewById(R.id.exitBtn)
        exitBtn.setOnClickListener {
            val fragment : Fragment = this
            parentFragmentManager.commit {
                remove(fragment)
            }
        }

        exitBtn.visibility = View.GONE
    }

    private fun setUpWidthSlider(view: View) {
        widthSlider = view.findViewById(R.id.lineWidthSlider)
        lineWidthValueText = view.findViewById(R.id.lineWidthValue)

        widthSlider.addOnChangeListener { _, value, _ ->
            onWidthSliderUpdate(value.toInt())
        }
    }

    private fun onWidthSliderUpdate(value: Int) {
        viewModel.setProperty("lineWidth", value)
        lineWidthValueText.text = "$value px"
    }

    private fun setUpColorPalette(view: View) {
        primaryColor = view.findViewById(R.id.primaryColor)
        secondaryColor = view.findViewById(R.id.secondaryColor)
        textColor = view.findViewById(R.id.textColor)

        primaryColor.setOnClickListener { startPalette(0) }
        secondaryColor.setOnClickListener { startPalette(1) }
        textColor.setOnClickListener { startPalette(2) }
    }

    private fun startPalette(colorIndex: Int) {
        childFragmentManager.commit {
            setReorderingAllowed(true)
            replace<ColorPaletteFragment>(R.id.colorPaletteContainer, args = bundleOf("colorIndex" to colorIndex))
        }
    }

    fun updateColors() {
        primaryColor.setBackgroundColor(viewModel.colors[0])
        if(viewModel.colors[1] == Color.TRANSPARENT){
            secondaryColor.setBackgroundResource(R.drawable.transparent_color_placeholder)
        }
        else {
            secondaryColor.setBackgroundColor(viewModel.colors[1])
        }
        textColor.setBackgroundColor(viewModel.colors[2])
    }

}