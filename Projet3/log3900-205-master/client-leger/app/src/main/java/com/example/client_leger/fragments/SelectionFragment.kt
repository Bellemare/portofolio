package com.example.client_leger.fragments

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.FrameLayout
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.example.client_leger.R
import com.example.client_leger.classes.SelectionFrameHandler
import com.example.client_leger.classes.SelectionGestureHandler
import com.example.client_leger.classes.Vec2
import com.example.client_leger.classes.apiEvents.*
import com.example.client_leger.services.CanvasSizeService
import kotlinx.coroutines.runBlocking
import kotlin.math.abs
import kotlin.math.min


class SelectionFragment : Fragment() {
    private lateinit var scalerTopLeft: View
    private lateinit var scalerTopRight: View
    private lateinit var scalerBottomLeft: View
    private lateinit var scalerBottomRight: View
    private lateinit var previewFrame: View

    private lateinit var textInput: EditText

    private lateinit var selectionView: View

    private lateinit var layoutParams: FrameLayout.LayoutParams
    private var position: Vec2 = Vec2(0.0f,0.0f)
    private var size: Vec2 = Vec2(100.0f,100.0f)

    private var selectionListener: AppEvents.IntWrapper = AppEvents.IntWrapper(0)
    private var selectionStopListener: AppEvents.IntWrapper = AppEvents.IntWrapper(0)

    private var isEditingText: Boolean = false

    private var currentText: String? = null
    private var selectionHasText: Boolean = false;

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.selection_fragment, container, false)
        selectionView = view

        textInput = view.findViewById(R.id.textInput)
        textInput.isFocusableInTouchMode = true

        scalerTopLeft = view.findViewById(R.id.scalerTopLeft)
        scalerTopRight = view.findViewById(R.id.scalerTopRight)
        scalerBottomLeft = view.findViewById(R.id.scalerBottomLeft)
        scalerBottomRight = view.findViewById(R.id.scalerBottomRight)
        previewFrame = view.findViewById(R.id.selectionFrame)

        layoutParams = FrameLayout.LayoutParams(size.x.toInt(), size.y.toInt())
        listenToEvents()
        selectionView.isVisible = false

        return view
    }

    override fun onStart() {
        super.onStart()
        listenSelection()
    }

    override fun onStop() {
        super.onStop()
        runBlocking {
            AppEvents.unregister(selectionListener.value, AppEventType.SELECTION_START_EVENT)
            AppEvents.unregister(selectionStopListener.value, AppEventType.SELECTION_STOP_EVENT)
        }
    }

    private fun listenSelection() {
        runBlocking {
            AppEvents.listenTo(AppEventType.SELECTION_START_EVENT, selectionListener) {
                it as SelectionStartEvent
                if (it.isShape) {
                    currentText = it.text
                }
                selectionHasText = it.isShape
                selectionView.isVisible = true
                position = it.position
                size = it.size
            updateView(false)
            }
            AppEvents.listenTo(AppEventType.SELECTION_STOP_EVENT, selectionStopListener) {
                selectionView.isVisible = false
                hideKeyboard()
            }
        }
    }

    private fun createOnTouchHandler(view: View, onUpdate: (translation: Vec2) -> Unit, onChange: (() -> Unit)? = null, onDoubleTap: (() -> Unit)? = null): SelectionFrameHandler {
        val gestureDetector = GestureDetector(view.context, SelectionGestureHandler(onDoubleTap))
        return SelectionFrameHandler(gestureDetector, onUpdate, onChange)
    }

    private fun listenToText() {
        if (!selectionHasText)
            return

        textInput.setText(currentText)
        if (currentText != null)
            textInput.setSelection(currentText!!.length)
        textInput.requestFocus()
        textInput.doOnTextChanged { text, a, b, c ->
            currentText = text.toString()
            AppEvents.emitEvent(SelectionTextChangeEvent(text.toString()))
        }
        isEditingText = true
        showKeyboard()
    }

    private fun listenToEvents() {
        previewFrame.setOnTouchListener(createOnTouchHandler(previewFrame, { translation: Vec2 ->
            position.x += translation.x
            position.y += translation.y
            updateView()
        }, null, {
            listenToText()
        }))
        scalerBottomRight.setOnTouchListener(createOnTouchHandler(scalerBottomRight, { resize: Vec2 ->
            size.x += resize.x
            size.y += resize.y
            updateView()
        }, { reformatPositionAndSize() }))
        scalerBottomLeft.setOnTouchListener(createOnTouchHandler(scalerBottomLeft, { resize: Vec2 ->
            size.x -= resize.x
            size.y += resize.y
            position.x += resize.x
            updateView()
        }, { reformatPositionAndSize() }))
        scalerTopLeft.setOnTouchListener(createOnTouchHandler(scalerTopLeft, { resize: Vec2 ->
            size.x -= resize.x
            size.y -= resize.y
            position.x += resize.x
            position.y += resize.y
            updateView()
        }, { reformatPositionAndSize() }))
        scalerTopRight.setOnTouchListener(createOnTouchHandler(scalerTopRight, { resize: Vec2 ->
            size.x += resize.x
            size.y -= resize.y
            position.y += resize.y
            updateView()
        }, { reformatPositionAndSize() }))
    }

    private fun reformatPositionAndSize() {
        position.y = min(position.y, position.y + size.y)
        position.x = min(position.x, position.x + size.x)
        size.x = abs(size.x)
        size.y = abs(size.y)

        AppEvents.emitEvent(SelectionResetEvent(position, size))
    }

    private fun updateView(sync: Boolean = true) {
        val pos = CanvasSizeService.addOffset(position)
        layoutParams.topMargin = min(pos.y, pos.y + size.y).toInt()
        layoutParams.marginStart = min(pos.x, pos.x + size.x).toInt()
        layoutParams.width = abs(size.x.toInt())
        layoutParams.height = abs(size.y.toInt())
        selectionView.layoutParams = layoutParams

        if (sync) {
            val xMin = min(position.x, position.x + size.x)
            val yMin = min(position.y, position.y + size.y)
            AppEvents.emitEvent(SelectionChangeEvent(Vec2(xMin, yMin), size))
        }
    }

    private fun Fragment.hideKeyboard() {
        view?.let { activity?.hideKeyboard(it) }
    }

    private fun Fragment.showKeyboard() {
        view?.let { activity?.showKeyboard(it) }
    }

    private fun Context.hideKeyboard(view: View) {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private fun Context.showKeyboard(view: View) {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }
}