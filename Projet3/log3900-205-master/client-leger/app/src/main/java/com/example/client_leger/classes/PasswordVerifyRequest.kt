package com.example.client_leger.classes

data class PasswordVerifyRequest(val drawingId: Int, val password: String)