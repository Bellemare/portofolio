package com.example.client_leger.classes.apiEvents

class ToolPropertySetEvent(val key: String, val value: Any): AppEvent() {
    override val type = AppEventType.TOOL_PROPERTY_SET_EVENT
}