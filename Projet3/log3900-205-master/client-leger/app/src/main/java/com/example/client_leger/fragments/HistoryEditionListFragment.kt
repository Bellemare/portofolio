package com.example.client_leger.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.add
import androidx.fragment.app.commit
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.client_leger.R
import com.example.client_leger.classes.*
import com.example.client_leger.viewModels.AlbumMenuViewModel
import com.example.client_leger.viewModels.ContactViewModel
import com.example.client_leger.viewModels.UserViewModel
import com.shopify.promises.Promise
import java.nio.charset.Charset

class HistoryEditionListFragment : Fragment(), OnClickEditionAction {

    private lateinit var editionListView: RecyclerView
    private lateinit var editionAdapter: HistoryEditionListAdapter
    private lateinit var currentUser: User

    private var editionArray: ArrayList<CollaborationSession> = ArrayList()
    private var albumArray: ArrayList<Album> = ArrayList()
    private var availableAlbumsArray: ArrayList<Album> = ArrayList()
    private val contactViewModel: ContactViewModel by activityViewModels()
    private val userViewModel: UserViewModel by activityViewModels()
    private val albumViewModel: AlbumMenuViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_history_edition_list, container, false)

        editionListView = view.findViewById(R.id.editionList)

        setUpEditionListView(view)

        when(parentFragment) {
            is ContactFragment -> {
                getEditionHistory((parentFragment as ContactFragment).userId)
            }
            is UserFragment -> {
                getEditionHistory()
            }
        }

        getCurrentUser()
        getUserAlbums()
        getAlbumList()

        return view
    }

    private fun setUpEditionListView(view: View) {
        editionListView = view.findViewById(R.id.editionList)
        editionAdapter = HistoryEditionListAdapter(editionArray, albumArray, availableAlbumsArray, this)
        editionListView.adapter = editionAdapter
        editionListView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
    }

    private fun getCurrentUser() {
        userViewModel.getCurrentUser().whenComplete {
            when(it) {
                is Promise.Result.Success -> {
                    currentUser = it.value
                }
                is Promise.Result.Error -> {
                    println(it.error.message)
                }
            }
        }
    }

    private fun getEditionHistory() {
        userViewModel.getCollaborativeSessions().whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    editionArray.addAll(it.value)
                    editionAdapter.notifyDataSetChanged()
                }
                is Promise.Result.Error -> {
                    println(it.error.message)
                }
            }
        }
    }

    private fun getEditionHistory(id: Int) {
        contactViewModel.getCollaborativeSessions(id).whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    editionArray.addAll(it.value)
                    editionAdapter.notifyDataSetChanged()
                }
                is Promise.Result.Error -> {
                    println(it.error.message)
                }
            }
        }
    }

    private fun getUserAlbums() {
        albumViewModel.fetchUserAlbums().whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    albumArray.clear()
                    albumArray.addAll(it.value)
                    editionAdapter.notifyDataSetChanged()
                }
                is Promise.Result.Error -> {
                    println(it.error.message)
                }
            }
        }
    }

    private fun getAlbumList() {
        albumViewModel.fetchAlbumList().whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    availableAlbumsArray.clear()
                    availableAlbumsArray.addAll(it.value)
                    editionAdapter.notifyDataSetChanged()
                }
                is Promise.Result.Error -> {
                    println(it.error.message)
                }
            }
        }
    }

    private fun joinDrawing(session: CollaborationSession) {
        if(session.drawing != null) {
            if(session.isPasswordProtected && session.userId != currentUser.id ) {
                when(parentFragment) {
                    is ContactFragment -> {
                        (parentFragment as ContactFragment).inflatePasswordView(session)
                    }
                    is UserFragment -> {
                        //theoretically will never be called
                        (parentFragment as UserFragment).inflatePasswordView(session)
                    }
                }
            }
            else {
                val bundle = bundleOf(Pair("drawingId", session.drawingId), Pair("drawingName", session.drawing))
                Navigation.findNavController(requireView()).navigate(R.id.drawingRoomFragment, args = bundle)
            }
        }
    }

    private fun requestToJoinAlbum(id: Int) {
        albumViewModel.requestToJoinAlbum(id).whenComplete { result ->
            when(result) {
                is Promise.Result.Success -> {
                    when(parentFragment) {
                        is ContactFragment -> {
                            result.value.displayResponse((parentFragment as ContactFragment).requireView())
                        }
                        is UserFragment -> {
                            result.value.displayResponse((parentFragment as UserFragment).requireView())
                        }
                    }
                }
                is Promise.Result.Error -> {
                    //handle error
                    val error = RequestResponse.Deserializer().deserialize(result.error.errorData.toString(
                        Charset.defaultCharset()))
                    when(parentFragment) {
                        is ContactFragment -> {
                            error.displayResponse((parentFragment as ContactFragment).requireView())
                        }
                        is UserFragment -> {
                            error.displayResponse((parentFragment as UserFragment).requireView())
                        }
                    }
                }
            }
        }
    }

    override fun callJoinAlbum(session: CollaborationSession) {
        joinDrawing(session)
    }

    override fun callRequestToJoin(id: Int) {
        requestToJoinAlbum(id)
    }
}