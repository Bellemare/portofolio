package com.example.client_leger.fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import com.example.client_leger.R
import com.example.client_leger.classes.DrawingModel
import com.example.client_leger.classes.PasswordVerifyRequest
import com.example.client_leger.classes.RequestFieldErrorResponse
import com.example.client_leger.classes.RequestResponse
import com.example.client_leger.viewModels.AlbumMenuViewModel
import com.github.kittinunf.fuel.core.FuelError
import com.shopify.promises.Promise
import java.nio.charset.Charset

class DrawingPasswordVerifyFragment : Fragment() {

    private val viewModel: AlbumMenuViewModel by activityViewModels()

    private lateinit var confirmBtn: Button
    private var confirmBtnFirstClick: Boolean = true
    private lateinit var cancelBtn: Button

    private lateinit var passwordText: EditText
    private lateinit var titleText: TextView
    private lateinit var drawing: DrawingModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_drawing_password_edit, container, false)
        drawing = requireArguments().get("drawing") as DrawingModel

        titleText = view.findViewById(R.id.title)
        titleText.text = "Rejoindre un dessin protégé"

        passwordText = view.findViewById(R.id.passwordText)
        confirmBtn = view.findViewById(R.id.createBtn)
        confirmBtn.setOnClickListener {

            if(confirmBtnFirstClick) {
                confirmBtnFirstClick = false
                val passwordVerifyRequest: PasswordVerifyRequest = PasswordVerifyRequest(drawing.id!!, passwordText.text.toString())
                viewModel.verifyPassword(passwordVerifyRequest).whenComplete { result: Promise.Result<RequestResponse, FuelError> ->
                    when (result) {
                        is Promise.Result.Success -> {
                            val requestResponse: RequestResponse = result.value
                            requestResponse.displayResponse(view)
                            (view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(view.windowToken, 0)

                            //go to drawingRoom
                            val bundle = bundleOf(Pair("drawingId", drawing.id!!), Pair("drawingName", drawing.name))
                            Navigation.findNavController(requireView()).navigate(R.id.drawingRoomFragment, args = bundle)
                        }
                        is Promise.Result.Error -> {
                            //handle error
                            confirmBtnFirstClick = true
                            val error = RequestResponse.Deserializer()
                                .deserialize(result.error.errorData.toString(Charset.defaultCharset()))
                            error.displayResponse(view)

                            (view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(view.windowToken, 0)
                        }
                    }
                }
            }
        }

        cancelBtn = view.findViewById(R.id.cancelBtn)
        cancelBtn.setOnClickListener {
            removeFragment()
            (view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(view.windowToken, 0)
        }

        return view
    }

    private fun removeFragment() {
        when(parentFragment){
            is AlbumInfoFragment -> (parentFragment as AlbumInfoFragment).removeActionFragment()
            is AlbumSearchDrawingFragment -> (parentFragment as AlbumSearchDrawingFragment).removeActionFragment()
            is ContactFragment -> (parentFragment as ContactFragment).removePasswordFragment()
            is UserFragment -> (parentFragment as UserFragment).removePasswordFragment()
        }
    }
}