package com.example.client_leger.classes

import android.annotation.SuppressLint
import android.graphics.*
import android.graphics.Color
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.example.client_leger.R
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import kotlin.collections.ArrayList

class DrawingListAdapter(
    var drawingList: ArrayList<Drawing>,
    var userId: Int,
    var listType: ListType,
    private val onJoinClick: ((position: Int) -> Unit)?,
    private val onVisualizeClick: ((position: Int) -> Unit)?,
    private val onEditClick: ((position: Int) -> Unit)?,
    private val onDeleteClick: ((position: Int) -> Unit)?,
    private val onExposeClick: ((position: Int, isExposed: Boolean) -> Unit)?,
    private val onPasswordChange: ((position: Int, isPasswordProtected: Boolean) -> Unit)?): RecyclerView.Adapter<DrawingListAdapter.ViewHolder>() {
    public val IMAGE_WIDTH: Int = 500
    public val IMAGE_HEIGHT: Int = 300

    enum class ListType {
        SearchList,
        PublicAlbum,
        PrivateAlbum,
        Exposition
    }

    class ViewHolder(view: View,
                     private val onJoinClick: ((position: Int) -> Unit?)?,
                     private val onVisualizeClick: ((position: Int) -> Unit?)?,
                     private val onEditClick: ((position: Int) -> Unit?)?,
                     private val onDeleteClick: ((position: Int) -> Unit?)?,
                     private val onExposeClick: ((position: Int, isExposed: Boolean) -> Unit)?,
                     private val onPasswordChange: ((position: Int, isPasswordProtected: Boolean) -> Unit)?): RecyclerView.ViewHolder(view) {

        var drawingName: TextView = itemView.findViewById(R.id.drawingName)
        var drawingOwner: TextView = itemView.findViewById(R.id.drawingOwner)
        var currentActiveUsersContainer: LinearLayout = itemView.findViewById(R.id.activeUsersContainer)
        var currentActiveUsers: TextView = itemView.findViewById(R.id.currentActiveUsers)
        var dateCreation: TextView = itemView.findViewById(R.id.drawingCreationDate)

        lateinit var imgBitmap: Bitmap
        var canvas: Canvas = Canvas()
        var imgView: ImageView = itemView.findViewById(R.id.imageView)

        var deleteBtn: TextView = itemView.findViewById(R.id.deleteBtn)
        var editBtn: Button = itemView.findViewById(R.id.editBtn)
        var visualizeBtn: Button = itemView.findViewById(R.id.visualizeBtn)
        var joinBtn: Button = itemView.findViewById(R.id.joinBtn)
        var expositionButton: ToggleButton = itemView.findViewById(R.id.expositionButton)
        var passwordButton: ToggleButton = itemView.findViewById(R.id.passwordButton)

        var passwordIcon: TextView = itemView.findViewById(R.id.passwordIcon)
        var passwordButtonIsSet: Boolean = false
        var expositionBtnIsSet: Boolean = false


        init {
            setUpButtons()
        }

        private fun setUpButtons() {
            deleteBtn.setOnClickListener {
                onDeleteClick?.invoke(adapterPosition)
            }
            editBtn.setOnClickListener {
                onEditClick?.invoke(adapterPosition)
            }
            visualizeBtn.setOnClickListener {
                onVisualizeClick?.invoke(adapterPosition)
            }
            joinBtn.setOnClickListener {
                onJoinClick?.invoke(adapterPosition)
            }
            expositionButton.setOnCheckedChangeListener { _, isExposed ->
                if(expositionBtnIsSet) {
                    onExposeClick?.invoke(adapterPosition, isExposed)
                }
            }
            passwordButton.setOnCheckedChangeListener { _, isPasswordProtected ->
                if(passwordButtonIsSet) {
                    onPasswordChange?.invoke(adapterPosition, isPasswordProtected)
                }
            }
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.fragment_drawing_list_item, viewGroup, false)
        return ViewHolder(view, onJoinClick, onVisualizeClick, onEditClick, onDeleteClick, onExposeClick, onPasswordChange)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val drawing = drawingList[position]
        setUpImage(holder, drawing.image)

        holder.drawingName.text = drawing.name
        holder.drawingOwner.text = drawing.owner

        val time = ZonedDateTime.parse(drawing.created_at)
        holder.dateCreation.text = time.format(DateTimeFormatter.ofPattern("yyyy-MM-dd kk:mm:ss").withZone(ZoneId.of("America/New_York")))
        holder.currentActiveUsers.text = drawing.active_collaborators.toString()

        setUpVisibilities(holder, drawing)
    }

    private fun setUpVisibilities(holder: ViewHolder, drawing: Drawing) {
        when(listType){
            ListType.Exposition -> {
                holder.joinBtn.visibility = View.GONE
                holder.visualizeBtn.visibility = View.VISIBLE
                holder.editBtn.visibility = View.GONE
                holder.deleteBtn.visibility = View.GONE
                holder.currentActiveUsersContainer.visibility = View.GONE
                holder.expositionButton.visibility = View.GONE
                holder.passwordIcon.visibility = View.GONE
                holder.passwordButton.visibility = View.GONE
            }
            ListType.PrivateAlbum -> {
                holder.passwordIcon.visibility = View.GONE
                holder.passwordButton.visibility = View.GONE

                holder.expositionButton.visibility = View.VISIBLE
                holder.passwordButtonIsSet = false
                holder.expositionBtnIsSet = false
                holder.expositionButton.isChecked = drawing.isExposed
                holder.expositionBtnIsSet = true

                if(drawing.owner_id != userId) {
                    holder.deleteBtn.visibility = View.INVISIBLE
                    holder.editBtn.visibility = View.INVISIBLE
                }
            }
            ListType.PublicAlbum -> {
                holder.expositionButton.visibility = View.GONE
                holder.passwordIcon.visibility = if(drawing.isPasswordProtected && drawing.owner_id != userId) View.VISIBLE else View.GONE

                if(drawing.owner_id != userId) {
                    holder.deleteBtn.visibility = View.INVISIBLE
                    holder.editBtn.visibility = View.INVISIBLE
                    holder.passwordButton.visibility = View.INVISIBLE
                }
                else {
                    holder.deleteBtn.visibility = View.VISIBLE

                    holder.passwordButton.visibility = View.VISIBLE
                    holder.passwordButtonIsSet = false
                    holder.passwordButton.isChecked = drawing.isPasswordProtected
                    holder.passwordButtonIsSet = true
                }
            }
            ListType.SearchList -> {
                if(drawing.album_id == 0) {
                    holder.expositionButton.visibility = View.GONE
                    holder.passwordIcon.visibility = if(drawing.isPasswordProtected) View.VISIBLE else View.GONE

                    if(drawing.owner_id == userId) {
                        holder.passwordIcon.visibility = View.GONE
                        holder.passwordButton.visibility = View.VISIBLE
                        holder.passwordButton.isChecked = drawing.isPasswordProtected
                        holder.passwordButtonIsSet = true
                    }
                }
                else {
                    holder.expositionBtnIsSet = false
                    holder.expositionButton.visibility = View.VISIBLE
                    holder.expositionButton.isChecked = drawing.isExposed
                    holder.expositionBtnIsSet = true

                    holder.passwordButton.visibility = View.GONE
                    holder.passwordIcon.visibility = View.GONE
                }

                if(drawing.owner_id != userId) {
                    holder.deleteBtn.visibility = View.INVISIBLE
                    holder.editBtn.visibility = View.INVISIBLE
                    holder.passwordButton.visibility = View.GONE
                }

                if(holder.passwordButton.visibility == View.GONE && holder.expositionButton.visibility == View.GONE) {
                    holder.passwordButton.visibility = View.INVISIBLE
                }
            }
        }
    }

    private fun setUpImage(holder: ViewHolder, image: String) {
        val img = image.replace("data:image/png;base64,", "")
        val decodedString: ByteArray = Base64.decode(img, Base64.DEFAULT)
        var decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
        val scalingMatrix: Matrix = Matrix()
        scalingMatrix.postScale(IMAGE_WIDTH.toFloat() / decodedByte.width, IMAGE_HEIGHT.toFloat() / decodedByte.height )

        holder.imgBitmap = Bitmap.createBitmap(IMAGE_WIDTH,IMAGE_HEIGHT, Bitmap.Config.ARGB_8888)
        holder.canvas.setBitmap(holder.imgBitmap)
        val basePaint: Paint = Paint()
        basePaint.color = Color.WHITE
        holder.canvas.drawPaint(basePaint)

        holder.canvas.drawBitmap(decodedByte, scalingMatrix, null)

        holder.imgView.setImageBitmap(holder.imgBitmap)
    }

    override fun getItemCount(): Int {
        return drawingList.size
    }

}