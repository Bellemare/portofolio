package com.example.client_leger.classes.toolProperties

import com.example.client_leger.classes.drawing.DrawingType

data class ShapeProperties(
    override var drawingType: DrawingType,
    override var lineWidth : Int
) : ToolProperties() {
}
