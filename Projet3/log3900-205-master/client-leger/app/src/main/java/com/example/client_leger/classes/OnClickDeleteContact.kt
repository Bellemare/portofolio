package com.example.client_leger.classes

interface OnClickDeleteContact {
    fun onClickDeleteContact(contact: Contact)
}