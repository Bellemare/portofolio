package com.example.client_leger.services.tools

import android.graphics.Paint
import android.view.MotionEvent
import com.example.client_leger.classes.Color
import com.example.client_leger.classes.Vec2
import com.example.client_leger.classes.drawing.DrawingType
import com.example.client_leger.classes.toolProperties.ShapeProperties
import com.example.client_leger.classes.toolProperties.ToolProperties
import com.example.client_leger.services.DrawingService

abstract class ShapeService : ToolService() {
    override var defaultConfiguration: ShapeProperties = ShapeProperties(lineWidth = 1, drawingType = DrawingType.StrokeFilled)
    override val currentAttributes: ShapeProperties = ShapeProperties(defaultConfiguration.drawingType, defaultConfiguration.lineWidth)

    override fun setPaintProperties(paint: Paint, properties: ToolProperties?) {
        var toolProperties: ShapeProperties
        if (properties != null) {
            toolProperties = properties as ShapeProperties
        } else {
            toolProperties = currentAttributes
        }

        paint.strokeWidth = toolProperties.lineWidth.toFloat()
        paint.strokeJoin = Paint.Join.MITER
        paint.strokeMiter = 10f
    }

    override fun onActionDown(event: MotionEvent) {
        actionDown = true
        actionDownCoord = Vec2(event.x, event.y)
        updatePreviewData(actionDownCoord)
        previewChanged()
    }

    override fun onActionMove(event: MotionEvent) {
        if (actionDown) {
            updatePreviewData(Vec2(event.x, event.y))
            previewChanged()
        }
    }

    override fun onActionUp(event: MotionEvent) {
        if(actionDown) {
            updatePreviewData(Vec2(event.x, event.y))
            previewChanged()
        }
        copyPreviewInData()
        dataChanged()
        reset()
    }

    abstract fun copyPreviewInData()

    private fun reset() {
        actionDown = false
        clearData()
        previewChanged()
        dataChanged()
    }

    protected abstract fun updatePreviewData(actionCoord: Vec2)

}