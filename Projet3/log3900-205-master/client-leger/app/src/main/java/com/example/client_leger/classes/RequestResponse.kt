package com.example.client_leger.classes

import android.os.Parcel
import android.os.Parcelable
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.example.client_leger.R
import com.example.client_leger.getApiMapper
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.android.material.snackbar.Snackbar

data class RequestResponse(val title: String, val body: String): Parcelable {
    class Deserializer : ResponseDeserializable<RequestResponse> {
        override fun deserialize(content: String) =
            getApiMapper().readValue(content, RequestResponse::class.java)
    }

    constructor(parcel: Parcel) : this(
        parcel.readString().toString(),
        parcel.readString().toString()
    ) {}

    fun displayResponse(view: View) {
        val snackbar = Snackbar.make(view, formatErrorString(), Snackbar.LENGTH_LONG)
            .setAction("Ok"){}
        snackbar.anchorView = view.findViewById(R.id.snackbarContainer)


        val textView: TextView = (snackbar.view.findViewById(com.google.android.material.R.id.snackbar_text) as TextView)
        textView.maxLines = 99
        textView.textSize = 15f

        val button: Button = (snackbar.view.findViewById(com.google.android.material.R.id.snackbar_action)) as Button
        button.setTextColor(ContextCompat.getColor(view.context, R.color.white))

        snackbar.setBackgroundTint(if(title != "Erreur") ContextCompat.getColor(view.context, R.color.successColor) else ContextCompat.getColor(view.context, R.color.errorColor))
        snackbar.view.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT

        snackbar.show()
    }

    private fun formatErrorString(): String {
        var formattedResponseString: String = title
        formattedResponseString += "\n\n$body"

        return formattedResponseString
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeString(body)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RequestResponse> {
        override fun createFromParcel(parcel: Parcel): RequestResponse {
            return RequestResponse(parcel)
        }

        override fun newArray(size: Int): Array<RequestResponse?> {
            return arrayOfNulls(size)
        }
    }
}
