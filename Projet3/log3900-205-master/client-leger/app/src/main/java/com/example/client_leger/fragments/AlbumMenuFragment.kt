package com.example.client_leger.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.os.bundleOf
import androidx.fragment.app.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.client_leger.R
import com.example.client_leger.classes.*
import com.example.client_leger.classes.sockets.PayloadTypes
import com.example.client_leger.services.RoomService
import com.example.client_leger.viewModels.AlbumMenuViewModel
import com.shopify.promises.Promise
import java.nio.charset.Charset


class AlbumMenuFragment : Fragment(), ViewDisabler {

    private lateinit var addDrawing: TextView
    private lateinit var searchDrawing: TextView
    private lateinit var addAlbum: TextView
    private lateinit var joinAlbums: TextView
    private lateinit var showExpositions: TextView


    private lateinit var errorContainer: ConstraintLayout

    private lateinit var mainView: LinearLayout
    private lateinit var actionMenuView: LinearLayout
    private lateinit var listViews: LinearLayout
    private lateinit var sideBar: FragmentContainerView

    private lateinit var myAlbumsList: RecyclerView
    private lateinit var myAlbumsAdapter: AlbumListAdapter

    private lateinit var myJoinedAlbumsList: RecyclerView
    private lateinit var joinedAlbumsAdapter: AlbumListAdapter

    private var ownedAlbums: ArrayList<Album> = ArrayList()
    private var joinedAlbums: ArrayList<Album> = ArrayList()

    private val viewModel: AlbumMenuViewModel by activityViewModels()
    private var userId: Int? = Authentication.getUser()?.id

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_album_menu, container, false)

        mainView = view.findViewById(R.id.mainAlbumView)
        actionMenuView = view.findViewById(R.id.albumMenuActions)
        listViews = view.findViewById(R.id.albumListViews)
        sideBar = view.findViewById(R.id.sideBarContainer)
        errorContainer = view.findViewById(R.id.errorContainer)

        setUpListViews(view)
        setUpButtons(view)
        listenToAlbumChange()

        loadUser()
        fetchAlbums()

        displayError()

        return view
    }

    private fun listenToAlbumChange() {
        RoomService.getAppClient().whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    it.value.onMessage(PayloadTypes.ALBUM_CHANGE) { payload ->
                        Handler(Looper.getMainLooper()).post {
                            fetchAlbums()
                        }
                    }
                }
                is Promise.Result.Error -> {
                    println(it.error)
                }
            }
        }
    }

    private fun displayError() {
        val error = arguments?.get("error")
        if (error != null && error is RequestResponse) {
            error.displayResponse(errorContainer)
        }
    }

    private fun setUpButtons(view : View) {
        addDrawing = view.findViewById(R.id.addDrawing)
        addDrawing.setOnClickListener {
            val albums = ownedAlbums + joinedAlbums.filter { it.id != 0 }

            val bundle: Bundle = bundleOf(Pair("albums", albums),
                                        Pair("isInsideAlbum", false),
                                        Pair("mode", DrawingCreationFragment.DrawingMode.CREATE))

            if(childFragmentManager.findFragmentByTag("albumAction") == null){
                childFragmentManager.commit {
                    setReorderingAllowed(true)
                    add<DrawingCreationFragment>(R.id.AlbumActionFragmentContainer, "albumAction", args = bundle)
                }
                enableDisableAlbumMenuView(false)
            }
        }

        searchDrawing = view.findViewById(R.id.searchDrawing)
        searchDrawing.setOnClickListener {
            viewModel.fetchUserAlbums().whenComplete { result ->
                when(result) {
                    is Promise.Result.Success -> {
                        val albums = result.value
                        val bundle: Bundle = bundleOf(Pair("albums", albums))

                        if(childFragmentManager.findFragmentByTag("albumAction") == null){
                            childFragmentManager.commit {
                                setReorderingAllowed(true)
                                add<AlbumSearchDrawingFragment>(R.id.AlbumActionFragmentContainer, "albumAction", args = bundle)
                            }
                            enableDisableAlbumMenuView(false)
                        }
                    }
                    is Promise.Result.Error -> {
                        //handle error
                        val error = RequestFieldErrorResponse.Deserializer().deserialize(result.error.errorData.toString(
                            Charset.defaultCharset()))
                        error.displayError(mainView)
                    }
                }
            }
        }

        addAlbum = view.findViewById(R.id.addFolder)
        addAlbum.setOnClickListener {
            if(childFragmentManager.findFragmentByTag("albumAction") == null){
                childFragmentManager.commit {
                    setReorderingAllowed(true)
                    add<AlbumCreationFragment>(R.id.AlbumActionFragmentContainer, "albumAction")
                }
                enableDisableAlbumMenuView(false)
            }
        }

        joinAlbums = view.findViewById(R.id.joinAlbums)
        joinAlbums.setOnClickListener {
            if(childFragmentManager.findFragmentByTag("albumAction") == null){
                childFragmentManager.commit {
                    setReorderingAllowed(true)
                    add<AlbumJoinFragment>(R.id.AlbumActionDepthFragmentContainer, "albumAction")
                }
            }
        }

        showExpositions = view.findViewById(R.id.showExposition)
        showExpositions.setOnClickListener {
            if(childFragmentManager.findFragmentByTag("albumAction") == null){
                childFragmentManager.commit {
                    setReorderingAllowed(true)
                    add<AlbumExpositionMenuFragment>(R.id.AlbumActionDepthFragmentContainer, "albumAction")
                }
            }
        }
    }

    fun removeAlbumActionFragment() {
        val albumActionFragment = childFragmentManager.findFragmentByTag("albumAction")
        if(albumActionFragment != null) {
            childFragmentManager.commit {
                setReorderingAllowed(true)
                remove(albumActionFragment)
            }
        }
        enableDisableAlbumMenuView(true)
        listenToAlbumChange()
        fetchAlbums()
    }

    fun enableDisableAlbumMenuView(enabled: Boolean) {
        enableDisableViewGroup(mainView, enabled)
        enableDisableViewGroup(sideBar, enabled)
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setUpListViews(view: View) {
        myAlbumsList = view.findViewById(R.id.myAlbumsList)
        myJoinedAlbumsList = view.findViewById(R.id.joinedAlbumsList)

        myAlbumsAdapter = AlbumListAdapter(ownedAlbums) { position: Int -> onOwnedAlbumCLick(position) }
        joinedAlbumsAdapter = AlbumListAdapter(joinedAlbums) { position: Int -> onJoinedAlbumClick(position) }

        myAlbumsList.adapter = myAlbumsAdapter
        myJoinedAlbumsList.adapter = joinedAlbumsAdapter

        myAlbumsList.layoutManager = GridLayoutManager(context, 2, RecyclerView.HORIZONTAL, false)
        myJoinedAlbumsList.layoutManager = GridLayoutManager(context, 2, RecyclerView.HORIZONTAL, false)
    }

    private fun fetchAlbums() {
        viewModel.fetchUserAlbums().whenComplete { result ->
            when(result) {
                is Promise.Result.Success -> {
                    if(userId != null) {
                        ownedAlbums.clear()
                        joinedAlbums.clear()

                        result.value.forEach {
                            if(it.ownerId == userId) ownedAlbums.add(it) else joinedAlbums.add(it)
                        }

                        myAlbumsAdapter.notifyDataSetChanged()
                        joinedAlbumsAdapter.notifyDataSetChanged()
                    }
                }
                is Promise.Result.Error -> {
                    println(result.error)
                }
            }
        }
    }

    private fun loadUser() {
        viewModel.getCurrentUser().whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    Authentication.setUser(it.value)
                }
                is Promise.Result.Error -> {
                    println(it.error.message)
                }
            }
        }
    }

    private fun onOwnedAlbumCLick(position: Int) {
        onAlbumCLick(ownedAlbums[position])
    }
    private fun onJoinedAlbumClick(position: Int) {
        onAlbumCLick(joinedAlbums[position])
    }

    private fun onAlbumCLick(album: Album) {
        val bundle: Bundle = bundleOf(Pair("album", album))
        childFragmentManager.commit {
            setReorderingAllowed(true)
            add<AlbumInfoFragment>(R.id.AlbumActionDepthFragmentContainer, "albumAction", args = bundle)
        }
        enableDisableViewGroup(listViews,false)
        enableDisableViewGroup(actionMenuView, false)
    }

    fun resetAlbumInfoSettings() {
        enableDisableViewGroup(listViews,false)
        enableDisableViewGroup(actionMenuView, false)
        enableDisableViewGroup(sideBar, true)
    }

}