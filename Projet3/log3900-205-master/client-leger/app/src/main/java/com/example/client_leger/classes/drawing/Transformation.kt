package com.example.client_leger.classes.drawing

class Transformation(
    var dx: Float,
    var dy: Float,
    var dw: Float,
    var dh: Float,
    var flipX: Boolean = false,
    var flipY: Boolean = false
)