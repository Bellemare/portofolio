package com.example.client_leger.classes.sockets

import com.example.client_leger.classes.drawing.DrawingEllipseEvent

class DrawingEllipseEventPayload(
    override val event: DrawingEllipseEvent,
    userId: Int,
    user: String,
) : DrawingEventPayload(event, userId, user) {}
class SocketDrawingEllipseEventPayload(type: Int, payload: DrawingEllipseEventPayload): SocketPayload(type, payload)
