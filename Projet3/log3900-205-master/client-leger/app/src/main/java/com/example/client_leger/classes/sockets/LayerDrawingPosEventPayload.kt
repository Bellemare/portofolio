package com.example.client_leger.classes.sockets

import com.example.client_leger.classes.drawing.DrawingPosEvent

class LayerDrawingPosEventPayload(
    override val event: DrawingPosEvent,
    id: Int,
    layer: Int,
    type: Int,
    userId: Int? = null,
    user: String? = null,
): LayerEventPayload(event, id, layer, type, userId, user) {}
class SocketLayerDrawingPosEventPayload(type: Int, payload: LayerDrawingPosEventPayload): SocketPayload(type, payload)
