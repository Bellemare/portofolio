package com.example.client_leger.classes.apiEvents

data class DrawingExpositionAddRequest(val drawing_id: Int)
