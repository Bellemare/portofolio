package com.example.client_leger.fragments.chat

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.client_leger.R
import com.example.client_leger.classes.User
import com.example.client_leger.classes.chat.ChatRoomMemberListAdapter
import com.example.client_leger.classes.chat.RoomListAddAdapter
import com.example.client_leger.classes.chat.RoomListSwitchAdapter
import com.example.client_leger.classes.chat.memberSelectedListener
import com.example.client_leger.services.RoomService
import com.example.client_leger.viewModels.ChatViewModel
import com.shopify.promises.Promise

class ChatMessageToFragment : Fragment() {

    private lateinit var adapter: ChatRoomMemberListAdapter
    lateinit var listview: RecyclerView
    private lateinit var viewModel: ChatViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(requireActivity()).get(ChatViewModel::class.java)
        val userList = ArrayList<User>()
        adapter = ChatRoomMemberListAdapter(userList, viewModel, parentFragment as memberSelectedListener)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_chat_message_to, container, false)
        listview = view.findViewById(R.id.memberList)
        listview.adapter = adapter
        listview.layoutManager = LinearLayoutManager(context)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setMemberList()
    }

    fun setMemberList(){
        if (viewModel.getCurrentRoom().name =="Session collaborative"){
            val users = ArrayList<User>()
            val clients = RoomService.getClientSyncs()
            for ( client in clients){
                val user = User(client.userId,"",client.user,false,client.avatar,false)
                users.add(user)
            }
            adapter.setList(users)
        } else {
            viewModel.fetchRoomMembers().whenComplete {
                when (it) {
                    is Promise.Result.Success -> {
                        adapter.setList(it.value)
                    }
                    is Promise.Result.Error -> {
                        println(it.error)
                    }
                }
            }
        }
    }

    fun filterList(searchTerm: String){
        adapter.filterList(searchTerm)
    }
}