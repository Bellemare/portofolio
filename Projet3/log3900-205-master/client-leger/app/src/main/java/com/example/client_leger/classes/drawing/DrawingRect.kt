package com.example.client_leger.classes.drawing

import com.example.client_leger.classes.Vec2
import com.example.client_leger.classes.toolProperties.ToolProperties
import com.example.client_leger.services.drawingServices.TextDrawingService
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.Gson
import kotlin.math.abs
import kotlin.math.min

@JsonIgnoreProperties(ignoreUnknown = true)
class DrawingRect(
    var x: Float,
    var y: Float,
    var width: Float,
    var height: Float,
    text: DrawingText? = null,
) : DrawingShape(text) {
    override fun deepCopy(): DrawingElement {
        val parser = Gson()
        val copy =  parser.fromJson(parser.toJson(this), DrawingRect::class.java)
        copyBoundingBox(copy)
        return copy
    }

    override fun editBoundingBox(): BoundingBox? {
        val boundingBox = boundingBox
        if (boundingBox == null) {
            return null
        }

        return BoundingBox(
            boundingBox.x,
            boundingBox.y,
            boundingBox.w,
            abs(height)
        )
    }

    override fun generateTextLines(attributes: ToolProperties): Array<String> {
        if (text == null || text!!.text.isEmpty()) {
            return arrayOf()
        }

        val dataLines = text!!.text.split("\n")
        val maxWidth = width - (attributes.lineWidth * 2) - TextDrawingService.PADDING.x * 2
        val lines: ArrayList<String> = arrayListOf()
        for (line in dataLines) {
            val newLine = getNewTextLine(line) {
                return@getNewTextLine maxWidth
            }
            lines.add(newLine)
        }

        return lines.toTypedArray()
    }

    override fun getTopLeftPosition(attributes: ToolProperties): Vec2 {
        return Vec2(min(x, x + width), min(y, y + height))
    }

    override fun applyScaling(dw: Float, dh: Float, flipX: Boolean, flipY: Boolean, attributes: ToolProperties): Boolean {
        if (dw == 0.0f || dh == 0.0f) {
            return false
        }

        super.applyScaling(dw, dh, flipX, flipY, attributes)
        width = abs(width * dw)
        height = abs(height * dh)
        return true
    }

    override fun applyTranslation(dx: Float, dy: Float) {
        super.applyTranslation(dx, dy)
        x += dx
        y += dy
    }
}
