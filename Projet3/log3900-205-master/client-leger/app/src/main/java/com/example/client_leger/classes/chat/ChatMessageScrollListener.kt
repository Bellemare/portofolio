package com.example.client_leger.classes.chat

import androidx.recyclerview.widget.RecyclerView

class ChatMessageScrollListener(val onScrollHandler: (Boolean) -> Unit): RecyclerView.OnScrollListener() {
    override fun onScrollStateChanged(recyclerView: RecyclerView, scrollState: Int) {
        if (scrollState == 0)
            return

        onScrollHandler(!recyclerView.canScrollVertically(1))
    }
}