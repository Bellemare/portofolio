package com.example.client_leger.classes.drawing

import com.example.client_leger.classes.Color
import com.example.client_leger.classes.toolProperties.ToolProperties

open class DrawingEvent(
    open val data: DrawingElement,
    open val attributes: ToolProperties,
    var type: Int? = null,
    var toolType: Int,
    var drawingType: Int,
    var strokeColor: Color,
    var fillColor: Color,
    var textColor: Color,
    var id: Int? = null,
) {
    fun deepCopy(): DrawingEvent {
        return DrawingEvent(data.deepCopy(), attributes, type, toolType, drawingType, strokeColor, fillColor, textColor, id)
    }
}