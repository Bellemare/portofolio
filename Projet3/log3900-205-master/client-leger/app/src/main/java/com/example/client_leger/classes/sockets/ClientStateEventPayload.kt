package com.example.client_leger.classes.sockets

class ClientStateEventPayload(val state: Int, val user: String, val userId: Int, val avatar: String): Payload()
class SocketClientStateEventPayload(type: Int, payload: ClientStateEventPayload): SocketPayload(type, payload)
