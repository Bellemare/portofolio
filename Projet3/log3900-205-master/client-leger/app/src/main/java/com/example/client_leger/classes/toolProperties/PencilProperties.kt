package com.example.client_leger.classes.toolProperties

import com.example.client_leger.classes.drawing.DrawingType

data class PencilProperties(
    override var drawingType: DrawingType,
    override var lineWidth : Int
) : ToolProperties() {
    override fun deepCopy(): ToolProperties {
        return PencilProperties(drawingType, lineWidth)
    }
}