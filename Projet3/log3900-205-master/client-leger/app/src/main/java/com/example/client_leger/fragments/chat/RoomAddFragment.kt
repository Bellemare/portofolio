package com.example.client_leger.fragments.chat

import android.app.Notification
import android.content.Context
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.client_leger.R
import com.example.client_leger.classes.RequestFieldErrorResponse
import com.example.client_leger.classes.RequestResponse
import com.example.client_leger.classes.chat.JoinRoomListener
import com.example.client_leger.classes.chat.RoomListAddAdapter
import com.example.client_leger.classes.apiEvents.Room
import com.example.client_leger.services.RoomService
import com.example.client_leger.viewModels.ChatViewModel
import com.shopify.promises.Promise
import java.lang.Exception
import java.nio.charset.Charset

class RoomAddFragment: Fragment(), JoinRoomListener {

    private lateinit var viewModel: ChatViewModel
    private var roomArray = ArrayList<Room>()
    private lateinit var adapter: RoomListAddAdapter
    lateinit var listview: RecyclerView
    private lateinit var joinRequestBtn : Button
    private lateinit var joinForm : LinearLayout
    private lateinit var searchBar : EditText
    private lateinit var searchBtn : Button
    private lateinit var createRequestBtn : Button
    private lateinit var createForm : LinearLayout
    private lateinit var nameForm : EditText
    private lateinit var confirmCreateBtn : Button
    private lateinit var returnToChatBtn : Button



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_room_add, container, false)
        listview = view.findViewById(R.id.roomListJoin)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(requireActivity()).get(ChatViewModel::class.java)
        joinRequestBtn = (view as View).findViewById(R.id.joinRequestButton)
        joinForm = (view as View).findViewById(R.id.joinForm)
        searchBar = (view as View).findViewById(R.id.searchBar)
        searchBtn = (view as View).findViewById(R.id.searchBtn)
        createRequestBtn = (view as View).findViewById(R.id.createRequestButton)
        createForm = (view as View).findViewById(R.id.createForm)
        nameForm = (view as View).findViewById(R.id.nameForm)
        nameForm.setOnKeyListener { view, i, keyEvent ->
            if(keyEvent.action == KeyEvent.ACTION_DOWN) {
                when(i) {
                    KeyEvent.KEYCODE_ENTER ->  (view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(view.windowToken, 0)
                }
            }
            true
        }
        confirmCreateBtn = (view as View).findViewById(R.id.createRoomBtn)
        returnToChatBtn = (view as View).findViewById(R.id.returnToChatBtn)

        adapter = RoomListAddAdapter(roomArray, this)
        listview.adapter = adapter
        listview.layoutManager = LinearLayoutManager(context)

        joinRequestBtn.setOnClickListener { showJoinForm() }
        createRequestBtn.setOnClickListener { showCreateForm() }
        returnToChatBtn.setOnClickListener { returnToChat() }
        fetchAvailableRooms()


    }

    private  fun showCreateForm() {
        createForm.visibility = View.VISIBLE
        joinForm.visibility = View.GONE
        confirmCreateBtn.setOnClickListener { onCreateClick() }

    }

    private fun showJoinForm() {
        joinForm.visibility = View.VISIBLE
        createForm.visibility = View.GONE
        searchBtn.setOnClickListener { onSearchClick() }
    }

    private fun fetchAvailableRooms(){
        viewModel.fetchAllRooms().whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    val activeRooms = RoomService.getActiveRooms()
                    for(room in it.value){
                        if (!activeRooms.containsKey(room.id))
                            adapter.setRoom(room)
                    }
                }
                is Promise.Result.Error -> {
                    println(it.error)
                }
            }
        }
    }

    private fun joinRoom(room: Room) {
        viewModel.joinRoom(room.id as Int).whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    val roomAddFragment = parentFragmentManager.findFragmentByTag("RoomAddFragment")
                    val chatWindowFragment = roomAddFragment?.parentFragment as ChatWindowFragment

                    chatWindowFragment.updateChat(room)
                    returnToChat()
                }
                is Promise.Result.Error -> {
                    println(it.error)
                }

            }
        }
    }

    override fun onJoinClick(room: Room){
        joinRoom(room)
    }

    fun onCreateClick(){
        var name = nameForm.text.toString()
        if (name == ""){
            return
        }
        viewModel.createRoom(name).whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    joinRoom(it.value)

                    val roomAddFragment = parentFragmentManager.findFragmentByTag("RoomAddFragment")
                    val chatWindowFragment = roomAddFragment?.parentFragment as ChatWindowFragment
                    chatWindowFragment.updateChat(it.value)
                    returnToChat()
                }
                is Promise.Result.Error -> {
                    try {
                        var errorString = it.error.errorData.toString(Charset.defaultCharset())
                        val error = RequestFieldErrorResponse.Deserializer().deserialize(errorString)
                        error.displayError(view as View)
                    }
                    catch (e: Exception) { }
                }
            }

        }

    }

    private fun onSearchClick(){
        adapter.clear()
        var searchTerm = searchBar.text.toString()

        if (searchTerm == "")
            return fetchAvailableRooms()

        viewModel.fetchRoomsByName(searchTerm).whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    adapter.clear()
                    val activeRooms = RoomService.getActiveRooms()
                    for(room in it.value){
                        if (!activeRooms.containsKey(room.id))
                            adapter.setRoom(room)
                    }
                }
                is Promise.Result.Error -> {
                    println(it.error)
                }
            }
        }

    }

    private fun returnToChat(){
        val roomAddFragment = parentFragmentManager.findFragmentByTag("RoomAddFragment")

        parentFragmentManager.commit {
            setReorderingAllowed(true)
            remove(roomAddFragment as Fragment)
        }
    }
}