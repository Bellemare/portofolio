package com.example.client_leger.fragments

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.add
import androidx.fragment.app.commit
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.client_leger.R
import com.example.client_leger.classes.*
import com.example.client_leger.classes.sockets.PayloadTypes
import com.example.client_leger.services.RoomService
import com.example.client_leger.viewModels.AlbumMenuViewModel
import com.github.kittinunf.fuel.core.FuelError
import com.shopify.promises.Promise
import java.nio.charset.Charset
import java.util.*
import kotlin.collections.ArrayList

class AlbumInfoFragment : Fragment(), ViewDisabler {
    private val viewModel: AlbumMenuViewModel by activityViewModels()
    private lateinit var mainView: LinearLayout
    private lateinit var noDrawingsMessage: TextView

    private lateinit var exitBtn: TextView
    private lateinit var addDrawingBtn: Button
    private lateinit var editAlbumBtn: Button
    private lateinit var deleteAlbumBtn: Button
    private lateinit var quitAlbumBtn: Button

    private lateinit var searchTerm: EditText
    private lateinit var searchButton: Button

    private lateinit var albumName: TextView
    private lateinit var albumDescription: TextView

    private lateinit var drawingListView: RecyclerView
    private lateinit var drawingListAdapter: DrawingListAdapter
    private var drawings = ArrayList<Drawing>()
    private var displayedDrawings = ArrayList<Drawing>()

    private lateinit var album: Album
    private lateinit var user: User

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =inflater.inflate(R.layout.fragment_album_info, container, false)
        album = requireArguments().get("album") as Album
        user = Authentication.getUser()!!
        mainView = view.findViewById(R.id.mainView)
        noDrawingsMessage = view.findViewById(R.id.noDrawingMsg)

        setUpExit(view)
        setUpSearch(view)
        setUpDrawingsListView(view)
        setUpDrawingCreation(view)
        setupAlbumInfo(view)
        setUpAlbumAction(view)
        listenToDrawingChange()

        fetchDrawings()

        return view
    }

    override fun onStop() {
        super.onStop()
        unregisterListenerToDrawingChange()
    }

    private fun unregisterListenerToDrawingChange() {
        RoomService.getAppClient().whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    it.value.onMessage(PayloadTypes.DRAWING_CHANGE) { }
                }
                is Promise.Result.Error -> {
                    println(it.error)
                }
            }
        }
    }

    private fun listenToDrawingChange() {
        RoomService.getAppClient().whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    it.value.onMessage(PayloadTypes.DRAWING_CHANGE) { payload ->
                        Handler(Looper.getMainLooper()).post {
                            fetchDrawings()
                        }
                    }

                }
                is Promise.Result.Error -> {
                    println(it.error)
                }
            }
        }
    }

    private fun setUpAlbumAction(view: View) {
        editAlbumBtn = view.findViewById(R.id.editAlbumBtn)
        deleteAlbumBtn = view.findViewById(R.id.deleteAlbum)
        quitAlbumBtn = view.findViewById(R.id.quitAlbumBtn)

        editAlbumBtn.setOnClickListener {
            val bundle: Bundle = bundleOf(Pair("album", album))
            if(childFragmentManager.findFragmentByTag("albumInfoAction") == null){
                childFragmentManager.commit {
                    setReorderingAllowed(true)
                    add<AlbumEditFragment>(R.id.AlbumInfoActionFragmentContainer, "albumInfoAction", args = bundle)
                }
                enableDisableViewGroup(mainView, false)
                (parentFragment as AlbumMenuFragment).enableDisableAlbumMenuView(false)
            }
        }
        deleteAlbumBtn.setOnClickListener {
            val bundle: Bundle = bundleOf(Pair("album", album))
            if(childFragmentManager.findFragmentByTag("albumInfoAction") == null){
                childFragmentManager.commit {
                    setReorderingAllowed(true)
                    add<AlbumDeleteFragment>(R.id.AlbumInfoActionFragmentContainer, "albumInfoAction", args = bundle)
                }
                enableDisableViewGroup(mainView, false)
                (parentFragment as AlbumMenuFragment).enableDisableAlbumMenuView(false)
            }
        }
        quitAlbumBtn.setOnClickListener {
            val bundle: Bundle = bundleOf(Pair("album", album))
            if(childFragmentManager.findFragmentByTag("albumInfoAction") == null){
                childFragmentManager.commit {
                    setReorderingAllowed(true)
                    add<AlbumLeaveFragment>(R.id.AlbumInfoActionFragmentContainer, "albumInfoAction", args = bundle)
                }
                enableDisableViewGroup(mainView, false)
                (parentFragment as AlbumMenuFragment).enableDisableAlbumMenuView(false)
            }
        }
        if(album.id == 0){
            editAlbumBtn.visibility = View.INVISIBLE
            deleteAlbumBtn.visibility = View.GONE
            quitAlbumBtn.visibility = View.INVISIBLE
        }
        else {
            if(album.ownerId != user.id) {
                editAlbumBtn.visibility = View.INVISIBLE
                deleteAlbumBtn.visibility = View.GONE
                quitAlbumBtn.visibility = View.VISIBLE
            }
            else {
                quitAlbumBtn.visibility = View.GONE
            }
        }
    }

    private fun setupAlbumInfo(view: View) {
        albumName = view.findViewById(R.id.albumName)
        albumDescription = view.findViewById(R.id.albumDescription)

        albumName.text = album.name
        albumDescription.text = album.description
    }

    private fun setUpDrawingCreation(view: View) {
        addDrawingBtn = view.findViewById(R.id.addDrawingBtn)
        addDrawingBtn.setOnClickListener {
            val albums = arrayListOf<Album>(album)

            val bundle: Bundle = bundleOf(Pair("albums", albums),
                                        Pair("isInsideAlbum", true),
                                        Pair("mode", DrawingCreationFragment.DrawingMode.CREATE))

            if(childFragmentManager.findFragmentByTag("albumInfoAction") == null){
                childFragmentManager.commit {
                    setReorderingAllowed(true)
                    add<DrawingCreationFragment>(R.id.AlbumInfoActionFragmentContainer, "albumInfoAction", args = bundle)
                }
                enableDisableViewGroup(mainView, false)
                (parentFragment as AlbumMenuFragment).enableDisableAlbumMenuView(false)
            }
        }
    }

    fun removeActionFragment() {
        val albumActionFragment = childFragmentManager.findFragmentByTag("albumInfoAction")
        if(albumActionFragment != null) {
            childFragmentManager.commit {
                setReorderingAllowed(true)
                remove(albumActionFragment)
            }
        }
        enableDisableViewGroup(mainView, true)
        (parentFragment as AlbumMenuFragment).resetAlbumInfoSettings()

        fetchDrawings()
    }

    fun returnToMainMenu() {
        (parentFragment as AlbumMenuFragment).removeAlbumActionFragment()
    }

    private fun setUpDrawingsListView(view: View) {
        drawingListView = view.findViewById(R.id.drawingsListView)
        drawingListAdapter = DrawingListAdapter(displayedDrawings, user.id, if(album.id == 0) DrawingListAdapter.ListType.PublicAlbum else DrawingListAdapter.ListType.PrivateAlbum,
                                                { position -> onDrawingJoin(position) },
                                                { position -> onDrawingVisualize(position) },
                                                { position -> onDrawingEdit(position) },
                                                { position -> onDrawingDelete(position) },
                                                { position: Int, isExposed: Boolean -> onDrawingExpose(position, isExposed) },
                                                { position, isPasswordProtected -> onDrawingPasswordClick(position, isPasswordProtected) })
        drawingListView.adapter = drawingListAdapter
        drawingListView.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
    }

    private fun onDrawingDelete(position: Int) {
        val drawing = displayedDrawings[position]
        val bundle: Bundle = bundleOf(Pair("drawing", DrawingModel(drawing.name, drawing.album_id, drawing.id, null)))

        if(childFragmentManager.findFragmentByTag("albumInfoAction") == null) {
            childFragmentManager.commit {
                setReorderingAllowed(true)
                add<DrawingDeleteFragment>(R.id.AlbumInfoActionFragmentContainer, "albumInfoAction", args = bundle)
            }
            enableDisableViewGroup(mainView, false)
            (parentFragment as AlbumMenuFragment).enableDisableAlbumMenuView(false)
        }
    }

    private fun onDrawingPasswordClick(position: Int, isPasswordProtected: Boolean) {
        val drawing = drawings[position]
        if(isPasswordProtected) {
            //setPasswordFrag
            val bundle = bundleOf(Pair("drawing", DrawingModel(drawing.name, drawing.album_id, drawing.id, null)))
            if(childFragmentManager.findFragmentByTag("albumInfoAction") == null){
                childFragmentManager.commit {
                    setReorderingAllowed(true)
                    add<DrawingPasswordEditFragment>(R.id.AlbumInfoActionFragmentContainer, "albumInfoAction", args = bundle)
                }
                enableDisableViewGroup(mainView, false)
                (parentFragment as AlbumMenuFragment).enableDisableAlbumMenuView(false)
            }
        }
        else {
            //removePassword
            val editedDrawingModel = DrawingModel(drawing.name, drawing.album_id, drawing.id, "")
            viewModel.updateDrawing(editedDrawingModel).whenComplete { result: Promise.Result<RequestResponse, FuelError> ->
                when (result) {
                    is Promise.Result.Success -> {
                        val requestResponse: RequestResponse = result.value
                        requestResponse.displayResponse(requireView())
                    }
                    is Promise.Result.Error -> {
                        //handle error
                        val error = RequestFieldErrorResponse.Deserializer()
                            .deserialize(result.error.errorData.toString(Charset.defaultCharset()))
                        error.displayError(requireView())
                    }
                }
            }
        }
    }

    private fun onDrawingEdit(position: Int) {
        var albums: ArrayList<Album>

        viewModel.fetchUserAlbums().whenComplete { result ->
            when(result) {
                is Promise.Result.Success -> {
                    albums = result.value
                    albums.remove(albums.find { it.id == 0 })
                    val drawing = displayedDrawings[position]
                    val bundle: Bundle = bundleOf(
                        Pair("albums", albums),
                        Pair("mode", DrawingCreationFragment.DrawingMode.EDIT),
                        Pair("drawing", DrawingModel(drawing.name, drawing.album_id, drawing.id, null)))

                    if(childFragmentManager.findFragmentByTag("albumInfoAction") == null){
                        childFragmentManager.commit {
                            setReorderingAllowed(true)
                            add<DrawingCreationFragment>(R.id.AlbumInfoActionFragmentContainer, "albumInfoAction", args = bundle)
                        }
                        enableDisableViewGroup(mainView, false)
                        (parentFragment as AlbumMenuFragment).enableDisableAlbumMenuView(false)
                    }
                }
                is Promise.Result.Error -> {
                    //handle error
                    val error = RequestFieldErrorResponse.Deserializer().deserialize(result.error.errorData.toString(
                        Charset.defaultCharset()))
                    error.displayError(mainView)
                }
            }
        }
    }

    private fun onDrawingExpose(position: Int, isExposed: Boolean) {
        val drawing = displayedDrawings[position]
        if(isExposed) {
            viewModel.addDrawingToExposition(drawing.album_id, drawing.id).whenComplete { result ->
                when(result) {
                    is Promise.Result.Success -> {
                        val requestResponse: RequestResponse = result.value
                        requestResponse.displayResponse(requireView())
                    }
                    is Promise.Result.Error -> {
                        val error = RequestResponse.Deserializer().deserialize(result.error.errorData.toString(
                            Charset.defaultCharset()))
                        error.displayResponse(requireView())
                    }
                }
            }
        }
        else {
            viewModel.removeDrawingFromExposition(drawing.album_id, drawing.id).whenComplete { result ->
                when(result) {
                    is Promise.Result.Success -> {
                        val requestResponse: RequestResponse = result.value
                        requestResponse.displayResponse(requireView())
                    }
                    is Promise.Result.Error -> {
                        val error = RequestResponse.Deserializer().deserialize(result.error.errorData.toString(
                            Charset.defaultCharset()))
                        error.displayResponse(requireView())
                    }
                }
            }
        }
    }

    private fun onDrawingVisualize(position: Int) {
        //GIF ??
        if(displayedDrawings.lastIndex < position){
            return
        }
        val drawing: Drawing = displayedDrawings[position]
        val bundle: Bundle = bundleOf(Pair("drawing", drawing.image))

        if(childFragmentManager.findFragmentByTag("albumInfoAction") == null){
            childFragmentManager.commit {
                setReorderingAllowed(true)
                add<DrawingVisualizeFragment>(R.id.AlbumInfoActionFragmentContainer, "albumInfoAction", args = bundle)
            }
            enableDisableViewGroup(mainView, false)
            (parentFragment as AlbumMenuFragment).enableDisableAlbumMenuView(false)
        }
    }

    private fun onDrawingJoin(position: Int) {
        val drawing = displayedDrawings.getOrNull(position)
        if(drawing != null) {
            if(drawing.isPasswordProtected && drawing.owner_id != user.id) {
                if(childFragmentManager.findFragmentByTag("albumInfoAction") == null) {
                    val bundle = bundleOf(Pair("drawing", DrawingModel(drawing.name, drawing.album_id, drawing.id, null)))
                    childFragmentManager.commit {
                        setReorderingAllowed(true)
                        add<DrawingPasswordVerifyFragment>(R.id.AlbumInfoActionFragmentContainer, "albumInfoAction", args = bundle)
                    }
                    enableDisableViewGroup(mainView, false)
                    (parentFragment as AlbumMenuFragment).enableDisableAlbumMenuView(false)
                }
            }
            else {
                val bundle = bundleOf(Pair("drawingId",drawing.id), Pair("drawingName", drawing.name))
                Navigation.findNavController(mainView).navigate(R.id.action_albumMenuFragment_to_drawingRoomFragment, args = bundle)
            }
        }
    }

    private fun setUpSearch(view: View) {
        searchTerm = view.findViewById(R.id.searchDrawing)
        searchTerm.setOnKeyListener { _, i, keyEvent ->
            if(keyEvent.action == KeyEvent.ACTION_DOWN) {
                when(i) {
                    KeyEvent.KEYCODE_ENTER -> filterDrawings(view)
                }
            }
            true
        }

        searchButton = view.findViewById(R.id.searchBtn)

        searchButton.setOnClickListener {
            filterDrawings(view)
        }
    }

    private fun filterDrawings(view: View) {
        if(searchTerm.text.toString().isNotEmpty()) {
            viewModel.drawingSmartFilter(searchTerm.text.toString(), album.id).whenComplete { result ->
                when(result) {
                    is Promise.Result.Success -> {
                        displayedDrawings.clear()
                        displayedDrawings.addAll(result.value)
                        drawingListAdapter.notifyDataSetChanged()

                        (view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(view.windowToken, 0)
                    }
                    is Promise.Result.Error -> {
                        drawingListAdapter.notifyDataSetChanged()
                    }
                }
            }
        }
        else {
            drawingListAdapter.notifyDataSetChanged()
        }
    }

    private fun setUpExit(view: View) {
        exitBtn = view.findViewById(R.id.exitBtn)
        exitBtn.setOnClickListener {
            returnToMainMenu()
        }
    }

    private fun fetchDrawings() {
        viewModel.getAlbumDrawings(album.id).whenComplete { result ->
            when (result) {
                is Promise.Result.Success -> {
                    drawings.clear()
                    displayedDrawings.clear()
                    drawings.addAll(result.value)
                    displayedDrawings.addAll(result.value)

                    if(drawings.size == 0) {
                        drawingListView.visibility = View.GONE
                        enableDisableViewGroup(drawingListView, false)
                        noDrawingsMessage.visibility = View.VISIBLE
                    }
                    else {
                        drawingListView.visibility = View.VISIBLE
                        enableDisableViewGroup(drawingListView, true)
                        noDrawingsMessage.visibility = View.GONE
                    }

                    if (view != null) {
                        filterDrawings(requireView())
                    }
                }
                is Promise.Result.Error -> {
                    println(result.error)
                }
            }
        }
    }
}