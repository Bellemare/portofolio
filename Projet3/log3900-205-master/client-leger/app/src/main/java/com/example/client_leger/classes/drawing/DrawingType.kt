package com.example.client_leger.classes.drawing

enum class DrawingType(val value: Int) {
    Stroke(0),
    Filled(1),
    StrokeFilled(2),
}