package com.example.client_leger.viewModels

import androidx.lifecycle.*
import com.example.client_leger.classes.RequestResponse
import com.example.client_leger.classes.User
import com.example.client_leger.classes.chat.RoomFetchHistoryRequest
import com.example.client_leger.classes.apiEvents.Room
import com.example.client_leger.classes.chat.MessageTypeConst
import com.example.client_leger.classes.sockets.*
import com.example.client_leger.services.RoomService
import com.example.client_leger.services.UserService
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.result.Result
import com.shopify.promises.Promise
import kotlinx.coroutines.launch

class ChatViewModel: ViewModel() {
    val FETCH_LIMIT = 50
    val GENERAL_ROOM = Room(0, "Général")
    val DRAWING_ROOM = Room(-1, "Session collaborative")

    private var unreadMessages = false
    lateinit var lastWindowName: CharSequence

    private var messageType = MessageTypeConst.DEFAULT_INT
    private var messageToReplyTo = MessagePayload(message = "")
    private var messageRecipient = ArrayList<Int>()
    private var roomMembers = ArrayList<User>()
    private var allUsers = ArrayList<User>()
    private lateinit var currentUser: User


    var BASE_ROOM = Room(0, "Général")
    private var currentRoom: MutableLiveData<Room> = MutableLiveData<Room>(BASE_ROOM)

    private var messages: HashMap<Int, MessagePayload> = HashMap()

    private var currentHistoryPage = 0
    var socket: SocketClient? = null
    private var newMessageHandler: ((MessagePayload) -> Unit)? = null

    fun setHandler(handler: (MessagePayload) -> Unit) {
        newMessageHandler = handler
    }

    fun setUnreadMessage(bool: Boolean) {
        unreadMessages = bool
    }

    fun getUnreadMessage(): Boolean {
        return unreadMessages
    }

    fun currentUser(): User {
        return currentUser
    }

    fun deleteRoom(roomId: Int): Promise<RequestResponse, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = RoomService.deleteRoom(roomId)
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun loadBaseRoom() {
        if (RoomService.getDrawingRoomConnected()) {
            currentRoom = MutableLiveData<Room>(DRAWING_ROOM)
            return
        }
        currentRoom = MutableLiveData<Room>(GENERAL_ROOM)
    }

    fun changeRoom(room: Room) {
        currentRoom.value = room
        currentHistoryPage = 0;
    }

    fun getCurrentRoom(): Room {
        return currentRoom.value as Room
    }

    fun getMessageType(): Int {
        return messageType
    }

    fun getMessageToReplyTo(): MessagePayload {
        return messageToReplyTo
    }

    fun setMessageToReplyTo(message: MessagePayload) {
        messageToReplyTo = message
    }

    fun sendMessage(message: String) {
        if (message.isNotBlank()) {
            socket?.send(
                MessagePayload(
                    message = message,
                    message_type = messageType,
                    reply_to = messageToReplyTo.id,
                    reply_message = messageToReplyTo.message,
                    recipients = messageRecipient.toTypedArray()
                ), PayloadTypes.MESSAGE
            )
        }
    }

    fun fetchHistory(): Promise<ArrayList<MessagePayload>, FuelError> {
        return Promise {
            if (currentRoom.value?.name == "Session collaborative") {
                resolve(RoomService.getDrawingRoomMessageHistory())
                onHistoryLoad(RoomService.getDrawingRoomMessageHistory())
            } else {
                viewModelScope.launch {
                    val offset = currentHistoryPage++ * FETCH_LIMIT
                    val res = RoomService.getFetchHistory(
                        RoomFetchHistoryRequest(
                            (currentRoom.value as Room).id as Int,
                            FETCH_LIMIT,
                            offset
                        )
                    )
                    when (res) {
                        is Result.Success -> {
                            onHistoryLoad(res.value)
                            resolve(res.value)
                        }
                        is Result.Failure -> {
                            reject(res.error)
                        }
                    }
                }
            }
        }
    }


    fun fetchJoinedRooms(): Promise<ArrayList<Room>, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = RoomService.fetchJoinedRooms()
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun fetchAllRooms(): Promise<ArrayList<Room>, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = RoomService.fetchAllRooms()
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun fetchRoomsByName(roomName: String): Promise<ArrayList<Room>, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = RoomService.fetchRoomsByName(roomName)
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun createRoom(roomName: String): Promise<Room, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = RoomService.createRoom(roomName)
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun connectToChat(): Promise<Unit, Unit> {
        if (currentRoom.value?.name == "Session collaborative") {
            return connectToDrawingChat()
        }

        return Promise {
            currentHistoryPage = 0;
            RoomService.connectRoom((currentRoom.value as Room).id as Int).whenComplete {
                when (it) {
                    is Promise.Result.Success -> {
                        socket = it.value
                        listenSocketMessage()
                        resolve(Unit)
                    }
                    is Promise.Result.Error -> {
                        reject(Unit)
                    }
                }
            }
        }
    }

    fun leaveRoom(roomId: Int): Promise<RequestResponse, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = RoomService.leaveRoom(roomId)
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun joinRoom(roomId: Int): Promise<RequestResponse, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = RoomService.joinRoom(roomId)
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun closeConnection() {
        if (currentRoom.value?.name == "Session collaborative") {
            socket?.onMessage(PayloadTypes.MESSAGE) { payload: Payload ->
                RoomService.addDrawingRoomMessageHistory(payload as MessagePayload)
            }
        } else {
            socket?.closeConnection()
            socket = null
        }
    }

    private fun listenSocketMessage() {
        socket?.onMessage(PayloadTypes.MESSAGE) { payload: Payload ->
            setUnreadMessage(true)

            if (currentRoom.value?.name == "Session collaborative") {
                RoomService.addDrawingRoomMessageHistory(payload as MessagePayload)
            }

            val handler = newMessageHandler
            if (handler != null) {
                handler(payload as MessagePayload)
                saveMessage(payload.id as Int, payload)
            }

        }
    }

    private fun connectToDrawingChat(): Promise<Unit, Unit> {
        return Promise {
            socket = RoomService.getDrawingClient()
            listenSocketMessage()
            resolve(Unit)
        }
    }

    fun setMessageType(messageType: Int) {
        this.messageType = messageType
    }

    fun saveMessage(messageId: Int, message: MessagePayload) {
        if (messages.containsKey(messageId))
            return
        messages.put(messageId, message)
        fetchRoomMembers()
    }

    fun getMessageById(messageId: Int): MessagePayload? {
        return messages.get(messageId)
    }

    private fun onHistoryLoad(value: ArrayList<MessagePayload>) {
        for (message in value) {
            saveMessage(message.id as Int, message)
        }
    }

    fun fetchRoomMembers(): Promise<ArrayList<User>, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = RoomService.fetchRoomMembers(currentRoom.value?.id as Int)
                when (res) {
                    is Result.Success -> {
                        roomMembers = res.value
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun fetchAllUsers() {
        viewModelScope.launch {
            val res = RoomService.fetchRoomMembers(0)
            when (res) {
                is Result.Success -> {
                    allUsers = res.value
                }
                is Result.Failure -> {
                }
            }
        }
    }

    fun parseTags(tags: ArrayList<String>) {
        var temp = ArrayList<Int>()
        for (member in roomMembers) {
            if (tags.contains(member.pseudonym))
                temp.add(member.id)
        }
        messageRecipient = temp
    }

    fun getUserAvatar(id: Int): String {
        for (member in allUsers)
            if (member.id == id)
                return member.avatar
        return ""
    }

    fun setCurrentUser() {
        viewModelScope.launch {
            val res = UserService.getCurrentUser()
            when (res) {
                is Result.Success -> {
                    currentUser = res.value
                }
                is Result.Failure -> {
                    println(res.error)
                }
            }

        }
    }
}

