package com.example.client_leger.classes.sockets

import com.example.client_leger.classes.drawing.DrawingEvent

class DrawingHistoryPayload(
    val events: ArrayList<DrawingEvent>,
): Payload() {}
class SocketDrawingHistoryPayload(type: Int, payload: DrawingHistoryPayload): SocketPayload(type, payload)
