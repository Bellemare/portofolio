package com.example.client_leger.classes

data class RegisterRequest(val email: String, val password: String, val passwordRepeat: String, val pseudonym: String, val avatar: String)