package com.example.client_leger.classes.apiEvents



import com.example.client_leger.getApiMapper
import com.github.kittinunf.fuel.core.ResponseDeserializable
import kotlin.collections.ArrayList

data class Room(
    val id: Int? = null,
    val name: String? = null,
    val ownerId: Int? = null,
) {
    class ListDeserializer : ResponseDeserializable<ArrayList<Room>> {
        override fun deserialize(content: String) : ArrayList<Room> {
            val mapper = getApiMapper()

            return mapper.readValue(content, mapper.typeFactory.constructCollectionType(ArrayList::class.java, Room::class.java))
        }
    }

    class Deserializer : ResponseDeserializable<Room> {
        override fun deserialize(content: String) : Room {
            val mapper = getApiMapper()

            return mapper.readValue(content, mapper.typeFactory.constructType( Room::class.java))
        }
    }
}

