package com.example.client_leger.fragments.chat

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.commit
import androidx.lifecycle.ViewModelProvider
import com.example.client_leger.R
import com.example.client_leger.classes.sockets.MessagePayload
import com.example.client_leger.viewModels.ChatViewModel


class ReplyToModal : Fragment() {

    private lateinit var viewModel: ChatViewModel
    private lateinit var confirmBtn: Button
    private lateinit var cancelBtn: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_reply_to_modal, container, false)
        confirmBtn = view.findViewById(R.id.confirmBtn)
        cancelBtn = view.findViewById(R.id.cancelBtn)
        viewModel = ViewModelProvider(requireActivity()).get(ChatViewModel::class.java)


        confirmBtn.setOnClickListener { onConfirmClick() }
        cancelBtn.setOnClickListener { onCancelClick() }
        return view
    }

    private fun onConfirmClick(){
        val replyToModalFragment = parentFragmentManager.findFragmentByTag("ReplyToModalFragment")
        val chatWindowFragment = replyToModalFragment?.parentFragment as ChatWindowFragment

        chatWindowFragment.replyToMessage()
        returnToChat()
    }

    private fun onCancelClick(){
        viewModel.setMessageToReplyTo(MessagePayload(message = ""))
        returnToChat()
    }

    private fun returnToChat(){
        val replyToModalFragment = parentFragmentManager.findFragmentByTag("ReplyToModalFragment")
        val chatWindowFragment = replyToModalFragment?.parentFragment as ChatWindowFragment
        chatWindowFragment.enableView()
        parentFragmentManager.commit {
            setReorderingAllowed(true)
            remove(replyToModalFragment as Fragment)
        }
    }






}