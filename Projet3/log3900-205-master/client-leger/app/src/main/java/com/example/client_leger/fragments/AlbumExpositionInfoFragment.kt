package com.example.client_leger.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.add
import androidx.fragment.app.commit
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.client_leger.R
import com.example.client_leger.classes.*
import com.example.client_leger.viewModels.AlbumMenuViewModel
import com.shopify.promises.Promise
import java.util.*
import kotlin.collections.ArrayList

class AlbumExpositionInfoFragment : Fragment(), ViewDisabler {
    private val viewModel: AlbumMenuViewModel by activityViewModels()
    private lateinit var mainView: LinearLayout
    private lateinit var exitBtn: TextView

    private lateinit var albumName: TextView
    private lateinit var albumDescription: TextView

    private lateinit var drawingListView: RecyclerView
    private lateinit var drawingListAdapter: DrawingListAdapter
    private var drawings = ArrayList<Drawing>()

    private lateinit var album: Album
    private lateinit var user: User

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =inflater.inflate(R.layout.fragment_album_exposition_info, container, false)
        album = requireArguments().get("album") as Album
        user = Authentication.getUser()!!
        mainView = view.findViewById(R.id.mainView)

        setUpExit(view)
        setUpDrawingsListView(view)
        setupAlbumInfo(view)

        fetchDrawings()

        return view
    }

    private fun setUpExit(view: View) {
        exitBtn = view.findViewById(R.id.exitBtn)
        exitBtn.setOnClickListener {
            returnToMainMenu()
        }
    }

    private fun setupAlbumInfo(view: View) {
        albumName = view.findViewById(R.id.albumName)
        albumDescription = view.findViewById(R.id.albumDescription)

        albumName.text = album.name
        albumDescription.text = album.description
    }

    fun returnToMainMenu() {
        when(parentFragment) {
            is AlbumExpositionMenuFragment -> {
                (parentFragment as AlbumExpositionMenuFragment).removeAlbumExpositionActionFragment()
            }
            is HistoryExpositionListFragment -> {
                (parentFragment as HistoryExpositionListFragment).removeAlbumExpositionActionFragment()
            }
        }
    }

    private fun setUpDrawingsListView(view: View) {
        drawingListView = view.findViewById(R.id.drawingsListView)
        drawingListAdapter = DrawingListAdapter(drawings, user.id, DrawingListAdapter.ListType.Exposition,
            null,
            { position: Int -> onVisualize(position) },
            null,
            null,
            null,
            null)
        drawingListView.adapter = drawingListAdapter
        drawingListView.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
    }

    private fun fetchDrawings() {
        viewModel.getAlbumExposition(album.id).whenComplete { result ->
            when (result) {
                is Promise.Result.Success -> {
                    drawings.clear()
                    drawings.addAll(result.value)
                    drawingListAdapter.notifyDataSetChanged()

                    if(drawings.size == 0) {
                        drawingListView.visibility = View.GONE
                        enableDisableViewGroup(drawingListView, false)
                    }
                    else {
                        drawingListView.visibility = View.VISIBLE
                        enableDisableViewGroup(drawingListView, true)
                    }

                }
                is Promise.Result.Error -> {
                    println(result.error)
                }
            }
        }
    }

    private fun onVisualize(position: Int) {
        val drawing: Drawing? = drawings.getOrNull(position)
        if(drawing != null) {
            val bundle: Bundle = bundleOf(Pair("drawing", drawing.image))

            if(childFragmentManager.findFragmentByTag("albumInfoAction") == null){
                childFragmentManager.commit {
                    setReorderingAllowed(true)
                    add<DrawingVisualizeFragment>(R.id.AlbumInfoActionFragmentContainer, "albumInfoAction", args = bundle)
                }
                enableDisableViewGroup(mainView, false)
                (parentFragment as AlbumExpositionMenuFragment).enableDisableAlbumMenuView(false)
            }
        }
    }

    fun removeVisualizeFragment() {
        val albumActionFragment = childFragmentManager.findFragmentByTag("albumInfoAction")
        if(albumActionFragment != null) {
            childFragmentManager.commit {
                setReorderingAllowed(true)
                remove(albumActionFragment)
            }
        }
        enableDisableViewGroup(mainView, true)
        (parentFragment as AlbumExpositionMenuFragment).enableDisableAlbumMenuView(true)
    }
}