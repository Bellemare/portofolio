package com.example.client_leger.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.example.client_leger.R
import com.example.client_leger.classes.Authentication
import com.example.client_leger.classes.apiEvents.*

import com.example.client_leger.viewModels.HomeViewModel
import com.shopify.promises.Promise
import kotlinx.coroutines.runBlocking

class HomeFragment : Fragment() {
    private val viewModel: HomeViewModel = HomeViewModel()

    private var eventListenerId: AppEvents.IntWrapper = AppEvents.IntWrapper(0)

    override fun onStop() {
        super.onStop()
        runBlocking {
            AppEvents.unregister(eventListenerId.value, AppEventType.UNAUTHORIZED)
        }
    }

    override fun onStart() {
        super.onStart()
        runBlocking {
            AppEvents.listenTo(AppEventType.UNAUTHORIZED, eventListenerId) { UnauthorizedEvent.handle(requireView(), R.id.action_homeFragment_to_loginFragment) }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        loadUser()

        return view
    }

    private fun loadUser() {
        viewModel.getCurrentUser().whenComplete {
            when (it) {
                is Promise.Result.Success -> {
                    Authentication.setUser(it.value)
                    Navigation.findNavController(requireView()).navigate(R.id.action_homeFragment_to_albumMenuFragment)
                }
                is Promise.Result.Error -> {
                    println(it.error.message)
                }
            }
        }
    }
}