package com.example.client_leger.views

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import com.example.client_leger.services.CanvasSizeService
import com.example.client_leger.services.DrawingService

class CanvasView : View {
    private lateinit var paint : Paint
    private var canvasData: Bitmap? = null

    constructor(context: Context) : super(context, null){
        initCanvasView()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs){
        initCanvasView()
    }

    private fun initCanvasView() {
        isFocusable = true
        isFocusableInTouchMode = true
    }

    fun initCanvas(color: Int) {
        setBackgroundColor(color)
    }

    fun setCanvasData(bitmap: Bitmap) {
        canvasData = bitmap
    }

    override fun onDraw(canvas: Canvas) {
        val data = canvasData
        if (data != null) {
            canvas.drawBitmap(data, Rect(0,0,CanvasSizeService.DEFAULT_WIDTH,CanvasSizeService.DEFAULT_HEIGHT), Rect(0,0,CanvasSizeService.DEFAULT_WIDTH,CanvasSizeService.DEFAULT_HEIGHT), null)
        }
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (DrawingService.disableEvents)
            return false

        when(event.action) {
            MotionEvent.ACTION_DOWN -> {
                DrawingService.getCurrentTool().onActionDown(event)
            }
            MotionEvent.ACTION_MOVE -> {
                DrawingService.getCurrentTool().onActionMove(event)
            }
            MotionEvent.ACTION_UP -> {
                DrawingService.getCurrentTool().onActionUp(event)
            }
        }
        return true
    }
}
