package com.example.client_leger.classes

class Environment: IEnvironment {
    override val production: Boolean
        get() = false

    override val defaultHost: String
        get() = "https://projet3.antoinec.dev"

    override val websocketHost: String
        get() = "wss://projet3.antoinec.dev"

    override val defaultPort: Int
        get() = 80

    override val defaultEndpoint: String
        get() = ""
}