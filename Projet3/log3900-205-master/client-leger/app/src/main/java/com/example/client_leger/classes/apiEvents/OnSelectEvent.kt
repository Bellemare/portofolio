package com.example.client_leger.classes.apiEvents

import com.example.client_leger.classes.drawing.DrawingEventWrapper

class OnSelectEvent(val selectedDrawingEvent: DrawingEventWrapper?, val layer: Int): AppEvent() {
    override val type = AppEventType.ON_SELECT_EVENT
}