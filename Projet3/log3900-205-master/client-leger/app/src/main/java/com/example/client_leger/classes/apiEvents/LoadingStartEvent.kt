package com.example.client_leger.classes.apiEvents

class LoadingStartEvent(): AppEvent() {
    override val type = AppEventType.LOADING_START_EVENT
}