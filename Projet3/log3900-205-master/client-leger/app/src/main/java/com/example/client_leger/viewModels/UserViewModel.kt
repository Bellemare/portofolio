package com.example.client_leger.viewModels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.client_leger.classes.*
import com.example.client_leger.services.UserService
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.result.Result
import com.shopify.promises.Promise
import kotlinx.coroutines.launch

class UserViewModel: ViewModel() {

    fun getCurrentUser(): Promise<User, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = UserService.getCurrentUser()
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun getDefaultAvatars(): Promise<ArrayList<String>, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = UserService.getDefaultAvatars()
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun getConnectionHistory(): Promise<ArrayList<ConnectionHistory>, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = UserService.getConnectionHistory()
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun getCollaborativeSessions(): Promise<ArrayList<CollaborationSession>, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = UserService.getCollaborativeSessions()
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun getUserMetrics(): Promise<UserMetrics, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = UserService.getUserMetrics()
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun updateUser(user: User): Promise<RequestResponse, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = UserService.updateUser(user)
                when(res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }

    fun getUser(id: Int): Promise<User, FuelError> {
        return Promise {
            viewModelScope.launch {
                val res = UserService.getUser(id)
                when (res) {
                    is Result.Success -> {
                        resolve(res.value)
                    }
                    is Result.Failure -> {
                        reject(res.error)
                    }
                }
            }
        }
    }
}