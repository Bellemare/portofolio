package com.example.client_leger.classes.apiEvents

class DisconnectEvent(): AppEvent() {
    override val type = AppEventType.DISCONNECTION
}