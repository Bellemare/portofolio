import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AppComponentsModule } from './app-components/app-components.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app/app.component';
import { AppDirective } from './components/app/app.directive';
import { DrawingComponent } from './components/drawing/drawing.component';
import { EditorComponent } from './components/editor/editor.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { MaterialModule } from './material/material.module';
import { ServicesModule } from './services/services.module';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { MatDialogModule } from "@angular/material/dialog"
import { AlbumComponent } from './components/albums/album/album.component';
import { AlbumListComponent } from './components/albums/album-list/album-list.component';
import { AlbumsComponent } from './components/albums/albums/albums.component';
import { UserComponent } from './components/user/user.component';
import { ContactComponent } from './components/contact/contact.component';
import { PopoutChatComponent } from './components/popout-chat/popout-chat.component';
import { ComponentContainerComponent } from './components/component-container/component-container.component';
import { ExpositionComponent } from './components/exposition/exposition.component';



@NgModule({
    declarations: [
        AppComponent, 
        EditorComponent, 
        SidebarComponent, 
        DrawingComponent, 
        AppDirective, 
        LoginComponent, 
        RegisterComponent, 
        AlbumComponent, 
        AlbumListComponent, 
        AlbumsComponent, 
        UserComponent, 
        ContactComponent, 
        PopoutChatComponent, 
        ComponentContainerComponent, 
        ExpositionComponent,
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        AppRoutingModule,
        ServicesModule,
        AppComponentsModule,
        BrowserAnimationsModule,
        MaterialModule,
        FontAwesomeModule,
        MatDialogModule,
    ],
    providers: [],
    bootstrap: [AppComponent],
})
export class AppModule {}
