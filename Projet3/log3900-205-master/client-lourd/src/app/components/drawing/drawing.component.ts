import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Params, Router } from '@angular/router';
import { DrawingCanvasHelper } from '@app/classes/drawing-canvas-helper';
import { Vec2 } from '@app/classes/drawing/vec2';
import { MouseEventWrapper } from '@app/classes/mouse-event-wrapper';
import { CanvasSizeService } from '@app/services/canvas-size/canvas-size.service';
import { DrawingService } from '@app/services/components/drawing/drawing.service';
import { ToolService } from '@app/services/tools/tool.service';

@Component({
    selector: 'app-drawing',
    templateUrl: './drawing.component.html',
    styleUrls: ['./drawing.component.scss'],
})
export class DrawingComponent implements AfterViewInit, OnInit, OnDestroy {
    static readonly CURSOR: string = 'crosshair';

    @ViewChild('baseCanvas', { static: false }) private baseCanvas: ElementRef<HTMLCanvasElement>;
    @ViewChild('previewCanvas', { static: false }) private previewCanvas: ElementRef<HTMLCanvasElement>;
    @ViewChild('collabPreview', { static: false }) private collabPreviewCanvas: ElementRef<HTMLCanvasElement>;

    private currentTool: ToolService;

    constructor(
        private drawingService: DrawingService,
        private canvasSizeService: CanvasSizeService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
    ) {}

    ngOnInit(): void {
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        this.activatedRoute.params.subscribe((params: Params) => {
            if (params.drawingID) {
                this.drawingService.onInit(params.drawingID);   
            }
        });
    }

    ngAfterViewInit(): void {
        DrawingCanvasHelper.previewCanvas = this.previewCanvas.nativeElement;
        DrawingCanvasHelper.baseCanvas = this.baseCanvas.nativeElement;
        DrawingCanvasHelper.collabPreviewCanvas = this.collabPreviewCanvas.nativeElement;

        setTimeout(() => {
            this.drawingService.initCtx();
        });
        this.listenToolChange();
        this.listenResized();
        this.listenMouseMove();
        this.listenMouseUp();
        this.listenRouteLeave();
    }

    loadDrawing(id: string): void {
        this.router.navigateByUrl(`/editor/${id}`);
    }

    private listenRouteLeave(): void {
        this.router.events.subscribe((event) => {
            // On ne peut pas émettre un event dans les tests, puisque events est readonly
            if (event instanceof NavigationEnd && event.url === '/home') {
                this.drawingService.onDestroy();
                this.canvasSizeService.onDestroy();
            }
        });
    }

    private listenResized(): void {
        this.canvasSizeService.listenResized().subscribe(() => {
            this.drawingService.initToolsProperties();
        });
    }

    private listenToolChange(): void {
        this.drawingService.getCurrentTool().subscribe((tool: ToolService) => (this.currentTool = tool));
    }

    onMouseWheel(event: MouseEvent): void {
        this.currentTool.onMouseWheel(event);
    }

    onCanvasMouseLeave(event: MouseEvent): void {
        this.currentTool.onMouseLeave(new MouseEventWrapper(event));
    }

    onCanvasMouseEnter(event: MouseEvent): void {
        this.currentTool.onMouseEnter(new MouseEventWrapper(event));
    }

    onCanvasMouseDown(event: MouseEvent): void {
        if (!this.drawingService.disableEvents)
            this.currentTool.onMouseDown(new MouseEventWrapper(event));
    }

    onDoubleClick(event: MouseEvent): void {
        this.currentTool.onDoubleClick(new MouseEventWrapper(event));
    }

    onWorkZoneMouseDown(event: MouseEvent): void {
        this.currentTool.onMouseDown(new MouseEventWrapper(event, this.baseCanvas.nativeElement));
    }

    private listenMouseMove(): void {
        document.onmousemove = this.onMouseMove.bind(this);
    }

    private onMouseMove(event: MouseEvent): void {
        if (!this.drawingService.disableEvents)
            this.currentTool.onMouseMove(this.getMouseEvent(event));
    }

    private listenMouseUp(): void {
        document.onmouseup = this.onMouseUp.bind(this);
    }

    private onMouseUp(event: MouseEvent): void {
        if (!this.drawingService.disableEvents)
            this.currentTool.onMouseUp(this.getMouseEvent(event));
    }

    private getMouseEvent(event: MouseEvent): MouseEventWrapper {
        let mouseEvent = new MouseEventWrapper(event);
        if (event.target !== this.previewCanvas.nativeElement) {
            mouseEvent = new MouseEventWrapper(event, this.baseCanvas.nativeElement);
        }
        mouseEvent.isOutsideWindow = event.target === document;
        return mouseEvent;
    }

    cursor(): string {
        if (this.currentTool !== undefined) {
            return this.currentTool.cursor !== '' ? this.currentTool.cursor : DrawingComponent.CURSOR;
        }
        return DrawingComponent.CURSOR;
    }

    reset(): void {
        this.canvasSizeService.setSizeCanvas();
        this.drawingService.reset();
    }

    get size(): Vec2 {
        return CanvasSizeService.canvasSize;
    }

    get workZoneSize(): Vec2 {
        return {
            x: Math.max(window.innerWidth, CanvasSizeService.canvasSize.x + this.offset.x + DrawingCanvasHelper.RIGHT_MARGIN),
            y: Math.max(window.innerHeight, CanvasSizeService.canvasSize.y + this.offset.y + DrawingCanvasHelper.BOTTOM_MARGIN),
        };
    }

    get scalerMouseEventsClass(): string {
        const shouldDisable = this.currentTool?.isDrawing;
        return shouldDisable ? 'mouse-events-disabled' : 'mouse-events-enabled';
    }

    get offset(): Vec2 {
        return { x: DrawingCanvasHelper.OFFSET_LEFT, y: DrawingCanvasHelper.OFFSET_TOP };
    }

    ngOnDestroy(): void {
        this.drawingService.onDestroy();
    }
}
