import { HttpClientModule } from '@angular/common/http';
import { ElementRef, EventEmitter } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Params, RouterModule } from '@angular/router';
import { DrawingCanvasHelper } from '@app/classes/drawing-canvas-helper';
import { ToolClasses } from '@app/classes/drawing/tool-classes';
import { MouseButton } from '@app/classes/mouse-button';
import { MouseEventWrapper } from '@app/classes/mouse-event-wrapper';
import { AttributesManagerService } from '@app/services/api/attributes-manager.service';
import { AutoSaveService } from '@app/services/api/auto-save.service';
import { DrawingApiService } from '@app/services/api/drawing-api.service';
import { DrawingService } from '@app/services/components/drawing/drawing.service';
import { KeyBindingResolverService } from '@app/services/key-binding/key-binding-resolver.service';
import { NotificationService } from '@app/services/notification/notification.service';
import { ColorService } from '@app/services/tools/color.service';
import { PencilService } from '@app/services/tools/pencil-service';
import { ToolService } from '@app/services/tools/tool.service';
import { UndoRedoService } from '@app/services/undo-redo/undo-redo.service';
import { of } from 'rxjs';
import { DrawingComponent } from './drawing.component';

class ToolStub extends ToolService {}

// Les tests ne se séparent pas bien dans d'autres fichiers et on dépasse
// seulement de quelques lignes
// tslint:disable:max-file-line-count
// tslint:disable:no-magic-numbers
// tslint:disable:no-string-literal
// tslint:disable:no-any
// tslint:disable:no-empty
describe('DrawingComponent', () => {
    let component: DrawingComponent;
    let fixture: ComponentFixture<DrawingComponent>;
    let toolStub: ToolStub;
    let drawingStub: DrawingService;
    let attributesManagerStub: AttributesManagerService;
    let colorServiceStub: ColorService;
    let keyBindingResolverServiceStub: KeyBindingResolverService;
    let toolClassesSpy: jasmine.SpyObj<ToolClasses>;
    let drawingApiService: DrawingApiService;

    beforeEach(async(() => {
        attributesManagerStub = new AttributesManagerService();
        colorServiceStub = new ColorService(attributesManagerStub);
        keyBindingResolverServiceStub = new KeyBindingResolverService();
        toolStub = new ToolStub(attributesManagerStub, colorServiceStub, keyBindingResolverServiceStub);

        toolClassesSpy = jasmine.createSpyObj('ToolClasses', ['getToolService', 'TOOLS', 'TOOL_SERVICES']);
        toolClassesSpy.getToolService.and.returnValue(toolStub);

        const undoRedoService = new UndoRedoService(keyBindingResolverServiceStub);
        const autoSaveService = new AutoSaveService();
        

        const notificationService = new NotificationService();
        const httpClient = jasmine.createSpyObj('HttpClient', ['get']);
        drawingApiService = new DrawingApiService(httpClient, notificationService);

        TestBed.configureTestingModule({
            declarations: [DrawingComponent],
            providers: [
                { provide: ToolClasses, useValue: toolClassesSpy },
                { provide: PencilService, useValue: toolStub },
                { provide: DrawingService, useValue: drawingStub },
                { provide: AttributesManagerService, useValue: attributesManagerStub },
                { provide: ColorService, useValue: colorServiceStub },
                { provide: KeyBindingResolverService, useValue: keyBindingResolverServiceStub },
                { provide: DrawingApiService, drawingApiService },
            ],
            imports: [RouterModule.forRoot([]), HttpClientModule],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DrawingComponent);
        component = fixture.componentInstance;
        component['currentTool'] = toolStub;
        const workZone = document.createElement('div') as HTMLDivElement;
        const verticalScaler = document.createElement('div') as HTMLDivElement;
        const bottomScaler = document.createElement('div') as HTMLDivElement;
        const cornerScaler = document.createElement('div') as HTMLDivElement;
        component['workZone'] = { nativeElement: workZone } as ElementRef<HTMLDivElement>;
        component['sideScaler'] = { nativeElement: verticalScaler } as ElementRef<HTMLDivElement>;
        component['bottomScaler'] = { nativeElement: bottomScaler } as ElementRef<HTMLDivElement>;
        component['cornerScaler'] = { nativeElement: cornerScaler } as ElementRef<HTMLDivElement>;
        const baseCanvas = document.createElement('canvas') as HTMLCanvasElement;
        const previewCanvas = document.createElement('canvas') as HTMLCanvasElement;
        const gridCanvas = document.createElement('canvas') as HTMLCanvasElement;
        component['baseCanvas'] = { nativeElement: baseCanvas } as ElementRef<HTMLCanvasElement>;
        component['previewCanvas'] = { nativeElement: previewCanvas } as ElementRef<HTMLCanvasElement>;
        component['grid'] = { nativeElement: gridCanvas } as ElementRef<HTMLCanvasElement>;
        spyOn(component['drawingService'], 'initCtx').and.callFake(() => {});
        setTimeout(() => {
            baseCanvas.width = 100;
            baseCanvas.height = 100;
            fixture.detectChanges();
        }, 2000);
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should load drawing correctly', () => {
        const routerSpy = spyOn(component['router'], 'navigateByUrl');
        const resetSpy = spyOn(component['drawingService'], 'resetToBaseDrawing');

        component['currentDrawingID'] = 'test1234';
        component.loadDrawing('test1234');
        expect(resetSpy).toHaveBeenCalled();

        component.loadDrawing(':)');
        expect(routerSpy).toHaveBeenCalledWith('/editor/:)');
    });

    it('should init drawing properly when no params', async (done: DoneFn) => {
        const observable = of({} as Params);
        component['activatedRoute'].params = observable;
        const initDrawingSpy = spyOn(component['drawingService'], 'initDrawing');
        spyOnProperty(component['autoSaveService'], 'hasDrawingSaved', 'get').and.returnValue(false);
        component['initDrawing']();
        observable.subscribe(() => {
            setTimeout(() => {
                expect(initDrawingSpy).toHaveBeenCalled();
                done();
            }, 100);
        });
    });

    it('should init drawing properly with correct drawing', async (done: DoneFn) => {
        const blob = new Blob();
        const observable = of({ drawingID: 'test' } as Params);
        const drawingObservable = of(blob);
        component['activatedRoute'].params = observable;
        const initDrawingSpy = spyOn(component['drawingService'], 'initFromImage');
        component['initDrawing']();
        observable.subscribe(() => {
            drawingObservable.subscribe(() => {
                setTimeout(() => {
                    expect(initDrawingSpy).toHaveBeenCalledWith(blob);
                    done();
                }, 100);
            });
        });
    });

    it('should get stubTool', () => {
        const currentTool = component['currentTool'];
        expect(currentTool).toEqual(toolStub);
    });

    it(" should call the tool's mouse move when receiving a mouse move event", () => {
        const event = new MouseEventWrapper({} as MouseEvent);
        const mouseEventSpy = spyOn(toolStub, 'onMouseMove');
        spyOnProperty(component, 'isResizing', 'get').and.returnValue(false);
        component['onMouseMove'](event.mouseEvent);
        expect(mouseEventSpy).toHaveBeenCalled();
    });

    it(" shouldn't call the tool's mouse move when receiving a mouse move event if resizing is active", () => {
        const event = new MouseEventWrapper({} as MouseEvent);
        const mouseEventSpy = spyOn(toolStub, 'onMouseMove');
        spyOnProperty(component, 'isResizing', 'get').and.returnValue(true);
        component['onMouseMove'](event.mouseEvent);
        expect(mouseEventSpy).not.toHaveBeenCalled();
    });

    it(" shouldn't call the tool's mouse move when receiving a mouse move event from work zone if resizing is active", () => {
        const event = new MouseEventWrapper({} as MouseEvent, component['baseCanvas']);
        const mouseEventSpy = spyOn(toolStub, 'onMouseMove');
        spyOnProperty(component, 'isResizing', 'get').and.returnValue(true);
        component.onWorkZoneMouseDown(event.mouseEvent);
        expect(mouseEventSpy).not.toHaveBeenCalled();
    });

    it(" should call the tool's mouse enter when receiving a mouse enter event", () => {
        const event = new MouseEventWrapper({} as MouseEvent);
        const mouseEventSpy = spyOn(toolStub, 'onMouseEnter').and.callThrough();
        spyOnProperty(component, 'isResizing', 'get').and.returnValue(false);
        component.onCanvasMouseEnter(event.mouseEvent);
        expect(mouseEventSpy).toHaveBeenCalled();
        expect(mouseEventSpy).toHaveBeenCalledWith(event);
    });

    it(" shouldn't call the tool's mouse enter when receiving a mouse enter event if resizing is active", () => {
        const event = new MouseEventWrapper({} as MouseEvent);
        const mouseEventSpy = spyOn(toolStub, 'onMouseEnter').and.callThrough();
        spyOnProperty(component, 'isResizing', 'get').and.returnValue(true);
        component.onCanvasMouseEnter(event.mouseEvent);
        expect(mouseEventSpy).not.toHaveBeenCalled();
    });

    it(" should call the tool's mouse leave when receiving a mouse leave event", () => {
        const event = new MouseEventWrapper({} as MouseEvent);
        const mouseEventSpy = spyOn(toolStub, 'onMouseLeave').and.callThrough();
        spyOnProperty(component, 'isResizing', 'get').and.returnValue(false);
        component.onCanvasMouseLeave(event.mouseEvent);
        expect(mouseEventSpy).toHaveBeenCalled();
        expect(mouseEventSpy).toHaveBeenCalledWith(event);
    });

    it(" should call the tool's mouse leave when receiving a mouse leave event", () => {
        const event = new MouseEventWrapper({} as MouseEvent);
        const mouseEventSpy = spyOn(toolStub, 'onMouseLeave').and.callThrough();
        spyOnProperty(component, 'isResizing', 'get').and.returnValue(true);
        component.onCanvasMouseLeave(event.mouseEvent);
        expect(mouseEventSpy).not.toHaveBeenCalled();
    });

    it(" should call the tool's mouse down when receiving a mouse down event", () => {
        const event = new MouseEventWrapper({} as MouseEvent);
        const mouseEventSpy = spyOn(toolStub, 'onMouseDown').and.callThrough();
        spyOnProperty(component, 'isResizing', 'get').and.returnValue(false);
        component.onCanvasMouseDown(event.mouseEvent);
        expect(mouseEventSpy).toHaveBeenCalled();
        expect(mouseEventSpy).toHaveBeenCalledWith(event);
    });

    it(" shouldn't call the tool's mouse down when receiving a mouse down event if resizing", () => {
        const event = new MouseEventWrapper({} as MouseEvent);
        const mouseEventSpy = spyOn(toolStub, 'onMouseDown').and.callThrough();
        spyOnProperty(component, 'isResizing', 'get').and.returnValue(true);
        component.onCanvasMouseDown(event.mouseEvent);
        expect(mouseEventSpy).not.toHaveBeenCalled();
    });

    it(" should call the tool's mouse down with offset when receiving a mouse down event from work zone", () => {
        const event = new MouseEventWrapper({} as MouseEvent);
        const mouseEventSpy = spyOn(toolStub, 'onMouseDown').and.callThrough();
        spyOnProperty(component, 'isResizing', 'get').and.returnValue(false);
        component.onWorkZoneMouseDown(event.mouseEvent);
        expect(mouseEventSpy).toHaveBeenCalled();
    });

    it(" should call the tool's mouse up when receiving a mouse up event", () => {
        const event = new MouseEventWrapper({} as MouseEvent);
        const mouseEventSpy = spyOn(toolStub, 'onMouseUp').and.callThrough();
        spyOnProperty(component, 'isResizing', 'get').and.returnValue(false);
        component['onMouseUp'](event.mouseEvent);
        expect(mouseEventSpy).toHaveBeenCalled();
    });

    it(' should forward change on method hasChange', () => {
        const hasChangeSpy = spyOnProperty(drawingStub, 'hasChange', 'get').and.returnValue(true);
        expect(component.hasChange).toEqual(true);
        hasChangeSpy.and.returnValue(false);
        expect(component.hasChange).toEqual(false);
    });

    it(' should call current tool doubleClick when doubleClicking', () => {
        const event = { button: MouseButton.Left } as MouseEvent;
        const spy = spyOn(component['currentTool'], 'onDoubleClick');
        component.onDoubleClick(event);
        expect(spy).toHaveBeenCalled();
    });

    it(' should subscribe to tool change', () => {
        const subscribeSpy = spyOn(component['drawingService'], 'getCurrentTool').and.callThrough();
        component['listenToolChange']();
        expect(subscribeSpy).toHaveBeenCalled();
    });

    it(' should initialize correctly', () => {
        const listenToolChangeSpy = spyOn<any>(component, 'listenToolChange');
        const listenResizeSpy = spyOn<any>(component, 'listenResized');
        const setBaseSpy = spyOnProperty(DrawingCanvasHelper, 'baseCanvas', 'set');
        const setPreviewSpy = spyOnProperty(DrawingCanvasHelper, 'previewCanvas', 'set');
        component.ngAfterViewInit();
        expect(listenToolChangeSpy).toHaveBeenCalled();
        expect(listenResizeSpy).toHaveBeenCalled();
        expect(setBaseSpy).toHaveBeenCalled();
        expect(setPreviewSpy).toHaveBeenCalled();
    });

    it('should return correct scaler mouse events class', () => {
        const isDrawingSpy = spyOnProperty(component['currentTool'], 'isDrawing', 'get');

        isDrawingSpy.and.returnValue(false);
        expect(component.scalerMouseEventsClass).toEqual('mouse-events-enabled');

        isDrawingSpy.and.returnValue(true);
        expect(component.scalerMouseEventsClass).toEqual('mouse-events-disabled');
    });

    it('onMouseWheel should currentTool.onMouseWheel', () => {
        const event = {} as MouseEvent;
        const mouseEventSpy = spyOn(toolStub, 'onMouseWheel');

        component.onMouseWheel(event);

        expect(mouseEventSpy).toHaveBeenCalled();
    });

    it('ngOnInit should onInit of drawingService', () => {
        const onInitSpy = spyOn(component['drawingService'], 'onInit');
        component.ngOnInit();
        expect(onInitSpy).toHaveBeenCalled();
    });

    it('cursor should return default cursor if tool is undefined', () => {
        component['currentTool'] = (undefined as unknown) as ToolService;
        expect(component.cursor()).toEqual(DrawingComponent.CURSOR);
    });
});
