import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Album } from '@app/classes/api/album';
import { CollaborationSession } from '@app/classes/api/collaboration-session';
import { User } from '@app/classes/api/user';
import { UserMetrics } from '@app/classes/api/user-metrics';
import { PayloadTypes } from '@app/classes/socket/payload-types';
import { SocketClient } from '@app/classes/socket/socket-client';
import { AlbumApiService } from '@app/services/api/album-api.service';
import { ContactApiService } from '@app/services/api/contact-api-service';
import { RoomService } from '@app/services/api/room.service';
import { UserApiService } from '@app/services/api/user-api.service';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';
import { faChevronDown, faFolder } from '@fortawesome/free-solid-svg-icons';
import * as moment from 'moment';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit, OnDestroy {
  readonly albumIcon: IconDefinition = faFolder;
  readonly expandIcon: IconDefinition = faChevronDown;
  
  user: User;

  confirming: boolean = false;
  confirmText: string = "";

  expositions: Album[] = [];
  collaborativeSessions: CollaborationSession[] = [];
  albums: Album[] = [];
  availableAlbums: Album[] = [];

  metrics: UserMetrics;

  selectedDrawingId: number = 0;
  isSettingPassword: boolean = false;
  
  private appClient: SocketClient;
  private albumChangeListener: number;

  @ViewChild("expositionsContainer") private expositionsElement: ElementRef<HTMLDivElement>;

  constructor(
    private activatedRoute: ActivatedRoute, 
    private userService: UserApiService, 
    private contactService: ContactApiService,
    private roomService: RoomService,
    private albumService: AlbumApiService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params: Params) => {
      if (params.id) {
        this.getContact(params.id);
      this.getCollaborativeSessions(params.id);
      this.getExpositions(params.id);
      }
    });
    this.listenChange();
    this.getMemberAlbums();
  }

  ngOnDestroy(): void {
    if (this.appClient != undefined)
      this.appClient.unregister(PayloadTypes.ALBUM_CHANGE, this.albumChangeListener);
  }

  hideAll(): void {
    this.isSettingPassword = false;
  }

  routeToDrawing(session: CollaborationSession): void {
    if (session.is_password_protected) {
      this.selectedDrawingId = session.drawing_id;
      this.isSettingPassword = true;
    } else {
      this.editDrawing(session.drawing_id);
    }
  }

  editDrawing(drawingId: number): void {
    this.router.navigateByUrl(`/editor/${drawingId}`);
  }

  private listenChange(): void {
    this.roomService.getAppClient().then((client: SocketClient) => {
      this.appClient = client;
      this.albumChangeListener = client.onMessage(PayloadTypes.ALBUM_CHANGE, () => {
        this.getMemberAlbums();
      });
    });
  }

  scrollExpositions(event: WheelEvent): void {
    if (this.expositionsElement == undefined) {
      return;
    }

    this.expositionsElement.nativeElement.scrollLeft += event.deltaY;
  }

  isAlbumMember(albumId: number): boolean {
    return this.albums.findIndex((a: Album) => a.id == albumId) > -1;
  }

  isPendingJoin(albumId: number): boolean {
    return !this.isAlbumMember(albumId) && this.availableAlbums.findIndex((a: Album) => a.id == albumId) == -1;
  }

  joinAlbum(albumId: number): void {
    const index = this.availableAlbums.findIndex((a: Album) => a.id == albumId);
    if (index != -1) {
      this.albumService.requestToJoinAlbum(albumId).subscribe(() => {
        this.availableAlbums.splice(index, 1);
      });
    }
  }

  addContact(): void {
    this.contactService.addContact(this.user.id).subscribe(() => {
      this.user.is_contact = true;
    });
  }

  removeContact(): void {
    this.confirming = true;
    this.confirmText = `Voulez-vous vraiment retirer ${this.user.pseudonym} de vos contacts?`;
  }

  onRemoveContact(): void {
    this.confirming = false;
    this.contactService.deleteContact(this.user.id).subscribe(() => {
      this.user.is_contact = false;
    });
  }

  private getContact(id: number): void {
    this.userService.getUser(id).subscribe((user: User) => this.user = user);
  }

  private getMemberAlbums(): void {
    this.albumService.getUserAlbums().subscribe((albums: Album[]) => this.albums = albums);
    this.albumService.getAlbumList().subscribe((albums: Album[]) => this.availableAlbums = albums);
  }

  private getExpositions(id: number): void {
    this.contactService.getContactExpositions(id).subscribe((expositions: Album[]) => this.expositions = expositions);
  }

  private getCollaborativeSessions(id: number): void {
    this.contactService.getCollaborativeSessions(id).subscribe((entries: CollaborationSession[]) => {
      entries.map((entry: CollaborationSession) => {
        entry.start_time = moment(entry.start_time);
        entry.end_time = moment(entry.end_time);
      });
      this.collaborativeSessions = entries;
    });
  }
}
