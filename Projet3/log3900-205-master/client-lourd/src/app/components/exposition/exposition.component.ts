import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Album } from '@app/classes/api/album';
import { Drawing } from '@app/classes/api/drawing';
import { User } from '@app/classes/api/user';
import { PayloadTypes } from '@app/classes/socket/payload-types';
import { SocketClient } from '@app/classes/socket/socket-client';
import { AlbumApiService } from '@app/services/api/album-api.service';
import { AuthService } from '@app/services/api/auth.service';
import { ExpositionApiService } from '@app/services/api/expositions-api.service';
import { RoomService } from '@app/services/api/room.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-exposition',
  templateUrl: './exposition.component.html',
  styleUrls: ['./exposition.component.scss']
})
export class ExpositionComponent implements OnInit {
  private albumId: number;
  album: Album;
  drawings: Drawing[] = [];
  user: User;

  showDrawingView: boolean = false;

  selectedDrawing: Drawing;
  private userSubscription: Subscription;
  private appClient: SocketClient;
  private drawingListener: number;
  private albumListener: number;

  constructor(
    private albumService: AlbumApiService, 
    private activatedRoute: ActivatedRoute, 
    private authService: AuthService,
    private router: Router,
    private roomService: RoomService,
    private expositionService: ExpositionApiService,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params: Params) => {
      if (params.albumID) {
        this.albumId = params.albumID;
        this.loadAlbum();
        this.loadDrawings();
      }
    });
    this.loadUser();
    this.listenChange();
  }

  ngOnDestroy(): void {
      this.userSubscription.unsubscribe();
      this.appClient?.unregister(PayloadTypes.DRAWING_CHANGE, this.drawingListener);
      this.appClient?.unregister(PayloadTypes.ALBUM_CHANGE, this.albumListener);
  }

  private listenChange(): void {
    this.roomService.getAppClient().then((client: SocketClient) => {
      this.appClient = client;
      this.drawingListener = client.onMessage(PayloadTypes.DRAWING_CHANGE, () => {
        this.loadDrawings();
      });
      this.albumListener = client.onMessage(PayloadTypes.ALBUM_CHANGE, () => {
        this.loadAlbum();
      });
    });
  }

  hideAll(): void {
    this.showDrawingView = false;
  }

  onDrawingView(drawing: Drawing): void {
    this.hideAll();
    this.selectedDrawing = drawing;
    this.showDrawingView = true;
  }

  albumEdited(album: Album): void {
    this.hideAll();
    this.album.name = album.name;
    this.album.description = album.description;
  }

  isOwner(): boolean {
    return this.user !== undefined && this.album !== undefined && this.user.id == this.album.owner_id;
  }

  private loadUser(): void {
    if (this.user == undefined)
      this.user = this.authService.getUser();

    this.userSubscription = this.authService.listenUserChange().subscribe((user: User) => {
      this.user = user
    });
  }

  private loadAlbum(): void {
    if (this.albumId > 0) {
      this.albumService.getAlbum(this.albumId).subscribe((album: Album) => {
        this.album = album;
      }, () => this.router.navigateByUrl("/album"));
    }
  }

  private loadDrawings(): void {
    this.expositionService.getExposition(this.albumId).subscribe((drawings: Drawing[]) => {
      this.drawings = drawings;
    });
  }
}
