import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponentsModule } from '@app/app-components/app-components.module';
import { MaterialModule } from '@app/material/material.module';
import { AttributesManagerService } from '@app/services/api/attributes-manager.service';
import { SelectionClipboardService } from '@app/services/clipboard/selection-clipboard.service';
import { KeyBindingResolverService } from '@app/services/key-binding/key-binding-resolver.service';
import { SelectionEditorService } from '@app/services/selection/selection-editor.service';
import { SelectionResizeService } from '@app/services/selection/selection-resize.service';
import { ColorService } from '@app/services/tools/color.service';
import { SelectionService } from '@app/services/tools/selection/selection.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { SidebarComponent } from './sidebar.component';

class ToolStub extends SelectionService {
    readonly TOOL_ICON: IconDefinition = faCheck;

    // tslint:disable-next-line:no-empty
    protected initDashedPrevisualization(): void {}
}

// tslint:disable:no-string-literal
describe('SidebarComponent', () => {
    let component: SidebarComponent;
    let fixture: ComponentFixture<SidebarComponent>;

    let toolStub: ToolStub;

    beforeEach(async(() => {
        const attrManager = new AttributesManagerService();
        const colorService = new ColorService(attrManager);
        const keybindService = new KeyBindingResolverService();
        const selectionEditor = new SelectionEditorService(keybindService);
        const selectionClipboard = new SelectionClipboardService(keybindService);
        const selectionResize = new SelectionResizeService(selectionEditor, keybindService);
        toolStub = new ToolStub(attrManager, colorService, keybindService, selectionClipboard, selectionEditor, selectionResize);

        TestBed.configureTestingModule({
            declarations: [SidebarComponent],
            imports: [MaterialModule, FontAwesomeModule, AppComponentsModule, BrowserAnimationsModule],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SidebarComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();

        spyOnProperty(component['drawingService'], 'tool', 'get').and.returnValue(toolStub);
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should output reset event', () => {
        const eventSpy = spyOn(component.resetCanvas, 'emit');
        component.callResetCanvas();
        expect(eventSpy).toHaveBeenCalled();
    });

    it('should call redo', () => {
        const eventSpy = spyOn(component['undoRedoService'], 'redo');
        component.callRedo();
        expect(eventSpy).toHaveBeenCalled();
    });

    it('should call undo', () => {
        const eventSpy = spyOn(component['undoRedoService'], 'undo');
        component.callUndo();
        expect(eventSpy).toHaveBeenCalled();
    });

    it('should call export', () => {
        const eventSpy = spyOn(component.exportDrawing, 'emit');
        component.callExport();
        expect(eventSpy).toHaveBeenCalled();
    });
});
