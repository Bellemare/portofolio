import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { AppComponentsModule } from '@app/app-components/app-components.module';
import { DrawingCanvasHelper } from '@app/classes/drawing-canvas-helper';
import { ToolClasses } from '@app/classes/drawing/tool-classes';
import { DrawingComponent } from '@app/components/drawing/drawing.component';
import { SidebarComponent } from '@app/components/sidebar/sidebar.component';
import { MaterialModule } from '@app/material/material.module';
import { AttributesManagerService } from '@app/services/api/attributes-manager.service';
import { AutoSaveService } from '@app/services/api/auto-save.service';
import { DrawingApiService } from '@app/services/api/drawing-api.service';
import { CanvasSizeService } from '@app/services/canvas-size/canvas-size.service';
import { DrawingService } from '@app/services/components/drawing/drawing.service';
import { KeyBindingResolverService } from '@app/services/key-binding/key-binding-resolver.service';
import { ColorService } from '@app/services/tools/color.service';
import { ToolService } from '@app/services/tools/tool.service';
import { UndoRedoService } from '@app/services/undo-redo/undo-redo.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { EditorComponent } from './editor.component';

class ToolStub extends ToolService {}

// tslint:disable:no-string-literal
// tslint:disable:no-magic-numbers
// tslint:disable:no-any
describe('EditorComponent', () => {
    let component: EditorComponent;
    let fixture: ComponentFixture<EditorComponent>;

    let keyBindingResolverService: KeyBindingResolverService;
    let drawingService: DrawingService;
    let drawingComponent: DrawingComponent;
    let toolService: ToolService;
    let colorServiceStub: ColorService;
    let activatedRoute: jasmine.SpyObj<ActivatedRoute>;
    let drawingApiService: jasmine.SpyObj<DrawingApiService>;
    let attributesManagerStub: AttributesManagerService;
    let toolClassesSpy: jasmine.SpyObj<ToolClasses>;

    let drawingApiServiceSpy: jasmine.SpyObj<DrawingApiService>;

    let canvas: HTMLCanvasElement;

    beforeEach(async(() => {
        drawingApiServiceSpy = jasmine.createSpyObj('DrawingApiService', ['saveDrawing']);

        attributesManagerStub = new AttributesManagerService();
        colorServiceStub = new ColorService(attributesManagerStub);
        keyBindingResolverService = new KeyBindingResolverService();
        toolService = new ToolStub(attributesManagerStub, colorServiceStub, keyBindingResolverService);
        toolService = new ToolStub(attributesManagerStub, colorServiceStub, keyBindingResolverService);

        toolClassesSpy = jasmine.createSpyObj('ToolClasses', ['getToolService', 'TOOLS', 'TOOL_SERVICES']);
        toolClassesSpy.getToolService.and.returnValue(toolService);

        TestBed.configureTestingModule({
            declarations: [EditorComponent, DrawingComponent, SidebarComponent],
            imports: [AppComponentsModule, MaterialModule, BrowserAnimationsModule, RouterModule.forRoot([]), FontAwesomeModule],
            providers: [{ provide: DrawingApiService, useValue: drawingApiServiceSpy }],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EditorComponent);
        component = fixture.componentInstance;
        canvas = document.createElement('canvas');
        DrawingCanvasHelper.baseCanvas = canvas;
        activatedRoute = jasmine.createSpyObj('ActivatedRoute', [], ['params']);
        drawingApiService = jasmine.createSpyObj('DrawingApiService', ['getDrawing']);

        const undoRedoService = new UndoRedoService(keyBindingResolverService);
        const autoSaveService = new AutoSaveService();
        const resizeService = new CanvasSizeService(undoRedoService, autoSaveService);
        drawingService = new DrawingService(keyBindingResolverService, toolClassesSpy, undoRedoService, resizeService, autoSaveService);
        const router = jasmine.createSpyObj('Router', ['events', 'navigateByUrl']);
        drawingComponent = new DrawingComponent(
            drawingService,
            resizeService,
            activatedRoute,
            router,
            autoSaveService,
        );
        component['drawingContainer'] = drawingComponent;
        setTimeout(() => {
            fixture.detectChanges();
        }, 1000);
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should register keybinds when initializing', () => {
        const spy = spyOn(component['keyBindingResolverService'], 'registerKeybind');
        component.ngOnInit();
        expect(spy).toHaveBeenCalled();
    });

    it('should unregister keybinds when destroying component', () => {
        const spy = spyOn(component['keyBindingResolverService'], 'unregisterKeybind');
        component.ngOnDestroy();
        expect(spy).toHaveBeenCalled();
    });

    it("should call drawing component's reset method and disable resetting behaviour when confirming reset", () => {
        component['drawingContainer'] = drawingComponent;
        const spy = spyOn(drawingComponent, 'reset');
        component['resetWithChange'] = true;
        component.confirmDiscardChanges();
        expect(spy).toHaveBeenCalled();
        expect(component['resetWithChange']).toEqual(false);
    });

    it('should disable resetting behaviour when declining reset', () => {
        component['resetWithChange'] = true;
        component.declineReset();
        expect(component['resetWithChange']).toEqual(false);
    });

    it('should return the state of resetting when getting the value', () => {
        component['resetWithChange'] = true;
        expect(component.resetWithChange).toEqual(true);
        component['resetWithChange'] = false;
        expect(component.resetWithChange).toEqual(false);
    });

    it('should enable resetting when drawing has change and calling reset canvas', () => {
        component['drawingContainer'] = drawingComponent;
        component['resetWithChange'] = false;
        spyOnProperty(drawingComponent, 'hasChange', 'get').and.returnValue(true);
        component.resetCanvas();
        expect(component['resetWithChange']).toEqual(true);
    });

    it('should reset when drawing has no change and calling reset canvas', () => {
        component['drawingContainer'] = drawingComponent;
        component['resetWithChange'] = false;
        const spy = spyOn(drawingComponent, 'reset');
        spyOnProperty(drawingComponent, 'hasChange', 'get').and.returnValue(false);
        component.resetCanvas();
        expect(spy).toHaveBeenCalled();
        expect(component['resetWithChange']).toEqual(false);
    });

    it('should handle afterEvent correctly', () => {
        const disabledSpy = spyOnProperty(component['keyBindingResolverService'], 'disabled', 'set');
        const clearKeysSpy = spyOn(component['keyBindingResolverService'], 'clearKeys');
        component['afterEvent']();
        expect(disabledSpy).toHaveBeenCalledWith(false);
        expect(clearKeysSpy).toHaveBeenCalled();
    });

    it('should check if saving correctly', () => {
        component['isSavingToServer'] = true;
        component['savingDrawing'] = true;
        expect(component.savingDrawing).toEqual(true);
        expect(component.isSavingToServer).toEqual(true);
    });

    it('confirmDiscardChanges, if isCarouselOpen is true, set to false and call routeToDrawing', () => {
        // La méthode est privée, on doit donc mettre any
        // tslint:disable-next-line
        const routeToDrawingSpy = spyOn<any>(component, 'routeToDrawing');
        component['isCarouselOpen'] = true;

        component.confirmDiscardChanges();

        expect(component.isCarouselOpen).toEqual(false);
        expect(routeToDrawingSpy).toHaveBeenCalled();
    });

    it('should route correctly', () => {
        const routeToDrawingSpy = spyOn(drawingComponent, 'loadDrawing');
        const id = '12938213';
        component['routeToDrawing'](id);
        expect(routeToDrawingSpy).toHaveBeenCalledWith(id);
    });
});
