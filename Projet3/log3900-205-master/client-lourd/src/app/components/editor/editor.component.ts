import { ClientStateSyncEventPayload } from './../../classes/socket/client-state-sync-event-payload';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Drawing } from '@app/classes/api/drawing';
import { User } from '@app/classes/api/user';
import { ClientStateEventPayload } from '@app/classes/socket/client-state-event-payload';
import { ClientStateType } from '@app/classes/socket/client-state-types';
import { PayloadTypes } from '@app/classes/socket/payload-types';
import { SocketClient } from '@app/classes/socket/socket-client';
import { SoundType } from '@app/classes/sound-types';
import { AuthService } from '@app/services/api/auth.service';
import { DrawingApiService } from '@app/services/api/drawing-api.service';
import { RoomService } from '@app/services/api/room.service';
import { AudioService } from '@app/services/audio/audio.service';
import { ColorHandlerService } from '@app/services/components/color/color-handler.service';
import { DrawingService } from '@app/services/components/drawing/drawing.service';
import { ClientSync } from '@app/classes/socket/client-sync';

@Component({
    selector: 'app-editor',
    templateUrl: './editor.component.html',
    styleUrls: ['./editor.component.scss'],
})
export class EditorComponent implements OnInit {
    resetWithChange: boolean = false;
    savingDrawing: boolean = false;
    isSavingToServer: boolean = false;
    exportingDrawing: boolean = false;
    isCarouselOpen: boolean = false;

    private drawingRoomClient: SocketClient | undefined;
    users: Map<number, User> = new Map();

    drawing: Drawing;

    constructor(
        private roomService: RoomService, 
        private colorHandler: ColorHandlerService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private drawingApiService: DrawingApiService,
        private drawingService: DrawingService,
        private audioService: AudioService,
        private authService: AuthService) {}

    ngOnInit(): void {
        this.listenToDrawingRoomSession();
        this.listenToQuitDrawingRoomSession();
        this.loadDrawing();
    }

    private loadDrawing(): void {
        this.activatedRoute.params.subscribe((params: Params) => {
            if (params.drawingID) {
                this.drawingApiService.getDrawing(params.drawingID).subscribe((drawing: Drawing) => this.drawing = drawing);
            }
        });
    }

    private listenToDrawingRoomSession(): void {
        this.roomService.listenDrawingRoomAdd().subscribe((conn: SocketClient) => {
            this.drawingRoomClient = conn;
            this.listenToCollaboratorsStateChange();
        });
    }

    private listenToQuitDrawingRoomSession(): void {
        this.roomService.listenDrawingRoomRemove().subscribe(() => {
            this.drawingRoomClient = undefined;
            this.users.clear();
        });
    }

    private listenToCollaboratorsStateChange(): void {
        this.drawingRoomClient?.onMessage(PayloadTypes.CLIENT_STATE_EVENT, (stateChange: ClientStateEventPayload) => {
            switch (stateChange.state) {
                case ClientStateType.CONNECT:
                    this.users.set(stateChange.user_id, {
                        pseudonym: stateChange.user,
                        id: stateChange.user_id,
                        avatar: stateChange.avatar,
                        is_contact: false,
                        email: "",
                    });
                    if (stateChange.user_id != this.authService.getUser().id) {
                        this.audioService.playSound(SoundType.NewCollaborator);
                    }
                    break;
                case ClientStateType.DISCONNECT:
                    this.users.delete(stateChange.user_id);
                    break;
            }
        });
        this.drawingRoomClient?.onMessage(PayloadTypes.CLIENT_STATE_SYNC_EVENT, (state: ClientStateSyncEventPayload) => {
            state.clients.forEach((client: ClientSync) => {
                this.users.set(client.user_id, {
                    pseudonym: client.user,
                    id: client.user_id,
                    avatar: client.avatar,
                    is_contact: false,
                    email: "",
                });
            });
         });
    }

    getUserColor(user: User): string {
        this.colorHandler.color = { r: 255, g: 0, b: 0, a: 1 };
        return this.colorHandler.getUserColor(user.id);
    }

    navigateToUser(user: User): void {
        if (user.id == this.authService.getUser().id) {
            this.router.navigateByUrl("/user");
        } else {
            this.router.navigateByUrl(`/contact/${user.id}`);
        }
    }

    get isDrawing(): boolean {
        return this.drawingService.tool?.isDrawing;
    }

    get isLoading(): boolean {
        return this.drawingService.isLoading;
    }
}
