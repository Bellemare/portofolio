import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopoutChatComponent } from './popout-chat.component';

describe('PopoutChatComponent', () => {
  let component: PopoutChatComponent;
  let fixture: ComponentFixture<PopoutChatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopoutChatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopoutChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
