import { Component } from '@angular/core';
import { ChatRoomComponent } from '@app/app-components/chat/chat-room/chat-room.component';
import { SocketClient } from '@app/classes/socket/socket-client';
import { DrawingApiService } from '@app/services/api/drawing-api.service';
import { ElectronService } from '@app/services/api/electron.service';
import { RoomService } from '@app/services/api/room.service';

@Component({
  selector: 'app-popout-chat',
  templateUrl: './popout-chat.component.html',
  styleUrls: ['./popout-chat.component.scss']
})
export class PopoutChatComponent extends ChatRoomComponent {
  show: boolean = true;

  constructor(
    public roomService: RoomService, 
    protected electronService: ElectronService,
    private drawingApiService: DrawingApiService,
  ) { 
    super(roomService, electronService);
  }

  ngOnInit(): void {
      this.loadRoomList();
      this.listenToDrawingRoomSession();
  }
  
  protected listenToDrawingRoomSession(): void {
    this.electronService.listenNewDrawingRoom().then((id: number) => {
      this.drawingApiService.joinCollaborativeSession(id).then((conn: SocketClient) => {
        this.addNewDrawingRoomSession(conn);
      });
      this.listenToQuitDrawingRoomSession();
    });
  }

  protected listenToQuitDrawingRoomSession(): void {
    this.electronService.listenQuitDrawingRoom().then(() => {
      this.drawingRoomClient?.closeConnection();
      this.quitDrawingRoomSession();
      this.listenToDrawingRoomSession();
    });
  }
}
