import { Component, OnInit, Renderer2 } from '@angular/core';
import { Router } from '@angular/router';
import { DrawingService } from '@app/services/components/drawing/drawing.service';
import { KeyBindingResolverService } from '@app/services/key-binding/key-binding-resolver.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
    constructor(
        private keyBindingResolverService: KeyBindingResolverService,
        private renderer: Renderer2,
        private router: Router,
        private drawingService: DrawingService,
    ) {
        this.listenKey();
    }

    ngOnInit(): void {
        document.oncontextmenu = () => false;
    }

    private listenKey(): void {
        this.renderer.listen('document', 'keydown', (e) => {
            this.keyBindingResolverService.keyboardPressEvent(e, e.target != null ? e.target.nodeName : null);
        });
        this.renderer.listen('document', 'keyup', (e) => {
            this.keyBindingResolverService.keyboardReleaseEvent(e, e.target != null ? e.target.nodeName : null);
        });
        this.renderer.listen('window', 'blur', this.keyBindingResolverService.clearKeys.bind(this.keyBindingResolverService));
        this.renderer.listen('document', "auxclick", (e) => {
            if (e.which == 2) {
                e.preventDefault();
            }
        })
    }

    get showActionsMenu(): boolean {
        return !(new RegExp("/chat").test(this.router.url));
    }

    get isDrawing(): boolean {
        return this.drawingService.tool?.isDrawing;
    }
}
