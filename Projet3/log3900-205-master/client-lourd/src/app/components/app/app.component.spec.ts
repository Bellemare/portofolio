import { Component, ComponentRef } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponentsModule } from '@app/app-components/app-components.module';
import { AppComponent } from './app.component';
import { AppDirective } from './app.directive';

@Component({
    template: '',
})
export class StubComponent {}

// tslint:disable:no-string-literal
// tslint:disable:no-empty
// tslint:disable:no-magic-numbers
describe('AppComponent', () => {
    let component: AppComponent;
    let fixture: ComponentFixture<AppComponent>;

    beforeEach(async(() => {
        const viewContainerRef = jasmine.createSpyObj('ViewContainerRef', ['createComponent']);
        viewContainerRef.createComponent.and.callThrough();
        const appDirective = new AppDirective(viewContainerRef);

        TestBed.configureTestingModule({
            imports: [RouterTestingModule, AppComponentsModule],
            declarations: [AppComponent, AppDirective],
            providers: [{ provide: AppDirective, useValue: appDirective }],
        });
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AppComponent);
        component = fixture.componentInstance;
        component.ngOnInit();
        fixture.detectChanges();
    });

    it('should create the app', () => {
        expect(component).toBeTruthy();
    });

    it('should return component when creating', () => {
        const componentStub = AppComponent.createComponent<StubComponent>(StubComponent);
        expect(componentStub).toBeInstanceOf(StubComponent);
        expect(AppComponent['instances'].length).toEqual(1);
    });

    it('should destroy component', () => {
        const spliceSpy = spyOn(AppComponent['instances'], 'splice').and.callThrough();
        const componentStub = new StubComponent();
        const componentRef = { instance: componentStub, destroy: () => {} } as ComponentRef<StubComponent>;
        const destroySpy = spyOn(componentRef, 'destroy');
        AppComponent['instances'].push(componentRef);
        AppComponent.destroyComponent(componentStub);
        expect(destroySpy).toHaveBeenCalled();
        expect(spliceSpy).toHaveBeenCalled();
    });

    it('should listen to key events', () => {
        const rendererSpy = spyOn(component['renderer'], 'listen');
        component['listenKey']();
        expect(rendererSpy).toHaveBeenCalledTimes(4);
    });

    it('should call keybindingService events', () => {
        const keydownSpy = spyOn(component['keyBindingResolverService'], 'keyboardPressEvent');
        const keyupSpy = spyOn(component['keyBindingResolverService'], 'keyboardReleaseEvent');
        document.dispatchEvent(new KeyboardEvent('keydown', { key: 'a' }));
        document.dispatchEvent(new KeyboardEvent('keyup', { key: 'a' }));
        component['listenKey']();
        expect(keydownSpy).toHaveBeenCalled();
        expect(keyupSpy).toHaveBeenCalled();
    });
});
