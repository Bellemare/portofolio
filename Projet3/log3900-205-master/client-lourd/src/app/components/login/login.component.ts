import { Component, OnInit } from '@angular/core';
import { AuthService } from '@app/services/api/auth.service';
import { LoginRequest } from '@app/classes/api/login-request';
import { AuthToken } from '@app/classes/api/auth-token';
import { Router } from '@angular/router';
import { RoomService } from '@app/services/api/room.service';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';
import { faPaintBrush } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  readonly drawingIcon: IconDefinition = faPaintBrush;

  loginData: LoginRequest = new LoginRequest();
  connecting: boolean = false;
  error: boolean = false;

  constructor(private authService: AuthService, private router: Router, private roomService: RoomService) { }

  ngOnInit(): void {
    if (this.authService.isAuthenticated()) {
      this.router.navigateByUrl("");
    } else {
      this.roomService.disconnectAll();
    }
  }

  login(): void {
    this.connecting = true;
    this.authService.login(this.loginData).subscribe((token: AuthToken) => {
      this.connecting = false;
      setTimeout(() => {  
        this.router.navigateByUrl("");
      }, 50);
    }, () => {
      this.connecting = false;
      this.error = true;
    });
  }

}
