import { Component, OnDestroy, OnInit } from '@angular/core';
import { User } from '@app/classes/api/user';
import { AuthService } from '@app/services/api/auth.service';
import { UserApiService } from '@app/services/api/user-api.service';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';
import { faCheck, faChevronDown, faChevronUp, faPencilAlt, faTimes } from '@fortawesome/free-solid-svg-icons';
import { ConnectionHistoryEntry } from '@app/classes/api/connection-history-entry';
import { CollaborationSession } from '@app/classes/api/collaboration-session';
import * as moment from 'moment';
import { AlbumApiService } from '@app/services/api/album-api.service';
import { Album } from '@app/classes/api/album';
import { RoomService } from '@app/services/api/room.service';
import { SocketClient } from '@app/classes/socket/socket-client';
import { PayloadTypes } from '@app/classes/socket/payload-types';
import { UserMetrics } from '@app/classes/api/user-metrics';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit, OnDestroy {
  readonly editIcon: IconDefinition = faPencilAlt;
  readonly confirmIcon: IconDefinition = faCheck;
  readonly cancelIcon: IconDefinition = faTimes;
  readonly expandIcon: IconDefinition = faChevronDown;
  readonly collapseIcon: IconDefinition = faChevronUp;

  user: User;

  selectingAvatar: boolean = false;
  error: boolean = false;
  isEditing: boolean = false;
  editedUser: User;

  history: ConnectionHistoryEntry[] = [];
  collaborativeSessions: CollaborationSession[] = [];
  albums: Album[] = [];
  availableAlbums: Album[] = [];

  metrics: UserMetrics;
  
  private appClient: SocketClient;
  private albumChangeListener: number;

  constructor(
    private authService: AuthService, 
    private userService: UserApiService, 
    private albumService: AlbumApiService,
    private roomService: RoomService,
  ) { }

  ngOnInit(): void {
    this.user = this.authService.getUser();

    this.authService.listenUserChange().subscribe((user: User) => this.user = user);
    this.getConnectionHistory();
    this.getCollaborativeSessions();
    this.getMemberAlbums();
    this.listenChange();
    this.getUserMetrics();
  }

  ngOnDestroy(): void {
    if (this.appClient != undefined)
      this.appClient.unregister(PayloadTypes.ALBUM_CHANGE, this.albumChangeListener);
  }

  private listenChange(): void {
    this.roomService.getAppClient().then((client: SocketClient) => {
      this.appClient = client;
      this.albumChangeListener = client.onMessage(PayloadTypes.ALBUM_CHANGE, () => {
        this.getMemberAlbums();
      });
    });
  }

  editUser(): void {
    this.isEditing = true;
    this.editedUser = JSON.parse(JSON.stringify(this.user));
  }

  cancelEdit(): void {
    this.isEditing = false;
    this.selectingAvatar = false;
  }

  selectAvatar(): void {
    this.selectingAvatar = true;
  }

  changeAvatar(avatar: string): void {
    this.selectingAvatar = false;
    const user = JSON.parse(JSON.stringify(this.user));
    user.avatar = avatar;
    this.userService.updateUser(user).subscribe(() => {
      this.user.avatar = user.avatar;
    });
  }

  confirmEdit(): void {
    this.error = false;
    this.userService.updateUser(this.editedUser).subscribe(() => {
      this.isEditing = false;
      this.user.pseudonym = this.editedUser.pseudonym;
    }, () => this.error = true);
  }

  setEmailConfidentiality(isPublic: string): void {
    this.editedUser = this.user;
    this.editedUser.is_public_email = isPublic == "true";
    this.userService.updateUser(this.editedUser).subscribe(() => {
      this.user.is_public_email = isPublic == "true";
    });
  }

  isAlbumMember(albumId: number): boolean {
    return this.albums.findIndex((a: Album) => a.id == albumId) > -1;
  }

  isPendingJoin(albumId: number): boolean {
    return !this.isAlbumMember(albumId) && this.availableAlbums.findIndex((a: Album) => a.id == albumId) == -1;
  }

  joinAlbum(albumId: number): void {
    const index = this.availableAlbums.findIndex((a: Album) => a.id == albumId);
    if (index != -1) {
      this.albumService.requestToJoinAlbum(albumId).subscribe(() => {
        this.availableAlbums.splice(index, 1);
      });
    }
  }

  formatSeconds(seconds: number): string {
    if (seconds == undefined) {
      return '';
    }

    const minutes = Math.floor(seconds / 60);
    const remaining = seconds % 60;
    return `${minutes.toString().padStart(2, "0")} min ${remaining.toString().padStart(2, "0")} s`;
  }

  private getUserMetrics(): void {
    this.userService.getUserMetrics().subscribe((metrics: UserMetrics) => this.metrics = metrics);
  }

  private getConnectionHistory(): void {
    this.userService.getConnectionHistory().subscribe((entries: ConnectionHistoryEntry[]) => {
      entries.map((entry: ConnectionHistoryEntry) => {
        entry.created_at = moment(entry.created_at);
        if (entry.invalidated_at != undefined) {
          entry.invalidated_at = moment(entry.invalidated_at);
        }
      });
      this.history = entries;
    });
  }

  private getMemberAlbums(): void {
    this.albumService.getUserAlbums().subscribe((albums: Album[]) => this.albums = albums);
    this.albumService.getAlbumList().subscribe((albums: Album[]) => this.availableAlbums = albums);
  }

  private getCollaborativeSessions(): void {
    this.userService.getCollaborationSessionHistory().subscribe((entries: CollaborationSession[]) => {
      entries.map((entry: CollaborationSession) => {
        entry.start_time = moment(entry.start_time);
        entry.end_time = moment(entry.end_time);
      });
      this.collaborativeSessions = entries;
    });
  }
}
