import { Component, OnInit } from '@angular/core';
import { RegisterRequest } from '@app/classes/api/register-request'; 
import { UserApiService } from '@app/services/api/user-api.service';
import { HttpErrorResponse } from '@angular/common/http';
import { AuthService } from '@app/services/api/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerData: RegisterRequest = new RegisterRequest();
  registering: boolean = false;
  error: boolean = false;
  errorFields: string[] = [];
  success: boolean = false;
  selectingAvatar: boolean = false;

  constructor(private authService: AuthService, private userService: UserApiService, private router: Router) { }

  ngOnInit(): void {
    if (this.authService.isAuthenticated()) {
      this.router.navigateByUrl("");
    }
  }

  register(): void {
    this.registering = true;
    this.errorFields = [];
    this.userService.register(this.registerData).subscribe(() => {
      this.success = true;
    }, (err: HttpErrorResponse) => {
      this.registering = false;
      if (err.error && err.error.fields) {
        this.errorFields = err.error.fields;
      }
      this.error = true;
    });
  }

  cancelSelect(): void {
    this.selectingAvatar = false;
  }

  selectAvatar(): void {
    this.selectingAvatar = true;
  }

  changeAvatar(avatar: string): void {
    this.selectingAvatar = false;
    this.registerData.avatar = avatar;
  }

  hasError(field: string): boolean {
    return this.errorFields.includes(field);
  }
}
