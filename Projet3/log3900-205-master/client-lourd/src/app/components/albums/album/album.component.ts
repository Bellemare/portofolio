import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Album } from '@app/classes/api/album';
import { AlbumApiService } from '@app/services/api/album-api.service';
import { Drawing } from '@app/classes/api/drawing';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AuthService } from '@app/services/api/auth.service';
import { User } from '@app/classes/api/user';
import { Subscription } from 'rxjs';
import { DrawingApiService } from '@app/services/api/drawing-api.service';
import { RoomService } from '@app/services/api/room.service';
import { SocketClient } from '@app/classes/socket/socket-client';
import { PayloadTypes } from '@app/classes/socket/payload-types';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';
import { faSearch, faTimesCircle } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.scss']
})
export class AlbumComponent implements OnInit, OnDestroy {
  readonly searchIcon: IconDefinition = faSearch;
  readonly deleteIcon: IconDefinition = faTimesCircle;

  private albumId: number;
  album: Album;
  drawings: Drawing[] = [];
  user: User;

  showDrawingCreate: boolean = false;
  showAlbumEdit: boolean = false;
  showDrawingEdit: boolean = false;
  deletingDrawing: boolean = false;
  showDrawingView: boolean = false;
  confirming: boolean = false;
  confirmMessage: string = "";
  isSettingPassword: boolean = false;

  editingAlbum: Album;
  selectedDrawing: Drawing;
  private userSubscription: Subscription;
  private appClient: SocketClient;
  private drawingListener: number;
  private albumListener: number;

  namesFilter: string[] = [];
  pseudonymsFilter: string[] = [];
  endDateFilter: string|null = null;
  startDateFilter: string|null = null;

  hasInitialized: boolean = false;

  @ViewChild("nameFilter") private nameFilterInput: ElementRef<HTMLInputElement>;
  @ViewChild("pseudonymFilter") private pseudonymFilterInput: ElementRef<HTMLInputElement>;

  constructor(
    private albumService: AlbumApiService, 
    private activatedRoute: ActivatedRoute, 
    private drawingService: DrawingApiService, 
    private authService: AuthService,
    private router: Router,
    private roomService: RoomService,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params: Params) => {
      if (params.albumID) {
        this.albumId = params.albumID;
        this.loadAlbum();
        this.search();
      }
    });
    this.loadUser();
    this.listenChange();
  }

  ngOnDestroy(): void {
      this.userSubscription.unsubscribe();
      this.appClient?.unregister(PayloadTypes.DRAWING_CHANGE, this.drawingListener);
      this.appClient?.unregister(PayloadTypes.ALBUM_CHANGE, this.albumListener);
  }

  private listenChange(): void {
    this.roomService.getAppClient().then((client: SocketClient) => {
      this.appClient = client;
      this.drawingListener = client.onMessage(PayloadTypes.DRAWING_CHANGE, () => {
        this.search();
      });
      this.albumListener = client.onMessage(PayloadTypes.ALBUM_CHANGE, () => {
        this.loadAlbum();
      });
    });
  }

  setNameFilter(value: string): void {
    if (value != "") 
      this.namesFilter.unshift(value);
    if (this.nameFilterInput != undefined)
      this.nameFilterInput.nativeElement.value = "";
  }

  setPseudonymFilter(value: string): void {
    if (value != "")
      this.pseudonymsFilter.unshift(value);
    if (this.pseudonymFilterInput != undefined)
      this.pseudonymFilterInput.nativeElement.value = "";
  }

  search(): void {
    this.albumService.filterDrawings(this.albumId, {
      names_filter: this.namesFilter,
      pseudonyms_filter: this.pseudonymsFilter,
      start_date: this.startDateFilter == "" ? null:this.startDateFilter,
      end_date: this.endDateFilter == "" ? null:this.endDateFilter,
    }).subscribe((drawings: Drawing[]) => {
      this.drawings = drawings;
      this.setInit();
    });
  }

  private setInit(): void {
      if (!this.hasInitialized) {
        setTimeout(() => {
          this.hasInitialized = true;
        }, 200);
      }
  }

  hideAll(): void {
    this.showDrawingCreate = false;
    this.showAlbumEdit = false;
    this.confirming = false;
    this.deletingDrawing = false;
    this.showDrawingEdit = false;
    this.showDrawingView = false;
    this.isSettingPassword = false;
  }

  setPassword(drawing: Drawing): void {
    this.isSettingPassword = true;
    this.selectedDrawing = drawing;
  }

  passwordSet(password: string): void {
    if (this.selectedDrawing.owner_id == this.authService.getUser().id) {
      this.selectedDrawing.is_password_protected = password != "";
      this.hideAll();
    } else {
      this.router.navigateByUrl(`/editor/${this.selectedDrawing.id}`);
    }
  }

  editDrawing(drawing: Drawing): void {
    this.showDrawingEdit = true;
    this.selectedDrawing = JSON.parse(JSON.stringify(drawing));
  }

  onDrawingEdit(drawing: Drawing): void {
    const index = this.drawings.findIndex((d: Drawing) => d.id == drawing.id);
    if (this.drawings[index].album_id != drawing.album_id) {
      this.drawings[index].album_id = drawing.album_id;
      this.drawings[index].was_transfered = true;
    } else {
      this.drawings[index].name = drawing.name;
    }
    this.hideAll();
  }

  createDrawing(): void {
    this.showDrawingCreate = true;
  }

  drawingCreated(drawing: Drawing): void {
    this.router.navigateByUrl(`/editor/${drawing.id}`);
  }

  confirmed(): void {
    if (this.deletingDrawing) {
      this.onDeleteDrawing();
    } else {
      this.onRemoveAlbum();
    }
  }

  onRemoveAlbum(): void {
    if (this.isOwner() && this.album.id !== undefined) {
      this.albumService.deleteAlbum(this.album.id).subscribe(() => this.router.navigateByUrl("/album"));
    } else if (this.album.id !== undefined) {
      this.albumService.quitAlbum(this.album.id).subscribe(() => this.router.navigateByUrl("/album"));
    }
  }

  editAlbum(): void {
    this.editingAlbum = JSON.parse(JSON.stringify(this.album));
    this.showAlbumEdit = true;
  }

  deleteDrawing(drawing: Drawing): void {
    if (drawing.was_transfered) {
      this.removeDrawingFromList(drawing);
      return;
    }
    this.selectedDrawing = drawing;
    this.deletingDrawing = true;
    this.confirmMessage = `Voulez-vous vraiment supprimer le dessin ${drawing.name}?`;
  }

  onDeleteDrawing(): void {
    this.drawingService.deleteDrawing(this.selectedDrawing.id).subscribe(() => {
      this.removeDrawingFromList(this.selectedDrawing);
    });
    this.deletingDrawing = false;
  }

  onDrawingView(drawing: Drawing): void {
    this.hideAll();
    this.selectedDrawing = drawing;
    this.showDrawingView = true;
  }

  albumEdited(album: Album): void {
    this.hideAll();
    this.album.name = album.name;
    this.album.description = album.description;
  }

  removeAlbum(): void {
    this.confirming = true;
    if (this.isOwner()) {
      this.confirmMessage = "Voulez-vous vraiment supprimer cet album? Tous les dessins seront supprimés.";
    } else {
      this.confirmMessage = "Voulez-vous vraiment quitter cet album? Tous vos dessins seront transférés au propriétaire de l'album.";
    }
  }

  isOwner(): boolean {
    return this.user !== undefined && this.album !== undefined && this.user.id == this.album.owner_id;
  }

  private removeDrawingFromList(drawing: Drawing): void {
    const index = this.drawings.findIndex((d: Drawing) => d.id == drawing.id);
    if (index != -1) {
      this.drawings.splice(index, 1);
    }
  }

  private loadUser(): void {
    if (this.user == undefined)
      this.user = this.authService.getUser();

    this.userSubscription = this.authService.listenUserChange().subscribe((user: User) => {
      this.user = user
    });
  }

  private loadAlbum(): void {
    if (this.albumId > 0) {
      this.albumService.getAlbum(this.albumId).subscribe((album: Album) => {
        this.album = album;
      }, () => this.router.navigateByUrl("/album"));
    } else {
      this.album = {
        id: 0,
        name: "Colorimage",
        description: "Album public"
      };
    }
  }
}
