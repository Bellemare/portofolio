import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Album } from '@app/classes/api/album';
import { Drawing } from '@app/classes/api/drawing';
import { User } from '@app/classes/api/user';
import { PayloadTypes } from '@app/classes/socket/payload-types';
import { SocketClient } from '@app/classes/socket/socket-client';
import { AlbumApiService } from '@app/services/api/album-api.service';
import { AuthService } from '@app/services/api/auth.service';
import { ExpositionApiService } from '@app/services/api/expositions-api.service';
import { RoomService } from '@app/services/api/room.service';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';
import { faFolder, faFolderPlus, faInfoCircle, faPaintBrush, faPencilAlt, faSearch, faTimesCircle, faTrash, faUsers } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.scss']
})
export class AlbumsComponent implements OnInit, OnDestroy {
  readonly albumIcon: IconDefinition = faFolder;
  readonly albumAddIcon: IconDefinition = faFolderPlus;
  readonly searchIcon: IconDefinition = faSearch;
  readonly palette: IconDefinition = faPaintBrush;
  readonly albumInfo: IconDefinition = faInfoCircle;
  readonly albumEdit: IconDefinition = faPencilAlt;
  readonly usersIcon: IconDefinition = faUsers;
  readonly albumLeave: IconDefinition = faTimesCircle;
  readonly albumRemove: IconDefinition = faTrash;

  expositions: Album[] = [];
  userAlbums: Album[] = [];
  showAlbumDetail: boolean = false;
  showAlbumCreate: boolean = false;
  showAlbumEdit: boolean = false;
  showDrawingCreate: boolean = false;
  selectedAlbum: Album;
  isSearching: boolean = false;

  private user: User;
  private albumChangeListener: number;
  private appClient: SocketClient;

  confirming: boolean = false;
  confirmMessage: string = "";

  constructor(
    private albumService: AlbumApiService, 
    private authService: AuthService, 
    private router: Router, 
    private roomService: RoomService,
    private expositionService: ExpositionApiService
  ) { }

  ngOnInit(): void {
    this.loadAlbums();
    this.loadUser();
    this.loadExpositions();
    this.listenChange();
  }

  ngOnDestroy(): void {
    this.appClient?.unregister(PayloadTypes.ALBUM_CHANGE, this.albumChangeListener);
  }

  private loadExpositions(): void {
    this.expositionService.getAll().subscribe((expositions: Album[]) => {
      this.expositions = expositions;
    });
  }

  private loadAlbums(): void {
    this.albumService.getUserAlbums().subscribe((albums: Album[]) => {
      this.userAlbums = albums;
    });
  }

  private listenChange(): void {
    this.roomService.getAppClient().then((client: SocketClient) => {
      this.appClient = client;
      this.albumChangeListener = client.onMessage(PayloadTypes.ALBUM_CHANGE, () => {
        this.loadAlbums();
        this.loadExpositions();
      });
    });
  }

  private loadUser(): void {
    if (this.user == undefined)
      this.user = this.authService.getUser();

    this.authService.listenUserChange().subscribe((user: User) => this.user = user);
  }

  showDetail(album: Album): void {
    this.selectedAlbum = album;
    this.showAlbumDetail = true;
  }

  hideAll(): void {
    this.showAlbumDetail = false;
    this.showAlbumCreate = false;
    this.showAlbumEdit = false;
    this.showDrawingCreate = false;
    this.confirming = false;
    this.isSearching = false;
  }

  drawingCreated(drawing: Drawing): void {
    this.router.navigateByUrl(`/editor/${drawing.id}`);
  }

  createDrawing(): void {
    this.showDrawingCreate = true;
  }

  editAlbum(album: Album): void {
    this.hideAll();
    this.showAlbumEdit = true;
    this.selectedAlbum = JSON.parse(JSON.stringify(album));
  }

  removeAlbum(album: Album): void {
    this.selectedAlbum = album;
    this.confirming = true;
    if (this.isOwner(album)) {
      this.confirmMessage = `Voulez-vous vraiment supprimer l'album ${album.name}? Tous les dessins seront supprimés.`;
    } else {
      this.confirmMessage = `Voulez-vous vraiment quitter l'album ${album.name}? Tous vos dessins seront transférés au propriétaire de l'album.`;
    }
  }

  searchDrawing(): void {
    this.isSearching = true;
  }

  private executeDeleteAlbum(album: Album): void {
    this.confirming = false;
    if (album.id !== undefined) {
      this.albumService.deleteAlbum(album.id).subscribe(() => this.removeAlbumFromList(album));
    }
  }

  private executeQuitAlbum(album: Album): void {
    this.confirming = false;
    if (album.id !== undefined) {
      this.albumService.quitAlbum(album.id).subscribe(() => this.removeAlbumFromList(album));
    }
  }

  private removeAlbumFromList(album: Album): void {
    const index = this.userAlbums.findIndex((a: Album) => album.id == a.id);
    if (index != -1) {
      this.userAlbums.splice(index, 1);
    }
  }

  onRemoveAlbum(album: Album): void {
    this.hideAll();
    if (this.isOwner(album)) {
      this.executeDeleteAlbum(album);
    } else {
      this.executeQuitAlbum(album);
    }
  }

  createAlbum(): void {
    this.showAlbumCreate = true;
  }

  albumCreated(album: Album): void {
    this.router.navigateByUrl(`/album/${album.id}`);
  }

  albumEdited(album: Album): void {
    this.hideAll();
    const index = this.userAlbums.findIndex((a: Album) => album.id == a.id);
    if (index != -1) {
      this.userAlbums[index] = album;
    }
  }

  isOwner(album: Album): boolean {
    return this.user !== undefined && this.user.id == album.owner_id;
  }

  get ownedAlbums(): Album[] {
    return this.userAlbums.filter((album: Album) => this.isOwner(album));
  }

  get joinedAlbums(): Album[] {
    return this.userAlbums.filter((album: Album) => !this.isOwner(album));
  }
}
