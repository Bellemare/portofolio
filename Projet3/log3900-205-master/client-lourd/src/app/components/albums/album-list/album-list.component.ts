import { Component } from '@angular/core';
import { Album } from '@app/classes/api/album';
import { AlbumApiService } from '@app/services/api/album-api.service';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';
import { faFolder } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-album-list',
  templateUrl: './album-list.component.html',
  styleUrls: ['./album-list.component.scss']
})
export class AlbumListComponent {
  readonly albumIcon: IconDefinition = faFolder;

  albumList: Album[] = [];
  showAlbumJoin: boolean = false;
  selectedAlbum: Album;
  
  nameFilter: string = "";

  constructor(private albumService: AlbumApiService) { }

  ngOnInit(): void {
    this.albumService.getAlbumList().subscribe((albums: Album[]) => {
      this.albumList = albums;
    });
  }

  hideAll(): void {
    this.showAlbumJoin = false;
  }

  joinAlbum(album: Album): void {
    this.selectedAlbum = album;
    this.showAlbumJoin = true;
  }

  afterJoin(album: Album): void {
    this.hideAll();
    const index = this.albumList.findIndex((a: Album) => a.id == album.id);
    if (index != -1) {
      this.albumList.splice(index, 1);
    }
  }

  get albums(): Album[] {
    if (this.nameFilter == "")
      return this.albumList;

    const filterRegexp = new RegExp(this.nameFilter);
    return this.albumList.filter((album: Album) => filterRegexp.test(album.name.toLowerCase()));
  }
}
