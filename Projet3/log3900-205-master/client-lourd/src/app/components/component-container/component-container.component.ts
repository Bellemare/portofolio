import { Component, ComponentFactoryResolver, ComponentRef, OnInit, ViewChild } from '@angular/core';
import { ToolParameter } from '@app/classes/tool-parameter';
import { AppDirective } from '../app/app.directive';

@Component({
  selector: 'app-component-container',
  templateUrl: './component-container.component.html',
  styleUrls: ['./component-container.component.scss']
})
export class ComponentContainerComponent implements OnInit {
    private static INDEX_ERROR: number = -1;
    private static componentFactoryResolver: ComponentFactoryResolver;
    private static appComponentHost: AppDirective;
    private static instances: ComponentRef<Component>[] = [];
    @ViewChild(AppDirective, { static: true }) private appHost: AppDirective;

    constructor(
        componentFactoryResolver: ComponentFactoryResolver,
    ) {
        ComponentContainerComponent.componentFactoryResolver = componentFactoryResolver;
    }

    static createComponent<T>(appComponent: new (...args: ToolParameter[]) => T): T {
        const componentFactory = ComponentContainerComponent.componentFactoryResolver.resolveComponentFactory(appComponent);

        const viewContainerRef = ComponentContainerComponent.appComponentHost.viewContainerRef;

        const componentRef = viewContainerRef.createComponent<T>(componentFactory);
        ComponentContainerComponent.instances.push(componentRef);
        return componentRef.instance;
    }

    static destroyComponent<T>(instance: T): void {
        const index = ComponentContainerComponent.instances.findIndex((ref: ComponentRef<Component>) => ref.instance === instance);
        if (index !== ComponentContainerComponent.INDEX_ERROR) {
            ComponentContainerComponent.instances[index].destroy();
            ComponentContainerComponent.instances.splice(index, 1);
        }
    }

    ngOnInit(): void {
        ComponentContainerComponent.appComponentHost = this.appHost;
    }
}
