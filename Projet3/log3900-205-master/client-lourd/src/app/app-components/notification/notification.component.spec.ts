import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NotificationEntry } from '@app/classes/notification-entry';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { of } from 'rxjs';
import { NotificationComponent } from './notification.component';

// tslint:disable:no-string-literal
// tslint:disable:no-magic-numbers
describe('NotificationComponent', () => {
    let component: NotificationComponent;
    let fixture: ComponentFixture<NotificationComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [NotificationComponent],
            imports: [FontAwesomeModule],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NotificationComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should subscribe to notifications', () => {
        const observable = of<NotificationEntry>();
        spyOn(component['notificationService'], 'getNotification').and.returnValue(observable);
        const subscribeSpy = spyOn(observable, 'subscribe');
        component.ngOnInit();
        expect(subscribeSpy).toHaveBeenCalled();
    });

    it('should handle notification correctly', async (done: DoneFn) => {
        const observable = of<NotificationEntry>({} as NotificationEntry);
        const initNotificationSpy = spyOn(component, 'initNotification');
        spyOn(component['notificationService'], 'getNotification').and.returnValue(observable);
        observable.subscribe((notification: NotificationEntry) => {
            setTimeout(() => {
                expect(initNotificationSpy).toHaveBeenCalled();
                done();
            }, 100);
        });
        component.ngOnInit();
        component['notificationService']['notification'].next({} as NotificationEntry);
    });

    it('should init notification correctly', () => {
        component.initNotification({} as NotificationEntry);
        expect(component.notifications.length).toEqual(1);
    });

    it('should remove notification after correct time', async (done: DoneFn) => {
        const timeout = 50;
        const entry = { timeMs: timeout } as NotificationEntry;
        component.initNotification(entry);
        setTimeout(() => {
            expect(component.notifications.length).toEqual(0);
            done();
        }, timeout + 10);
    });

    it('should check if is success', () => {
        expect(component.isSuccess(200)).toEqual(true);
        expect(component.isSuccess(299)).toEqual(true);
        expect(component.isSuccess(300)).toEqual(false);
    });

    it('should check if is warning', () => {
        expect(component.isWarning(300)).toEqual(true);
        expect(component.isWarning(399)).toEqual(true);
        expect(component.isWarning(400)).toEqual(false);
    });

    it('should check if is error', () => {
        expect(component.isError(400)).toEqual(true);
        expect(component.isError(500)).toEqual(true);
    });
});
