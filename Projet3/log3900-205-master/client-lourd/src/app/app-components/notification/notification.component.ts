import { Component, OnInit } from '@angular/core';
import { NotificationEntry } from '@app/classes/notification-entry';
import { NotificationService } from '@app/services/notification/notification.service';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'app-notification',
    templateUrl: './notification.component.html',
    styleUrls: ['./notification.component.scss'],
})
export class NotificationComponent implements OnInit {
    readonly deleteIcon: IconDefinition = faTimesCircle;
    readonly WARNING_NOTIFICATION_THRESHOLD: number = 300;
    readonly ERROR_NOTIFICATION_THRESHOLD: number = 400;
    notifications: NotificationEntry[] = [];

    constructor(private notificationService: NotificationService) {}

    ngOnInit(): void {
        this.notificationService.getNotification().subscribe((notification: NotificationEntry | null) => {
            if (notification !== null) {
                this.initNotification(notification);
            }
        });
    }

    initNotification(entry: NotificationEntry): void {
        const time = entry.timeMs !== undefined ? entry.timeMs : this.notificationService.DEFAULT_NOTIFICATION_TIME;
        this.notifications.push(entry);
        setTimeout(() => {
            const index = this.notifications.findIndex((notif: NotificationEntry) => notif === entry);
            this.notifications.splice(index, 1);
        }, time);
    }

    isSuccess(code: number): boolean {
        return code < this.WARNING_NOTIFICATION_THRESHOLD;
    }

    isWarning(code: number): boolean {
        return code >= this.WARNING_NOTIFICATION_THRESHOLD && code < this.ERROR_NOTIFICATION_THRESHOLD;
    }

    isError(code: number): boolean {
        return code >= this.ERROR_NOTIFICATION_THRESHOLD;
    }
}
