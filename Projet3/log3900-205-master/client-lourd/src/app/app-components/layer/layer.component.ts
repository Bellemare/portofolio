import { AfterViewInit, Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { DrawingEvent } from '@app/classes/drawing/drawing-event';
import { DrawingEventWrapper } from '@app/classes/drawing/drawing-event-wrapper';
import { ToolType } from '@app/classes/drawing/tool-type';
import { LayerEventType } from '@app/classes/socket/layer-event-type';
import { DrawingService } from '@app/services/components/drawing/drawing.service';
import { CouchesService } from '@app/services/couches/couches.service';
import { SynchronizerService } from '@app/services/synchronization/synchronizer.service';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';
import { faCaretLeft, faCaretRight, faCheck, faPencilAlt, faTimes, faTrash } from '@fortawesome/free-solid-svg-icons';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-layer',
  templateUrl: './layer.component.html',
  styleUrls: ['./layer.component.scss']
})
export class LayerComponent implements AfterViewInit {
  readonly deleteIcon: IconDefinition = faTrash;
  readonly editIcon: IconDefinition = faPencilAlt;
  readonly leftIcon: IconDefinition = faCaretLeft;
  readonly rightIcon: IconDefinition = faCaretRight;
  readonly confirmIcon: IconDefinition = faCheck;
  readonly cancelIcon: IconDefinition = faTimes;

  @Output() onDelete: EventEmitter<void> = new EventEmitter();
  @Output() onLayerIndexChange: EventEmitter<number> = new EventEmitter();

  @Input() disableMove: boolean = false;
  @Input() maxLayer: number = 0;
  @Input() layerIndex: number = 0;
  @Input() isCurrentSelection: boolean = false;
  @Input() set layer(layer: DrawingEventWrapper) {
    this._layer = layer;
    if (this.subscription != undefined)
      this.subscription.unsubscribe();

    this.subscription = this._layer.render.subscribe(() => {
      this.loadLayer();
    });
    this.loadLayer();
  }
  _layer: DrawingEventWrapper;
  isEditing: boolean = false;
  layerIndexEdit: number = 0;
  private subscription: Subscription;

  @ViewChild('layerCanvas') private layerCanvas: ElementRef<HTMLCanvasElement>;
  @ViewChild('editInput') private editInput: ElementRef<HTMLInputElement>;
  private ctx: CanvasRenderingContext2D;

  constructor(private couchesService: CouchesService, private drawingService: DrawingService, private synchronizer: SynchronizerService) {}

  ngAfterViewInit(): void {
    if (this.layerCanvas !== undefined) {
      this.ctx = this.layerCanvas.nativeElement.getContext("2d") as CanvasRenderingContext2D;
      if (this._layer !== undefined) {
        this.loadLayer();
      }
    } 
  }

  editLayer(): void {
    if (this.disableMove)
      return;

    this.layerIndexEdit = this.layerIndex;
    this.isEditing = true;
    setTimeout(() => {
      if (this.editInput != undefined)
        this.editInput.nativeElement.focus();
    }, 10);
  }

  confirmEdit(): void {
    this.onLayerIndexChange.emit(this.layerIndexEdit);
    this.isEditing = false;
  }

  setLayerIndex(index: number): void {
    this.layerIndexEdit = Math.max(Math.min(index, this.maxLayer), 0);
    if (this.editInput != undefined)
      this.editInput.nativeElement.value = this.layerIndexEdit.toString();
  }

  cancelEdit(): void {
    this.isEditing = false;
  }

  select(): void {
    if (this._layer.isSelected) {
      return 
    }

    this.drawingService.setCurrentTool(ToolType.SELECTION);
    this.synchronizer.layerEvent(LayerEventType.SELECT, this._layer.drawingEvent as DrawingEvent, this.couchesService.getDrawingIndex(this._layer));
  }

  moveIndex(offset: number): void {
    this.onLayerIndexChange.emit(this.layerIndex + offset);
  }

  private loadLayer(): void {
    if (this._layer.image == undefined || this.layerCanvas == undefined || this.ctx == undefined) 
      return;

    this.ctx.clearRect(0, 0, this.layerCanvas.nativeElement.width, this.layerCanvas.nativeElement.height);
    this.loadLayerData(this._layer.image);
  }

  private loadLayerData(img: HTMLCanvasElement): void {
    const canvasSize = { x: this.layerCanvas.nativeElement.width, y: this.layerCanvas.nativeElement.height };
    const ratioWidth = canvasSize.x / img.width;
    const ratioHeight = canvasSize.y / img.height;
    const ratio = Math.min(ratioWidth, ratioHeight);
    const adjustedSize = { x: img.width * ratio, y: img.height * ratio };
    const corner = {
        x: (canvasSize.x - adjustedSize.x) / 2,
        y: (canvasSize.y - adjustedSize.y) / 2,
    };
    this.ctx.drawImage(img, corner.x, corner.y, adjustedSize.x, adjustedSize.y);
  }

  get selectionClass(): string {
    if (this.isCurrentSelection)
      return 'current-selection';

    return this.isSelected ? 'selected':'';
  }

  get isSelected(): boolean {
    return this._layer?.isSelected;
  }
}
