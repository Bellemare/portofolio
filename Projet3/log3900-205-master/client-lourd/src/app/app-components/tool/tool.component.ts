import { Component, OnInit } from '@angular/core';
import { ToolAttributesComponent } from '@app/app-components/tool-components/tool-attributes/tool-attributes.component';
import { ToolClasses } from '@app/classes/drawing/tool-classes';
import { ToolProperties } from '@app/classes/tool-properties/tool-properties';
import { TooltipHandler } from '@app/classes/tooltip-handler';
import { DrawingService } from '@app/services/components/drawing/drawing.service';
import { ToolService } from '@app/services/tools/tool.service';
import { faExclamationTriangle, IconDefinition } from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'app-tool',
    templateUrl: './tool.component.html',
    styleUrls: ['./tool.component.scss'],
})
export class ToolComponent implements OnInit {
    private readonly MOUSE_EVENTS_DISABLED_CLASS: string = 'mouse-events-disabled';
    private readonly MOUSE_EVENTS_ENABLED_CLASS: string = 'mouse-events-enabled';

    currentTool: ToolService;

    showAttributes: boolean = true;
    showToolList: boolean = false;

    currentToolAttributes: ToolProperties;

    constructor(
        public drawingService: DrawingService,
        private toolClasses: ToolClasses,
    ) {}

    ngOnInit(): void {
        this.listenToolChange();
    }

    private listenToolChange(): void {
        this.drawingService.getCurrentTool().subscribe((tool: ToolService) => {
            this.currentTool = tool;
        });
    }

    toolHeaderClicked(): void {
        this.showToolList = !this.showToolList;
    }

    get currenToolLabel(): string {
        return this.currentTool != undefined ? this.currentTool.TOOL_LABEL : '';
    }

    get currenToolIcon(): IconDefinition | null {
        return this.currentTool != undefined ? this.currentTool.TOOL_ICON : faExclamationTriangle;
    }

    get toolComponent(): new (t: ToolService) => ToolAttributesComponent {
        return this.toolClasses.getToolComponent(this.currentTool.TOOL_NAME);
    }

    get toolList(): ToolService[] {
        return this.drawingService.getToolList();
    }

    get currentEventsClass(): string {
        return this.drawingService.tool.isDrawing ? this.MOUSE_EVENTS_DISABLED_CLASS : this.MOUSE_EVENTS_ENABLED_CLASS;
    }

    get tooltipDelay(): number {
        return TooltipHandler.DEFAULT_DELAY;
    }

    get tooltipPosition(): string {
        return TooltipHandler.DEFAULT_POSITION;
    }

    getTooltipText(tool: ToolService): string {
        return this.currentTool !== undefined ? TooltipHandler.arrayToString(tool.toolTooltips) : '';
    }

    setTool(tool: ToolService): void {
        this.showToolList = false;
        this.drawingService.setCurrentTool(tool.TOOL_TYPE);
    }
}
