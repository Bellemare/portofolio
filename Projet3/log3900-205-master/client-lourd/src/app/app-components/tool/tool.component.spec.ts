import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponentsModule } from '@app/app-components/app-components.module';
import { AlphaSliderComponent } from '@app/app-components/color/alpha-slider/alpha-slider.component';
import { ColorPaletteComponent } from '@app/app-components/color/color-palette/color-palette.component';
import { ColorPickerComponent } from '@app/app-components/color/color-picker/color-picker.component';
import { ColorSliderComponent } from '@app/app-components/color/color-slider/color-slider.component';
import { GridAttributesComponent } from '@app/app-components/grid-attributes/grid-attributes.component';
import { ToolAttributesContainerComponent } from '@app/app-components/tool-attributes/tool-attributes-container/tool-attributes-container.component';
import { ToolClasses } from '@app/classes/drawing/tool-classes';
import { Tooltip } from '@app/classes/tooltip';
import { TooltipHandler } from '@app/classes/tooltip-handler';
import { MaterialModule } from '@app/material/material.module';
import { AttributesManagerService } from '@app/services/api/attributes-manager.service';
import { AutoSaveService } from '@app/services/api/auto-save.service';
import { DrawingService } from '@app/services/components/drawing/drawing.service';
import { KeyBindingResolverService } from '@app/services/key-binding/key-binding-resolver.service';
import { ResizeService } from '@app/services/resize/resize.service';
import { ColorService } from '@app/services/tools/color.service';
import { ToolService } from '@app/services/tools/tool.service';
import { UndoRedoService } from '@app/services/undo-redo/undo-redo.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faDiceOne } from '@fortawesome/free-solid-svg-icons';
import { ToolComponentStub } from './tool-component-stub.spec';
import { ToolComponent } from './tool.component';

class ToolStub extends ToolService {
    readonly TOOL_NAME: string = 'stub';
    readonly TOOL_ICON: IconDefinition = faDiceOne;
    readonly TOOL_LABEL: string = 'stub';
    readonly TOOLTIPS: Tooltip[] = [
        {
            shortcut: 'x',
            name: 'test',
        },
    ];
}

// tslint:disable:no-string-literal
// tslint:disable:no-empty
// tslint:disable:no-any
describe('ToolComponent', () => {
    let component: ToolComponent;
    let fixture: ComponentFixture<ToolComponent>;

    let drawingService: DrawingService;
    let tool: ToolStub;
    let toolComponent: ToolComponentStub;

    let listenToolChangeSpy: jasmine.Spy<any>;

    beforeEach(async(() => {
        const attr = new AttributesManagerService();
        const color = new ColorService(attr);
        const keybind = new KeyBindingResolverService();
        tool = new ToolStub(attr, color, keybind);
        toolComponent = new ToolComponentStub(tool);
        const toolClassesSpy = jasmine.createSpyObj('ToolClasses', ['getToolService', 'getToolComponent']);
        toolClassesSpy.getToolService.and.returnValue(tool);
        toolClassesSpy.getToolComponent.and.returnValue(toolComponent);
        const undoRedoService = new UndoRedoService(keybind);
        const autoSaveService = new AutoSaveService();
        drawingService = new DrawingService(
            keybind,
            toolClassesSpy,
            undoRedoService,
            new ResizeService(undoRedoService, autoSaveService),
            autoSaveService,
        );

        TestBed.configureTestingModule({
            providers: [
                { provide: DrawingService, useValue: drawingService },
                { provide: ToolClasses, useValue: toolClassesSpy },
            ],
            declarations: [
                ToolComponent,
                ColorPickerComponent,
                ColorPaletteComponent,
                ColorSliderComponent,
                AlphaSliderComponent,
                ToolAttributesContainerComponent,
                GridAttributesComponent,
            ],
            imports: [MaterialModule, FontAwesomeModule, BrowserAnimationsModule, AppComponentsModule],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ToolComponent);
        component = fixture.componentInstance;
        component.showAttributes = false;
        listenToolChangeSpy = spyOn<any>(component, 'listenToolChange');
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should hide/show tool list', () => {
        component['currentTool'] = tool;
        component.showToolList = false;
        component.toolHeaderClicked();
        expect(component.showToolList).toEqual(true);
    });

    it('should get tool list', () => {
        component['currentTool'] = tool;
        const drawingServiceSpy = spyOn(drawingService, 'getToolList');
        drawingServiceSpy.and.returnValue([]);
        expect(component.toolList).toEqual([]);
        expect(drawingServiceSpy).toHaveBeenCalled();
    });

    it('should get tool label', () => {
        component['currentTool'] = tool;
        expect(component.currenToolLabel).toEqual(tool.TOOL_LABEL);
    });

    it('should get tooltip', () => {
        component['currentTool'] = tool;
        expect(component.getTooltipText(tool)).toEqual(TooltipHandler.arrayToString(tool.TOOLTIPS));
    });

    it('should get tool icon', () => {
        component['currentTool'] = tool;
        expect(component.currenToolIcon).toEqual(tool.TOOL_ICON);
    });

    it('should get correct event class', () => {
        component['currentTool'] = tool;

        tool['mouseDown'] = true;
        expect(component.currentEventsClass).toEqual(component['MOUSE_EVENTS_DISABLED_CLASS']);

        tool['mouseDown'] = false;
        expect(component.currentEventsClass).toEqual(component['MOUSE_EVENTS_ENABLED_CLASS']);
    });

    it('should get tool component', () => {
        component['currentTool'] = tool;

        expect(component.toolComponent).toEqual(toolComponent);
    });

    it('should set tool on drawingService', () => {
        component['currentTool'] = tool;

        const drawingServiceSpy = spyOn(drawingService, 'setCurrentTool');
        component.setTool(tool);
        expect(drawingServiceSpy).toHaveBeenCalledWith(tool.TOOL_NAME);
    });

    it('should listen to tool change', () => {
        const getCurrentToolSpy = spyOn(drawingService, 'getCurrentTool').and.callThrough();
        listenToolChangeSpy.and.callThrough();
        component['listenToolChange']();
        expect(getCurrentToolSpy).toHaveBeenCalled();
    });
});
