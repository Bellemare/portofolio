import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AlbumJoinRequest } from '@app/classes/api/album-join-request';
import { PayloadTypes } from '@app/classes/socket/payload-types';
import { SocketClient } from '@app/classes/socket/socket-client';
import { AlbumApiService } from '@app/services/api/album-api.service';
import { RoomService } from '@app/services/api/room.service';
import { NotificationPayload } from '@app/classes/socket/notification-payload';
import { AudioService } from '@app/services/audio/audio.service';
import { SoundType } from '@app/classes/sound-types';

@Component({
  selector: 'app-app-notifications',
  templateUrl: './app-notifications.component.html',
  styleUrls: ['./app-notifications.component.scss']
})
export class AppNotificationsComponent implements OnInit {
  @Output() onNewNotifications: EventEmitter<void> = new EventEmitter();

  joinRequests: AlbumJoinRequest[] = [];
  notifications: string[] = [];

  constructor(private albumService: AlbumApiService, private roomService: RoomService, private audioService: AudioService) { }

  ngOnInit(): void {
    this.getJoinRequests();
    this.roomService.getAppClient().then((client: SocketClient) => {
      client.onMessage(PayloadTypes.REFRESH_ALBUM_JOIN_REQUESTS, () => {
        this.getJoinRequests();
      });
      client.onMessage(PayloadTypes.NOTIFICATION, (notification: NotificationPayload) => {
        this.audioService.playSound(SoundType.NewJoinRequest);
        this.newNotification(notification);
      });
    });
  }

  private newNotification(notification: NotificationPayload): void {
    this.onNewNotifications.emit();
    this.notifications.push(notification.message);
  }

  removeNotification(index: number): void {
    this.notifications.splice(index, 1);
  }

  private getJoinRequests(): void {
    this.albumService.getAllJoinRequests().subscribe((requests: AlbumJoinRequest[]) => {
      if (requests.length > 0) {
        this.audioService.playSound(SoundType.NewJoinRequest);        
        this.onNewNotifications.emit();
      }
      this.joinRequests = requests;
    });
  }

  private removeJoinRequest(request: AlbumJoinRequest): void {
    const index = this.joinRequests.findIndex((req: AlbumJoinRequest) => req.id == request.id);
    if (index != -1) {
      this.joinRequests.splice(index, 1);
    }
  }

  declineJoinRequest(request: AlbumJoinRequest): void {
    this.albumService.declineJoinRequest(request.album_id, request.id).subscribe(() => {
      this.removeJoinRequest(request);
    }, () => this.removeJoinRequest(request));
  }

  acceptJoinRequest(request: AlbumJoinRequest): void {
    this.albumService.acceptJoinRequest(request.album_id, request.id).subscribe(() => {
      this.removeJoinRequest(request);
    }, () => this.removeJoinRequest(request));
  }

  get hasNotifications(): boolean {
    return this.joinRequests.length > 0 || this.notifications.length > 0;
  }
}
