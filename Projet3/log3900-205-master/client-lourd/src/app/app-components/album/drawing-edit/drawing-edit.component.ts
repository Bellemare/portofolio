import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Album } from '@app/classes/api/album';
import { Drawing } from '@app/classes/api/drawing';
import { FieldsErrorResponse } from '@app/classes/api/error-response';
import { AlbumApiService } from '@app/services/api/album-api.service';
import { DrawingApiService } from '@app/services/api/drawing-api.service';

@Component({
  selector: 'app-drawing-edit',
  templateUrl: './drawing-edit.component.html',
  styleUrls: ['./drawing-edit.component.scss']
})
export class DrawingEditComponent {
  @Output() onClose: EventEmitter<void> = new EventEmitter();
  @Output() onEdit: EventEmitter<Drawing> = new EventEmitter();

  @Input() set open(open: boolean) {
    if (open) {
      this.onOpen();
    }
  }
  @Input("drawing") set _drawing(drawing: Drawing) {
    this.drawing = drawing;
    this.reset();
  }
  drawing: Drawing;
  albums: Album[] = [];
  isEditing: boolean = false;
  visibility: string = "public";
  password: string = "";

  private errorFields: string[] = [];

  constructor(private albumService: AlbumApiService, private drawingService: DrawingApiService) {}

  private onOpen(): void {
    this.albumService.getUserAlbums().subscribe((albums: Album[]) => {
      this.albums = albums;
      this.albums.splice(0, 1);
    });
  }

  setVisibility(visibility: string): void {
    this.visibility = visibility;
    if (this.visibility == "public") {
      this.drawing.album_id = 0;
    } else {
      this.drawing.album_id = this.albums[0].id as number;
    }
  }

  close(): void {
    this.reset();
    this.onClose.emit();
  }

  reset(): void {
    this.visibility = this.drawing !== undefined && this.drawing.album_id > 0 ? "private":"public";
    this.errorFields = [];
    this.password = "";
  }

  hasError(field: string): boolean {
    return this.errorFields !== undefined && this.errorFields.includes(field);
  }

  updateDrawing(): void {
    this.isEditing = true;
    this.drawingService.updateDrawing({
      id: this.drawing.id,
      name: this.drawing.name,
      album_id: this.visibility == "public" ? 0:this.drawing.album_id,
      password: this.password,
    }).subscribe(() => {
      this.isEditing = false;
      this.reset();
      this.drawing.is_password_protected = this.password != "";
      this.onEdit.emit(this.drawing);
    }, (error: HttpErrorResponse) => {
      this.isEditing = false;
      const err: FieldsErrorResponse = error.error
      if (err !== undefined && err.fields !== undefined) {
        this.errorFields = err.fields;
      }
    });
  }
}
