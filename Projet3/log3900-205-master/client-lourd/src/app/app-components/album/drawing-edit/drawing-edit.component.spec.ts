import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrawingEditComponent } from './drawing-edit.component';

describe('DrawingEditComponent', () => {
  let component: DrawingEditComponent;
  let fixture: ComponentFixture<DrawingEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrawingEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrawingEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
