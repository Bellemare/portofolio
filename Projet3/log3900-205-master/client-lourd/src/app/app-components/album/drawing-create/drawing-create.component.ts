import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Album } from '@app/classes/api/album';
import { Drawing } from '@app/classes/api/drawing';
import { DrawingModel } from '@app/classes/api/drawing-model';
import { FieldsErrorResponse } from '@app/classes/api/error-response';
import { AlbumApiService } from '@app/services/api/album-api.service';
import { DrawingApiService } from '@app/services/api/drawing-api.service';

@Component({
  selector: 'app-drawing-create',
  templateUrl: './drawing-create.component.html',
  styleUrls: ['./drawing-create.component.scss']
})
export class DrawingCreateComponent {
  @Output() onClose: EventEmitter<void> = new EventEmitter();
  @Output() onCreated: EventEmitter<Drawing> = new EventEmitter();

  @Input() set open(open: boolean) {
    if (open) {
      this.onOpen();
    }
  }
  @Input() albumId: number | undefined;
  albums: Album[] = [];
  drawing: DrawingModel;
  isCreating: boolean = false;
  lockAlbum: boolean = false;
  visibility: string = "public";

  private errorFields: string[] = [];

  constructor(private albumService: AlbumApiService, private drawingService: DrawingApiService) {}

  private onOpen(): void {
    this.lockAlbum = this.albumId !== undefined;
    this.albumService.getUserAlbums().subscribe((albums: Album[]) => {
      this.albums = albums;
      this.albums.splice(0, 1);
    });
    this.reset();
  }

  setVisibility(visibility: string): void {
    this.visibility = visibility;
    if (this.visibility == "public") {
      this.drawing.album_id = 0;
    } else {
      this.drawing.album_id = this.albums[0].id as number;
    }
  }
  
  close(): void {
    this.reset();
    this.onClose.emit();
  }

  reset(): void {
    this.drawing = {
      name:"",
      album_id: this.albumId != undefined ? this.albumId:0,
    };
    this.visibility = this.albumId !== undefined && this.albumId > 0 ? "private":"public";
    this.errorFields = [];
  }

  hasError(field: string): boolean {
    return this.errorFields !== undefined && this.errorFields.includes(field);
  }

  createDrawing(): void {
    this.isCreating = true;
    this.drawing.album_id = this.visibility == "public" ? 0:this.drawing.album_id;
    this.drawingService.create(this.drawing).subscribe((drawing: Drawing) => {
      this.isCreating = false;
      this.reset();
      drawing.is_password_protected = this.drawing.password != "";
      this.onCreated.emit(drawing);
    }, (error: HttpErrorResponse) => {
      this.isCreating = false;
      const err: FieldsErrorResponse = error.error
      if (err !== undefined && err.fields !== undefined) {
        this.errorFields = err.fields;
      }
    });
  }
}
