import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrawingCreateComponent } from './drawing-create.component';

describe('DrawingCreateComponent', () => {
  let component: DrawingCreateComponent;
  let fixture: ComponentFixture<DrawingCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrawingCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrawingCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
