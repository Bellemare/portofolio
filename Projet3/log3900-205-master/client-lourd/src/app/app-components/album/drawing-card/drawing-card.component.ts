import { AfterViewInit, Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Drawing } from '@app/classes/api/drawing';
import { ImgViewerHelper } from '@app/classes/img-viewer-helper';
import { AuthService } from '@app/services/api/auth.service';
import { DrawingApiService } from '@app/services/api/drawing-api.service';
import { ExpositionApiService } from '@app/services/api/expositions-api.service';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';
import { faLock, faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import * as moment from 'moment';

@Component({
  selector: 'app-drawing-card',
  templateUrl: './drawing-card.component.html',
  styleUrls: ['./drawing-card.component.scss']
})
export class DrawingCardComponent implements AfterViewInit {
  readonly OK_COLOR: string = "#96f051";
  readonly WARN_COLOR: string = "#f0ed4d";
  readonly FULL_COLOR: string = "#f05151";
  readonly deleteIcon: IconDefinition = faTimesCircle;
  readonly lockIcon: IconDefinition = faLock;

  @Output() onDelete: EventEmitter<void> = new EventEmitter();
  @Output() onEdit: EventEmitter<void> = new EventEmitter();
  @Output() onView: EventEmitter<void> = new EventEmitter();
  @Output() onSetPassword: EventEmitter<void> = new EventEmitter();

  @Input() set drawing(drawing: Drawing) {
    this._drawing = drawing;
    this._drawing.created_at = moment(this._drawing.created_at);
    this.loadImage();
  }

  _drawing: Drawing;
  private ctx: CanvasRenderingContext2D;

  @ViewChild('drawingCanvas') private drawingCanvas: ElementRef<HTMLCanvasElement>;

  constructor(
    private router: Router, 
    private expositionService: ExpositionApiService, 
    private drawingApiService: DrawingApiService,
    private authService: AuthService,
  ) {}

  ngAfterViewInit(): void {
    if (this.drawingCanvas !== undefined) {
      this.ctx = this.drawingCanvas.nativeElement.getContext("2d") as CanvasRenderingContext2D;
      if (this._drawing !== undefined) {
        this.loadImage();
      }
    } 
  }

  setPassword(): void {
    this.onSetPassword.emit();
  }

  unsetPassword(): void {
    this.drawingApiService.updateDrawing({
      name: this._drawing.name,
      album_id: this._drawing.album_id,
      id: this._drawing.id,
      password: "",
    }).subscribe(() => this._drawing.is_password_protected = false);
  }

  navigateToDrawing(): void {
    if (this._drawing == undefined) 
      return;

    if (this._drawing.is_password_protected && !this.isOwner) {
      this.onSetPassword.emit();
    } else {
      this.router.navigateByUrl(`/editor/${this._drawing.id}`);
    }
  }

  addDrawingToExposition(): void {
    this.expositionService.addDrawingToExposition(this._drawing.album_id, this._drawing.id).subscribe(() => {
      this._drawing.is_exposed = true;
    });
  }

  removeDrawingFromExposition(): void {
    this.expositionService.removeDrawingFromExposition(this._drawing.album_id, this._drawing.id).subscribe(() => {
      this._drawing.is_exposed = false;
    });
  }

  modify(): void {
    this.onEdit.emit();
  }

  delete(): void {
    this.onDelete.emit();
  }

  view(): void {
    this.onView.emit();
  }

  private loadImage(): void {
    if (this._drawing.image == "" || this.drawingCanvas == undefined || this.ctx == undefined) 
      return;

    const img = document.createElement('img');
    img.onload = ImgViewerHelper.loadImageData.bind(this, this.drawingCanvas.nativeElement, img); 
    img.src = this._drawing.image;
  }

  get activeCollaboratorsColor(): string {
    const activeCollaborators = this._drawing == undefined || this._drawing.active_collaborators == undefined ? 0:this._drawing.active_collaborators;
   
    if (activeCollaborators == 4)
      return this.FULL_COLOR;
    else if (activeCollaborators == 3)
      return this.WARN_COLOR;

    return this.OK_COLOR;
  }

  get isOwner(): boolean {
    return this.authService.getUser() !== undefined && this._drawing !== undefined && this._drawing.owner_id == this.authService.getUser().id;
  }
}
