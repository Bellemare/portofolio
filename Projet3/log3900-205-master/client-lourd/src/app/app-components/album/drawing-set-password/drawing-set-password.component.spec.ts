import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrawingSetPasswordComponent } from './drawing-set-password.component';

describe('DrawingSetPasswordComponent', () => {
  let component: DrawingSetPasswordComponent;
  let fixture: ComponentFixture<DrawingSetPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrawingSetPasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrawingSetPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
