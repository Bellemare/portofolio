import { AfterViewInit, Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { Drawing } from '@app/classes/api/drawing';
import { AuthService } from '@app/services/api/auth.service';
import { DrawingApiService } from '@app/services/api/drawing-api.service';

@Component({
  selector: 'app-drawing-set-password',
  templateUrl: './drawing-set-password.component.html',
  styleUrls: ['./drawing-set-password.component.scss']
})
export class DrawingSetPasswordComponent implements AfterViewInit {
  @Output() onCancel: EventEmitter<void> = new EventEmitter<void>();
  @Output() onConfirm: EventEmitter<string> = new EventEmitter<string>();

  @Input("drawing") set _drawing(drawing: Drawing) {
    this.drawing = drawing;
    this.password = "";
  }
  drawing: Drawing;
  password: string = "";

  isSettingPassword: boolean = false;

  @ViewChild("input") private input: ElementRef<HTMLInputElement>

  constructor(private drawingApiService: DrawingApiService, private authService: AuthService) {}

  ngAfterViewInit(): void {
    if (this.input != undefined)
      this.input.nativeElement.focus();      
  }

  confirm(): void {
    this.isSettingPassword = true;
    if (this.drawing.owner_id == this.authService.getUser().id) {
      this.updatePassword();
    } else {
      this.verifyPassword();
    }
  }

  private verifyPassword(): void {
    this.drawingApiService.verifyPassword({
      drawing_id: this.drawing.id, 
      password: this.password,
    }).subscribe(() => {
      this.isSettingPassword = false;
      this.onConfirm.emit();
    }, () => this.isSettingPassword = false);
  }

  private updatePassword(): void {
    this.drawingApiService.updateDrawing({
      id: this.drawing.id,
      password: this.password,
      album_id: this.drawing.album_id,
      name: this.drawing.name,
    }).subscribe(() => {
      this.isSettingPassword = false;
      this.onConfirm.emit(this.password);
    }, () => this.isSettingPassword = false);
  }

  decline(): void {
      this.onCancel.emit();
  }
}
