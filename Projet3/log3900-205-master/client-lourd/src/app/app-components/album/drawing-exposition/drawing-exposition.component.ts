import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { Drawing } from '@app/classes/api/drawing';
import { User } from '@app/classes/api/user';
import * as moment from 'moment';

@Component({
  selector: 'app-drawing-exposition',
  templateUrl: './drawing-exposition.component.html',
  styleUrls: ['./drawing-exposition.component.scss']
})
export class DrawingExpositionComponent {
  @Output() onView: EventEmitter<void> = new EventEmitter();

  @Input() set drawing(drawing: Drawing) {
    this._drawing = drawing;
    this._drawing.created_at = moment(this._drawing.created_at);
    this.loadImage();
  }

  _drawing: Drawing;
  private ctx: CanvasRenderingContext2D;
  @Input() user: User;

  @ViewChild('drawingCanvas') private drawingCanvas: ElementRef<HTMLCanvasElement>;

  ngAfterViewInit(): void {
    if (this.drawingCanvas !== undefined) {
      this.ctx = this.drawingCanvas.nativeElement.getContext("2d") as CanvasRenderingContext2D;
      if (this._drawing !== undefined) {
        this.loadImage();
      }
    } 
  }

  view(): void {
    this.onView.emit();
  }

  private loadImage(): void {
    if (this._drawing.image == "" || this.drawingCanvas == undefined || this.ctx == undefined) 
      return;

    const img = document.createElement('img');
    img.onload = this.loadImageData.bind(this, img); 
    img.src = this._drawing.image;
  }

  private loadImageData(img: HTMLImageElement): void {
    const canvasSize = { x: this.drawingCanvas.nativeElement.width, y: this.drawingCanvas.nativeElement.height };
    const ratioWidth = canvasSize.x / img.width;
    const ratioHeight = canvasSize.y / img.height;
    const ratio = Math.min(ratioWidth, ratioHeight);
    const adjustedSize = { x: img.width * ratio, y: img.height * ratio };
    const corner = {
        x: (canvasSize.x - adjustedSize.x) / 2,
        y: (canvasSize.y - adjustedSize.y) / 2,
    };
    this.ctx.drawImage(img, corner.x, corner.y, adjustedSize.x, adjustedSize.y);
  }
}
