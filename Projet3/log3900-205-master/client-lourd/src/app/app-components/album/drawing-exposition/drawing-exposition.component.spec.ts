import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrawingExpositionComponent } from './drawing-exposition.component';

describe('DrawingExpositionComponent', () => {
  let component: DrawingExpositionComponent;
  let fixture: ComponentFixture<DrawingExpositionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrawingExpositionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrawingExpositionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
