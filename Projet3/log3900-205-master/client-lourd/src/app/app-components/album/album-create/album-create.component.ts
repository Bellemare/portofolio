import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Output } from '@angular/core';
import { Album } from '@app/classes/api/album';
import { FieldsErrorResponse } from '@app/classes/api/error-response';
import { AlbumApiService } from '@app/services/api/album-api.service';

@Component({
  selector: 'app-album-create',
  templateUrl: './album-create.component.html',
  styleUrls: ['./album-create.component.scss']
})
export class AlbumCreateComponent {
  @Output() onClose: EventEmitter<void> = new EventEmitter();
  @Output() onCreated: EventEmitter<Album> = new EventEmitter();
  name: string = "";
  description: string = "";
  isCreating: boolean = false;

  private errorFields: string[] = [];

  constructor(private albumService: AlbumApiService) {}

  close(): void {
    this.reset();
    this.onClose.emit();
  }

  reset(): void {
    this.name = "";
    this.description = "";
    this.errorFields = [];
  }

  hasError(field: string): boolean {
    return this.errorFields !== undefined && this.errorFields.includes(field);
  }

  createAlbum(): void {
    this.isCreating = true;
    this.albumService.create({
      name: this.name,
      description: this.description
    }).subscribe((album: Album) => {
      this.isCreating = false;
      this.reset();
      this.onCreated.emit(album);
    }, (error: HttpErrorResponse) => {
      this.isCreating = false;
      const err: FieldsErrorResponse = error.error
      if (err !== undefined && err.fields !== undefined) {
        this.errorFields = err.fields;
      }
    });
  }
}
