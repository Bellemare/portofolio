import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Album } from '@app/classes/api/album';
import { FieldsErrorResponse } from '@app/classes/api/error-response';
import { AlbumApiService } from '@app/services/api/album-api.service';

@Component({
  selector: 'app-album-edit',
  templateUrl: './album-edit.component.html',
  styleUrls: ['./album-edit.component.scss']
})
export class AlbumEditComponent {
  @Input() album: Album;
  @Output() onClose: EventEmitter<void> = new EventEmitter();
  @Output() onEdit: EventEmitter<Album> = new EventEmitter();
  isEditing: boolean = false;

  private errorFields: string[] = [];

  constructor(private albumService: AlbumApiService) {}

  close(): void {
    this.reset();
    this.onClose.emit();
  }

  reset(): void {
    this.errorFields = [];
  }

  hasError(field: string): boolean {
    return this.errorFields !== undefined && this.errorFields.includes(field);
  }

  editAlbum(): void {
    this.isEditing = true;
    this.albumService.update(this.album).subscribe(() => {
      this.isEditing = false;
      this.reset();
      this.onEdit.emit(this.album);
    }, (error: HttpErrorResponse) => {
      this.isEditing = false;
      const err: FieldsErrorResponse = error.error
      if (err !== undefined && err.fields !== undefined) {
        this.errorFields = err.fields;
      }
    });
  }
}
