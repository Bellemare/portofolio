import { AfterViewInit, Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { DrawingApiService } from '@app/services/api/drawing-api.service';

@Component({
  selector: 'app-drawing-confirm-password',
  templateUrl: './drawing-confirm-password.component.html',
  styleUrls: ['./drawing-confirm-password.component.scss']
})
export class DrawingConfirmPasswordComponent implements AfterViewInit {
  @Output() onCancel: EventEmitter<void> = new EventEmitter<void>();
  @Output() onConfirm: EventEmitter<string> = new EventEmitter<string>();

  @Input() drawingId: number;
  password: string = "";

  isSettingPassword: boolean = false;

  @ViewChild("input") private input: ElementRef<HTMLInputElement>

  constructor(private drawingApiService: DrawingApiService) {}

  ngAfterViewInit(): void {
    if (this.input != undefined)
      this.input.nativeElement.focus();      
  }

  confirm(): void {
    this.isSettingPassword = true;
    this.verifyPassword();
  }

  private verifyPassword(): void {
    this.drawingApiService.verifyPassword({
      drawing_id: this.drawingId, 
      password: this.password,
    }).subscribe(() => {
      this.isSettingPassword = false;
      this.onConfirm.emit();
    }, () => this.isSettingPassword = false);
  }

  decline(): void {
      this.onCancel.emit();
  }
}
