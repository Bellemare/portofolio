import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrawingConfirmPasswordComponent } from './drawing-confirm-password.component';

describe('DrawingConfirmPasswordComponent', () => {
  let component: DrawingConfirmPasswordComponent;
  let fixture: ComponentFixture<DrawingConfirmPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrawingConfirmPasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrawingConfirmPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
