import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { Drawing } from '@app/classes/api/drawing';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-drawing-view',
  templateUrl: './drawing-view.component.html',
  styleUrls: ['./drawing-view.component.scss']
})
export class DrawingViewComponent {
  readonly closeIcon: IconDefinition = faTimesCircle;

  @Output() onClose: EventEmitter<void> = new EventEmitter();

  @Input("drawing") set _drawing(drawing: Drawing) {
    this.drawing = drawing;
    if (this.show) 
      this.setDrawingImage();
  }
  drawing: Drawing;
  private show: boolean = false;
  @Input() set open(show: boolean) {
    this.show = show;
    if (show)
      this.setDrawingImage();
  }

  @ViewChild("drawing") private canvas: ElementRef<HTMLCanvasElement>;

  private setDrawingImage(): void {
    if (this.canvas == undefined || this.drawing == undefined)
      return;

    const ctx: CanvasRenderingContext2D = this.canvas.nativeElement.getContext("2d") as CanvasRenderingContext2D;
    const img = new Image();
    img.onload = () => {
      this.canvas.nativeElement.width = img.width;
      this.canvas.nativeElement.height = img.height;
      ctx.drawImage(img, 0, 0);
    }
    img.src = this.drawing.image;
  }
}
