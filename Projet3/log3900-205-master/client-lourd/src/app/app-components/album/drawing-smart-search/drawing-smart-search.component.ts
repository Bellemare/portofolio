import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Album } from '@app/classes/api/album';
import { Drawing } from '@app/classes/api/drawing';
import { User } from '@app/classes/api/user';
import { AuthService } from '@app/services/api/auth.service';
import { DrawingApiService } from '@app/services/api/drawing-api.service';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-drawing-smart-search',
  templateUrl: './drawing-smart-search.component.html',
  styleUrls: ['./drawing-smart-search.component.scss']
})
export class DrawingSmartSearchComponent {
  readonly searchIcon: IconDefinition = faSearch;

  @ViewChild("drawingList") private drawingsView: ElementRef<HTMLElement>;

  @Output() onCancel: EventEmitter<void> = new EventEmitter();
  @Input("userAlbums") set _userAlbums(albums: Album[]) {
    this.userAlbums = albums;
  }
  userAlbums: Album[] = [];

  isSearching: boolean = false;

  value: string = "";
  searchAllAlbums: boolean = true;
  albumId: number = 0;

  drawings: Drawing[] = [];

  album: Album;
  user: User;

  showDrawingEdit: boolean = false;
  deletingDrawing: boolean = false;
  showDrawingView: boolean = false;
  confirming: boolean = false;
  confirmMessage: string = "";
  isSettingPassword: boolean = false;

  selectedDrawing: Drawing;

  constructor(
    private drawingApiService: DrawingApiService,
    private authService: AuthService,
    private router: Router,
  ) { }

  setSearchAlbum(searchAll: boolean): void {
    this.searchAllAlbums = searchAll;
    if (this.userAlbums.length > 0)
      this.albumId = this.userAlbums[0].id as number;
  }

  search(): void {
    this.isSearching = true;
    this.drawingApiService.smartFilter(this.value, this.searchAllAlbums ? -1:this.albumId).subscribe((drawings: Drawing[]) => {
      this.isSearching = false;
      this.drawings = drawings;
    }, () => this.isSearching = false);    
  }

  scroll(event: WheelEvent): void {
    if (this.drawingsView == undefined)
      return;
      
    this.drawingsView.nativeElement.scrollLeft += event.deltaY;
    event.preventDefault();
  }

  cancel(): void {
    this.onCancel.emit();
  }

  hideAll(): void {
    this.confirming = false;
    this.deletingDrawing = false;
    this.showDrawingEdit = false;
    this.showDrawingView = false;
    this.isSettingPassword = false;
  }

  setPassword(drawing: Drawing): void {
    this.isSettingPassword = true;
    this.selectedDrawing = drawing;
  }

  passwordSet(password: string): void {
    if (this.selectedDrawing.owner_id == this.authService.getUser().id) {
      this.selectedDrawing.is_password_protected = password != "";
      this.hideAll();
    } else {
      this.router.navigateByUrl(`/editor/${this.selectedDrawing.id}`);
    }
  }

  editDrawing(drawing: Drawing): void {
    this.showDrawingEdit = true;
    this.selectedDrawing = JSON.parse(JSON.stringify(drawing));
  }

  onDrawingEdit(drawing: Drawing): void {
    const index = this.drawings.findIndex((d: Drawing) => d.id == drawing.id);
    if (this.drawings[index].album_id != drawing.album_id) {
      this.drawings[index].album_id = drawing.album_id;
      this.drawings[index].was_transfered = true;
    } else {
      this.drawings[index].name = drawing.name;
    }
    this.hideAll();
  }

  confirmed(): void {
    if (this.deletingDrawing) {
      this.onDeleteDrawing();
    }
  }

  deleteDrawing(drawing: Drawing): void {
    if (drawing.was_transfered) {
      this.removeDrawingFromList(drawing);
      return;
    }
    this.selectedDrawing = drawing;
    this.deletingDrawing = true;
    this.confirmMessage = `Voulez-vous vraiment supprimer le dessin ${drawing.name}?`;
  }

  onDeleteDrawing(): void {
    this.drawingApiService.deleteDrawing(this.selectedDrawing.id).subscribe(() => {
      this.removeDrawingFromList(this.selectedDrawing);
    });
    this.deletingDrawing = false;
  }

  onDrawingView(drawing: Drawing): void {
    this.hideAll();
    this.selectedDrawing = drawing;
    this.showDrawingView = true;
  }

  private removeDrawingFromList(drawing: Drawing): void {
    const index = this.drawings.findIndex((d: Drawing) => d.id == drawing.id);
    if (index != -1) {
      this.drawings.splice(index, 1);
    }
  }
}
