import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrawingSmartSearchComponent } from './drawing-smart-search.component';

describe('DrawingSmartSearchComponent', () => {
  let component: DrawingSmartSearchComponent;
  let fixture: ComponentFixture<DrawingSmartSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrawingSmartSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrawingSmartSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
