import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Album } from '@app/classes/api/album';
import { User } from '@app/classes/api/user';
import { AuthService } from '@app/services/api/auth.service';

@Component({
  selector: 'app-album-detail',
  templateUrl: './album-detail.component.html',
  styleUrls: ['./album-detail.component.scss']
})
export class AlbumDetailComponent implements OnInit {
  @Input() album: Album;
  @Output() onClose: EventEmitter<void> = new EventEmitter();
  @Output() onRemove: EventEmitter<Album> = new EventEmitter();
  @Output() onEdit: EventEmitter<Album> = new EventEmitter();

  disableActions: boolean = false;
  isDeleting: boolean = false;
  confirming: boolean = false;

  confirmMessage: string = "";

  private currentUser: User;

  constructor(private authService: AuthService) {}

  ngOnInit(): void {
    if (this.currentUser == undefined)
      this.currentUser = this.authService.getUser();  

    this.authService.listenUserChange().subscribe(() => {
      this.currentUser = this.authService.getUser();
    });
  }

  close(): void {
    this.onClose.emit();
  }

  deleteAlbum(): void {
    this.confirmMessage = "Voulez-vous vraiment supprimer cet album? Tous les dessins seront supprimés.";
    this.isDeleting = true;
    this.confirming = true;
  }

  cancelDelete(): void {
    this.isDeleting = false;
    this.confirming = false;
  }

  onConfirm(): void {
    this.onRemove.emit(this.album);
  }

  editAlbum(): void {
    this.onEdit.emit(this.album);
  }

  quitAlbum(): void {
    this.confirmMessage = "Voulez-vous vraiment quitter cet album? Tous vos dessins seront transférés au propriétaire de l'album.";
    this.confirming = true;
  }

  get isOwner(): boolean {
    return this.album !== undefined && this.currentUser !== undefined && this.album.owner_id == this.currentUser.id;
  }

  get canQuit(): boolean {
    return this.album !== undefined && this.album.id !== undefined && this.album.id > 0 && !this.isOwner;
  }
}
