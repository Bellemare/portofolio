import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlbumJoinComponent } from './album-join.component';

describe('AlbumJoinComponent', () => {
  let component: AlbumJoinComponent;
  let fixture: ComponentFixture<AlbumJoinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlbumJoinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbumJoinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
