import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Album } from '@app/classes/api/album';
import { AlbumApiService } from '@app/services/api/album-api.service';

@Component({
  selector: 'app-album-join',
  templateUrl: './album-join.component.html',
  styleUrls: ['./album-join.component.scss']
})
export class AlbumJoinComponent {
  @Input() album: Album;
  @Output() onClose: EventEmitter<void> = new EventEmitter();
  @Output() onRequest: EventEmitter<Album> = new EventEmitter();

  disableActions: boolean = false;

  constructor(private albumService: AlbumApiService) {}

  close(): void {
    this.disableActions = false;
    this.onClose.emit();
  }

  afterRequest(): void {
    this.disableActions = false;
    this.onRequest.emit(this.album);
  }

  requestToJoin(): void {
    if (this.album != undefined && this.album.id !== undefined) {
      this.disableActions = true;
      this.albumService.requestToJoinAlbum(this.album.id).subscribe(() => {
        this.afterRequest();
      }, () =>  {
        this.afterRequest();
      });
    }    
  }
}
