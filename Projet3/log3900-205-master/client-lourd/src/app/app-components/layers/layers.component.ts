import { Component, OnInit } from '@angular/core';
import { DrawingEventWrapper } from '@app/classes/drawing/drawing-event-wrapper';
import { CouchesService } from '@app/services/couches/couches.service';
import { SynchronizerService } from '@app/services/synchronization/synchronizer.service';
import { SelectionService } from '@app/services/tools/selection/selection.service';

@Component({
  selector: 'app-layers',
  templateUrl: './layers.component.html',
  styleUrls: ['./layers.component.scss']
})
export class LayersComponent implements OnInit {
  readonly NUM_ELEMENTS_VISIBLE = 8;

  currentSelection: DrawingEventWrapper;
  currentIndex: number = 0;

  constructor(private couchesService: CouchesService, private selectionService: SelectionService, private synchronizerService: SynchronizerService) { }

  ngOnInit(): void {
      this.selectionService.onSelectEvent = (selection: DrawingEventWrapper, layer: number) => {
        this.currentSelection = selection;
        if (this.currentSelection != undefined)
          this.scrollToLayer(layer);
      }
  }

  private scrollToLayer(layer: number): void {
    this.currentIndex = Math.floor(layer / this.NUM_ELEMENTS_VISIBLE);
    setTimeout(() => {
      const element = document.getElementById(`layer-${layer}`);
      if (element) {
        element.scrollIntoView({
          behavior: 'auto',
          block: 'center',
          inline: 'center'
        });
      }
    }, 10);
  }

  deleteSelection(): void {
    this.selectionService.deleteSelection();
  }

  changeLayer(newIndex: number, oldIndex: number): void {
    if (newIndex < 0 || newIndex > this.maxLayer) {
      return;
    }

    this.synchronizerService.moveLayerEvent(newIndex, oldIndex);
    this.scrollToLayer(newIndex);
  }

  setIndex(index: number): void {
    this.currentIndex = Math.max(Math.min(index, this.lastIndex), 0);
  }

  get layers(): DrawingEventWrapper[] {
    const start = this.currentIndex * this.NUM_ELEMENTS_VISIBLE;
    return this.couchesService.layers.slice(start, start + this.NUM_ELEMENTS_VISIBLE);
  }

  get maxLayer(): number {
    return this.couchesService.layers.length - 1;
  }

  get lastIndex(): number {
    return Math.max(Math.ceil(this.couchesService.layers.length / this.NUM_ELEMENTS_VISIBLE) - 1, 0);
  }
}
