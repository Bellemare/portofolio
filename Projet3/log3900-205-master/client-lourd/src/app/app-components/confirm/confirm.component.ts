import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent {
  @Output() onCancel: EventEmitter<void> = new EventEmitter<void>();
  @Output() onConfirm: EventEmitter<void> = new EventEmitter<void>();

  @Input() message: string = "Voulez-vous vraiment effectuer cette action?";

  confirm(): void {
      this.onConfirm.emit();
  }

  decline(): void {
      this.onCancel.emit();
  }
}
