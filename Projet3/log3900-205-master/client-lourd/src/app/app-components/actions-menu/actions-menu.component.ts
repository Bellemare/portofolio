import { Component, OnInit } from "@angular/core";
import { AuthService } from "@app/services/api/auth.service";
import { User } from '@app/classes/api/user';
import { NavigationStart, Router } from "@angular/router";
import { IconDefinition } from "@fortawesome/fontawesome-common-types";
import { faBell, faComments, faSignOutAlt, faUsers } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-actions-menu',
  templateUrl: './actions-menu.component.html',
  styleUrls: ['./actions-menu.component.scss']
})
export class ActionsMenuComponent implements OnInit {
  readonly disconnectIcon: IconDefinition = faSignOutAlt;
  readonly chatIcon: IconDefinition = faComments;
  readonly notificationsIcon: IconDefinition = faBell;
  readonly contactsIcon: IconDefinition = faUsers;

  user: User;

  showChatBox: boolean = false;
  showNotifications: boolean = false;
  showContacts: boolean = false;

  hasUnseenMessages: boolean = false;
  hasNewNotifications: boolean = false;

  isPoppedOut: boolean = false;

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit(): void {
    this.getUser();
    this.router.events.subscribe((evt) => {
      if (evt instanceof NavigationStart) {
        this.hideAll();
      }
    });
  }

  private getUser(): void {
    this.authService.listenUserChange().subscribe(() => {
      if (this.authService.isAuthenticated()) {
        this.user = this.authService.getUser();
      }
    });
  }

  navigateToUser(): void {
    this.router.navigateByUrl(`/user`);
  }

  isChatPoppedOut(poppedOut: boolean): void {
    this.isPoppedOut = poppedOut;
    this.showChatBox = !this.isPoppedOut;
  }

  disconnectUser(): void {
    this.authService.logout().subscribe(() => {
      this.router.navigateByUrl("/login");
    });
  }

  hideAll(): void {
    this.showChatBox = false;
    this.showContacts = false;
    this.showNotifications = false;
  }

  toggleChatBox(): void {
    const show = this.showChatBox;
    this.hideAll();
    this.showChatBox = !show;
  }

  toggleNotifications(): void {
    const show = this.showNotifications;
    this.hideAll();
    this.hasNewNotifications = false;
    this.showNotifications = !show;
  }

  toggleContactsList(): void {
    const show = this.showContacts;
    this.hideAll();
    this.showContacts = !show;
  }

  newNotifications(): void {
    this.hasNewNotifications = true;
  }

  get showHeader(): boolean {
    return this.authService.isAuthenticated();
  }
}
