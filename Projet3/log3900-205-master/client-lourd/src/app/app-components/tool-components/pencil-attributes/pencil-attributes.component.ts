import { Component } from '@angular/core';
import { ToolAttributesComponent } from '@app/app-components/tool-components/tool-attributes/tool-attributes.component';
import { PencilProperties } from '@app/classes/tool-properties/pencil-properties';
import { PencilService } from '@app/services/tools/pencil-service';

@Component({
    selector: 'app-pencil-attributes',
    templateUrl: './pencil-attributes.component.html',
    styleUrls: ['../tool-attributes/tool-attributes.component.scss', './pencil-attributes.component.scss'],
})
export class PencilAttributesComponent extends ToolAttributesComponent {
    readonly LINE_WIDTH_MIN: number = 1;
    readonly LINE_WIDTH_MAX: number = 100;
    protected currentAttributes: PencilProperties;

    constructor(protected toolService: PencilService) {
        super(toolService);
    }
}
