import { Component } from '@angular/core';
import { ToolAttributesComponent } from '@app/app-components/tool-components/tool-attributes/tool-attributes.component';
import { EllipseProperties } from '@app/classes/tool-properties/ellipse-properties';
import { EllipseService } from '@app/services/tools/ellipse.service';

@Component({
    selector: 'app-ellipse-attributes',
    templateUrl: './ellipse-attributes.component.html',
    styleUrls: ['../tool-attributes/tool-attributes.component.scss', './ellipse-attributes.component.scss'],
})
export class EllipseAttributesComponent extends ToolAttributesComponent {
    readonly LINE_WIDTH_MIN: number = 1;
    readonly LINE_WIDTH_MAX: number = 100;
    protected currentAttributes: EllipseProperties;

    constructor(protected toolService: EllipseService) {
        super(toolService);
    }
}
