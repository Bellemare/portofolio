import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponentsModule } from '@app/app-components/app-components.module';
import { MaterialModule } from '@app/material/material.module';
import { EllipseAttributesComponent } from './ellipse-attributes.component';

describe('EllipseAttributesComponent', () => {
    let component: EllipseAttributesComponent;
    let fixture: ComponentFixture<EllipseAttributesComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [EllipseAttributesComponent],
            imports: [MaterialModule, AppComponentsModule, BrowserAnimationsModule],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EllipseAttributesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
