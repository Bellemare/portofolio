import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponentsModule } from '@app/app-components/app-components.module';
import { MaterialModule } from '@app/material/material.module';
import { RectAttributesComponent } from './rect-attributes.component';

describe('RectAttributesComponent', () => {
    let component: RectAttributesComponent;
    let fixture: ComponentFixture<RectAttributesComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [RectAttributesComponent],
            imports: [MaterialModule, AppComponentsModule, BrowserAnimationsModule],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RectAttributesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
