import { Component } from '@angular/core';
import { ToolAttributesComponent } from '@app/app-components/tool-components/tool-attributes/tool-attributes.component';
import { RectangleProperties } from '@app/classes/tool-properties/rectangle-properties';
import { RectangleService } from '@app/services/tools/rectangle.service';

@Component({
    selector: 'app-rect-attributes',
    templateUrl: './rect-attributes.component.html',
    styleUrls: ['../tool-attributes/tool-attributes.component.scss', './rect-attributes.component.scss'],
})
export class RectAttributesComponent extends ToolAttributesComponent {
    readonly LINE_WIDTH_MIN: number = 1;
    readonly LINE_WIDTH_MAX: number = 100;
    protected currentAttributes: RectangleProperties;

    constructor(protected toolService: RectangleService) {
        super(toolService);
    }
}
