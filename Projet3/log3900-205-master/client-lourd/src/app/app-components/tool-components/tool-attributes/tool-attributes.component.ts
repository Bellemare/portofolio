import { Component } from '@angular/core';
import { DrawingType } from '@app/classes/drawing/drawing-type';
import { ToolProperties } from '@app/classes/tool-properties/tool-properties';
import { ToolProperty } from '@app/classes/tool-properties/tool-property';
import { ToolService } from '@app/services/tools/tool.service';

@Component({
    selector: 'app-tool-attributes',
    templateUrl: './tool-attributes.component.html',
    styleUrls: ['./tool-attributes.component.scss'],
})
export class ToolAttributesComponent {
    readonly LINE_WIDTH_MIN: number = 1;
    readonly LINE_WIDTH_MAX: number = 100;
    readonly DEFAULT_DRAWING_TYPE: number = DrawingType.Stroke;
    readonly DRAWING_TYPE_LABELS: string[] = ['Contour', 'Plein', 'Plein avec contour'];
    readonly LINE_WIDTH_KEY: string = 'line_width';
    readonly DRAWING_TYPE_KEY: string = 'drawing_type';
    protected currentAttributes: ToolProperties;

    constructor(protected toolService: ToolService) {}

    init(): void {
        this.currentAttributes = this.toolService.toolAttributes;
    }

    attributeChanged(attribute: string, value: ToolProperty): void {
        this.toolService.setProperty(attribute, value);
    }

    get drawingTypes(): number[] {
        const drawingTypes = DrawingType;
        const keys = Object.values(drawingTypes);
        return keys.slice(keys.length / 2) as number[];
    }

    get drawingType(): number {
        return this.getAttribute(this.DRAWING_TYPE_KEY, this.DEFAULT_DRAWING_TYPE) as number;
    }

    getAttribute(key: string, defaultValue: ToolProperty): ToolProperty {
        const attributIsSet = this.currentAttributes != undefined && this.currentAttributes[key] != null;
        return attributIsSet ? this.currentAttributes[key] : defaultValue;
    }
}
