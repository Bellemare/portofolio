import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DrawingType } from '@app/classes/drawing/drawing-type';
import { ToolProperties } from '@app/classes/tool-properties/tool-properties';
import { AttributesManagerService } from '@app/services/api/attributes-manager.service';
import { KeyBindingResolverService } from '@app/services/key-binding/key-binding-resolver.service';
import { ColorService } from '@app/services/tools/color.service';
import { ToolService } from '@app/services/tools/tool.service';
import { ToolAttributesComponent } from './tool-attributes.component';

class ToolStub extends ToolService {}

// tslint:disable:no-string-literal
describe('ToolAttributesComponent', () => {
    let component: ToolAttributesComponent;
    let fixture: ComponentFixture<ToolAttributesComponent>;

    let toolService: ToolStub;
    let toolAttributesSpy: jasmine.Spy<jasmine.Func>;
    const attributes: ToolProperties = { drawingType: DrawingType.Stroke, lineWidth: 5 };

    beforeEach(async(() => {
        const attributesManagerService = new AttributesManagerService();
        const colorService = new ColorService(attributesManagerService);
        const keybindService = new KeyBindingResolverService();
        toolService = new ToolStub(attributesManagerService, colorService, keybindService);
        toolAttributesSpy = spyOnProperty(toolService, 'toolAttributes', 'get');
        toolAttributesSpy.and.returnValue(attributes);

        TestBed.configureTestingModule({
            declarations: [ToolAttributesComponent],
            providers: [{ provide: ToolService, useValue: toolService }],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ToolAttributesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should get current tool attributes', () => {
        component.init();
        expect(toolAttributesSpy).toHaveBeenCalled();
        expect(component['currentAttributes']).toEqual(attributes);
    });

    it('should set attribute on tool', () => {
        const setToolAttributeSpy = spyOn(toolService, 'setProperty');
        const key = 'lineWidth';
        const value = 5;
        component.attributeChanged(key, value);
        expect(setToolAttributeSpy).toHaveBeenCalledWith(key, value);
    });

    it('should return drawing types', () => {
        expect(component.drawingTypes).toEqual([0, 1, 2]);
    });

    it('should return drawing type', () => {
        component.init();
        expect(component.drawingType).toEqual(attributes.drawingType);
    });

    it('should return default drawing type when none is set', () => {
        expect(component.drawingType).toEqual(component.DEFAULT_DRAWING_TYPE);
    });

    it('should return attribute', () => {
        component.init();
        expect(component.getAttribute('lineWidth', 0)).toEqual(attributes.lineWidth as number);
    });

    it('should return default attribute when it is not set', () => {
        const defaultVal = 15;
        expect(component.getAttribute('lineWidth', defaultVal)).toEqual(defaultVal);
    });
});
