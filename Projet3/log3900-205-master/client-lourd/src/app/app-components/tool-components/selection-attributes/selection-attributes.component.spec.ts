import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponentsModule } from '@app/app-components/app-components.module';
import { MaterialModule } from '@app/material/material.module';
import { SelectionService } from '@app/services/tools/selection/selection.service';
import { SelectionAttributesComponent } from './selection-attributes.component';

// tslint:disable:no-string-literal
// tslint:disable:no-empty
class SelectionStub extends SelectionService {
    protected initDashedPrevisualization(): void {}
}

describe('SelectionAttributesComponent', () => {
    let component: SelectionAttributesComponent;
    let fixture: ComponentFixture<SelectionAttributesComponent>;
    let selectionService: jasmine.SpyObj<SelectionStub>;

    beforeEach(async(() => {
        selectionService = jasmine.createSpyObj('SelectionStub', ['selectAll']);

        TestBed.configureTestingModule({
            declarations: [SelectionAttributesComponent],
            imports: [MaterialModule, BrowserAnimationsModule, AppComponentsModule],
            providers: [{ provide: SelectionService, useValue: selectionService }],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SelectionAttributesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should call select all on selection service', () => {
        component.callSelectAll();
        expect(selectionService.selectAll).toHaveBeenCalled();
    });

    it('should call copy', () => {
        const spy = spyOn(component['selectionClipboard'], 'copy');
        component.callCopy();
        expect(spy).toHaveBeenCalled();
    });

    it('should call paste', () => {
        const spy = spyOn(component['selectionClipboard'], 'paste');
        component.callPaste();
        expect(spy).toHaveBeenCalled();
    });

    it('should call cut', () => {
        const spy = spyOn(component['selectionClipboard'], 'cut');
        component.callCut();
        expect(spy).toHaveBeenCalled();
    });

    it('should call delete', () => {
        const spy = spyOn(component['selectionClipboard'], 'delete');
        component.callDelete();
        expect(spy).toHaveBeenCalled();
    });
});
