import { Component } from '@angular/core';
import { ToolAttributesComponent } from '@app/app-components/tool-components/tool-attributes/tool-attributes.component';
import { ToolProperties } from '@app/classes/tool-properties/tool-properties';
import { ToolProperty } from '@app/classes/tool-properties/tool-property';
import { SelectionService } from '@app/services/tools/selection/selection.service';

@Component({
    selector: 'app-selection-attributes',
    templateUrl: './selection-attributes.component.html',
    styleUrls: ['./selection-attributes.component.scss'],
})
export class SelectionAttributesComponent extends ToolAttributesComponent {
    readonly LINE_WIDTH_MIN: number = 1;
    readonly LINE_WIDTH_MAX: number = 100;
    protected currentAttributes: ToolProperties;

    constructor(protected toolService: SelectionService) {
        super(toolService);
    }

    getAttribute(key: string, defaultValue: ToolProperty): ToolProperty {
        const attributes = this.toolService.toolAttributes;
        const attributIsSet = attributes != undefined && attributes[key] != null;
        return attributIsSet ? attributes[key] : defaultValue;
    }
}
