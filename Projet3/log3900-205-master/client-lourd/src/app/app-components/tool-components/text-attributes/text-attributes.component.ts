import { Component } from '@angular/core';
import { ThemePalette } from '@angular/material/core';
import { ToolAttributesComponent } from '@app/app-components/tool-components/tool-attributes/tool-attributes.component';
import { TextFonts } from '@app/classes/text-fonts.enum';
import { TextProperties } from '@app/classes/tool-properties/text-properties';
import { TextService } from '@app/services/tools/text.service';

@Component({
    selector: 'app-text-attributes',
    templateUrl: './text-attributes.component.html',
    styleUrls: ['./text-attributes.component.scss'],
})
export class TextAttributesComponent extends ToolAttributesComponent {
    readonly FONT_KEY: string = 'font';
    readonly FONT_DEFAULT: string = TextFonts.ComicSansMS;

    readonly FONT_SIZE_KEY: string = 'fontSize';
    readonly FONT_SIZE_MIN: number = 1;
    readonly FONT_SIZE_MAX: number = 100;
    readonly FONT_SIZE_DEFAULT: number = 12;

    readonly BOLD_KEY: string = 'isBold';
    readonly BOLD_LABEL: string = 'Gras';
    readonly BOLD_DEFAULT: boolean = false;

    readonly ITALIC_KEY: string = 'isItalic';
    readonly ITALIC_LABEL: string = 'Italique';
    readonly ITALIC_DEFAULT: boolean = false;

    readonly ALIGNMENT_KEY: string = 'alignment';
    readonly ALIGNMENT_DEFAULT: CanvasTextAlign = 'left';
    readonly ALIGNMENT_COLOR: ThemePalette = 'primary';

    protected currentAttributes: TextProperties;

    constructor(protected toolService: TextService) {
        super(toolService);
    }

    get fonts(): string[] {
        const fontsEnum = TextFonts;
        const fontsStrings = Object.values(fontsEnum);
        return fontsStrings as string[];
    }

    setAlignment(value: CanvasTextAlign): void {
        if (this.isAlignmentValid(value)) {
            this.attributeChanged(this.ALIGNMENT_KEY, value);
        }
    }

    private isAlignmentValid(value: CanvasTextAlign): boolean {
        return value === 'left' || value === 'center' || value === 'right';
    }
}
