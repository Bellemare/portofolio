import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponentsModule } from '@app/app-components/app-components.module';
import { MaterialModule } from '@app/material/material.module';
import { TextAttributesComponent } from './text-attributes.component';

describe('TextAttributesComponent', () => {
    let component: TextAttributesComponent;
    let fixture: ComponentFixture<TextAttributesComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [TextAttributesComponent],
            imports: [MaterialModule, AppComponentsModule, BrowserAnimationsModule],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TextAttributesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('fonts should return an array of the fonts', () => {
        expect(component.fonts).toEqual(['Comic Sans MS', 'Arial', 'Times New Roman', 'Impact', 'Courier']);
    });

    it('setAlignment should call attributeChanged if value is valid', () => {
        const attributeChangedSpy = spyOn(component, 'attributeChanged');
        component.setAlignment('left');
        expect(attributeChangedSpy).toHaveBeenCalledWith(component.ALIGNMENT_KEY, 'left');
    });

    it('setAlignment should not call attributeChanged if value is invalid', () => {
        const attributeChangedSpy = spyOn(component, 'attributeChanged');
        component.setAlignment('start');
        expect(attributeChangedSpy).not.toHaveBeenCalled();
    });
});
