import { Component, EventEmitter, Output } from '@angular/core';
import { RoomService } from '@app/services/api/room.service';
import { Room } from '@app/classes/api/room';
import { HttpErrorResponse } from '@angular/common/http';
import { FieldsErrorResponse } from '@app/classes/api/error-response';


@Component({
  selector: 'app-chat-menu',
  templateUrl: './chat-menu.component.html',
  styleUrls: ['./chat-menu.component.scss']
})
export class ChatMenuComponent  {
  @Output() onClose: EventEmitter<void> = new EventEmitter();
  @Output() onAdd: EventEmitter<Room> = new EventEmitter();

  showAvailableRooms: boolean = false;
  showCreateForm: boolean = false;
  availableRooms : Room[] = [];
  private allRooms : Room[] = [];
  private errorFields: string[] = [];

  newRoomName: string = "";
  nameFilter: string = "";

  constructor(private roomService: RoomService) { }

  ngOnInit() {
    this.roomService.fetchAllRooms().subscribe((list: Room[]) => {
      this.allRooms = list;
      this.availableRooms = this.allRooms;
    });
  }

  toggleCreate(): void{
    this.showAvailableRooms = false;
    this.showCreateForm = true;
  }

  toggleJoin(): void{
    this.showCreateForm = false;
    this.showAvailableRooms = true;
  }

  hasError(field: string): boolean {
    return this.errorFields !== undefined && this.errorFields.includes(field);
  }

  addRoom(roomInfo: Room): void {
    this.onAdd.emit(roomInfo);
  }

  close(): void {
    this.onClose.emit();
  }

  joinRoom(room: Room): void {
    this.roomService.joinRoom(room.id).subscribe(() => {
      this.addRoom(room);
    }, () => {
      const index = this.allRooms.findIndex((r: Room) => r.id == room.id);
      if (index != -1) {
        this.allRooms.splice(index, 1);
      }
    });
  }

  searchByName(): void{
    this.availableRooms = [];
    if(this.nameFilter == ""){
      this.availableRooms = this.allRooms;
      return;
    }

    this.roomService.fetchAllRoomsByName(this.nameFilter).subscribe((list: Room[]) => {
      this.availableRooms = list;
    });
  }

  createRoom(): void {
    this.roomService.createRoom(this.newRoomName).subscribe((roomInfo: Room) => {
      this.addRoom(roomInfo);
    }, (error: HttpErrorResponse) => {
      const err: FieldsErrorResponse = error.error
      if (err !== undefined && err.fields !== undefined) {
        this.errorFields = err.fields;
      }
    });
  }
}
