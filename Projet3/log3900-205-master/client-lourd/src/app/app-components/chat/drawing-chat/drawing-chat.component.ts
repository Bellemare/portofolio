import { Component, Input } from '@angular/core';
import { SocketClient } from '@app/classes/socket/socket-client';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';
import { faPaperPlane } from '@fortawesome/free-solid-svg-icons';
import { ChatComponent } from '../chat/chat.component';

@Component({
  selector: 'app-drawing-chat',
  templateUrl: '../chat/chat.component.html',
  styleUrls: ['../chat/chat.component.scss']
})
export class DrawingChatComponent extends ChatComponent {
  readonly SEND_BUTTON: string = "send";
  readonly SEND_ICON: IconDefinition = faPaperPlane;

  @Input("conn") set _conn(conn: SocketClient) {
    this.initHandler(conn);
  }

  message: string = "";

  private initHandler(conn: SocketClient): void {
    if (!this.handler.hasInitialized()) {
      this.handler.init(conn);
      this.handler.listenClientState(conn);
    }
  }

  protected loadMembers(): void {}

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this.handler.destroy();
  }

  onScroll(): void {
    this.autoScroll = this.chatBox.nativeElement.clientHeight == this.chatBox.nativeElement.scrollHeight - this.chatBox.nativeElement.scrollTop;
  }
}
