import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrawingChatComponent } from './drawing-chat.component';

describe('DrawingChatComponent', () => {
  let component: DrawingChatComponent;
  let fixture: ComponentFixture<DrawingChatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrawingChatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrawingChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
