import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { RoomService } from '@app/services/api/room.service';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';
import { faAngleDoubleDown, faAngleDoubleUp, faWindowRestore } from '@fortawesome/free-solid-svg-icons';
import { Room } from '@app/classes/api/room';
import { SocketClient } from '@app/classes/socket/socket-client';
import { ChatListComponent } from '../chat-list/chat-list.component';
import { ElectronService } from '@app/services/api/electron.service';

@Component({
  selector: 'app-chat-room',
  templateUrl: './chat-room.component.html',
  styleUrls: ['./chat-room.component.scss'],
})
export class ChatRoomComponent implements OnInit {
  readonly moreIcon: IconDefinition = faAngleDoubleDown;
  readonly lessIcon: IconDefinition = faAngleDoubleUp;
  readonly popOutIcon: IconDefinition = faWindowRestore;
  
  showChatList: boolean = false;

  currentRoom: Room;
  rooms: Room[] = [];

  drawingRoomClient: SocketClient | undefined;

  @Input() set showChatBox(show: boolean) {
    this.show = show;
    this.showChatList = false;
  }

  @Output() onNotification: EventEmitter<boolean> = new EventEmitter();
  @Output() onPopoutChange: EventEmitter<boolean> = new EventEmitter();

  show: boolean = false;

  @ViewChild(ChatListComponent) private chatList: ChatListComponent;

  private hidingChatList: any;

  constructor(public roomService: RoomService, protected electronService: ElectronService) { }

  ngOnInit(): void {
      this.loadRoomList();
      this.listenToDrawingRoomSession();
      this.listenToQuitDrawingRoomSession();
  }

  newNotification(): void {
    this.onNotification.emit(this.rooms.findIndex((r: Room) => r.has_notification) > -1);
  }

  selectRoom(room: Room): void {
    this.currentRoom = room;
    this.showChatList = false;
  }

  popoutChat():  void {
    this.onPopoutChange.emit(true);
    this.electronService.popoutChat(this.drawingRoomClient?.getId()).then(() => {
      this.onPopoutChange.emit(false);
    }).catch((e) => console.log(e));
  }

  protected listenToDrawingRoomSession(): void {
    this.roomService.listenDrawingRoomAdd().subscribe((conn: SocketClient) => {
      this.addNewDrawingRoomSession(conn);
      this.electronService.sendNewDrawingRoom(conn.getId());
    });
  }

  protected addNewDrawingRoomSession(conn: SocketClient): void {
    this.drawingRoomClient = conn;
    this.rooms.unshift({
      id: -1,
      name: "Session collaborative",
      owner_id: 0,
    });
    this.currentRoom = this.rooms[0];
  }

  protected listenToQuitDrawingRoomSession(): void {
    this.roomService.listenDrawingRoomRemove().subscribe(() => {
      this.quitDrawingRoomSession();
      this.electronService.sendQuitDrawingRoom();
    });
  }

  protected quitDrawingRoomSession(): void {
    const index = this.rooms.findIndex((r: Room) => r.id == -1);
    if (index != -1) {
      this.rooms.splice(index, 1);
    }
    this.drawingRoomClient = undefined;
    this.currentRoom = this.rooms[0];
  }

  setShowChatList(): void {
    this.showChatList = true;
    this.cancelHideChatList();
  }

  cancelHideChatList(): void {
    clearTimeout(this.hidingChatList);
    this.hidingChatList = undefined;
  }

  startHideChatList(): void {
    if (this.chatList != undefined && this.chatList.showChatMenu)
      return;

    if (this.hidingChatList != undefined)
      this.cancelHideChatList();

    this.hidingChatList = setTimeout(() => {
      this.showChatList = false;
    }, 500);
  }

  protected loadRoomList(): void {
    this.roomService.fetchJoinedRooms().subscribe((rooms: Room[]) => {
      this.rooms.push(...rooms);
      this.currentRoom = this.rooms[0];
    });
  }
}
