import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { RoomService } from '@app/services/api/room.service';
import { Room } from '@app/classes/api/room';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';
import { faPlus, faTimesCircle, faTrash } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from '@app/services/api/auth.service';
import { User } from '@app/classes/api/user';

@Component({
  selector: 'app-chat-list',
  templateUrl: './chat-list.component.html',
  styleUrls: ['./chat-list.component.scss']
})
export class ChatListComponent implements OnInit {
  readonly addIcon: IconDefinition = faPlus;
  readonly removeIcon: IconDefinition = faTimesCircle;
  readonly deleteIcon: IconDefinition = faTrash;

  @Output() onRoomSelect: EventEmitter<Room> = new EventEmitter();
  @Input() selectedRoomId: number;

  @Input() rooms: Room[] = [];

  showChatMenu: boolean = false;
  confirming: boolean = false;
  confirmText: string = "";
  selectedRoom: Room;

  private user: User;

  constructor(public roomService: RoomService, private authService: AuthService) { }

  ngOnInit(): void {
    this.user = this.authService.getUser();

    this.authService.listenUserChange().subscribe((user: User) => this.user = user);
  }

  private removeRoomFromList(room: Room): void {
    const index = this.rooms.findIndex((r: Room) => r.id == room.id);
    if (index != -1) {
      this.rooms.splice(index, 1);
    }

    if (this.selectedRoomId == room.id) {
      this.selectRoom(this.rooms[0]);
    }
  }

  selectRoom(room: Room): void {
    this.onRoomSelect.emit(room);
  }

  leaveRoom(room: Room): void {
    this.roomService.leaveRoom(room.id).subscribe(() => {
      this.removeRoomFromList(room);
    }, () => this.removeRoomFromList(room));
  }

  deleteRoom(room: Room): void {
    this.selectedRoom = room;
    this.confirmText = `Voulez-vous vraiment supprimer le canal ${room.name}?`;
    this.confirming = true;
  }

  onDelete(): void {
    this.confirming = false;
    this.roomService.deleteRoom(this.selectedRoom.id).subscribe(() => {
      this.removeRoomFromList(this.selectedRoom);
    }, () => this.removeRoomFromList(this.selectedRoom));
  }

  isOwner(room: Room): boolean {
    return this.user != undefined && this.user.id == room.owner_id;
  }

  addRoom(room: Room): void {
    this.showChatMenu = false;
    this.rooms.push(room);
    this.onRoomSelect.emit(room);
  }

  openRoomMenu():void{
    this.showChatMenu = true;
  }
}
