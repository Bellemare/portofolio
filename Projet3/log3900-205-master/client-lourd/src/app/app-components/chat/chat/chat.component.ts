import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { Room } from "@app/classes/api/room";
import { MessagePayload } from "@app/classes/socket/message-payload";
import { AuthService } from "@app/services/api/auth.service";
import { RoomService } from "@app/services/api/room.service";
import { ColorHandlerService } from "@app/services/components/color/color-handler.service";
import { IconDefinition } from "@fortawesome/fontawesome-common-types";
import {
  faAt,
  faBullhorn,
  faExclamationCircle,
  faInfoCircle,
  faPaperPlane,
  faReply,
  faTimesCircle,
} from "@fortawesome/free-solid-svg-icons";
import { UserApiService } from "@app/services/api/user-api.service";
import { ChatHandler } from "@app/classes/chat-handler";
import { MessageType } from "@app/classes/socket/message-type";
import { User } from "@app/classes/api/user";
import { AudioService } from "@app/services/audio/audio.service";

@Component({
  selector: "app-chat",
  templateUrl: "./chat.component.html",
  styleUrls: ["./chat.component.scss"],
})
export class ChatComponent implements OnInit {
  readonly sendIcon: IconDefinition = faPaperPlane;
  readonly replyIcon: IconDefinition = faReply;
  readonly stopReplyIcon: IconDefinition = faTimesCircle;
  readonly urgentTypeIcon: IconDefinition = faExclamationCircle;
  readonly informationTypeIcon: IconDefinition = faInfoCircle;
  readonly announcementTypeIcon: IconDefinition = faBullhorn;
  readonly taggedIcon: IconDefinition = faAt;
  
  readonly newLineRegex: RegExp = /(?:\r\n|\r|\n)/g;

  readonly normalType: number = 0;
  readonly urgentType: number = 1;
  readonly informationType: number = 2;
  readonly announcementType: number = 3;

  @Output() onNotification: EventEmitter<boolean> = new EventEmitter();
  @Input("room") set _room(room: Room) {
    this.room = room;
    this.onRoomLoad();
  }
  room: Room;

  @Input() enableRouteToUser: boolean = true;
  @Input() set showChatBox(show: boolean) {
    this.show = show;
    if (show) {
      this.room.has_notification = false;
      this.onNotification.emit(false);
      this.onShow();
    }
  }
  protected show: boolean = false;

  @ViewChild("msg") protected msgInput: ElementRef<HTMLTextAreaElement>;
  @ViewChild("chat") protected chatBox: ElementRef<HTMLDivElement>;

  message: string = "";
  protected autoScroll: boolean = true;

  replyingTo: MessagePayload | undefined;
  highlightedReplyMessage: MessagePayload | undefined;
  focusedMessage: MessagePayload | undefined;
  messageType: number = 0;

  taggedUser: string = "";
  showMembers: boolean = false;
  protected handler: ChatHandler;
  protected loadingHistory: boolean = false;

  constructor(
    private roomService: RoomService,
    private colorHandler: ColorHandlerService,
    private authService: AuthService,
    private router: Router,
    protected userService: UserApiService,
    protected audioService: AudioService,
  ) {
    this.handler = new ChatHandler(authService, audioService, roomService, userService);
  }

  ngOnInit(): void {
    this.handler.messageListener.subscribe(() => {
      if (!this.show && this.messages.length > 0) {
        this.room.has_notification = true;
        this.onNotification.emit(true);
      }
      if (this.autoScroll) {
        this.scrollToBottom();
      }
    });
  }

  protected onShow(): void {
    if (!this.handler.hasInitialized()) {
      this.onRoomLoad();
    } else if (this.handler.getCurrentPage() == 0 && this.room.id >= 0) {
      this.handler.loadHistory().then(() => this.scrollToBottom());
    }

    if (this.msgInput != undefined) {
      setTimeout(() => {
        this.msgInput.nativeElement.focus();
        this.scrollToBottom();
      }, 0);
    }
  }

  protected onRoomLoad(): void {
    if (this.room.id == -1)
      return;

    this.handler.onRoomLoad(this.room.id).then(() => {
      if (this.show) {
        this.handler.loadHistory().then(() => {
          this.scrollToBottom();
        });
      }
    }, () => {
      console.log("failed");
    });
  }

  replyTo(message: MessagePayload): void {
    this.replyingTo = message;
    this.scrollToBottom();
    this.msgInput.nativeElement.focus();
  }

  stopReply(): void {
    this.replyingTo = undefined;
  }

  onScroll(): void {
    if (this.chatBox.nativeElement.scrollTop == 0 && !this.loadingHistory) {
      this.chatBox.nativeElement.scrollTop = 1;
      this.loadingHistory = true;
      this.handler.loadHistory().then(() => {
        this.loadingHistory = false;
      });
    }

    this.autoScroll = this.chatBox.nativeElement.clientHeight == this.chatBox.nativeElement.scrollHeight - this.chatBox.nativeElement.scrollTop;
  }

  protected loadMembers(): void {
    if (this.handler.roomMembersCache.length == 0) {
      this.handler.loadRoomMembers().then();
    }
  }

  protected getInputSelectionWordPositionEnd(): number {
    let position = this.msgInput.nativeElement.selectionStart;
    while (this.message[position] != " " && this.message[position] != undefined) {
      position++;
    }

    return position;
  }

  completeUserTag(member: User): void {
    const position = this.getInputSelectionWordPositionEnd();
    let firstPartMessage = this.message.substring(0, position);
    const words = firstPartMessage.replace(this.newLineRegex, " ").split(" ");
    firstPartMessage = firstPartMessage.replace(new RegExp(words[words.length - 1] + "$"), `@${member.pseudonym}`);
    this.message = `${firstPartMessage} ${this.message.substring(position)}`;
    this.msgInput.nativeElement.focus();
    this.showMembers = false;
  }

  setMessage(message: string): void {
    this.message = message;
    const position = this.getInputSelectionWordPositionEnd();
    const words = this.message.substring(0, position).replace(this.newLineRegex, " ").split(" ");
    const currentWord = words[words.length - 1];
    const userTagRegex = new RegExp("^\@\w*", "gm");
    this.showMembers = userTagRegex.test(currentWord);
    this.taggedUser = this.showMembers ? currentWord.replace("@", ""):'';
    if (this.showMembers) {
      this.loadMembers();
      this.scrollToBottom();
    }
  }

  getUserColor(userId: number): string {
    this.colorHandler.color = { r: 255, g: 0, b: 0, a: 1 };
    return this.colorHandler.getUserColor(userId);
  }

  protected scrollToBottom(): void {
    setTimeout(() => {
      this.chatBox.nativeElement.scrollTop = this.chatBox.nativeElement.scrollHeight;
    }, 50);
  }

  ngOnDestroy(): void {
    this.roomService.disconnectRoom(this.room.id);
  }

  sendMessage(): void {
    if ("" == this.message.trim()) {
      return;
    }
    this.handler.sendMessage(this.createNewMessage());
    this.message = "";
    this.scrollToBottom();
    this.replyingTo = undefined;
    this.messageType = MessageType.Normal;
  }

  private createNewMessage(): MessagePayload {
    const message: MessagePayload = {
      message: this.message.trim(),
      message_type: this.messageType,
      recipients: [],
    };
    this.message.match(/(\@\w*)/g)?.forEach((match: string) => {
      const index = this.handler.roomMembersCache.findIndex((u : User) => u.pseudonym == match.replace("@", ""));
      if (index != -1 && !message.recipients.includes(this.handler.roomMembersCache[index].id)) {
        message.recipients.push(this.handler.roomMembersCache[index].id);
      }
    });
    if (this.replyingTo != undefined) 
      message.reply_to = this.replyingTo.id;

    return message;
  }

  navigateToUser(userId: number): void {
    if (!this.enableRouteToUser) 
      return;

    if (userId == this.authService.getUser().id) {
      this.router.navigateByUrl("/user");
    } else {
      this.router.navigateByUrl(`/contact/${userId}`);
    }
  }

  getUserAvatar(userId: number): string {
    return this.handler.getUserAvatar(userId);
  }

  setMessageType(type: number): void {
    if (this.messageType == type) {
      this.messageType = this.normalType;
    } else if (type <= 3 && type >= 0) {
      this.messageType = type;
    }
  }

  highlightReplyMessage(message: MessagePayload): void {
    if (message.reply_to != undefined && message.reply_to > 0) {
      this.focusedMessage = undefined;
      this.highlightedReplyMessage = this.getReplyMessage(message.reply_to);
    }
  }

  stopHighlightReplyMessage(): void {
    this.highlightedReplyMessage = undefined;
  }

  getReplyMessage(id: number): MessagePayload | undefined {
    return this.handler.messages.find((m: MessagePayload) => m.id == id);
  }

  scrollToMessage(id: number): void {
    this.focusedMessage = this.highlightedReplyMessage;
    const element = document.getElementById(`message-${id}`);
    if (element) {
      element.scrollIntoView(true);
    } else {
      this.handler.loadMessage(id).then((message: MessagePayload) => {
        this.highlightedReplyMessage = message;
        this.scrollToMessage(id)
      });
    }
  }

  getMessageHighlightClass(message: MessagePayload): string {
    if (this.replyingTo?.id == message.id) {
      return "reply-highlight";
    } else if (this.highlightedReplyMessage?.id == message.id || this.focusedMessage?.id == message.id) {
      return "reply-message-highlight";
    }

    return this.getMessageTypeHighlightClass(message);
  }

  private getMessageTypeHighlightClass(message: MessagePayload): string {
    switch (message.message_type) {
      case MessageType.Urgent: 
        return "urgent-type";
      case MessageType.Announcement:
        return "announcement-type";
      case MessageType.Information:
        return "information-type";
    }

    return this.isTagged(message) ? "tagged":"";
  }

  autoGrowInput(evt: any): void {
    evt.target.style.height = "auto";
    evt.target.style.height = evt.target.scrollHeight + "px";
    evt.target.style.maxHeight = "200px";
  }

  isTagged(message: MessagePayload): boolean {
    return message.recipients?.includes(this.authService.getUser().id);
  }

  get messages(): MessagePayload[] {
    return this.handler.messages.sort((entry1: MessagePayload, entry2: MessagePayload) => {
      if (entry1.sent_at?.isSame(entry2.sent_at)) {
        return 0;
      } else if (entry1.sent_at?.isAfter(entry2.sent_at)) {
        return 1;
      }

      return -1;
    });
  }

  get members(): User[] {
    const nameRegex = new RegExp(this.taggedUser.toLowerCase());
    return this.handler.roomMembersCache.filter((u: User) => nameRegex.test(u.pseudonym.toLowerCase()));
  }
}
