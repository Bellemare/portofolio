import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ThemePalette } from '@angular/material/core';
import { ToolAttributeComponent } from '@app/app-components/tool-attributes/tool-attribute/tool-attribute.component';

@Component({
    selector: 'app-slide-toggle',
    templateUrl: './slide-toggle.component.html',
    styleUrls: ['./slide-toggle.component.scss'],
})
export class SlideToggleComponent extends ToolAttributeComponent {
    readonly COLOR: ThemePalette = 'accent';
    @Input() value: boolean = false;
    @Output() valueChange: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Input() label: string = 'Label';

    protected isValid(value: boolean): boolean {
        return true;
    }
}
