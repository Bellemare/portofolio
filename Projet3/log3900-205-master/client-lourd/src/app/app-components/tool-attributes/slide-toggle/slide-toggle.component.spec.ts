import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '@app/material/material.module';
import { SlideToggleComponent } from './slide-toggle.component';

// tslint:disable:no-string-literal
describe('SlideToggleComponent', () => {
    let component: SlideToggleComponent;
    let fixture: ComponentFixture<SlideToggleComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SlideToggleComponent],
            imports: [MaterialModule, BrowserAnimationsModule],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SlideToggleComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('isValid should return true', () => {
        expect(component['isValid'](false)).toEqual(true);
        expect(component['isValid'](true)).toEqual(true);
    });
});
