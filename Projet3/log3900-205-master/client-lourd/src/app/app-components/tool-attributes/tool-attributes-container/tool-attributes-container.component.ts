import { Component, ComponentFactoryResolver, Input, ViewChild } from '@angular/core';
import { ToolAttributesDirective } from '@app/app-components/tool-attributes/tool-attributes.directive';
import { ToolAttributesComponent } from '@app/app-components/tool-components/tool-attributes/tool-attributes.component';
import { ToolService } from '@app/services/tools/tool.service';

@Component({
    selector: 'app-tool-attributes-container',
    templateUrl: './tool-attribute-container.component.html',
    styleUrls: ['./tool-attribute-container.component.scss'],
})
export class ToolAttributesContainerComponent {
    @Input() set toolAttributesComponent(toolAttributesComponent: new () => ToolAttributesComponent) {
        this.initComponent(toolAttributesComponent);
    }
    @Input() toolService: ToolService;

    @ViewChild(ToolAttributesDirective, { static: true }) private toolAttributeHost: ToolAttributesDirective;

    constructor(private componentFactoryResolver: ComponentFactoryResolver) {}

    private initComponent(toolAttributesComponent: new (t: ToolService) => ToolAttributesComponent): void {
        const componentFactory = this.componentFactoryResolver.resolveComponentFactory(toolAttributesComponent);

        const viewContainerRef = this.toolAttributeHost.viewContainerRef;
        viewContainerRef.clear();

        const containerRef = viewContainerRef.createComponent<ToolAttributesComponent>(componentFactory);
        containerRef.instance.init();
    }
}
