import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ToolAttributesContainerComponent } from './tool-attributes-container.component';

describe('ToolAttributeContainerComponent', () => {
    let component: ToolAttributesContainerComponent;
    let fixture: ComponentFixture<ToolAttributesContainerComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ToolAttributesContainerComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ToolAttributesContainerComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
