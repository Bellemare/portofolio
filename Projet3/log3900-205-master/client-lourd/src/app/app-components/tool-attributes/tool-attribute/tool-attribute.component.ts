import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ToolProperty } from '@app/classes/tool-properties/tool-property';

@Component({
    selector: 'toolAttribute',
    templateUrl: './tool-attribute.component.html',
    styleUrls: ['./tool-attribute.component.scss'],
})
export class ToolAttributeComponent {
    @Input() value: ToolProperty = 0;
    @Output() valueChange: EventEmitter<ToolProperty> = new EventEmitter<ToolProperty>();

    valueChanged(value: ToolProperty): void {
        if (this.isValid(value)) {
            this.value = value;
            this.valueChange.emit(value);
        }
    }

    // Les enfants implémentent le fonctionnement
    protected isValid(value?: ToolProperty): boolean {
        return false;
    }
}
