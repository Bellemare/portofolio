import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ToolAttributeComponent } from './tool-attribute.component';

describe('ToolAttributeComponent', () => {
    let component: ToolAttributeComponent;
    let fixture: ComponentFixture<ToolAttributeComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ToolAttributeComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ToolAttributeComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should emit event on value change when value is valid', () => {
        // tslint:disable:no-any
        spyOn<any>(component, 'isValid').and.returnValue(true);
        const valueChangeSpy = spyOn(component.valueChange, 'emit');
        const value = 5;
        component.valueChanged(value);
        expect(valueChangeSpy).toHaveBeenCalledWith(value);
    });

    it('should not emit when value is not valid', () => {
        // tslint:disable:no-any
        spyOn<any>(component, 'isValid').and.callThrough();
        const valueChangeSpy = spyOn(component.valueChange, 'emit');
        component.valueChanged(0);
        expect(valueChangeSpy).not.toHaveBeenCalled();
    });
});
