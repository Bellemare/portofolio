import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { MatSlider } from '@angular/material/slider';
import { ToolAttributeComponent } from '@app/app-components/tool-attributes/tool-attribute/tool-attribute.component';
import { ToolProperty } from '@app/classes/tool-properties/tool-property';

@Component({
    selector: 'app-slider',
    templateUrl: './slider.component.html',
    styleUrls: ['./slider.component.scss'],
})
export class SliderComponent extends ToolAttributeComponent {
    @ViewChild('slider', { static: false }) private slider: MatSlider;

    @Input() value: number = 0;
    @Output() valueChange: EventEmitter<number> = new EventEmitter<number>();
    @Input() min: number;
    @Input() max: number;
    @Input() unit: string;
    @Input() step: number = 1;

    onMouseUp(): void {
        this.slider.blur();
    }

    protected isValid(value: ToolProperty): boolean {
        return value >= this.min && value <= this.max;
    }
}
