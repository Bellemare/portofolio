import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSlider } from '@angular/material/slider';
import { MaterialModule } from '@app/material/material.module';
import { SliderComponent } from './slider.component';

// tslint:disable:no-string-literal
// tslint:disable:no-magic-numbers
describe('SliderComponent', () => {
    let component: SliderComponent;
    let fixture: ComponentFixture<SliderComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SliderComponent],
            imports: [MaterialModule],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SliderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();

        TestBed.createComponent(MatSlider);
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should unfocus slider on mouseup', () => {
        const blurSpy = spyOn(component['slider'], 'blur');
        component.onMouseUp();
        expect(blurSpy).toHaveBeenCalled();
    });

    it('should check if value is valid', () => {
        component.min = 0;
        component.max = 10;
        expect(component['isValid'](-1)).toEqual(false);
        expect(component['isValid'](5)).toEqual(true);
        expect(component['isValid'](11)).toEqual(false);
    });
});
