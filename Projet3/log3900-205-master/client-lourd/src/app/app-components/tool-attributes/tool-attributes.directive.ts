import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
    selector: '[appToolAttributes]',
})
export class ToolAttributesDirective {
    constructor(public viewContainerRef: ViewContainerRef) {}
}
