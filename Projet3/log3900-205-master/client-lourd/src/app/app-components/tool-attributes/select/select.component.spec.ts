import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '@app/material/material.module';
import { SelectComponent } from './select.component';

// tslint:disable:no-string-literal
describe('SelectComponent', () => {
    let component: SelectComponent;
    let fixture: ComponentFixture<SelectComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SelectComponent],
            imports: [MaterialModule, BrowserAnimationsModule],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SelectComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should check if value is valid', () => {
        component.values = [0];
        expect(component['isValid'](0)).toEqual(true);
        expect(component['isValid'](1)).toEqual(false);
    });

    it('valueChanged should close material select ', () => {
        component.values = [0];
        const closeSpy = spyOn(component['select'], 'close');
        component.valueChanged(0);
        expect(closeSpy).toHaveBeenCalled();
    });
});
