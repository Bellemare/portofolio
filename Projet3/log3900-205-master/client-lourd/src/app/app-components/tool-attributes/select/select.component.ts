import { Component, Input, ViewChild } from '@angular/core';
import { MatSelect } from '@angular/material/select';
import { ToolAttributeComponent } from '@app/app-components/tool-attributes/tool-attribute/tool-attribute.component';
import { ToolProperty } from '@app/classes/tool-properties/tool-property';

@Component({
    selector: 'app-select',
    templateUrl: './select.component.html',
    styleUrls: ['./select.component.scss'],
})
export class SelectComponent extends ToolAttributeComponent {
    @Input() values: ToolProperty[];
    @Input() valueLabels: string[];

    @ViewChild('select', { static: false }) private select: MatSelect;

    protected isValid(value: ToolProperty): boolean {
        return this.values.some((val: ToolProperty) => val === value);
    }

    valueChanged(event: ToolProperty): void {
        this.select.close();
        super.valueChanged(event);
    }
}
