import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { Color } from '@app/classes/drawing/color';
import { Vec2 } from '@app/classes/drawing/vec2';
import { MouseButton } from '@app/classes/mouse-button';
import { MouseEventWrapper } from '@app/classes/mouse-event-wrapper';
import { ColorHandlerService } from '@app/services/components/color/color-handler.service';
import { ColorService } from '@app/services/tools/color.service';

@Component({
    selector: 'app-color-canvas-element',
    templateUrl: './color-canvas-element.component.html',
    styleUrls: ['./color-canvas-element.component.scss'],
})
export abstract class ColorCanvasElementComponent implements AfterViewInit {
    protected readonly WIDTH: number = 0;
    protected readonly HEIGHT: number = 0;

    @ViewChild('canvas', { static: true }) protected canvas: ElementRef<HTMLCanvasElement>;
    protected ctx: CanvasRenderingContext2D;

    protected mouseDown: boolean = false;

    constructor(protected colorHandlerService: ColorHandlerService) {}

    ngAfterViewInit(): void {
        this.ctx = this.canvas.nativeElement.getContext('2d') as CanvasRenderingContext2D;
    }

    onMouseDown(event: MouseEvent): void {
        if (event.button === MouseButton.Left) {
            this.mouseDown = true;
            this.elementChanged({ x: event.offsetX, y: event.offsetY });
        }
    }

    onMouseMove(event: MouseEvent): void {
        if (this.mouseDown) {
            this.elementChanged({ x: event.offsetX, y: event.offsetY });
        }
    }

    onMouseEnter(event: MouseEvent): void {
        this.mouseDown = this.mouseDown && MouseEventWrapper.isLeftButtonPressed(event);
    }

    onMouseUp(): void {
        this.mouseDown = false;
    }

    // On désactive le no-empty, puisque la classe est abstraite et on ne fourni pas d'implémentation de base.
    // tslint:disable-next-line:no-empty
    protected elementChanged(pos: Vec2): void {}

    protected getColorAtPosition(pos: Vec2): Color {
        const pixel = this.ctx.getImageData(pos.x, pos.y, 1, 1).data;
        return {
            r: pixel[ColorService.PIXEL_RED_INDEX],
            g: pixel[ColorService.PIXEL_GREEN_INDEX],
            b: pixel[ColorService.PIXEL_BLUE_INDEX],
            a: pixel[ColorService.PIXEL_ALPHA_INDEX] / ColorService.MAX_VALUE,
        };
    }

    protected reset(): void {
        this.ctx.clearRect(0, 0, this.canvasWidth, this.canvasHeight);
    }

    get canvasWidth(): number {
        return this.WIDTH;
    }

    get canvasHeight(): number {
        return this.HEIGHT;
    }
}
