import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponentsModule } from '@app/app-components/app-components.module';
import { Color } from '@app/classes/drawing/color';
import { MaterialModule } from '@app/material/material.module';
import { ColorPaletteComponent } from './color-palette.component';

// tslint:disable:no-any
// tslint:disable:no-string-literal
// tslint:disable:no-magic-numbers
describe('ColorPaletteComponent', () => {
    let component: ColorPaletteComponent;
    let fixture: ComponentFixture<ColorPaletteComponent>;

    let color: Color;
    let canvas: HTMLCanvasElement;
    let ctx: CanvasRenderingContext2D;

    beforeEach(async(() => {
        color = { r: 78, g: 166, b: 78, a: 1 };

        TestBed.configureTestingModule({
            declarations: [ColorPaletteComponent],
            imports: [MaterialModule, AppComponentsModule, BrowserAnimationsModule],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ColorPaletteComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();

        canvas = document.createElement('canvas');
        canvas.width = component.canvasWidth;
        canvas.height = component.canvasHeight;
        ctx = canvas.getContext('2d') as CanvasRenderingContext2D;
        component['ctx'] = ctx;
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should set correct color on startPickingColor', () => {
        component.startColorPicking(color);
        expect(component['colorHandlerService'].color).toEqual(color);
        expect(component['newColor']).toEqual(color);
    });

    it('should init sliders on startPickingColor', () => {
        const alphasliderSpy = spyOn(component['alphaSlider'], 'init');
        const colorsliderSpy = spyOn(component['colorSlider'], 'init');
        component.startColorPicking(color);
        expect(alphasliderSpy).toHaveBeenCalledWith(color);
        expect(colorsliderSpy).toHaveBeenCalled();
    });

    it('should draw gradient on startPickingColor', () => {
        component.startColorPicking(color);
        const topLeftCorner = Array.from(ctx.getImageData(0, 0, 1, 1).data);
        const topRightCorner = Array.from(ctx.getImageData(canvas.width - 1, 0, 1, 1).data);
        const bottomRightCorner = Array.from(ctx.getImageData(canvas.width - 1, canvas.height - 1, 1, 1).data);
        const bottomLeftCorner = Array.from(ctx.getImageData(0, canvas.height - 1, 1, 1).data);
        expect(topLeftCorner[3]).toBeGreaterThan(0);
        expect(topRightCorner[3]).toBeGreaterThan(0);
        expect(bottomRightCorner[3]).toBeGreaterThan(0);
        expect(bottomLeftCorner[3]).toBeGreaterThan(0);
    });

    it('should emit cancel event on cancel', () => {
        const cancelEventSpy = spyOn(component.cancelEvent, 'emit');
        component.cancel();
        expect(cancelEventSpy).toHaveBeenCalled();
    });

    it('should emit color change event on confirm', () => {
        const colorChangeSpy = spyOn(component.colorChange, 'emit');
        component['newColor'] = color;
        component.confirm();
        expect(colorChangeSpy).toHaveBeenCalledWith(color);
    });

    it('should redraw and set correct color on hue change', () => {
        component['newColor'] = color;
        component['colorHandlerService'].color = color;
        const drawGradientSpy = spyOn<any>(component, 'drawColorGradient');
        const alphaSliderSpy = spyOn(component['alphaSlider'], 'init');
        const colorShiftSpy = spyOn(component['colorHandlerService'], 'colorShift').and.callThrough();
        const hue = 180;
        const newColor = { r: 78, g: 166, b: 166, a: 1 };
        component.colorHueChanged(hue);
        expect(drawGradientSpy).toHaveBeenCalled();
        expect(alphaSliderSpy).toHaveBeenCalledWith(newColor);
        expect(colorShiftSpy).toHaveBeenCalledWith(hue);
        expect(component['newColor']).toEqual(newColor);
    });

    it('should set correct alpha on alpha change', () => {
        component['newColor'] = color;
        const alpha = 0.5;
        component.alphaChanged(alpha);
        expect(component['newColor'].a).toEqual(alpha);
    });

    it('should have correct hexcode', () => {
        component['newColor'] = color;
        expect(component.colorHexCode).toEqual('#4EA64E');
    });

    it('should set correct color from hexcode', () => {
        const newColor = { r: 0, g: 255, b: 0, a: 1 };
        component.hexCodeChanged('#00ff00');
        expect(component['newColor']).toEqual(newColor);

        component['newColor'] = color;
        component.hexCodeChanged('#0f0');
        expect(component['newColor']).toEqual(newColor);
    });

    it('should set error when setting invalid hexcode', () => {
        component['newColor'] = color;
        component['hexCodeError'] = false;
        component.hexCodeChanged('#00ff0');
        expect(component['newColor']).toEqual(color);
        expect(component.hexCodeError).toEqual(true);
    });

    it('should return correct color string', () => {
        component['newColor'] = color;
        expect(component.newColorStr).toEqual(`rgba(${color.r},${color.g},${color.b},${color.a})`);
    });

    it('should set correct color on mousemove', () => {
        component.startColorPicking(color);
        component['mouseDown'] = true;
        const colorData = Array.from(ctx.getImageData(component.canvasWidth / 2, 0, 1, 1).data);
        const newColor = { r: colorData[0], g: colorData[1], b: colorData[2], a: colorData[3] / 255 };
        const mouseEvent = {
            offsetX: component.canvasWidth / 2,
            offsetY: 0,
        } as MouseEvent;
        component.onMouseMove(mouseEvent);
        expect(component['newColor']).toEqual(newColor);
    });
});
