import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { ColorCanvasElementComponent } from '@app/app-components/color/color-canvas-element/color-canvas-element.component';
import { ColorSliderComponent } from '@app/app-components/color/color-slider/color-slider.component';
import { Color } from '@app/classes/drawing/color';
import { Vec2 } from '@app/classes/drawing/vec2';
import { ColorHandlerService } from '@app/services/components/color/color-handler.service';
import { ColorService } from '@app/services/tools/color.service';

@Component({
    selector: 'app-color-palette',
    templateUrl: './color-palette.component.html',
    styleUrls: ['./color-palette.component.scss'],
})
export class ColorPaletteComponent extends ColorCanvasElementComponent {
    private readonly HEX_CODE_SIMPLE_REGEX: RegExp = /^#?([a-fA-F\d]{2})([a-fA-F\d]{2})([a-fA-F\d]{2})$/;
    private readonly HEX_CODE_DUAL_REGEX: RegExp = /^#?([a-fA-F\d]{1})([a-fA-F\d]{1})([a-fA-F\d]{1})$/;
    private readonly R_REGEX_POSITION: number = 1;
    private readonly G_REGEX_POSITION: number = 2;
    private readonly B_REGEX_POSITION: number = 3;
    readonly HEX_CODE_LENGTH: number = 6;

    private readonly TIMEOUT_BEFORE_SHOW_SELECTION: number = 10;

    private readonly COLOR_BLACK: Color = { r: 0, g: 0, b: 0, a: 1 };
    private readonly COLOR_BLACK_TRANSPARENT: Color = { r: 0, g: 0, b: 0, a: 0 };
    private readonly COLOR_WHITE: Color = { r: 255, g: 255, b: 255, a: 1 };
    private readonly COLOR_WHITE_TRANSPARENT: Color = { r: 255, g: 255, b: 255, a: 0 };

    readonly SELECTION_RADIUS: number = 5;

    protected readonly WIDTH: number = 220;
    protected readonly HEIGHT: number = 220;

    @ViewChild('colorslider', { static: true }) private colorSlider: ColorSliderComponent;

    @Output() colorChange: EventEmitter<Color> = new EventEmitter<Color>();
    @Output() cancelEvent: EventEmitter<void> = new EventEmitter<void>();

    private newColor: Color = { r: 0, g: 0, b: 0, a: 1 };

    hexCodeError: boolean = false;
    private selectionX: number;
    private selectionY: number;

    constructor(protected colorHandlerService: ColorHandlerService) {
        super(colorHandlerService);
    }

    cancel(): void {
        this.cancelEvent.emit();
    }

    confirm(): void {
        this.colorChange.emit(this.newColor);
    }

    protected elementChanged(size: Vec2): void {
        size.x = this.clampToCanvas(size.x, this.canvasWidth);
        size.y = this.clampToCanvas(size.y, this.canvasHeight);
        const color = this.getColorAtPosition(size);
        this.newColor = { r: color.r, g: color.g, b: color.b, a: this.newColor.a };
        this.colorHandlerService.color = color;
        this.drawSelection(size.x, size.y);
    }

    startColorPicking(color: Color): void {
        this.colorHandlerService.color = { r: color.r, g: color.g, b: color.b, a: color.a };
        this.newColor = color;
        this.colorSlider.init();
        this.drawColorGradient(this.colorSlider.color);
        setTimeout(() => {
            this.drawSelection();
        }, this.TIMEOUT_BEFORE_SHOW_SELECTION);
    }

    colorHueChanged(hue: number): void {
        this.colorHandlerService.colorShift(hue);
        this.drawColorGradient(this.colorSlider.color);
        const color = this.colorHandlerService.color;
        this.newColor = { r: color.r, g: color.g, b: color.b, a: this.newColor.a };
    }

    alphaChanged(alpha: number): void {
        this.newColor = { r: this.newColor.r, g: this.newColor.g, b: this.newColor.b, a: alpha };
    }

    protected drawSelection(x: number | null = null, y: number | null = null): void {
        if (x === null) {
            this.selectionX = this.colorHandlerService.colorSaturation * this.canvasWidth;
        } else {
            this.selectionX = x;
        }
        if (y === null) {
            this.selectionY = (1 - this.colorHandlerService.colorBrightness) * this.canvasHeight;
        } else {
            this.selectionY = y;
        }
        this.selectionX += this.canvas.nativeElement.offsetLeft - this.SELECTION_RADIUS;
        this.selectionY += this.canvas.nativeElement.offsetTop - this.SELECTION_RADIUS;
    }

    // Code inspiré de https://malcoded.com/posts/angular-color-picker/
    protected drawColorGradient(color: Color): void {
        const width = this.canvas.nativeElement.width;
        const height = this.canvas.nativeElement.height;

        this.ctx.fillStyle = ColorService.getColorRGBA(color);
        this.ctx.fillRect(0, 0, width, height);

        this.ctx.fillStyle = this.getGradient(this.COLOR_WHITE, this.COLOR_WHITE_TRANSPARENT, { x: width, y: 0 });
        this.ctx.fillRect(0, 0, width, height);

        this.ctx.fillStyle = this.getGradient(this.COLOR_BLACK_TRANSPARENT, this.COLOR_BLACK, { x: 0, y: height });
        this.ctx.fillRect(0, 0, width, height);
    }

    private getGradient(startColor: Color, endColor: Color, size: Vec2): CanvasGradient {
        const gradient = this.ctx.createLinearGradient(0, 0, size.x, size.y);
        gradient.addColorStop(0, ColorService.getColorRGBA(startColor));
        gradient.addColorStop(1, ColorService.getColorRGBA(endColor));
        return gradient;
    }

    clampToCanvas(x: number, canvasSize: number): number {
        return Math.max(Math.min(canvasSize, x), 0);
    }

    private transformToHex(num: number): string {
        const str = Math.round(num).toString(16).toUpperCase();
        return str.length < 2 ? str.repeat(2) : str;
    }

    private parseHexValue(hex: string): number {
        return parseInt(hex.length < 2 ? hex.repeat(2) : hex, 16);
    }

    hexCodeChanged(value: string): void {
        const simpleTest = this.HEX_CODE_SIMPLE_REGEX.exec(value);
        const dualTest = this.HEX_CODE_DUAL_REGEX.exec(value);
        this.hexCodeError = !(simpleTest || dualTest);
        if (!this.hexCodeError) {
            const res = (simpleTest ? simpleTest : dualTest) as RegExpExecArray;
            this.newColor = {
                r: this.parseHexValue(res[this.R_REGEX_POSITION]),
                g: this.parseHexValue(res[this.G_REGEX_POSITION]),
                b: this.parseHexValue(res[this.B_REGEX_POSITION]),
                a: this.newColor.a,
            };
            this.colorHandlerService.color = this.newColor;
            this.colorSlider.init();
            this.drawSelection();
            this.reset();
            this.drawColorGradient(this.colorSlider.color);
        }
    }

    get newColorStr(): string {
        return ColorService.getColorRGBA(this.newColor);
    }

    get colorHexCode(): string {
        return `#${this.transformToHex(this.newColor.r)}${this.transformToHex(this.newColor.g)}${this.transformToHex(this.newColor.b)}`;
    }

    get selectionPos(): Vec2 {
        return { x: this.selectionX, y: this.selectionY };
    }
}
