import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponentsModule } from '@app/app-components/app-components.module';
import { Color } from '@app/classes/drawing/color';
import { MouseButton } from '@app/classes/mouse-button';
import { MaterialModule } from '@app/material/material.module';
import { AttributesManagerService } from '@app/services/api/attributes-manager.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ColorPickerComponent } from './color-picker.component';

// tslint:disable:no-empty
// tslint:disable:no-string-literal
describe('ColorPickerComponent', () => {
    let component: ColorPickerComponent;
    let fixture: ComponentFixture<ColorPickerComponent>;

    let primaryColor: Color;
    let secondaryColor: Color;

    let colorPaletteSpy: jasmine.Spy<(color: Color) => void>;
    let primaryColorSpy: jasmine.Spy<(color: Color) => void>;
    let secondaryColorSpy: jasmine.SpyObj<(color: Color) => void>;
    let attributesManagerServiceSpy: jasmine.SpyObj<AttributesManagerService>;

    let latest: Color[];
    let placeholder: Color;

    beforeEach(async(() => {
        primaryColor = { r: 0, g: 0, b: 0, a: 1 };
        secondaryColor = { r: 255, g: 255, b: 255, a: 1 };

        attributesManagerServiceSpy = jasmine.createSpyObj(AttributesManagerService, [
            'getPrimaryColor',
            'getSecondaryColor',
            'getLatestColors',
            'saveSecondaryColor',
            'savePrimaryColor',
        ]);
        attributesManagerServiceSpy.getPrimaryColor.and.returnValue(primaryColor);
        attributesManagerServiceSpy.getSecondaryColor.and.returnValue(secondaryColor);
        attributesManagerServiceSpy.getLatestColors.and.returnValue([primaryColor, secondaryColor]);
        attributesManagerServiceSpy.savePrimaryColor.and.callFake((color: Color) => {});
        attributesManagerServiceSpy.saveSecondaryColor.and.callFake((color: Color) => {});

        placeholder = { r: -1, g: -1, b: -1, a: -1 };
        latest = [
            primaryColor,
            secondaryColor,
            placeholder,
            placeholder,
            placeholder,
            placeholder,
            placeholder,
            placeholder,
            placeholder,
            placeholder,
        ];

        TestBed.configureTestingModule({
            providers: [{ provide: AttributesManagerService, useValue: attributesManagerServiceSpy }],
            declarations: [ColorPickerComponent],
            imports: [AppComponentsModule, MaterialModule, FontAwesomeModule, BrowserAnimationsModule],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ColorPickerComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();

        colorPaletteSpy = spyOn(component['colorPalette'], 'startColorPicking').and.callFake((color: Color) => {});

        primaryColorSpy = spyOn(component['colorService'], 'setPrimaryColor');
        secondaryColorSpy = spyOn(component['colorService'], 'setSecondaryColor');
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should start color picking on palette with correct color', () => {
        component.startColorPicking(true);
        expect(colorPaletteSpy).toHaveBeenCalledWith(primaryColor);

        component.startColorPicking(false);
        expect(colorPaletteSpy).toHaveBeenCalledWith(secondaryColor);
    });

    it('should swap colors', () => {
        const swapSpy = spyOn(component['colorService'], 'swapColors');
        component.swap();
        expect(swapSpy).toHaveBeenCalled();
    });

    it('should hide elements on cancel', () => {
        component['showPickerEls'] = true;
        component.cancelPicking();
        expect(component.showPickerEls).toEqual(false);
    });

    it('should set primary on color service when setting new primary color', () => {
        component['changingPrimaryColor'] = true;
        component.setNewColor(primaryColor);
        expect(primaryColorSpy).toHaveBeenCalledWith(primaryColor);
    });

    it('should set secondary on color service when setting new secondary color', () => {
        component['changingPrimaryColor'] = false;
        component.setNewColor(secondaryColor);
        expect(secondaryColorSpy).toHaveBeenCalledWith(secondaryColor);
    });

    it('should set latest color placeholders when there are not 10 colors', () => {
        expect(component.latestColors).toEqual(latest);
    });

    it('should not select placeholder latest color', () => {
        const mouseEvent = {} as MouseEvent;
        component.latestSelected(mouseEvent, placeholder);
        expect(component.priColor).toEqual(primaryColor);
    });

    it('should set primary color when left clicking on latest color', () => {
        const mouseEvent = {
            button: MouseButton.Left,
            buttons: 1,
        } as MouseEvent;
        const color = { r: 20, g: 20, b: 20, a: 1 };
        component.latestSelected(mouseEvent, color);
        expect(primaryColorSpy).toHaveBeenCalledWith(color);
    });

    it('should set secondary color when right clicking on latest color', () => {
        const mouseEvent = {
            button: MouseButton.Right,
            buttons: 1,
        } as MouseEvent;
        const color = { r: 20, g: 20, b: 20, a: 1 };
        component.latestSelected(mouseEvent, color);
        expect(secondaryColorSpy).toHaveBeenCalledWith(color);
    });
});
