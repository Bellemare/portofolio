import { Component, ViewChild } from '@angular/core';
import { ColorPaletteComponent } from '@app/app-components/color/color-palette/color-palette.component';
import { Color } from '@app/classes/drawing/color';
import { ColorService } from '@app/services/tools/color.service';
import { SelectionService } from '@app/services/tools/selection/selection.service';

@Component({
    selector: 'app-color-picker',
    templateUrl: './color-picker.component.html',
    styleUrls: ['./color-picker.component.scss'],
})
export class ColorPickerComponent {
    @ViewChild('palette', { static: true }) private colorPalette: ColorPaletteComponent;

    priColor: Color;
    secColor: Color;
    textColor: Color;

    showPickerEls: boolean = false;
    isTransparent: boolean = false;

    private changingColor: string = "";

    constructor(private colorService: ColorService, private selectionService: SelectionService) {
        this.listenColorChanges();
    }

    swap(): void {
        this.colorService.swapColors();
    }

    cancelPicking(): void {
        this.showPickerEls = false;
    }

    setNewColor(color: Color): void {
        this.showPickerEls = false;
        if (this.changingColor == "primary") {
            this.colorService.setPrimaryColor(color);
        } else if (this.changingColor == "text") {
            this.colorService.setTextColor(color);
        } else {
            this.colorService.setSecondaryColor(color);
        }
    }

    setTransparency(checked: boolean): void {
        this.isTransparent = checked;
        this.secColor.a = this.isTransparent ? 0:1;
        this.colorService.setSecondaryColor(this.secColor);
    }

    listenColorChanges(): void {
        this.colorService.getPrimaryColor().subscribe((color: Color) => (this.priColor = color));
        this.colorService.getSecondaryColor().subscribe((color: Color) => {
            this.secColor = color;
            this.isTransparent = this.secColor.a == 0;
        });
        this.colorService.getTextColor().subscribe((color: Color) => (this.textColor = color));
    }

    startColorPickingPrimary(): void {
        this.showPickerEls = true;
        this.changingColor = "primary"
        this.colorPalette.startColorPicking(this.priColor);
    }

    startColorPickingText(): void {
        this.showPickerEls = true;
        this.changingColor = "text"
        this.colorPalette.startColorPicking(this.textColor);
    }

    startColorPickingSecondary(): void {
        this.showPickerEls = true;
        this.changingColor = "secondary"
        this.colorPalette.startColorPicking(this.secColor);
    }

    getColorStr(color: Color): string {
        return color.a > 0 ? ColorService.getColorRGB(color) : '';
    }

    get hasSelection(): boolean {
        return this.selectionService.hasSelection;
    }
}
