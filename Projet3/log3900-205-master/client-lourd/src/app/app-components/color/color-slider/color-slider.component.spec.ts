import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Color } from '@app/classes/drawing/color';
import { MouseButton } from '@app/classes/mouse-button';
import { ColorSliderComponent } from './color-slider.component';

// tslint:disable:no-any
// tslint:disable:no-magic-numbers
// tslint:disable:no-string-literal
describe('ColorSliderComponent', () => {
    let component: ColorSliderComponent;
    let fixture: ComponentFixture<ColorSliderComponent>;

    let color: Color;
    let canvas: HTMLCanvasElement;
    let ctx: CanvasRenderingContext2D;

    beforeEach(async(() => {
        color = { r: 78, g: 166, b: 78, a: 1 };

        TestBed.configureTestingModule({
            declarations: [ColorSliderComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ColorSliderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();

        component['colorHandlerService'].color = color;
        canvas = document.createElement('canvas');
        canvas.width = component.canvasWidth;
        canvas.height = component.canvasHeight;
        ctx = canvas.getContext('2d') as CanvasRenderingContext2D;
        component['ctx'] = ctx;
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should draw color gradient on init', () => {
        component.init();
        const topLeftCorner = Array.from(ctx.getImageData(0, 0, 1, 1).data);
        const topRightCorner = Array.from(ctx.getImageData(canvas.width - 1, 0, 1, 1).data);
        const bottomRightCorner = Array.from(ctx.getImageData(canvas.width - 1, canvas.height - 1, 1, 1).data);
        const bottomLeftCorner = Array.from(ctx.getImageData(0, canvas.height - 1, 1, 1).data);

        expect(topLeftCorner[3]).toBeGreaterThan(0);
        expect(topRightCorner[3]).toBeGreaterThan(0);
        expect(bottomRightCorner[3]).toBeGreaterThan(0);
        expect(bottomLeftCorner[3]).toBeGreaterThan(0);
    });

    it('should set selection at good location on init', () => {
        const drawSelectionSpy = spyOn<any>(component, 'drawSelection');
        component.init();
        expect(drawSelectionSpy).toHaveBeenCalledWith(73.33333333333333);
    });

    it('should return clamped width when calling getWidth outside threshold values', () => {
        expect(component['getWidth'](-1)).toEqual(component['WIDTH_LEFT_THRESHOLD']);
        expect(component['getWidth'](221)).toEqual(220);
    });

    it('should call element changed when mouse moving and mouse down is set', () => {
        component['mouseDown'] = true;
        const mouseEvent = {
            offsetX: 120,
            offsetY: 20,
        } as MouseEvent;
        const elementChangedSpy = spyOn<any>(component, 'elementChanged');
        component.onMouseMove(mouseEvent);
        expect(elementChangedSpy).toHaveBeenCalledWith({ x: mouseEvent.offsetX, y: mouseEvent.offsetY });
    });

    it('should not call element changed when mouse moving and mouse down is not set', () => {
        component['mouseDown'] = false;
        const mouseEvent = {} as MouseEvent;
        const elementChangedSpy = spyOn<any>(component, 'elementChanged');
        component.onMouseMove(mouseEvent);
        expect(elementChangedSpy).toHaveBeenCalledTimes(0);
    });

    it('should set correct color and reset canvas on mouse move', () => {
        component['mouseDown'] = true;
        const mouseEvent = {
            offsetX: 0,
            offsetY: 20,
        } as MouseEvent;
        const newColor = { r: 255, g: 3, b: 0, a: 1 };
        const resetSpy = spyOn<any>(component, 'reset');
        component.onMouseMove(mouseEvent);
        expect(resetSpy).toHaveBeenCalled();
        expect(component.color).toEqual(newColor);
    });

    it('should output correct hue on mouse move', async (done: DoneFn) => {
        component['mouseDown'] = true;
        const mouseEvent = {
            offsetX: 0,
            offsetY: 20,
        } as MouseEvent;
        component.colorHueChange.subscribe((hue: number) => {
            expect(hue).toEqual(0);
            done();
        });
        component.onMouseMove(mouseEvent);
    });

    it('should call elementChanged on mouse down with left click', () => {
        const mouseEvent = {
            offsetX: 0,
            offsetY: 20,
            button: MouseButton.Left,
        } as MouseEvent;
        const elementChangedSpy = spyOn<any>(component, 'elementChanged');
        component.onMouseDown(mouseEvent);
        expect(elementChangedSpy).toHaveBeenCalledWith({ x: mouseEvent.offsetX, y: mouseEvent.offsetY });
    });

    it('should not call elementChanged on mouse down not left click', () => {
        const mouseEvent = {
            offsetX: 0,
            offsetY: 20,
            button: MouseButton.Right,
        } as MouseEvent;
        const elementChangedSpy = spyOn<any>(component, 'elementChanged');
        component.onMouseDown(mouseEvent);
        expect(elementChangedSpy).toHaveBeenCalledTimes(0);
    });

    it('should unset mouseDown when no buttons are pressed or not left click on mouse enter', () => {
        component['mouseDown'] = true;
        const mouseEvent = {
            offsetX: 0,
            offsetY: 20,
            buttons: 0,
            button: MouseButton.Right,
        };
        component.onMouseEnter(mouseEvent as MouseEvent);
        expect(component['mouseDown']).toEqual(false);

        component['mouseDown'] = true;
        mouseEvent.button = MouseButton.Left;
        component.onMouseEnter(mouseEvent as MouseEvent);
        expect(component['mouseDown']).toEqual(false);
    });

    it('should unset mouseDown on mouse up', () => {
        component['mouseDown'] = true;
        component.onMouseUp();
        expect(component['mouseDown']).toEqual(false);
    });
});
