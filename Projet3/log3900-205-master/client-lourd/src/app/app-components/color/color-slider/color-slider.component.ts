import { Component, EventEmitter, Output } from '@angular/core';
import { ColorCanvasElementComponent } from '@app/app-components/color/color-canvas-element/color-canvas-element.component';
import { Color } from '@app/classes/drawing/color';
import { Vec2 } from '@app/classes/drawing/vec2';
import { ColorHandlerService } from '@app/services/components/color/color-handler.service';
import { ColorService } from '@app/services/tools/color.service';

@Component({
    selector: 'app-color-slider',
    templateUrl: './color-slider.component.html',
    styleUrls: ['./color-slider.component.scss'],
})
export class ColorSliderComponent extends ColorCanvasElementComponent {
    private readonly CIRCLE_DEGREES: number = 360;
    private readonly WIDTH_LEFT_THRESHOLD: number = 0;
    private readonly WIDTH_RIGHT_THRESHOLD: number = 0;

    private readonly COLOR_RED: Color = { r: 255, g: 0, b: 0, a: 1 };
    private readonly COLOR_BLUE: Color = { r: 0, g: 0, b: 255, a: 1 };
    private readonly COLOR_GREEN: Color = { r: 0, g: 255, b: 0, a: 1 };
    private readonly COLOR_YELLOW: Color = { r: 255, g: 255, b: 0, a: 1 };
    private readonly COLOR_TURQUOISE: Color = { r: 0, g: 255, b: 255, a: 1 };
    private readonly COLOR_MAGENTA: Color = { r: 255, g: 0, b: 255, a: 1 };

    private readonly SELECTION_LINE_WIDTH: number = 2;
    private readonly SELECTION_WIDTH: number = 5;
    private readonly SELECTION_RECT_COLOR: Color = { r: 0, g: 0, b: 0, a: 1 };
    private readonly SELECTION_FILL_COLOR: Color = { r: 255, g: 255, b: 255, a: 0.35 };

    protected readonly WIDTH: number = 220;
    protected readonly HEIGHT: number = 20;

    private readonly GRADIENT_COLORS: Color[] = [
        this.COLOR_RED,
        this.COLOR_YELLOW,
        this.COLOR_GREEN,
        this.COLOR_TURQUOISE,
        this.COLOR_BLUE,
        this.COLOR_MAGENTA,
        this.COLOR_RED,
    ];

    @Output() colorHueChange: EventEmitter<number> = new EventEmitter<number>();

    color: Color = { r: 255, g: 0, b: 0, a: 1 };

    constructor(protected colorHandlerService: ColorHandlerService) {
        super(colorHandlerService);
    }

    protected elementChanged(size: Vec2): void {
        this.reset();
        this.drawColorGradient();
        size.x = this.getWidth(size.x);
        this.color = this.getColorAtPosition({ x: size.x, y: this.yPosition });
        const hue = (size.x / this.canvasWidth) * ColorHandlerService.CIRCLE_DEGREES;
        this.colorHueChange.emit(hue);
        this.drawSelection(size.x);
    }

    init(): void {
        this.reset();
        this.drawColorGradient();
        const hue = this.colorHandlerService.colorHue;

        const huePercent = hue / this.CIRCLE_DEGREES;
        const xValue = this.getWidth(this.canvasWidth * huePercent);
        this.color = this.getColorAtPosition({ x: xValue, y: this.yPosition });
        this.drawSelection(xValue);
    }

    protected drawSelection(width: number): void {
        this.ctx.beginPath();
        this.ctx.lineWidth = this.SELECTION_LINE_WIDTH;
        this.ctx.strokeStyle = ColorService.getColorRGBA(this.SELECTION_RECT_COLOR);
        this.ctx.fillStyle = ColorService.getColorRGBA(this.SELECTION_FILL_COLOR);
        this.ctx.rect(width, 0, this.SELECTION_WIDTH, this.canvasHeight);
        this.ctx.stroke();
        this.ctx.fill();
    }

    protected drawColorGradient(): void {
        this.ctx.beginPath();
        this.ctx.rect(0, 0, this.canvasWidth, this.canvasHeight);
        this.ctx.fillStyle = this.getGradient(this.canvasWidth);
        this.ctx.fill();
    }

    private getGradient(width: number): CanvasGradient {
        const gradient = this.ctx.createLinearGradient(0, 0, width, 0);
        for (let i = 0; i < this.GRADIENT_COLORS.length; i++) {
            gradient.addColorStop(i * (1 / (this.GRADIENT_COLORS.length - 1)), ColorService.getColorRGBA(this.GRADIENT_COLORS[i]));
        }
        return gradient;
    }

    private getWidth(width: number): number {
        if (width > this.canvasWidth - this.WIDTH_RIGHT_THRESHOLD) {
            width = this.canvasWidth - this.WIDTH_RIGHT_THRESHOLD;
        } else if (width < this.WIDTH_LEFT_THRESHOLD) {
            width = this.WIDTH_LEFT_THRESHOLD;
        }
        return width;
    }

    private get yPosition(): number {
        return this.canvasHeight / 2;
    }
}
