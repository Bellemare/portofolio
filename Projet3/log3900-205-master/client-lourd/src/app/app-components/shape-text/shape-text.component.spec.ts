import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShapeTextComponent } from './shape-text.component';

describe('ShapeTextComponent', () => {
  let component: ShapeTextComponent;
  let fixture: ComponentFixture<ShapeTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShapeTextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShapeTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
