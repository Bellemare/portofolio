import { Component, ElementRef, ViewChild } from '@angular/core';
import { DrawingCanvasHelper } from '@app/classes/drawing-canvas-helper';
import { DrawingEvent } from '@app/classes/drawing/drawing-event';
import { DrawingShape } from '@app/classes/drawing/drawing-shape';
import { CanvasSizeService } from '@app/services/canvas-size/canvas-size.service';

@Component({
  selector: 'app-shape-text',
  templateUrl: './shape-text.component.html',
  styleUrls: ['./shape-text.component.scss']
})
export class ShapeTextComponent {
  private readonly keyEventsHandlers: Map<string, (start: number, end :number) => void> = new Map([
    [ "Backspace", this.handleBackspace.bind(this) ],
    [ "Enter", this.handleNewLine.bind(this) ]
  ]);

  _top: number = 0;
  _left: number = 0;
  _width: number = 0;
  _height: number = 0;

  hidden: boolean = false;

  shapeElement: DrawingShape;
  event: DrawingEvent;

  text: string = "";

  private currentLines: string[] = [];

  onChange: () => void;
  onExit: () => void;
  @ViewChild("input") private input: ElementRef<HTMLTextAreaElement>;

  init(): void {
    this.focusInput();
  }

  exit(): void {
    if (this.onExit != undefined)
      this.onExit();
  }

  private focusInput(): void {
    if (this.input == undefined) {
      setTimeout(() => {
        this.focusInput();
      }, 100);
    } else {
      this.input.nativeElement.focus();
    }
  }

  setText(): void {
    if (this.input == undefined) {
      return;
    }
    const ctx = DrawingCanvasHelper.previewCanvas.getContext("2d") as CanvasRenderingContext2D;
    this.currentLines = this.shapeElement.generateTextLines(ctx);
    this.input.nativeElement.value = this.currentLines.join("\n");
  }
  
  handleKeypress(event: KeyboardEvent): void {
    let inputStart = this.input.nativeElement.selectionStart;
    let inputEnd = this.input.nativeElement.selectionEnd;
    if (event.key.length == 1) {
      event.preventDefault();
      this.handleNewChar(event.key, inputStart, inputEnd);
    } else {
      const handler = this.keyEventsHandlers.get(event.key);
      if (handler != undefined) {
        event.preventDefault();
        handler(inputStart, inputEnd);
      } 
    }
    if (this.onChange != undefined) {
      this.onChange();
    }
  }

  private handleNewChar(char: string, inputStart: number, inputEnd: number): void {
    const start = this.getRealPosition(inputStart);
    this.shapeElement.text?.insertText(char, start);
    inputStart++;
    inputEnd++;
    this.setText();
    if (this.input.nativeElement.value[inputStart - 1] == "\n") {
      inputStart++;
      inputEnd++;
    }
    this.input.nativeElement.setSelectionRange(inputStart, inputEnd);
  }

  private handleNewLine(inputStart: number, inputEnd: number): void {
    const start = this.getRealPosition(inputStart);
    this.shapeElement.text?.insertText("\n", start);
    this.setText();
    inputStart++;
    inputEnd++;
    this.input.nativeElement.setSelectionRange(inputStart, inputEnd);
  }

  private handleBackspace(inputStart: number, inputEnd: number): void {
    const start = this.getRealPosition(inputStart);
    const end = this.getRealPosition(inputEnd);
    this.shapeElement.text?.remove(start, end);
    this.setText();
    inputEnd = inputStart;
    this.input.nativeElement.setSelectionRange(inputStart, inputEnd);
  }

  private getRealPosition(position: number): number {
    let total = 0;
    let index = 0;
    let offset = 0;
    while (total < position) {
      const line = this.currentLines[index++];
      if (line == undefined || line.length == 0) {
        total += 1;
        continue;
      }
      const newLinesChar = line.substring(0, position - total).match(/\n/g);
      offset += newLinesChar == undefined ? 0:newLinesChar.length;
      total += line.length;
    }

    return position - offset;
  }

  get top(): number {
    return this._top;
  }

  get left(): number {
    return this._left;
  }

  get width(): number {
    return Math.min(this._width, document.body.scrollWidth - this._left);
  }

  get height(): number {
    return Math.min(CanvasSizeService.HEIGHT, document.body.scrollHeight - this._top - 10);
  }

  get offset(): number {
    return this.event == undefined ? 0:this.event.attributes.line_width;
  }

  get padding(): number {
    return this.shapeElement == undefined ? 0:this.shapeElement.TEXT_PADDING;
  }
}
