import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MaterialModule } from '@app/material/material.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ColorPaletteComponent } from './color/color-palette/color-palette.component';
import { ColorPickerComponent } from './color/color-picker/color-picker.component';
import { ColorSliderComponent } from './color/color-slider/color-slider.component';
import { NotificationComponent } from './notification/notification.component';
import { PrevisualizationRectangleComponent } from './previsualization-rectangle/previsualization-rectangle.component';
import { SelectComponent } from './tool-attributes/select/select.component';
import { SlideToggleComponent } from './tool-attributes/slide-toggle/slide-toggle.component';
import { SliderComponent } from './tool-attributes/slider/slider.component';
import { ToolAttributeComponent } from './tool-attributes/tool-attribute/tool-attribute.component';
import { ToolAttributesContainerComponent } from './tool-attributes/tool-attributes-container/tool-attributes-container.component';
import { ToolAttributesDirective } from './tool-attributes/tool-attributes.directive';
import { EllipseAttributesComponent } from './tool-components/ellipse-attributes/ellipse-attributes.component';
import { PencilAttributesComponent } from './tool-components/pencil-attributes/pencil-attributes.component';
import { RectAttributesComponent } from './tool-components/rect-attributes/rect-attributes.component';
import { SelectionAttributesComponent } from './tool-components/selection-attributes/selection-attributes.component';
import { TextAttributesComponent } from './tool-components/text-attributes/text-attributes.component';
import { ToolAttributesComponent } from './tool-components/tool-attributes/tool-attributes.component';
import { ToolComponent } from './tool/tool.component';
import { ActionsMenuComponent } from './actions-menu/actions-menu.component';
import { ChatRoomComponent } from './chat/chat-room/chat-room.component';
import { ChatMenuComponent } from './chat/chat-menu/chat-menu.component';
import { AlbumDetailComponent } from './album/album-detail/album-detail.component';
import { RouterModule } from '@angular/router';
import { AlbumCreateComponent } from './album/album-create/album-create.component';
import { ConfirmComponent } from './confirm/confirm.component';
import { AlbumEditComponent } from './album/album-edit/album-edit.component';
import { AlbumJoinComponent } from './album/album-join/album-join.component';
import { AppNotificationsComponent } from './app-notifications/app-notifications.component';
import { DrawingCardComponent } from './album/drawing-card/drawing-card.component';
import { DrawingCreateComponent } from './album/drawing-create/drawing-create.component';
import { DrawingEditComponent } from './album/drawing-edit/drawing-edit.component';
import { DrawingViewComponent } from './album/drawing-view/drawing-view.component';
import { ContactListComponent } from './contact/contact-list/contact-list.component';
import { AvatarComponent } from './contact/avatar/avatar.component';
import { ContactAddComponent } from './contact/contact-add/contact-add.component';
import { ChatListComponent } from './chat/chat-list/chat-list.component';
import { ChatComponent } from './chat/chat/chat.component';
import { DrawingChatComponent } from './chat/drawing-chat/drawing-chat.component';
import { AvatarEditComponent } from './contact/avatar-edit/avatar-edit.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { LoadingComponent } from './loading/loading.component';
import { LayersComponent } from './layers/layers.component';
import { LayerComponent } from './layer/layer.component';
import { DrawingExpositionComponent } from './album/drawing-exposition/drawing-exposition.component';
import { DrawingSetPasswordComponent } from './album/drawing-set-password/drawing-set-password.component';
import { DrawingConfirmPasswordComponent } from './album/drawing-confirm-password/drawing-confirm-password.component';
import { DrawingSmartSearchComponent } from './album/drawing-smart-search/drawing-smart-search.component';
import { AvatarSelectComponent } from './contact/avatar-select/avatar-select.component';
import { ShapeTextComponent } from './shape-text/shape-text.component';

@NgModule({
    declarations: [
        ToolComponent,
        SliderComponent,
        ToolAttributeComponent,
        ToolAttributesContainerComponent,
        ToolAttributesDirective,
        ToolAttributesComponent,
        PencilAttributesComponent,
        RectAttributesComponent,
        PrevisualizationRectangleComponent,
        ColorPaletteComponent,
        ColorSliderComponent,
        ColorPickerComponent,
        EllipseAttributesComponent,
        PrevisualizationRectangleComponent,
        SelectComponent,
        NotificationComponent,
        SelectionAttributesComponent,
        TextAttributesComponent,
        SlideToggleComponent,
        ActionsMenuComponent,
        ChatRoomComponent,
        ChatMenuComponent,
        AlbumDetailComponent,
        AlbumCreateComponent,
        ConfirmComponent,
        AlbumEditComponent,
        AlbumJoinComponent,
        AppNotificationsComponent,
        DrawingCardComponent,
        DrawingCreateComponent,
        DrawingEditComponent,
        DrawingViewComponent,
        ContactListComponent,
        AvatarComponent,
        ContactAddComponent,
        ChatListComponent,
        ChatComponent,
        DrawingChatComponent,
        AvatarEditComponent,
        LoadingComponent,
        LayersComponent,
        LayerComponent,
        DrawingExpositionComponent,
        DrawingSetPasswordComponent,
        DrawingSmartSearchComponent,
        AvatarSelectComponent,
        ShapeTextComponent,
        DrawingConfirmPasswordComponent,
    ],
    imports: [RouterModule, CommonModule, MaterialModule, FontAwesomeModule, MatCheckboxModule],
    exports: [
        ToolComponent,
        PrevisualizationRectangleComponent,
        NotificationComponent,
        ActionsMenuComponent,
        AlbumDetailComponent,
        AlbumCreateComponent,
        AlbumEditComponent,
        AlbumJoinComponent,
        DrawingCardComponent,
        DrawingCreateComponent,
        ConfirmComponent,
        DrawingEditComponent,
        DrawingViewComponent,
        AvatarComponent,
        AvatarEditComponent,
        ChatListComponent,
        ChatComponent,
        DrawingChatComponent,
        LoadingComponent,
        LayersComponent,
        DrawingExpositionComponent,
        DrawingSetPasswordComponent,
        DrawingSmartSearchComponent,
        AvatarSelectComponent,
        DrawingConfirmPasswordComponent,
    ],
})
export class AppComponentsModule {}
