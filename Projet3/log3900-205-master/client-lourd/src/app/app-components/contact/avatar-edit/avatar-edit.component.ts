import { Component, EventEmitter, Input, Output } from '@angular/core';
import { AvatarComponent } from '../avatar/avatar.component';

@Component({
  selector: 'app-avatar-edit',
  templateUrl: './avatar-edit.component.html',
  styleUrls: ['./avatar-edit.component.scss']
})
export class AvatarEditComponent extends AvatarComponent {
  readonly IMG_WIDTH: number = 200;
  readonly IMG_HEIGHT: number = 200;

  @Output() onAvatarSelect: EventEmitter<void> = new EventEmitter();
  @Input() disabled: boolean = false;
}
