import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ImgViewerHelper } from '@app/classes/img-viewer-helper';
import { UserApiService } from '@app/services/api/user-api.service';
import { NotificationService } from '@app/services/notification/notification.service';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';
import { faUpload } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-avatar-select',
  templateUrl: './avatar-select.component.html',
  styleUrls: ['./avatar-select.component.scss']
})
export class AvatarSelectComponent implements OnInit {
  readonly importIcon: IconDefinition = faUpload;

  @Output() onSelectAvatar: EventEmitter<string> = new EventEmitter();
  @Output() onCancel: EventEmitter<void> = new EventEmitter();

  avatars: string[] = [];

  constructor(private notificationService: NotificationService, private userService: UserApiService) { }

  ngOnInit(): void {
    this.userService.getDefaultAvatars().subscribe((avatars: string[]) => {
      this.avatars = avatars;
      this.loadAvatars();
    });
  }

  cancel(): void {
    this.onCancel.emit();
  }

  private loadAvatars(): void {
    if (this.avatars.length == 0) {
      return;
    }

    if (document.getElementById(`avatar-canvas-0`) == null) {
      setTimeout(() => {
        this.loadAvatars();
      }, 200);
    } else {
      this.avatars.forEach((avatar: string, i: number) => {
        const canvas = document.getElementById(`avatar-canvas-${i}`);
        if (canvas != undefined) {
          this.setCanvasData(canvas as HTMLCanvasElement, avatar);
        }
      });
    }
  }

  private setCanvasData(canvas: HTMLCanvasElement, imgSrc: string): void {
    if (canvas == undefined)
      return;

    const img = new Image();
    img.onload = () => ImgViewerHelper.loadImageData(canvas, img);
    img.src = imgSrc;
  }

  setAvatar(avatar: string): void {
    this.onSelectAvatar.emit(avatar);
  }
  
  onAvatarChange(files: FileList): void {
    if (files.length > 0) {
      const img = files.item(0);
      if (img?.type != "image/png" && img?.type != "image/jpeg") {
        this.notificationService.setNotification({
          title: "Alerte",
          code: 400,
          message: "L'avatar doit être un JPEG ou un PNG"
        });
        return;
      }

      const reader = new FileReader();
      reader.readAsDataURL(files.item(0) as File);
      reader.onload = () => {
        this.setAvatar(reader.result as string);
      }
    }
  }
}
