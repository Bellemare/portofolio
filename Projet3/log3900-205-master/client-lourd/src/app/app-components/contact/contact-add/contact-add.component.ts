import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '@app/classes/api/user';
import { SoundType } from '@app/classes/sound-types';
import { ContactApiService } from '@app/services/api/contact-api-service';
import { AudioService } from '@app/services/audio/audio.service';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-contact-add',
  templateUrl: './contact-add.component.html',
  styleUrls: ['./contact-add.component.scss']
})
export class ContactAddComponent implements OnInit {
  readonly addIcon: IconDefinition = faPlusCircle;

  users: User[] = [];

  constructor(private contactService: ContactApiService, private router: Router, private audioService: AudioService) { }

  ngOnInit(): void {
    this.getUserList();
  }

  private getUserList(): void {
    this.contactService.getUserList().subscribe((users: User[]) => this.users = users);
  }

  addContact(user: User): void {
    this.contactService.addContact(user.id).subscribe(() => {
      this.audioService.playSound(SoundType.NewContact);
      this.removeUserFromList(user);
    }, () => this.removeUserFromList(user));
  }

  navigateToContact(user: User): void {
    this.router.navigateByUrl(`/contact/${user.id}`);
  }

  private removeUserFromList(user: User): void {
    const index = this.users.findIndex((u: User) => u.id == user.id);
    if (index != -1) {
      this.users.splice(index, 1);
    }
  }
}
