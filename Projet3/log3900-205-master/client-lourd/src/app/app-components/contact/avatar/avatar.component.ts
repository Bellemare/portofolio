import { AfterViewInit, Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { ImgViewerHelper } from '@app/classes/img-viewer-helper';

@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss']
})
export class AvatarComponent implements AfterViewInit {
  readonly IMG_WIDTH: number = 40;
  readonly IMG_HEIGHT: number = 40;
  @Output()  onClick: EventEmitter<void> = new EventEmitter();
  @Input("imgSrc") set _imgSrc(src: string) {
    if (src == null) {
      src = "";
    }
    this.imgSrc = src;

    this.onSrcChange();
  }
  imgSrc: string = "";

  @ViewChild('imgCanvas') protected imgCanvas: ElementRef<HTMLCanvasElement>;
  
  protected ctx: CanvasRenderingContext2D;

  protected onSrcChange(): void {
    if (this.imgCanvas == undefined)
      return;
      
    const img = new Image();
    img.onload = () => ImgViewerHelper.loadImageData(this.imgCanvas.nativeElement, img);
    img.src = this.imgSrc;
  }

  ngAfterViewInit(): void {
    this.ctx = this.imgCanvas.nativeElement.getContext("2d") as CanvasRenderingContext2D;
    this.onSrcChange();
  }
}
