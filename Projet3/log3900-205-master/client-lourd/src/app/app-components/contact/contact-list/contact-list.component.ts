import { Component, Input, OnInit } from '@angular/core';
import { ContactApiService } from '@app/services/api/contact-api-service';
import { Contact } from '@app/classes/api/contact';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';
import { faPlus, faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { NavigationEnd, Router } from '@angular/router';
import { RoomService } from '@app/services/api/room.service';
import { SocketClient } from '@app/classes/socket/socket-client';
import { PayloadTypes } from '@app/classes/socket/payload-types';
import { AlbumApiService } from '@app/services/api/album-api.service';
import { Album } from '@app/classes/api/album';
import { ContactCollaborationSession } from '@app/classes/api/contact-collaborative-session';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss']
})
export class ContactListComponent implements OnInit {
  readonly deleteIcon: IconDefinition = faTimesCircle;
  readonly addContactIcon: IconDefinition = faPlus;

  @Input() set open(show: boolean) {
    if (show) {
      this.hideAll();
      this.getContacts();
      this.listenToContactChange();
      this.getMemberAlbums();
    } else {
      this.removeListeners();
    }
  }

  contacts: Contact[] = [];

  isAdding: boolean = false;
  confirming: boolean = false;
  isSettingPassword: boolean = false;
  confirmText: string = "";
  selectedContact: Contact

  albums: Album[] = [];
  availableAlbums: Album[] = [];

  selectedDrawingId: number = 0;

  private appClient: SocketClient;
  private changesListener: number;
  private albumChangesListener: number;

  private currentDrawingRoom: number = 0;

  constructor(
    private contactService: ContactApiService, 
    private router: Router, 
    private roomService: RoomService,
    private albumService: AlbumApiService,
  ) { }

  private getContacts(): void {
    this.contactService.getContacts().subscribe((contacts: Contact[]) => this.contacts = contacts);
  }
  
  ngOnInit(): void {
      this.router.events.subscribe((evt) => {
        if (evt instanceof NavigationEnd) {
          const editorRegex = new RegExp("editor");
          if (editorRegex.test(this.router.url)) {
            const parts = this.router.url.split("/");
            this.currentDrawingRoom = Number(parts[parts.length - 1]);
          } else {
            this.currentDrawingRoom = 0;
          }
        }
      });
  }

  navigateToContact(contact: Contact): void {
    this.router.navigateByUrl(`/contact/${contact.contact_id}`);
  }

  private listenToContactChange(): void {
    this.roomService.getAppClient().then((client: SocketClient) => {
      this.appClient = client;
      this.changesListener = this.appClient.onMessage(PayloadTypes.CLIENT_STATE_EVENT, this.clientStateChange.bind(this));
      this.albumChangesListener = this.appClient.onMessage(PayloadTypes.ALBUM_CHANGE, this.getMemberAlbums.bind(this));
    });
  }

  private clientStateChange(): void {
    this.getContacts();
  }

  private removeListeners(): void {
    if (this.appClient == undefined) {
      return;
    }

    this.appClient.unregister(PayloadTypes.CLIENT_STATE_EVENT, this.changesListener);
    this.appClient.unregister(PayloadTypes.ALBUM_CHANGE, this.albumChangesListener);
  }

  closeAddContact(): void {
    this.getContacts();
    this.hideAll();
  }

  deleteContact(contact: Contact): void {
    this.selectedContact = contact;
    this.confirming = true;
    this.confirmText = `Voulez-vous vraiment supprimer ${contact.contact} de vos contacts?`;
  }

  hideAll(): void {
    this.confirming = false;
    this.isAdding = false;
    this.isSettingPassword = false;
  }

  onDelete(): void {
    this.hideAll();
    this.contactService.deleteContact(this.selectedContact.contact_id).subscribe(() => {
      const index = this.contacts.findIndex((c: Contact) => this.selectedContact.id == c.id);
      if (index != -1)
        this.contacts.splice(index, 1);
    });
  }

  routeToDrawing(session: ContactCollaborationSession): void {
    if (session.is_password_protected) {
      this.selectedDrawingId = session.drawing_id;
      this.isSettingPassword = true;
    } else {
      this.joinDrawing(session.drawing_id);
    }
  }

  joinDrawing(drawingId: number): void {
    this.router.navigateByUrl(`/editor/${drawingId}`);
  }

  isAlbumMember(albumId: number): boolean {
    return this.albums.findIndex((a: Album) => a.id == albumId) > -1;
  }

  isPendingJoin(albumId: number): boolean {
    return !this.isAlbumMember(albumId) && this.availableAlbums.findIndex((a: Album) => a.id == albumId) == -1;
  }

  joinAlbum(albumId: number): void {
    const index = this.availableAlbums.findIndex((a: Album) => a.id == albumId);
    if (index != -1) {
      this.albumService.requestToJoinAlbum(albumId).subscribe(() => {
        this.availableAlbums.splice(index, 1);
      });
    }
  }

  shouldShowJoin(drawingId: number): boolean {
    return this.currentDrawingRoom != drawingId;
  }

  private getMemberAlbums(): void {
    this.albumService.getUserAlbums().subscribe((albums: Album[]) => this.albums = albums);
    this.albumService.getAlbumList().subscribe((albums: Album[]) => this.availableAlbums = albums);
  }
}
