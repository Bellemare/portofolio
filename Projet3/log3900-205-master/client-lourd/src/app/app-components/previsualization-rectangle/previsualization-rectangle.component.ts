import { Component, ElementRef, Input, QueryList, Renderer2, ViewChildren } from '@angular/core';
import { DrawingCanvasHelper } from '@app/classes/drawing-canvas-helper';
import { Vec2 } from '@app/classes/drawing/vec2';
import { MouseEventWrapper } from '@app/classes/mouse-event-wrapper';
import { OutlineStyle } from '@app/classes/outline-style';
import { SelectionResizeScalers } from '@app/classes/selection/selection-resize-scalers';

@Component({
    selector: 'app-previsualization-rectangle',
    templateUrl: './previsualization-rectangle.component.html',
    styleUrls: ['./previsualization-rectangle.component.scss'],
})
export class PrevisualizationRectangleComponent {
    private readonly DEFAULT_CIRCULAR_VALUE: boolean = false;
    private readonly DEFAULT_OUTLINE_STYLE: OutlineStyle = OutlineStyle.DASHED;
    readonly SCALER_SIZE: number = 7;
    @Input("top") _top: number = 0;
    @Input("left") _left: number = 0;
    @Input("width") _width: number = 0;
    @Input("height") _height: number = 0;
    @Input() isCircular: boolean = this.DEFAULT_CIRCULAR_VALUE;
    @Input() outlineStyle: OutlineStyle = this.DEFAULT_OUTLINE_STYLE;

    onResized: (pos: Vec2) => void;
    onStartResized: (pos: SelectionResizeScalers) => void;
    onStartMove: (event: MouseEventWrapper) => void;

    private mouseMoveListener: () => void;
    private mouseUpListener: () => void;

    @ViewChildren('scaler') private scalers: QueryList<ElementRef<HTMLDivElement>>;

    eSelectionResizeScalers: typeof SelectionResizeScalers = SelectionResizeScalers;
    private startPosition: Vec2;
    isResizing: boolean = false;

    constructor(private renderer: Renderer2) {}

    startPrevisualization(
        startPosition: Vec2,
        isCircular: boolean = this.DEFAULT_CIRCULAR_VALUE,
        outlineStyle: OutlineStyle = this.DEFAULT_OUTLINE_STYLE,
    ): void {
        this._left = startPosition.x;
        this._top = startPosition.y;
        this.startPosition = startPosition;
        this.isCircular = isCircular;
        this.outlineStyle = outlineStyle;
    }

    updatePosition(position: Vec2): void {
        const topLeftCorner: Vec2 = { x: Math.min(this.startPosition.x, position.x), y: Math.min(this.startPosition.y, position.y) };
        const bottomRightCorner: Vec2 = { x: Math.max(this.startPosition.x, position.x), y: Math.max(this.startPosition.y, position.y) };
        this._width = bottomRightCorner.x - topLeftCorner.x;
        this._height = bottomRightCorner.y - topLeftCorner.y;
        this._left = topLeftCorner.x - 1;
        this._top = topLeftCorner.y - 1;
    }

    updateMovingPosition(translation: Vec2): void {
        this._left += translation.x;
        this._top += translation.y;
    }

    get outlineOutline(): OutlineStyle {
        return OutlineStyle.OUTLINE;
    }

    get scalerOffset(): Vec2 {
        return { x: this.SCALER_SIZE / 2, y: this.SCALER_SIZE / 2 };
    }

    onMouseDown(event: MouseEvent): void {
        if (this.onStartMove != undefined) {
            this.onStartMove(new MouseEventWrapper(event, DrawingCanvasHelper.baseCanvas));
        }
    }

    getScalerPosition(scalerPos: SelectionResizeScalers): Vec2 {
        if (scalerPos === SelectionResizeScalers.Center) {
            const leftScaler = this.getScalerPosition(SelectionResizeScalers.Left);
            const topScaler = this.getScalerPosition(SelectionResizeScalers.Top);
            return { x: topScaler.x, y: leftScaler.y };
        }
        const scaler = this.scalers.toArray()[scalerPos];
        const top = { x: scaler.nativeElement.offsetLeft, y: scaler.nativeElement.offsetTop };
        const center = { x: top.x + Math.floor(this.scalerOffset.x), y: top.y + Math.floor(this.scalerOffset.y) };
        return center;
    }

    onMouseDownScaler(scalerPos: SelectionResizeScalers): void {
        this.isResizing = true;
        this.mouseMoveListener = this.renderer.listen('window', 'mousemove', this.onMouseMoveScaler.bind(this));
        this.mouseUpListener = this.renderer.listen('window', 'mouseup', this.onMouseUpScaler.bind(this));
        if (this.onStartResized != undefined)
            this.onStartResized(scalerPos);
    }

    onMouseMoveScaler(event: MouseEvent): void {
        if (this.isResizing) {
            const position = { x: event.x, y: event.y };
            if (this.onResized != undefined)
                this.onResized(position);
        }
    }

    onMouseUpScaler(): void {
        this.mouseMoveListener();
        this.mouseUpListener();
        this.isResizing = false;
    }

    get top(): number {
        return this._top;
    }

    get left(): number {
        return this._left;
    }

    get width(): number {
        return Math.min(this._width, document.body.scrollWidth - this._left);
    }

    get height(): number {
        return Math.min(this._height, document.body.scrollHeight - this._top);
    }
}
