import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Vec2 } from '@app/classes/drawing/vec2';
import { MouseButton } from '@app/classes/mouse-button';
import { OutlineStyle } from '@app/classes/outline-style';
import { SelectionResizeScalers } from '@app/classes/selection/selection-resize-scalers';
import { PrevisualizationRectangleComponent } from './previsualization-rectangle.component';

// tslint:disable:no-string-literal
// tslint:disable:no-any
// tslint:disable:no-magic-numbers
describe('PrevisualizationRectangleComponent', () => {
    let component: PrevisualizationRectangleComponent;
    let fixture: ComponentFixture<PrevisualizationRectangleComponent>;

    let startPosition: Vec2;
    let endPosition: Vec2;

    beforeEach(async(() => {
        startPosition = { x: 50, y: 50 };
        endPosition = { x: 150, y: 150 };

        TestBed.configureTestingModule({
            declarations: [PrevisualizationRectangleComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PrevisualizationRectangleComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should set start position', () => {
        component.startPrevisualization(startPosition);
        expect(component.top).toEqual(startPosition.y);
        expect(component.left).toEqual(startPosition.x);
        expect(component['startPosition']).toEqual(startPosition);
    });

    it('should set width and height on update position', () => {
        component['startPosition'] = startPosition;
        component.updatePosition(endPosition);
        expect(component.height).toEqual(endPosition.y - startPosition.y);
        expect(component.width).toEqual(endPosition.x - startPosition.x);
    });

    it('updateMovingPosition should update left and top to correct values', () => {
        component.top = 0;
        component.left = 0;
        const movement: Vec2 = { x: 20, y: 40 };
        component.updateMovingPosition(movement);
        expect(component.left).toEqual(movement.x);
        expect(component.top).toEqual(movement.y);
    });

    it('should emit mouse up event on mouseup', () => {
        const event = {} as MouseEvent;
        const mouseUpSpy = spyOn(component.mouseup, 'emit');
        component.onMouseUp(event);
        expect(mouseUpSpy).toHaveBeenCalled();
    });

    it('get OutlineOutline should return OutlineStyle.OUTLINE', () => {
        expect(component.outlineOutline).toEqual(OutlineStyle.OUTLINE);
    });

    it('get scalerOffset should return the correct scaler offset', () => {
        expect(component.scalerOffset.x).toEqual(component['SCALER_SIZE'] / 2);
        expect(component.scalerOffset.y).toEqual(component['SCALER_SIZE'] / 2);
    });

    it('onMouseDownScaler should change isResizing to true', () => {
        component['isResizing'] = false;
        const spyListen = spyOn<any>(component['renderer'], 'listen');
        const spyEmit = spyOn<any>(component.previewStartResize, 'emit');
        const scaler = {
            offsetLeft: 0,
            offsetTop: 0,
        } as HTMLDivElement;
        spyOn(component['scalers'], 'toArray').and.returnValue([{ nativeElement: scaler }]);
        component.onMouseDownScaler(SelectionResizeScalers.TopLeft);

        expect(component['isResizing']).toEqual(true);
        expect(spyListen).toHaveBeenCalledTimes(2);
        expect(spyEmit).toHaveBeenCalledWith(SelectionResizeScalers.TopLeft);
    });

    it('onMouseMoveScaler should emit the correct position', () => {
        component['isResizing'] = true;
        const mouseEvent: MouseEvent = {
            x: 10,
            y: 10,
            button: MouseButton.Left,
        } as MouseEvent;
        const spyEmit = spyOn(component.previewResized, 'emit');
        component.onMouseMoveScaler(mouseEvent);
        expect(spyEmit).toHaveBeenCalledWith({ x: 10, y: 10 });
    });

    it('onMouseUpScaler should change isResizing to false', () => {
        component['isResizing'] = true;
        component.onMouseUpScaler();
        expect(component['isResizing']).toEqual(false);
    });

    it('get isCurrentlyResizing should return the condition isResizing', () => {
        expect(component.isResizing).toEqual(component['isResizing']);
    });

    it('getScalerPos should call itself twice if scalerPos is center', () => {
        const scaler = {
            offsetLeft: 435,
            offsetTop: 34,
        } as HTMLDivElement;
        spyOn(component['scalers'], 'toArray').and.returnValue([
            { nativeElement: scaler },
            { nativeElement: scaler },
            { nativeElement: scaler },
            { nativeElement: scaler },
            { nativeElement: scaler },
            { nativeElement: scaler },
            { nativeElement: scaler },
            { nativeElement: scaler },
        ]);

        const scalerPosSpy = spyOn(component, 'getScalerPosition');
        scalerPosSpy.and.callThrough();
        component.getScalerPosition(SelectionResizeScalers.Center);
        expect(scalerPosSpy).toHaveBeenCalledTimes(3);
    });
});
