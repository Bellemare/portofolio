import { EventEmitter } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { DrawingCanvasHelper } from '@app/classes/drawing-canvas-helper';
import { Vec2 } from '@app/classes/drawing/vec2';
import { MouseButton } from '@app/classes/mouse-button';
import { MouseEventWrapper } from '@app/classes/mouse-event-wrapper';
import { CanvasSizeService } from '@app/services/canvas-size/canvas-size.service';
import { Observable } from 'rxjs';

// tslint:disable:no-string-literal
// tslint:disable:no-any
describe('ResizeService', () => {
    let service: CanvasSizeService;

    let previewCanvas: HTMLCanvasElement;

    let mouseEvent: MouseEventWrapper;
    let mouseMoveSpy: jasmine.Spy<(position: Vec2) => void>;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(CanvasSizeService);

        mouseEvent = new MouseEventWrapper({ offsetX: 500, offsetY: 500, button: MouseButton.Left, buttons: 1 } as MouseEvent);

        mouseMoveSpy = spyOn(service, 'resizeMove');

        const verticalScaler = document.createElement('div') as HTMLDivElement;
        const bottomScaler = document.createElement('div') as HTMLDivElement;
        const cornerScaler = document.createElement('div') as HTMLDivElement;
        service.sideScaler = verticalScaler;
        service.bottomScaler = bottomScaler;
        service.cornerScaler = cornerScaler;
        const baseCanvas = document.createElement('canvas') as HTMLCanvasElement;
        previewCanvas = document.createElement('canvas') as HTMLCanvasElement;
        const gridCanvas = document.createElement('canvas') as HTMLCanvasElement;
        DrawingCanvasHelper.baseCanvas = baseCanvas;
        DrawingCanvasHelper.previewCanvas = previewCanvas;
        DrawingCanvasHelper.gridCanvas = gridCanvas;
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should destroy correctly', () => {
        const size = { x: 50, y: 50 };
        CanvasSizeService['currentSize'] = size;
        CanvasSizeService['previousSize'] = size;
        CanvasSizeService['newSize'] = size;
        service['ctx'] = document.createElement('canvas').getContext('2d') as CanvasRenderingContext2D;

        service.onDestroy();
        const defaultSize = { x: 0, y: 0 };
        expect(CanvasSizeService.canvasSize).toEqual(defaultSize);
        expect(CanvasSizeService.canvasSize).toEqual(defaultSize);
        expect(CanvasSizeService.canvasSize).toEqual(defaultSize);
    });

    it('should return resize event as observable', () => {
        expect(service.listenResized()).toBeInstanceOf(Observable);
    });

    it('should resizeMove on window move event', () => {
        spyOnProperty(service, 'isResizing', 'get').and.returnValue(true);
        const event = ({
            type: 'mousemove',
            target: previewCanvas,
            offsetX: 50,
            offsetY: 50,
        } as any) as Event;
        const callable = window.onmousemove as CallableFunction;
        callable(event);
        expect(mouseMoveSpy).toHaveBeenCalled();
    });

    it('should handle resize move correctly', () => {
        mouseMoveSpy.and.callThrough();
        service['resizingHorizontal'] = true;
        service['resizingVertical'] = true;
        const position = { x: 500, y: 500 };
        service.resizeMove(position);
        expect(CanvasSizeService.newCanvasSize.x).toEqual(position.x);
        expect(CanvasSizeService.newCanvasSize.y).toEqual(position.y);
    });

    it('should not do anything on resize move if not resizing', () => {
        spyOnProperty(service, 'isResizing', 'get').and.returnValue(false);
        service.resizeMove(mouseEvent.relativePosition);
        expect(service['undoRedoService'].disabled).toEqual(false);
    });

    it('should handle mouse down vertical correctly', () => {
        service['resizingVertical'] = false;
        service.onMouseDownVertical(mouseEvent);
        expect(service['resizingVertical']).toEqual(true);
        expect(mouseMoveSpy).toHaveBeenCalled();
    });

    it('should handle mouse down horizontal correctly', () => {
        service['resizingHorizontal'] = false;
        service.onMouseDownHorizontal(mouseEvent);
        expect(service['resizingHorizontal']).toEqual(true);
        expect(mouseMoveSpy).toHaveBeenCalled();
    });

    it('should handle mouse down corner correctly', () => {
        service['resizingVertical'] = false;
        service['resizingHorizontal'] = false;
        service.onMouseDownCorner(mouseEvent);
        expect(service['resizingVertical']).toEqual(true);
        expect(service['resizingHorizontal']).toEqual(true);
        expect(mouseMoveSpy).toHaveBeenCalled();
    });

    it('should confirm resize on mouse up', () => {
        const confirmSpy = spyOn(service, 'resizeConfirm');
        spyOnProperty(service, 'isResizing', 'get').and.returnValue(true);
        service.onMouseUpScaling(mouseEvent);
        expect(confirmSpy).toHaveBeenCalled();
    });

    it('should handle resize canvas correctly', () => {
        const ctx = DrawingCanvasHelper.baseCanvas.getContext('2d') as CanvasRenderingContext2D;
        const getImgSpy = spyOn(ctx, 'getImageData');
        const putImgSpy = spyOn(ctx, 'putImageData');
        const setSizeSpy = spyOn(service, 'setSizeCanvas');
        service.resizeCanvas(mouseEvent.relativePosition);
        expect(setSizeSpy).toHaveBeenCalledWith(mouseEvent.relativePosition, true);
        expect(getImgSpy).toHaveBeenCalled();
        expect(putImgSpy).toHaveBeenCalled();
    });

    it('should set correct canvas size', () => {
        const undoRedoSpy = spyOn(service['undoRedoService'], 'addResizeEvent');
        const resizedSpy = spyOn(service['resized'], 'emit');
        service.setSizeCanvas(mouseEvent.relativePosition, true);
        expect(CanvasSizeService.canvasSize.x).toEqual(mouseEvent.relativePosition.x);
        expect(CanvasSizeService.canvasSize.y).toEqual(mouseEvent.relativePosition.y);
        expect(resizedSpy).toHaveBeenCalled();
        expect(undoRedoSpy).toHaveBeenCalled();
    });

    it('should handle resize confirm correctly', () => {
        service['undoRedoService']['undoRedoDisabled'] = true;
        const resizeSpy = spyOn(service, 'resizeCanvas');
        service.resizeConfirm();
        expect(service.isResizing).toEqual(false);
        expect(service['undoRedoService'].disabled).toEqual(false);
        expect(resizeSpy).toHaveBeenCalled();
    });

    it('should handle undo/redo size event properly', async (done: DoneFn) => {
        const resizeSpy = spyOn(service, 'resizeCanvas');
        const event = new EventEmitter<Vec2>();
        const observable = event.asObservable();
        const pos = {} as Vec2;
        spyOn(service['undoRedoService'], 'listenSizeEvent').and.returnValue(observable);
        service['listenUndoRedo']();
        observable.subscribe((p: Vec2) => {
            setTimeout(() => {
                expect(resizeSpy).toHaveBeenCalledWith(pos, false);
                done();
                // tslint:disable-next-line:no-magic-numbers
            }, 100);
        });
        event.emit(pos);
    });

    it('should check if is in canvas', () => {
        CanvasSizeService['currentSize'] = { x: 100, y: 100 };

        expect(CanvasSizeService.isInCanvas({ x: -1, y: -1 })).toEqual(false);
        expect(CanvasSizeService.isInCanvas({ x: CanvasSizeService.canvasSize.x + 1, y: CanvasSizeService.canvasSize.y + 1 })).toEqual(false);
        expect(CanvasSizeService.isInCanvas({ x: CanvasSizeService.canvasSize.x / 2, y: CanvasSizeService.canvasSize.y / 2 })).toEqual(true);
    });

    it('should return previous size', () => {
        const size = { x: 50, y: 40 };
        CanvasSizeService['previousSize'] = size;
        expect(CanvasSizeService.previousCanvasSize.x).toEqual(size.x);
        expect(CanvasSizeService.previousCanvasSize.y).toEqual(size.y);
    });
});
