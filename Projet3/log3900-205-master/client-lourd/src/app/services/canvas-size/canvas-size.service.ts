import { EventEmitter, Injectable } from '@angular/core';
import { DrawingCanvasHelper } from '@app/classes/drawing-canvas-helper';
import { Vec2 } from '@app/classes/drawing/vec2';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class CanvasSizeService {
    static readonly WIDTH: number = 1200;
    static readonly HEIGHT: number = 720;

    private static currentSize: Vec2 = { x: 0, y: 0 };

    private resized: EventEmitter<void> = new EventEmitter<void>();

    constructor() {}

    static get canvasSize(): Vec2 {
        return { x: CanvasSizeService.currentSize.x, y: CanvasSizeService.currentSize.y };
    }

    static isInCanvas(position: Vec2): boolean {
        return position.x >= 0 && position.y >= 0 && position.x < CanvasSizeService.canvasSize.x && position.y < CanvasSizeService.canvasSize.y;
    }

    listenResized(): Observable<void> {
        return this.resized.asObservable();
    }

    setSizeCanvas(): void {
        CanvasSizeService.currentSize = { x: CanvasSizeService.WIDTH, y: CanvasSizeService.HEIGHT };
        DrawingCanvasHelper.baseCanvas.width = CanvasSizeService.currentSize.x;
        DrawingCanvasHelper.baseCanvas.height = CanvasSizeService.currentSize.y;
        DrawingCanvasHelper.previewCanvas.width = CanvasSizeService.currentSize.x;
        DrawingCanvasHelper.previewCanvas.height = CanvasSizeService.currentSize.y;
        DrawingCanvasHelper.collabPreviewCanvas.width = CanvasSizeService.currentSize.x;
        DrawingCanvasHelper.collabPreviewCanvas.height = CanvasSizeService.currentSize.y;
        this.resized.emit();
    }

    onDestroy(): void {
        CanvasSizeService.currentSize = { x: 0, y: 0 };
    }
}
