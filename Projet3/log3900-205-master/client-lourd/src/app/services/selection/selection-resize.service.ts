import { Injectable } from '@angular/core';
import { PrevisualizationRectangleComponent } from '@app/app-components/previsualization-rectangle/previsualization-rectangle.component';
import { Vec2 } from '@app/classes/drawing/vec2';
import { ScalerEvent } from '@app/classes/selection/scaler-event';
import { SelectionResizeScalers } from '@app/classes/selection/selection-resize-scalers';
import { SelectionScalerMirror } from '@app/classes/selection/selection-scaler-mirror';
import { Subscription } from 'rxjs';
import { SelectionEditorService } from './selection-editor.service';

@Injectable({
    providedIn: 'root',
})
export class SelectionResizeService {
    private readonly SCALER_SINGLE_X_MIRROR: SelectionScalerMirror[] = [
        [SelectionResizeScalers.Left, SelectionResizeScalers.Right],
        [SelectionResizeScalers.TopLeft, SelectionResizeScalers.TopRight],
        [SelectionResizeScalers.BottomLeft, SelectionResizeScalers.BottomRight],
    ];
    private readonly SCALER_SINGLE_Y_MIRROR: SelectionScalerMirror[] = [
        [SelectionResizeScalers.Bottom, SelectionResizeScalers.Top],
        [SelectionResizeScalers.TopLeft, SelectionResizeScalers.BottomLeft],
        [SelectionResizeScalers.TopRight, SelectionResizeScalers.BottomRight],
    ];
    private readonly SCALER_DOUBLE_MIRROR: SelectionScalerMirror[] = [
        [SelectionResizeScalers.Left, SelectionResizeScalers.Right],
        [SelectionResizeScalers.Bottom, SelectionResizeScalers.Top],
        [SelectionResizeScalers.TopLeft, SelectionResizeScalers.BottomRight],
        [SelectionResizeScalers.TopRight, SelectionResizeScalers.BottomLeft],
    ];

    private previsualizationRect: PrevisualizationRectangleComponent;

    onSizeChange: () => void;
    private subscriptions: Subscription[] = [];
    private scalerEvent: ScalerEvent;
    private scaler: SelectionResizeScalers;

    constructor(private selectionEditor: SelectionEditorService) {}

    onInit(): void {
        this.listenPreviewEvents();
    }

    onDestroy(): void {
        this.subscriptions.forEach((sub: Subscription) => sub.unsubscribe());
    }

    initResize(previsualizationRect: PrevisualizationRectangleComponent | undefined): void {
        if (previsualizationRect !== undefined) this.previsualizationRect = previsualizationRect;
    }

    private listenPreviewEvents(): void {
        this.previsualizationRect.onResized = this.previewResized.bind(this);
        this.previsualizationRect.onStartResized = this.previewStartResize.bind(this);
    }

    private getMirroredScaler(scaler: SelectionResizeScalers, scalerTuples: SelectionScalerMirror[]): SelectionResizeScalers | undefined {
        const scalerMirror = scalerTuples.find((mirror: SelectionScalerMirror) => scaler === mirror[0] || scaler === mirror[1]);

        if (scalerMirror !== undefined && (scalerMirror[0] === scaler || scalerMirror[1] === scaler)) {
            return scalerMirror[0] === scaler ? scalerMirror[1] : scalerMirror[0];
        }
        return undefined;
    }

    private getEventScaler(scaler: SelectionResizeScalers): SelectionResizeScalers {
        let mirroredScaler;
        if (this.selectionEditor.currentSize.x <= 0 && this.selectionEditor.currentSize.y <= 0) {
            mirroredScaler = this.getMirroredScaler(scaler, this.SCALER_DOUBLE_MIRROR);
        } else if (this.selectionEditor.currentSize.x <= 0) {
            mirroredScaler = this.getMirroredScaler(scaler, this.SCALER_SINGLE_X_MIRROR);
        } else if (this.selectionEditor.currentSize.y <= 0) {
            mirroredScaler = this.getMirroredScaler(scaler, this.SCALER_SINGLE_Y_MIRROR);
        }

        if (mirroredScaler !== undefined) return mirroredScaler;
        return scaler;
    }

    private previewStartResize(scaler: SelectionResizeScalers): void {
        this.scaler = this.getEventScaler(scaler);
    }

    private previewResized(position: Vec2): void {
        this.scalerEvent = { mousePosition: position, scalerPosition: this.scaler };
        this.updatePreview();
    }

    private updatePreview(): void {
        let startPosition: Vec2 = this.processResizeStartPosition(this.scalerEvent);
        let endPosition: Vec2 = this.processResizeEndPosition(this.scalerEvent);
        this.sizeChanged(startPosition, endPosition);
    }

    private processResizeStartPosition(event: ScalerEvent): Vec2 {
        const startPosition: Vec2 = this.selectionEditor.currentPosition;
        switch (event.scalerPosition) {
            case SelectionResizeScalers.Left:
            case SelectionResizeScalers.BottomLeft:
                startPosition.x = event.mousePosition.x;
                break;
            case SelectionResizeScalers.TopRight:
            case SelectionResizeScalers.Top:
                startPosition.y = event.mousePosition.y;
                break;
            case SelectionResizeScalers.TopLeft:
                startPosition.x = event.mousePosition.x;
                startPosition.y = event.mousePosition.y;
                break;
        }
        return startPosition;
    }

    private processResizeEndPosition(event: ScalerEvent): Vec2 {
        const endPosition: Vec2 = {
            x: this.selectionEditor.currentPosition.x + this.selectionEditor.currentSize.x,
            y: this.selectionEditor.currentPosition.y + this.selectionEditor.currentSize.y,
        };
        switch (event.scalerPosition) {
            case SelectionResizeScalers.TopRight:
            case SelectionResizeScalers.Right:
                endPosition.x = event.mousePosition.x;
                break;
            case SelectionResizeScalers.BottomLeft:
            case SelectionResizeScalers.Bottom:
                endPosition.y = event.mousePosition.y;
                break;
            case SelectionResizeScalers.BottomRight:
                endPosition.x = event.mousePosition.x;
                endPosition.y = event.mousePosition.y;
                break;
        }
        return endPosition;
    }

    private sizeChanged(positionStart: Vec2, positionEnd: Vec2): void {
        this.selectionEditor.setRectFromPositions(positionStart, positionEnd);
        if (this.onSizeChange != undefined)
            this.onSizeChange();
    }
}
