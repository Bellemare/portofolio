import { Renderer2 } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { PrevisualizationRectangleComponent } from '@app/app-components/previsualization-rectangle/previsualization-rectangle.component';
import { ScalerEvent } from '@app/classes/selection/scaler-event';
import { SelectionResizeScalers } from '@app/classes/selection/selection-resize-scalers';
import { SelectionResizeService } from './selection-resize.service';

// tslint:disable:no-string-literal
// tslint:disable:no-any
// tslint:disable:prefer-const
// tslint:disable:no-magic-numbers
describe('SelectionResizeService', () => {
    let service: SelectionResizeService;
    let scaler: SelectionResizeScalers;

    let renderer: Renderer2;
    let previewRect: PrevisualizationRectangleComponent;
    let scalerEvent: ScalerEvent;
    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(SelectionResizeService);
        previewRect = new PrevisualizationRectangleComponent(renderer);
        scalerEvent = { mousePosition: { x: 100, y: 100 }, scalerPosition: SelectionResizeScalers.BottomRight };
        scaler = SelectionResizeScalers.TopLeft;
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('onInit should call previewEvents and registerKeybinds', () => {
        const listenSpy = spyOn<any>(service, 'listenPreviewEvents');
        const registerSpy = spyOn<any>(service, 'registerKeyBindings');
        service.onInit();
        expect(listenSpy).toHaveBeenCalled();
        expect(registerSpy).toHaveBeenCalled();
    });

    it('onDestroy should unsubscribe and unregisterKeybinds', () => {
        const subscriptionSpy = spyOn(service['subscriptions'], 'forEach');
        const unregisterSpy = spyOn<any>(service, 'unregisterKeyBindings');
        service.onDestroy();
        expect(subscriptionSpy).toHaveBeenCalled();
        expect(unregisterSpy).toHaveBeenCalled();
    });

    it('initResize should set previewRect', () => {
        service.initResize(undefined);
        expect(service['previsualizationRect']).toEqual(service['previsualizationRect']);
        service.initResize(previewRect);
        expect(service['previsualizationRect']).toEqual(previewRect);
    });

    it('listenResizeEvent should return observable', () => {
        const spy = spyOn(service['sizeChange'], 'asObservable');
        service.listenResizeEvent();
        expect(spy).toHaveBeenCalled();
    });

    it('listenPreviewEvent should push ResizeEvent and StartResizeEvent', () => {
        const startResizeSpy = spyOn<any>(service['subscriptions'], 'push');
        service['previsualizationRect'] = previewRect;
        service['listenPreviewEvents']();
        expect(startResizeSpy).toHaveBeenCalled();
    });

    it(' getEventScaler should call getMirroredScalerSpy ', () => {
        const getMirroredScalerSpy = spyOn<any>(service, 'getMirroredScaler').and.callThrough();
        service['getEventScaler'](service['scaler']);
        expect(getMirroredScalerSpy).toHaveBeenCalled();
    });

    it('getEventScaler should return correct mirrored scaler', () => {
        service['selectionEditor']['size'] = { x: -10, y: -10 };
        expect(service['getEventScaler'](SelectionResizeScalers.TopRight)).toEqual(SelectionResizeScalers.BottomLeft);
        service['selectionEditor']['size'] = { x: -10, y: 10 };
        expect(service['getEventScaler'](SelectionResizeScalers.Left)).toEqual(SelectionResizeScalers.Right);
        service['selectionEditor']['size'] = { x: 10, y: -10 };
        expect(service['getEventScaler'](SelectionResizeScalers.Top)).toEqual(SelectionResizeScalers.Bottom);
    });

    it(' previewStartResize should set scaler ', () => {
        const getEventScaler = spyOn<any>(service, 'getEventScaler').and.callThrough();
        service['previewStartResize'](scaler);
        expect(getEventScaler).toHaveBeenCalledWith(scaler);
    });

    it('previewResized should call updatePreview', () => {
        const updatePreviewSpy = spyOn<any>(service, 'updatePreview');
        service['previewResized']({ x: 5, y: 5 });
        expect(updatePreviewSpy).toHaveBeenCalled();
    });

    it('setSquared with true should change shiftIsPressed and call updatePreview', () => {
        service['previsualizationRect'] = new PrevisualizationRectangleComponent(renderer);
        service['shiftIsPressed'] = false;
        service['setSquared'](true);
        expect(service['shiftIsPressed']).toEqual(true);
    });

    it('isCorner should return true with a corner scaler and false with a side scaler', () => {
        expect(service['isCorner'](SelectionResizeScalers.TopRight)).toEqual(true);
        expect(service['isCorner'](SelectionResizeScalers.Bottom)).toEqual(false);
    });

    it('updatePreview should call processResizeStartPosition, processResizeEndPosition and sizeChanged', () => {
        const processStartPosSpy = spyOn<any>(service, 'processResizeStartPosition');
        const processEndPosSpy = spyOn<any>(service, 'processResizeEndPosition');
        const sizeChangedSpy = spyOn<any>(service, 'sizeChanged').and.callFake(() => {
            return;
        });
        service['updatePreview'](scalerEvent);
        expect(processStartPosSpy).toHaveBeenCalled();
        expect(processEndPosSpy).toHaveBeenCalled();
        expect(sizeChangedSpy).toHaveBeenCalled();
    });

    it('updatePreview with shiftPressed and corner scaler should call clampResizeStartPosition and clampResizeEndPosition', () => {
        service['shiftIsPressed'] = true;
        service['scalerEvent'] = { mousePosition: { x: 50, y: 50 }, scalerPosition: SelectionResizeScalers.BottomRight };
        const clampStartPosSpy = spyOn<any>(service, 'clampResizeStartPosition');
        const clampEndPosSpy = spyOn<any>(service, 'clampResizeEndPosition');
        spyOn<any>(service, 'sizeChanged').and.callFake(() => {
            return;
        });
        service['updatePreview'](scalerEvent);
        expect(clampStartPosSpy).toHaveBeenCalled();
        expect(clampEndPosSpy).toHaveBeenCalled();
    });

    it('compareEndSize should return true with first endSize smaller in absolute size than second endSize', () => {
        expect(service['compareEndSize'](22, 48)).toEqual(true);
        expect(service['compareEndSize'](-12, 10)).toEqual(false);
    });

    it('clampResizeStartPosition should return correct clamped start position', () => {
        expect(service['clampResizeStartPosition'](scalerEvent, { x: 50, y: 50 }, { x: 13, y: 79 })).toEqual({ x: 50, y: 50 });
        scalerEvent.scalerPosition = SelectionResizeScalers.TopLeft;
        expect(service['clampResizeStartPosition'](scalerEvent, { x: 100, y: 100 }, { x: 150, y: 200 })).toEqual({ x: 100, y: 150 });
        expect(service['clampResizeStartPosition'](scalerEvent, { x: 100, y: 100 }, { x: 200, y: 150 })).toEqual({ x: 150, y: 100 });
        scalerEvent.scalerPosition = SelectionResizeScalers.TopRight;
        expect(service['clampResizeStartPosition'](scalerEvent, { x: 100, y: 100 }, { x: 150, y: 200 })).toEqual({ x: 100, y: 150 });
        scalerEvent.scalerPosition = SelectionResizeScalers.BottomLeft;
        expect(service['clampResizeStartPosition'](scalerEvent, { x: 100, y: 100 }, { x: 200, y: 150 })).toEqual({ x: 150, y: 100 });
        spyOn<any>(service, 'compareEndSize').and.returnValue(false);
        expect(service['clampResizeStartPosition'](scalerEvent, { x: 100, y: 100 }, { x: 150, y: 200 })).toEqual({ x: 100, y: 100 });
        scalerEvent.scalerPosition = SelectionResizeScalers.TopRight;
        expect(service['clampResizeStartPosition'](scalerEvent, { x: 100, y: 100 }, { x: 150, y: 200 })).toEqual({ x: 100, y: 100 });
    });

    it('clampResizeEndPosition should return correct clamped position', () => {
        expect(service['clampResizeEndPosition'](scalerEvent, { x: 20, y: 50 }, { x: 100, y: 100 })).toEqual({ x: 70, y: 100 });
        expect(service['clampResizeEndPosition'](scalerEvent, { x: 50, y: 20 }, { x: 100, y: 100 })).toEqual({ x: 100, y: 70 });
        scalerEvent.scalerPosition = SelectionResizeScalers.TopLeft;
        expect(service['clampResizeEndPosition'](scalerEvent, { x: 23, y: 37 }, { x: 100, y: 100 })).toEqual({ x: 100, y: 100 });
        scalerEvent.scalerPosition = SelectionResizeScalers.TopRight;
        expect(service['clampResizeEndPosition'](scalerEvent, { x: 20, y: 50 }, { x: 100, y: 100 })).toEqual({ x: 70, y: 100 });
        scalerEvent.scalerPosition = SelectionResizeScalers.BottomLeft;
        expect(service['clampResizeEndPosition'](scalerEvent, { x: 50, y: 20 }, { x: 100, y: 100 })).toEqual({ x: 100, y: 70 });
        spyOn<any>(service, 'compareEndSize').and.returnValue(false);
        expect(service['clampResizeEndPosition'](scalerEvent, { x: 100, y: 100 }, { x: 150, y: 200 })).toEqual({ x: 150, y: 200 });
        scalerEvent.scalerPosition = SelectionResizeScalers.TopRight;
        expect(service['clampResizeEndPosition'](scalerEvent, { x: 100, y: 100 }, { x: 150, y: 200 })).toEqual({ x: 150, y: 200 });
    });

    it('processResizeStartPosition should return correct start position ', () => {
        service['selectionEditor']['position'] = { x: 50, y: 50 };
        service['selectionEditor']['size'] = { x: 100, y: 100 };
        expect(service['processResizeStartPosition'](scalerEvent)).toEqual({ x: 50, y: 50 });
        scalerEvent.scalerPosition = SelectionResizeScalers.TopLeft;
        expect(service['processResizeStartPosition'](scalerEvent)).toEqual({ x: 100, y: 100 });
        scalerEvent.scalerPosition = SelectionResizeScalers.TopRight;
        expect(service['processResizeStartPosition'](scalerEvent)).toEqual({ x: 50, y: 100 });
        scalerEvent.scalerPosition = SelectionResizeScalers.BottomLeft;
        expect(service['processResizeStartPosition'](scalerEvent)).toEqual({ x: 100, y: 50 });
    });

    it('processResizeEndPosition should return correct end position ', () => {
        service['selectionEditor']['position'] = { x: 50, y: 50 };
        service['selectionEditor']['size'] = { x: 100, y: 100 };
        expect(service['processResizeEndPosition'](scalerEvent)).toEqual({ x: 100, y: 100 });
        scalerEvent.scalerPosition = SelectionResizeScalers.TopLeft;
        expect(service['processResizeEndPosition'](scalerEvent)).toEqual({ x: 150, y: 150 });
        scalerEvent.scalerPosition = SelectionResizeScalers.TopRight;
        expect(service['processResizeEndPosition'](scalerEvent)).toEqual({ x: 100, y: 150 });
        scalerEvent.scalerPosition = SelectionResizeScalers.BottomLeft;
        expect(service['processResizeEndPosition'](scalerEvent)).toEqual({ x: 150, y: 100 });
    });

    it('sizeChanged should call setRectFromPositions from selectionEditor and emit from sizeChange', () => {
        const setRectFromPositionsSpy = spyOn(service['selectionEditor'], 'setRectFromPositions');
        const emitSpy = spyOn(service['sizeChange'], 'emit');
        service['sizeChanged']({ x: 10, y: 10 }, { x: 50, y: 50 });
        expect(setRectFromPositionsSpy).toHaveBeenCalled();
        expect(emitSpy).toHaveBeenCalled();
    });
});
