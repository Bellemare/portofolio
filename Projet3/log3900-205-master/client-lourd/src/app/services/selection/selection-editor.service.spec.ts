import { EventEmitter, Renderer2 } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { PrevisualizationRectangleComponent } from '@app/app-components/previsualization-rectangle/previsualization-rectangle.component';
import { DrawingCanvasHelper } from '@app/classes/drawing-canvas-helper';
import { DrawingSelection } from '@app/classes/drawing/drawing-selection';
import { MouseButton } from '@app/classes/mouse-button';
import { MouseEventWrapper } from '@app/classes/mouse-event-wrapper';
import { OutlineStyle } from '@app/classes/outline-style';
import { ArrowKeys } from '@app/classes/selection/arrow-keys';
import { AppComponent } from '@app/components/app/app.component';
import { ResizeService } from '@app/services/resize/resize.service';
import { interval } from 'rxjs';
import { takeWhile } from 'rxjs/operators';
import { SelectionEditorService } from './selection-editor.service';
// tslint:disable:no-any
// tslint:disable:no-string-literal
// tslint:disable:no-magic-numbers
describe('SelectionEditorService', () => {
    let service: SelectionEditorService;
    let preview: PrevisualizationRectangleComponent;
    let mouseEventLClick: MouseEventWrapper;
    let appComponentCreateSpy: jasmine.Spy<<T>(appComponent: new () => T) => T>;
    // tslint:disable-next-line:prefer-const
    let renderer: Renderer2;
    const INDEX_NOT_FOUND = -1;
    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(SelectionEditorService);

        mouseEventLClick = new MouseEventWrapper({
            offsetX: 20,
            offsetY: 25,
            x: 150,
            y: 150,
            button: MouseButton.Left,
            buttons: 1,
        } as MouseEvent);
        preview = new PrevisualizationRectangleComponent(renderer);
        preview.startPrevisualization({ x: 12, y: 24 }, false, 0);
        appComponentCreateSpy = spyOn(AppComponent, 'createComponent');
        appComponentCreateSpy.and.returnValue(preview);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('get dashedPreviewIsSet should return correct value', () => {
        expect(service.dashedPreviewIsSet).toEqual(false);
    });

    it('get listenArrowMovement should return correct value', () => {
        const arrowMovementEvent: EventEmitter<void> = new EventEmitter<void>();
        service['arrowMovementEvent'] = arrowMovementEvent;
        expect(service.listenArrowMovement()).toEqual(arrowMovementEvent.asObservable());
    });

    it('destroyPrevisualizations should call destroy dashed preview and destroy global preview', () => {
        service['dashedPrevisualization'] = new PrevisualizationRectangleComponent(renderer);
        service['globalPrevisualization'] = new PrevisualizationRectangleComponent(renderer);
        const dashedSpy = spyOn<any>(service, 'destroyDashedPrevisualization').and.callThrough();
        const globalSpy = spyOn<any>(service, 'destroyGlobalPrevisualization').and.callThrough();
        service.finishSelection();
        expect(dashedSpy).toHaveBeenCalled();
        expect(globalSpy).toHaveBeenCalled();
    });

    it('initDashedPrevisualization should call destroyDashedPrevisualization and startPrevisualization', () => {
        service['dashedPrevisualization'] = new PrevisualizationRectangleComponent(renderer);
        const destroySpy = spyOn<any>(service, 'destroyDashedPrevisualization');
        service.initDashedPrevisualization({ x: 5, y: 5 }, true);
        expect(destroySpy).toHaveBeenCalled();
        const startSpy = spyOn<any>(service['dashedPrevisualization'], 'startPrevisualization');
        destroySpy.and.callFake(() => {
            return;
        });
        service.initDashedPrevisualization({ x: 5, y: 5 }, true);
        expect(startSpy).toHaveBeenCalled();
    });

    it('initGlobalPrevisualization should change attributes', () => {
        service['globalPrevisualization'] = new PrevisualizationRectangleComponent(renderer);
        service['globalPrevisualization'].isCircular = true;
        service['globalPrevisualization'].outlineStyle = OutlineStyle.DASHED;
        service.initGlobalPrevisualization();
        expect(service['globalPrevisualization'].isCircular).toEqual(false);
        expect(service['globalPrevisualization'].outlineStyle as number).toEqual(OutlineStyle.OUTLINE);
    });

    it('startEditSelection should put correct values in the attributes', () => {
        service['position'] = { x: 100, y: 100 };
        service['globalPrevisualization'] = new PrevisualizationRectangleComponent(renderer);
        const scalerPosSpy = spyOn<any>(service['globalPrevisualization'], 'getScalerPosition');
        scalerPosSpy.and.returnValue({ x: 45, y: 45 });
        service.setStartMovingPosition(mouseEventLClick.absolutePosition);
        expect(service['mouseDownCursorPos']).toEqual(mouseEventLClick.absolutePosition);
        expect(service['mouseDownSelectionPos']).toEqual({ x: 100, y: 100 });
        expect(service['mouseDownAnchorPos']).toEqual({ x: 45, y: 45 });
    });

    it('should register arrow keybinds', () => {
        const registerSpy = spyOn(service['keyBindingService'], 'registerKeybind');
        service.registerArrowKeybinds();
        expect(registerSpy).toHaveBeenCalledTimes(4);
    });

    it('should unregister arrow keybinds', () => {
        const unregisterSpy = spyOn(service['keyBindingService'], 'unregisterKeybind');
        service.unregisterArrowKeyBinds();
        expect(unregisterSpy).toHaveBeenCalledTimes(4);
    });

    it('should handle selectAll correctly', () => {
        ResizeService['currentSize'] = { x: 500, y: 400 };
        const updatePreviewSpy = spyOn<any>(service, 'updatePreviewComponent');
        const setElementSpy = spyOn(service, 'setDrawingElementRect');
        const element = {} as DrawingSelection;
        service.selectAllPrevisualizationRect(element);
        expect(updatePreviewSpy).toHaveBeenCalledTimes(2);
        expect(setElementSpy).toHaveBeenCalled();
        expect(service['position']).toEqual({ x: DrawingCanvasHelper.OFFSET_LEFT - 1, y: DrawingCanvasHelper.OFFSET_TOP - 1 });
        expect(service['size']).toEqual({ x: 500, y: 400 });
    });

    it('should update moving position correctly without magnetization', () => {
        service['position'] = { x: 100, y: 100 };
        service['mouseDownCursorPos'] = { x: 50, y: 50 };
        service['mouseDownSelectionPos'] = { x: 100, y: 100 };
        service['mouseDownAnchorPos'] = { x: 50, y: 50 };
        service['magnetService']['enabled'] = false;
        const previewSpy = spyOn<any>(service, 'updatePreviewComponent');
        service.updateMovingPreview({ x: 55, y: 55 });
        expect(service['position']).toEqual({ x: 105, y: 105 });
        expect(previewSpy).toHaveBeenCalledTimes(2);
    });

    it('should update moving position correctly with magnetization', () => {
        service['position'] = { x: 320, y: 20 };
        service['mouseDownCursorPos'] = { x: 450, y: 50 };
        service['mouseDownSelectionPos'] = { x: 320, y: 20 };
        service['mouseDownAnchorPos'] = { x: 320, y: 20 };
        service['magnetService']['enabled'] = true;
        service['magnetService']['gridService']['scale'] = 50;
        const previewSpy = spyOn<any>(service, 'updatePreviewComponent');
        service.updateMovingPreview({ x: 450, y: 60 });
        expect(service['position']).toEqual({ x: 310, y: 10 });
        expect(previewSpy).toHaveBeenCalledTimes(2);
    });

    it('should set attributes correctly based on rect positions', () => {
        const previewSpy = spyOn<any>(service, 'updatePreviewComponent');
        service.setRectFromPositions({ x: 0, y: 0 }, { x: 100, y: 100 });
        expect(service['position']).toEqual({ x: 0, y: 0 });
        expect(service['size']).toEqual({ x: 100, y: 100 });
        expect(previewSpy).toHaveBeenCalledTimes(2);
    });

    it('should check if coord is in selection', () => {
        service['position'] = { x: 0, y: 0 };
        service['size'] = { x: 200, y: 200 };
        expect(service.isInPreviewRect(mouseEventLClick.absolutePosition)).toEqual(true);
    });

    it('setDrawingElementRect should change attributes', () => {
        const drawSelec: DrawingSelection = new DrawingSelection();
        service['dashedPrevisualization'] = preview;
        service.setDrawingElementRect(drawSelec);
        expect(drawSelec.size).toEqual({ x: service['dashedPrevisualization'].width, y: service['dashedPrevisualization'].height });
    });

    it('getArrowKeyIndex should return the key index of the pressed key', () => {
        const key: ArrowKeys = ArrowKeys.Up;
        const findIndexSpy = spyOn(service['arrowKeysPressed'], 'findIndex');
        service['getArrowKeyIndex'](key);
        expect(findIndexSpy).toHaveBeenCalled();
    });

    it('registerArrowKey should updatePreviewPositionWithArrows', () => {
        const key: ArrowKeys = ArrowKeys.Down;
        spyOn<any>(service, 'updatePreviewPositionWithArrows');
        service['registerArrowKey'](key);
        expect(service['updatePreviewPositionWithArrows']).toHaveBeenCalledWith(key);
    });

    it('registerArrowKey should not startArrowKeyTimeout if index is valid', () => {
        const key: ArrowKeys = ArrowKeys.Down;
        spyOn<any>(service, 'getArrowKeyIndex').and.returnValue(ArrowKeys.Down as number);
        spyOn<any>(service, 'startArrowKeyTimeout');
        service['registerArrowKey'](key);
        expect(service['startArrowKeyTimeout']).not.toHaveBeenCalled();
    });

    it('unregisterArrowKey should getArrowKeyIndex and clearTimeout if arrowKeysPressed is empty', () => {
        const key: ArrowKeys = ArrowKeys.Left;
        spyOn<any>(service, 'getArrowKeyIndex');
        service['unregisterArrowKey'](key);
        expect(service['getArrowKeyIndex']).toHaveBeenCalledWith(key);
    });

    it('unregisterArrowKey should not clearTimeout if arrowKeysPressed different then 0', () => {
        const key: ArrowKeys = ArrowKeys.Left;
        spyOn<any>(service, 'getArrowKeyIndex').and.returnValue(ArrowKeys.Left as number);
        const spy = spyOn(service['arrowKeysPressed'], 'splice');
        service['arrowKeysPressed'].length = ArrowKeys.Up as number;
        service['unregisterArrowKey'](key);
        expect(spy).toHaveBeenCalledWith(ArrowKeys.Left as number, ArrowKeys.Up as number);
    });

    it('unregisterArrowKey should do nothing if arrowsKey index is not found', () => {
        const key: ArrowKeys = INDEX_NOT_FOUND;
        spyOn<any>(service, 'getArrowKeyIndex').and.returnValue(key);
        const spy = spyOn<any>(service['arrowKeysPressed'], 'splice');
        service['unregisterArrowKey'](key);
        expect(spy).not.toHaveBeenCalled();
    });

    it('startArrowKeyTimeout should do nothing if arrowsKey index is not found', () => {
        spyOn<any>(service, 'startArrowInterval');
        service['startArrowKeyTimeout']();
        expect(service['startArrowInterval']).not.toHaveBeenCalled();
    });

    it('startArrowKeyTimeout should do nothing if arrowsKey index is not found', () => {
        spyOn<any>(service, 'startArrowInterval');
        service['intervalIsRegistered'] = true;
        service['startArrowKeyTimeout']();
        expect(service['startArrowInterval']).not.toHaveBeenCalled();
    });

    it('startArrowInterval should do nothing if interval is regisitered', () => {
        spyOn<any>(service, 'updatePreviewPositionWithArrows');
        service['intervalIsRegistered'] = true;
        service['startArrowInterval']();
        expect(service['updatePreviewPositionWithArrows']).not.toHaveBeenCalled();
    });

    it('updatePreviewPositionWithArrows should update position correctly when magnetized', () => {
        service['position'] = { x: 320, y: 20 };
        service['magnetService']['enabled'] = true;
        service['magnetService']['gridService']['scale'] = 50;
        service['globalPrevisualization'] = new PrevisualizationRectangleComponent(renderer);
        const scalerPosSpy = spyOn<any>(service['globalPrevisualization'], 'getScalerPosition');
        scalerPosSpy.and.returnValue({ x: 355, y: 55 });
        service['updatePreviewPositionWithArrows'](ArrowKeys.Right);
        expect(service['position']).toEqual({ x: 325, y: 25 });
    });

    it('should start interval correctly', async (done: DoneFn) => {
        spyOn<any>(service, 'intervalIsDone').and.returnValues(false, false, true);
        const updatePreviewPositionWithArrowsSpy = spyOn<any>(service, 'updatePreviewPositionWithArrows');
        const int = interval(service['INTERVAL_MS'] + 1);
        service['startArrowInterval']();
        let count = 0;
        int.pipe(
            takeWhile(() => {
                return count++ < 2;
            }),
        ).subscribe(() => {
            if (count === 2) {
                expect(updatePreviewPositionWithArrowsSpy).toHaveBeenCalledTimes(2);
                done();
            }
        });
    });

    it('currentPosition getter should return a Vec2 with position coordinates', () => {
        service['position'] = { x: 100, y: 100 };
        expect(service.currentPosition).toEqual({ x: 100, y: 100 });
    });
});
