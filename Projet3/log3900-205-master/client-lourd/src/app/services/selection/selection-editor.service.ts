import { Injectable } from '@angular/core';
import { PrevisualizationRectangleComponent } from '@app/app-components/previsualization-rectangle/previsualization-rectangle.component';
import { Vec2 } from '@app/classes/drawing/vec2';
import { MouseEventWrapper } from '@app/classes/mouse-event-wrapper';
import { OutlineStyle } from '@app/classes/outline-style';
import { ComponentContainerComponent } from '@app/components/component-container/component-container.component';

@Injectable({
    providedIn: 'root',
})
export class SelectionEditorService {
    readonly INDEX_NOT_FOUND: number = -1;

    private position: Vec2 = { x: 0, y: 0 };
    private size: Vec2 = { x: 0, y: 0 };

    private globalPrevisualization: PrevisualizationRectangleComponent | undefined;

    private mouseDownCursorPos: Vec2 = { x: 0, y: 0 };
    private mouseDownSelectionPos: Vec2 = { x: 0, y: 0 };

    get globalPrevisualizationComponent(): PrevisualizationRectangleComponent | undefined {
        return this.globalPrevisualization;
    }

    get currentPosition(): Vec2 {
        return { x: this.position.x, y: this.position.y };
    }

    get currentSize(): Vec2 {
        return { x: this.size.x, y: this.size.y };
    }

    setSize(size: Vec2): void {
        this.size.x = size.x;
        this.size.y = size.y;
        this.updatePreviewComponent(this.globalPrevisualizationComponent);
    }

    finishSelection(): void {
        this.destroyGlobalPrevisualization();
        this.position = { x: 0, y: 0 };
        this.size = { x: 0, y: 0 };
        this.mouseDownCursorPos = { x: 0, y: 0 };
        this.mouseDownSelectionPos = { x: 0, y: 0 };
    }

    private destroyGlobalPrevisualization(): void {
        if (this.globalPrevisualization !== undefined) {
            ComponentContainerComponent.destroyComponent(this.globalPrevisualization);
            this.globalPrevisualization = undefined;
        }
    }

    initGlobalPrevisualization(onStartMove: (event: MouseEventWrapper) => void): void {
        this.globalPrevisualization = ComponentContainerComponent.createComponent<PrevisualizationRectangleComponent>(PrevisualizationRectangleComponent);
        this.globalPrevisualization.isCircular = false;
        this.globalPrevisualization.outlineStyle = OutlineStyle.OUTLINE;
        this.globalPrevisualization.onStartMove = onStartMove;
        this.updatePreviewComponent(this.globalPrevisualization);
    }

    private updatePreviewComponent(preview: PrevisualizationRectangleComponent | undefined): void {
        if (preview !== undefined) {
            const posX = this.position.x + this.size.x;
            const posY = this.position.y + this.size.y;
            const top = { x: Math.min(posX, this.position.x), y: Math.min(posY, this.position.y) };

            preview._top = top.y;
            preview._left = top.x;
            preview._width = Math.abs(this.size.x) - 1;
            preview._height = Math.abs(this.size.y) - 1;
        }
    }

    // Update la position selon une translation de la denière update.
    updateMovingPreview(absolutePosition: Vec2): void {
        if (this.mouseDownCursorPos == undefined) {
            return;
        }
        let translation = { x: absolutePosition.x - this.mouseDownCursorPos.x, y: absolutePosition.y - this.mouseDownCursorPos.y };
        this.position = { x: this.mouseDownSelectionPos.x + translation.x, y: this.mouseDownSelectionPos.y + translation.y };
        this.updatePreviewComponent(this.globalPrevisualization);
    }

    // Set la position de départ pour commencer à bouger la sélection
    setStartMovingPosition(position: Vec2): void {
        this.mouseDownCursorPos = position;
        this.mouseDownSelectionPos = this.position;
    }

    // Set les attributs position et size selon deux positions (TopLeft, BottomRight)
    setRectFromPositions(positionStart: Vec2, positionEnd: Vec2): void {
        this.position = positionStart;
        this.size = { x: positionEnd.x - positionStart.x, y: positionEnd.y - positionStart.y };
        this.updatePreviewComponent(this.globalPrevisualization);
    }

    isInPreviewRect(absolutePosition: Vec2): boolean {
        const xPositionSize = this.position.x + this.size.x;
        const yPositionSize = this.position.y + this.size.y;
        const top = { x: Math.min(this.position.x, xPositionSize), y: Math.min(this.position.y, yPositionSize) };
        const bottom = { x: Math.max(this.position.x, xPositionSize), y: Math.max(this.position.y, yPositionSize) };

        return absolutePosition.x >= top.x && absolutePosition.y >= top.y && absolutePosition.x <= bottom.x && absolutePosition.y <= bottom.y;
    }  
}
