import { TestBed } from '@angular/core/testing';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { DrawingCanvasHelper } from '@app/classes/drawing-canvas-helper';
import { Color } from '@app/classes/drawing/color';
import { DrawingEvent } from '@app/classes/drawing/drawing-event';
import { DrawingType } from '@app/classes/drawing/drawing-type';
import { ResizeEvent } from '@app/classes/drawing/resize-event';
import { ToolClasses } from '@app/classes/drawing/tool-classes';
import { Vec2 } from '@app/classes/drawing/vec2';
import { ToolProperties } from '@app/classes/tool-properties/tool-properties';
import { AttributesManagerService } from '@app/services/api/attributes-manager.service';
import { KeyBindingResolverService } from '@app/services/key-binding/key-binding-resolver.service';
import { ColorService } from '@app/services/tools/color.service';
import { PencilService } from '@app/services/tools/pencil-service';
import { ToolService } from '@app/services/tools/tool.service';
import { DrawingService } from './drawing.service';

class PencilServiceStub extends ToolService {
    readonly TOOL_NAME: string = 'pencil';
    readonly KEYBIND: string = 'c';
}

// tslint:disable:no-any
// tslint:disable:no-string-literal
// tslint:disable:no-empty
// tslint:disable:no-magic-numbers
describe('DrawingService', () => {
    let service: DrawingService;
    let canvasTestHelper: CanvasTestHelper;

    let attributesManagerStub: AttributesManagerService;
    let colorServiceStub: ColorService;
    let keyBindingResolverServiceStub: KeyBindingResolverService;
    let pencilServiceStub: PencilServiceStub;

    let drawingEvent: DrawingEvent;

    let resizeSpy: jasmine.Spy<(size: Vec2, saveEvent?: boolean) => void>;

    beforeEach(() => {
        attributesManagerStub = new AttributesManagerService();
        colorServiceStub = new ColorService(attributesManagerStub);
        keyBindingResolverServiceStub = new KeyBindingResolverService();
        pencilServiceStub = new PencilServiceStub(attributesManagerStub, colorServiceStub, keyBindingResolverServiceStub);

        const toolClassesSpy = jasmine.createSpyObj('ToolClasses', ['getToolService', 'TOOLS', 'TOOL_SERVICES']);
        toolClassesSpy.getToolService.and.returnValue(pencilServiceStub);

        drawingEvent = new DrawingEvent();
        drawingEvent.data = {};
        drawingEvent.tool = pencilServiceStub;
        drawingEvent.drawingType = DrawingType.Stroke;
        drawingEvent.strokeColor = {} as Color;
        drawingEvent.fillColor = {} as Color;
        drawingEvent.attributes = {} as ToolProperties;
        drawingEvent.drawingService = pencilServiceStub['TOOL_DRAWING_SERVICE'];
        drawingEvent.canvasSize = {} as Vec2;

        TestBed.configureTestingModule({
            providers: [
                { provide: PencilService, useValue: PencilServiceStub },
                { provide: ToolClasses, useValue: toolClassesSpy },
            ],
        });
        service = TestBed.inject(DrawingService);

        canvasTestHelper = TestBed.inject(CanvasTestHelper);
        DrawingCanvasHelper.baseCanvas = canvasTestHelper.canvas;
        DrawingCanvasHelper.previewCanvas = canvasTestHelper.drawCanvas;

        service['baseCtx'] = canvasTestHelper.canvas.getContext('2d') as CanvasRenderingContext2D;
        service['previewCtx'] = canvasTestHelper.drawCanvas.getContext('2d') as CanvasRenderingContext2D;
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should have tool list', () => {
        const tools: ToolService[] = [pencilServiceStub];
        expect(service.getToolList()).toEqual(tools);
    });

    it('should set current tool', () => {
        expect(service['currentTool']).toEqual(pencilServiceStub);
    });

    it('should set canvas properties on tool service', () => {
        const toolSpy = spyOn(pencilServiceStub, 'setCanvasProperties').and.callThrough();
        service.initToolsProperties();
        expect(toolSpy).toHaveBeenCalled();
        expect(toolSpy).toHaveBeenCalledWith(service['baseCtx']);
        expect(toolSpy).toHaveBeenCalledWith(service['previewCtx']);
    });

    it('should initialize tool properly when setting current tool', () => {
        const toolCanvasPropertiesSpy = spyOn(pencilServiceStub, 'setCanvasProperties').and.callThrough();
        const defaultPropertiesSpy = spyOn(pencilServiceStub, 'setDefaultProperties').and.callThrough();
        const onInitSpy = spyOn(pencilServiceStub, 'onInit').and.callThrough();

        service.setCurrentTool(pencilServiceStub.TOOL_NAME);

        expect(defaultPropertiesSpy).toHaveBeenCalled();
        expect(service['currentTool']).toEqual(pencilServiceStub);
        expect(onInitSpy).toHaveBeenCalled();
        expect(toolCanvasPropertiesSpy).toHaveBeenCalled();
        expect(toolCanvasPropertiesSpy).toHaveBeenCalledWith(service['baseCtx']);
        expect(toolCanvasPropertiesSpy).toHaveBeenCalledWith(service['previewCtx']);
    });

    it('should destroy current tool before setting current tool', () => {
        const onDestroySpy = spyOn(pencilServiceStub, 'onDestroy').and.callThrough();
        const subscriptionsSpy = spyOn<any>(service, 'flushSubscriptions');

        service.setCurrentTool(pencilServiceStub.TOOL_NAME);

        expect(onDestroySpy).toHaveBeenCalled();
        expect(subscriptionsSpy).toHaveBeenCalled();
    });

    it('should clear the whole canvas', () => {
        service.clearCanvas(service['baseCtx']);
        const pixelBuffer = new Uint32Array(
            service['baseCtx'].getImageData(0, 0, DrawingCanvasHelper.baseCanvas.width, DrawingCanvasHelper.baseCanvas.height).data.buffer,
        );
        const hasColoredPixels = pixelBuffer.some((color) => color !== 0);
        expect(hasColoredPixels).toEqual(false);
    });

    it('should return current tool', async (done: DoneFn) => {
        service.getCurrentTool().subscribe((tool: ToolService) => {
            expect(tool).toEqual(pencilServiceStub);
            done();
        });
    });

    it('should set current tool on keybind press', () => {
        const currentToolSpy = spyOn(service, 'setCurrentTool');
        service['keyBindingResolverService'].keyboardPressEvent({ key: pencilServiceStub.KEYBIND, preventDefault: () => {} } as KeyboardEvent, 'div');
        expect(currentToolSpy).toHaveBeenCalledWith(pencilServiceStub.TOOL_NAME);
    });

    it('should unregister keybinds and destroy tools on destroy', () => {
        const keybindSpy = spyOn(service['keyBindingResolverService'], 'unregisterKeybind');
        const destroySpy = spyOn(pencilServiceStub, 'onDestroy');
        service.onDestroy();
        expect(keybindSpy).toHaveBeenCalled();
        expect(destroySpy).toHaveBeenCalled();
    });

    it('should resolve drawing service on change', () => {
        const resolveSpy = spyOn<any>(service, 'resolveDrawingService');
        pencilServiceStub['previewDataChange'].next({ data: [1] });
        expect(resolveSpy).toHaveBeenCalled();
    });

    it('should add new drawing event to undoRedoService', () => {
        spyOn<any>(service, 'resolveDrawingService').and.returnValue(drawingEvent);
        const undoRedoSpy = spyOn(service['undoRedoService'], 'addDrawingEvent');
        pencilServiceStub['dataChange'].next({ data: [1] });
        expect(undoRedoSpy).toHaveBeenCalledWith(drawingEvent);
    });

    it('should redraw all events on undo', () => {
        const resizeEvent = new ResizeEvent();
        const events = [drawingEvent, drawingEvent, drawingEvent, resizeEvent];
        const redrawSpy = spyOn<any>(service, 'redrawEvent');
        const imageData = service['baseCtx'].getImageData(0, 0, DrawingCanvasHelper.baseCanvas.width, DrawingCanvasHelper.baseCanvas.height);
        service['baseDrawing'] = imageData;
        service['undoRedoService']['undoEvent'].emit(events);
        expect(resizeSpy).toHaveBeenCalled();
        expect(redrawSpy).toHaveBeenCalledTimes(3);
    });

    it('should redraw event on redo', () => {
        const redrawSpy = spyOn<any>(service, 'redrawEvent');
        service['undoRedoService']['redoEvent'].emit(drawingEvent);
        expect(redrawSpy).toHaveBeenCalledTimes(1);
    });

    it('should handle drawEvent correctly on redraw', () => {
        const drawSpy = spyOn(drawingEvent.drawingService, 'drawEvent');
        const setCanvasPropertiesSpy = spyOn(pencilServiceStub, 'setCanvasProperties');
        service['redrawEvent'](drawingEvent);
        expect(resizeSpy).toHaveBeenCalled();
        expect(setCanvasPropertiesSpy).toHaveBeenCalledWith(service['baseCtx'], drawingEvent.attributes);
        expect(drawSpy).toHaveBeenCalledWith(service['baseCtx'], drawingEvent);
    });

    it('should draw on canvas if data is in canvas', () => {
        spyOn(pencilServiceStub['TOOL_DRAWING_SERVICE'], 'isInCanvas').and.returnValue(true);
        const drawSpy = spyOn(drawingEvent.drawingService, 'draw');
        service['resolveDrawingService'](service['baseCtx'], drawingEvent.data);
        expect(drawSpy).toHaveBeenCalledWith(service['baseCtx'], drawingEvent.data, drawingEvent.tool);
    });

    it('should not draw on canvas if data is not in canvas', () => {
        spyOn(pencilServiceStub['TOOL_DRAWING_SERVICE'], 'isInCanvas').and.returnValue(false);
        const drawSpy = spyOn(drawingEvent.drawingService, 'draw');
        service['resolveDrawingService'](service['baseCtx'], drawingEvent.data);
        expect(drawSpy).not.toHaveBeenCalled();
    });

    it('should reset properly', () => {
        const initDrawingSpy = spyOn(service, 'initDrawing');
        service.reset();
        expect(initDrawingSpy).toHaveBeenCalled();
    });

    it('should return tool', () => {
        expect(service.tool).toEqual(service['currentTool']);
    });

    it('should init drawing from image correctly', () => {
        const blob = new Blob();
        const createSpy = spyOn(document, 'createElement').and.callThrough();
        service.initFromImage(blob);
        expect(createSpy).toHaveBeenCalled();
    });

    it('should handle tool change events correctly', async (done: DoneFn) => {
        const observable = service['keyBindingResolverService']['toolChangeEvent'].asObservable();
        service['listenKeybindToolChangeEvent']();
        const toolChangeSpy = spyOn(service, 'setCurrentTool');
        service['currentTool'] = {} as ToolService;

        observable.subscribe(() => {
            setTimeout(() => {
                expect(toolChangeSpy).toHaveBeenCalledWith(pencilServiceStub.TOOL_NAME);
                done();
            }, 100);
        });
        service['keyBindingResolverService']['toolChangeEvent'].emit(pencilServiceStub);
    });
});
