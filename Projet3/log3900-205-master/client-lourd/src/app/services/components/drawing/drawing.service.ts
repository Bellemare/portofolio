import { Injectable } from '@angular/core';
import { DrawingCanvasHelper } from '@app/classes/drawing-canvas-helper';
import { DrawingElement } from '@app/classes/drawing/drawing-element';
import { DrawingEvent } from '@app/classes/drawing/drawing-event';
import { ToolClasses } from '@app/classes/drawing/tool-classes';
import { Vec2 } from '@app/classes/drawing/vec2';
import { AutoSaveService } from '@app/services/api/auto-save.service';
import { KeyBindingResolverService } from '@app/services/key-binding/key-binding-resolver.service';
import { CanvasSizeService } from '@app/services/canvas-size/canvas-size.service';
import { ToolService } from '@app/services/tools/tool.service';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { SynchronizerService } from '@app/services/synchronization/synchronizer.service';
import { EventType } from '@app/classes/drawing/event-type';
import { DrawingEventPayload } from '@app/classes/socket/drawing-event-payload';
import { ToolType } from '@app/classes/drawing/tool-type';
import { DrawingHistoryPayload } from '@app/classes/socket/drawing-history-payload';
import { CouchesService } from '@app/services/couches/couches.service';
import { DrawingEventWrapper } from '@app/classes/drawing/drawing-event-wrapper';
import { AuthService } from '@app/services/api/auth.service';
import { SelectionService } from '@app/services/tools/selection/selection.service';
import { LayerEventType } from '@app/classes/socket/layer-event-type';

@Injectable({
    providedIn: 'root',
})
export class DrawingService {
    private tools: Map<ToolType, ToolService> = new Map<ToolType, ToolService>();
    private currentTool: ToolService;
    private previousTool: ToolService;
    private currentToolChange: BehaviorSubject<ToolService>;

    private subscriptions: Subscription[] = [];
    private drawingSubscriptions: Subscription[] = [];

    private baseCtx: CanvasRenderingContext2D;
    private previewCtx: CanvasRenderingContext2D;
    private collabPreviewCtx: CanvasRenderingContext2D;

    private drawingEventCanvas: HTMLCanvasElement;

    hasInitialized: boolean = false;

    isLoading: boolean = true;
    disableEvents: boolean = false;

    constructor(
        private keyBindingResolverService: KeyBindingResolverService,
        private toolClasses: ToolClasses,
        private CanvasSizeService: CanvasSizeService,
        private autoSaveService: AutoSaveService,
        private synchronizer: SynchronizerService,
        private couchesService: CouchesService,
        private authService: AuthService,
    ) {}

    onInit(drawingId: number): void {
        if (!this.hasInitialized) {
            this.drawingEventCanvas = document.createElement("canvas");
            this.drawingEventCanvas.width = CanvasSizeService.canvasSize.x;
            this.drawingEventCanvas.height = CanvasSizeService.canvasSize.y;

            this.hasInitialized = true;
            this.initTools();
            this.listenDataChanged();
            
            this.synchronizer.init(drawingId);
            this.listenToCollaboratorsEvents();
            this.subscriptions.push(this.synchronizer.listenOnLoad().subscribe((drawing: DrawingHistoryPayload) => {
                this.isLoading = true;
                this.waitForCtx(() => {
                    this.initLayerTree(drawing);
                    this.isLoading = false;
                });
            }));
            this.listenLayerEvents();
        }
    }

    private waitForCtx(onLoad: () => void): void {
        if (this.baseCtx == undefined) {
            setTimeout(() => {
                this.waitForCtx(onLoad);
            }, 100);
        } else {
            onLoad();
        }
    }

    private initTools(): void {
        ToolClasses.TOOLS.forEach((toolName: string) => {
            const tool = this.toolClasses.getToolService(toolName);
            tool.onDestroy();
            this.tools.set(tool.TOOL_TYPE, tool);
            tool.onCreate();
        });
        this.currentTool = this.tools.values().next().value;
        this.currentToolChange = new BehaviorSubject<ToolService>(this.currentTool);
        this.currentTool.setDefaultProperties();
        this.currentTool.onInit();
        this.listenToolAttributesChanged();
        this.initToolKeyBinds();
        this.listenKeybindToolChangeEvent();
        this.listenSelectionEvents();
    }

    private listenKeybindToolChangeEvent(): void {
        this.keyBindingResolverService.listenToolChangeEntry().subscribe((tool: ToolService) => {
            if (tool !== this.currentTool) {
                this.setCurrentTool(tool.TOOL_TYPE);
            }
        });
    }

    private initToolKeyBinds(): void {
        this.tools.forEach((tool: ToolService) => {
            this.keyBindingResolverService.registerKeybind({
                keybind: tool.KEYBIND,
                callback: () => this.setCurrentTool(tool.TOOL_TYPE),
            });
        });
    }

    private initLayerTree(drawing: DrawingHistoryPayload): void {
        this.CanvasSizeService.setSizeCanvas();
        if (drawing.events) {
            drawing.events.forEach((event: DrawingEvent) => {
                event.data = ToolClasses.registerNewElement(event.data, event.tool_type);
                const layer = new DrawingEventWrapper(event);
                layer.renderFct = this.drawFromDrawingEventWrapper.bind(this);
                layer.redrawFct = (evt: DrawingEvent) => this.drawFromEvent(evt, this.drawingEventCanvas.getContext("2d") as CanvasRenderingContext2D);
                this.drawFromDrawingEventWrapper(layer);
                this.couchesService.addLayer(layer, false);
            });
            this.couchesService.renderAll();
            this.drawTopLayer();
        }
    }

    initCtx(): void {
        this.baseCtx = DrawingCanvasHelper.baseCanvas.getContext('2d') as CanvasRenderingContext2D;
        this.previewCtx = DrawingCanvasHelper.previewCanvas.getContext('2d') as CanvasRenderingContext2D;
        this.collabPreviewCtx = DrawingCanvasHelper.collabPreviewCanvas.getContext('2d') as CanvasRenderingContext2D;
    }

    initFromSrc(src: string): void {
        const img = document.createElement('img');
        img.onload = this.loadImageData.bind(this, img);
        img.src = src;
    }

    initFromImage(image: Blob): void {
        this.initFromSrc(URL.createObjectURL(image));
    }

    private loadImageData(img: HTMLImageElement): void {
        this.CanvasSizeService.setSizeCanvas();
        this.baseCtx.drawImage(img, 0, 0);
        this.initDrawing(false);
    }

    initDrawing(fillWhite: boolean = true): void {
        if (fillWhite) {
            this.fillWhiteRect({ x: 0, y: 0 }, CanvasSizeService.canvasSize);
        }
        this.setCanvasProperties();
        this.autoSaveService.saveDrawingState();
    }

    fillWhiteRect(start: Vec2, end: Vec2): void {
        if (this.baseCtx !== undefined) {
            this.baseCtx.fillStyle = '#fff';
            this.baseCtx.fillRect(start.x, start.y, end.x, end.y);
        }
    }

    initToolsProperties(): void {
        this.setCanvasProperties();
    }

    getToolList(): ToolService[] {
        return Array.from(this.tools.values());
    }

    private listenSelectionEvents(): void {
        const selection = this.tools.get(ToolType.SELECTION) as SelectionService;
        selection.onRedrawSelectionEvent = () => {
            this.drawTopLayer();
        };
        selection.onPreviewSelectionEvent = (selection: DrawingEventWrapper) => {
            this.clearCanvas(this.previewCtx);
            if (selection?.drawingEvent) {
                this.drawFromEvent(selection.drawingEvent, this.previewCtx);
            }
        }
        this.drawingSubscriptions.push(selection.listenEndSelection().subscribe((changeTool: boolean) => {
            if (this.previousTool != undefined && changeTool) {
                this.setCurrentTool(this.previousTool.TOOL_TYPE);
            }
        }));
    }

    private listenLayerEvents(): void {
        this.synchronizer.layerAddEvent = () => {
            this.drawTopLayer();
        };
        this.drawingSubscriptions.push(this.synchronizer.listenToRedrawEvent().subscribe((save: boolean) => {
            this.drawTopLayer();
            if (save) {
                this.synchronizer.saveDrawing();
            }
        }));
    }

    private listenDataChanged(): void {
        this.subscriptions.push(this.currentTool.getData().subscribe(this.dataChanged.bind(this)));
        this.subscriptions.push(this.currentTool.getPreviewData().subscribe(this.previewDataChanged.bind(this)));
    }

    private dataChanged(data: DrawingElement): void {
        if (!this.currentTool.currentToolDrawingService.isEmpty(data)) {
            const drawingEvent = this.resolveDrawingService(data, EventType.DRAW);
            if (drawingEvent !== undefined) {
                this.synchronizer.sendEvent(drawingEvent);
                this.disableEvents = true;
            }
        }
    }

    private previewDataChanged(data: DrawingElement): void {
        data.bounding_box = undefined;
        data.base_bounding_box = undefined;
        const drawingEvent = this.resolveDrawingService(data, EventType.PREVIEW);
        if (drawingEvent !== undefined) {
            if (this.currentTool.CLEAR_ON_PREVIEW) {
                this.clearCanvas(this.previewCtx);
            }
            this.currentTool.currentToolDrawingService.drawEvent(this.previewCtx, drawingEvent);
            this.synchronizer.sendEvent(drawingEvent);
        }
    }

    private collaboratorsPreviewEvent(clearPreview: boolean): void {
        this.clearCanvas(this.collabPreviewCtx);
        if (clearPreview) {
            this.clearCanvas(this.previewCtx);
            const toolType = this.currentTool.TOOL_TYPE;
            if (toolType == ToolType.SELECTION && (this.currentTool as SelectionService).selectedDrawingEvent?.drawingEvent != undefined) {
                this.drawFromEvent((this.currentTool as SelectionService).selectedDrawingEvent?.drawingEvent as DrawingEvent, this.previewCtx);
            }
        }
        this.synchronizer.getCurrentLayerPreviewEvents().forEach((evt: DrawingEventWrapper) => {
            this.drawFromEvent(evt.drawingEvent as DrawingEvent, this.collabPreviewCtx);
        });
        this.synchronizer.getCurrentPreviewEvents().forEach((evt: DrawingEventPayload) => {
            this.drawFromEvent(evt.event, this.collabPreviewCtx);
        });
    }

    private collaboratorsDrawEvent(evt: DrawingEventPayload): void {
        const layerEvent = new DrawingEventWrapper(evt.event);
        layerEvent.renderFct = this.drawFromDrawingEventWrapper.bind(this);
        layerEvent.redrawFct = (evt: DrawingEvent) => this.drawFromEvent(evt, this.drawingEventCanvas.getContext("2d") as CanvasRenderingContext2D);
        layerEvent.renderLayer();
        this.couchesService.addLayer(layerEvent, false);
        if (evt.user_id == this.authService.getUser().id) {
            this.disableEvents = false;
            this.setCurrentTool(ToolType.SELECTION, false);
            const tool = this.currentTool as SelectionService;
            tool.onInit(true);
            this.synchronizer.layerEvent(LayerEventType.SELECT, layerEvent.drawingEvent as DrawingEvent, this.couchesService.getDrawingIndex(layerEvent));
        }
    }

    private listenToCollaboratorsEvents(): void {
        this.drawingSubscriptions.push(this.synchronizer.listenToPreviewEvent().subscribe(this.collaboratorsPreviewEvent.bind(this)));
        this.drawingSubscriptions.push(this.synchronizer.listenToDrawEvent().subscribe(this.collaboratorsDrawEvent.bind(this)));
    }

    private drawTopLayer(): void {
        const boundingBox = this.couchesService.topLayer.boundingBox;
        const image = this.couchesService.topLayer.image;
        if (boundingBox != undefined && image != undefined && image.width > 0 && image.height > 0) {
            this.clearCanvas(this.baseCtx);
            this.baseCtx.drawImage(this.couchesService.topLayer.image, boundingBox.x, boundingBox.y);
        }
    }

    private drawFromDrawingEventWrapper(drawingEventWrapper: DrawingEventWrapper): boolean {
        if (drawingEventWrapper.drawingEvent == undefined)
            return false;
        
        const canvas = document.createElement("canvas");
        canvas.width = CanvasSizeService.canvasSize.x;
        canvas.height = CanvasSizeService.canvasSize.y;
        const ctx = canvas.getContext("2d") as CanvasRenderingContext2D;
        this.drawFromEvent(drawingEventWrapper.drawingEvent, ctx);
        const boundingBox = drawingEventWrapper.drawingEvent.data.bounding_box;
        let success = boundingBox != undefined && boundingBox.w > 0 && boundingBox.h > 0;
        if (success && boundingBox != undefined) {
            const image = document.createElement("canvas");
            image.width = boundingBox.w;
            image.height = boundingBox.h;
            const imageCtx = image.getContext("2d") as CanvasRenderingContext2D;
            imageCtx.putImageData(ctx.getImageData(boundingBox.x, boundingBox.y, boundingBox.w, boundingBox.h), 0, 0);
            drawingEventWrapper.boundingBox = boundingBox;
            drawingEventWrapper.image = image;
        }

        return success
    }

    private drawFromEvent(event: DrawingEvent, ctx: CanvasRenderingContext2D): void {
        const tool = this.tools.get(event.tool_type);
        if (tool != undefined) {
            tool.setCanvasProperties(ctx, event.attributes);
            tool.currentToolDrawingService.drawEvent(ctx, event);
            tool.setCanvasProperties(ctx, this.currentTool.toolAttributes);
        }
    }

    private listenToolAttributesChanged(): void {
        this.subscriptions.push(this.currentTool.listenAttributeChange().subscribe(this.setCanvasProperties.bind(this)));
    }

    private resolveDrawingService(data: DrawingElement, eventType: EventType): DrawingEvent | undefined {
        if (this.currentTool.currentToolDrawingService.isInCanvas(data)) {
            return this.currentTool.currentToolDrawingService.createDrawingEvent(data, this.currentTool, eventType);
        }
        return undefined;
    }

    setCurrentTool(tool: ToolType, init: boolean = true): void {
        if (this.disableEvents)
            return;

        if (this.tools.has(tool)) {
            this.currentTool.onDestroy();
            this.flushSubscriptions();

            this.previousTool = this.currentTool;
            this.currentTool = this.tools.get(tool) as ToolService;
            this.currentTool.setDefaultProperties();
            if (this.currentTool.TOOL_TYPE != ToolType.SELECTION)
                this.currentTool.setProperty("line_width", this.previousTool.toolAttributes.line_width);
            
            if (init)
                this.currentTool.onInit();

            this.setCanvasProperties();
            this.listenDataChanged();
            this.listenToolAttributesChanged();

            this.currentToolChange.next(this.currentTool);
        }
    }

    private setCanvasProperties(): void {
        if (this.baseCtx != undefined && this.previewCtx != undefined) {
            this.currentTool.setCanvasProperties(this.baseCtx);
            this.currentTool.setCanvasProperties(this.previewCtx);
        }
    }

    getCurrentTool(): Observable<ToolService> {
        return this.currentToolChange.asObservable();
    }

    clearCanvas(context: CanvasRenderingContext2D): void {
        if (context != undefined) {
            context.clearRect(0, 0, CanvasSizeService.canvasSize.x, CanvasSizeService.canvasSize.y);
        }
    }

    reset(): void {
        this.flushSubscriptions();
        this.hasInitialized = false;
        this.synchronizer.onDestroy();
        this.couchesService.reset();
        this.drawingSubscriptions.forEach((sub: Subscription) => sub.unsubscribe());
        this.drawingSubscriptions = [];
        this.isLoading = true;
        this.disableEvents = false;
    }

    private flushSubscriptions(): void {
        this.subscriptions.forEach((sub: Subscription) => sub.unsubscribe());
        this.subscriptions = [];
    }

    onDestroy(): void {
        if (this.currentTool.TOOL_TYPE == ToolType.SELECTION) {
            const service = this.currentTool as SelectionService;
            if (service.selectedDrawingEvent != undefined) {
                service.selectedDrawingEvent.isSelected = false;
                service.selectedDrawingEvent.renderLayer();
                this.drawTopLayer();
            }
            this.synchronizer.saveDrawing();
        }
        this.tools.forEach((tool: ToolService) => {
            tool.onDelete();
            this.keyBindingResolverService.unregisterKeybind(tool.KEYBIND);
        });
        this.reset();
    }

    get tool(): ToolService {
        return this.currentTool;
    }
}
