import { Injectable } from '@angular/core';
import { Color } from '@app/classes/drawing/color';
import { ColorService } from '@app/services/tools/color.service';

@Injectable({
    providedIn: 'root',
})
export class ColorHandlerService {
    private static readonly BLUE_HUE_OFFSET: number = 4;
    private static readonly GREEN_HUE_OFFSET: number = 2;
    private static readonly HUE_VALUE_TO_CIRCLE: number = 60;
    private static readonly TO_PERCENT: number = 100;
    static readonly CIRCLE_DEGREES: number = 360;

    private hue: number;
    private saturation: number;
    private brightness: number;

    private static calculateRGBPercentFormat(color: Color): Color {
        const rPercent = Math.round(color.r) / ColorService.MAX_VALUE;
        const gPercent = Math.round(color.g) / ColorService.MAX_VALUE;
        const bPercent = Math.round(color.b) / ColorService.MAX_VALUE;
        return { r: rPercent, g: gPercent, b: bPercent, a: color.a };
    }

    private static getMax(color: Color): number {
        return Math.max(color.r, color.g, color.b);
    }

    private static getMin(color: Color): number {
        return Math.min(color.r, color.g, color.b);
    }

    static calculateColorHue(color: Color): number {
        const colorPercent = this.calculateRGBPercentFormat(color);
        const min = this.getMin(colorPercent);
        const max = this.getMax(colorPercent);
        const delta = max - min;

        let hue = 0;
        if (max !== min) {
            switch (max) {
                case colorPercent.r:
                    hue = (colorPercent.g - colorPercent.b) / delta;
                    break;
                case colorPercent.g:
                    hue = (colorPercent.b - colorPercent.r) / delta + ColorHandlerService.GREEN_HUE_OFFSET;
                    break;
                case colorPercent.b:
                    hue = (colorPercent.r - colorPercent.g) / delta + ColorHandlerService.BLUE_HUE_OFFSET;
                    break;
            }
        }
        hue *= ColorHandlerService.HUE_VALUE_TO_CIRCLE;
        return Math.round(hue < 0 ? hue + ColorHandlerService.CIRCLE_DEGREES : hue);
    }

    static calculateColorBrightness(color: Color): number {
        const colorPercent = this.calculateRGBPercentFormat(color);
        const max = this.getMax(colorPercent);

        return Math.round(max * ColorHandlerService.TO_PERCENT) / ColorHandlerService.TO_PERCENT;
    }

    static calculateSaturation(color: Color): number {
        const colorPercent = this.calculateRGBPercentFormat(color);
        const min = this.getMin(colorPercent);
        const max = this.getMax(colorPercent);

        return Math.round((max !== 0 ? (max - min) / max : 0) * ColorHandlerService.TO_PERCENT) / ColorHandlerService.TO_PERCENT;
    }

    colorShift(hue: number): void {
        this.hue = hue;
    }

    getUserColor(userId: number): string {
        this.colorShift((userId * 37) % 360);
        return ColorService.getColorRGB(this.color);
    }

    get colorHue(): number {
        return this.hue;
    }

    get colorBrightness(): number {
        return this.brightness;
    }

    get colorSaturation(): number {
        return this.saturation;
    }

    set color(color: Color) {
        this.hue = ColorHandlerService.calculateColorHue(color);
        this.brightness = ColorHandlerService.calculateColorBrightness(color);
        this.saturation = ColorHandlerService.calculateSaturation(color);
    }

    get color(): Color {
        if (this.saturation === 0) {
            const value = ColorService.MAX_VALUE;
            return { r: value, g: value, b: value, a: ColorService.MAX_ALPHA };
        } else {
            return this.hsvToRgb();
        }
    }

    private hsvToRgb(): Color {
        const c = this.brightness * this.saturation;
        const x = c * (1 - Math.abs(((this.hue / ColorHandlerService.HUE_VALUE_TO_CIRCLE) % 2) - 1));
        const m = this.brightness - c;
        let tmpColor = { r: 0, g: 0, b: 0, a: 0 };
        // On désactive les magic numbers ici pour ne pas créer des constantes inutiles
        // comme SIX=6
        // tslint:disable:no-magic-numbers
        switch (Math.floor(this.hue / ColorHandlerService.HUE_VALUE_TO_CIRCLE)) {
            case 0:
                tmpColor = { r: c, g: x, b: 0, a: 0 };
                break;
            case 1:
                tmpColor = { r: x, g: c, b: 0, a: 0 };
                break;
            case 2:
                tmpColor = { r: 0, g: c, b: x, a: 0 };
                break;
            case 3:
                tmpColor = { r: 0, g: x, b: c, a: 0 };
                break;
            case 4:
                tmpColor = { r: x, g: 0, b: c, a: 0 };
                break;
            case 5:
            case 6:
                tmpColor = { r: c, g: 0, b: x, a: 0 };
                break;
        }
        // tslint:enable
        return {
            r: Math.round((tmpColor.r + m) * ColorService.MAX_VALUE),
            g: Math.round((tmpColor.g + m) * ColorService.MAX_VALUE),
            b: Math.round((tmpColor.b + m) * ColorService.MAX_VALUE),
            a: ColorService.MAX_ALPHA,
        };
    }
}
