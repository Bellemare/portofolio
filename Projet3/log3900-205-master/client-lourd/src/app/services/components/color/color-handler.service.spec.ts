import { TestBed } from '@angular/core/testing';
import { Color } from '@app/classes/drawing/color';
import { ColorHandlerService } from './color-handler.service';

// tslint:disable:no-magic-numbers
// tslint:disable:no-string-literal
describe('ColorHandlerService', () => {
    let service: ColorHandlerService;

    let color: Color;
    let h: number;
    let s: number;
    let v: number;

    beforeEach(() => {
        color = { r: 86, g: 191, b: 75, a: 1 };
        h = 114;
        s = 0.61;
        v = 0.75;

        TestBed.configureTestingModule({});
        service = TestBed.inject(ColorHandlerService);

        service.color = color;
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should return correct current color from hsv values', () => {
        expect(service.color).toEqual(color);
    });

    it('should have correct hsv values', () => {
        expect(service.colorHue).toEqual(h);
        expect(service.colorSaturation).toEqual(s);
        expect(service.colorBrightness).toEqual(v);
    });

    it('should return good color when shifing color hue', () => {
        service.colorShift(218);
        expect(service.color).toEqual({ r: 75, g: 117, b: 191, a: 1 });
    });

    it('should be white when no saturation', () => {
        service['hue'] = 240;
        service['saturation'] = 0;
        service['brightness'] = 100;
        expect(service.color).toEqual({ r: 255, g: 255, b: 255, a: 1 });
    });

    it('should be black when brightness and hue are 0', () => {
        service['hue'] = 0;
        service['saturation'] = 50;
        service['brightness'] = 0;
        expect(service.color).toEqual({ r: 0, g: 0, b: 0, a: 1 });
    });

    it('should have correct hue values for r,g and b', () => {
        service.color = { r: 255, g: 0, b: 0, a: 1 };
        expect(service.colorHue).toEqual(0);

        service.color = { r: 0, g: 255, b: 0, a: 1 };
        expect(service.colorHue).toEqual(120);

        service.color = { r: 0, g: 0, b: 255, a: 1 };
        expect(service.colorHue).toEqual(240);
    });

    it('should have no blue in the first 120 degrees', () => {
        service['saturation'] = 1;
        service['brightness'] = 1;
        service['hue'] = 10;
        expect(service.color.b).toEqual(0);

        service['hue'] = 120;
        service.colorShift(120);
        expect(service.color.b).toEqual(0);
    });

    it('should have no red between 120 and 240 degrees', () => {
        service['saturation'] = 1;
        service['brightness'] = 1;
        service['hue'] = 120;
        expect(service.color.r).toEqual(0);

        service['hue'] = 240;
        expect(service.color.r).toEqual(0);
    });

    it('should have no green between 240 and 360 degrees', () => {
        service['saturation'] = 1;
        service['brightness'] = 1;
        service['hue'] = 240;
        expect(service.color.g).toEqual(0);

        service['hue'] = 360;
        expect(service.color.g).toEqual(0);
    });

    it('should have 0 hue when gray tone', () => {
        expect(ColorHandlerService.calculateColorHue({ r: 140, g: 140, b: 140, a: 1 })).toEqual(0);
    });

    it('should always have positive hue', () => {
        expect(ColorHandlerService.calculateColorHue({ r: 255, g: 85, b: 223, a: 1 })).toBeGreaterThanOrEqual(0);
    });
});
