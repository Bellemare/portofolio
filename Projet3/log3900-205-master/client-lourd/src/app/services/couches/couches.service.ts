import { Injectable } from '@angular/core';
import { CoucheBranche } from '@app/classes/couches/couche-branche';
import { DrawingEventWrapper } from '@app/classes/drawing/drawing-event-wrapper';
import { Vec2 } from '@app/classes/drawing/vec2';

@Injectable({
    providedIn: 'root'
})
export class CouchesService {
    
    private _topLayer: CoucheBranche;
    private _layers: DrawingEventWrapper[];
    private _height: number;
    private _nElement: number;
    static readonly IDEAL_CHILD_PER_LAYER: number = 4;

    constructor() {
        this.reset();
    }

    reset(): void {
        this._layers = [];
        this._topLayer = new CoucheBranche();
        this._height = 1;
        this._nElement = 0;
    }

    findDrawingEvent(position: Vec2): DrawingEventWrapper | undefined {
        return this._topLayer.selectChild(position);
    }

    getDrawingIndex(drawingEvent: DrawingEventWrapper): number {
        return this._layers.indexOf(drawingEvent);
    }

    moveLayer(layer: number, newLayer: number): void {
        const evt = this.removeLayer(layer);
        if (evt != undefined) {
            this.addLayer(evt, true, newLayer);
        }
    }

    removeLayer(pos: number): DrawingEventWrapper {
        let activeLayer = this._topLayer;
        let total = 0;

        // Seek position
        for (let layer = 0; layer < this._height; layer++) {
            for (let i = 0; i < activeLayer.childCount; i++) {
                if (total + (activeLayer.getChild(i)).drawingEventCount > pos) {
                    activeLayer = activeLayer.getChild(i);
                    break;
                } else {
                    total += (activeLayer.getChild(i)).drawingEventCount;
                }
            }
        }

        activeLayer.removeChild(pos - total);
        this._nElement--;
        return this._layers.splice(pos, 1)[0];
    }

    renderAll(): void {
        let couches = new Array<CoucheBranche>();

        couches.push(this.topLayer);
        this.addChildrenToList(this.topLayer, couches);
        while (couches.length > 0) {
            couches.pop()?.makeRender(false);
        }
    }

    private addChildrenToList(couche: CoucheBranche, couches: CoucheBranche[]): void {
        for (let i = 0; i < couche.childCount; i++) {
            const child = couche.getChild(i);
            if (child instanceof CoucheBranche)
                couches.push(child);
        }

        for (let i = 0; i < couche.childCount; i++) {
            const child = couche.getChild(i);
            if (child instanceof CoucheBranche)
                this.addChildrenToList(child, couches);
        }
    }

    addLayer(drawingEvent: DrawingEventWrapper, render: boolean = true, position?: number): void {
        this._nElement++;
        // Insérer dans l'arbre
        if (position !== undefined && position < this._nElement) {
            // Position précise
            if (position <= 0) {
                this.insertLeft(drawingEvent, render);
                this._layers.splice(0, 0, drawingEvent);
            } else {
                // Trouver la position d'insertion dans l'arbre
                this.insert(drawingEvent, render, position);
                this._layers.splice(position, 0, drawingEvent);
            }
        } else {
            this.insertRight(drawingEvent, render);
            this._layers.push(drawingEvent);
        }
    }

    optimise(): void {
        // Make a tree with a maximum of IDEAL_CHILD_PER_LAYER.
        // let height = Math.ceil(Math.log(this._nElement) / Math.log(CouchesService.IDEAL_CHILD_PER_LAYER));
        // TODO
    }

    getElement(index: number): DrawingEventWrapper {
        return this.seekElement(index)?.drawingEvent;
    }

    getElementFromId(id: number): DrawingEventWrapper {
        return this._layers.find((evt: DrawingEventWrapper) => evt.drawingEvent?.id == id) as DrawingEventWrapper;
    }

    private scaleUpTree(render: boolean): void {
        let newTopLayer = new CoucheBranche();
        newTopLayer.addChild(this._topLayer, render);
        this._topLayer = newTopLayer;
        this._height++;
    }

    private seekElement(pos: number): CoucheBranche {
        let activeLayer = this._topLayer;
        let total = 0;

        // Seek position
        for (let layer = 0; layer < this._height; layer++) {
            for (let i = 0; i < activeLayer.childCount; i++) {
                if (total + (activeLayer.getChild(i)).drawingEventCount > pos) {
                    activeLayer = activeLayer.getChild(i);
                    break;
                } else {
                    total += (activeLayer.getChild(i)).drawingEventCount;
                }
            }
        }

        return activeLayer.getChild(pos - total);
    }

    private static isFilledBranch(c: CoucheBranche): boolean {
        return (c.childCount >= CouchesService.IDEAL_CHILD_PER_LAYER);
    }

    private findBranchingPoint(stack: Array<CoucheBranche>, render: boolean) {
        // Find branching point
        for (let i = stack.length - 1; i >= 0; i--) {
            if (CouchesService.isFilledBranch(stack[i])) {
                // Layer is uneligible
                stack.pop();
            } else {
                break;
            }
        }

        // If no branching point, raise tree to get a new branching point.
        if (stack.length == 0) {
            this.scaleUpTree(render);
            stack.push(this._topLayer);
        }
    }

    private insert(drawingEvent: DrawingEventWrapper, render: boolean, pos: number): void {
        let total = 0;
        let activeLayer = this._topLayer;

        // Seek position
        for (let layer = 0; layer < this._height; layer++) {
            for (let i = 0; i < activeLayer.childCount; i++) {
                if (total + (activeLayer.getChild(i)).drawingEventCount >= pos) {
                    activeLayer = activeLayer.getChild(i);
                    break;
                } else {
                    total += (activeLayer.getChild(i)).drawingEventCount;
                }
            }
        }

        activeLayer.addChild(new CoucheBranche(drawingEvent), render, pos - total);
    }

    private insertLeft(drawingEvent: DrawingEventWrapper, render: boolean): void {
        let couches = new Array<CoucheBranche>();
        let activeLayer = this._topLayer;

        // Make stack
        while (couches.length < this._height) {
            couches.push(activeLayer);
            activeLayer = activeLayer.getChild(0);
        }

        this.findBranchingPoint(couches, render);

        // Add branches until at the floor of tree
        while (couches.length < this._height) {
            couches.push(new CoucheBranche());
            couches[couches.length - 2].addChild(couches[couches.length - 1], false, 0);
        }

        // Add child to the floor of the tree.
        couches[couches.length - 1].addChild(new CoucheBranche(drawingEvent), render, 0);
    }

    private insertRight(drawingEvent: DrawingEventWrapper, render: boolean): void {
        let couches = new Array<CoucheBranche>();
        let activeLayer = this._topLayer;

        // Make stack
        while (couches.length < this._height) {
            couches.push(activeLayer);
            activeLayer = activeLayer.getChild(activeLayer.childCount - 1);
        }

        this.findBranchingPoint(couches, render);

        // Add branches until at the floor of tree
        while (couches.length < this._height) {
            couches.push(new CoucheBranche());
            couches[couches.length - 2].addChild(couches[couches.length - 1], false);
        }

        // Add child to the floor of the tree.
        couches[couches.length - 1].addChild(new CoucheBranche(drawingEvent), render);
    }

    get topLayer(): CoucheBranche {
        return this._topLayer;
    }

    get layers(): DrawingEventWrapper[] {
        return this._layers;
    }
}
