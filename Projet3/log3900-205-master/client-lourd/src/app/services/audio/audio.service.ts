import { Injectable } from '@angular/core';
import { SoundType } from '@app/classes/sound-types';

@Injectable({
    providedIn: 'root',
})
export class AudioService {
    readonly AUDIO_LOCATION = "assets/audios";

    private soundTriggers: Map<SoundType, number> = new Map();

    playSound(sound: SoundType, timeout: number = 100): void {
        if (this.soundTriggers.has(sound)) {
            return;
        }

        const audio = new Audio();
        audio.src = `${this.AUDIO_LOCATION}/${sound}`;
        audio.load();
        audio.play();
        this.soundTriggers.set(sound, timeout);
        setTimeout(() => {
            this.soundTriggers.delete(sound);
        }, timeout);
    }
}
