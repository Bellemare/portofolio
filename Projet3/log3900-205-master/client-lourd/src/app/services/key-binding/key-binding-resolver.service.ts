import { EventEmitter, Injectable } from '@angular/core';
import { KeyBindEntry } from '@app/classes/key-bind-entry';
import { ToolService } from '@app/services/tools/tool.service';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class KeyBindingResolverService {
    static readonly KEYBIND_ADD: string = '+';

    private readonly ELEMENTS_DISABLED_REGEX: RegExp = /TEXTAREA|INPUT|SELECT|MAT-SLIDER/;

    private readonly INDEX_ERROR: number = -1;
    private keypressEntries: KeyBindEntry[];
    private keyupEntries: KeyBindEntry[];
    private keysDown: string[];

    private toolChangeEvent: EventEmitter<ToolService> = new EventEmitter<ToolService>();

    constructor() {
        this.keysDown = [];
        this.keypressEntries = [];
        this.keyupEntries = [];
    }

    listenToolChangeEntry(): Observable<ToolService> {
        return this.toolChangeEvent.asObservable();
    }

    private shouldDisable(targetName: string | null): boolean {
        return targetName != null && this.ELEMENTS_DISABLED_REGEX.test(targetName);
    }

    private disableEnableEntries(disabled: boolean, entries: KeyBindEntry[]): void {
        entries.map((entry: KeyBindEntry) => (entry.disabled = disabled));
    }

    private formatKeybind(entry: KeyBindEntry): KeyBindEntry {
        entry.keybind = entry.keybind.toLowerCase();
        return entry;
    }

    set disabled(disabled: boolean) {
        this.disableEnableEntries(disabled, this.keypressEntries);
        this.disableEnableEntries(disabled, this.keyupEntries);
    }

    clearKeys(): void {
        this.keysDown = [];
    }

    keyboardPressEvent(event: KeyboardEvent, targetName: string | null): void {
        if (this.shouldDisable(targetName)) return;

        if (this.getKeyIndex(event) === this.INDEX_ERROR) {
            this.keysDown.push(event.key.toLowerCase());
            this.resolveCurrentKeybind(event, this.keypressEntries);
        }
    }

    keyboardReleaseEvent(event: KeyboardEvent, targetName: string | null): void {
        if (this.shouldDisable(targetName)) return;

        const index = this.getKeyIndex(event);
        if (index !== this.INDEX_ERROR) {
            this.resolveCurrentKeybind(event, this.keyupEntries);
            this.keysDown.splice(index, 1);
        }
    }

    registerKeybind(keypressEntry: KeyBindEntry, keyupEntry: KeyBindEntry | null = null): void {
        if (this.getKeyEntryIndex(keypressEntry.keybind, this.keypressEntries) === this.INDEX_ERROR) {
            this.keypressEntries.push(this.formatKeybind(keypressEntry));
            if (keyupEntry !== null) {
                this.keyupEntries.push(this.formatKeybind(keyupEntry));
            }
        }
    }

    unregisterKeybind(keybind: string): void {
        this.unregister(keybind, this.keypressEntries);
        this.unregister(keybind, this.keyupEntries);
    }

    private unregister(keybind: string, keybindEntries: KeyBindEntry[]): void {
        const keyIndex = this.getKeyEntryIndex(keybind, keybindEntries);
        if (keyIndex !== this.INDEX_ERROR) {
            keybindEntries.splice(keyIndex, 1);
        }
    }

    private getKeyEntryIndex(keybind: string, keyEntries: KeyBindEntry[]): number {
        return keyEntries.findIndex((entry: KeyBindEntry) => entry.keybind === keybind.toLowerCase());
    }

    private getKeyIndex(key: KeyboardEvent): number {
        return this.keysDown.findIndex((k: string) => k === key.key.toLowerCase());
    }

    private resolveCurrentKeybind(event: KeyboardEvent, keyEntries: KeyBindEntry[]): void {}
}
