import { TestBed } from '@angular/core/testing';
import { KeyBindEntry } from '@app/classes/key-bind-entry';
import { ToolService } from '@app/services/tools/tool.service';
import { Observable } from 'rxjs';
import { KeyBindingResolverService } from './key-binding-resolver.service';

// tslint:disable:no-empty
class KeybindTestHelper {
    keypressEvent(): void {}

    keyupEvent(): void {}
}

// tslint:disable:no-string-literal
describe('KeyBindingResolverService', () => {
    let service: KeyBindingResolverService;

    let keybind: string;

    let keybindHelper: jasmine.SpyObj<KeybindTestHelper>;
    let keypressEntry: KeyBindEntry;
    let keyupEntry: KeyBindEntry;

    let keyEvent: KeyboardEvent;

    beforeEach(() => {
        keybind = 's';

        keybindHelper = jasmine.createSpyObj('KeybindTestHelper', ['keypressEvent', 'keyupEvent']);
        keypressEntry = { keybind, callback: keybindHelper.keypressEvent };
        keyupEntry = { keybind, callback: keybindHelper.keyupEvent };
        keyEvent = { key: keybind, preventDefault: () => {} } as KeyboardEvent;

        TestBed.configureTestingModule({});
        service = TestBed.inject(KeyBindingResolverService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should register keybind', () => {
        service.registerKeybind(keypressEntry, keyupEntry);
        expect(service['keypressEntries']).toEqual([keypressEntry]);
        expect(service['keyupEntries']).toEqual([keyupEntry]);
    });

    it('should not register keybind twice', () => {
        service['keypressEntries'] = [keypressEntry];
        service.registerKeybind(keypressEntry, keyupEntry);
        expect(service['keypressEntries']).toEqual([keypressEntry]);
    });

    it('should unregister keybind', () => {
        service['keyupEntries'] = [keyupEntry];
        service['keypressEntries'] = [keypressEntry];
        service.unregisterKeybind(keybind);
        expect(service['keypressEntries']).toEqual([]);
        expect(service['keyupEntries']).toEqual([]);
    });

    it('should call all onEventActivate entries', () => {
        keypressEntry.activateOnEvent = true;
        const entry = { keybind: 'a', callback: keybindHelper.keyupEvent, activateOnEvent: true };
        service['keypressEntries'] = [keypressEntry, entry];
        service['keysDown'] = ['a'];
        service.keyboardPressEvent(keyEvent, 'div');
        expect(keybindHelper.keypressEvent).toHaveBeenCalled();
    });

    it('should call keypress fct', () => {
        service['keypressEntries'] = [keypressEntry];
        service.keyboardPressEvent(keyEvent, 'div');
        expect(keybindHelper.keypressEvent).toHaveBeenCalled();
    });

    it('should call keyup fct', () => {
        service['keyupEntries'] = [keyupEntry];
        service.keyboardPressEvent(keyEvent, 'div');
        service.keyboardReleaseEvent(keyEvent, 'div');
        expect(keybindHelper.keyupEvent).toHaveBeenCalled();
    });

    it('should clear keys down on clearKeys', () => {
        service.clearKeys();
        expect(service['keysDown']).toEqual([]);
    });

    it('should disable entries when setting disabled', () => {
        // tslint:disable-next-line
        const disableEnableEntriesSpy = spyOn<any>(service, 'disableEnableEntries');
        service.disabled = false;
        expect(disableEnableEntriesSpy).toHaveBeenCalledTimes(2);
    });

    it('should should change all entries to disabled boolean', () => {
        const entries: KeyBindEntry[] = [{ disabled: false } as KeyBindEntry, { disabled: false } as KeyBindEntry];
        service['disableEnableEntries'](true, entries);
        expect(entries.every((entry: KeyBindEntry) => entry.disabled)).toBe(true);
    });

    it('should emit tool change correctly', () => {
        const entry: KeyBindEntry = {
            keybind: 'c',
            tool: {} as ToolService,
            callback: () => {},
        };
        const spy = spyOn(service['toolChangeEvent'], 'emit');
        service['triggerToolChangeEvent'](entry);
        expect(spy).toHaveBeenCalled();
    });

    it('should return tool change event as observable', () => {
        const spy = spyOn(service['toolChangeEvent'], 'asObservable').and.callThrough();
        expect(service.listenToolChangeEntry()).toBeInstanceOf(Observable);
        expect(spy).toHaveBeenCalled();
    });
});
