import { Injectable } from '@angular/core';
import { BoundingBox } from '@app/classes/drawing/bounding-box';
import { DrawingEllipse } from '@app/classes/drawing/drawing-ellipse';
import { DrawingEvent } from '@app/classes/drawing/drawing-event';
import { DrawingType } from '@app/classes/drawing/drawing-type';
import { EllipseProperties } from '@app/classes/tool-properties/ellipse-properties';
import { ToolDrawingService } from '@app/services/drawing-services/tool-drawing.service';
import { TextDrawingService } from './text-drawing.service';

@Injectable({
    providedIn: 'root',
})
export class EllipseDrawingService extends ToolDrawingService {
    
    constructor(private textDrawingService: TextDrawingService) { super(); }

    drawEvent(ctx: CanvasRenderingContext2D, event: DrawingEvent): DrawingEvent {
        if (ctx !== undefined) {
            const data = event.data as DrawingEllipse;
            const attributes = event.attributes as EllipseProperties;
            this.prepareDraw(ctx, event);
            const radiusX = Math.abs(data.radius_x);
            const radiusY = Math.abs(data.radius_y);
            const minimumRadius = Math.min(radiusX, radiusY);
            const adjustedLineWidth = Math.min(attributes.line_width, minimumRadius);
            ctx.lineWidth = Math.max(1, adjustedLineWidth);
            const radiusReduction = event.drawing_type === DrawingType.Filled ? 0 : ctx.lineWidth / 2;
            const adjustedRadiusX = Math.max(0, data.radius_x - radiusReduction);
            const adjustedRadiusY = Math.max(0, data.radius_y - radiusReduction);
            ctx.ellipse(data.x, data.y, adjustedRadiusX, adjustedRadiusY, 0, 0, 2 * Math.PI, false);

            if (data.base_bounding_box == undefined) {
                data.base_bounding_box = this.setBoundingBox(data, radiusX, radiusY, 0);
            }
            this.endDraw(ctx, event.drawing_type);
            const textHeight = this.drawText(ctx, event);
            if (data.bounding_box == undefined) {
                data.bounding_box = this.setBoundingBox(data, radiusX, radiusY, textHeight);
            }
        }
        return event;
    }

    private drawText(ctx: CanvasRenderingContext2D, event: DrawingEvent): number {
        this.textDrawingService.prepareText(ctx, event);
        const data = event.data as DrawingEllipse;
        const lw = ctx.lineWidth;
        const fontHeight = this.textDrawingService.getFontHeight(ctx);
        const padding = this.textDrawingService.padding;
        const pos = {
            x: data.x,
            y: data.y - data.radius_y + lw + fontHeight / 2 + padding.y,
        }
        const lines = data.generateTextLines(ctx);
        return this.textDrawingService.drawText(ctx, pos, lines) + lw;
    }

    private setBoundingBox(data: DrawingEllipse, radiusX: number, radiusY: number, textHeight: number): BoundingBox {
        return {
            x: Math.floor(data.x - radiusX),
            y: Math.floor(data.y - radiusY),
            w: Math.ceil(radiusX * 2),
            h: Math.max(Math.ceil(radiusY * 2), textHeight + (this.textDrawingService.padding.y * 2)),
        };
    }

    isEmpty(data: DrawingEllipse): boolean {
        return (
            data.x === undefined ||
            data.y === undefined ||
            data.radius_y === undefined ||
            data.radius_x === undefined || 
            data.radius_x <= 0.5 || 
            data.radius_y <= 0.5
        );
    }

    isInCanvas(data: DrawingEllipse): boolean {
        const top = { x: data.x - data.radius_x, y: data.y - data.radius_y };
        const bottom = { x: data.x + data.radius_x, y: data.y + data.radius_y };

        return this.rectangleBoxIsInCanvas(top, bottom);
    }
}
