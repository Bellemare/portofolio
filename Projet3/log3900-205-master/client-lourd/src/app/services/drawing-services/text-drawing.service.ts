import { Injectable } from '@angular/core';
import { DrawingEvent } from '@app/classes/drawing/drawing-event';
import { Vec2 } from '@app/classes/drawing/vec2';
import { ToolDrawingService } from '@app/services/drawing-services/tool-drawing.service';
import { ColorService } from '../tools/color.service';

@Injectable({
    providedIn: 'root',
})
export class TextDrawingService extends ToolDrawingService {
    private readonly FONT_HEIGHT_REFERENCE_STRING: string = '|';
    private readonly FONT_HEIGHT_MULTIPLIER: number = 1.2;

    prepareText(ctx: CanvasRenderingContext2D, event: DrawingEvent): void {
        ctx.textAlign = 'center';
        ctx.font = "16px Arial"
        ctx.textBaseline = 'middle';
        ctx.fillStyle = ColorService.getColorRGBA(event.text_color);
    }

    drawText(ctx: CanvasRenderingContext2D, pos: Vec2, lines: string[]): number {
        const fontHeight = this.getFontHeight(ctx);
        let totalHeight = 0;
        let currentIndex = 0;
        for (let i = 0; i < lines.length; i++) {
            const newLines = lines[i].split("\n");
            for (let j = 0; j < newLines.length; j++) {
                const yOffset = (currentIndex++) * fontHeight;
                ctx.fillText(newLines[j], pos.x, pos.y + yOffset);
            }
            totalHeight += fontHeight * newLines.length;
        }

        return totalHeight;
    }

    getFontHeight(ctx: CanvasRenderingContext2D): number {
        const referenceMetrics = ctx.measureText(this.FONT_HEIGHT_REFERENCE_STRING);
        const referenceHeight = referenceMetrics.actualBoundingBoxAscent + referenceMetrics.actualBoundingBoxDescent;
        return referenceHeight * this.FONT_HEIGHT_MULTIPLIER;
    }

    get padding(): Vec2 {
        return {
            x: 10,
            y: 10,
        };
    }
}
