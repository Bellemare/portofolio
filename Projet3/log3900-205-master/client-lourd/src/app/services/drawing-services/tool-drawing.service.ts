import { Injectable } from '@angular/core';
import { DrawingElement } from '@app/classes/drawing/drawing-element';
import { DrawingEvent } from '@app/classes/drawing/drawing-event';
import { DrawingPos } from '@app/classes/drawing/drawing-pos';
import { DrawingType } from '@app/classes/drawing/drawing-type';
import { EventType } from '@app/classes/drawing/event-type';
import { Vec2 } from '@app/classes/drawing/vec2';
import { ColorService } from '@app/services/tools/color.service';
import { ToolService } from '@app/services/tools/tool.service';
import { CanvasSizeService } from '../canvas-size/canvas-size.service';

@Injectable({
    providedIn: 'root',
})
export class ToolDrawingService {
    protected prepareDraw(ctx: CanvasRenderingContext2D, event: DrawingEvent): void {
        ctx.strokeStyle = ColorService.getColorRGBA(event.stroke_color);
        ctx.fillStyle = ColorService.getColorRGBA(event.fill_color);
        ctx.beginPath();
    }

    createDrawingEvent(data: DrawingElement, tool: ToolService, eventType: EventType): DrawingEvent {
        const event = new DrawingEvent();
        event.stroke_color = tool.strokeColor;
        event.fill_color = tool.fillColor;
        event.text_color = tool.textColor;
        event.data = data;
        event.attributes = JSON.parse(JSON.stringify(tool.toolAttributes));
        event.drawing_type = tool.drawingType;
        event.tool_type = tool.TOOL_TYPE;
        event.type = eventType;
        return event;
    }

    // Les classes enfants implémentent cette fonctionnalité
    drawEvent(ctx: CanvasRenderingContext2D, event: DrawingEvent): DrawingEvent {
        return event;
    }

    // Les classes enfants implémentent cette fonctionnalité
    isEmpty(data: DrawingElement): boolean {
        return false;
    }

    // Les classes enfants implémentent cette fonctionnalité
    isInCanvas(data: DrawingElement): boolean {
        return false;
    }

    protected endDraw(ctx: CanvasRenderingContext2D, drawingType: DrawingType): void {
        switch (drawingType) {
            case DrawingType.Stroke:
                ctx.stroke();
                break;
            case DrawingType.Filled:
                ctx.fill();
                break;
            case DrawingType.StrokeFilled:
                ctx.fill();
                ctx.stroke();
                break;
        }
    }

    protected isEmptyDrawingPos(data: DrawingPos): boolean {
        return !data || data.data === undefined || data.data.length === 0;
    }

    protected isInCanvasDrawingPos(data: DrawingPos): boolean {
        if (this.isEmptyDrawingPos(data)) return false;

        return data.data.some((pos: Vec2) => CanvasSizeService.isInCanvas(pos));
    }

    protected rectangleBoxIsInCanvas(topLeft: Vec2, bottomRight: Vec2): boolean {
        return topLeft.x < CanvasSizeService.canvasSize.x && topLeft.y < CanvasSizeService.canvasSize.y && bottomRight.x >= 0 && bottomRight.y >= 0;
    }
}
