import { Injectable } from '@angular/core';
import { BoundingBox } from '@app/classes/drawing/bounding-box';
import { DrawingEvent } from '@app/classes/drawing/drawing-event';
import { DrawingRect } from '@app/classes/drawing/drawing-rect';
import { DrawingType } from '@app/classes/drawing/drawing-type';
import { ToolDrawingService } from '@app/services/drawing-services/tool-drawing.service';
import { ColorService } from '@app/services/tools/color.service';
import { TextDrawingService } from './text-drawing.service';

@Injectable({
    providedIn: 'root',
})
export class RectDrawingService extends ToolDrawingService {

    constructor(private textDrawingService: TextDrawingService) { super(); }

    drawEvent(ctx: CanvasRenderingContext2D, event: DrawingEvent): DrawingEvent {
        if (ctx !== undefined) {
            const data = event.data as DrawingRect;
            this.prepareDraw(ctx, event);

            const adjustedWidth = Math.max(0, Math.abs(data.width) - ctx.lineWidth);
            const adjustedHeight = Math.max(0, Math.abs(data.height) - ctx.lineWidth);
            const shouldFill = ctx.lineWidth > adjustedWidth || ctx.lineWidth > adjustedHeight;
            if (shouldFill) {
                ctx.fillStyle = ColorService.getColorRGBA(event.stroke_color);
                ctx.rect(data.x, data.y, data.width, data.height);
            } else {
                const sizeAdjustment = ctx.lineWidth / 2;
                const xDirection = Math.sign(data.width);
                const yDirection = Math.sign(data.height);
                const x = data.x + xDirection * sizeAdjustment;
                const y = data.y + yDirection * sizeAdjustment;
                ctx.rect(x, y, xDirection * adjustedWidth, yDirection * adjustedHeight);
            }
           
            if (data.base_bounding_box == undefined) {
                data.base_bounding_box = this.setBoundingBox(data, 0);
            } 
            this.endDraw(ctx, shouldFill ? DrawingType.Filled : event.drawing_type);
            const textHeight = this.drawText(ctx, event);
            if (data.bounding_box == undefined) {
                data.bounding_box = this.setBoundingBox(data, textHeight);
            }
        }
        return event;
    }

    private drawText(ctx: CanvasRenderingContext2D, event: DrawingEvent): number {
        this.textDrawingService.prepareText(ctx, event);
        const data = event.data as DrawingRect;
        const lw = ctx.lineWidth;
        const fontHeight = this.textDrawingService.getFontHeight(ctx);
        const padding = this.textDrawingService.padding;
        const pos = {
            x: data.x + data.width / 2,
            y: data.y + lw + fontHeight / 2 + padding.y,
        }
        const lines = data.generateTextLines(ctx);
        return this.textDrawingService.drawText(ctx, pos, lines) + lw;
    }

    private setBoundingBox(data: DrawingRect, textHeight: number): BoundingBox {
        return {
            x: Math.min(data.x, data.x + data.width),
            y: Math.min(data.y, data.y + data.height),
            w: Math.abs(data.width),
            h: Math.max(Math.abs(data.height), textHeight + (this.textDrawingService.padding.y * 2)),
        };
    }

    isEmpty(data: DrawingRect): boolean {
        return (
            data.x === undefined ||
            data.y === undefined ||
            data.width === undefined ||
            data.height === undefined ||
            Math.abs(data.width) <= 0.5 ||
            Math.abs(data.height) <= 0.5
        );
    }

    isInCanvas(data: DrawingRect): boolean {
        const topCorner = { x: Math.min(data.x, data.x + data.width), y: Math.min(data.y, data.y + data.height) };
        const bottomCorner = { x: Math.max(data.x, data.x + data.width), y: Math.max(data.y, data.y + data.height) };

        return this.rectangleBoxIsInCanvas(topCorner, bottomCorner);
    }
}
