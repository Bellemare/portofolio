import { TestBed } from '@angular/core/testing';
import { Color } from '@app/classes/drawing/color';
import { DrawingPos } from '@app/classes/drawing/drawing-pos';
import { DrawingType } from '@app/classes/drawing/drawing-type';
import { ToolProperties } from '@app/classes/tool-properties/tool-properties';
import { AttributesManagerService } from '@app/services/api/attributes-manager.service';
import { KeyBindingResolverService } from '@app/services/key-binding/key-binding-resolver.service';
import { ResizeService } from '@app/services/resize/resize.service';
import { ColorService } from '@app/services/tools/color.service';
import { PencilService } from '@app/services/tools/pencil-service';
import { ToolService } from '@app/services/tools/tool.service';
import { PencilDrawingService } from './pencil-drawing.service';

class PencilServiceStub extends ToolService {
    get strokeColor(): Color {
        return { r: 0, g: 0, b: 0, a: 1 };
    }

    get fillColor(): Color {
        return { r: 0, g: 0, b: 0, a: 1 };
    }

    get toolAttributes(): ToolProperties {
        return { drawingType: DrawingType.Stroke };
    }
}

// tslint:disable:no-any
// tslint:disable:no-magic-numbers
// tslint:disable:prefer-const
// tslint:disable:no-string-literal
describe('PencilDrawingService', () => {
    let service: PencilDrawingService;

    let pencilService: PencilServiceStub;

    let canvas: HTMLCanvasElement;
    let ctx: CanvasRenderingContext2D;

    let drawingType: DrawingType;
    let data: DrawingPos;

    beforeEach(() => {
        canvas = document.createElement('canvas');
        canvas.width = 100;
        canvas.height = 100;
        ctx = canvas.getContext('2d') as CanvasRenderingContext2D;

        const attributesManagerService = new AttributesManagerService();
        const colorService = new ColorService(attributesManagerService);
        const keyBindingService = new KeyBindingResolverService();
        pencilService = new PencilServiceStub(attributesManagerService, colorService, keyBindingService);
        const drawingTypeSpy = spyOnProperty(pencilService, 'drawingType', 'get');
        drawingType = DrawingType.Stroke;
        drawingTypeSpy.and.returnValue(drawingType);
        pencilService.setCanvasProperties(ctx);

        data = new DrawingPos();
        data.data = [
            { x: 50, y: 50 },
            { x: 51, y: 50 },
        ];

        TestBed.configureTestingModule({});
        service = TestBed.inject(PencilDrawingService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should call prepare draw on draw call', () => {
        const prepareDrawSpy = spyOn<any>(service, 'prepareDraw');
        const event = service.draw(ctx, data, pencilService as PencilService);
        expect(prepareDrawSpy).toHaveBeenCalledWith(ctx, event);
    });

    it('should call end draw on draw call', () => {
        const endDrawSpy = spyOn<any>(service, 'endDraw');
        service.draw(ctx, data, pencilService as PencilService);
        expect(endDrawSpy).toHaveBeenCalledWith(ctx, pencilService.drawingType);
    });

    it('should draw on canvas on draw call', () => {
        service.draw(ctx, data, pencilService as PencilService);
        const canvasData = Array.from(ctx.getImageData(data.data[0].x, data.data[0].y, 1, 1).data);
        expect(canvasData[0]).toEqual(0);
        expect(canvasData[1]).toEqual(0);
        expect(canvasData[2]).toEqual(0);
        expect(canvasData[3]).toBeGreaterThan(0);
    });

    it('should check if data is empty', () => {
        expect(service.isEmpty(new DrawingPos())).toEqual(true);
        expect(service.isEmpty({ data: [] } as DrawingPos)).toEqual(true);
        expect(service.isEmpty({ data: [{}] } as DrawingPos)).toEqual(false);
    });

    it('should check if data is in canvas', () => {
        const canvasSize = { x: 500, y: 500 };
        ResizeService['currentSize'] = canvasSize;
        expect(service.isInCanvas({ data: [{ x: 501, y: 501 }] } as DrawingPos)).toEqual(false);
        expect(service.isInCanvas({ data: [{ x: 499, y: 499 }] } as DrawingPos)).toEqual(true);
        expect(service.isInCanvas({ data: [{ x: -1, y: -1 }] } as DrawingPos)).toEqual(false);
    });
});
