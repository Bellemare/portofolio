import { TestBed } from '@angular/core/testing';
import { DrawingCanvasHelper } from '@app/classes/drawing-canvas-helper';
import { Color } from '@app/classes/drawing/color';
import { DrawingEvent } from '@app/classes/drawing/drawing-event';
import { DrawingText } from '@app/classes/drawing/drawing-text';
import { DrawingType } from '@app/classes/drawing/drawing-type';
import { Vec2 } from '@app/classes/drawing/vec2';
import { TextFonts } from '@app/classes/text-fonts.enum';
import { TextProperties } from '@app/classes/tool-properties/text-properties';
import { AttributesManagerService } from '@app/services/api/attributes-manager.service';
import { AutoSaveService } from '@app/services/api/auto-save.service';
import { KeyBindingResolverService } from '@app/services/key-binding/key-binding-resolver.service';
import { ResizeService } from '@app/services/resize/resize.service';
import { ColorService } from '@app/services/tools/color.service';
import { TextService } from '@app/services/tools/text.service';
import { UndoRedoService } from '@app/services/undo-redo/undo-redo.service';
import { TextDrawingService } from './text-drawing.service';

// tslint:disable:no-any
// tslint:disable:no-string-literal
// tslint:disable:no-magic-numbers
describe('TextDrawingService', () => {
    let service: TextDrawingService;

    let canvas: HTMLCanvasElement;
    let ctx: CanvasRenderingContext2D;
    let drawingText: DrawingText;
    let textProperties: TextProperties;

    let attributesManagerStub: AttributesManagerService;
    let colorServiceStub: ColorService;
    let keyBindingResolverServiceStub: KeyBindingResolverService;
    let resizeServiceStub: ResizeService;
    let undoRedoServiceStub: UndoRedoService;
    let autoSaveServiceStub: AutoSaveService;
    let textServiceStub: TextService;

    let event: DrawingEvent;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(TextDrawingService);

        canvas = document.createElement('canvas');
        canvas.width = 100;
        canvas.height = 100;
        ctx = canvas.getContext('2d') as CanvasRenderingContext2D;

        drawingText = new DrawingText(5, 5, ['ligne 1', 'ligne 2'], 0, 3);

        textProperties = {
            drawingType: DrawingType.Filled,
            font: TextFonts.TimesNewRoman,
            fontSize: 34,
            isBold: false,
            isItalic: false,
            alignment: 'left',
            baseline: 'middle',
        };

        attributesManagerStub = new AttributesManagerService();
        colorServiceStub = new ColorService(attributesManagerStub);
        keyBindingResolverServiceStub = new KeyBindingResolverService();
        undoRedoServiceStub = new UndoRedoService(keyBindingResolverServiceStub);
        autoSaveServiceStub = new AutoSaveService();
        resizeServiceStub = new ResizeService(undoRedoServiceStub, autoSaveServiceStub);
        textServiceStub = new TextService(attributesManagerStub, colorServiceStub, keyBindingResolverServiceStub, resizeServiceStub);

        event = new DrawingEvent();
        event.attributes = textProperties;
        event.canvasSize = { x: ctx.canvas.width, y: ctx.canvas.height };
        event.data = drawingText;
        event.drawingService = service;
        event.drawingType = DrawingType.Filled;
        const textColor: Color = { r: 1, g: 1, b: 1, a: 1 };
        event.fillColor = textColor;
        event.strokeColor = textColor;
        event.tool = textServiceStub;
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('drawEvent should call prepareDraw, drawText and endDraw if canvas is defined', () => {
        const prepareDrawSpy = spyOn<any>(service, 'prepareDraw').and.callThrough();
        const drawTextSpy = spyOn<any>(service, 'drawText').and.callThrough();
        const endDrawSpy = spyOn<any>(service, 'endDraw').and.callThrough();
        service.drawEvent(ctx, event);
        expect(prepareDrawSpy).toHaveBeenCalled();
        expect(drawTextSpy).toHaveBeenCalled();
        expect(endDrawSpy).toHaveBeenCalled();
    });

    it('drawEvent should call drawCursor if tool is editing text', () => {
        const drawCursorSpy = spyOn<any>(service, 'drawCursor').and.callThrough();
        spyOnProperty(event.tool as TextService, 'isEditing', 'get').and.returnValue(true);
        service.drawEvent(ctx, event);
        expect(drawCursorSpy).toHaveBeenCalled();
    });

    it('isEmpty should return correct value', () => {
        expect(service.isEmpty({} as DrawingText)).toEqual(true);
        expect(service.isEmpty({ x: 5 } as DrawingText)).toEqual(true);
        expect(service.isEmpty({ x: 5, y: 5 } as DrawingText)).toEqual(true);
        expect(service.isEmpty({ x: 5, y: 5, lines: ['sfdsf'] } as DrawingText)).toEqual(true);
        expect(service.isEmpty({ x: 5, y: 5, lines: ['sfdsf'], lineIndex: 0 } as DrawingText)).toEqual(true);
        expect(service.isEmpty({ x: 5, y: 5, lines: ['sfdsf'], lineIndex: 0, charIndex: 3 } as DrawingText)).toEqual(false);
        expect(service.isEmpty({ x: 5, y: 5, lines: [''], lineIndex: 0, charIndex: 0 } as DrawingText)).toEqual(true);
    });

    it('getLineBoundingBoxes should return one box per line', () => {
        expect(service.getLineBoundingBoxes(ctx, drawingText).length).toEqual(drawingText.lines.length);
    });

    it('isInCanvas should return true if a box is in canvas', () => {
        const previewCanvasHelperSpy = spyOnProperty(DrawingCanvasHelper, 'baseCanvas', 'get');
        previewCanvasHelperSpy.and.returnValue(canvas);
        const canvasSizeSpy = spyOnProperty(ResizeService, 'canvasSize', 'get');
        canvasSizeSpy.and.returnValue({ x: 100, y: 100 } as Vec2);
        const getLineBoundingBoxesSpy = spyOn(service, 'getLineBoundingBoxes');
        getLineBoundingBoxesSpy.and.returnValue([{ topLeft: { x: 0, y: 0 }, bottomRight: { x: 10, y: 10 } }]);
        expect(service.isInCanvas(drawingText)).toEqual(true);
    });

    it('isInCanvas should return false if no box is in canvas', () => {
        const previewCanvasHelperSpy = spyOnProperty(DrawingCanvasHelper, 'baseCanvas', 'get');
        previewCanvasHelperSpy.and.returnValue(canvas);
        const canvasSizeSpy = spyOnProperty(ResizeService, 'canvasSize', 'get');
        canvasSizeSpy.and.returnValue({ x: 100, y: 100 } as Vec2);
        const getLineBoundingBoxesSpy = spyOn(service, 'getLineBoundingBoxes');
        getLineBoundingBoxesSpy.and.returnValue([{ topLeft: { x: 101, y: 101 }, bottomRight: { x: 202, y: 202 } }]);
        expect(service.isInCanvas(drawingText)).toEqual(false);
    });
});
