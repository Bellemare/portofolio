import { TestBed } from '@angular/core/testing';
import { Color } from '@app/classes/drawing/color';
import { DrawingEvent } from '@app/classes/drawing/drawing-event';
import { DrawingRect } from '@app/classes/drawing/drawing-rect';
import { DrawingType } from '@app/classes/drawing/drawing-type';
import { RectangleProperties } from '@app/classes/tool-properties/rectangle-properties';
import { AttributesManagerService } from '@app/services/api/attributes-manager.service';
import { RectDrawingService } from '@app/services/drawing-services/rect-drawing.service';
import { KeyBindingResolverService } from '@app/services/key-binding/key-binding-resolver.service';
import { ResizeService } from '@app/services/resize/resize.service';
import { ColorService } from '@app/services/tools/color.service';
import { RectangleService } from '@app/services/tools/rectangle.service';

// tslint:disable:no-any
// tslint:disable:no-string-literal
// tslint:disable:no-magic-numbers
class RectangleServiceStub extends RectangleService {
    protected readonly DEFAULT_CONFIGURATION: RectangleProperties = {
        lineWidth: 1,
        drawingType: DrawingType.StrokeFilled,
    };

    get strokeColor(): Color {
        return { r: 64, g: 64, b: 64, a: 1 };
    }

    get fillColor(): Color {
        return { r: 32, g: 32, b: 32, a: 0.5 };
    }
}

describe('RectDrawingService', () => {
    let service: RectDrawingService;

    let canvas: HTMLCanvasElement;
    let ctx: CanvasRenderingContext2D;
    let rectAttributes: DrawingRect;

    let attributesManagerStub: AttributesManagerService;
    let colorServiceStub: ColorService;
    let keyBindingResolverServiceStub: KeyBindingResolverService;
    let rectangleServiceStub: RectangleServiceStub;

    let event: DrawingEvent;

    beforeEach(() => {
        canvas = document.createElement('canvas');
        canvas.width = 100;
        canvas.height = 100;
        ctx = canvas.getContext('2d') as CanvasRenderingContext2D;

        rectAttributes = { x: 0, y: 0, width: 50, height: 50, widthAdjustment: 0, heightAdjustment: 0 };

        attributesManagerStub = new AttributesManagerService();
        colorServiceStub = new ColorService(attributesManagerStub);
        keyBindingResolverServiceStub = new KeyBindingResolverService();

        rectangleServiceStub = new RectangleServiceStub(attributesManagerStub, colorServiceStub, keyBindingResolverServiceStub);
        rectangleServiceStub.setDefaultProperties();

        event = new DrawingEvent();
        event.attributes = rectangleServiceStub.toolAttributes;
        event.canvasSize = { x: ctx.canvas.width, y: ctx.canvas.height };
        event.data = rectAttributes;
        event.drawingService = service;
        event.drawingType = rectangleServiceStub.drawingType;
        event.fillColor = rectangleServiceStub.fillColor;
        event.strokeColor = rectangleServiceStub.strokeColor;
        event.tool = rectangleServiceStub;

        TestBed.configureTestingModule({ providers: [{ provide: RectangleService, useValue: RectangleServiceStub }] });
        service = TestBed.inject(RectDrawingService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('drawEvent should call prepareDraw', () => {
        const spy = spyOn<any>(service, 'prepareDraw');
        service.drawEvent(ctx, event);
        expect(spy).toHaveBeenCalled();
    });

    it('drawEvent should not call prepareDraw if canvas is undefined', () => {
        const spy = spyOn<any>(service, 'prepareDraw');
        service.drawEvent((undefined as unknown) as CanvasRenderingContext2D, event);
        expect(spy).not.toHaveBeenCalled();
    });

    it('if shouldFill is true, should change the fillStyle of the context to the correct color', () => {
        (event.data as DrawingRect).shouldFill = true;
        service.drawEvent(ctx, event);
        const fillStyleArray = ctx.fillStyle.toString().slice(1, 7);

        expect(parseInt(fillStyleArray.slice(0, 2), 16)).toEqual(rectangleServiceStub.strokeColor.r);
        expect(parseInt(fillStyleArray.slice(2, 4), 16)).toEqual(rectangleServiceStub.strokeColor.g);
        expect(parseInt(fillStyleArray.slice(4, 6), 16)).toEqual(rectangleServiceStub.strokeColor.b);
    });

    it('ctx should call rect in draw method', () => {
        const spy = spyOn(ctx, 'rect');
        service.drawEvent(ctx, event);
        expect(spy).toHaveBeenCalled();
    });

    it('drawEvent should call endDraw', () => {
        const spy = spyOn<any>(service, 'endDraw');
        service.drawEvent(ctx, event);
        expect(spy).toHaveBeenCalled();
    });

    it('RectDrawService should draw on the canvas', () => {
        ctx.lineWidth = 6;
        service.drawEvent(ctx, event);
        const topLeftCornerData = Array.from(ctx.getImageData(rectAttributes.x, rectAttributes.y, 1, 1).data);

        expect(topLeftCornerData[3]).toBeGreaterThan(0);
    });

    it('should check if data is empty', () => {
        expect(service.isEmpty(new DrawingRect())).toEqual(true);
        expect(service.isEmpty({ x: 5 } as DrawingRect)).toEqual(true);
        expect(service.isEmpty({ x: 5, y: 5 } as DrawingRect)).toEqual(true);
        expect(service.isEmpty({ x: 5, y: 5, width: 5 } as DrawingRect)).toEqual(true);
        expect(service.isEmpty({ x: 5, y: 5, width: 0, height: 5 } as DrawingRect)).toEqual(true);
        expect(service.isEmpty({ x: 5, y: 5, width: 5, height: 0 } as DrawingRect)).toEqual(true);
        expect(service.isEmpty({ x: 5, y: 5, width: 0, height: 0 } as DrawingRect)).toEqual(true);
        expect(service.isEmpty({ x: 5, y: 5, width: 5, height: 5 } as DrawingRect)).toEqual(false);
    });

    it('should check if data is in canvas', () => {
        const canvasSize = { x: 500, y: 500 };
        ResizeService['currentSize'] = canvasSize;
        expect(service.isInCanvas({ x: 505, y: 505, width: -6, height: -6 } as DrawingRect)).toEqual(true);
        expect(service.isInCanvas({ x: 505, y: 505, width: -4, height: -4 } as DrawingRect)).toEqual(false);
        expect(service.isInCanvas({ x: -5, y: -5, width: 4, height: 4 } as DrawingRect)).toEqual(false);
        expect(service.isInCanvas({ x: -5, y: -5, width: 6, height: 6 } as DrawingRect)).toEqual(true);
    });
});
