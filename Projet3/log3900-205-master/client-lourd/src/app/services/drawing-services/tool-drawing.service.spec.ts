import { TestBed } from '@angular/core/testing';
import { DrawingElement } from '@app/classes/drawing/drawing-element';
import { DrawingEvent } from '@app/classes/drawing/drawing-event';
import { DrawingType } from '@app/classes/drawing/drawing-type';
import { AttributesManagerService } from '@app/services/api/attributes-manager.service';
import { KeyBindingResolverService } from '@app/services/key-binding/key-binding-resolver.service';
import { ColorService } from '@app/services/tools/color.service';
import { ToolService } from '@app/services/tools/tool.service';
import { ToolDrawingService } from './tool-drawing.service';

class ToolStub extends ToolService {}

// tslint:disable:no-any
// tslint:disable:no-string-literal
// tslint:disable:no-magic-numbers
describe('ToolDrawingService', () => {
    let service: ToolDrawingService;

    let toolStub: ToolStub;

    let canvas: HTMLCanvasElement;
    let ctx: CanvasRenderingContext2D;

    let event: DrawingEvent;

    beforeEach(() => {
        canvas = document.createElement('canvas');
        canvas.width = 100;
        canvas.height = 100;
        ctx = canvas.getContext('2d') as CanvasRenderingContext2D;

        const attributesManagerService = new AttributesManagerService();
        const colorService = new ColorService(attributesManagerService);
        const keyBindingService = new KeyBindingResolverService();
        toolStub = new ToolStub(attributesManagerService, colorService, keyBindingService);
        toolStub.setCanvasProperties(ctx);
        event = new DrawingEvent();
        event.tool = toolStub;
        event.drawing_service = service;
        event.attributes = { drawingType: DrawingType.StrokeFilled };

        TestBed.configureTestingModule({});
        service = TestBed.inject(ToolDrawingService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should call the right ctx draw function depending on drawing type', () => {
        const ctxStrokeSpy = spyOn(ctx, 'stroke');
        const ctxFillSpy = spyOn(ctx, 'fill');

        service['endDraw'](ctx, DrawingType.Stroke);
        expect(ctxStrokeSpy).toHaveBeenCalled();

        service['endDraw'](ctx, DrawingType.Filled);
        expect(ctxFillSpy).toHaveBeenCalled();

        service['endDraw'](ctx, DrawingType.StrokeFilled);
        expect(ctxStrokeSpy).toHaveBeenCalled();
        expect(ctxFillSpy).toHaveBeenCalled();
    });

    it('should begin draw, set drawing color and set canvas properties when prepare draw called', () => {
        const beginDrawSpy = spyOn(ctx, 'beginPath');
        const strokeColorSpy = spyOnProperty(ctx, 'strokeStyle', 'set');
        const fillColorSpy = spyOnProperty(ctx, 'fillStyle', 'set');
        const setCanvasPropertiesSpy = spyOn(event.tool, 'setCanvasProperties');
        const color = { r: 0, g: 0, b: 0, a: 1 };
        const stringColor = `rgba(${color.r},${color.g},${color.b},${color.a})`;
        event.stroke_color = color;
        event.fill_color = color;

        service['prepareDraw'](ctx, event);
        expect(beginDrawSpy).toHaveBeenCalled();
        expect(strokeColorSpy).toHaveBeenCalledWith(stringColor);
        expect(fillColorSpy).toHaveBeenCalledWith(stringColor);
        expect(setCanvasPropertiesSpy).toHaveBeenCalledWith(ctx, event.attributes);
    });

    it('should return default empty value', () => {
        expect(service.isEmpty({} as DrawingElement)).toEqual(false);
    });

    it('should return default in canvas value', () => {
        expect(service.isInCanvas({} as DrawingElement)).toEqual(false);
    });

    it('draw should setup drawing event and call drawEvent', () => {
        const setupDrawingEventSpy = spyOn<any>(service, 'setupDrawingEvent');
        setupDrawingEventSpy.and.returnValue(event);
        const drawEventSpy = spyOn<any>(service, 'drawEvent');
        drawEventSpy.and.returnValue(event);
        service.draw(ctx, {} as DrawingElement, {} as ToolService);
        expect(setupDrawingEventSpy).toHaveBeenCalled();
        expect(drawEventSpy).toHaveBeenCalledWith(ctx, event);
    });

    it('drawEvent should return event unchanged', () => {
        const returnedEvent = service.drawEvent(ctx, event);
        expect(returnedEvent).toEqual(event);
    });
});
