import { Injectable } from '@angular/core';
import { DrawingEvent } from '@app/classes/drawing/drawing-event';
import { DrawingPos } from '@app/classes/drawing/drawing-pos';
import { Vec2 } from '@app/classes/drawing/vec2';
import { CanvasSizeService } from '../canvas-size/canvas-size.service';
import { ToolDrawingService } from './tool-drawing.service';
import { Box } from '@app/classes/drawing/box';
import { BoundingBox } from '@app/classes/drawing/bounding-box';
import { PencilProperties } from '@app/classes/tool-properties/pencil-properties';

@Injectable({
    providedIn: 'root',
})
export class PencilDrawingService extends ToolDrawingService {
    readonly BOX_PADDING: number = 5;

    drawEvent(ctx: CanvasRenderingContext2D, event: DrawingEvent): DrawingEvent {
        const data = event.data as DrawingPos;
        const path = data.data;
        if (ctx !== undefined && !this.isEmpty(data) && path.length > 1) {
            const attributes = event.attributes as PencilProperties;
            ctx.lineWidth = data.getTransformedLineWidth(attributes);

            const radius = ctx.lineWidth / 2;
            const baseBox: Box = { xMax: 0, yMax: 0, xMin: CanvasSizeService.canvasSize.x, yMin: CanvasSizeService.canvasSize.y };
            const transformedBox: Box = { xMax: 0, yMax: 0, xMin: CanvasSizeService.canvasSize.x, yMin: CanvasSizeService.canvasSize.y };
            this.prepareDraw(ctx, event);
            for (let i = 0; i < path.length - 1; i++) {
                const p1: Vec2 = path[i];
                const p2: Vec2 = path[i + 1];
                this.selectBoxPoint(baseBox, p1, p2, radius);
                this.selectBoxPoint(transformedBox, p1, p2, radius);
                const midPoint: Vec2 = this.getMidPoint(p1, p2);
                ctx.quadraticCurveTo(p1.x, p1.y, midPoint.x, midPoint.y);
            }
            
            if (data.base_bounding_box == undefined) {
                data.base_bounding_box = this.setBoundingBox(baseBox);
            }
            if (data.bounding_box == undefined) {
                data.bounding_box = this.setBoundingBox(transformedBox);
            }
            this.endDraw(ctx, event.drawing_type);
        }
        return event;
    }

    private selectBoxPoint(box: Box, p1: Vec2, p2: Vec2, radius: number): void {
        box.xMin = Math.min(box.xMin, p1.x - radius - this.BOX_PADDING, p2.x - radius - this.BOX_PADDING);
        box.yMin = Math.min(box.yMin, p1.y - radius - this.BOX_PADDING, p2.y - radius - this.BOX_PADDING);
        box.xMax = Math.max(box.xMax, p1.x + radius + this.BOX_PADDING, p2.x + radius + this.BOX_PADDING);
        box.yMax = Math.max(box.yMax, p1.y + radius + this.BOX_PADDING, p2.y + radius + this.BOX_PADDING);
    }

    private setBoundingBox(box: Box): BoundingBox {
        return {
            x: Math.floor(box.xMin),
            y: Math.floor(box.yMin),
            w: Math.ceil(box.xMax - box.xMin),
            h: Math.ceil(box.yMax - box.yMin),
        };
    }

    isEmpty(data: DrawingPos): boolean {
        return this.isEmptyDrawingPos(data);
    }

    isInCanvas(data: DrawingPos): boolean {
        return this.isInCanvasDrawingPos(data);
    }

    private getMidPoint(p1: Vec2, p2: Vec2): Vec2 {
        return {
            x: (p1.x + p2.x) / 2,
            y: (p1.y + p2.y) / 2,
        };
    }
}
