import { TestBed } from '@angular/core/testing';
import { Color } from '@app/classes/drawing/color';
import { DrawingEllipse } from '@app/classes/drawing/drawing-ellipse';
import { DrawingEvent } from '@app/classes/drawing/drawing-event';
import { DrawingType } from '@app/classes/drawing/drawing-type';
import { EllipseProperties } from '@app/classes/tool-properties/ellipse-properties';
import { AttributesManagerService } from '@app/services/api/attributes-manager.service';
import { KeyBindingResolverService } from '@app/services/key-binding/key-binding-resolver.service';
import { ResizeService } from '@app/services/resize/resize.service';
import { ColorService } from '@app/services/tools/color.service';
import { EllipseService } from '@app/services/tools/ellipse.service';
import { EllipseDrawingService } from './ellipse-drawing.service';

// tslint:disable:no-any
// tslint:disable:no-string-literal
// tslint:disable:no-magic-numbers
// tslint:disable:prefer-const
class EllipseServiceStub extends EllipseService {
    protected readonly DEFAULT_CONFIGURATION: EllipseProperties = {
        lineWidth: 1,
        drawingType: DrawingType.StrokeFilled,
    };
}

describe('EllipseDrawingService', () => {
    let service: EllipseDrawingService;

    let canvas: HTMLCanvasElement;
    let ctx: CanvasRenderingContext2D;
    let ellipseAttributes: DrawingEllipse;

    let attributesManagerStub: AttributesManagerService;
    let colorServiceStub: ColorService;
    let keyBindingResolverServiceStub: KeyBindingResolverService;
    let ellipseServiceStub: EllipseServiceStub;

    let defaultStrokeColor: Color;
    let defaultFillColor: Color;

    let event: DrawingEvent;

    beforeEach(() => {
        canvas = document.createElement('canvas');
        canvas.width = 100;
        canvas.height = 100;
        ctx = canvas.getContext('2d') as CanvasRenderingContext2D;

        ellipseAttributes = {
            x: 25,
            y: 50,
            radiusX: 20,
            radiusY: 30,
            rotation: 0,
            startAngle: 0,
            endAngle: 2 * Math.PI,
            anticlockwise: false,
        };

        attributesManagerStub = new AttributesManagerService();
        colorServiceStub = new ColorService(attributesManagerStub);
        keyBindingResolverServiceStub = new KeyBindingResolverService();

        defaultStrokeColor = { r: 0, g: 0, b: 0, a: 1 };
        defaultFillColor = { r: 0, g: 0, b: 0, a: 1 };

        ellipseServiceStub = new EllipseServiceStub(attributesManagerStub, colorServiceStub, keyBindingResolverServiceStub);
        ellipseServiceStub.setDefaultProperties();
        ellipseServiceStub['primaryColor'] = defaultFillColor;
        ellipseServiceStub['secondaryColor'] = defaultStrokeColor;

        event = new DrawingEvent();
        event.attributes = ellipseServiceStub.toolAttributes;
        event.canvasSize = { x: ctx.canvas.width, y: ctx.canvas.height };
        event.data = ellipseAttributes;
        event.drawingService = service;
        event.drawingType = ellipseServiceStub.drawingType;
        event.fillColor = ellipseServiceStub.fillColor;
        event.strokeColor = ellipseServiceStub.strokeColor;
        event.tool = ellipseServiceStub;

        TestBed.configureTestingModule({ providers: [{ provide: EllipseService, useValue: EllipseServiceStub }] });
        service = TestBed.inject(EllipseDrawingService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('drawEvent should call prepareDraw', () => {
        const spy = spyOn<any>(service, 'prepareDraw');
        service.drawEvent(ctx, event);
        expect(spy).toHaveBeenCalled();
    });

    it('drawEvent should not call prepareDraw if canvas is undefined', () => {
        const spy = spyOn<any>(service, 'prepareDraw');
        service.drawEvent((undefined as unknown) as CanvasRenderingContext2D, event);
        expect(spy).not.toHaveBeenCalled();
    });

    it('ctx should call ellipse in draw method', () => {
        const spy = spyOn(ctx, 'ellipse');
        service.drawEvent(ctx, event);
        expect(spy).toHaveBeenCalled();
    });

    it('draw should call endDraw', () => {
        const spy = spyOn<any>(service, 'endDraw');
        service.drawEvent(ctx, event);
        expect(spy).toHaveBeenCalled();
    });

    it('ellipse should be called with the unaltered attributes of the passed DrawingEllipse data when drawing type is filled', () => {
        event.drawingType = DrawingType.Filled;
        const spy = spyOn(ctx, 'ellipse');
        service.drawEvent(ctx, event);
        expect(spy).toHaveBeenCalledWith(
            ellipseAttributes.x,
            ellipseAttributes.y,
            ellipseAttributes.radiusX,
            ellipseAttributes.radiusY,
            ellipseAttributes.rotation,
            ellipseAttributes.startAngle,
            ellipseAttributes.endAngle,
            ellipseAttributes.anticlockwise,
        );
    });

    it('adjusted radii should be computed correctly when drawing type is not filled', () => {
        const expectedRadiusX = 10;
        const expectedRadiusY = 20;

        event.drawingType = DrawingType.Stroke;
        event.attributes.lineWidth = 75;
        const spy = spyOn(ctx, 'ellipse');
        service.drawEvent(ctx, event);

        expect(spy).toHaveBeenCalledWith(
            ellipseAttributes.x,
            ellipseAttributes.y,
            expectedRadiusX,
            expectedRadiusY,
            ellipseAttributes.rotation,
            ellipseAttributes.startAngle,
            ellipseAttributes.endAngle,
            ellipseAttributes.anticlockwise,
        );
    });

    it('should check if data is empty', () => {
        expect(service.isEmpty({} as DrawingEllipse)).toEqual(true);
        expect(service.isEmpty({ x: 5 } as DrawingEllipse)).toEqual(true);
        expect(service.isEmpty({ x: 5, y: 5 } as DrawingEllipse)).toEqual(true);
        expect(service.isEmpty({ x: 5, y: 5, radiusX: 5 } as DrawingEllipse)).toEqual(true);
        expect(service.isEmpty({ x: 5, y: 5, radiusX: 5, radiusY: 5 } as DrawingEllipse)).toEqual(true);
        expect(service.isEmpty({ x: 5, y: 5, radiusX: 5, radiusY: 5, rotation: 5 } as DrawingEllipse)).toEqual(true);
        expect(service.isEmpty({ x: 5, y: 5, radiusX: 5, radiusY: 5, rotation: 5, startAngle: 5 } as DrawingEllipse)).toEqual(true);
        expect(service.isEmpty({ x: 5, y: 5, radiusX: 0, radiusY: 0, rotation: 5, startAngle: 5, endAngle: 5 } as DrawingEllipse)).toEqual(true);
        expect(
            service.isEmpty({ x: 5, y: 5, radiusX: 5, radiusY: 5, rotation: 5, startAngle: 5, endAngle: 5, anticlockwise: false } as DrawingEllipse),
        ).toEqual(false);
    });

    it('should check if data is in canvas', () => {
        const canvasSize = { x: 500, y: 500 };
        ResizeService['currentSize'] = canvasSize;
        expect(service.isInCanvas({ x: 505, y: 505, radiusX: 6, radiusY: 6 } as DrawingEllipse)).toEqual(true);
        expect(service.isInCanvas({ x: 505, y: 505, radiusX: 4, radiusY: 4 } as DrawingEllipse)).toEqual(false);
        expect(service.isInCanvas({ x: -5, y: -5, radiusX: 4, radiusY: 4 } as DrawingEllipse)).toEqual(false);
        expect(service.isInCanvas({ x: -5, y: -5, radiusX: 6, radiusY: 6 } as DrawingEllipse)).toEqual(true);
    });
});
