import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { DrawingService } from './components/drawing/drawing.service';
import { PencilService } from './tools/pencil-service';
import { ApiHttpInterceptor } from '@app/services/api/api-http-interceptor.service';
import { AuthService } from './api/auth.service';
import { RoomService } from '@app/services/api/room.service';
import { AlbumApiService } from '@app/services/api/album-api.service';
import { CouchesService } from '@app/services/couches/couches.service';
import { ContactApiService } from '@app/services/api/contact-api-service';
import { ElectronService } from '@app/services/api/electron.service';
import { ExpositionApiService } from '@app/services/api/expositions-api.service';
import { AudioService } from '@app/services/audio/audio.service';

@NgModule({
    declarations: [],
    imports: [CommonModule],
    providers: [
        DrawingService, 
        PencilService, 
        ApiHttpInterceptor,
        AuthService,
        RoomService,
        AlbumApiService,
        CouchesService,
        ContactApiService,
        ElectronService,
        ExpositionApiService,
        AudioService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ApiHttpInterceptor,
            multi: true,
        },
    ],
})
export class ServicesModule {}
