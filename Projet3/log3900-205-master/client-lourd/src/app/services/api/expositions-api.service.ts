import { Injectable } from '@angular/core';
import { ApiResponse } from '@app/classes/api/api-response';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
    providedIn: 'root',
})
export class ExpositionApiService extends ApiService {
    readonly BASE_URL: string = '/Exposition';

    addDrawingToExposition(id: number, drawingId: number): Observable<ApiResponse> {
        return this.post({
            url: `${this.BASE_URL}/${id}`
        }, {
            drawing_id: drawingId
        });
    }

    removeDrawingFromExposition(id: number, drawingId: number): Observable<ApiResponse> {
        return this.delete({
            url: `${this.BASE_URL}/${id}/Drawing/${drawingId}`
        });
    }

    getAll(): Observable<ApiResponse> {
        return this.get({
            url: this.BASE_URL
        });
    }

    getExposition(albumId: number): Observable<ApiResponse> {
        return this.get({
            url: `${this.BASE_URL}/${albumId}`
        });
    }
}
