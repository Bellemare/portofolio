import { Injectable } from '@angular/core';
import { DrawingCanvasHelper } from '@app/classes/drawing-canvas-helper';
import { LocalApiService } from './local-api.service';

@Injectable({
    providedIn: 'root',
})
export class AutoSaveService extends LocalApiService {
    private readonly SAVE_KEY: string = 'drawing_state';
    private readonly IMAGE_MIME_TYPE: string = 'image/png';

    saveDrawingState(): void {
        const canvas = DrawingCanvasHelper.baseCanvas;
        const image = canvas.toDataURL(this.IMAGE_MIME_TYPE);
        this.saveToLocalStorage(this.SAVE_KEY, image);
    }

    retrieveDrawingState(): string | null {
        return this.parseObj<string>(this.getFromLocalStorage(this.SAVE_KEY));
    }

    deleteDrawingState(): void {
        this.deleteFromStorage(this.SAVE_KEY);
    }

    get hasDrawingSaved(): boolean {
        return this.retrieveDrawingState() !== null;
    }
}
