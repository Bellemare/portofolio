import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiResponse } from '@app/classes/api/api-response';
import { RegisterRequest } from '@app/classes/api/register-request';
import { User } from '@app/classes/api/user';
import { NotificationService } from '@app/services/notification/notification.service';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
    providedIn: 'root',
})
export class UserApiService extends ApiService {
    readonly BASE_URL: string = '/User';

    constructor(protected http: HttpClient, protected notificationService: NotificationService) {
        super(http, notificationService);
    }

    getCurrentUser(): Observable<ApiResponse> {
        return this.get({
            url: `${this.BASE_URL}/Current`
        });
    }

    getDefaultAvatars(): Observable<ApiResponse> {
        return this.get({
            url: `${this.BASE_URL}/DefaultAvatars`
        });
    }

    getUserAvatar(userId: number): Observable<ApiResponse> {
        return this.get({
            url: `${this.BASE_URL}/${userId}/Avatar`
        });
    }

    getUser(id: number): Observable<ApiResponse> {
        return this.get({
            url: `${this.BASE_URL}/${id}`
        });
    }

    getConnectionHistory(): Observable<ApiResponse> {
        return this.get({
            url: `${this.BASE_URL}/ConnectionHistory`
        });
    }

    getCollaborationSessionHistory(): Observable<ApiResponse> {
        return this.get({
            url: `${this.BASE_URL}/CollaborativeSessions`
        });
    }

    getUserMetrics(): Observable<ApiResponse> {
        return this.get({
            url: `${this.BASE_URL}/Metrics`
        });
    }

    register(req: RegisterRequest): Observable<ApiResponse> {
        return this.post({
            url: this.BASE_URL
        }, req);
    }

    updateUser(user: User): Observable<ApiResponse> {
        return this.put({
            url: `${this.BASE_URL}/${user.id}`,
            handleSuccess: false,
        }, user);
    }
}
