import { HttpClient, HttpClientModule, HttpErrorResponse } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { ApiResponse } from '@app/classes/api/api-response';
import { RequestConfig } from '@app/classes/api/request-config';
import { Message } from '@common/communication/message';
import { Observable, of } from 'rxjs';
import { ApiService } from './api.service';

// tslint:disable:no-string-literal
describe('ApiService', () => {
    let service: ApiService;

    let httpclientSpy: jasmine.SpyObj<HttpClient>;
    let testUrl: string;
    let defaultUri: string;
    let apiResponse: Observable<ApiResponse>;
    let defaultRequestConfig: RequestConfig;

    beforeEach(() => {
        httpclientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);

        TestBed.configureTestingModule({
            imports: [HttpClientModule],
            providers: [{ provide: HttpClient, useValue: httpclientSpy }],
        });
        service = TestBed.inject(ApiService);
        apiResponse = of<ApiResponse>();
        testUrl = '/test';
        defaultUri = `${service['DEFAULT_HOST']}:${service['DEFAULT_PORT']}${service['DEFAULT_ENDPOINT']}${testUrl}`;
        defaultRequestConfig = { url: testUrl };
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should handle get request properly', () => {
        // La méthode est protected, donc on doit mettre any pour spy.
        // tslint:disable-next-line:no-any
        const constructUrlSpy = spyOn<any>(service, 'constructUrl').and.callThrough();
        httpclientSpy.get.and.returnValue(apiResponse);
        service['get'](defaultRequestConfig);
        expect(httpclientSpy.get).toHaveBeenCalledWith(defaultUri);
        expect(constructUrlSpy).toHaveBeenCalled();
    });

    it('should handle post request properly', () => {
        // La méthode est protected, donc on doit mettre any pour spy.
        // tslint:disable-next-line:no-any
        const constructUrlSpy = spyOn<any>(service, 'constructUrl').and.callThrough();
        const data = { value: 'test' };
        httpclientSpy.post.and.returnValue(apiResponse);
        service['post'](defaultRequestConfig, data);
        expect(httpclientSpy.post).toHaveBeenCalledWith(defaultUri, data);
        expect(constructUrlSpy).toHaveBeenCalled();
    });

    it('should handle delete request properly', () => {
        // La méthode est protected, donc on doit mettre any pour spy.
        // tslint:disable-next-line:no-any
        const constructUrlSpy = spyOn<any>(service, 'constructUrl').and.callThrough();
        httpclientSpy.delete.and.returnValue(apiResponse);
        service['delete'](defaultRequestConfig);
        expect(httpclientSpy.delete).toHaveBeenCalledWith(defaultUri);
        expect(constructUrlSpy).toHaveBeenCalled();
    });

    it('should construct url properly', () => {
        expect(service['constructUrl'](defaultRequestConfig)).toEqual(defaultUri);

        const config: RequestConfig = {
            url: '/testing',
            host: 'http://10.0.0.1',
            port: 8000,
            endpoint: '/apitest',
        };
        const expectedUrl = `${config.host}:${config.port}${config.endpoint}${config.url}`;
        expect(service['constructUrl'](config)).toEqual(expectedUrl);
    });

    it('should handle success and return response properly', () => {
        const notificationSpy = spyOn(service['notificationService'], 'setNotification');
        const response: Message = { title: 'Test', body: 'Testing' };
        expect(service['handleSuccess'](response)).toEqual(response);
        expect(notificationSpy).toHaveBeenCalledWith({
            title: response.title,
            message: response.body,
            code: 200,
        });
    });

    it('should send error notification properly', () => {
        const notificationSpy = spyOn(service['notificationService'], 'setNotification');
        const error: Message = { title: 'Erreur', body: 'Testing' };
        const code = 400;
        service['sendErrorNotificationMessage'](error, code);
        expect(notificationSpy).toHaveBeenCalledWith({
            title: error.title,
            message: error.body,
            code,
        });
    });

    it('should handle error properly', () => {
        // La méthode est protected, donc on doit mettre any pour spy.
        // tslint:disable-next-line:no-any
        const notificationSpy = spyOn<any>(service, 'sendErrorNotificationMessage');
        const error = { error: {} as Message } as HttpErrorResponse;
        expect((): void => {
            service['handleError'](error);
            expect(notificationSpy).toHaveBeenCalledWith(error.error);
        }).toThrow(error);
    });
});
