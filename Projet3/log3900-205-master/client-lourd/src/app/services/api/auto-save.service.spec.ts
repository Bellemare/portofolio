import { TestBed } from '@angular/core/testing';
import { DrawingCanvasHelper } from '@app/classes/drawing-canvas-helper';
import { AutoSaveService } from './auto-save.service';

// tslint:disable:no-string-literal
// tslint:disable:no-any
describe('AutoSaveService', () => {
    let service: AutoSaveService;

    let canvas: HTMLCanvasElement;
    let url: string;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(AutoSaveService);
        canvas = document.createElement('canvas');
        DrawingCanvasHelper.baseCanvas = canvas;
        url = canvas.toDataURL('image/png');
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should save drawing state to local storage', () => {
        const saveSpy = spyOn(service, 'saveToLocalStorage');
        service.saveDrawingState();
        expect(saveSpy).toHaveBeenCalledWith(service['SAVE_KEY'], url);
    });

    it('should retrieve saved drawing state', () => {
        spyOn(service, 'getFromLocalStorage').and.returnValue(JSON.stringify(url));
        expect(service.retrieveDrawingState()).toEqual(url);
    });

    it('should delete saved drawing state', () => {
        const deleteSpy = spyOn<any>(service, 'deleteFromStorage');

        service.deleteDrawingState();
        expect(deleteSpy).toHaveBeenCalledWith(service['SAVE_KEY']);
    });

    it('should check if drawing state is saved', () => {
        const retrieveSpy = spyOn(service, 'retrieveDrawingState');

        retrieveSpy.and.returnValue(null);
        expect(service.hasDrawingSaved).toEqual(false);

        retrieveSpy.and.returnValue(url);
        expect(service.hasDrawingSaved).toEqual(true);
    });
});
