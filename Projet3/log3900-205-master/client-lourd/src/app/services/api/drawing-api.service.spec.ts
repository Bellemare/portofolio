import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { ApiResponse } from '@app/classes/api/api-response';
import { DrawingSave } from '@app/classes/api/drawing-save';
import { ImageFormat } from '@app/classes/api/image-format';
import { Message } from '@common/communication/message';
import { of } from 'rxjs';
import { DrawingApiService } from './drawing-api.service';

// tslint:disable:no-any
// tslint:disable:no-string-literal
describe('DrawingApiService', () => {
    let service: DrawingApiService;
    let httpSpy: jasmine.SpyObj<HttpClient>;

    let jsonParseSpy: jasmine.SpyObj<JSON>;

    beforeEach(() => {
        httpSpy = jasmine.createSpyObj('HttpClient', ['get', 'post']);
        jsonParseSpy = jasmine.createSpyObj('JSON', ['parse']);

        TestBed.configureTestingModule({
            providers: [
                { provide: HttpClient, useValue: httpSpy },
                { provide: JSON, useValue: jsonParseSpy },
            ],
        });
        service = TestBed.inject(DrawingApiService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should call post on drawing save', () => {
        // Ici, on doit mettre any, puisque la méthode n'est pas publique
        // tslint:disable-next-line:no-any
        const postSpy = spyOn<any>(service, 'post');
        const canvas = document.createElement('canvas');
        service.saveDrawing({ name: 'test' } as DrawingSave, canvas);
        expect(postSpy).toHaveBeenCalled();
    });

    it('should call get on getDrawings call', () => {
        // Ici, on doit mettre any, puisque la méthode n'est pas publique
        // tslint:disable-next-line:no-any
        const getSpy = spyOn<any>(service, 'get');
        service.getDrawings();
        expect(getSpy).toHaveBeenCalled();
    });

    it('should create get request on getDrawing', () => {
        // Ici, on doit mettre any, puisque la méthode n'est pas publique
        // tslint:disable-next-line:no-any
        const constructUrlSpy = spyOn<any>(service, 'constructUrl');
        httpSpy.get.and.returnValue(of<ApiResponse>());
        service.getDrawing('1');
        expect(constructUrlSpy).toHaveBeenCalled();
        expect(httpSpy.get).toHaveBeenCalled();
    });

    it('should create get request on verifyExists', () => {
        // Ici, on doit mettre any, puisque la méthode n'est pas publique
        // tslint:disable-next-line:no-any
        const getSpy = spyOn<any>(service, 'get');
        service.verifyDrawingExists('1');
        expect(getSpy).toHaveBeenCalled();
    });

    it('should create delete request on deleteDrawing', () => {
        // Ici, on doit mettre any, puisque la méthode n'est pas publique
        // tslint:disable-next-line:no-any
        const deleteSpy = spyOn<any>(service, 'delete');
        service.deleteDrawing('1');
        expect(deleteSpy).toHaveBeenCalled();
    });

    it('should handle connection error correctly', async (done: DoneFn) => {
        const result = { status: 0 } as HttpErrorResponse;
        const notifSpy = spyOn<any>(service, 'sendErrorNotificationMessage');
        await service['handleBlobError'](result).catch((err) => {
            expect(err).toBeInstanceOf(Object);
            expect(notifSpy).toHaveBeenCalled();
            done();
        });
    });

    it('should export image to imgur correctly', () => {
        const img = document.createElement('canvas');
        const format = ImageFormat.PNG;

        const dataUrlSpy = spyOn(img, 'toDataURL').and.callThrough();
        httpSpy.post.and.returnValue(of());
        service.exportToImgur(img, format);
        expect(httpSpy.post).toHaveBeenCalled();
        expect(dataUrlSpy).toHaveBeenCalledWith('image/png');
    });

    it('should handle imgur error', () => {
        const notificationSpy = spyOn<any>(service, 'sendErrorNotificationMessage');
        const error = { error: {} as Message } as HttpErrorResponse;
        expect((): void => {
            service['handleImgurError'](error);
            expect(notificationSpy).toHaveBeenCalled();
        }).toThrow(error);
    });
});
