import { TestBed } from '@angular/core/testing';
import { AutoSaveService } from './auto-save.service';

// tslint:disable:no-string-literal
describe('LocalApiService', () => {
    let service: AutoSaveService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(AutoSaveService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should get from local storage', () => {
        const getSpy = spyOn(localStorage, 'getItem');
        service.getFromLocalStorage('test');
        expect(getSpy).toHaveBeenCalledWith('test');
    });

    it('should save to local storage', () => {
        const saveSpy = spyOn(localStorage, 'setItem');
        service.saveToLocalStorage('test', { test: 1 });
        expect(saveSpy).toHaveBeenCalledWith('test', JSON.stringify({ test: 1 }));
    });

    it('should delete from local storage', () => {
        const deleteSpy = spyOn(localStorage, 'removeItem');
        service['deleteFromStorage']('test');
        expect(deleteSpy).toHaveBeenCalledWith('test');
    });

    it('should parse item', () => {
        const obj = { test: 1 };
        expect(service['parseObj'](JSON.stringify(obj))).toEqual(obj);
    });
});
