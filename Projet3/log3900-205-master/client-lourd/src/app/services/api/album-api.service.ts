import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Album } from '@app/classes/api/album';
import { ApiResponse } from '@app/classes/api/api-response';
import { NotificationService } from '@app/services/notification/notification.service';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';
import { DrawingsFilterRequest } from '@app/classes/api/drawings-filter-request';

@Injectable({
    providedIn: 'root',
})
export class AlbumApiService extends ApiService {
    readonly BASE_URL: string = '/Album';

    constructor(protected http: HttpClient, protected notificationService: NotificationService) {
        super(http, notificationService);
    }

    getDrawings(albumId: number): Observable<ApiResponse> {
        return this.get({
            url: `${this.BASE_URL}/${albumId}/Drawings`
        });
    }

    filterDrawings(albumId: number, filter: DrawingsFilterRequest): Observable<ApiResponse> {
        return this.post({
            url: `${this.BASE_URL}/${albumId}/Drawings/Filter`
        }, filter);
    }

    getUserAlbums(): Observable<ApiResponse> {
        return this.get({
            url: `${this.BASE_URL}/JoinedAlbums`
        });
    }

    getAlbumList(): Observable<ApiResponse> {
        return this.get({
            url: this.BASE_URL
        });
    }

    deleteAlbum(albumId: number): Observable<ApiResponse> {
        return this.delete({
            url: `${this.BASE_URL}/${albumId}`
        });
    }

    quitAlbum(albumId: number): Observable<ApiResponse> {
        return this.post({
            url: `${this.BASE_URL}/${albumId}/Leave`
        });
    }

    requestToJoinAlbum(albumId: number): Observable<ApiResponse> {
        return this.post({
            url: `${this.BASE_URL}/${albumId}/RequestToJoin`
        });
    }

    acceptJoinRequest(albumId: number, requestId: number): Observable<ApiResponse> {
        return this.post({
            url: `${this.BASE_URL}/${albumId}/JoinRequest/${requestId}/Accept`,
            handleSuccess: false,
        });
    }

    declineJoinRequest(albumId: number, requestId: number): Observable<ApiResponse> {
        return this.post({
            url: `${this.BASE_URL}/${albumId}/JoinRequest/${requestId}/Decline`,
            handleSuccess: false,
        });
    }

    getAllJoinRequests(): Observable<ApiResponse> {
        return this.get({
            url: `${this.BASE_URL}/PendingJoinRequests`
        });
    }

    create(req: Album): Observable<ApiResponse> {
        return this.post({
            url: this.BASE_URL
        }, req);
    }

    update(req: Album): Observable<ApiResponse> {
        return this.put({
            url: `${this.BASE_URL}/${req.id}`
        }, req);
    }

    getAlbum(albumId: number): Observable<ApiResponse> {
        return this.get({
            url: `${this.BASE_URL}/${albumId}`
        });
    }
}
