import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiResponse } from '@app/classes/api/api-response';
import { DrawingModel } from '@app/classes/api/drawing-model';
import { SocketClient } from '@app/classes/socket/socket-client';
import { NotificationService } from '@app/services/notification/notification.service';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';
import { AuthService } from './auth.service';
import { DrawingEdit } from '@app/classes/api/drawing-edit';
import { PasswordVerifyRequest } from '@app/classes/api/password-verify-request';

@Injectable({
    providedIn: 'root',
})
export class DrawingApiService extends ApiService {
    readonly BASE_URL: string = '/Drawing';

    private activeSessions: Map<number, SocketClient> = new Map<number, SocketClient>();

    constructor(protected http: HttpClient, protected notificationService: NotificationService, private authService: AuthService) {
        super(http, notificationService);
    }

    joinCollaborativeSession(drawingId: number): Promise<SocketClient> {
        const promise = new Promise<SocketClient>((resolve, reject) => {
            const token = this.authService.isAuthenticated() ? this.authService.getAccessToken().token:"";

            const conn = new SocketClient();
            conn.onClose = () => {
                reject();
            }
            let url = `${this.BASE_URL}/${drawingId}/JoinSession/${token}`;
            conn.connect(drawingId, url, () => {
                conn.onClose = this.leaveSession.bind(this, drawingId);
                this.activeSessions.set(drawingId, conn);
                resolve(conn);
            });
        });

        return promise;
    }

    leaveSession(drawingId: number): void {
        const room = this.activeSessions.get(drawingId);
        if (room !== undefined) {
            room.closeConnection();
            this.activeSessions.delete(drawingId);
        }
    }

    getDrawing(id: number): Observable<ApiResponse> {
        return this.get({
            url: `${this.BASE_URL}/${id}`
        });
    }

    smartFilter(value: string, albumId: number): Observable<ApiResponse> {
        return this.post({
            url: `${this.BASE_URL}/SmartFilter`
        }, { 
            "search_value": value,
            "album_id": albumId,
        });
    }

    create(drawing: DrawingModel): Observable<ApiResponse> {
        return this.post({
            url: this.BASE_URL
        }, drawing);
    }

    updateDrawing(drawing: DrawingEdit): Observable<ApiResponse> {
        return this.put({
            url: `${this.BASE_URL}/${drawing.id}`,
            handleSuccess: false,
        }, drawing);
    }

    verifyPassword(req: PasswordVerifyRequest): Observable<ApiResponse> {
        return this.post({
            url: `${this.BASE_URL}/${req.drawing_id}/VerifyPassword`,
            handleSuccess: false,
        }, req);
    }

    deleteDrawing(drawingId: number): Observable<ApiResponse> {
        return this.delete({
            url: `${this.BASE_URL}/${drawingId}`
        });
    }
}
