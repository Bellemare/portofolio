import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpParams, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { AuthService } from "@app/services/api/auth.service";
import { catchError } from "rxjs/operators";
import { ApiResponse } from "@app/classes/api/api-response";
import { Router } from "@angular/router";

@Injectable()
export class ApiHttpInterceptor implements HttpInterceptor {

    constructor(private authService: AuthService, private router: Router) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(this.cloneRequest(req)).pipe(catchError(this.handleError.bind(this)));
    }

    protected handleError(result: HttpErrorResponse): (error: HttpErrorResponse) => Observable<ApiResponse> {
        if (result.status == 401) {
            this.authService.deleteToken();
            this.router.navigateByUrl("/login");
        }
        throw result;
    }

    private cloneRequest(req: HttpRequest<any>): HttpRequest<any> {
        let headers = req.headers;
        if (this.authService.getAccessToken() != undefined) {
            headers = req.headers.set("Authorization", `Bearer ${this.authService.getAccessToken().token}`);
        }
        return req.clone({
            headers: headers,
            params: new HttpParams
        });
    }
}
