import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { NotificationService } from '@app/services/notification/notification.service';
import { LoginRequest } from '@app/classes/api/login-request';
import { AuthToken } from '@app/classes/api/auth-token';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';
import { ApiResponse } from '@app/classes/api/api-response';
import { map } from 'rxjs/operators';
import { LocalApiService } from './local-api.service';
import * as moment from 'moment';
import { UserApiService } from './user-api.service';
import { User } from '@app/classes/api/user';

@Injectable({
    providedIn: 'root',
})
export class AuthService extends ApiService {
    readonly BASE_URL: string = '/Auth';

    private user: User;
    private userChange: EventEmitter<User> = new EventEmitter();
    private token: AuthToken;
    private tokenChange: EventEmitter<void> = new EventEmitter();

    constructor(
        protected http: HttpClient,
        protected notificationService: NotificationService, 
        private localApi: LocalApiService,
        private userService: UserApiService
    ) {
        super(http, notificationService);
    }

    login(loginRequest: LoginRequest): Observable<ApiResponse> {
        const req = this.post({
            url: this.BASE_URL
        }, loginRequest).pipe(
            map((res: AuthToken) => {
                this.token = res;
                this.localApi.saveToLocalStorage("session", this.token);
                this.fetchUser();
                this.tokenChange.emit();
                return res;
            })
        );

        return req;
    }

	logout(): Observable<ApiResponse> {
		return this.post({
			url: `${this.BASE_URL}/Disconnect`,
            handleSuccess: false,
		}).pipe(
			map(res => {
				this.deleteToken();
				return res;
			})
		);
	}

    private fetchUser(): void {
        this.userService.getCurrentUser().subscribe((user: User) => {
            this.user = user;
            this.userChange.emit(this.user);
        });
    }

    isAuthenticated(): boolean {
        if (this.token == undefined || this.token.token == null) {
            const token = this.localApi.getFromLocalStorage("session");
            if (token !== null) {
                const parsedToken: AuthToken = JSON.parse(token);
                this.token = parsedToken;
                this.fetchUser();
                this.tokenChange.emit();
            } else {
                return false;
            }
        }
        return this.token !== undefined && this.token.token !== null && moment(this.token.expires_at) > moment();
    }

    listenTokenChange(): Observable<void> {
        return this.tokenChange.asObservable();
    }

    listenUserChange(): Observable<User> {
        return this.userChange.asObservable();
    }

	deleteToken(): void {
		this.localApi.deleteFromStorage("session");
		this.token = new AuthToken();
	}

    getUser(): User {
        return this.user;
    }

    getAccessToken(): AuthToken {
        return this.token;
    }
}
