import { Injectable } from '@angular/core';
import { LocalApiService } from './local-api.service';

@Injectable({
    providedIn: 'root',
})
export class AttributesManagerService extends LocalApiService {
    getProperties<T>(tool: string): T {
        return this.parseObj<T>(this.getFromLocalStorage(tool));
    }
}
