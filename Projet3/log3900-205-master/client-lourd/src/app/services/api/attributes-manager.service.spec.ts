import { TestBed } from '@angular/core/testing';
import { Color } from '@app/classes/drawing/color';
import { DrawingType } from '@app/classes/drawing/drawing-type';
import { ToolProperties } from '@app/classes/tool-properties/tool-properties';
import { AttributesManagerService } from './attributes-manager.service';

// tslint:disable:no-magic-numbers
// tslint:disable:no-string-literal
describe('AttributesManagerService', () => {
    let service: AttributesManagerService;
    let getLocalStorageSpy: jasmine.Spy<(key: string) => string | null>;
    let saveLocalStorageSpy: jasmine.Spy<<T>(key: string, attr: T) => void>;

    let toolProperties: ToolProperties;
    let primaryColor: Color;
    let secondaryColor: Color;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(AttributesManagerService);

        toolProperties = { drawingType: DrawingType.Stroke };
        primaryColor = { r: 0, g: 0, b: 0, a: 1 };
        secondaryColor = { r: 255, g: 255, b: 255, a: 1 };

        getLocalStorageSpy = spyOn(service, 'getFromLocalStorage');
        saveLocalStorageSpy = spyOn(service, 'saveToLocalStorage');
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should return tool properties from local storage', () => {
        getLocalStorageSpy.and.returnValue(JSON.stringify(toolProperties));
        expect(service.getProperties<ToolProperties>('')).toEqual(toolProperties);
    });

    it('should return primary color from local storage', () => {
        getLocalStorageSpy.and.returnValue(JSON.stringify(primaryColor));
        expect(service.getPrimaryColor()).toEqual(primaryColor);
    });

    it('should return secondary color form local storage', () => {
        getLocalStorageSpy.and.returnValue(JSON.stringify(secondaryColor));
        expect(service.getSecondaryColor()).toEqual(secondaryColor);
    });

    it('should save to local storage', () => {
        const spy = spyOn(localStorage, 'setItem');
        saveLocalStorageSpy.and.callThrough();
        service.saveToLocalStorage('test', 'test');
        expect(spy).toHaveBeenCalled();
    });

    it('should call save to local storage once savePimaryColor is called', () => {
        service.savePrimaryColor(primaryColor);

        expect(saveLocalStorageSpy).toHaveBeenCalledTimes(2);
    });

    it('should call save to local storage once saveSecondaryColor is called', () => {
        service.saveSecondaryColor(secondaryColor);

        expect(saveLocalStorageSpy).toHaveBeenCalledTimes(2);
    });

    it('should pop last lastest color when it is at max length', () => {
        const latestSpy = spyOn(service, 'getLatestColors');
        const latest = [
            primaryColor,
            primaryColor,
            primaryColor,
            primaryColor,
            primaryColor,
            secondaryColor,
            secondaryColor,
            secondaryColor,
            secondaryColor,
            secondaryColor,
        ];
        const popSpy = spyOn(latest, 'pop');
        latestSpy.and.returnValue(latest);
        service.savePrimaryColor({ r: 255, g: 255, b: 0, a: 1 });

        expect(popSpy).toHaveBeenCalled();
    });

    it('should put latest color at start on select latest', () => {
        const latestSpy = spyOn(service, 'getLatestColors');
        const color = { r: 255, g: 255, b: 0, a: 1 };
        const latest = [
            primaryColor,
            primaryColor,
            primaryColor,
            primaryColor,
            primaryColor,
            secondaryColor,
            secondaryColor,
            secondaryColor,
            secondaryColor,
            color,
        ];
        latestSpy.and.returnValue(latest);
        service.savePrimaryColor(color);

        latest[0] = color;
        latest[5] = primaryColor;
        latest[9] = secondaryColor;

        expect(saveLocalStorageSpy).toHaveBeenCalledWith(service['LATEST_COLORS_KEY'], latest);
    });
});
