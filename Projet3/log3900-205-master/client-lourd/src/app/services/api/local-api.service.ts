import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class LocalApiService {
    getFromLocalStorage(key: string): string | null {
        return localStorage.getItem(key);
    }

    saveToLocalStorage<T>(key: string, attr: T): void {
        localStorage.setItem(key, JSON.stringify(attr));
    }

    deleteFromStorage(key: string): void {
        localStorage.removeItem(key);
    }

    protected parseObj<T>(obj: string | null, defaultValue: null | string | [] = null): T {
        return obj != null ? JSON.parse(obj) : defaultValue;
    }
}
