import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiResponse } from '@app/classes/api/api-response';
import { NotificationService } from '@app/services/notification/notification.service';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
    providedIn: 'root',
})
export class ContactApiService extends ApiService {
    readonly BASE_URL: string = '/Contact';

    constructor(protected http: HttpClient, protected notificationService: NotificationService) {
        super(http, notificationService);
    }

    getContacts(): Observable<ApiResponse> {
        return this.get({
            url: this.BASE_URL
        });
    }

    getUserList(): Observable<ApiResponse> {
        return this.get({
            url: `${this.BASE_URL}/UserList`
        });
    }

    addContact(id: number): Observable<ApiResponse> {
        return this.post({
            url: this.BASE_URL
        }, {contact_id: id});
    }

    deleteContact(id: number): Observable<ApiResponse> {
        return this.delete({
            url: `${this.BASE_URL}/${id}`
        });
    }

    getCollaborativeSessions(contactId: number): Observable<ApiResponse> {
        return this.get({
            url: `${this.BASE_URL}/${contactId}/CollaborativeSessions`
        });
    }

    getContactExpositions(contactId: number): Observable<ApiResponse> {
        return this.get({
            url: `${this.BASE_URL}/${contactId}/Expositions`
        });
    }
}
