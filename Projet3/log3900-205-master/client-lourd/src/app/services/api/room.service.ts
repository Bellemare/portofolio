import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { NotificationService } from '@app/services/notification/notification.service';
import { ApiService } from './api.service';
import { AuthService } from './auth.service';
import { SocketClient } from '@app/classes/socket/socket-client';
import { Observable } from 'rxjs';
import { ApiResponse } from '@app/classes/api/api-response';
import { RoomFetchHistoryRequest } from '@app/classes/api/room-fetch-history-req';
import { RoomCreateRequest} from '@app/classes/api/room-create-request'
import { RequestConfig } from '@app/classes/api/request-config';

@Injectable({
    providedIn: 'root',
})
export class RoomService extends ApiService {
    readonly BASE_URL: string = '/Room';
    readonly PUBLIC_ROOM_ID: number = 0;

    private activeRooms: Map<number, SocketClient> = new Map<number, SocketClient>();

    private appClientPromise: Promise<SocketClient> | undefined;
    private appClient: SocketClient;

    private roomAddListener: EventEmitter<SocketClient> = new EventEmitter();
    private roomRemoveListener: EventEmitter<void> = new EventEmitter();

    constructor(protected http: HttpClient, protected notificationService: NotificationService, private authService: AuthService) {
        super(http, notificationService);
    }

    getRoomMembers(id: number): Observable<ApiResponse> {
        return this.get({
            url: `${this.BASE_URL}/${id}/Members`
        });
    }

    getHistory(req: RoomFetchHistoryRequest): Observable<ApiResponse> {
        return this.get({
            url: `${this.BASE_URL}/${req.room_id}/History/Limit/${req.limit}/Offset/${req.offset}`
        });
    }

    fetchAllRooms(): Observable<ApiResponse> {
      let url: RequestConfig = {url: this.BASE_URL};
      return this.get(url);
    }

    fetchJoinedRooms(): Observable<ApiResponse> {
      let url: RequestConfig = {url: `${this.BASE_URL}/JoinedRooms`};
      return this.get(url);
    }

    fetchAllRoomsByName(name: string): Observable<ApiResponse> {
      let url: RequestConfig = {url: `${this.BASE_URL}/By/${name}`};
      return this.get(url);
    }

    deleteRoom(id: number): Observable<ApiResponse> {
        return this.delete({
            url: `${this.BASE_URL}/${id}`,
            handleSuccess: false,
        });
    }

    leaveRoom(id: number): Observable<ApiResponse> {
        return this.post({
            url: `${this.BASE_URL}/${id}/Leave`,
            handleSuccess: false,
        });
    }

    joinRoom(id: number): Observable<ApiResponse> {
      return this.post({
          url: `${this.BASE_URL}/${id}/Join`,
          handleSuccess: false,
      });
    }

    createRoom(name: string): Observable<ApiResponse>{
        let roomCreationReq : RoomCreateRequest = {name: name};
        let url: RequestConfig = {url: this.BASE_URL}
        return this.post(url,roomCreationReq);
    }

    getAppClient(): Promise<SocketClient> {
        if (this.appClientPromise != undefined)
            return this.appClientPromise;

        this.appClientPromise =  new Promise<SocketClient>((resolve, reject) => {
            if (this.appClient !== undefined) {
                resolve(this.appClient);
            }

            const token = this.authService.isAuthenticated() ? this.authService.getAccessToken().token:"";
            const conn = new SocketClient();
            conn.onClose = () => reject();
            conn.connect(-1, `${this.BASE_URL}/ConnectToApp/${token}`, () => {
                this.appClient = conn;
                conn.onClose = () => this.appClient.closeConnection();
                resolve(conn);
            });
        });
        return this.appClientPromise;
    }

    listenDrawingRoomAdd(): Observable<SocketClient> {
        return this.roomAddListener.asObservable();
    }

    listenDrawingRoomRemove(): Observable<void> {
        return this.roomRemoveListener.asObservable();
    }

    addDrawingRoom(conn: SocketClient): void {
        this.roomAddListener.emit(conn);
    }

    removeDrawingRoom(): void {
        this.roomRemoveListener.emit();
    }

    connectRoom(roomId: number): Promise<SocketClient> {
        const promise = new Promise<SocketClient>((resolve, reject) => {
            const token = this.authService.isAuthenticated() ? this.authService.getAccessToken().token:"";

            const conn = new SocketClient();
            conn.onClose = () => {
                reject();
            }
            let url;
            if (roomId == this.PUBLIC_ROOM_ID) {
                url = `${this.BASE_URL}/ConnectToPublic/${token}`
            } else {
                url = `${this.BASE_URL}/${roomId}/Connect/${token}`
            }
            conn.connect(roomId, url, () => {
                conn.onClose = this.disconnectRoom.bind(this, roomId);
                this.activeRooms.set(roomId, conn);
                resolve(conn);
            });
        });

        return promise;
    }

    disconnectRoom(roomId: number): void {
        const room = this.activeRooms.get(roomId);
        if (room !== undefined) {
            room.closeConnection();
            this.activeRooms.delete(roomId);
        }
    }

    disconnectAll(): void {
        this.appClient?.closeConnection();
        this.appClientPromise = undefined;
        this.activeRooms.forEach((room: SocketClient, num: number) => {
            room.closeConnection();
            this.activeRooms.delete(num);
        });
    }
}
