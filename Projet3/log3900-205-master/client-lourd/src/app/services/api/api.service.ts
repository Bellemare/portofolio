import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiResponse } from '@app/classes/api/api-response';
import { RequestConfig } from '@app/classes/api/request-config';
import { Message } from '@app/message';
import { NotificationService } from '@app/services/notification/notification.service';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ErrorResponse, FieldsErrorResponse } from '@app/classes/api/error-response';
import { NotificationEntry } from '@app/classes/notification-entry';

@Injectable({
    providedIn: 'root',
})
export class ApiService {
    protected readonly ERROR_CODE_CONNETION: number = 0;
    protected readonly ERROR_CODE_THRESHOLD: number = 400;
    protected readonly DEFAULT_HOST: string = environment.defaultHost;
    protected readonly DEFAULT_PORT: number = environment.defaultPort;
    protected readonly DEFAULT_ENDPOINT: string = environment.defaultEndpoint;

    constructor(protected http: HttpClient, protected notificationService: NotificationService) {}

    protected get(config: RequestConfig): Observable<ApiResponse> {
        const req = this.http.get<ApiResponse>(this.constructUrl(config));
    
        return this.registerRequestHandlers(config, req);
    }

    protected post(config: RequestConfig, data: object | Message = {}): Observable<ApiResponse> {
        const req = this.http.post<ApiResponse>(this.constructUrl(config), data);
    
        return this.registerRequestHandlers(config, req);
    }

    protected put(config: RequestConfig, data: object | Message = {}): Observable<ApiResponse> {
        const req = this.http.put<ApiResponse>(this.constructUrl(config), data)

        return this.registerRequestHandlers(config, req);
    }

    protected delete(config: RequestConfig): Observable<ApiResponse> {
        const req = this.http.delete<ApiResponse>(this.constructUrl(config));

        return this.registerRequestHandlers(config, req);
    }

    private registerRequestHandlers(config: RequestConfig, req: Observable<ApiResponse>): Observable<ApiResponse> {
        if ((config.handleSuccess !== undefined && config.handleSuccess) || config.handleSuccess === undefined) {
            req = req.pipe(map(this.handleSuccess.bind(this)));
        }        
        if ((config.handleError !== undefined && config.handleError) || config.handleError === undefined) {
            req = req.pipe(catchError(this.handleError.bind(this)));
        }
        return req;
    }

    protected constructUrl(config: RequestConfig): string {
        const host = config.host != undefined ? config.host : this.DEFAULT_HOST;
        const port = config.port != undefined ? config.port : this.DEFAULT_PORT;
        const endpoint = config.endpoint != undefined ? config.endpoint : this.DEFAULT_ENDPOINT;
        if (port != 80) {
            return `${host}:${port}${endpoint}${config.url}`;
        } else {
            return `${host}${endpoint}${config.url}`;
        }
    }

    protected handleSuccess(response: Message): ApiResponse {
        if (response != null && response.title !== undefined) {
            this.notificationService.setNotification({
                title: response.title,
                message: response.body !== undefined ? response.body : '',
                code: 200,
            });
        }
        return response;
    }

    protected handleError(result: HttpErrorResponse): (error: HttpErrorResponse) => Observable<ApiResponse> {
        const error = result.error;
        this.sendErrorNotificationMessage(error, result.status);
        throw result;
    }

    protected sendErrorNotificationMessage(error: ErrorResponse|FieldsErrorResponse, code: number = this.ERROR_CODE_THRESHOLD): void {
        if (error === undefined)
            error = new ErrorResponse();
        const titleIsSet = error !== undefined && error.title !== undefined;

        error.body = this.isErrorNoConnection(code) ? "Impossible d'établir une connexion avec le serveur" : error.body;

        const messageIsSet = error !== undefined && error.body !== undefined;

        const entry: NotificationEntry = {
            title: titleIsSet ? error.title : 'Erreur',
            code: Math.max(this.ERROR_CODE_THRESHOLD, code),
        }
        if (Array.isArray(error.body)) {
            entry.messages = error.body;
            entry.timeMs = 2000 * entry.messages.length;
        } else {
            entry.message = messageIsSet ? error.body : '';
        }
        this.notificationService.setNotification(entry);
    }

    protected isErrorNoConnection(code: number): boolean {
        return code === this.ERROR_CODE_CONNETION;
    }
}
