import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class ElectronService {
    private ipcRenderer: any;

    constructor() {
        const ipc = (<any>window).ipcRenderer;
        if (ipc != undefined) {
            this.ipcRenderer = ipc;
        }
    }

    popoutChat(drawingId?: number): Promise<void> {
        return new Promise((resolve, reject) => {
            if (this.ipcRenderer == undefined)
                reject();

            this.ipcRenderer.invoke("popout-chat", drawingId).then(() => resolve());
        });
    }

    sendNewDrawingRoom(id: number): void {
        if (this.ipcRenderer == undefined)
            return;

        this.ipcRenderer.send("new-drawing-room", id);
    }

    listenNewDrawingRoom(): Promise<number> {
        return new Promise((resolve, reject) => {
            if (this.ipcRenderer == undefined)
                reject();

            this.ipcRenderer.invoke("get-drawing-room").then((id: number) => resolve(id));
        });
    }

    sendQuitDrawingRoom(): void {
        if (this.ipcRenderer == undefined)
            return;

        this.ipcRenderer.send("quit-drawing-room");
    }

    listenQuitDrawingRoom(): Promise<void> {
        return new Promise((resolve, reject) => {
            if (this.ipcRenderer == undefined)
                reject();

            this.ipcRenderer.invoke("listen-quit-drawing-room").then(() => resolve());
        });
    }
}
