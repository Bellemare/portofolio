import { EventEmitter, Injectable } from '@angular/core';
import { DrawingEvent } from '@app/classes/drawing/drawing-event';
import { PayloadTypes } from '@app/classes/socket/payload-types';
import { SocketClient } from '@app/classes/socket/socket-client';
import { DrawingApiService } from '@app/services/api/drawing-api.service';
import { DrawingEventPayload } from '@app/classes/socket/drawing-event-payload';
import { DrawingHistoryPayload } from '@app/classes/socket/drawing-history-payload';
import { EventType } from '@app/classes/drawing/event-type';
import { Observable } from 'rxjs';
import { DrawingElement } from '@app/classes/drawing/drawing-element';
import { DrawingCanvasHelper } from '@app/classes/drawing-canvas-helper';
import { DrawingSavePayload } from '@app/classes/socket/drawing-save-payload';
import { NotificationService } from '../notification/notification.service';
import { Router } from '@angular/router';
import { RoomService } from '../api/room.service';
import { User } from '@app/classes/api/user';
import { LayerEventPayload } from '@app/classes/socket/layer-event-payload';
import { LayerPreviewEventPayload } from '@app/classes/socket/layer-preview-event-payload';
import { LayerMoveEventPayload } from '@app/classes/socket/layer-move-event-payload';
import { LayerTextEventPayload } from '@app/classes/socket/layer-text-event-payload';
import { LayerEventType } from '@app/classes/socket/layer-event-type';
import { DrawingEllipse } from '@app/classes/drawing/drawing-ellipse';
import { DrawingPos } from '@app/classes/drawing/drawing-pos';
import { DrawingRect } from '@app/classes/drawing/drawing-rect';
import { ToolType } from '@app/classes/drawing/tool-type';
import { CouchesService } from '../couches/couches.service';
import { DrawingEventWrapper } from '@app/classes/drawing/drawing-event-wrapper';
import { BoundingBox } from '@app/classes/drawing/bounding-box';
import { KickEventPayload } from '@app/classes/socket/kick-event-payload';
import { DrawingShape } from '@app/classes/drawing/drawing-shape';
import { AuthService } from '../api/auth.service';

@Injectable({
    providedIn: 'root',
})
export class SynchronizerService {
    readonly WAIT_MS_ON_PREVIEW: number = 30;

    private drawingId: number;
    private conn: SocketClient;
    private previousEvent: DrawingElement|undefined;

    private layerEvents: Map<number, DrawingEventWrapper> = new Map();
    private previewEvents: Map<number, DrawingEventPayload> = new Map();
    private drawEvent: EventEmitter<DrawingEventPayload> = new EventEmitter();
    private redrawPreviewEvent: EventEmitter<boolean> = new EventEmitter();

    private onLoadEvent: EventEmitter<DrawingHistoryPayload> = new EventEmitter();

    private waiter: any;

    private newCollaboratorEvent: EventEmitter<User> = new EventEmitter();
    private collaborateLeaveEvent: EventEmitter<User> = new EventEmitter();

    private redrawEvent: EventEmitter<boolean> = new EventEmitter();
    layerAddEvent: (event: DrawingEvent, layer: number) => void;

    layerSelectEvent: (event: DrawingEventWrapper) => void;
    layerDeleteEvent: () => void;

    constructor(
        private drawingApiService: DrawingApiService, 
        private notificationService: NotificationService, 
        private router: Router,
        private roomService: RoomService,
        private couchesService: CouchesService,
        private authService: AuthService,
    ) {}

    init(drawingId: number): void {
        this.drawingId = drawingId;
        this.drawingApiService.joinCollaborativeSession(this.drawingId).then((conn: SocketClient) => {
            this.conn = conn;
            this.roomService.addDrawingRoom(conn);
            this.conn.onMessage(PayloadTypes.DRAWING_EVENT, this.onNewDrawingEvent.bind(this));
            this.conn.onMessage(PayloadTypes.DRAWING_PREVIEW_EVENT, this.onNewDrawingPreviewEvent.bind(this));
            this.conn.onMessage(PayloadTypes.LAYER_EVENT, this.onNewLayerEvent.bind(this));
            this.conn.onMessage(PayloadTypes.LAYER_PREVIEW_EVENT, this.onNewLayerPreviewEvent.bind(this));
            this.conn.onMessage(PayloadTypes.LAYER_TEXT_EVENT, this.onNewLayerTextEvent.bind(this));
            this.conn.onMessage(PayloadTypes.LAYER_MOVE_EVENT, this.onLayerMoveEvent.bind(this));
            this.conn.onMessage(PayloadTypes.DRAWING_HISTORY, this.onDrawingHistory.bind(this));
            this.conn.onMessage(PayloadTypes.KICK_EVENT, this.onKick.bind(this));
        }, () => {
            this.notificationService.setNotification({
                code: 400,
                title: "Erreur",
                message: "Impossible de se connecter au dessin"
            });
            this.router.navigateByUrl("/");
        });
    }

    getCurrentLayerPreviewEvents(): Map<number, DrawingEventWrapper> {
        return this.layerEvents;
    }

    getCurrentPreviewEvents(): Map<number, DrawingEventPayload> {
        return this.previewEvents;
    }

    listenToNewCollaborator(): Observable<User> {
        return this.newCollaboratorEvent.asObservable();
    }

    listenToCollaboratorLeave(): Observable<User> {
        return this.collaborateLeaveEvent.asObservable();
    }

    listenToPreviewEvent(): Observable<boolean> {
        return this.redrawPreviewEvent.asObservable();
    }

    listenToDrawEvent(): Observable<DrawingEventPayload> {
        return this.drawEvent.asObservable();
    }

    listenToRedrawEvent(): Observable<boolean> {
        return this.redrawEvent.asObservable();
    }

    listenOnLoad(): Observable<DrawingHistoryPayload> {
        return this.onLoadEvent.asObservable();
    }

    private onNewUserLayerEvent(userId: number, id: number): void {
        let shouldRefresh = false;
        this.layerEvents.forEach((evt: DrawingEventWrapper, key: number) => {
            if (evt.selectedBy == userId && id != key) {
                evt.selectedBy = -1;
                evt.isSelected = false;
                evt.renderLayer();
                this.layerEvents.delete(key);
                shouldRefresh = true;
            }
        });
        if (shouldRefresh) {
            this.redrawEvent.emit();
            this.launchPreviewRefresh(false);
        }
    }

    private onKick(payload: KickEventPayload): void {
        this.notificationService.setNotification({
            code: 400,
            title: "Alerte",
            message: payload.reason
        });
        this.router.navigateByUrl("/");
    }

    private onDrawingHistory(payload: DrawingHistoryPayload): void {
        this.onLoadEvent.emit(payload);
    }

    private registerNewElement(data: DrawingElement, type: ToolType): DrawingElement {
        let element: DrawingElement = new DrawingElement(data);
        switch (type) {
            case ToolType.PENCIL:
                element = new DrawingPos(data);
                break;
            case ToolType.RECTANGLE:
                element = new DrawingRect(data);
                break;
            case ToolType.ELLIPSE:
                element = new DrawingEllipse(data);
                break;
        }
        return element;
    }

    private registerNewEvent(payload: DrawingEventPayload): DrawingEventPayload {
        payload.event.data = this.registerNewElement(payload.event.data, payload.event.tool_type);
        return payload;
    }

    private onNewLayerPreviewEvent(payload: LayerPreviewEventPayload): void {
        let layer = this.layerEvents.get(payload.id);
        if (layer == undefined) {
            layer = this.couchesService.getElementFromId(payload.id);
        }

        if (layer != undefined) {
            const transform = payload.transform;
            const layerEvent = this.layerEvents.get(payload.id) as DrawingEventWrapper;
            if (layerEvent.drawingEvent) {
                layerEvent.drawingEvent.attributes = payload.attributes;
                layerEvent.drawingEvent.stroke_color = payload.stroke_color;
                layerEvent.drawingEvent.fill_color = payload.fill_color;
                layerEvent.drawingEvent.text_color = payload.text_color;
                const data = layerEvent.drawingEvent.data;
                const currentBox = layerEvent.drawingEvent.data.getEditBoundingBox() as BoundingBox;
                const dw = transform.dw / currentBox.w;
                const dh = transform.dh / currentBox.h;
                const dx = transform.dx - currentBox.x;
                const dy = transform.dy - currentBox.y;
                data.applyScaling(dw, dh, transform.flip_x, transform.flip_y, payload.attributes);
                data.applyTranslation(dx, dy);
                layerEvent.drawingEvent.data.bounding_box = undefined;
                layerEvent.redraw();
                this.launchPreviewRefresh(false);
            }
        }
    }

    private onNewLayerTextEvent(payload: LayerTextEventPayload): void {
        let layer = this.layerEvents.get(payload.id);
        if (layer == undefined) {
            layer = this.couchesService.getElementFromId(payload.id);
            this.setLayerSelect(layer, payload.id, payload.user_id as number);
        }

        if (layer != undefined) {
            const layerEvent = this.layerEvents.get(payload.id) as DrawingEventWrapper;
            if (layerEvent.drawingEvent && layerEvent.drawingEvent.data instanceof DrawingShape) {
                layerEvent.drawingEvent.data.text = payload.text;
                layerEvent.renderLayer();
                this.launchPreviewRefresh(payload.user_id == this.authService.getUser().id);
            }
        }
    }

    private onLayerMoveEvent(payload: LayerMoveEventPayload): void {
        this.onNewUserLayerEvent(payload.user_id as number, payload.id);
        this.couchesService.moveLayer(payload.layer, payload.new_layer);

        this.redrawEvent.emit(payload.user_id == this.authService.getUser().id);
        this.launchPreviewRefresh(payload.user_id == this.authService.getUser().id);
    }

    private onNewLayerEvent(payload: LayerEventPayload): void {
        this.onNewUserLayerEvent(payload.user_id as number, payload.id);
        switch (payload.type) {
            case LayerEventType.SELECT:
                this.onLayerSelect(payload);
                break;
            case LayerEventType.CHANGE:
                this.onLayerChange(payload);
                break;
            case LayerEventType.DELETE:
                this.onLayerDelete(payload);
                break;
        }
    }

    private setLayerSelect(event: DrawingEventWrapper, id: number, userId: number): void {
        this.layerEvents.set(id, event);
        event.isSelected = true;
        event.selectedBy = userId;
        event.render.emit();
        this.redrawEvent.emit(false);
        this.launchPreviewRefresh(false);
    }

    private onLayerSelect(payload: LayerEventPayload): void {
        const layer = this.couchesService.getElementFromId(payload.id);
        if (payload.user_id == this.authService.getUser().id) {
            this.layerSelectEvent(layer);
        } else {
            if (layer) {
                this.setLayerSelect(layer, payload.id, payload.user_id as number);
            }
        }
    }

    private onLayerChange(payload: LayerEventPayload): void {
        let layer = this.layerEvents.get(payload.id);
        if (!this.layerEvents.has(payload.id)) {
            layer = this.couchesService.getElementFromId(payload.id);
        }

        if (layer != undefined) {
            const drawingEvent = payload.event;
            drawingEvent.data = this.registerNewElement(drawingEvent.data, drawingEvent.tool_type);
            layer.drawingEvent = drawingEvent;
            layer.isSelected = false;
            layer.selectedBy = -1;
            layer.renderLayer();
            this.redrawEvent.emit(payload.user_id == this.authService.getUser().id);
        }
        this.layerEvents.delete(payload.id);
        this.launchPreviewRefresh(payload.user_id == this.authService.getUser().id);
    }

    private onLayerDelete(payload: LayerEventPayload): void {
        this.layerEvents.delete(payload.id);
        this.couchesService.removeLayer(payload.layer);
        if (payload.user_id == this.authService.getUser().id) {
            this.layerDeleteEvent();
        }
        this.redrawEvent.emit(payload.user_id == this.authService.getUser().id);
        this.launchPreviewRefresh(payload.user_id == this.authService.getUser().id);
    }

    private launchPreviewRefresh(clearPreview: boolean): void {
        if (this.waiter == undefined) {
            this.waiter = setTimeout(() => {  
                this.redrawPreviewEvent.emit(clearPreview);
                this.waiter = undefined;  
            }, this.WAIT_MS_ON_PREVIEW);
        }
    }

    private onNewDrawingPreviewEvent(payload: DrawingEventPayload): void {
        if (!this.previewEvents.has(payload.user_id)) {
            this.previewEvents.set(payload.user_id, this.registerNewEvent(payload));
        } else {
            const evt = this.previewEvents.get(payload.user_id);
            const element = evt?.event.data as DrawingElement;
            const success = element.handleNewEvent(payload);
            if (!success) {
                this.previewEvents.set(payload.user_id, this.registerNewEvent(payload));
            }
        }
        this.launchPreviewRefresh(false);
    }

    private onNewDrawingEvent(payload: DrawingEventPayload): void {
        this.previewEvents.delete(payload.user_id);
        this.redrawPreviewEvent.emit();
        this.drawEvent.emit(this.registerNewEvent(payload));
    }

    sendEvent(event: DrawingEvent): void {
        const evt: DrawingEventPayload = {
            event,
            user_id: 0,
            user: "",
        }
        if (event.type == EventType.PREVIEW) {
            const previous = this.previousEvent;
            this.previousEvent = JSON.parse(JSON.stringify(event.data));
            event.data = event.data.createNewEvent(previous);
            this.conn.send(evt, PayloadTypes.DRAWING_PREVIEW_EVENT);
        } else {
            this.previousEvent = undefined;
            this.conn.send(evt, PayloadTypes.DRAWING_EVENT);
        }
    }

    saveDrawing(): void {
        const payload: DrawingSavePayload = {
            image: DrawingCanvasHelper.baseCanvas.toDataURL("image/png")
        }
        this.conn.send(payload, PayloadTypes.DRAWING_SAVE);
    }

    layerEvent(type: LayerEventType, event: DrawingEvent, layer: number): void {
        const id = this.couchesService.getElement(layer)?.drawingEvent?.id as number;
        if (id == undefined) {
            return;
        }
        const payload: LayerEventPayload = {
            id,
            type,
            event,
            layer,
        }

        this.conn.send(payload, PayloadTypes.LAYER_EVENT);
    }

    layerPreviewEvent(payload: LayerPreviewEventPayload): void {
        this.conn.send(payload, PayloadTypes.LAYER_PREVIEW_EVENT);
    }

    layerTextEvent(payload: LayerTextEventPayload): void {
        this.conn.send(payload, PayloadTypes.LAYER_TEXT_EVENT);
    }

    moveLayerEvent(newIndex: number, oldIndex: number): void {
        const id = this.couchesService.getElement(oldIndex).drawingEvent?.id as number;
        let afterId = -1;
        if (newIndex > 0)
            afterId = this.couchesService.getElement(newIndex - (newIndex > oldIndex ? 0:1))?.drawingEvent?.id as number;

        const payload: LayerMoveEventPayload = {
            id: id,
            after: afterId,
            layer: oldIndex,
            new_layer: newIndex,
        }

        this.conn.send(payload, PayloadTypes.LAYER_MOVE_EVENT);
    }

    onDestroy(): void {
        this.drawingApiService.leaveSession(this.drawingId);
        this.conn.closeConnection();
        this.roomService.removeDrawingRoom();
        this.layerEvents = new Map();
        this.previewEvents = new Map();
    }
}
