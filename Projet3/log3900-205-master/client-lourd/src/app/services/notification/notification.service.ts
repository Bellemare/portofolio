import { Injectable } from '@angular/core';
import { NotificationEntry } from '@app/classes/notification-entry';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class NotificationService {
    readonly DEFAULT_NOTIFICATION_TIME: number = 5000;

    private notification: BehaviorSubject<NotificationEntry | null> = new BehaviorSubject<NotificationEntry | null>(null);

    getNotification(): Observable<NotificationEntry | null> {
        return this.notification.asObservable();
    }

    setNotification(entry: NotificationEntry): void {
        this.notification.next(entry);
    }
}
