import { TestBed } from '@angular/core/testing';
import { NotificationEntry } from '@app/classes/notification-entry';
import { Observable } from 'rxjs';
import { NotificationService } from './notification.service';

describe('NotificationService', () => {
    let service: NotificationService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(NotificationService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should return observable for notifications', () => {
        expect(service.getNotification()).toBeInstanceOf(Observable);
    });

    it('should set notification properly', () => {
        // tslint:disable-next-line:no-string-literal
        const nextSpy = spyOn(service['notification'], 'next');
        service.setNotification({} as NotificationEntry);
        expect(nextSpy).toHaveBeenCalled();
    });
});
