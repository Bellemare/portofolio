import { TestBed } from '@angular/core/testing';
import { AttributesManagerService } from '@app/services/api/attributes-manager.service';
import { KeyBindingResolverService } from '@app/services/key-binding/key-binding-resolver.service';
import { ColorService } from './color.service';
import { ToolService } from './tool.service';

class ToolStub extends ToolService {}

// tslint:disable:no-any
// tslint:disable:no-string-literal
describe('ToolService', () => {
    let service: ToolStub;

    beforeEach(() => {
        TestBed.configureTestingModule({});

        const attributesManagerService = new AttributesManagerService();
        const colorService = new ColorService(attributesManagerService);
        const keyBindingService = new KeyBindingResolverService();
        service = new ToolStub(attributesManagerService, colorService, keyBindingService);

        service.setDefaultProperties();
        service.onInit();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should return tooltips', () => {
        expect(service.toolTooltips).toEqual(service['TOOLTIPS']);
    });

    it('should reset correctly on init', () => {
        const clearDataSpy = spyOn<any>(service, 'clearData');
        service['mouseDown'] = true;
        service['mouseDownCoord'] = { x: 2, y: 2 };
        service.onInit();
        expect(service['mouseDown']).toEqual(false);
        expect(service['mouseDownCoord']).toEqual({ x: 0, y: 0 });
        expect(clearDataSpy).toHaveBeenCalled();
    });

    it('should check if is drawing', () => {
        service['mouseDown'] = false;
        expect(service.isDrawing).toEqual(false);
        service['mouseDown'] = true;
        expect(service.isDrawing).toEqual(true);
    });

    it('should return drawing type', () => {
        expect(service.drawingType).toEqual(service['currentAttributes'].drawingType);
    });

    it('should return stroke color', () => {
        expect(service.strokeColor).toEqual(service['primaryColor']);
    });

    it('should return fill color', () => {
        expect(service.fillColor).toEqual(service['primaryColor']);
    });

    it('should return current attributes', () => {
        service['currentAttributes'] = service['DEFAULT_CONFIGURATION'];
        expect(service.toolAttributes).toEqual(service['currentAttributes']);
    });

    it('should save default properties when none are saved', () => {
        const getSavedAttributesSpy = spyOn<any>(service, 'getSavedAttributes');
        const saveAttributesSpy = spyOn(service['attributesManagerService'], 'saveToLocalStorage');
        getSavedAttributesSpy.and.returnValue(null);
        service['getDefaultAttributes'](service.TOOL_NAME, service['DEFAULT_CONFIGURATION']);
        expect(saveAttributesSpy).toHaveBeenCalledWith(service.TOOL_NAME, service['DEFAULT_CONFIGURATION']);
    });

    it('should set new properties when getting default properties', () => {
        const getSavedAttributesSpy = spyOn<any>(service, 'getSavedAttributes');
        getSavedAttributesSpy.and.returnValue(JSON.parse(JSON.stringify(service['DEFAULT_CONFIGURATION'])));
        const saveAttributesSpy = spyOn(service['attributesManagerService'], 'saveToLocalStorage');
        service['DEFAULT_CONFIGURATION'].test = 'test';
        service.setDefaultProperties();
        expect(service['currentAttributes'].test).toEqual('test');
        expect(saveAttributesSpy).toHaveBeenCalled();
    });

    it('should emit attribute change and save to local storage when setting property', () => {
        const saveAttributesSpy = spyOn(service['attributesManagerService'], 'saveToLocalStorage');
        const attributeChangeSpy = spyOn(service['attributesChanged'], 'emit');
        service.setProperty('test', 'testing');
        expect(attributeChangeSpy).toHaveBeenCalled();
        expect(saveAttributesSpy).toHaveBeenCalled();
    });

    it('should emit data change event when calling dataChanged', () => {
        const dataChangeSpy = spyOn(service['dataChange'], 'next');
        service['dataChanged']();
        expect(dataChangeSpy).toHaveBeenCalled();
    });

    it('should emit preview data change event when calling previewDataChanged', () => {
        const previewDataChangeSpy = spyOn(service['previewDataChange'], 'next');
        service['previewDataChanged']();
        expect(previewDataChangeSpy).toHaveBeenCalled();
    });

    it('cursor should return currentCursor', () => {
        service['currentCursor'] = 'text';
        expect(service.cursor).toEqual('text');
    });
});
