import { TestBed } from '@angular/core/testing';
import { Color } from '@app/classes/drawing/color';
import { AttributesManagerService } from '@app/services/api/attributes-manager.service';
import { ColorService } from './color.service';

// tslint:disable:no-magic-numbers
// tslint:disable:no-string-literal
// tslint:disable:no-empty
describe('ColorService', () => {
    let service: ColorService;
    let attributesManagerSpy: jasmine.SpyObj<AttributesManagerService>;

    let primaryColor: Color;
    let secondaryColor: Color;

    beforeEach(() => {
        primaryColor = { r: 20, g: 20, b: 20, a: 1 };
        secondaryColor = { r: 250, g: 250, b: 250, a: 1 };

        attributesManagerSpy = jasmine.createSpyObj('AttributesManagerService', [
            'getPrimaryColor',
            'getSecondaryColor',
            'savePrimaryColor',
            'saveSecondaryColor',
            'saveToLocalStorage',
            'getLatestColors',
        ]);
        attributesManagerSpy.getPrimaryColor.and.returnValue(primaryColor);
        attributesManagerSpy.getSecondaryColor.and.returnValue(secondaryColor);
        attributesManagerSpy.getLatestColors.and.returnValue([primaryColor, secondaryColor]);
        attributesManagerSpy.saveToLocalStorage.and.callFake((key: string, attr) => {});

        TestBed.configureTestingModule({
            providers: [{ provide: AttributesManagerService, useValue: attributesManagerSpy }],
        });
        service = TestBed.inject(ColorService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should return good rgba string format', () => {
        expect(ColorService.getColorRGBA(primaryColor)).toEqual('rgba(20,20,20,1)');
    });

    it('should return good rgb string format', () => {
        expect(ColorService.getColorRGB(primaryColor)).toEqual('rgb(20,20,20)');
    });

    it('should set default primary color when none is saved locally', async (done: DoneFn) => {
        attributesManagerSpy.getPrimaryColor.and.callThrough();
        service['loadDefaultColor']();
        service.getPrimaryColor().subscribe((color: Color) => {
            expect(color).toEqual(service['DEFAULT_PRIMARY_COLOR']);
            done();
        });
    });

    it('should set default secondary color when none is saved locally', async (done: DoneFn) => {
        attributesManagerSpy.getSecondaryColor.and.callThrough();
        service['loadDefaultColor']();
        service.getSecondaryColor().subscribe((color: Color) => {
            expect(color).toEqual(service['DEFAULT_SECONDARY_COLOR']);
            done();
        });
    });

    it('should have primary color value', async (done: DoneFn) => {
        service.getPrimaryColor().subscribe((color: Color) => {
            expect(color).toEqual(primaryColor);
            done();
        });
    });

    it('should have secondary color value', async (done: DoneFn) => {
        service.getSecondaryColor().subscribe((color: Color) => {
            expect(color).toEqual(secondaryColor);
            done();
        });
    });

    it('should have return good latest colors', async (done: DoneFn) => {
        service.getLatestColors().subscribe((colors: Color[]) => {
            expect(colors).toEqual([primaryColor, secondaryColor]);
            done();
        });
    });

    it('should save primary color to local storage', () => {
        service.setPrimaryColor(primaryColor);
        expect(attributesManagerSpy.savePrimaryColor).toHaveBeenCalledWith(primaryColor);
    });

    it('should save secondary color to local storage', () => {
        service.setSecondaryColor(secondaryColor);
        expect(attributesManagerSpy.saveSecondaryColor).toHaveBeenCalledWith(secondaryColor);
    });

    it('should swap colors', () => {
        const primary = service['primaryColor'].value;
        const secondary = service['secondaryColor'].value;
        service.swapColors();
        expect(service['primaryColor'].value).toEqual(secondary);
        expect(service['secondaryColor'].value).toEqual(primary);
    });
});
