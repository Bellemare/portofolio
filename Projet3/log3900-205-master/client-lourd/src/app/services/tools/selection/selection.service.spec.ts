import { EventEmitter, Injectable, Renderer2 } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { PrevisualizationRectangleComponent } from '@app/app-components/previsualization-rectangle/previsualization-rectangle.component';
import { DrawingCanvasHelper } from '@app/classes/drawing-canvas-helper';
import { DrawingSelection } from '@app/classes/drawing/drawing-selection';
import { Vec2 } from '@app/classes/drawing/vec2';
import { MouseButton } from '@app/classes/mouse-button';
import { MouseEventWrapper } from '@app/classes/mouse-event-wrapper';
import { SelectionStates } from '@app/classes/selection/states';
import { RectSelectionDrawingService } from '@app/services/drawing-services/selection-drawing/rect-selection-drawing.service';
import { ResizeService } from '@app/services/resize/resize.service';
import { of } from 'rxjs';
import { SelectionService } from './selection.service';
// tslint:disable:no-any
// tslint:disable:no-string-literal
// tslint:disable:max-file-line-count
@Injectable({
    providedIn: 'root',
})
class SelectionServiceMock extends SelectionService {
    protected initDashedPrevisualization(): void {
        this.selectionEditor.initDashedPrevisualization({ x: 10, y: 10 }, false);
    }
}

describe('SelectionService', () => {
    let service: SelectionService;
    let mouseEvent: MouseEventWrapper;
    let drawingElement: DrawingSelection;
    let keyBindingUnregisterSpy: jasmine.Spy<any>;
    let setValuesSpy: jasmine.Spy<any>;
    let previewDataChangedSpy: jasmine.Spy<any>;
    // tslint:disable-next-line:prefer-const
    let renderer: Renderer2;

    beforeEach(() => {
        TestBed.configureTestingModule({});

        service = TestBed.inject(SelectionServiceMock);
        service['arrowMovementSubscription'] = of().subscribe();
        keyBindingUnregisterSpy = spyOn<any>(service, 'unregisterKeyBindings').and.callThrough();
        setValuesSpy = spyOn<any>(service, 'setInitialValues').and.callThrough();
        previewDataChangedSpy = spyOn<any>(service, 'previewDataChanged');
        service['TOOL_DRAWING_SERVICE'] = new RectSelectionDrawingService();
        const baseCanvas = document.createElement('canvas');
        DrawingCanvasHelper.baseCanvas = baseCanvas;

        drawingElement = {
            imageData: document.createElement('canvas'),
            state: SelectionStates.EditingSelection,
            size: { x: 0, y: 0 },
            startPosition: { x: 10, y: 11 },
            position: { x: 61, y: 61 },
        } as DrawingSelection;

        mouseEvent = new MouseEventWrapper({
            offsetX: 10,
            offsetY: 11,
            movementX: 51,
            movementY: 50,
            button: MouseButton.Left,
        } as MouseEvent);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should return right cursor', () => {
        service['previewDrawingElement'] = { state: SelectionStates.EditingSelection } as DrawingSelection;
        spyOn(service['selectionEditor'], 'isInPreviewRect').and.returnValue(true);
        expect(service.cursor).toEqual('pointer');
    });

    it('should set previewState to waiting register keyBindings and listen to arrow mouvement', () => {
        const registerSpy = spyOn(service['keyBindingService'], 'registerKeybind');
        spyOn<any>(service, 'listenArrowMovement');
        service.onInit();
        service['previewDrawingElement'].state = SelectionStates.WaitingSelection;
        expect(registerSpy).toHaveBeenCalled();
        expect(service['listenArrowMovement']).toHaveBeenCalled();
        expect(service['previewDrawingElement'].state).toEqual(SelectionStates.WaitingSelection as number);
    });

    it('should have a selectionEditor that destroy the preview rectangle onDestroy', () => {
        spyOn(service['selectionEditor'], 'finishSelection');
        spyOn<any>(service, 'confirmSelection').and.callThrough();
        spyOn<any>(service, 'clearData');
        service['previewDrawingElement'].state = SelectionStates.EditingSelection;
        service.onDestroy();
        expect(keyBindingUnregisterSpy).toHaveBeenCalled();
        expect(service['confirmSelection']).toHaveBeenCalled();
        expect(service['clearData']).toHaveBeenCalled();
        expect(service['selectionEditor'].finishSelection).toHaveBeenCalled();
    });

    it('should get if it is drawing', () => {
        service['previewDrawingElement'] = drawingElement;
        expect(service.isDrawing).toEqual(true);
    });

    it('should get if it can selectAll', () => {
        service['previewDrawingElement'] = drawingElement;
        service['mouseDown'] = true;
        service['previewDrawingElement'].state = SelectionStates.EditingSelection;
        expect(service.canSelectAll).toEqual(false);
        service['previewDrawingElement'].state = SelectionStates.WaitingSelection;
        expect(service.canSelectAll).toEqual(true);
    });

    it('should set squared to true if shift is pressed', () => {
        spyOn<any>(service, 'setSquared');
        service['setSquared'](true);
        expect(service['setSquared']).toHaveBeenCalledWith(true);
    });

    it('should set squared to false if shift is released', () => {
        spyOn<any>(service, 'setSquared');
        service['setSquared'](false);
        expect(service['setSquared']).toHaveBeenCalledWith(false);
    });

    it('should listen to arrow movement and setDrawingElement', async (done: DoneFn) => {
        const DELAY = 100;
        const selectionEditorSpy = spyOn(service['selectionEditor'], 'listenArrowMovement');
        const eventEmitter = new EventEmitter<void>();
        const observable = eventEmitter.asObservable();
        const setElementRectSpy = spyOn(service['selectionEditor'], 'setDrawingElementRect');
        selectionEditorSpy.and.returnValue(observable);
        service['listenArrowMovement']();
        observable.subscribe(() => {
            setTimeout(() => {
                expect(setElementRectSpy).toHaveBeenCalled();
                expect(previewDataChangedSpy).toHaveBeenCalled();
                done();
            }, DELAY);
        });
        eventEmitter.emit();
    });

    it('setSquared should updatePreview if is true ', () => {
        const updatePreviewSpy = spyOn<any>(service, 'updatePreview').and.callThrough();
        const lastAbsCoord: Vec2 = { x: 35, y: 15 };
        service['previewDrawingElement'] = drawingElement;
        service.onInit();
        service.setDefaultProperties();
        service['previewDrawingElement'].state = SelectionStates.WaitingSelection;
        service['setSquared'](true);
        expect(updatePreviewSpy).not.toHaveBeenCalled();
        service['previewDrawingElement'].state = SelectionStates.Selecting;
        service['setSquared'](true);
        expect(updatePreviewSpy).not.toHaveBeenCalled();
        service['mouseDown'] = true;
        service['lastRelativeCoord'] = lastAbsCoord;
        service['setSquared'](true);
        expect(updatePreviewSpy).toHaveBeenCalledWith(lastAbsCoord);
    });

    it('stopSelection should destroy preview, setInitialValues and switch selection state', () => {
        const destroySpy = spyOn(service['selectionEditor'], 'finishSelection');
        service['previewDrawingElement'].state = SelectionStates.EditingSelection;
        service['stopSelection']();
        expect(destroySpy).not.toHaveBeenCalled();
        service['previewDrawingElement'].state = SelectionStates.Selecting;
        service['stopSelection']();
        expect(setValuesSpy).toHaveBeenCalled();
        expect(destroySpy).toHaveBeenCalled();
        expect(service['previewDrawingElement'].state).toEqual(SelectionStates.WaitingSelection as number);
    });

    it('selectAll should set initial values, initDashed preview ', () => {
        spyOn<any>(service, 'initDashedPrevisualization').and.callFake(() => {
            service['selectionEditor']['dashedPrevisualization'] = new PrevisualizationRectangleComponent(renderer);
            return;
        });
        spyOn(service['selectionEditor'], 'initGlobalPrevisualization').and.callFake(() => {
            service['selectionEditor']['globalPrevisualization'] = new PrevisualizationRectangleComponent(renderer);
            return;
        });
        spyOn<any>(service, 'confirmSelection');
        const initImageSpy = spyOn(service, 'initImageData');
        service['previewDrawingElement'] = drawingElement;
        service['previewDrawingElement'].state = SelectionStates.EditingSelection;
        service['mouseDown'] = false;
        service.selectAll();
        expect(service['confirmSelection']).toHaveBeenCalled();
        expect(setValuesSpy).toHaveBeenCalled();
        expect(service['previewDrawingElement'].state).toEqual(SelectionStates.EditingSelection as number);
        expect(initImageSpy).toHaveBeenCalled();
    });

    it('onMouseDown should set mouseDown to true on left click', () => {
        service.onMouseDown(mouseEvent);
        expect(service['mouseDown']).toEqual(true);
    });

    it('should reset properly', () => {
        const destroySpy = spyOn(service['selectionEditor'], 'finishSelection');
        service.reset();
        expect(destroySpy).toHaveBeenCalled();
    });

    it('onMouseDown should confirmSelection if not in selection area ', () => {
        const confirmSpy = spyOn<any>(service, 'confirmSelection');
        service['isInSelectionArea'] = false;
        service.onMouseDown(mouseEvent);
        expect(confirmSpy).toHaveBeenCalled();
    });

    it('onMouseDown should setInitialValues, mouseCoordDown and switch state to selecting when WaitingSelection ', () => {
        const spy = spyOn(ResizeService, 'isInCanvas');
        spy.and.returnValue(true);
        service['previewDrawingElement'] = drawingElement;
        service.onMouseDown(mouseEvent);
        expect(setValuesSpy).toHaveBeenCalled();
        expect(mouseEvent.relativePosition).toEqual(mouseEvent.relativePosition);
        expect(service['previewDrawingElement'].state).toEqual(SelectionStates.Selecting as number);
    });

    it('onMouseDown should start editing when EditingSelection ', () => {
        const startEditingSelectionSpy = spyOn(service['selectionEditor'], 'setStartMovingPosition');
        spyOn(service['selectionEditor'], 'isInPreviewRect').and.returnValue(true);
        service['previewDrawingElement'] = drawingElement;
        service['previewDrawingElement'].state = SelectionStates.EditingSelection;
        service.onMouseDown(mouseEvent);
        expect(startEditingSelectionSpy).toHaveBeenCalledWith(mouseEvent.absolutePosition);
        expect(service['previewDrawingElement'].state).toEqual(SelectionStates.EditingSelection as number);
    });

    it('OnMouseMove should initDashedPrevisualization if not already set on Selecting', () => {
        const initDashedPrevisualizationSpy = spyOn<any>(service, 'initDashedPrevisualization');
        spyOn<any>(service, 'updatePreview');
        initDashedPrevisualizationSpy.and.returnValue(false);
        service['mouseDown'] = true;
        service['previewDrawingElement'] = drawingElement;
        service['previewDrawingElement'].state = SelectionStates.Selecting;
        service.onMouseMove(mouseEvent);
        expect(initDashedPrevisualizationSpy).toHaveBeenCalled();
    });

    it('OnMouseMove should updatePreview and previewDataChanged on Selecting', () => {
        const updateMovingPreviewSpy = spyOn(service['selectionEditor'], 'updateMovingPreview');
        service['mouseDown'] = true;
        service['previewDrawingElement'] = drawingElement;
        service['previewDrawingElement'].state = SelectionStates.EditingSelection;
        service.onMouseMove(mouseEvent);
        expect(updateMovingPreviewSpy).toHaveBeenCalled();
        expect(previewDataChangedSpy).toHaveBeenCalled();
    });

    it('OnMouseMove should do default if not Selecting or EditingSelection', () => {
        service['mouseDown'] = true;
        service['previewDrawingElement'] = drawingElement;
        service['previewDrawingElement'].state = SelectionStates.ConfirmSelection;
        service.onMouseMove(mouseEvent);
        expect(service['previewDrawingElement'].state).toEqual(SelectionStates.WaitingSelection as number);
    });

    it('OnMouseUp should do nothing if not Selecting', () => {
        service['previewDrawingElement'] = drawingElement;
        service['previewDrawingElement'].state = SelectionStates.ConfirmSelection;
        service.onMouseUp();
        expect(service['mouseDown']).toEqual(false);
        expect(service['previewDrawingElement'].state).toEqual(SelectionStates.ConfirmSelection as number);
    });

    it('OnMouseUp should set previewDrawingElement state to waiting on Selecting if size = 0', () => {
        const spy = spyOn(service['selectionEditor'], 'setDrawingElementRect');
        service['previewDrawingElement'] = drawingElement;
        service['previewDrawingElement'].state = SelectionStates.Selecting;
        service.onMouseUp();
        expect(spy).toHaveBeenCalledWith(service['previewDrawingElement']);
        expect(service['previewDrawingElement'].state).toEqual(SelectionStates.WaitingSelection as number);
    });

    it('OnMouseUp should set previewDrawingElement state to waiting on Selecting if size = 0', () => {
        spyOn(service['selectionEditor'], 'setDrawingElementRect').and.callFake(() => {
            return;
        });
        spyOn(service['selectionEditor'], 'initGlobalPrevisualization').and.callFake(() => {
            return;
        });
        const initImageSpy = spyOn(service, 'initImageData');
        service['previewDrawingElement'] = drawingElement;
        service['previewDrawingElement'].state = SelectionStates.Selecting;
        service['previewDrawingElement'].size = { x: 5, y: 5 };
        const initResize = spyOn(service['selectionResize'], 'initResize');
        const onInitSpy = spyOn(service['selectionResize'], 'onInit');
        service.onMouseUp();
        expect(initImageSpy).toHaveBeenCalled();
        expect(initResize).toHaveBeenCalled();
        expect(onInitSpy).toHaveBeenCalled();
        expect(service['previewDrawingElement'].startPosition).toEqual(drawingElement.startPosition);
        expect(service['previewDrawingElement'].state).toEqual(SelectionStates.EditingSelection as number);
    });

    it('updatePreview should call min to change mouseCoord if relativePosition is not in canvas', () => {
        const relativePos = { x: -1, y: 102 };
        const spyMin = spyOn(Math, 'min');
        service['mouseDownCoord'] = { x: 0, y: 0 };
        service['updatePreview'](relativePos);
        expect(spyMin).toHaveBeenCalled();
    });

    it('clampMousecoord should modify y value if distanceX < distanceY', () => {
        const mouseCoord: Vec2 = { x: 10, y: 10 };
        service['mouseDownCoord'] = { x: 10, y: 0 };
        expect(service['clampMouseCoord'](mouseCoord)).toEqual({ x: 10, y: 0 });
    });

    it('initImageData should set previewDrawingElement imageData to imageData', () => {
        const imageData = document.createElement('canvas');
        spyOn(service['TOOL_DRAWING_SERVICE'], 'getSelectionData').and.returnValue(imageData);
        service.initImageData();
        expect(service['previewDrawingElement'].imageData).toEqual(imageData);
    });

    it('should handle copy event correctly', () => {
        const element = {
            state: SelectionStates.EditingSelection,
            imageData: document.createElement('canvas'),
        } as DrawingSelection;
        const setSelectionSpy = spyOn(service['selectionClipboard'], 'setSelection');
        service['previewDrawingElement'] = element;
        service['selectionRequestEvent']();
        expect(setSelectionSpy).toHaveBeenCalledWith(element.imageData as HTMLCanvasElement, service, element);
    });

    it('should handle paste event correctly', () => {
        const element = {
            state: SelectionStates.EditingSelection,
        } as DrawingSelection;
        const setDeleteSpy = spyOn(service['selectionClipboard'], 'setDeleteConfig').and.callThrough();
        const confirmSelection = spyOn<any>(service, 'confirmSelection');
        service['previewDrawingElement'] = element;
        service['selectionDeleteEvent']();
        expect(setDeleteSpy).toHaveBeenCalled();
        expect(confirmSelection).toHaveBeenCalled();
        expect(element.imageData).toEqual(undefined);
    });

    it('should handle on create correctly', () => {
        const createSpy = spyOn(service['selectionClipboard'], 'onCreate');
        service.onCreate();
        expect(createSpy).toHaveBeenCalled();
    });

    it('should handle on delete correctly', () => {
        const deleteSpy = spyOn(service['selectionClipboard'], 'onDelete');
        service.onDelete();
        expect(deleteSpy).toHaveBeenCalled();
    });

    it('should handle on paste correctly', () => {
        const previsuSpy = spyOn<any>(service, 'initDashedPrevisualization');
        const initGlobalPrevisuSpy = spyOn(service['selectionEditor'], 'initGlobalPrevisualization');
        const initResize = spyOn(service['selectionResize'], 'initResize');
        const onInitSpy = spyOn(service['selectionResize'], 'onInit');
        const element = {
            state: SelectionStates.EditingSelection,
            position: { x: 1, y: 1 },
            size: { x: 1, y: 1 },
        } as DrawingSelection;
        service['previewDrawingElement'] = element;
        const confirmSpy = spyOn<any>(service, 'confirmSelection');
        const setPasteConfigSpy = spyOn(service['selectionClipboard'], 'setPasteConfig').and.callThrough();
        service['selectionPasteEvent']();
        expect(initResize).toHaveBeenCalled();
        expect(onInitSpy).toHaveBeenCalled();
        expect(previsuSpy).toHaveBeenCalled();
        expect(initGlobalPrevisuSpy).toHaveBeenCalled();
        expect(confirmSpy).toHaveBeenCalled();
        expect(setPasteConfigSpy).toHaveBeenCalled();
        expect(previewDataChangedSpy).toHaveBeenCalled();
    });

    it('should enable arrow events on mouse up in editing state', () => {
        const spy = spyOn(service['selectionEditor'], 'registerArrowKeybinds');
        service['previewDrawingElement'] = { state: SelectionStates.EditingSelection } as DrawingSelection;
        service.onMouseUp();
        expect(spy).toHaveBeenCalled();
    });
});
