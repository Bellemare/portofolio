import { EventEmitter, Injectable } from '@angular/core';
import { DrawingCanvasHelper } from '@app/classes/drawing-canvas-helper';
import { BoundingBox } from '@app/classes/drawing/bounding-box';
import { Color } from '@app/classes/drawing/color';
import { DrawingElement } from '@app/classes/drawing/drawing-element';
import { DrawingEvent } from '@app/classes/drawing/drawing-event';
import { DrawingEventWrapper } from '@app/classes/drawing/drawing-event-wrapper';
import { DrawingShape } from '@app/classes/drawing/drawing-shape';
import { DrawingText } from '@app/classes/drawing/drawing-text';
import { ToolType } from '@app/classes/drawing/tool-type';
import { Transformation } from '@app/classes/drawing/transformation';
import { Vec2 } from '@app/classes/drawing/vec2';
import { MouseEventWrapper } from '@app/classes/mouse-event-wrapper';
import { SelectionStates } from '@app/classes/selection/states';
import { LayerEventType } from '@app/classes/socket/layer-event-type';
import { LayerPreviewEventPayload } from '@app/classes/socket/layer-preview-event-payload';
import { LayerTextEventPayload } from '@app/classes/socket/layer-text-event-payload';
import { ToolProperties } from '@app/classes/tool-properties/tool-properties';
import { ToolProperty } from '@app/classes/tool-properties/tool-property';
import { AttributesManagerService } from '@app/services/api/attributes-manager.service';
import { CanvasSizeService } from '@app/services/canvas-size/canvas-size.service';
import { CouchesService } from '@app/services/couches/couches.service';
import { KeyBindingResolverService } from '@app/services/key-binding/key-binding-resolver.service';
import { SelectionEditorService } from '@app/services/selection/selection-editor.service';
import { SelectionResizeService } from '@app/services/selection/selection-resize.service';
import { SynchronizerService } from '@app/services/synchronization/synchronizer.service';
import { ColorService } from '@app/services/tools/color.service';
import { ToolService } from '@app/services/tools/tool.service';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';
import { faVectorSquare } from '@fortawesome/free-solid-svg-icons';
import { Observable, Subscription } from 'rxjs';
import { TextService } from '../text.service';

@Injectable({
    providedIn: 'root',
})
export class SelectionService extends ToolService {
    readonly TOOL_ICON: IconDefinition = faVectorSquare;
    readonly TOOL_LABEL: string = 'Sélection';
    readonly KEYBIND: string = 'r';
    readonly TOOL_NAME: string = 'selection';
    readonly TOOL_TYPE: ToolType = ToolType.SELECTION;

    protected isInSelectionArea: boolean = false;
    protected subscriptions: Subscription[] = [];
    protected mouseMovePos: Vec2;

    protected startPosition: Vec2;

    protected currentState: SelectionStates;

    protected lastSize: Vec2|undefined;
    selectedDrawingEvent: DrawingEventWrapper|undefined;

    protected isQuickSelect: boolean = false;

    private clickTimeout: any;

    protected endSelectionEvent: EventEmitter<boolean> = new EventEmitter();
    onRedrawSelectionEvent: () => void;
    onPreviewSelectionEvent: (selection: DrawingEventWrapper|undefined) => void;
    onSelectEvent: (selection: DrawingEventWrapper|undefined, layer: number) => void;

    constructor(
        protected attributesManagerService: AttributesManagerService,
        protected colorService: ColorService,
        protected keyBindingService: KeyBindingResolverService,
        protected selectionEditor: SelectionEditorService,
        protected selectionResize: SelectionResizeService,
        protected couchesService: CouchesService,
        protected synchronizerService: SynchronizerService,
        protected textService: TextService,
    ) {
        super(attributesManagerService, colorService, keyBindingService);
        synchronizerService.layerSelectEvent = this.selectDrawingEvent.bind(this);
        synchronizerService.layerDeleteEvent = () => {
            this.selectedDrawingEvent = undefined;
            this.finishSelection(true);
        }
    }

    get selectedLayer(): number {
        if (!this.selectedDrawingEvent)
            return -1;    

        return this.couchesService.getDrawingIndex(this.selectedDrawingEvent);
    }

    get state(): SelectionStates {
        return this.currentState;
    }

    get isDrawing(): boolean {
        return false;
    }

    get hasSelection(): boolean {
        return this.selectedDrawingEvent != undefined;
    }

    get selectionHasText(): boolean {
        return this.hasSelection && (this.selectedDrawingEvent?.drawingEvent?.tool_type == ToolType.ELLIPSE || this.selectedDrawingEvent?.drawingEvent?.tool_type == ToolType.RECTANGLE);
    }

    get cursor(): string {
        const isHoveringSelection = this.state === SelectionStates.EditingSelection && this.mouseMovePos != undefined && this.selectionEditor.isInPreviewRect(this.mouseMovePos);
        return isHoveringSelection ? 'pointer' : '';
    }

    protected getPreviewTransform(flipX: boolean = false, flipY: boolean = false): Transformation {
        const boundingBox = this.selectedDrawingEvent?.drawingEvent?.data.getEditBoundingBox() as BoundingBox;
        return {
            dx: boundingBox.x,
            dy: boundingBox.y,
            dw: boundingBox.w,
            dh: boundingBox.h,
            flip_x: flipX,
            flip_y: flipY
        }
    }

    protected listenColorChanges(): void {
        this.colorService.getPrimaryColor().subscribe((color: Color) => {
            if (this.selectedDrawingEvent?.drawingEvent) {
                this.selectedDrawingEvent.drawingEvent.stroke_color = color;
                this.syncLayerPreview(this.getPreviewTransform());
                this.renderTransformation();
            }
        });
        this.colorService.getSecondaryColor().subscribe((color: Color) => {
            if (this.selectedDrawingEvent?.drawingEvent) {
                this.selectedDrawingEvent.drawingEvent.fill_color = color;
                this.syncLayerPreview(this.getPreviewTransform());
                this.renderTransformation();
            }
        });
        this.colorService.getTextColor().subscribe((color: Color) => {
            if (this.selectedDrawingEvent?.drawingEvent) {
                this.selectedDrawingEvent.drawingEvent.text_color = color;
                this.syncLayerPreview(this.getPreviewTransform());
                this.renderTransformation();
            }
        });
    }

    listenEndSelection(): Observable<boolean> {
        return this.endSelectionEvent.asObservable();
    }

    onInit(isQuickSelect: boolean = false): void {
        super.onInit();
        this.keyBindingService.registerKeybind({
            keybind: 'DELETE',
            callback: this.deleteSelection.bind(this)
        });
        this.isQuickSelect = isQuickSelect;
        this.currentState = SelectionStates.WaitingSelection;
        this.listenResizeEvents();
    }

    onDestroy(): void {
        if (this.selectedDrawingEvent != undefined) {
            this.finishSelection();
        }
        super.onDestroy();
        this.clearData();
        this.keyBindingService.unregisterKeybind("DELETE");
        this.lastSize = undefined;
        this.selectionEditor.finishSelection();
        this.subscriptions.forEach((sub: Subscription) => sub.unsubscribe());
    }

    reset(): void {
        this.selectionEditor.finishSelection();
        this.clearData();
    }

    private syncLayerText(): void {
        if (!this.selectionHasText) {
            return;
        }
        const text = (this.selectedDrawingEvent?.drawingEvent?.data as DrawingShape).text as DrawingText;

        const payload: LayerTextEventPayload = {
            text: text,
            id: this.selectedDrawingEvent?.drawingEvent?.id as number,
            layer: this.selectedLayer,
        }
        this.synchronizerService.layerTextEvent(payload);
    }

    private syncLayerPreview(transform: Transformation): void {
        const payload: LayerPreviewEventPayload = {
            transform: transform,
            id: this.selectedDrawingEvent?.drawingEvent?.id as number,
            attributes: this.selectedDrawingEvent?.drawingEvent?.attributes as ToolProperties,
            stroke_color: this.selectedDrawingEvent?.drawingEvent?.stroke_color as Color,
            fill_color: this.selectedDrawingEvent?.drawingEvent?.fill_color as Color,
            text_color: this.selectedDrawingEvent?.drawingEvent?.text_color as Color,
            layer: this.selectedLayer,
        }
        this.synchronizerService.layerPreviewEvent(payload);
    }

    deleteSelection(): void {
        if (this.selectedDrawingEvent == undefined)
            return;

        this.synchronizerService.layerEvent(LayerEventType.DELETE, this.selectedDrawingEvent.drawingEvent as DrawingEvent, this.selectedLayer);
    }

    setProperty(key: string, value: ToolProperty): void {
        super.setProperty(key, value);
        if (this.selectedDrawingEvent?.drawingEvent?.data.bounding_box) {
            const boundingBox = this.selectedDrawingEvent.drawingEvent.data.getEditBoundingBox() as BoundingBox;
            this.selectionEditor.setSize({
                x: boundingBox.w,
                y: boundingBox.h,
            });
            this.resizeDrawingEvent();
        }
        this.renderTransformation();
    }

    selectDrawingEvent(evt: DrawingEventWrapper): void {
        this.finishSelection();
        this.selectedDrawingEvent = evt;
        this.selectedDrawingEvent.isSelected = true;
        this.currentAttributes = this.selectedDrawingEvent.drawingEvent?.attributes as ToolProperties;
        this.colorService.setPrimaryColor(evt.drawingEvent?.stroke_color as Color);
        this.colorService.setSecondaryColor(evt.drawingEvent?.fill_color as Color);
        this.colorService.setTextColor(evt.drawingEvent?.text_color as Color);
        this.currentState = SelectionStates.EditingSelection;
        if (!this.isQuickSelect) {
            this.selectedDrawingEvent.render.emit();
            this.onRedrawSelectionEvent();
        }
        this.renderTransformation();

        this.initPreview();
    }

    private initPreview(): void {
        const boundingBox = this.selectedDrawingEvent?.drawingEvent?.data.getEditBoundingBox() as BoundingBox;
        const position: Vec2 = { x: boundingBox.x, y: boundingBox.y };
        const size: Vec2 = { x: boundingBox.w, y: boundingBox.h };
        if (size.x > 0 && size.y > 0) {
            this.selectionEditor.initGlobalPrevisualization(this.onSelectionClick.bind(this));
            const bottomLeft: Vec2 = { x: position.x + size.x - 1, y: position.y + size.y - 1 };
            this.selectionEditor.setRectFromPositions(
                DrawingCanvasHelper.relativePositionToAbsolute(position),
                DrawingCanvasHelper.relativePositionToAbsolute(bottomLeft)
            );
            this.selectionResize.initResize(this.selectionEditor.globalPrevisualizationComponent);
            this.selectionResize.onInit();
            this.selectionEditor.setStartMovingPosition(this.startPosition);
            this.currentState = SelectionStates.EditingSelection;
        } else {
            this.currentState = SelectionStates.WaitingSelection;
        }
        if (this.onSelectEvent)
            this.onSelectEvent(this.selectedDrawingEvent, this.selectedLayer);
    }

    private listenResizeEvents(): void {
        this.selectionResize.onSizeChange = this.resizeDrawingEvent.bind(this);
    }

    private resizeDrawingEvent(): void {
        this.textService.pause();
        const boundingBox = this.selectedDrawingEvent?.drawingEvent?.data.getEditBoundingBox();
        const newSize = this.selectionEditor.currentSize;
        if (boundingBox != undefined) {
            const flipX = this.lastSize != undefined && ((newSize.x <= 0 && this.lastSize.x > 0) || (newSize.x > 0 && this.lastSize.x <= 0));
            const flipY = this.lastSize != undefined && ((newSize.y <= 0 && this.lastSize.y > 0) || (newSize.y > 0 && this.lastSize.y <= 0));
            const dw = newSize.x / boundingBox.w;
            const dh = newSize.y / boundingBox.h;
            const resized = this.selectedDrawingEvent?.drawingEvent?.data.applyScaling(dw, dh, flipX, flipY, this.selectedDrawingEvent.drawingEvent.attributes);
            if (resized) {
                this.lastSize = newSize;
                this.translateDrawingEvent();
                const data = this.selectedDrawingEvent?.drawingEvent?.data as DrawingElement;
                data.bounding_box = undefined;
                this.renderTransformation();
                this.syncLayerPreview(this.getPreviewTransform(flipX, flipY));
            }
        }
    }

    private translateDrawingEvent(): Vec2 {
        this.textService.pause();
        const attributes = this.selectedDrawingEvent?.drawingEvent?.attributes as ToolProperties;
        const dataTopLeftCorner = this.selectedDrawingEvent?.drawingEvent?.data.getTopLeftPosition(attributes) as Vec2;
        const position = this.selectionEditor.currentPosition;
        const size = this.selectionEditor.currentSize;
        let editorTopLeft = { x: Math.min(position.x, position.x + size.x), y: Math.min(position.y, position.y + size.y) };
        editorTopLeft = DrawingCanvasHelper.absolutePositionToRelative(editorTopLeft);
        const dx = editorTopLeft.x - dataTopLeftCorner.x;
        const dy = editorTopLeft.y - dataTopLeftCorner.y;
        this.selectedDrawingEvent?.drawingEvent?.data.applyTranslation(dx, dy);
        const data = this.selectedDrawingEvent?.drawingEvent?.data as DrawingElement;
        data.bounding_box = undefined;

        return { x: dx, y: dy };
    }

    protected renderTransformation(): void {
        if (this.onPreviewSelectionEvent != undefined) {
            this.onPreviewSelectionEvent(this.selectedDrawingEvent);
        }
    }

    protected finishSelection(changeTool: boolean = false): void {
        this.currentState = SelectionStates.WaitingSelection;
        this.selectionEditor.finishSelection();
        this.selectionResize.onDestroy();
        this.lastSize = undefined;
        if (this.selectedDrawingEvent) {
            this.synchronizerService.layerEvent(LayerEventType.CHANGE, this.selectedDrawingEvent.drawingEvent as DrawingEvent, this.selectedLayer);
        }
        this.selectedDrawingEvent = undefined;
        this.textService.reset();
        if (this.onSelectEvent)
            this.onSelectEvent(undefined, -1);
        this.endSelectionEvent.emit(changeTool && this.isQuickSelect);
    }

    private findElement(position: Vec2): DrawingEventWrapper|undefined {
        let evt;
        if (CanvasSizeService.isInCanvas(position)) {
            evt = this.couchesService.findDrawingEvent(position);
        }
        return evt;
    }

    onMouseDown(event: MouseEventWrapper): void {
        this.mouseDown = event.isLeftButton;
        if (this.mouseDown) {
            switch (this.state) {
                case SelectionStates.WaitingSelection:
                    if (this.isQuickSelect) {
                        return;
                    }
                    let layer = this.findElement(event.relativePosition);
                    if (layer != undefined) {
                        this.currentState = SelectionStates.EditingSelection;
                        this.synchronizerService.layerEvent(LayerEventType.SELECT, layer.drawingEvent as DrawingEvent, this.couchesService.getDrawingIndex(layer));
                        this.startPosition = event.absolutePosition;
                    }
                    break;
                case SelectionStates.EditingSelection:
                    this.onSelectionClick(event);
                    break;
                default:
                    this.currentState = SelectionStates.WaitingSelection;
                    break;
            }
        }
    }

    private onSelectionClick(event: MouseEventWrapper): void {
        this.mouseDown = event.isLeftButton;
        this.isInSelectionArea = this.selectionEditor.isInPreviewRect(event.absolutePosition);
        if (!this.isInSelectionArea) {  
            this.handleClickOutside();
        } else {
            this.handleSelectionClick(event);
        }
    }

    private handleClickOutside(): void {
        this.finishSelection(true);
    }

    private handleSelectionClick(event: MouseEventWrapper): void {
        if (this.clickTimeout != undefined && this.selectedDrawingEvent != undefined && this.selectionHasText) {
            this.clickTimeout = undefined;
            const data = this.selectedDrawingEvent.drawingEvent?.data as DrawingShape;
            const event = this.selectedDrawingEvent.drawingEvent as DrawingEvent;
            const pos = this.selectionEditor.currentPosition;
            const size = this.selectionEditor.currentSize
            this.textService.init(event, pos, size, () => {
                this.syncLayerText();
                data.bounding_box = undefined;
                this.renderTransformation();
            });
            return;
        }
        this.clickTimeout = setTimeout(() => {
            this.clickTimeout = undefined;
        }, 500);

        this.selectionEditor.setStartMovingPosition(event.absolutePosition);
    }

    onMouseMove(event: MouseEventWrapper): void {
        this.mouseMovePos = event.absolutePosition;
        if (this.mouseDown) {
            this.clickTimeout = undefined;
            switch (this.state) {
                case SelectionStates.EditingSelection:
                    this.selectionEditor.updateMovingPreview(event.absolutePosition);
                    this.translateDrawingEvent();
                    this.renderTransformation();
                    this.syncLayerPreview(this.getPreviewTransform());
                    break;
                default:
                    this.currentState = SelectionStates.WaitingSelection;
                    break;
            }
        }
    }

    onMouseUp(): void {
        this.textService.resume(this.selectionEditor.currentPosition, this.selectionEditor.currentSize);
        this.mouseDown = false;
    }
}
