import { TestBed } from '@angular/core/testing';
import { DrawingEllipse } from '@app/classes/drawing/drawing-ellipse';
import { EllipseService } from './ellipse.service';

// tslint:disable:no-string-literal
describe('EllipseService', () => {
    let service: EllipseService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(EllipseService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('updatePreviewData should correctly compute and set the previewDrawingElement attributes', () => {
        const expectedDrawingEllipse: DrawingEllipse = {
            x: 25,
            y: 50,
            radiusX: 15,
            radiusY: 30,
            rotation: 0,
            startAngle: 0,
            endAngle: 2 * Math.PI,
            anticlockwise: false,
        };
        const mouseDownCoord = { x: 10, y: 20 };
        const mouseCoord = { x: 40, y: 80 };

        service['mouseDownCoord'] = mouseDownCoord;
        service['updatePreviewData'](mouseCoord);

        expect(service['previewDrawingElement'].x).toEqual(expectedDrawingEllipse.x);
        expect(service['previewDrawingElement'].y).toEqual(expectedDrawingEllipse.y);
        expect(service['previewDrawingElement'].radiusX).toEqual(expectedDrawingEllipse.radiusX);
        expect(service['previewDrawingElement'].radiusY).toEqual(expectedDrawingEllipse.radiusY);
        expect(service['previewDrawingElement'].rotation).toEqual(expectedDrawingEllipse.rotation);
        expect(service['previewDrawingElement'].startAngle).toEqual(expectedDrawingEllipse.startAngle);
        expect(service['previewDrawingElement'].endAngle).toEqual(expectedDrawingEllipse.endAngle);
        expect(service['previewDrawingElement'].anticlockwise).toEqual(expectedDrawingEllipse.anticlockwise);
    });

    it('clearData should create new drawing elements', () => {
        service['clearData']();
        expect(service['previewDrawingElement']).toEqual(new DrawingEllipse());
        expect(service['drawingElement']).toEqual(new DrawingEllipse());
    });
});
