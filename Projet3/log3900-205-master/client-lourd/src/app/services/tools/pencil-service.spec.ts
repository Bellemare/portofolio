import { TestBed } from '@angular/core/testing';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { DrawingCanvasHelper } from '@app/classes/drawing-canvas-helper';
import { ToolClasses } from '@app/classes/drawing/tool-classes';
import { Vec2 } from '@app/classes/drawing/vec2';
import { MouseButton } from '@app/classes/mouse-button';
import { MouseEventWrapper } from '@app/classes/mouse-event-wrapper';
import { AutoSaveService } from '@app/services/api/auto-save.service';
import { DrawingService } from '@app/services/components/drawing/drawing.service';
import { KeyBindingResolverService } from '@app/services/key-binding/key-binding-resolver.service';
import { ResizeService } from '@app/services/resize/resize.service';
import { UndoRedoService } from '@app/services/undo-redo/undo-redo.service';
import { PencilService } from './pencil-service';

// tslint:disable:no-any
// tslint:disable:no-string-literal
describe('PencilService', () => {
    let service: PencilService;
    let mouseEvent: MouseEventWrapper;
    let canvasTestHelper: CanvasTestHelper;

    let drawingService: DrawingService;
    let keyBindingService: KeyBindingResolverService;

    let toolClassesSpy: jasmine.SpyObj<ToolClasses>;
    let drawSpy: jasmine.Spy<any>;
    let previewSpy: jasmine.Spy<any>;

    let baseCtxStub: CanvasRenderingContext2D;
    let previewCtxStub: CanvasRenderingContext2D;

    beforeEach(() => {
        toolClassesSpy = jasmine.createSpyObj('ToolClasses', ['getToolService']);
        keyBindingService = new KeyBindingResolverService();

        TestBed.configureTestingModule({
            providers: [{ provide: KeyBindingResolverService, useValue: keyBindingService }],
        });
        canvasTestHelper = TestBed.inject(CanvasTestHelper);
        baseCtxStub = canvasTestHelper.canvas.getContext('2d') as CanvasRenderingContext2D;
        previewCtxStub = canvasTestHelper.drawCanvas.getContext('2d') as CanvasRenderingContext2D;

        service = TestBed.inject(PencilService);

        toolClassesSpy.getToolService.and.returnValue(service);
        const autoSaveService = new AutoSaveService();
        const undoRedoService = new UndoRedoService(keyBindingService);
        drawingService = new DrawingService(
            keyBindingService,
            toolClassesSpy,
            undoRedoService,
            new ResizeService(undoRedoService, autoSaveService),
            autoSaveService,
        );
        drawSpy = spyOn<any>(service, 'dataChanged');
        previewSpy = spyOn<any>(service, 'previewDataChanged');

        DrawingCanvasHelper.baseCanvas = canvasTestHelper.canvas;
        drawingService['baseCtx'] = baseCtxStub;
        drawingService['previewCtx'] = previewCtxStub;

        mouseEvent = new MouseEventWrapper({
            offsetX: 25,
            offsetY: 25,
            button: MouseButton.Left,
        } as MouseEvent);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should set canvas properties from current properties', () => {
        service.setCanvasProperties(baseCtxStub);
        expect(baseCtxStub.lineWidth).toEqual(service['currentAttributes'].lineWidth);
        expect(baseCtxStub.lineCap).toEqual(service['DEFAULT_LINE_CAP']);
        expect(baseCtxStub.lineJoin).toEqual(service['DEFAULT_LINE_JOIN']);
        expect(baseCtxStub.miterLimit).toEqual(service['DEFAULT_MITER_LIMIT']);
    });

    it(' mouseDown should set mouseDownCoord to correct position', () => {
        const expectedResult: Vec2 = { x: 25, y: 25 };
        service.onMouseDown(mouseEvent);
        expect(service['mouseDownCoord']).toEqual(expectedResult);
    });

    it(' mouseDown should set mouseDown property to true on left click', () => {
        service.onMouseDown(mouseEvent);
        expect(service['mouseDown']).toEqual(true);
    });

    it(' mouseDown should set mouseDown property to false on right click', () => {
        const mouseEventRClick = new MouseEventWrapper({
            offsetX: 25,
            offsetY: 25,
            button: MouseButton.Right,
        } as MouseEvent);
        service.onMouseDown(mouseEventRClick);
        expect(service['mouseDown']).toEqual(false);
    });

    it(' onMouseUp should call drawing service', () => {
        service['mouseDownCoord'] = { x: 0, y: 0 };
        service['mouseDown'] = true;
        service['previewDrawingElement'].data = [service['mouseDownCoord']];

        service.onMouseUp(mouseEvent);
        expect(drawSpy).toHaveBeenCalled();
    });

    it(' onMouseEnter should unset mouse down if mouse down was already set', () => {
        service['mouseDownCoord'] = { x: 0, y: 0 };
        service['mouseDown'] = true;

        service.onMouseEnter(new MouseEventWrapper({ button: 0, buttons: 0 } as MouseEvent));
        expect(service['mouseDown']).toEqual(false);
    });

    it(' onMouseMove should call drawing service if mouse was already down', () => {
        service['mouseDownCoord'] = { x: 0, y: 0 };
        service['mouseDown'] = true;
        service['previewDrawingElement'].data = [service['mouseDownCoord']];

        service.onMouseMove(mouseEvent);
        expect(previewSpy).toHaveBeenCalled();
    });

    it(' onMouseMove should not call dawing service if mouse was not already down', () => {
        service['mouseDownCoord'] = { x: 0, y: 0 };
        service['mouseDown'] = false;

        expect(previewSpy).not.toHaveBeenCalled();
    });
});
