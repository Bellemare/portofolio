import { Injectable } from '@angular/core';
import { DrawingEllipse } from '@app/classes/drawing/drawing-ellipse';
import { ToolType } from '@app/classes/drawing/tool-type';
import { Vec2 } from '@app/classes/drawing/vec2';
import { Tooltip } from '@app/classes/tooltip';
import { AttributesManagerService } from '@app/services/api/attributes-manager.service';
import { EllipseDrawingService } from '@app/services/drawing-services/ellipse-drawing.service';
import { KeyBindingResolverService } from '@app/services/key-binding/key-binding-resolver.service';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faCircle } from '@fortawesome/free-solid-svg-icons';
import { ColorService } from './color.service';
import { ShapeService } from './shape.service';

@Injectable({
    providedIn: 'root',
})
export class EllipseService extends ShapeService {
    readonly TOOL_ICON: IconDefinition = faCircle;
    readonly TOOL_LABEL: string = 'Ellipse';
    readonly KEYBIND: string = '2';
    readonly TOOL_NAME: string = 'ellipse';
    readonly TOOL_TYPE: ToolType = ToolType.ELLIPSE;
    readonly TOOLTIPS: Tooltip[] = [
        {
            name: this.TOOL_LABEL,
            shortcut: this.KEYBIND,
        },
        {
            name: 'Cercle',
            shortcut: 'Shift',
        },
    ];

    protected previewDrawingElement: DrawingEllipse = new DrawingEllipse();
    protected drawingElement: DrawingEllipse = new DrawingEllipse();

    constructor(
        protected attributesManagerService: AttributesManagerService,
        protected colorService: ColorService,
        protected keyBindingService: KeyBindingResolverService,
        protected toolDrawingService: EllipseDrawingService,
    ) {
        super(attributesManagerService, colorService, keyBindingService);
    }

    protected updatePreviewData(mouseCoord: Vec2): void {
        this.previewDrawingElement.x = (mouseCoord.x + this.mouseDownCoord.x) / 2;
        this.previewDrawingElement.y = (mouseCoord.y + this.mouseDownCoord.y) / 2;
        this.previewDrawingElement.radius_x = Math.abs(mouseCoord.x - this.mouseDownCoord.x) / 2;
        this.previewDrawingElement.radius_y = Math.abs(mouseCoord.y - this.mouseDownCoord.y) / 2;
    }

    protected clearData(): void {
        this.previewDrawingElement = new DrawingEllipse();
        this.drawingElement = new DrawingEllipse();
    }
}
