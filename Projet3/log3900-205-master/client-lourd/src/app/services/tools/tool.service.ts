import { EventEmitter, Injectable } from '@angular/core';
import { Color } from '@app/classes/drawing/color';
import { DrawingElement } from '@app/classes/drawing/drawing-element';
import { DrawingType } from '@app/classes/drawing/drawing-type';
import { ToolType } from '@app/classes/drawing/tool-type';
import { Vec2 } from '@app/classes/drawing/vec2';
import { MouseEventWrapper } from '@app/classes/mouse-event-wrapper';
import { ToolProperties } from '@app/classes/tool-properties/tool-properties';
import { ToolProperty } from '@app/classes/tool-properties/tool-property';
import { Tooltip } from '@app/classes/tooltip';
import { AttributesManagerService } from '@app/services/api/attributes-manager.service';
import { ToolDrawingService } from '@app/services/drawing-services/tool-drawing.service';
import { KeyBindingResolverService } from '@app/services/key-binding/key-binding-resolver.service';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ColorService } from './color.service';

@Injectable({
    providedIn: 'root',
})
export abstract class ToolService {
    readonly TOOL_ICON: IconDefinition;
    readonly TOOL_LABEL: string = '';
    readonly KEYBIND: string = '';
    readonly TOOL_NAME: string = '';
    readonly TOOLTIPS: Tooltip[] = [];
    readonly CLEAR_ON_PREVIEW: boolean = true;
    readonly CURSOR: string = '';
    readonly TOOL_TYPE: ToolType = ToolType.UNDEFINED;

    protected toolDrawingService: ToolDrawingService = new ToolDrawingService();
    protected currentCursor: string = '';

    protected readonly DEFAULT_CONFIGURATION: ToolProperties = {
        line_width: 8,
        drawing_type: DrawingType.Stroke,
    };
    protected currentAttributes: ToolProperties;

    protected previewDrawingElement: DrawingElement = new DrawingElement();
    protected previewDataChange: BehaviorSubject<DrawingElement> = new BehaviorSubject<DrawingElement>(new DrawingElement());
    protected drawingElement: DrawingElement = new DrawingElement();
    protected dataChange: BehaviorSubject<DrawingElement> = new BehaviorSubject<DrawingElement>(new DrawingElement());

    protected attributesChanged: EventEmitter<void> = new EventEmitter<void>();
    protected mouseDownCoord: Vec2;
    protected mouseDown: boolean = false;

    protected primaryColor: Color;
    protected secondaryColor: Color;
    textColor: Color;

    constructor(
        protected attributesManagerService: AttributesManagerService,
        protected colorService: ColorService,
        protected keyBindingService: KeyBindingResolverService,
    ) {
        this.listenColorChanges();
    }

    protected listenColorChanges(): void {
        this.colorService.getPrimaryColor().subscribe((color: Color) => (this.primaryColor = color));
        this.colorService.getSecondaryColor().subscribe((color: Color) => (this.secondaryColor = color));
        this.colorService.getTextColor().subscribe((color: Color) => (this.textColor = color));
    }

    get toolTooltips(): Tooltip[] {
        return this.TOOLTIPS;
    }

    get isDrawing(): boolean {
        return this.mouseDown;
    }

    get drawingType(): DrawingType {
        return this.currentAttributes.drawing_type;
    }

    get strokeColor(): Color {
        return this.primaryColor;
    }

    get fillColor(): Color {
        return this.secondaryColor;
    }

    get currentToolDrawingService(): ToolDrawingService {
        return this.toolDrawingService;
    }

    get toolAttributes(): ToolProperties {
        return this.currentAttributes;
    }

    get cursor(): string {
        return this.currentCursor;
    }

    onCreate(): void {
        this.clearData();
    }

    onInit(): void {
        this.clearData();
        this.mouseDown = false;
        this.mouseDownCoord = { x: 0, y: 0 };
    }

    onDestroy(): void {
        this.previewDataChange = new BehaviorSubject<DrawingElement>(new DrawingElement());
        this.dataChange = new BehaviorSubject<DrawingElement>(new DrawingElement());
    }

    onDelete(): void {
        this.onDestroy();
    }

    // On désactive tslint no-empty ici, puisque la classe est abstraite et
    // on ne donne pas d'implémentation de base.
    // tslint:disable:no-empty
    onMouseDown(event?: MouseEventWrapper): void {}

    onMouseLeave(event?: MouseEventWrapper): void {}

    onMouseEnter(event?: MouseEventWrapper): void {}

    onMouseUp(event?: MouseEventWrapper): void {}

    onDoubleClick(event?: MouseEventWrapper): void {}

    onMouseWheel(event?: MouseEvent): void {}

    onMouseMove(event?: MouseEventWrapper): void {}

    setCanvasProperties(ctx: CanvasRenderingContext2D, properties: ToolProperties | null = null): void {}

    reset(): void {}

    protected clearData(): void {}
    // tslint:enable

    setDefaultProperties(): void {
        this.currentAttributes = this.DEFAULT_CONFIGURATION;
        this.attributesChanged.emit();
    }

    setProperty(key: string, value: ToolProperty): void {
        this.currentAttributes[key] = value;
        this.attributesManagerService.saveToLocalStorage<ToolProperties>(this.TOOL_NAME, this.currentAttributes);
        this.attributesChanged.emit();
    }

    listenAttributeChange(): Observable<void> {
        return this.attributesChanged.asObservable();
    }

    protected getSavedAttributes<T>(tool: string): T {
        return this.attributesManagerService.getProperties(tool);
    }

    getData(): Observable<DrawingElement> {
        return this.dataChange.asObservable();
    }

    getPreviewData(): Observable<DrawingElement> {
        return this.previewDataChange.asObservable();
    }

    protected previewDataChanged(): void {
        this.previewDataChange.next(this.previewDrawingElement);
    }

    protected dataChanged(): void {
        this.dataChange.next(this.drawingElement);
    }
}
