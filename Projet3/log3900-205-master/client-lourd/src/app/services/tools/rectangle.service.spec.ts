import { Renderer2 } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { PrevisualizationRectangleComponent } from '@app/app-components/previsualization-rectangle/previsualization-rectangle.component';
import { DrawingRect } from '@app/classes/drawing/drawing-rect';
import { DrawingType } from '@app/classes/drawing/drawing-type';
import { RectangleProperties } from '@app/classes/tool-properties/rectangle-properties';
import { RectangleService } from './rectangle.service';

// tslint:disable:no-string-literal
describe('RectangleService', () => {
    let service: RectangleService;
    let previewRect: PrevisualizationRectangleComponent;
    // tslint:disable-next-line:prefer-const
    let renderer: Renderer2;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(RectangleService);
        previewRect = new PrevisualizationRectangleComponent(renderer);
        service['previsualizationRect'] = previewRect;
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('updatePreviewData should set shouldFillInstead to true', () => {
        const mouseDownCoord = { x: 0, y: 0 };
        const mouseCoord = { x: 1, y: 1 };
        const attributes: RectangleProperties = { lineWidth: 5, drawingType: DrawingType.Stroke };
        service.setDefaultProperties();
        service.onInit();
        service['currentAttributes'] = attributes;
        service['mouseDownCoord'] = mouseDownCoord;
        service['updatePreviewData'](mouseCoord);
        expect(service['previewDrawingElement'].shouldFill).toBe(true);
    });

    it('updatePreviewData should set shouldFillInstead to false', () => {
        const mouseDownCoord = { x: 0, y: 0 };
        const mouseCoord = { x: 55, y: 55 };
        const attributes: RectangleProperties = { lineWidth: 5, drawingType: DrawingType.Filled };
        service.onInit();
        service['currentAttributes'] = attributes;
        service['mouseDownCoord'] = mouseDownCoord;
        service['updatePreviewData'](mouseCoord);
        expect(service['previewDrawingElement'].shouldFill).toBe(false);
    });

    it('updatePreviewData should set shouldFillInstead to true and test every condition', () => {
        const mouseDownCoord = { x: 0, y: 0 };
        const mouseCoord = { x: 55, y: 1 };
        const attributes: RectangleProperties = { lineWidth: 5, drawingType: DrawingType.Stroke };
        service.onInit();
        service['currentAttributes'] = attributes;
        service['mouseDownCoord'] = mouseDownCoord;
        service['updatePreviewData'](mouseCoord);
        expect(service['previewDrawingElement'].shouldFill).toBe(true);
    });

    it('clearData should create new drawing elements', () => {
        service['clearData']();
        expect(service['previewDrawingElement']).toEqual(new DrawingRect());
        expect(service['drawingElement']).toEqual(new DrawingRect());
    });
});
