import { Injectable } from '@angular/core';
import { DrawingRect } from '@app/classes/drawing/drawing-rect';
import { ToolType } from '@app/classes/drawing/tool-type';
import { Vec2 } from '@app/classes/drawing/vec2';
import { Tooltip } from '@app/classes/tooltip';
import { AttributesManagerService } from '@app/services/api/attributes-manager.service';
import { RectDrawingService } from '@app/services/drawing-services/rect-drawing.service';
import { KeyBindingResolverService } from '@app/services/key-binding/key-binding-resolver.service';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faSquare } from '@fortawesome/free-solid-svg-icons';
import { ColorService } from './color.service';
import { ShapeService } from './shape.service';

@Injectable({
    providedIn: 'root',
})
export class RectangleService extends ShapeService {
    readonly TOOL_ICON: IconDefinition = faSquare;
    readonly TOOL_LABEL: string = 'Rectangle';
    readonly KEYBIND: string = '1';
    readonly TOOL_NAME: string = 'rectangle';
    readonly TOOL_TYPE: ToolType = ToolType.RECTANGLE;
    readonly TOOLTIPS: Tooltip[] = [
        {
            name: this.TOOL_LABEL,
            shortcut: this.KEYBIND,
        },
        {
            name: 'Carré',
            shortcut: 'Shift',
        },
    ];

    protected previewDrawingElement: DrawingRect = new DrawingRect();
    protected drawingElement: DrawingRect = new DrawingRect();

    constructor(
        protected attributesManagerService: AttributesManagerService,
        protected colorService: ColorService,
        protected keyBindingService: KeyBindingResolverService,
        protected toolDrawingService: RectDrawingService,
    ) {
        super(attributesManagerService, colorService, keyBindingService);
    }

    protected updatePreviewData(mouseCoord: Vec2): void {
        const mouseCoordDiff: Vec2 = { x: mouseCoord.x - this.mouseDownCoord.x, y: mouseCoord.y - this.mouseDownCoord.y };
        this.previewDrawingElement.x = this.mouseDownCoord.x;
        this.previewDrawingElement.y = this.mouseDownCoord.y;
        this.previewDrawingElement.width = mouseCoordDiff.x;
        this.previewDrawingElement.height = mouseCoordDiff.y;
    }

    protected clearData(): void {
        this.previewDrawingElement = new DrawingRect();
        this.drawingElement = new DrawingRect();
    }
}
