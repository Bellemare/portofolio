import { Injectable, Renderer2 } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { PrevisualizationRectangleComponent } from '@app/app-components/previsualization-rectangle/previsualization-rectangle.component';
import { Color } from '@app/classes/drawing/color';
import { DrawingType } from '@app/classes/drawing/drawing-type';
import { Vec2 } from '@app/classes/drawing/vec2';
import { MouseButton } from '@app/classes/mouse-button';
import { MouseEventWrapper } from '@app/classes/mouse-event-wrapper';
import { ShapeProperties } from '@app/classes/tool-properties/shape-properties';
import { AppComponent } from '@app/components/app/app.component';
import { ShapeService } from './shape.service';

@Injectable({
    providedIn: 'root',
})
class ShapeServiceMock extends ShapeService {
    protected updatePreviewData(mouseCoord: Vec2): void {
        return;
    }
}
// tslint:disable:no-any
// tslint:disable:no-string-literal
describe('ShapeService', () => {
    let service: ShapeServiceMock;

    let expectedPrimaryColor: Color;
    let expectedSecondaryColor: Color;
    let expectedLastRelativeCoord: Vec2;
    let expectedLastAbsoluteCoord: Vec2;

    let mouseEventLClick: MouseEventWrapper;
    let mouseEventRClick: MouseEventWrapper;
    let mouseWrapperStub: MouseEventWrapper;

    let previewRect: PrevisualizationRectangleComponent;

    let appComponentCreateSpy: jasmine.Spy<<T>(appComponent: new () => T) => T>;
    let appComponentDestroySpy: jasmine.Spy<<T>(instance: T) => void>;

    let setConstraintSpy: jasmine.Spy<any>;

    let resetSpy: jasmine.Spy<any>;

    let clearDataSpy: jasmine.Spy<any>;
    let previewDataChangedSpy: jasmine.Spy<any>;
    let dataChangedSpy: jasmine.Spy<any>;
    let updatePreviewSpy: jasmine.Spy<any>;
    let updatePreviewDataSpy: jasmine.Spy<any>;
    // tslint:disable-next-line:prefer-const
    let renderer: Renderer2;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(ShapeServiceMock);

        mouseWrapperStub = new MouseEventWrapper({} as MouseEvent);

        previewRect = new PrevisualizationRectangleComponent(renderer);
        appComponentCreateSpy = spyOn(AppComponent, 'createComponent');
        appComponentDestroySpy = spyOn(AppComponent, 'destroyComponent');
        appComponentCreateSpy.and.returnValue(previewRect);

        mouseEventLClick = new MouseEventWrapper({
            offsetX: 20,
            offsetY: 25,
            button: MouseButton.Left,
            buttons: 1,
        } as MouseEvent);

        mouseEventRClick = new MouseEventWrapper({
            offsetX: 30,
            offsetY: 35,
            button: MouseButton.Right,
            buttons: 2,
        } as MouseEvent);

        expectedPrimaryColor = { r: 50, g: 150, b: 250, a: 1 };
        expectedSecondaryColor = { r: 255, g: 0, b: 255, a: 0.5 };

        service['primaryColor'] = expectedPrimaryColor;
        service['secondaryColor'] = expectedSecondaryColor;

        expectedLastRelativeCoord = { x: 20, y: 20 };
        expectedLastAbsoluteCoord = { x: 100, y: 100 };

        service['lastRelativeCoord'] = expectedLastRelativeCoord;
        service['lastAbsoluteCoord'] = expectedLastAbsoluteCoord;

        setConstraintSpy = spyOn<any>(service, 'setConstraint');
        setConstraintSpy.and.callThrough();

        resetSpy = spyOn<any>(service, 'reset');
        resetSpy.and.callThrough();

        clearDataSpy = spyOn<any>(service, 'clearData');
        previewDataChangedSpy = spyOn<any>(service, 'previewDataChanged');
        dataChangedSpy = spyOn<any>(service, 'dataChanged');
        updatePreviewSpy = spyOn<any>(service, 'updatePreview');
        updatePreviewDataSpy = spyOn<any>(service, 'updatePreviewData');
        updatePreviewDataSpy.and.callFake(() => {
            return;
        });
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('onInit should call registerKeyBind on keyBindingService', () => {
        const registerKeybindSpy = spyOn(service['keyBindingService'], 'registerKeybind');
        service.onInit();
        expect(registerKeybindSpy).toHaveBeenCalled();
    });

    it('onDestroy should call unregisterKeyBind on keyBindingService', () => {
        const unregisterKeybindSpy = spyOn(service['keyBindingService'], 'unregisterKeybind');
        service.onDestroy();
        expect(unregisterKeybindSpy).toHaveBeenCalled();
    });

    it('onDestroy should call reset', () => {
        service.onDestroy();
        expect(resetSpy).toHaveBeenCalled();
    });

    it('shiftPressed should call setConstraint with true', () => {
        service['shiftPressed']();
        expect(setConstraintSpy).toHaveBeenCalledWith(true);
    });

    it('shiftReleased should setConstraint with false', () => {
        service['shiftReleased']();
        expect(setConstraintSpy).toHaveBeenCalledWith(false);
    });

    it('isConstrained should be initially false', () => {
        expect(service['isConstrained']).toEqual(false);
    });

    it('setConstraint should set isConstrained with passed value', () => {
        service['setConstraint'](true);
        expect(service['isConstrained']).toEqual(true);
    });

    it('setConstraint should call updatePreview with correct parameters if mouseDown is true', () => {
        service['mouseDown'] = true;
        service['setConstraint'](true);
        expect(updatePreviewSpy).toHaveBeenCalledWith(expectedLastRelativeCoord, expectedLastAbsoluteCoord);
    });

    it('setConstraint should call updatePreview with correct parameters if mouseDown is true', () => {
        service['mouseDown'] = false;
        service['setConstraint'](true);
        expect(updatePreviewSpy).not.toHaveBeenCalled();
    });

    it('strokeColor should return secondaryColor', () => {
        expect(service.strokeColor).toEqual(expectedSecondaryColor);
    });

    it('toolAttributes should return currentAttributes', () => {
        const attributes: ShapeProperties = { lineWidth: 5, drawingType: DrawingType.Filled };
        service['currentAttributes'] = attributes;
        expect(service.toolAttributes).toEqual(service['currentAttributes']);
    });

    it('setCanvasProperties should set context attributes to correct attributes', () => {
        const canvas = document.createElement('canvas');
        const ctx = canvas.getContext('2d') as CanvasRenderingContext2D;
        const attributes: ShapeProperties = { lineWidth: 5, drawingType: DrawingType.Filled };
        service['currentAttributes'] = attributes;
        service.setCanvasProperties(ctx);
        expect(ctx.lineWidth).toEqual(service['currentAttributes'].lineWidth);
        expect(ctx.lineJoin).toEqual(service['DEFAULT_LINE_JOIN']);
        expect(ctx.miterLimit).toEqual(service['DEFAULT_MITER_LIMIT']);
    });

    it('setCanvasProperties should do nothing if attributes are null ', () => {
        const canvas = document.createElement('canvas');
        const ctx = canvas.getContext('2d') as CanvasRenderingContext2D;
        service.setCanvasProperties(ctx);
        expect(ctx.lineWidth).toEqual(1);
    });

    it('onMouseDown should set mouseDownCoord to the correct position', () => {
        const expectedResult: Vec2 = { x: 20, y: 25 };
        service.onMouseDown(mouseEventLClick);
        expect(service['mouseDownCoord']).toEqual(expectedResult);
    });

    it('onMouseDown should set mouseDown property to true on left click', () => {
        service.onMouseDown(mouseEventLClick);
        expect(service['mouseDown']).toEqual(true);
    });

    it('onMouseDown should set mouseDown property to false on right click', () => {
        service.onMouseDown(mouseEventRClick);
        expect(service['mouseDown']).toEqual(false);
    });

    it('onMouseDown should call startPrevisualization', () => {
        const startPrevisualizationSpy = spyOn<any>(service, 'startPrevisualization');
        service.onMouseDown(mouseEventLClick);
        expect(startPrevisualizationSpy).toHaveBeenCalledWith(mouseEventLClick);
    });

    it('onMouseMove should do nothing if mouse is up', () => {
        service['mouseDown'] = false;
        service.onMouseMove(mouseWrapperStub);
        expect(updatePreviewSpy).not.toHaveBeenCalled();
        expect(resetSpy).not.toHaveBeenCalled();
    });

    it('onMouseMove should call updatePreview if mouse was already down', () => {
        service['mouseDown'] = true;
        service.onMouseMove(mouseEventLClick);
        expect(updatePreviewSpy).toHaveBeenCalled();
    });

    it('onMouseMove should call reset if mouse was already down but no buttons are pressed', () => {
        service['mouseDown'] = true;
        service.onMouseMove({ buttons: 0 } as MouseEventWrapper);
        expect(resetSpy).toHaveBeenCalled();
    });

    it('onMouseUp should call updatePreview with last mouse coords if left mouse was down', () => {
        service['mouseDown'] = true;
        service.onMouseUp(mouseEventLClick);
        expect(updatePreviewSpy).toHaveBeenCalledWith(mouseEventLClick.relativePosition, mouseEventLClick.absolutePosition);
    });

    it('onMouseUp should not call updatePreview if left mouse was not down', () => {
        service['mouseDown'] = false;
        service.onMouseUp(mouseEventLClick);
        expect(updatePreviewSpy).not.toHaveBeenCalled();
    });

    it('onMouseUp should set drawing element to preview drawing element', () => {
        service['mouseDown'] = false;
        service['previewDrawingElement'] = { lineWidth: 87, drawingType: DrawingType.StrokeFilled } as ShapeProperties;
        service.onMouseUp(mouseEventLClick);
        expect(service['drawingElement']).toEqual(service['previewDrawingElement']);
    });

    it('onMouseUp should call dataChanged and reset', () => {
        service.onMouseUp(mouseEventLClick);
        expect(dataChangedSpy).toHaveBeenCalled();
        expect(resetSpy).toHaveBeenCalled();
    });

    it('startPrevisualization should call startPrevisualization of preview rectangle', () => {
        service['previsualizationRect'] = previewRect;
        const startPrevisualizationSpy = spyOn<any>(service['previsualizationRect'], 'startPrevisualization');
        service['startPrevisualization'](mouseEventLClick);
        expect(startPrevisualizationSpy).toHaveBeenCalledWith(mouseEventLClick.absolutePosition);
    });

    it('reset should destroy preview rect if it exists', () => {
        service['previsualizationRect'] = previewRect;
        service['reset']();
        expect(appComponentDestroySpy).toHaveBeenCalledWith(previewRect);
    });

    it('reset should set mouseDown to false', () => {
        service['mouseDown'] = true;
        service['reset']();
        expect(service['mouseDown']).toEqual(false);
    });

    it('reset should call clearData, previewDataChanged and dataChanged', () => {
        service['reset']();
        expect(clearDataSpy).toHaveBeenCalled();
        expect(previewDataChangedSpy).toHaveBeenCalled();
        expect(dataChangedSpy).toHaveBeenCalled();
    });

    it('updatePreview should call clampMouseCoord if isCircle is true', () => {
        const clampMouseSpy = spyOn<any>(service, 'clampMouseCoord');
        const mouseCoord: Vec2 = { x: 10, y: 15 };
        const clampMouseCoord: Vec2 = { x: 4, y: 9 };

        service['previsualizationRect'] = previewRect;
        service['isConstrained'] = true;
        previewRect.startPrevisualization(mouseCoord);
        updatePreviewSpy.and.callThrough();
        clampMouseSpy.and.returnValue({} as Vec2);
        service['updatePreview'](mouseCoord, clampMouseCoord);
        expect(clampMouseSpy).toHaveBeenCalled();
    });

    it('updatePreview should call updatePosition, updatePreviewData and previewDataChanged', () => {
        const mouseCoord: Vec2 = { x: 10, y: 15 };
        const rectCoord: Vec2 = { x: 4, y: 9 };
        const updatePositionSpy = spyOn<any>(previewRect, 'updatePosition');
        service['previsualizationRect'] = previewRect;
        updatePreviewSpy.and.callThrough();
        service['updatePreview'](mouseCoord, rectCoord);
        expect(updatePositionSpy).toHaveBeenCalledWith(rectCoord);
        expect(updatePreviewDataSpy).toHaveBeenCalledWith(mouseCoord);
        expect(previewDataChangedSpy).toHaveBeenCalled();
    });

    it('clampMousecoord should modify y value if distanceX < distanceY', () => {
        const clampMouseSpy = spyOn<any>(service, 'clampMouseCoord');
        const mouseCoord: Vec2 = { x: 10, y: 14 };
        const clampMouseCoord: Vec2 = { x: 10, y: 10 };
        service.setDefaultProperties();
        service.onInit();
        service['previsualizationRect'] = previewRect;
        service['isConstrained'] = true;
        previewRect.startPrevisualization(mouseCoord);
        updatePreviewSpy.and.callThrough();
        clampMouseSpy.and.returnValue(clampMouseCoord);
        clampMouseSpy.and.callThrough();
        service['updatePreview'](mouseCoord, clampMouseCoord);
        expect(clampMouseSpy).toHaveBeenCalled();
    });

    it('clampMousecoord should modify x value if distanceX > distanceY', () => {
        const clampMouseSpy = spyOn<any>(service, 'clampMouseCoord');
        const mouseCoord: Vec2 = { x: 7, y: 5 };
        const clampMouseCoord: Vec2 = { x: 5, y: 5 };
        service.setDefaultProperties();
        service.onInit();
        service['previsualizationRect'] = previewRect;
        service['isConstrained'] = true;
        previewRect.startPrevisualization(mouseCoord);
        updatePreviewSpy.and.callThrough();
        clampMouseSpy.and.returnValue(clampMouseCoord);
        clampMouseSpy.and.callThrough();
        service['updatePreview'](mouseCoord, clampMouseCoord);
        expect(clampMouseSpy).toHaveBeenCalled();
    });
});
