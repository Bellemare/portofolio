import { Injectable } from '@angular/core';
import { DrawingPos } from '@app/classes/drawing/drawing-pos';
import { DrawingType } from '@app/classes/drawing/drawing-type';
import { ToolType } from '@app/classes/drawing/tool-type';
import { Vec2 } from '@app/classes/drawing/vec2';
import { MouseEventWrapper } from '@app/classes/mouse-event-wrapper';
import { PencilProperties } from '@app/classes/tool-properties/pencil-properties';
import { Tooltip } from '@app/classes/tooltip';
import { AttributesManagerService } from '@app/services/api/attributes-manager.service';
import { PencilDrawingService } from '@app/services/drawing-services/pencil-drawing.service';
import { KeyBindingResolverService } from '@app/services/key-binding/key-binding-resolver.service';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faPencilAlt } from '@fortawesome/free-solid-svg-icons';
import { ColorService } from './color.service';
import { ToolService } from './tool.service';

@Injectable({
    providedIn: 'root',
})
export class PencilService extends ToolService {
    readonly TOOL_ICON: IconDefinition = faPencilAlt;
    readonly TOOL_LABEL: string = 'Crayon';
    readonly KEYBIND: string = 'c';
    readonly TOOL_NAME: string = 'pencil';
    readonly TOOL_TYPE: ToolType = ToolType.PENCIL;
    readonly TOOLTIPS: Tooltip[] = [
        {
            name: this.TOOL_LABEL,
            shortcut: this.KEYBIND,
        },
    ];
    protected readonly DEFAULT_LINE_CAP: CanvasLineCap = 'round';
    protected readonly DEFAULT_LINE_JOIN: CanvasLineJoin = 'round';
    protected readonly DEFAULT_MITER_LIMIT: number = 3;

    protected readonly DEFAULT_CONFIGURATION: PencilProperties = {
        line_width: 7,
        drawing_type: DrawingType.Stroke,
    };
    protected currentAttributes: PencilProperties;

    protected previewDrawingElement: DrawingPos = new DrawingPos();
    protected drawingElement: DrawingPos = new DrawingPos();

    protected startMouseCoord: Vec2 | undefined;

    constructor(
        protected attributesManagerService: AttributesManagerService,
        protected colorService: ColorService,
        protected keyBindingService: KeyBindingResolverService,
        protected toolDrawingService: PencilDrawingService,
    ) {
        super(attributesManagerService, colorService, keyBindingService);
    }

    setCanvasProperties(ctx: CanvasRenderingContext2D, properties: PencilProperties | null = null): void {
        if (this.currentAttributes != null || properties != null) {
            const attr = properties != null ? properties : this.currentAttributes;
            ctx.lineWidth = attr.line_width;

            ctx.lineCap = this.DEFAULT_LINE_CAP;
            ctx.lineJoin = this.DEFAULT_LINE_JOIN;
            ctx.miterLimit = this.DEFAULT_MITER_LIMIT;
        }
    }

    private addFirstPoint(): void {
        if (this.startMouseCoord != undefined) {
            this.previewDrawingElement.data.push(this.startMouseCoord);
            this.startMouseCoord = undefined;
        }
    }

    onMouseDown(event: MouseEventWrapper): void {
        this.mouseDown = event.isLeftButton;
        if (this.mouseDown) {
            this.startMouseCoord = event.relativePosition;
        }
    }

    onMouseEnter(event: MouseEventWrapper): void {
        if (this.mouseDown && event.buttons === 0) {
            this.drawingElement = this.previewDrawingElement;
            this.dataChanged();
            this.clearData();
            this.mouseDown = false;
        }
    }

    onMouseUp(event: MouseEventWrapper): void {
        this.drawingElement = this.previewDrawingElement;
        if (this.mouseDown) {
            this.addFirstPoint();
            const mousePosition = event.relativePosition;
            this.drawingElement.data.push(mousePosition);
        }
        this.mouseDown = false;
        this.dataChanged();
        this.clearData();
    }

    onMouseMove(event: MouseEventWrapper): void {
        if (this.mouseDown) {
            this.addFirstPoint();
            const mousePosition = event.relativePosition;
            this.previewDrawingElement.data.push(mousePosition);
            this.previewDataChanged();
        }
    }

    protected clearData(): void {
        this.previewDrawingElement = new DrawingPos();
        this.drawingElement = new DrawingPos();
        this.previewDrawingElement.data = [];
        this.drawingElement.data = [];
        this.previewDataChanged();
        this.dataChanged();
    }
}
