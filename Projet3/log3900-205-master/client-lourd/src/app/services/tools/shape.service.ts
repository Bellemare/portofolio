import { Injectable } from '@angular/core';
import { PrevisualizationRectangleComponent } from '@app/app-components/previsualization-rectangle/previsualization-rectangle.component';
import { DrawingShape } from '@app/classes/drawing/drawing-shape';
import { DrawingText } from '@app/classes/drawing/drawing-text';
import { DrawingType } from '@app/classes/drawing/drawing-type';
import { Vec2 } from '@app/classes/drawing/vec2';
import { MouseEventWrapper } from '@app/classes/mouse-event-wrapper';
import { ShapeProperties } from '@app/classes/tool-properties/shape-properties';
import { ComponentContainerComponent } from '@app/components/component-container/component-container.component';
import { AttributesManagerService } from '@app/services/api/attributes-manager.service';
import { KeyBindingResolverService } from '@app/services/key-binding/key-binding-resolver.service';
import { ColorService } from './color.service';
import { ToolService } from './tool.service';

@Injectable({
    providedIn: 'root',
})
export abstract class ShapeService extends ToolService {
    protected readonly DEFAULT_LINE_JOIN: CanvasLineJoin = 'miter';
    protected readonly DEFAULT_MITER_LIMIT: number = 10;
    protected readonly DEFAULT_CONFIGURATION: ShapeProperties = {
        drawing_type: DrawingType.StrokeFilled,
        line_width: 7,
    };

    protected currentAttributes: ShapeProperties;
    private isConstrained: boolean = false;
    private lastAbsoluteCoord: Vec2 = new Vec2();
    private lastRelativeCoord: Vec2 = new Vec2();

    protected previewDrawingElement: DrawingShape;
    protected drawingElement: DrawingShape;

    protected previsualizationRect: PrevisualizationRectangleComponent;

    constructor(
        protected attributesManagerService: AttributesManagerService,
        protected colorService: ColorService,
        protected keyBindingService: KeyBindingResolverService,
    ) {
        super(attributesManagerService, colorService, keyBindingService);
    }

    onInit(): void {
        super.onInit();
        const keybind = 'Shift';
        this.keyBindingService.registerKeybind(
            {
                keybind,
                callback: this.shiftPressed.bind(this),
            },
            {
                keybind,
                callback: this.shiftReleased.bind(this),
            },
        );
    }

    onDestroy(): void {
        super.onDestroy();
        this.keyBindingService.unregisterKeybind('Shift');
        this.reset();
    }

    protected shiftPressed(): void {
        this.setConstraint(true);
    }

    protected shiftReleased(): void {
        this.setConstraint(false);
    }

    protected setConstraint(isConstrained: boolean): void {
        this.isConstrained = isConstrained;
        if (this.mouseDown) {
            this.updatePreview(this.lastRelativeCoord, this.lastAbsoluteCoord);
        }
    }

    get toolAttributes(): ShapeProperties {
        return this.currentAttributes;
    }

    setCanvasProperties(ctx: CanvasRenderingContext2D, properties: ShapeProperties | null = null): void {
        if (this.currentAttributes != null || properties != null) {
            const attr = properties != null ? properties : this.currentAttributes;
            ctx.lineWidth = attr.line_width;
            ctx.lineJoin = this.DEFAULT_LINE_JOIN;
            ctx.miterLimit = this.DEFAULT_MITER_LIMIT;
        }
    }

    onMouseDown(event: MouseEventWrapper): void {
        this.mouseDown = event.isLeftButton;
        if (this.mouseDown) {
            this.mouseDownCoord = event.relativePosition;

            this.previsualizationRect = ComponentContainerComponent.createComponent<PrevisualizationRectangleComponent>(PrevisualizationRectangleComponent);
            this.startPrevisualization(event);

            this.updatePreview(event.relativePosition, event.absolutePosition);
        }
    }

    onMouseMove(event: MouseEventWrapper): void {
        if (this.mouseDown) {
            if (event.buttons === 1) {
                this.updatePreview(event.relativePosition, event.absolutePosition);
            } else {
                this.reset();
            }
        }
    }

    onMouseUp(event: MouseEventWrapper): void {
        if (this.mouseDown) {
            this.updatePreview(event.relativePosition, event.absolutePosition);
        }
        this.drawingElement = this.previewDrawingElement;
        this.drawingElement.text = new DrawingText();
        this.dataChanged();
        this.reset();
    }

    protected startPrevisualization(event: MouseEventWrapper): void {
        this.previsualizationRect.startPrevisualization(event.absolutePosition);
    }

    reset(): void {
        if (this.previsualizationRect !== undefined) {
            ComponentContainerComponent.destroyComponent(this.previsualizationRect);
        }
        this.mouseDown = false;
        this.clearData();
        this.previewDataChanged();
        this.dataChanged();
    }

    private updatePreview(relativePosition: Vec2, absolutePosition: Vec2): void {
        let mouseCoord = relativePosition;
        if (this.isConstrained) {
            mouseCoord = this.clampMouseCoord(mouseCoord);
        }
        const mouseCoordDiff = { x: relativePosition.x - mouseCoord.x, y: relativePosition.y - mouseCoord.y };
        const rectMouseCoord = { x: absolutePosition.x - mouseCoordDiff.x, y: absolutePosition.y - mouseCoordDiff.y };
        this.previsualizationRect.updatePosition(rectMouseCoord);
        this.updatePreviewData(mouseCoord);
        this.previewDataChanged();
        this.lastAbsoluteCoord = absolutePosition;
        this.lastRelativeCoord = relativePosition;
    }

    protected abstract updatePreviewData(mouseCoord: Vec2): void;

    private clampMouseCoord(mouseCoord: Vec2): Vec2 {
        const clampedMouseCoord: Vec2 = { x: mouseCoord.x, y: mouseCoord.y };
        const distX: number = mouseCoord.x - this.mouseDownCoord.x;
        const distY: number = mouseCoord.y - this.mouseDownCoord.y;
        if (Math.abs(distX) < Math.abs(distY)) {
            clampedMouseCoord.y = this.mouseDownCoord.y + Math.sign(distY) * Math.abs(distX);
        } else {
            clampedMouseCoord.x = this.mouseDownCoord.x + Math.sign(distX) * Math.abs(distY);
        }
        return clampedMouseCoord;
    }
}
