import { Injectable } from '@angular/core';
import { ShapeTextComponent } from '@app/app-components/shape-text/shape-text.component';
import { DrawingEvent } from '@app/classes/drawing/drawing-event';
import { DrawingShape } from '@app/classes/drawing/drawing-shape';
import { DrawingText } from '@app/classes/drawing/drawing-text';
import { Vec2 } from '@app/classes/drawing/vec2';
import { TextProperties } from '@app/classes/tool-properties/text-properties';
import { ComponentContainerComponent } from '@app/components/component-container/component-container.component';
import { AttributesManagerService } from '@app/services/api/attributes-manager.service';
import { KeyBindingResolverService } from '@app/services/key-binding/key-binding-resolver.service';
import { ColorService } from './color.service';
import { ToolService } from './tool.service';

@Injectable({
    providedIn: 'root',
})
export class TextService extends ToolService {
    protected shapeElement: DrawingShape;
    protected previewDrawingElement: DrawingText = new DrawingText();

    private editTextElement: ShapeTextComponent;

    constructor(
        protected attributesManagerService: AttributesManagerService,
        protected colorService: ColorService,
        protected keyBindingService: KeyBindingResolverService,
    ) {
        super(attributesManagerService, colorService, keyBindingService);
    }

    static getFontStringFromTextProperties(properties: TextProperties): string {
        const boldness: string = properties.isBold ? 'bold ' : '';
        const italicness: string = properties.isItalic ? 'italic ' : '';
        return `${boldness}${italicness}${properties.fontSize}px ${properties.font}`;
    }

    init(event: DrawingEvent, pos: Vec2, size: Vec2, onChange: () => void): void {
        const element = event.data as DrawingShape;
        this.shapeElement = element;
        this.previewDrawingElement = element.text as DrawingText;
        this.editTextElement = ComponentContainerComponent.createComponent<ShapeTextComponent>(ShapeTextComponent);
        this.editTextElement.shapeElement = element;
        this.editTextElement.event = event;
        this.updateEditElement(pos, size);
        this.editTextElement.init();
        this.editTextElement.onChange = onChange;
        this.editTextElement.onExit = this.reset.bind(this);
    }

    private updateEditElement(pos: Vec2, size: Vec2): void {
        this.editTextElement._top = pos.y;
        this.editTextElement._left = pos.x;
        this.editTextElement._width = Math.abs(size.x);
        this.editTextElement._height = Math.abs(size.y);
        this.editTextElement.setText();
    }

    reset(): void {
        this.clearData();
        this.cancelTextEditing();
        ComponentContainerComponent.destroyComponent(this.editTextElement);
    }

    cancelTextEditing(): void {
        this.previewDrawingElement = new DrawingText();
    }

    pause(): void {
        if (this.editTextElement != undefined) {
            this.editTextElement.hidden = true;
        }
    }

    resume(pos: Vec2, size: Vec2): void {
        if (this.editTextElement != undefined) {
            pos.x = Math.min(pos.x, pos.x + size.x);
            pos.y = Math.min(pos.y, pos.y + size.y);
            this.editTextElement.hidden = false;
            this.updateEditElement(pos, size);
        }
    }

    protected clearData(): void {
        this.previewDrawingElement = new DrawingText();
    }
}
