import { TestBed } from '@angular/core/testing';
import { DrawingCanvasHelper } from '@app/classes/drawing-canvas-helper';
import { DrawingType } from '@app/classes/drawing/drawing-type';
import { MouseButton } from '@app/classes/mouse-button';
import { MouseEventWrapper } from '@app/classes/mouse-event-wrapper';
import { TextFonts } from '@app/classes/text-fonts.enum';
import { TextProperties } from '@app/classes/tool-properties/text-properties';
import { KeyBindingResolverService } from '@app/services/key-binding/key-binding-resolver.service';
import { ResizeService } from '@app/services/resize/resize.service';
import { Subscription } from 'rxjs';
import { TextService } from './text.service';

// tslint:disable:no-any
// tslint:disable:no-string-literal
// tslint:disable:no-magic-numbers
describe('TextService', () => {
    let service: TextService;
    let mouseEventLClick: MouseEventWrapper;
    let canvas: HTMLCanvasElement;
    let ctx: CanvasRenderingContext2D;
    let attributes: TextProperties;
    let keyboardEventLetter: KeyboardEvent;
    let keyboardEventKeybind: KeyboardEvent;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(TextService);

        mouseEventLClick = new MouseEventWrapper({
            offsetX: 20,
            offsetY: 25,
            button: MouseButton.Left,
            buttons: 1,
        } as MouseEvent);

        keyboardEventLetter = new KeyboardEvent('keydown', { key: 'a' });
        keyboardEventKeybind = new KeyboardEvent('keydown', { key: 'Delete' });

        canvas = document.createElement('canvas');
        ctx = canvas.getContext('2d') as CanvasRenderingContext2D;

        attributes = {
            drawingType: DrawingType.Filled,
            font: TextFonts.TimesNewRoman,
            fontSize: 56,
            isBold: false,
            isItalic: false,
            alignment: 'center',
            baseline: 'middle',
        };
    });

    it('toolAttributes should return currentAttributes', () => {
        service['currentAttributes'] = attributes;
        expect(service.toolAttributes).toEqual(attributes);
    });

    it('isEditing should return isEditingText', () => {
        service['isEditingText'] = true;
        expect(service.isEditing).toEqual(true);
    });

    it('setCanvasProperties should set canvas properties', () => {
        service['currentAttributes'] = attributes;
        service.setCanvasProperties(ctx);
        expect(ctx.textAlign).toEqual('center');
        expect(ctx.textBaseline).toEqual('middle');
        expect(ctx.font).toEqual('56px "Times New Roman"');
    });

    it('onMouseDown should set mouseDown to true', () => {
        service.onMouseDown(mouseEventLClick);
        expect(service['mouseDown']).toEqual(true);
    });

    it('onMouseUp should start text editing if in canvas and not editing', () => {
        service['mouseDown'] = true;
        service['isEditingText'] = false;
        const isInCanvasSpy = spyOn(ResizeService, 'isInCanvas');
        isInCanvasSpy.and.returnValue(true);
        const startTextEditingSpy = spyOn<any>(service, 'startTextEditing');
        startTextEditingSpy.and.callThrough();
        service.onMouseUp(mouseEventLClick);
        expect(startTextEditingSpy).toHaveBeenCalledWith(mouseEventLClick.relativePosition);
        expect(service['isEditingText']).toEqual(true);
    });

    it('onMouseUp should confirm text editing if editing and not in textarea', () => {
        service['mouseDown'] = true;
        service['isEditingText'] = true;
        const getLineBoundingBoxesSpy = spyOn(service['TOOL_DRAWING_SERVICE'], 'getLineBoundingBoxes');
        getLineBoundingBoxesSpy.and.returnValue([{ topLeft: { x: 0, y: 0 }, bottomRight: { x: 10, y: 10 } }]);
        const previewCanvasHelperSpy = spyOnProperty(DrawingCanvasHelper, 'previewCanvas', 'get');
        previewCanvasHelperSpy.and.returnValue(canvas);
        service['keyBindingService'] = new KeyBindingResolverService();
        const confirmTextEditingSpy = spyOn<any>(service, 'confirmTextEditing');
        confirmTextEditingSpy.and.callThrough();
        service.onMouseUp(mouseEventLClick);
        expect(confirmTextEditingSpy).toHaveBeenCalled();
        expect(service['isEditingText']).toEqual(false);
    });

    it('onMouseUp should confirm text editing if editing and not in textarea', () => {
        const clearDataSpy = spyOn<any>(service, 'clearData');
        clearDataSpy.and.callThrough();
        const cancelTextEditingSpy = spyOn<any>(service, 'cancelTextEditing');
        service.reset();
        expect(clearDataSpy).toHaveBeenCalled();
        expect(cancelTextEditingSpy).toHaveBeenCalled();
    });

    it('onInit should create appropriate subscriptions', () => {
        service.onInit();
        expect(service['attributeChangedSubscription']).not.toEqual(Subscription.EMPTY);
        expect(service['colorChangedSubscription']).not.toEqual(Subscription.EMPTY);
        expect(service['resizeSubscription']).not.toEqual(Subscription.EMPTY);
    });

    it('onDestroy should unsubscribe subscriptions', () => {
        service.onDestroy();
        expect(service['attributeChangedSubscription'].closed).toEqual(true);
        expect(service['colorChangedSubscription'].closed).toEqual(true);
        expect(service['resizeSubscription'].closed).toEqual(true);
    });

    it('onDestroy should confirm text editing if editing', () => {
        service['isEditingText'] = true;
        const confirmSpy = spyOn<any>(service, 'confirmTextEditing');
        service.onDestroy();
        expect(confirmSpy).toHaveBeenCalled();
    });

    it('handleKeypress should insert text if event.key is letter', () => {
        service['isEditingText'] = true;
        const insertTextSpy = spyOn<any>(service, 'insertText');
        service.handleKeypress(keyboardEventLetter);
        expect(insertTextSpy).toHaveBeenCalledWith(keyboardEventLetter.key);
    });

    it('handleKeypress should resolve keybind if event.key is keybind', () => {
        service['isEditingText'] = true;
        const resolveKeybindSpy = spyOn<any>(service, 'resolveKeybind');
        resolveKeybindSpy.and.callThrough();
        service.handleKeypress(keyboardEventKeybind);
        expect(resolveKeybindSpy).toHaveBeenCalledWith(keyboardEventKeybind.key);
    });

    it('insertText should call insertText of preview element and call previewDataChanged', () => {
        const insertTextSpy = spyOn(service['previewDrawingElement'], 'insertText');
        const previewDataChangedSpy = spyOn<any>(service, 'previewDataChanged');
        service['insertText'](keyboardEventLetter.key);
        expect(insertTextSpy).toHaveBeenCalledWith(keyboardEventLetter.key);
        expect(previewDataChangedSpy).toHaveBeenCalled();
    });

    it('breakLine should call breakLine of preview element and call previewDataChanged', () => {
        const breakLineSpy = spyOn(service['previewDrawingElement'], 'breakLine');
        const previewDataChangedSpy = spyOn<any>(service, 'previewDataChanged');
        service['breakLine']();
        expect(breakLineSpy).toHaveBeenCalled();
        expect(previewDataChangedSpy).toHaveBeenCalled();
    });

    it('backspace should call backspace of preview element and call previewDataChanged', () => {
        const backspaceSpy = spyOn(service['previewDrawingElement'], 'backspace');
        const previewDataChangedSpy = spyOn<any>(service, 'previewDataChanged');
        service['backspace']();
        expect(backspaceSpy).toHaveBeenCalled();
        expect(previewDataChangedSpy).toHaveBeenCalled();
    });

    it('delete should call delete of preview element and call previewDataChanged', () => {
        const deleteSpy = spyOn(service['previewDrawingElement'], 'delete');
        const previewDataChangedSpy = spyOn<any>(service, 'previewDataChanged');
        service['delete']();
        expect(deleteSpy).toHaveBeenCalled();
        expect(previewDataChangedSpy).toHaveBeenCalled();
    });

    it('moveCursorUp should call moveCursorUp of preview element and call previewDataChanged', () => {
        const previewCanvasHelperSpy = spyOnProperty(DrawingCanvasHelper, 'previewCanvas', 'get');
        previewCanvasHelperSpy.and.returnValue(canvas);
        const moveCursorUpSpy = spyOn(service['previewDrawingElement'], 'moveCursorUp');
        const previewDataChangedSpy = spyOn<any>(service, 'previewDataChanged');
        service['moveCursorUp']();
        expect(moveCursorUpSpy).toHaveBeenCalled();
        expect(previewDataChangedSpy).toHaveBeenCalled();
    });

    it('moveCursorLeft should call moveCursorLeft of preview element and call previewDataChanged', () => {
        const moveCursorLeftSpy = spyOn(service['previewDrawingElement'], 'moveCursorLeft');
        const previewDataChangedSpy = spyOn<any>(service, 'previewDataChanged');
        service['moveCursorLeft']();
        expect(moveCursorLeftSpy).toHaveBeenCalled();
        expect(previewDataChangedSpy).toHaveBeenCalled();
    });

    it('moveCursorRight should call moveCursorRight of preview element and call previewDataChanged', () => {
        const moveCursorRightSpy = spyOn(service['previewDrawingElement'], 'moveCursorRight');
        const previewDataChangedSpy = spyOn<any>(service, 'previewDataChanged');
        service['moveCursorRight']();
        expect(moveCursorRightSpy).toHaveBeenCalled();
        expect(previewDataChangedSpy).toHaveBeenCalled();
    });

    it('moveCursorDown should call moveCursorDown of preview element and call previewDataChanged', () => {
        const previewCanvasHelperSpy = spyOnProperty(DrawingCanvasHelper, 'previewCanvas', 'get');
        previewCanvasHelperSpy.and.returnValue(canvas);
        const moveCursorDownSpy = spyOn(service['previewDrawingElement'], 'moveCursorDown');
        const previewDataChangedSpy = spyOn<any>(service, 'previewDataChanged');
        service['moveCursorDown']();
        expect(moveCursorDownSpy).toHaveBeenCalled();
        expect(previewDataChangedSpy).toHaveBeenCalled();
    });
});
