import { Injectable } from '@angular/core';
import { Color } from '@app/classes/drawing/color';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class ColorService {
    static readonly MAX_VALUE: number = 255;
    static readonly MIN_VALUE: number = 0;
    static readonly MAX_ALPHA: number = 1;
    static readonly MIN_ALPHA: number = 0;

    static readonly PIXEL_RED_INDEX: number = 0;
    static readonly PIXEL_GREEN_INDEX: number = 1;
    static readonly PIXEL_BLUE_INDEX: number = 2;
    static readonly PIXEL_ALPHA_INDEX: number = 3;

    private primaryColor: BehaviorSubject<Color>;
    private secondaryColor: BehaviorSubject<Color>;
    private textColor: BehaviorSubject<Color>;
    private readonly DEFAULT_PRIMARY_COLOR: Color = {
        r: 0,
        g: 0,
        b: 0,
        a: 1,
    };
    private readonly DEFAULT_SECONDARY_COLOR: Color = {
        r: 255,
        g: 255,
        b: 255,
        a: 1,
    };

    static getColorRGBA(color: Color): string {
        return `rgba(${color.r},${color.g},${color.b},${color.a})`;
    }

    static getColorRGB(color: Color): string {
        return `rgb(${color.r},${color.g},${color.b})`;
    }

    constructor() {
        this.loadDefaultColor();
    }

    private loadDefaultColor(): void {
        this.primaryColor = new BehaviorSubject<Color>(this.DEFAULT_PRIMARY_COLOR);
        this.secondaryColor = new BehaviorSubject<Color>(this.DEFAULT_SECONDARY_COLOR);
        this.textColor = new BehaviorSubject<Color>(this.DEFAULT_PRIMARY_COLOR);
    }

    getPrimaryColor(): Observable<Color> {
        return this.primaryColor.asObservable();
    }

    getSecondaryColor(): Observable<Color> {
        return this.secondaryColor.asObservable();
    }

    getTextColor(): Observable<Color> {
        return this.textColor.asObservable();
    }

    swapColors(): void {
        const secondary = this.secondaryColor.value;
        this.secondaryColor.next(this.primaryColor.value);
        this.primaryColor.next(secondary);
    }

    setPrimaryColor(color: Color): void {
        this.primaryColor.next(color);
    }

    setSecondaryColor(color: Color): void {
        this.secondaryColor.next(color);
    }

    setTextColor(color: Color): void {
        this.textColor.next(color);
    }
}
