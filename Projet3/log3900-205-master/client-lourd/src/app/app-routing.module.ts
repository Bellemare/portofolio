import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditorComponent } from './components/editor/editor.component';
import { LoginComponent } from './components/login/login.component';
import { AuthGuardService } from '@app/services/api/auth-guard.service'; 
import { RegisterComponent } from './components/register/register.component';
import { AlbumComponent } from './components/albums/album/album.component';
import { AlbumListComponent } from './components/albums/album-list/album-list.component';
import { AlbumsComponent } from './components/albums/albums/albums.component';
import { UserComponent } from './components/user/user.component';
import { ContactComponent } from './components/contact/contact.component';
import { PopoutChatComponent } from './components/popout-chat/popout-chat.component';
import { ExpositionComponent } from './components/exposition/exposition.component';

const routes: Routes = [
    { path: '', redirectTo: '/album', pathMatch: 'full', canActivate: [AuthGuardService] },
    { path: 'chat', component: PopoutChatComponent, canActivate: [AuthGuardService] },
    { path: 'user', component: UserComponent, canActivate: [AuthGuardService] },
    { path: 'contact/:id', component: ContactComponent, canActivate: [AuthGuardService] },
    { path: 'album', component: AlbumsComponent, canActivate: [AuthGuardService] },
    { path: 'album/:albumID', component: AlbumComponent, canActivate: [AuthGuardService] },
    { path: 'exposition/:albumID', component: ExpositionComponent, canActivate: [AuthGuardService] },
    { path: 'albumList', component: AlbumListComponent, canActivate: [AuthGuardService] },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'editor', component: EditorComponent, canActivate: [AuthGuardService] },
    { path: 'editor/:drawingID', component: EditorComponent, canActivate: [AuthGuardService] },
    { path: '**', redirectTo: '/album' },
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: true })],
    exports: [RouterModule],
})
export class AppRoutingModule {}
