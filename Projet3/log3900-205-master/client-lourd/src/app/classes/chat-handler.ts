import { EventEmitter } from "@angular/core";
import { AuthService } from "@app/services/api/auth.service";
import { RoomService } from "@app/services/api/room.service";
import { UserApiService } from "@app/services/api/user-api.service";
import { AudioService } from "@app/services/audio/audio.service";
import * as moment from "moment";
import { User } from "./api/user";
import { UserAvatar } from "./api/user-avatar";
import { ClientStateEventPayload } from "./socket/client-state-event-payload";
import { ClientStateSyncEventPayload } from "./socket/client-state-sync-event-payload";
import { ClientStateType } from "./socket/client-state-types";
import { ClientSync } from "./socket/client-sync";
import { MessageHistoryPayload } from "./socket/message-history-payload";
import { MessagePayload } from "./socket/message-payload";
import { PayloadTypes } from "./socket/payload-types";
import { SocketClient } from "./socket/socket-client";
import { SoundType } from "./sound-types";

export class ChatHandler {
    protected conn: SocketClient;
    protected currentPage: number;
    protected drawingRoomClient: SocketClient | undefined;
    protected clientStateListener: number;
    protected clientStateSyncListener: number;
    
    messageListener: EventEmitter<void> = new EventEmitter();

    roomMembersCache: User[] = [];
    avatarCache: Map<number, UserAvatar> = new Map();
    messages: MessagePayload[] = [];
    roomId: number;

    protected roomLoadPromise: Promise<void>;

    constructor(
        private authService: AuthService,
        private audioService: AudioService,
        private roomService: RoomService, 
        protected userService: UserApiService) { }

    hasInitialized(): boolean {
        return this.conn != undefined || this.roomLoadPromise != undefined;
    }

    hasLoaded(): boolean {
        return this.currentPage > 0;
    }

    init(conn: SocketClient): void {
        this.conn = conn;
        this.initListeners();
    }

    destroy(): void {
        this.drawingRoomClient?.unregister(PayloadTypes.CLIENT_STATE_EVENT, this.clientStateListener);
        this.drawingRoomClient?.unregister(PayloadTypes.CLIENT_STATE_SYNC_EVENT, this.clientStateSyncListener);
        this.drawingRoomClient = undefined;
    }

    onRoomLoad(roomId: number): Promise<void> {
        this.roomLoadPromise = new Promise((resolve, reject) => {
            this.roomId = roomId;
            this.roomService.connectRoom(roomId).then((conn: SocketClient) => {
                this.conn = conn;
                this.currentPage = 0;
                this.messages = [];

                this.initListeners();
                resolve();
            }).catch(() => {
                reject();
            })
        });

        return this.roomLoadPromise;
    }

    private initListeners(): void {
        this.conn.onMessage(PayloadTypes.MESSAGE, this.handleNewMessage.bind(this));
        this.conn.onMessage(PayloadTypes.MESSAGE_HISTORY, this.handleHistory.bind(this));
    }

    listenClientState(conn: SocketClient): void {
        this.drawingRoomClient = conn;
        this.listenToCollaboratorsStateChange();
    }

    private getUserCache(userId: number): number {
        return this.roomMembersCache.findIndex((u: User) => u.id == userId);
    }

    private listenToCollaboratorsStateChange(): void {
        this.clientStateListener = this.drawingRoomClient?.onMessage(PayloadTypes.CLIENT_STATE_EVENT, (stateChange: ClientStateEventPayload) => {
            switch (stateChange.state) {
                case ClientStateType.CONNECT:
                    if (this.getUserCache(stateChange.user_id) != -1)
                        return;
                        
                    this.roomMembersCache.push({
                        pseudonym: stateChange.user,
                        id: stateChange.user_id,
                        avatar: stateChange.avatar,
                        is_contact: false,
                        email: "",
                    });
                    break;
                case ClientStateType.DISCONNECT:
                    const index = this.getUserCache(stateChange.user_id);
                    if (index != -1) {
                        this.roomMembersCache.splice(index, 1);
                    }
                    break;
            }
        }) as number;
        this.clientStateSyncListener = this.drawingRoomClient?.onMessage(PayloadTypes.CLIENT_STATE_SYNC_EVENT, (state: ClientStateSyncEventPayload) => {
            state.clients.forEach((client: ClientSync) => {
                this.roomMembersCache.push({
                    pseudonym: client.user,
                    id: client.user_id,
                    avatar: client.avatar,
                    is_contact: false,
                    email: "",
                });
            });
        }) as number;
    }

    loadRoomMembers(): Promise<void> {
        return new Promise((resolve, reject) => { 
            this.roomService.getRoomMembers(this.roomId).subscribe((users: User[]) => {
                this.roomMembersCache = users;
                resolve();
            }, () => reject());
        });
    }

    loadHistory(): Promise<void> {
        return new Promise((resolve, reject) => {
            this.roomService.getHistory({
                limit: 50,
                offset: this.currentPage * 50,
                room_id: this.roomId
            }).subscribe((history: MessagePayload[]) => {
                this.onHistoryLoad(history);
                resolve();
            }, () => reject());
        });
    }

    loadMessage(id: number): Promise<MessagePayload> {
        return new Promise((resolve) => {
            this.loadHistoryUntil(id, (message: MessagePayload) => {
                resolve(message);
            });
        });
    }

    private loadHistoryUntil(id: number, onSuccess: (message: MessagePayload) => void): void {
        this.loadHistory().then(() => {
            const index = this.messages.findIndex((m: MessagePayload) => m.id == id);
            if (index != -1) { 
                onSuccess(this.messages[index]);
            } else {
                this.loadHistoryUntil(id, onSuccess);
            }
        });
    }

    protected onHistoryLoad(history: MessagePayload[]): void {
        const hasLoaded = this.hasLoaded();
        this.currentPage++;
        history.map((entry: MessagePayload) => entry.sent_at = moment(entry.sent_at));
        if (!hasLoaded) {
            this.messages = history;
        } else {
            this.messages.push(...history);
        }
    }

    protected loadUserAvatar(userId: number): void {
        this.userService.getUserAvatar(userId).subscribe((avatar: UserAvatar) => {
        if (this.avatarCache.has(avatar.user_id)) {
            const userAvatar = this.avatarCache.get(avatar.user_id) as UserAvatar;
            userAvatar.avatar = avatar.avatar;
        } else {
            this.avatarCache.set(avatar.user_id, avatar);
        }
        });
    }

    protected handleNewMessage(message: MessagePayload): void {
        message.sent_at = moment(message.sent_at);
        this.messages.push(message);
        this.messageListener.emit();
        if (message.user_id != this.authService.getUser().id) {
            this.audioService.playSound(SoundType.NewMessage);
        }
    }

    protected handleHistory(history: MessageHistoryPayload): void {
        if (history != null) {
            history.messages.map((message: MessagePayload) => message.sent_at = moment(message.sent_at));
            this.messages.push(...history.messages);
            this.messageListener.emit();
        }
    }

    sendMessage(message: MessagePayload): void {
        this.conn.send(message, PayloadTypes.MESSAGE);
    }

    getUserAvatar(userId: number): string {
        if (userId == undefined)
            return '';

        if (!this.avatarCache.has(userId)) {
            this.avatarCache.set(userId, {
                user_id: userId,
                avatar: "",
            });
            this.loadUserAvatar(userId);
        }

        return this.avatarCache.get(userId)?.avatar as string;
    }

    getCurrentPage(): number {
        return this.currentPage;
    }
}