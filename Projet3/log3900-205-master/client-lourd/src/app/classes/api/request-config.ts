export interface RequestConfig {
    endpoint?: string;
    host?: string;
    port?: number;
    url: string;

    handleSuccess?: boolean;
    handleError?: boolean;
}
