export class Album {
    id?: number;
    name: string;
    description: string;
    owner_id?: number;
}