import * as moment from 'moment';

export class ConnectionHistoryEntry {
    created_at: moment.Moment;
    invalidated_at?: moment.Moment;
}