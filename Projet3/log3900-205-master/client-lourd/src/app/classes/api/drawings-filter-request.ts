export class DrawingsFilterRequest {
    names_filter: string[];
    pseudonyms_filter: string[];
    start_date: string|null;
    end_date: string|null;
}