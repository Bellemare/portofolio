export class ErrorResponse {
    title: string;
    body: string;
    fields: string[];
}

export class FieldsErrorResponse {
    title: string;
    body: string[];
    fields: string[];
}