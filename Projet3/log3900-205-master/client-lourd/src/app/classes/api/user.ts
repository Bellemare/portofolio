export class User {
    id: number;
    email: string;
    pseudonym: string;
    is_contact: boolean;
    avatar?: string;
    is_public_email?: boolean;
}