export class AlbumJoinRequest {
    id: number;
	user_id: number;  
	user: string;
	album_id: number;  
    album: string;
}