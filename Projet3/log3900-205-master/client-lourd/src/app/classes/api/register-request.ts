export class RegisterRequest {
    email: string;
    password: string;
    password_repeat: string;
    pseudonym: string;
    avatar: string;
}