import * as moment from "moment";

export class Drawing {
    id: number;
    image: string;
    owner_id: number;
    owner: string;
    album_id: number;
    name: string;
    created_at: moment.Moment;
    active_collaborators: number;
    is_exposed: boolean;
    is_password_protected: boolean;

    was_transfered?: boolean;
}