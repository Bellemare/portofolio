import { ContactCollaborationSession } from './contact-collaborative-session';

export class Contact {
    id: number;
    user_id: number;
    contact_id: number;
    contact: string;
    contact_avatar?: string;
    is_connected: boolean;
    is_in_collaborative_session: boolean;
    collaborative_session: ContactCollaborationSession;
}