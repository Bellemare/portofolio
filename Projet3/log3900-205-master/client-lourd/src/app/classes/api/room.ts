export class Room {
    id: number;
    name: string;
    owner_id: number;

    has_notification?: boolean = false;
}