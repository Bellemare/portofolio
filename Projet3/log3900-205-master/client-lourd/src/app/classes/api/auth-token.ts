import * as moment from "moment";

export class AuthToken {
    user_id: string;
    token: string;
    expires_at: moment.Moment;
}