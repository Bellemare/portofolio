export class ContactCollaborationSession {
    drawing_id: number;
    drawing: string;
    is_password_protected: boolean;
    album_id: number;
    album: string;
}