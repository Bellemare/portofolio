export class DrawingEdit {
    id: number;
    album_id: number;
    name: string;
    password?: string;
}