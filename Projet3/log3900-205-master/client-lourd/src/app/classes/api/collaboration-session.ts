import * as moment from 'moment';

export class CollaborationSession {
    user_id: number;
    drawing_id: number;
    drawing: string;
    album_id: number;
    album: string;
    is_password_protected: boolean;
    edited_drawing: boolean;
    start_time: moment.Moment;
    end_time: moment.Moment;
}