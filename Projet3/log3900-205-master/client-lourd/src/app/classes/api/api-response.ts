import { Message } from '@app/message';

export type ApiResponse = void | Message | object | boolean;
