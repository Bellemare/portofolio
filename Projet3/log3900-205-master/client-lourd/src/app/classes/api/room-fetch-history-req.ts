export class RoomFetchHistoryRequest {
	room_id: number
	limit:  number;
	offset: number;
}