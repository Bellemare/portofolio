export class UserMetrics {
    user_id: number;
    owned_drawings: number;
    collaborated_drawings: number;
    member_albums: number;
    average_collaboration_time: number;
    total_collaboration_time: number;
}