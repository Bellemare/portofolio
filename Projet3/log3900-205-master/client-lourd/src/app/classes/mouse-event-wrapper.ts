import { DrawingCanvasHelper } from './drawing-canvas-helper';
import { Vec2 } from './drawing/vec2';
import { MouseButton } from './mouse-button';

export class MouseEventWrapper {
    private event: MouseEvent;
    private target: HTMLElement | null;
    private outsideWindow: boolean = false;

    constructor(event: MouseEvent, target: HTMLElement | null = null) {
        this.event = event;
        this.target = target;
    }

    static isLeftButtonPressed(event: MouseEvent): boolean {
        return event.button === MouseButton.Left && event.buttons > 0;
    }

    private getTargetOffset(): Vec2 {
        return this.target != null ? { x: this.target.offsetLeft, y: this.target.offsetTop } : { x: 0, y: 0 };
    }

    get relativePosition(): Vec2 {
        if (this.outsideWindow) {
            const absolute = this.absolutePosition;
            return { x: absolute.x - DrawingCanvasHelper.OFFSET_LEFT, y: absolute.y - DrawingCanvasHelper.OFFSET_TOP };
        }
        const targetOffset = this.getTargetOffset();
        return { x: this.event.offsetX - targetOffset.x, y: this.event.offsetY - targetOffset.y };
    }

    get absolutePosition(): Vec2 {
        return { x: this.event.x, y: this.event.y };
    }

    get button(): number {
        return this.event.button;
    }

    get buttons(): number {
        return this.event.buttons;
    }

    get isLeftButton(): boolean {
        return this.event.button === MouseButton.Left;
    }

    get anyButtonsPressed(): boolean {
        return this.event.buttons > 0;
    }

    get mouseEvent(): MouseEvent {
        return this.event;
    }

    set isOutsideWindow(outside: boolean) {
        this.outsideWindow = outside;
    }
}
