export class NotificationEntry {
    title: string;
    messages?: string[];
    message?: string;
    code?: number;
    timeMs?: number;
}
