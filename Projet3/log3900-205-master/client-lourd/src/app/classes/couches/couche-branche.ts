import { EventEmitter, Injectable } from '@angular/core';
import { DrawingEventWrapper } from '@app/classes/drawing/drawing-event-wrapper';
import { CanvasSizeService } from '@app/services/canvas-size/canvas-size.service';
import { Subscription } from 'rxjs';
import { BoundingBox } from '@app/classes/drawing/bounding-box';
import { Vec2 } from '@app/classes/drawing/vec2';

@Injectable({
    "providedIn": "root",
})
export class CoucheBranche {
    render = new EventEmitter<void>();
    private childs: CoucheBranche[] = [];
    private branchSubscriptions: Subscription[] = [];
    private leafSubscriptions: Subscription[] = [];
    private _drawingEventWrapper: DrawingEventWrapper;
    private _isLeaf: boolean;

    constructor(drawing_event_wrapper?: DrawingEventWrapper) {
        if (this._isLeaf = drawing_event_wrapper != undefined)
            this._drawingEventWrapper = drawing_event_wrapper;
        else 
            this._drawingEventWrapper = new DrawingEventWrapper();
    }

    addChild(child: CoucheBranche, render: boolean = true, pos: number = -1): void {
        if (this._isLeaf)
            return;
        
        if (pos == -1) {
            this.childs.push(child);
            this.branchSubscriptions.push(child.render.asObservable().subscribe(this.makeRender.bind(this)));
            this.leafSubscriptions.push(child._drawingEventWrapper.render.subscribe(this.makeRender.bind(this)));
        } else {
            this.childs.splice(pos, 0, child);
            this.branchSubscriptions.splice(pos, 0, child.render.asObservable().subscribe(this.makeRender.bind(this)));
            this.leafSubscriptions.splice(pos, 0, child._drawingEventWrapper.render.asObservable().subscribe(this.makeRender.bind(this)));
        }
        
        if (render)
            this.makeRender();
    }

    selectChild(position: Vec2): DrawingEventWrapper|undefined {
        if (this.isLeaf)
            return this._drawingEventWrapper;

        for (let i = this.childCount - 1; i >= 0; i--) {
            let child = this.getChild(i);
            if (child == undefined || child.boundingBox == undefined)
                continue;
                
            const localPosition: Vec2 = { x: position.x - child.boundingBox.x, y: position.y - child.boundingBox.y };
            if (localPosition.x >= child.boundingBox.w || localPosition.y >= child.boundingBox.h || localPosition.x < 0 || localPosition.y < 0) {
                continue; 
            }

            let opacity = (child.image.getContext('2d') as CanvasRenderingContext2D)
                .getImageData(0, 0, child.boundingBox.w, child.boundingBox.h)
                .data[(localPosition.y * child.boundingBox.w + localPosition.x) * 4 + 3];
            if (opacity != 0) {
                return child.selectChild(position);
            }
        }
        return undefined;
    }

    removeChild(pos: number): void {
        if (this._isLeaf)
            return;
        
        this.childs.splice(pos, 1);
        this.branchSubscriptions.splice(pos, 1)[0].unsubscribe();
        this.leafSubscriptions.splice(pos, 1)[0].unsubscribe();
        this.makeRender();
    }

    getChild(pos: number): CoucheBranche {
        return this.childs[pos];
    }

    makeRender(renderParents: boolean = true): void {
        if (this._isLeaf)
            return;
        
        const stackedImage = document.createElement("canvas");
        const maxBoundingBox = this.getMaxBoundingBox();
        this._drawingEventWrapper.boundingBox = maxBoundingBox;
        stackedImage.width = Math.ceil(maxBoundingBox.w);
        stackedImage.height = Math.ceil(maxBoundingBox.h);
        const ctx = stackedImage.getContext("2d") as CanvasRenderingContext2D;
        
        for (let i = 0; i < this.childs.length; i++) {
            const child = this.childs[i];
            if (child._drawingEventWrapper.isSelected || child._drawingEventWrapper.boundingBox == undefined) {
                continue;
            }

            const boundingBox = child._drawingEventWrapper.boundingBox;
            if (boundingBox.w == 0 || boundingBox.h == 0 || child.image.width == 0 || child.image.height == 0) {
                continue;
            }
            const x = Math.floor(boundingBox.x - maxBoundingBox.x);
            const y = Math.floor(boundingBox.y - maxBoundingBox.y);
            ctx.drawImage(child.image, x, y, Math.ceil(boundingBox.w), Math.ceil(boundingBox.h));
        }
        
        this._drawingEventWrapper.setBaseImage(stackedImage); 
        if (renderParents)
            this.render.emit();
    }

    getMaxBoundingBox(): BoundingBox {
        let xMin = CanvasSizeService.canvasSize.x;
        let yMin = CanvasSizeService.canvasSize.y;
        let xMax = 0;
        let yMax = 0;
        for (let i = 0; i < this.childs.length; i++) {
            const child = this.childs[i];
            if (child._drawingEventWrapper.isSelected)
                continue;
                
            const boundingBox = child._drawingEventWrapper.boundingBox;
            if (boundingBox == undefined) {
                continue;
            }
            xMin = Math.min(boundingBox.x, xMin);
            yMin = Math.min(boundingBox.y, yMin);
            xMax = Math.max(boundingBox.x + boundingBox.w, xMax);
            yMax = Math.max(boundingBox.y + boundingBox.h, yMax);
        }

        return {
            x: xMin,
            y: yMin,
            w: xMax - xMin,
            h: yMax - yMin,
        };
    }

    get image(): HTMLCanvasElement {
        return this._drawingEventWrapper.image;
    }
    
    get isLeaf(): boolean {
        return this._isLeaf;
    }

    get childCount(): number {
        if (this._isLeaf)
            return 0;
        
        return this.childs.length;
    }

    get drawingEventCount(): number {
        let count = 0;
        this.childs.forEach(child => {
            if (child.isLeaf) {
                count++;
            } else {
                count += child.drawingEventCount;
            }
        });
        return count;
    }

    get boundingBox(): BoundingBox {
        return this._drawingEventWrapper.boundingBox;
    }

    get drawingEvent(): DrawingEventWrapper {
        return this._drawingEventWrapper;
    }
}
