import { Payload } from './socket-payload';

export class KickEventPayload extends Payload {
    reason: string;
}