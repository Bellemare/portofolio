import { environment } from "@environments/environment";
import { Payload, SocketPayload } from '@app/classes/socket/socket-payload';
import { PayloadTypes } from '@app/classes/socket/payload-types';
import { SocketEventListener } from './event-listener';
import * as moment from "moment";

export class SocketClient {
    protected readonly WEBSOCKET_HOST: string = environment.websocketHost;
    protected readonly WEBSOCKET_PORT: number = environment.defaultPort;

    private conn: WebSocket;
    private listenerCount: number = 0;
    private onMessageMap: Map<PayloadTypes, SocketEventListener[]> = new Map();
    private id: number;
    onClose: () => void;

    connect(id: number, uri: string, onConnect: () => void): void {
        this.id = id;;
        let url;
        if (this.WEBSOCKET_PORT != 80) {
            url = `${this.WEBSOCKET_HOST}:${this.WEBSOCKET_PORT}${uri}`;
        } else {
            url = `${this.WEBSOCKET_HOST}${uri}`;
        }

        this.conn = new WebSocket(url);
        this.conn.onopen = () => {
            onConnect();
        }
        this.listenClose();
        this.listenMessage();
    }

    getId(): number {
        return this.id;
    }

    closeConnection(): void {
        this.conn.close();
    }

    send(payload: Payload, type: PayloadTypes): void {
        const map = new Map(Object.entries(payload));
        map.forEach((value, key) => {
            if (moment.isMoment(value)) {
                payload[key] = value.format("yyyy/MM/DD hh:mm:ss");
            }
        });

        const data: SocketPayload = {
            type: type.valueOf(),
            payload
        }
        this.conn.send(JSON.stringify(data));
    }

    onMessage(type: PayloadTypes, onmessage: (data: Payload) => void): number {
        if (!this.onMessageMap.has(type)) {
            this.onMessageMap.set(type, []);
        }

        const id = this.listenerCount++;
        this.onMessageMap.get(type)?.push({
            id: id,
            handler: onmessage
        });
        return id; 
    }

    unregister(type: PayloadTypes, id: number): void {
        const listeners = this.onMessageMap.get(type)
        listeners?.forEach((listener: SocketEventListener, index: number) => {
            if (listener.id == id) {
                listeners.splice(index, 1);
                return;
            }
        });
    }

    private listenMessage(): void {
        this.conn.onmessage = (evt: any) => {
            if (this.onMessage != undefined) {
                const data: SocketPayload = JSON.parse(evt.data);
                const listeners = this.onMessageMap.get(data.type);
                listeners?.forEach((listener: SocketEventListener) => {
                    listener.handler(data.payload);
                });
            }
        }
    }

    private listenClose(): void {
        this.conn.onclose = () => {
            if (this.onClose != undefined) {
                this.onClose();
            }
        }
    }
}