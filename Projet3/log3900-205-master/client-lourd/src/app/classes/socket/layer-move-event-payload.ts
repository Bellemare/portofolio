import { Payload } from './socket-payload';

export class LayerMoveEventPayload extends Payload {
    user_id?: number;
    user?: string;
    id: number;
    after: number;
    layer: number;
    new_layer: number;
}