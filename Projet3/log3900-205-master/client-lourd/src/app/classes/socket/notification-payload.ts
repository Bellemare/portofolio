import { Payload } from './socket-payload';

export class NotificationPayload extends Payload {
    message: string;
}