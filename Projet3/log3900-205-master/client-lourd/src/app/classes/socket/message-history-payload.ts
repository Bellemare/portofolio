import { Payload } from './socket-payload';
import { MessagePayload } from './message-payload';

export class MessageHistoryPayload extends Payload {
    messages: MessagePayload[];
}