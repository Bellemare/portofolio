export class Payload {
    [key: string]: any;
}

export class SocketPayload {
    type: number;
    payload: Payload;
}