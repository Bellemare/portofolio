export enum LayerEventType {
    CHANGE = 1,
    DELETE,
    ADD,
    SELECT,
}