export enum ClientStateType {
    CONNECT = 1,
    DISCONNECT,
    CHANGE,
}