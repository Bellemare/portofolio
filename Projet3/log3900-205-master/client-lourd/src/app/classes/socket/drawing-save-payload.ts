import { Payload } from './socket-payload';

export class DrawingSavePayload extends Payload {
    image: string;
}