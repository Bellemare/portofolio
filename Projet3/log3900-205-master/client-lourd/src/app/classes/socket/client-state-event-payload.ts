import { ClientStateType } from './client-state-types';
import { Payload } from './socket-payload';

export class ClientStateEventPayload extends Payload {
    state: ClientStateType;
    user: string;
    user_id: number;
    avatar: string;
}