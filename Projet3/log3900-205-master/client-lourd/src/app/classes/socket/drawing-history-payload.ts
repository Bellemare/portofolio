import { DrawingEvent } from '../drawing/drawing-event';
import { Payload } from './socket-payload';

export class DrawingHistoryPayload extends Payload {
    events: DrawingEvent[];
}