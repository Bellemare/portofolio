import { DrawingText } from '../drawing/drawing-text';
import { Payload } from './socket-payload';

export class LayerTextEventPayload extends Payload {
    user_id?: number;
    user?: string;
    id: number;
    layer: number;
    text: DrawingText;
}