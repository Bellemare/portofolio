import { ClientSync } from './client-sync';
import { Payload } from './socket-payload';

export class ClientStateSyncEventPayload extends Payload {
    clients: ClientSync[];
}