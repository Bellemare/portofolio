import { Payload } from './socket-payload';
import { DrawingEvent } from '@app/classes/drawing/drawing-event';
import { LayerEventType } from './layer-event-type';

export class LayerEventPayload extends Payload {
    event: DrawingEvent;
    user_id?: number;
    user?: string;
    id: number;
    layer: number;
    type: LayerEventType;
}