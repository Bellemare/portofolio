import { Payload } from './socket-payload';

export class SocketEventListener {
    handler: (data: Payload) => void
    id: number;
}