import { Color } from '../drawing/color';
import { Transformation } from '../drawing/transformation';
import { ToolProperties } from '../tool-properties/tool-properties';
import { Payload } from './socket-payload';

export class LayerPreviewEventPayload extends Payload {
    user_id?: number;
    user?: string;
    id: number;
    layer: number;
    transform: Transformation;
    attributes: ToolProperties;
    stroke_color: Color;
    fill_color: Color;
    text_color: Color;
}