import { Payload } from './socket-payload';
import { DrawingEvent } from '@app/classes/drawing/drawing-event';

export class DrawingEventPayload extends Payload {
    event: DrawingEvent;
    user_id: number;
    user: string;
}