export enum MessageType {
    Normal = 0,
    Urgent,
    Information,
    Announcement,
}