import * as moment from 'moment';
import { Payload } from './socket-payload';
import { MessageType } from '@app/classes/socket/message-type';

export class MessagePayload extends Payload {
    id?: number;
    user_id?: number;
    user?: string;
    message: string;
    message_type: MessageType;
    sent_at?: moment.Moment;

    reply_to?: number;
    reply_message?: string;

    recipients: number[];
}