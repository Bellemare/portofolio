export enum SelectionResizeScalers {
    TopLeft = 0,
    TopRight,
    BottomLeft,
    BottomRight,
    Top,
    Left,
    Right,
    Bottom,
    Center,
}
