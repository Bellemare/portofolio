import { SelectionResizeScalers } from './selection-resize-scalers';

export type SelectionScalerMirror = [SelectionResizeScalers, SelectionResizeScalers];
