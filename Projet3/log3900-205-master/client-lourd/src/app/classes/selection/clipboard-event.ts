export enum ClipboardEvent {
    Delete = 0,
    Paste,
    PasteDelete,
}
