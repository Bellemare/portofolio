import { Vec2 } from '@app/classes/drawing/vec2';
import { SelectionResizeScalers } from '@app/classes/selection/selection-resize-scalers';

export interface ScalerEvent {
    mousePosition: Vec2;
    scalerPosition: SelectionResizeScalers;
}
