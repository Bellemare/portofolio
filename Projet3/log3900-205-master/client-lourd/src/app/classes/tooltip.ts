export interface Tooltip {
    name: string;
    shortcut?: string;
    delay?: number;
    position?: string;
}
