import { DrawingEvent } from './drawing/drawing-event';
import { ResizeEvent } from './drawing/resize-event';

export type UndoRedoEntry = DrawingEvent | ResizeEvent;
