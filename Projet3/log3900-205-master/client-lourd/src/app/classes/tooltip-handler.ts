import { Tooltip } from './tooltip';

export class TooltipHandler {
    static readonly DEFAULT_DELAY: number = 500;
    static readonly DEFAULT_POSITION: string = 'right';

    static arrayToString(tooltips: Tooltip[]): string {
        let str = '';
        tooltips.forEach((tooltip: Tooltip) => {
            str += TooltipHandler.toString(tooltip) + '\n';
        });
        return str;
    }

    static toString(tooltip: Tooltip): string {
        return `o ${tooltip.name}${tooltip.shortcut !== '' && tooltip.shortcut !== undefined ? ` (${tooltip.shortcut})` : ''}`;
    }
}
