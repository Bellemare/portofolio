import { Vec2 } from './drawing/vec2';

export type RectangleBox = {
    topLeft: Vec2;
    bottomRight: Vec2;
};
