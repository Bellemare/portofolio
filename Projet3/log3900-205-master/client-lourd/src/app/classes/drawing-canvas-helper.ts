import { Vec2 } from './drawing/vec2';

export class DrawingCanvasHelper {
    static readonly OFFSET_TOP: number = 60;
    static readonly OFFSET_LEFT: number = 260;
    static readonly BOTTOM_MARGIN: number = 50;
    static readonly RIGHT_MARGIN: number = 50;

    private static canvas: HTMLCanvasElement;
    private static prevCanvas: HTMLCanvasElement;
    private static collabPrevCanvas: HTMLCanvasElement;

    static absolutePositionToRelative(absolutePosition: Vec2): Vec2 {
        return { x: absolutePosition.x - DrawingCanvasHelper.OFFSET_LEFT, y: absolutePosition.y - DrawingCanvasHelper.OFFSET_TOP };
    }

    static relativePositionToAbsolute(relativePosition: Vec2): Vec2 {
        return { x: relativePosition.x + DrawingCanvasHelper.OFFSET_LEFT, y: relativePosition.y + DrawingCanvasHelper.OFFSET_TOP };
    }

    static set baseCanvas(canvas: HTMLCanvasElement) {
        DrawingCanvasHelper.canvas = canvas;
    }

    static get baseCanvas(): HTMLCanvasElement {
        return DrawingCanvasHelper.canvas;
    }

    static set previewCanvas(canvas: HTMLCanvasElement) {
        DrawingCanvasHelper.prevCanvas = canvas;
    }

    static get previewCanvas(): HTMLCanvasElement {
        return DrawingCanvasHelper.prevCanvas;
    }

    static set collabPreviewCanvas(canvas: HTMLCanvasElement) {
        DrawingCanvasHelper.collabPrevCanvas = canvas;
    }

    static get collabPreviewCanvas(): HTMLCanvasElement {
        return DrawingCanvasHelper.collabPrevCanvas;
    }
}
