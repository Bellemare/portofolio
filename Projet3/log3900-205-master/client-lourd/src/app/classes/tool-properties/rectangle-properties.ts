import { ShapeProperties } from './shape-properties';

export type RectangleProperties = ShapeProperties;
