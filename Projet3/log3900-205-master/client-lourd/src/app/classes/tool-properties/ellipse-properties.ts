import { ShapeProperties } from './shape-properties';

export type EllipseProperties = ShapeProperties;
