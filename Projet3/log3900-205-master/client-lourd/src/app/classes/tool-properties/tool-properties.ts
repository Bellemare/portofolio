import { DrawingType } from '@app/classes/drawing/drawing-type';
import { ToolProperty } from './tool-property';

export type ToolProperties = {
    [key: string]: ToolProperty;
    drawing_type: DrawingType;
    line_width: number;
};
