import { ToolProperties } from './tool-properties';

export interface ShapeProperties extends ToolProperties {
    line_width: number;
}
