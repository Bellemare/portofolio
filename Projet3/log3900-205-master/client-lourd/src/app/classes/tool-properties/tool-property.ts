export type ToolProperty = number | string | [] | object | CanvasLineJoin | CanvasLineCap | CanvasTextAlign | CanvasTextBaseline | boolean;
