import { ToolProperties } from './tool-properties';

export interface TextProperties extends ToolProperties {
    font: string;
    fontSize: number;
    isBold: boolean;
    isItalic: boolean;
    alignment: CanvasTextAlign;
    baseline: CanvasTextBaseline;
}
