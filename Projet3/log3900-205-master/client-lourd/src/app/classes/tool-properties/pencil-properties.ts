import { ToolProperties } from './tool-properties';

export interface PencilProperties extends ToolProperties {
    line_width: number;
}
