import { ToolProperties } from './tool-properties';

export interface EllipseSelectionProperties extends ToolProperties {}
