export class ImgViewerHelper {
  static loadImageData(dst: HTMLCanvasElement, src: HTMLImageElement): void {
    const canvasSize = { x: dst.width, y: dst.height };
    const ratioWidth = canvasSize.x / src.width;
    const ratioHeight = canvasSize.y / src.height;
    const ratio = Math.min(ratioWidth, ratioHeight);
    const adjustedSize = { x: src.width * ratio, y: src.height * ratio };
    const corner = {
        x: (canvasSize.x - adjustedSize.x) / 2,
        y: (canvasSize.y - adjustedSize.y) / 2,
    };
    const ctx = dst.getContext("2d") as CanvasRenderingContext2D;
    ctx.clearRect(0, 0, canvasSize.x, canvasSize.y);
    ctx.drawImage(src, corner.x, corner.y, adjustedSize.x, adjustedSize.y);
  }
}