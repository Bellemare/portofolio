import { EventEmitter, Injectable } from "@angular/core";
import { CanvasSizeService } from "@app/services/canvas-size/canvas-size.service";
import { BoundingBox } from "./bounding-box";
import { DrawingEvent } from "./drawing-event";

@Injectable({
    providedIn: "root",
})
export class DrawingEventWrapper {
    render = new EventEmitter<void>();
    isSelected: boolean = false;
    selectedBy: number = -1;
    drawingEvent?: DrawingEvent;
    private _boundingBox: BoundingBox;
    private _image: HTMLCanvasElement;

    private _renderFct: (evt: DrawingEventWrapper) => boolean;
    private _redrawFct: (evt: DrawingEvent) => void;

    constructor(drawing_event?: DrawingEvent)  {
        if (drawing_event != undefined) {
            this.drawingEvent = drawing_event;
            if (drawing_event.data.bounding_box == undefined) {
                this._boundingBox = new BoundingBox();
            } else {
                this.boundingBox = drawing_event.data.bounding_box;
            }
        }

        this._image = document.createElement("canvas");
        this._image.width = CanvasSizeService.canvasSize.x;
        this._image.height = CanvasSizeService.canvasSize.y;
    }

    setBaseImage(image: HTMLCanvasElement): void {
        this._image = image;
    }

    renderLayer(): boolean {
        if (this._renderFct != undefined) {
            return this._renderFct(this);
        }

        return false;
    }

    redraw(): boolean {
        if (this._redrawFct != undefined && this.drawingEvent != undefined) {
            this._redrawFct(this.drawingEvent);
            return true;
        }

        return false;
    }

    set renderFct(fct: (evt: DrawingEventWrapper) => boolean) {
        this._renderFct = fct;
    }

    set redrawFct(fct: (evt: DrawingEvent) => void) {
        this._redrawFct = fct;
    }

    get boundingBox(): BoundingBox {
        return this._boundingBox;
    }

    set boundingBox(boundingBox: BoundingBox) {
        this._boundingBox = boundingBox;
    }

    get image(): HTMLCanvasElement {
        return this._image;
    }

    set image(image: HTMLCanvasElement) {
        this._image = image;
        if (!this.isSelected)
            this.render.emit();
    }
}
