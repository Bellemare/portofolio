import { Injectable, Injector } from '@angular/core';
import { EllipseAttributesComponent } from '@app/app-components/tool-components/ellipse-attributes/ellipse-attributes.component';
import { PencilAttributesComponent } from '@app/app-components/tool-components/pencil-attributes/pencil-attributes.component';
import { RectAttributesComponent } from '@app/app-components/tool-components/rect-attributes/rect-attributes.component';
import { SelectionAttributesComponent } from '@app/app-components/tool-components/selection-attributes/selection-attributes.component';
import { ToolAttributesComponent } from '@app/app-components/tool-components/tool-attributes/tool-attributes.component';
import { ToolParameter } from '@app/classes/tool-parameter';
import { EllipseService } from '@app/services/tools/ellipse.service';
import { PencilService } from '@app/services/tools/pencil-service';
import { RectangleService } from '@app/services/tools/rectangle.service';
import { SelectionService } from '@app/services/tools/selection/selection.service';
import { ToolService } from '@app/services/tools/tool.service';
import { DrawingElement } from './drawing-element';
import { DrawingEllipse } from './drawing-ellipse';
import { DrawingPos } from './drawing-pos';
import { DrawingRect } from './drawing-rect';
import { ToolType } from './tool-type';

@Injectable({
    providedIn: 'root',
})
export class ToolClasses {
    private static readonly TOOL_COMPONENTS: {
        [key: string]: new (...args: ToolParameter[]) => ToolAttributesComponent;
    } = {
        pencil: PencilAttributesComponent,
        rectangle: RectAttributesComponent,
        ellipse: EllipseAttributesComponent,
        selection: SelectionAttributesComponent,
    };

    private static readonly TOOL_SERVICES: {
        [key: string]: new (...args: ToolParameter[]) => ToolService;
    } = {
        pencil: PencilService,
        rectangle: RectangleService,
        ellipse: EllipseService,
        selection: SelectionService,
    };

    static readonly TOOLS: string[] = [
        'pencil',
        'rectangle',
        'ellipse',
        'selection',
    ];

    constructor(private injector: Injector) {}

    getToolService(toolName: string): ToolService {
        return this.injector.get(ToolClasses.TOOL_SERVICES[toolName]);
    }

    getToolComponent(toolName: string): new (t: ToolService) => ToolAttributesComponent {
        return ToolClasses.TOOL_COMPONENTS[toolName];
    }

    static registerNewElement(data: DrawingElement, type: ToolType): DrawingElement {
        let element: DrawingElement = new DrawingElement(data);
        switch (type) {
            case ToolType.PENCIL:
                element = new DrawingPos(data);
                break;
            case ToolType.RECTANGLE:
                element = new DrawingRect(data);
                break;
            case ToolType.ELLIPSE:
                element = new DrawingEllipse(data);
                break;
        }
        return element;
    }
}
