import { DrawingElement } from './drawing-element';

export class DrawingText extends DrawingElement {
    text: string;
    
    constructor(init?:Partial<DrawingText>) {
        super(init);
        if (this.text == undefined) {
            this.text = "";
        }
    }

    insertText(value: string, position?: number): void {
        if (position != undefined) {
            this.text = this.text.substring(0, position) + value + this.text.substring(position, this.text.length);
        } else {
            this.text += value;
        }
    }

    breakLine(): void {
        this.text += "\n";
    }

    back(): void {
        this.text = this.text.slice(0, this.text.length - 1);
    }

    remove(start: number, end: number): void {
        if (start == end) {
            start--;
        }
        this.text = this.text.substring(0, start) + this.text.substring(end, this.text.length);
    }
}
