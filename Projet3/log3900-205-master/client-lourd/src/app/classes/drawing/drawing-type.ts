export enum DrawingType {
    Stroke = 0,
    Filled,
    StrokeFilled,
}
