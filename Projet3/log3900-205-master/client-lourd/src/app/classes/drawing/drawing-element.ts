import { MouseButton } from '@app/classes/mouse-button';
import { ToolProperty } from '@app/classes/tool-properties/tool-property';
import { DrawingEventPayload } from '../socket/drawing-event-payload';
import { ToolProperties } from '../tool-properties/tool-properties';
import { BoundingBox } from './bounding-box';
import { Transformation } from './transformation';
import { Vec2 } from './vec2';

export class DrawingElement {
    [key: string]: ToolProperty | undefined | HTMLImageElement | Vec2 | MouseButton;
    base_bounding_box?: BoundingBox;
    bounding_box?: BoundingBox;
    transformation?: Transformation;

    constructor(init?:Partial<DrawingElement>) {
        Object.assign(this, init);
    }

    getEditBoundingBox(): BoundingBox|undefined {
        return this.bounding_box;
    }

    handleNewEvent(evt: DrawingEventPayload): boolean {
        return false
    }

    createNewEvent(previousEvent?: DrawingElement): DrawingElement {
        return this;
    }

    getTopLeftPosition(attributes: ToolProperties): Vec2 {
        return { x: 0, y: 0 };
    }

    protected setBaseTransformation(): void {
        this.transformation = {
            dx: 0, 
            dy: 0,
            dw: 1,
            dh: 1,
        };
    }

    applyScaling(dw: number, dh: number, flipX?: boolean, flipY?: boolean, attributes?: ToolProperties): boolean {
        if (!this.transformation)
            this.setBaseTransformation();

        const transformation = this.transformation as Transformation;
        if (this.bounding_box && this.base_bounding_box) {
            transformation.dw = (this.bounding_box.w * Math.abs(dw) / this.base_bounding_box.w) * Math.sign(dw);
            transformation.dh = (this.bounding_box.h * Math.abs(dh) / this.base_bounding_box.h) * Math.sign(dh);
        }
        return true;
    }

    applyTranslation(dx: number, dy: number): void {
        if (!this.transformation)
            this.setBaseTransformation();

        const transformation = this.transformation as Transformation;
        if (this.bounding_box && this.base_bounding_box) {
            transformation.dx = (this.bounding_box.x + dx) - this.base_bounding_box.x;
            transformation.dy = (this.bounding_box.x + dy) - this.base_bounding_box.y;
        }
    }

    get padding(): number {
        return 0;
    }
}
