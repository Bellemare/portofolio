import { DrawingText } from './drawing-text';

// tslint:disable:no-any
// tslint:disable:no-string-literal
// tslint:disable:no-magic-numbers
describe('DrawingText', () => {
    let drawingText: DrawingText;
    let canvas: HTMLCanvasElement;
    let ctx: CanvasRenderingContext2D;
    let lines: string[];

    beforeEach(() => {
        lines = ['OOOOOOOOOOOO', 'OOOOOO', 'OOOOOOOOO', 'O'];
        const lineCopy = Array<string>();
        for (const line of lines) {
            lineCopy.push(line);
        }
        drawingText = new DrawingText(5, 10, lineCopy, 1, 2);
        canvas = document.createElement('canvas');
        canvas.width = 200;
        canvas.height = 100;
        ctx = canvas.getContext('2d') as CanvasRenderingContext2D;
        ctx.font = '14px Arial';
        ctx.textBaseline = 'middle';
        ctx.textAlign = 'left';
    });

    it('should create an instance', () => {
        expect(new DrawingText()).toBeTruthy();
    });

    it('insert text should insert text at cursor position', () => {
        drawingText.insertText('aa');
        expect(drawingText.lines[1]).toEqual('OOaaOOOO');
    });

    it('backspace should delete character if not at beginning of line', () => {
        drawingText.backspace();
        expect(drawingText.lines[1]).toEqual('OOOOO');
    });

    it('backspace should merge lines if at beginning of line', () => {
        drawingText.charIndex = 0;
        drawingText.backspace();
        expect(drawingText.lines[0]).toEqual(lines[0].concat(lines[1]));
        expect(drawingText.lines[1]).toEqual(lines[2]);
    });

    it('delete should delete character if not at beginning of line', () => {
        drawingText.delete();
        expect(drawingText.lines[1]).toEqual('OOOOO');
    });

    it('delete should merge lines if at beginning of line', () => {
        drawingText.charIndex = drawingText.currentLine.length;
        drawingText.delete();
        expect(drawingText.lines[1]).toEqual(lines[1].concat(lines[2]));
        expect(drawingText.lines[2]).toEqual(lines[3]);
    });

    it('breakline should make the current line into two lines', () => {
        drawingText.breakLine();
        expect(drawingText.lines[1]).toEqual(lines[1].slice(0, 2));
        expect(drawingText.lines[2]).toEqual(lines[1].slice(2));
    });

    it('moveCursorLeft should move cursor one character if not at beginning of line', () => {
        drawingText.moveCursorLeft();
        expect(drawingText.charIndex).toEqual(1);
    });

    it('moveCursorRightt should move cursor one character if not at end of line', () => {
        drawingText.moveCursorRight();
        expect(drawingText.charIndex).toEqual(3);
    });

    it('moveCursorLeft should change lines if at beginning of line', () => {
        drawingText.charIndex = 0;
        drawingText.moveCursorLeft();
        expect(drawingText.lineIndex).toEqual(0);
        expect(drawingText.charIndex).toEqual(drawingText.currentLine.length);
    });

    it('moveCursorRight should change lines if at end of line', () => {
        drawingText.charIndex = drawingText.currentLine.length;
        drawingText.moveCursorRight();
        expect(drawingText.lineIndex).toEqual(2);
        expect(drawingText.charIndex).toEqual(0);
    });

    it('moveCursorUp should change lines if not in first line', () => {
        drawingText.moveCursorUp(ctx);
        expect(drawingText.lineIndex).toEqual(0);
        expect(drawingText.charIndex).toEqual(2);
    });

    it('moveCursorDown should change lines if not in last line', () => {
        drawingText.moveCursorDown(ctx);
        expect(drawingText.lineIndex).toEqual(2);
        expect(drawingText.charIndex).toEqual(2);
    });
});
