import { DrawingElement } from './drawing-element';

export class DrawingImage extends DrawingElement {
    image: CanvasImageSource;
    dx: number;
    dy: number;
}
