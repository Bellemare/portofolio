export enum ToolType {
    UNDEFINED = 0,
    PENCIL,
    ELLIPSE,
    RECTANGLE,
    SELECTION,
}