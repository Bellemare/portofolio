export class Transformation {
    dx: number;
    dy: number;
    dw: number;
    dh: number;

    flip_x?: boolean;
    flip_y?: boolean;
}
