import { RectangleProperties } from '../tool-properties/rectangle-properties';
import { BoundingBox } from './bounding-box';
import { DrawingShape } from './drawing-shape';
import { DrawingText } from './drawing-text';
import { Vec2 } from './vec2';

export class DrawingRect extends DrawingShape {
    x: number;
    y: number;
    width: number;
    height: number;
    
    constructor(init?:Partial<DrawingRect>) {
        super(init);
        this.text = new DrawingText(init?.text);
    }

    getEditBoundingBox(): BoundingBox|undefined {
        if (this.bounding_box == undefined)
            return undefined;

        return {
            x: this.bounding_box.x,
            y: this.bounding_box.y,
            w: this.bounding_box.w,
            h: Math.abs(this.height),
        };
    }
    
    generateTextLines(ctx: CanvasRenderingContext2D): string[] {
        if (this.text == undefined || this.text.text.length == 0)
            return [];
        
        const dataLines = this.text.text.split("\n");
        const maxWidth = this.width - (ctx.lineWidth * 2) - this.TEXT_PADDING * 2;
        const lines: string[] = [];
        dataLines.forEach((line: string) => {
            lines.push(this.getNewTextLine(ctx, line, () => maxWidth));
        });

        return lines;
    }

    getTopLeftPosition(attributes: RectangleProperties): Vec2 {
        return { x: Math.min(this.x, this.x + this.width), y: Math.min(this.y, this.y + this.height) }; 
    }

    applyScaling(dw: number, dh: number): boolean {
        if (dw == 0 || dh == 0)
            return false;
        
        super.applyScaling(dw, dh);
        this.width = Math.abs(this.width * dw);
        this.height = Math.abs(this.height * dh);
        return true;
    }

    applyTranslation(dx: number, dy: number): void {
        super.applyTranslation(dx, dy);
        this.x += dx;
        this.y += dy;
    }
}
