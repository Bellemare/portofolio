import { EllipseProperties } from '../tool-properties/ellipse-properties';
import { BoundingBox } from './bounding-box';
import { DrawingShape } from './drawing-shape';
import { DrawingText } from './drawing-text';
import { Vec2 } from './vec2';

export class DrawingEllipse extends DrawingShape {
    x: number;
    y: number;
    radius_x: number;
    radius_y: number;
    
    constructor(init?:Partial<DrawingEllipse>) {
        super(init);
        this.text = new DrawingText(init?.text);
    }

    getEditBoundingBox(): BoundingBox|undefined {
        if (this.bounding_box == undefined)
            return undefined;

        return {
            x: this.bounding_box.x,
            y: this.bounding_box.y,
            w: this.bounding_box.w,
            h: Math.abs(this.radius_y * 2),
        };
    }
    
    generateTextLines(ctx: CanvasRenderingContext2D): string[] {
        if (this.text == undefined || this.text.text.length == 0)
            return [];
        
        const lineHeight = this.getLineHeight(ctx);
        const dataLines = this.text.text.split("\n");
        const lines: string[] = [];
        const apparentRadiusX = this.radius_x - this.TEXT_PADDING - ctx.lineWidth;
        const apparentRadiusY = this.radius_y - this.TEXT_PADDING - ctx.lineWidth;
        let currentIndex = 0;
        let max = 0;
        dataLines.forEach((line: string, i: number) => {
            lines.push(this.getNewTextLine(ctx, line, () => {
                let maxWidth = this.getMaxWidth(lineHeight, apparentRadiusX, apparentRadiusY, currentIndex++);
                if (maxWidth == 0) {
                    maxWidth = max;
                }
                max = Math.max(maxWidth, max);
                return maxWidth;
            }));
        });

        return lines;
    }

    private getMaxWidth(lineHeight: number, apparentRadiusX: number, apparentRadiusY: number, i: number): number {
        const height = lineHeight * (i + 1);
        const y = Math.abs(apparentRadiusY - height);
        const x = Math.sqrt(Math.pow(apparentRadiusX, 2) * (1 - (Math.pow(y, 2)/Math.pow(apparentRadiusY, 2))));
        let maxWidth = x * 2;
        if (!x) {
            maxWidth = 0;
        }

        return maxWidth;
    }

    private getLineHeight(ctx: CanvasRenderingContext2D): number {
        const referenceMetrics = ctx.measureText("|");
        const referenceHeight = referenceMetrics.actualBoundingBoxAscent + referenceMetrics.actualBoundingBoxDescent;
        return referenceHeight * 1.1;
    }

    getTopLeftPosition(attributes: EllipseProperties): Vec2 {
        return { x: this.x - this.radius_x, y: this.y - this.radius_y };
    }

    applyScaling(dw: number, dh: number): boolean {
        if (dw == 0 || dh == 0)
            return false;
        
        super.applyScaling(dw, dh);
        this.radius_x = Math.abs(this.radius_x * dw);
        this.radius_y = Math.abs(this.radius_y * dh);
        return true;
    }
    
    applyTranslation(dx: number, dy: number): void {
        super.applyTranslation(dx, dy);
        this.x += dx;
        this.y += dy;
    }
}
