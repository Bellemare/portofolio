export class BoundingBox {
    x: number;
    y: number;
    w: number;
    h: number;
}
