import { Vec2 } from './vec2';

export class ResizeEvent {
    sizeBefore: Vec2;
    sizeAfter: Vec2;
}
