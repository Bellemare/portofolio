import { DrawingElement } from './drawing-element';
import { DrawingText } from './drawing-text';
import lettersMap from './letters-widths';

export class DrawingShape extends DrawingElement {
    readonly TEXT_PADDING: number = 10;
    
    text?: DrawingText;
    
    generateTextLines(ctx: CanvasRenderingContext2D): string[] {
        return this.text == undefined ? []:this.text?.text.split("\n");
    }

    private measureText(text: string): number {
        let lineWidth = 0;
        const letters = [...text];

        for (let i = 0; i < letters.length; i++) {
            if (lettersMap.has(letters[i])) {
                lineWidth += lettersMap.get(letters[i]) as number;
            } else {
                lineWidth += 55.625;
            }
        }

        return lineWidth * 0.16;
    }

    protected getNewTextLine(ctx: CanvasRenderingContext2D, line: string, maxWidthHandler: () => number): string {
        const lineWidth = this.measureText(line);
        let maxWidth = maxWidthHandler();
        maxWidth = Math.max(maxWidth, 10);

        if (lineWidth > maxWidth && line.length > 1) {
            let charIndex = 1;
            let width = this.measureText(line.substring(0, charIndex++));
            while (width < maxWidth) {
                charIndex++;
                width = this.measureText(line.substring(0, charIndex));
                if (line.substring(0, charIndex - 1).length <= 1 && width >= maxWidth) 
                    break; 
            }

            let newLine = line.substring(0, charIndex - 1);
            const nextLine = line.substring(charIndex - 1, line.length);
            if (nextLine.length != 0) {
                newLine += "\n" + this.getNewTextLine(ctx, nextLine, maxWidthHandler);
            }

            return newLine;
        }

        return line;
    }
}
