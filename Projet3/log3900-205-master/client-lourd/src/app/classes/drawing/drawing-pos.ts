import { DrawingEventPayload } from '../socket/drawing-event-payload';
import { PencilProperties } from '../tool-properties/pencil-properties';
import { DrawingElement } from './drawing-element';
import { Vec2 } from './vec2';

export class DrawingPos extends DrawingElement {
    data: Vec2[];

    handleNewEvent(evt: DrawingEventPayload): boolean {
        const drawingPos = evt.event.data as DrawingPos;
        this.data.push(...drawingPos.data);
        
        return true;
    }

    createNewEvent(previousEvent?: DrawingPos): DrawingElement {
        if (previousEvent == undefined || previousEvent.data == undefined) {
            return this;
        }
        const newEvent = new DrawingPos();
        newEvent.data = this.data.slice(previousEvent.data.length - 1, this.data.length - 1);

        return newEvent;
    }

    getTransformedLineWidth(attributes: PencilProperties): number {
        if (this.transformation) {
            const adw = Math.abs(this.transformation.dw);
            const adh = Math.abs(this.transformation.dh);
            return Math.max(attributes.line_width * Math.min(adw, adh), 1);
        }

        return attributes.line_width;
    }

    getTopLeftPosition(attributes: PencilProperties): Vec2 {
        let xMin = Number.MAX_SAFE_INTEGER;
        let yMin = Number.MAX_SAFE_INTEGER;
        const lw = this.getTransformedLineWidth(attributes);
        const lineRadius = lw / 2;
        this.data.forEach((pt: Vec2) => {
            xMin = Math.min(pt.x - lineRadius - this.padding, xMin);
            yMin = Math.min(pt.y - lineRadius - this.padding, yMin);
        });
        return { x: Math.floor(xMin), y: Math.floor(yMin) };
    }

    applyScaling(dw: number, dh: number, flipX: boolean, flipY: boolean, attributes: PencilProperties): boolean {
        if (dw == 0 || dh == 0)
            return false;
            
        super.applyScaling(dw, dh);
        let adw = Math.abs(dw);
        let adh = Math.abs(dh);
        if (this.bounding_box != undefined) {
            const lw = this.getTransformedLineWidth(attributes);
            const transformX = this.bounding_box.w * adw >= lw && this.bounding_box.w * adw > 5;
            const transformY = this.bounding_box.h * adh >= lw && this.bounding_box.h * adh > 5;

            const boundingBox = this.bounding_box;
            const centerX = boundingBox.x + boundingBox.w / 2;
            const centerY = boundingBox.y + boundingBox.h / 2;
            
            this.data.forEach((pt: Vec2) => {
                let x = transformX ? (pt.x - centerX) * adw:pt.x;
                let y = transformY ? (pt.y - centerY) * adh:pt.y;
                if (flipX) {
                    x = -x;
                }
                if (flipY) {
                    y = -y;
                }
                pt.x = x + (transformX ? centerX:0);
                pt.y = y + (transformY ? centerY:0);
            });
        }
        return true;
    }

    applyTranslation(dx: number, dy: number): void {
        super.applyTranslation(dx, dy);
        this.data.forEach((pt: Vec2) => {
            pt.x += dx;
            pt.y += dy;
        });
    }

    get padding(): number {
        return 5;
    }
}
