import { ClipboardEvent } from '@app/classes/selection/clipboard-event';
import { SelectionStates } from '@app/classes/selection/states';
import { DrawingElement } from './drawing-element';
import { Vec2 } from './vec2';

export class DrawingSelection extends DrawingElement {
    [key: string]: any;

    startSize: Vec2;
    startPosition: Vec2;

    size: Vec2;
    position: Vec2;

    state: SelectionStates;
    clipboardEvent?: ClipboardEvent;

    imageData?: HTMLCanvasElement;
}
