import { ToolProperties } from '@app/classes/tool-properties/tool-properties';
import { ToolType } from '@app/classes/drawing/tool-type';
import { Color } from './color';
import { DrawingElement } from './drawing-element';
import { DrawingType } from './drawing-type';
import { EventType } from '@app/classes/drawing/event-type';

export class DrawingEvent {
    type: EventType;
    data: DrawingElement;
    tool_type: ToolType;
    attributes: ToolProperties;
    stroke_color: Color;
    fill_color: Color;
    text_color: Color;
    drawing_type: DrawingType;
    id?: number;
}
