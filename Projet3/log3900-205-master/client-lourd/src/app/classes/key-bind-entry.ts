import { ToolService } from '@app/services/tools/tool.service';
export interface KeyBindEntry {
    keybind: string;
    callback: () => void;
    activateOnEvent?: boolean;
    disabled?: boolean;
    activationVerification?: () => boolean;
    tool?: ToolService;
}
