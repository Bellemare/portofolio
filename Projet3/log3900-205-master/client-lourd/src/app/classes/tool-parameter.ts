import { Renderer2 } from '@angular/core';
import { AttributesManagerService } from '@app/services/api/attributes-manager.service';
import { CanvasSizeService } from '@app/services/canvas-size/canvas-size.service';
import { CouchesService } from '@app/services/couches/couches.service';
import { ToolDrawingService } from '@app/services/drawing-services/tool-drawing.service';
import { KeyBindingResolverService } from '@app/services/key-binding/key-binding-resolver.service';
import { SelectionEditorService } from '@app/services/selection/selection-editor.service';
import { SelectionResizeService } from '@app/services/selection/selection-resize.service';
import { SynchronizerService } from '@app/services/synchronization/synchronizer.service';
import { ColorService } from '@app/services/tools/color.service';
import { ToolService } from '@app/services/tools/tool.service';

export type ToolParameter =
    | AttributesManagerService
    | KeyBindingResolverService
    | ColorService
    | SelectionEditorService
    | Renderer2
    | SelectionResizeService
    | ToolService
    | CanvasSizeService
    | CouchesService
    | ToolDrawingService
    | SynchronizerService;
