import { DrawingCanvasHelper } from '@app/classes/drawing-canvas-helper';
import { Vec2 } from './drawing/vec2';
describe('DrawingCanvasHelper', () => {
    const relative: Vec2 = { x: 0, y: 0 };
    const absolute: Vec2 = { x: DrawingCanvasHelper.OFFSET_LEFT, y: DrawingCanvasHelper.OFFSET_TOP };

    it('should change relative position to absolute', () => {
        expect(DrawingCanvasHelper.relativePositionToAbsolute(relative)).toEqual(absolute);
    });

    it('should change absolute position to relative', () => {
        expect(DrawingCanvasHelper.absolutePositionToRelative(absolute)).toEqual(relative);
    });
});
