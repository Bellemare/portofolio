// Sons importés de: https://mixkit.co/free-sound-effects/notification/

export enum SoundType {
    NewCollaborator="mixkit-positive.wav",
    NewMessage="mixkit-message.mp3",
    NewContact="mixkit-stopwatch.wav",
    NewJoinRequest="mixkit-correct.wav",
}