import { ElementRef } from '@angular/core';
import { MouseEventWrapper } from '@app/classes/mouse-event-wrapper';
import { DrawingCanvasHelper } from './drawing-canvas-helper';

describe('MouseEventWrapper', () => {
    let instance: MouseEventWrapper;
    let event: MouseEvent;

    beforeEach(() => {
        event = {
            offsetX: 50,
            offsetY: 50,
            x: 100,
            y: 100,
            button: 0,
            buttons: 1,
        } as MouseEvent;
        instance = new MouseEventWrapper(event);
    });

    it('should be created', () => {
        expect(instance).toBeTruthy();
    });

    it('should return event', () => {
        expect(instance.mouseEvent).toEqual(event);
    });

    it('should check if left button is pressed', () => {
        expect(MouseEventWrapper.isLeftButtonPressed({ button: 0, buttons: 1 } as MouseEvent)).toEqual(true);
        expect(MouseEventWrapper.isLeftButtonPressed({ button: 0, buttons: 0 } as MouseEvent)).toEqual(false);
    });

    it('should return correct relative position', () => {
        expect(instance.relativePosition).toEqual({ x: event.offsetX, y: event.offsetY });

        instance.isOutsideWindow = true;
        expect(instance.relativePosition).toEqual({ x: event.x - DrawingCanvasHelper.OFFSET_LEFT, y: event.y - DrawingCanvasHelper.OFFSET_TOP });
    });

    it('should return correct absolute position', () => {
        expect(instance.absolutePosition).toEqual({ x: event.x, y: event.y });
    });

    it('should return button', () => {
        expect(instance.button).toEqual(event.button);
    });

    it('should return buttons', () => {
        expect(instance.buttons).toEqual(event.buttons);
    });

    it('should check if left is pressed', () => {
        expect(instance.isLeftButton).toEqual(true);
    });

    it('should check if any buttons are pressed', () => {
        expect(instance.anyButtonsPressed).toEqual(true);
    });

    it('should return correct relative position to element', () => {
        const element = {
            nativeElement: {
                offsetTop: 20,
                offsetLeft: 20,
            },
        } as ElementRef;
        instance = new MouseEventWrapper(event, element);
        expect(instance.relativePosition).toEqual({
            x: event.offsetX - element.nativeElement.offsetLeft,
            y: event.offsetY - element.nativeElement.offsetTop,
        });
    });
});
