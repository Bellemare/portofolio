export enum TextFonts {
    ComicSansMS = 'Comic Sans MS',
    Arial = 'Arial',
    TimesNewRoman = 'Times New Roman',
    Impact = 'Impact',
    Courier = 'Courier',
}
