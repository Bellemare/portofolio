import { IEnvironment } from './IEnvironment';

export const environment: IEnvironment = {
    production: true,
    defaultHost: 'https://projet3.antoinec.dev',
    websocketHost: 'wss://projet3.antoinec.dev',
    defaultPort: 80,
    defaultEndpoint: '',
};
