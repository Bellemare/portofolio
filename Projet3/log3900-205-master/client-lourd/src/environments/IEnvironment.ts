/* tslint:disable: file-name-casing */
export interface IEnvironment {
    production: boolean;
    defaultHost: string;
    websocketHost: string;
    defaultPort: number;
    defaultEndpoint: string;
}
