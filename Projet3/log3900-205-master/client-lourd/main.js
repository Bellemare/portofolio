const {
  app,
  BrowserWindow,
  ipcMain,
  screen
} = require('electron')
const url = require("url");
const path = require("path");

let appWindow;

function openWindow(window, route = "") {
  window.setMenuBarVisibility(false)
  route = route == "" ? "":`#${route}`
  window.loadURL(
    url.format({
      pathname: path.join(__dirname, `/dist/client/index.html`),
      protocol: "file:",
      slashes: false
    }) + route
  );
}

function getDisplayHeight() {
  return screen.getPrimaryDisplay().bounds.height; 
}

function getDisplayWidth() {
  return screen.getPrimaryDisplay().bounds.width; 
}

function initMainWindow() {
  appWindow = new BrowserWindow({
    // fullscreen: true,
    height: getDisplayHeight(),
    width: getDisplayWidth(),
    webPreferences: {
      nodeIntegration: true,
      preload: __dirname + '/preload.js'
    }
  });
  appWindow.maximize();

  // Electron Build Path
  openWindow(appWindow);

  appWindow.on('closed', function () {
    appWindow = null
  })
  appWindow.webContents.on("did-fail-load", () => {
    const path = appWindow.webContents.getURL().split("#");
    const route = path.length > 1 ? path[1]:""
    openWindow(appWindow, route);
  })
}

let newDrawingRoomClient

function getDrawingRoomClient() {
  return new Promise((resolve) => {
    const getId = () => {
      if (newDrawingRoomClient != undefined) {
        resolve(newDrawingRoomClient);
      } else {
        setTimeout(() => {
          getId();
        }, 500);
      }
    }

    getId();
  });
}

function waitUnsetDrawingRoomClient() {
  return new Promise((resolve) => {
    const getId = () => {
      if (newDrawingRoomClient == undefined) {
        resolve();
      } else {
        setTimeout(() => {
          getId();
        }, 500);
      }
    }

    getId();
  });
}

function popoutChat() {
  const displayHeight = getDisplayHeight();
  const displayWidth = getDisplayWidth();

  return new Promise((resolve, reject) => {
    const window = new BrowserWindow({
      height: displayHeight * 0.80,
      width: displayWidth * 0.25,
      x: displayWidth - (displayWidth * 0.25),
      webPreferences: {
        nodeIntegration: true,
        preload: __dirname + '/preload.js'
      }
    });
    window.resizable = false;
    
    openWindow(window, "/chat");
    window.on('closed', function () {
      resolve();
    })
    window.webContents.on("did-fail-load", () => {
      reject();
    })
  });
}

ipcMain.handle("popout-chat", async (evt, client) => {
  newDrawingRoomClient = client;
  await popoutChat();
});

ipcMain.on("new-drawing-room", (evt, client) => {
  newDrawingRoomClient = client;
});

ipcMain.on("quit-drawing-room", () => {
  newDrawingRoomClient = undefined;
});

ipcMain.handle("get-drawing-room", async () => {
  return await getDrawingRoomClient();
});

ipcMain.handle("listen-quit-drawing-room", async () => {
  return await waitUnsetDrawingRoomClient();
});

app.on('ready', initMainWindow)

// Close when all windows are closed.
app.on('window-all-closed', function () {

  // On macOS specific close process
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  if (win === null) {
    initWindow()
  }
})